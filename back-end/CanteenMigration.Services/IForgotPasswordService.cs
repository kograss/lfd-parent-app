﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IForgotPasswordService
    {
        void SendForgotPasswordEmail(Parent parent);
        void ChangePassword(Parent parent, string newPassword);
    }
}
