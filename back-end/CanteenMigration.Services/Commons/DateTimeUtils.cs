﻿using System;

namespace CanteenMigration.Commons
{
    public static class DateTimeUtils
    {
        public static DateTime GetWithoutTime()
        {
            DateTime now = DateTime.Now;
            DateTime withoutTime = new DateTime(year: now.Year,
                month: now.Month,
                day: now.Day,
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0);
            return withoutTime;
        }

        public static DateTime GetWithoutTime(DateTime now)
        {
            DateTime withoutTime = new DateTime(year: now.Year,
                month: now.Month,
                day: now.Day,
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0);
            return withoutTime;
        }
    }
}