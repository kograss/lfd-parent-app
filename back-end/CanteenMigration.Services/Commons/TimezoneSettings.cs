﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.Commons
{
    public class TimezoneSettings
    {
        public DateTime getDateBySchoolTimeZone(int schoolId, string timeZone)
        {
            //1. get current server date and time
            //2. if timeZone is null set its value 'E'
            //3. add time zone
            if (string.IsNullOrEmpty(timeZone))
                timeZone = "E";

            DateTime currentTime = DateTime.Now;
            switch (timeZone)
            {
                case "W":
                    currentTime = DateTime.Now.AddHours(-2);
                    break;
                case "C":
                    currentTime = DateTime.Now.AddMinutes(-30);
                    break;
                case "E":
                    currentTime = DateTime.Now.AddHours(0); 
                    break;
                case "ET0":
                    currentTime = DateTime.Now.AddMinutes(-30);
                    break;
                case "CDT":
                    currentTime = DateTime.Now.AddMinutes(630);
                    break;
                case "EDT":
                    currentTime = DateTime.Now.AddHours(0);
                    break;
                case "EDT0":
                    currentTime = DateTime.Now.AddHours(-4);
                    break;
                case "EDT1":
                    currentTime = DateTime.Now.AddHours(-2); 
                    break;
                case "EDT3":
                    currentTime = DateTime.Now.AddHours(-9);
                    break;
                case "EDT2":
                    currentTime = DateTime.Now.AddMinutes(270); 
                    break;

                case "EDTQLD":
                    currentTime = DateTime.Now.AddHours(-1);
                    //currentTime = DateTime.Now;
                    break;
                case "EDTWA":
                    currentTime = DateTime.Now.AddHours(-3);
                    break;
                case "EDTNT":
                    currentTime = DateTime.Now.AddMinutes(-90);
                    break;




            }
            return currentTime;
        }
    }
}