﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IUniformService
    {
        List<UniformCategories> GetUniformCategories(int schoolId);
        List<UniformNews> GetUniformNews(int schoolId, int year);
        List<UniformProduct> GetUniformProductsByCategory(int schoolId, int categoryId);
        List<UniformPack> GetUniformPacksByCategory(int schoolId, int categoryId);
        List<Models.UniformBasketItem> GetBasketItems(int basketId, int parentId, int schoolId);
        Models.UniformBasket GetBasket(int basketId, int parentId, int schoolId);
        int AddToBasket(int parentId, int schoolId, UniformCartItem item);
        int GetBasketCount(int basketId, int parentId, int schoolId);
        bool UpdateBasket(int parentId, int schoolId, UpdateCartInput updateCart);
        int? GetProductStock(StockInput input);
        Models.UniformBasket UpdateBasket(int basketId, int parentId, int schoolId, int studentId, string studentClass, string comment , string firstName);
        Models.UniformOrderData CreateOrder(int schoolId, int basketId, Models.Parent parent, string topupMethod, CollectionOption collectionOption);
        Models.UniformOrderPayment CreatePayment(Models.UniformOrderData uniformOrderData, Models.UniformBasket basket, List<Models.UniformBasketItem> basketItems, CollectionOption collectionOption, bool isCreditOrder);
        Models.UniformOrderPayment GetUniformOrderPayment(Models.PaymentGatewayTransactionLog log);
        Models.PaymentGatewayTransactionLog CreatePaymentGateayTransactionLog(Models.UniformOrderPayment uniformOrderPayment);
        List<UniformOrder> GetParentOrders(int parentId, int schoolId, int year);
        List<OrderDetails> GetUniformOrderDetails(int schoolId, int parentId, int orderId);
        bool MakeOrderSuccess(Models.UniformOrderPayment uniformOrderPayment,
                                     string paymentGatewayTransactionId, Models.PaymentGatewayTransactionLog log, int schoolId);
        void UpdateStock(int schoolId, Models.UniformOrderData uniformOrderData, List<Models.UniformBasketItem> basketItems);
        UniformStockCheckResult CheckStock(int schoolId, List<Models.UniformBasketItem> basketItems);
        UniformCheckoutSchoolDetails getUniformSchoolDetails(int schoolId);
        bool MakeOrderSuccess(Models.UniformOrderPayment uniformOrderPayment,
                                     int schoolId, Models.Parent parent);
        List<CanteenMigration.JSONModels.UniformPackProduct> GetUniformPackProducts(int schoolId, int uniformPackId);
        List<Models.UniformPackItemProduct> GetAllUPackSubItem(int id);
        Models.UniformOrderData GetUniformOrderData(int basketId, Models.UniformOrderPayment uniformOrderPayment);
        List<CollectionOption> GetCollectionOptions(int schoolId);
        Models.UniformAddress GetUniformAddress(int parentId);
        Models.UniformAddress AddUniformAddress(Models.UniformAddress uniformAddress);
        Models.UniformAddress UpdateUniformAddress(Models.UniformAddress uniformAddress);
    }
}
