﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IHolidayClousuresService
    {
        HolidayClousures GetHolidayClosures(int schoolId, int year);
    }
}
