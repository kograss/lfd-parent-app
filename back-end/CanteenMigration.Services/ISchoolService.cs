﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface ISchoolService
    {
        SchoolInfo GetSchoolInfo(int schoolId);
        CanteenMigration.JSONModels.SchoolShops GetSchoolShops(int schoolId);
    }
}
