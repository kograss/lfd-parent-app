﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IStripePaymentChargeService
    {
        PaymentResult CreatePaymentCharge(int parentId, int schoolId, string stripeToken, decimal amount,
                            string logId, string transactionType, string parentEmail, bool saveCard,
                            int eventId = 0, int orderId = 0);
        Stripe.PaymentIntent GetPaymentIntent(int parentId, int schoolId, decimal amount,
                            string logId, string transactionType, string parentEmail,
                            int eventId = 0, int orderId = 0);

        Stripe.Card GetCard(string customerId, string cardId);

        Models.CustomerStripeDetails GetStripeDetails(int parentId, int schoolId);
    }
}
