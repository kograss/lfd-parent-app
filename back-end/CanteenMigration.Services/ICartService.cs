﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;

namespace CanteenMigration.Services
{
    public interface ICartService
    {
        List<CartProduct> GetCart(int parentId, int schoolId, DateTime deliveryDate);
        bool UpdateCart(RemoveCartModel removeCart, int schoolId, int parentId);
        AddToCartResult AddItemToCart(AddToCart cart, int schoolId, int parentId);
        List<CanteenShopcartItem> GetAllCartItems(int parentId, int schoolId, DateTime deliveryDate);
        decimal GetCartAmout(List<CanteenShopcartItem> cartList, int schoolId);
    }
}