﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IOrderHistoryService
    {
        List<ParentOrder> GetParentOrder(int schoolId, int parentId, bool orderCancelStatus, int year);
        IEnumerable<OrderItemProduct> GetOrderDetails(int schoolId, int orderId, bool isRepeatOrder);
        ReorderOrderItemProducts GetOrderDetailsForReOrder(int schoolId, int orderId);
        ParentOrder GetParentOrder(int schoolId, int orderId, int parentId);
        SchoolOrder GetSchoolOrder(int schoolId, int orderId, int parentId);
        IEnumerable<OrderItemProduct> GetOrderDetailsForEvent(int schoolId, int orderId, bool isSpecialOrder);
        IEnumerable<OrderItemProduct> GetOrderDetailsForSchoolEvent(int schoolId, int orderId);
        List<CanteenMigration.JSONModels.ParentTransaction> GetAllParentsTransactions(int parentId, int schoolId, int year);
        List<ParentOrder> GetFundRaisingOrder(int schoolId, int parentId, int year);
    }
}
