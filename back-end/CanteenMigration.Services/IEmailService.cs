﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;

namespace CanteenMigration.Services
{
    public interface IEmailService
    {
        bool AddEmail(string to, int orderId, int parentId, int schoolId, string parentName);
        bool SendForgotPasswordLink(string toEmail, string subject, string linkToSend, string parentName);
        bool SendParentPlanUpdatedEmail(string toEmail, decimal balanceBefore, decimal balanceAfter, decimal amount, string parentName);
        bool SendCancelOrderEmail(string toEmail, decimal balanceBefore, decimal balaceAfter, decimal orderAmount, string parentName, string orderId);
        bool SendEventEmail(string to, int orderId, int parentId, int schoolId, string parentName);
        bool SendSchoolEventEmail(string to, int orderId, int parentId, int schoolId, string parentName);
        bool SendManultopupEmailToAdmin(string toEmail, decimal topupAmount, string parentName, string comment);
        bool SendUniformOrderEmail(int orderId, int parentId, int schoolId, string parentEmail, string parentName, DateTime orderdate, string studentClass, string comment, CollectionOption collectionOption = null);
        bool SendAsynEmail(string to, int orderId, int parentId, int schoolId, string parentName);
        bool SendRegistrationEmail(string toEmail, string parentName, string linkToVerify);
        bool SendAsynEmailDonationEvent(string to, string parentName, string eventName, int amount);
    }
}
