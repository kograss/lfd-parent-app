﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IPaymentService
    {
        PaymentResult MakePayment(StripePaymentRequest paymentRequest, decimal orderAmount, int schoolId, int parentId,
           string logId, string transactionType, string parentEmail);
    }
}
