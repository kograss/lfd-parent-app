﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IStudentService
    {
        List<StudentWithKlass> GetStudentsByParent(int schoolId, int parentId, bool status);
        bool AddNewStudent(Student newStudent, int schoolId);
        bool EditStudent(Student student, int schoolId);
        List<KlassModel> GetAllClasses(int schoolId);
    }
}
