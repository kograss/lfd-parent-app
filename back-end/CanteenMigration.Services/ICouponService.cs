﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface ICouponService
    {
        List<CouponsForParent> GetAllCoupons(int parentId, int schoolId);
        RedeemResult RedeemCoupon(int couponId, int parentId, int schoolId);
    }
}
