﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IProductService
    {
        List<Product> GetProducts(int schoolId, string mealName, int categoryId, DateTime deliveryDate);
        List<ProductStock> GetProductsStock(List<int> productsIds);
    }
}
