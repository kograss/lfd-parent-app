﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IOrderCheckService
    {
        bool CheckIfOrderCanBePlace(SchoolInfo schoolInfo, Parent parent, DateTime deliveryDate, List<CanteenShopcartItem> cartItems,
            decimal totalOrderAmount, bool isCreditPayment, bool isPayAtPickUpOrder, bool isCutOffCheckRequired);
    }
}
