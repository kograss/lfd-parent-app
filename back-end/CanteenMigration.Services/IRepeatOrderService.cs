﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IRepeatOrderService
    {
        OrderResult PlaceRepeatOrder(int orderId, DateTime deliveryDate, bool isCreditOrder, string transactionId, int parentId, int schoolId);
        (bool, List<String>) AddOrderToCart(int parentId, int schoolId, int orderId, DateTime deliveryDate); 
    }
}
