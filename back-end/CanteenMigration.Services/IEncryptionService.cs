﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
   public  interface IEncryptionService
    {

       string encrypt(string encryptString);
       string decrypt(string encrypytedString);
    }
}
