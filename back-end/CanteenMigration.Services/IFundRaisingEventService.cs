﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IFundRaisingEventService
    {
        List<FundRaisingEventOrders> AddDonation(int parentId, int schoolId, int eventId, List<JSONModels.StripeDonationRequest.StudentAndAmount> students, string status);
        bool Update(List<FundRaisingEventOrders> lst, string paymentGatewayTransactionId, string stripeTransactionId, string status);
        bool AddTransaction(List<FundRaisingEventOrders> lst, int parentId, int schoolId, decimal credit, string transactionId, bool isCreditOrder);
    }
}
