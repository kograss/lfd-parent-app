﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class RotaService : IRotaService
    {
        IRotaRepository _rotaRepository;
        IPublicHolidaysRepository _publicHolidayRepository;
        public RotaService(IRotaRepository rotaRepository, IPublicHolidaysRepository publicHolidayRepository)
        {
            _rotaRepository = rotaRepository;
            _publicHolidayRepository = publicHolidayRepository;
        }

        public List<JSONModels.RosterNews> GetRosterNews(int schoolId)
        {
            return _rotaRepository.GetRosterNews(schoolId);
        }

        public List<ParentsShift> GetParentShifts(int parentId, int schoolId)
        {
            return _rotaRepository.GetParentShifts(parentId, schoolId);
        }

        public List<ShiftsByDate> GetMonthsShiftsAndVolunteers(int schoolId, int month, int year)
        {
            month += 1;
            DateTime startDay = new DateTime(year, month, 1);
            int tmp = (int)startDay.DayOfWeek;

            DateTime endDay = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            IEnumerable<DateTime> days = Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                                                    .Select(day => new DateTime(year, month, day));

            IEnumerable<RosterHolidays> _holidays = _rotaRepository.GetHolidayByMonth(schoolId, month);//_rotaRepository.GetMonthsHoliday(schoolId, startDay, endDay);
            List<PublicHolidays> publicHolidays = _publicHolidayRepository.GetPublicHolidaysByMonth(schoolId, month);
            IEnumerable<Shift> shifts = _rotaRepository.GetSchoolShifts(schoolId);

            List<ShiftsByDate> shiftsByDate = (from sd in days
                                               select new ShiftsByDate
                                               {
                                                   date = sd,
                                                   isHoliday = (_holidays.Count(e => e.holidayStartDate <= sd && e.holidayEndDate >= sd) > 0
                                                                || publicHolidays.Count(e => e.holidayDateStart <= sd && e.holidayDateEnd >= sd) > 0),
                                                   isNonWorkingday = shifts.Where(e => e.day == ((int)sd.DayOfWeek + 1)).Count() < 1,
                                                   shifts = GetShiftsForDate(
                                                       schoolId,
                                                       sd,
                                                      shifts.Where(e => e.day == (int)sd.DayOfWeek + 1 || (e.day2Work == sd && e.isSingleDateShift != null && e.isSingleDateShift == true)),
                                                   _holidays.Count(e => e.holidayStartDate >= sd && e.holidayEndDate <= sd) > 0,
                                                   shifts.Where(e => e.day == (int)sd.DayOfWeek + 1 || (e.day2Work == sd && e.isSingleDateShift != null && e.isSingleDateShift == true)).Count() < 1)
                                               }).ToList();

            return shiftsByDate;
            //return _rotaRepository.GetMonthsShiftsAndVolunteers(schoolId, month, year);
        }

        private List<ShiftInfo> GetShiftsForDate(int schoolId, DateTime shiftDate, IEnumerable<Shift> shifts, bool isHoliday, bool isNonWorkingDay)
        {
            List<ShiftInfo> shiftmodels = null;
            if (!isHoliday && !isNonWorkingDay)
            {

                IEnumerable<VolsAndShift> shiftsWithVolunteers = _rotaRepository.GetShiftsWithVolunteers(schoolId, shiftDate);
                int? leader = shiftsWithVolunteers.Where(e => e.Extra3 == 1).Count() > 0 ?
                                    shiftsWithVolunteers.Where(e => e.Extra3 == 1).FirstOrDefault().ParentId.Value : -1;

                shiftmodels = shifts.Select(
                shift => new ShiftInfo
                {
                    id = (int)shift.shiftId,
                    name = shift.shiftName,
                    shiftMax = (int)shift.shiftMax,

                    date = shiftDate,
                    leader = leader.Value,
                    volunteers = shiftsWithVolunteers.Where(e => e.ShiftId == shift.shiftId).Select(m => new Volunteer
                    {
                        id = (int)m.ParentId,
                        name = m.FirstName + " " + m.LastName,
                        email = m.Email,
                        mobile = m.Mobile
                    }
                    ).ToList(),
                    noOfVolReq = (int)shift.shiftMax - shiftsWithVolunteers.Where(e => e.ShiftId == shift.shiftId).Count(),
                    isPastDay = shiftDate >= DateTime.Now ? false : true
                }
            ).ToList();
                return shiftmodels;
            }
            else
            {
                return null;
            }
        }

        public bool RegisterVolunteerToShift(int schoolId, int shiftId, int parentId, DateTime regDate, string name, ref string _message)
        {
            return _rotaRepository.RegisterVolunteerToShift(schoolId, shiftId, parentId, regDate, name, ref _message);
        }
    }
}
