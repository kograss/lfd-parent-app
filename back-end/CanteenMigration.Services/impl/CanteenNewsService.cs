﻿
using CanteenMigration.DataDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CanteenMigration.Services
{
    public class CanteenNewsService: ICanteenNewsService
    {
        INewsRepository _newsRepo;
        IOrderHistoryRepository _orderRepo;
        public CanteenNewsService(INewsRepository newsRepo, IOrderHistoryRepository orderRepo)
        {
            this._newsRepo = newsRepo;
            this._orderRepo = orderRepo;
        }

        public List<CanteenNewsModel> GetCanteenNews(int schoolId)
        {
            try
            {
                List<CanteenNewsModel> newsForUI = new List<CanteenNewsModel>();
              List<CanteenNews> newsFromDB =   _orderRepo.GetCanteenNews(schoolId);
              foreach (var item in newsFromDB)
              {
                  CanteenNewsModel news = new CanteenNewsModel();
                  news.NewsID = item.NewsId;
                  news.Comment = item.Comment;
                  news.Description = item.Description;
                  news.DateEntered = item.DateEntered.Value;
                  news.Priority = Convert.ToInt32(item.Priority);
                  news.Subject = item.Subject;
                  news.EnteredBy = item.EnteredBy;
                  news.schoolid = item.SchoolId.Value;
                  news.dayid = item.DayId;

                  newsForUI.Add(news);

              }
              return newsForUI;
               //string strNews = _newsRepo.GetCanteenNews(schoolId);
               // if (!string.IsNullOrEmpty(strNews))
               //    return JsonConvert.DeserializeObject<List<CanteenNewsModel>>(strNews);
               // else
               //     return null;                
            }
            catch(JsonException ex)
            {
                throw ex;
            }
           
        }

    }
}
