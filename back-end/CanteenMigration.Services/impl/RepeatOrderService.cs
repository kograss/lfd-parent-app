﻿using CanteenMigration.Commons;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class RepeatOrderService : IRepeatOrderService
    {
        IProductRepository _productRepository;
        IOrderRepository _orderRepository;
        ISchoolRepository _schoolRepository;
        IParentRepository _parentRepository;
        ITransactionRepository _transactionRepo;
        IDBContext _context;
        IOrderHistoryService orderHistoryService;
        ICartService cartService;
        ISchoolService _schoolService;
        public RepeatOrderService(IProductRepository _productRepository, IOrderRepository _orderRepository,
                                  ISchoolRepository _schoolRepository, IParentRepository _parentRepository,
                                  ITransactionRepository _transactionRepo, IDBContext _context,
                                  IOrderHistoryService orderHistoryService, ICartService cartService,
                                  ISchoolService schoolService)
        {
            this._productRepository = _productRepository;
            this._orderRepository = _orderRepository;
            this._schoolRepository = _schoolRepository;
            this._parentRepository = _parentRepository;
            this._transactionRepo = _transactionRepo;
            this._context = _context;
            this.orderHistoryService = orderHistoryService;
            this.cartService = cartService;
            _schoolService = schoolService;
        }

        public (bool, List<String>) AddOrderToCart(int parentId, int schoolId, int orderId, DateTime deliveryDate)//repeat order
        {
            var school = _context.Schools.Where(e => e.schoolId == schoolId).FirstOrDefault();
            List<String> missingProductsName = new List<String>();
            List<Models.Product> products = new List<Models.Product>();
            List<int> productsIds = new List<int>();
            
            IEnumerable<OrderItemProduct> orderedItems = orderHistoryService.GetOrderDetails(schoolId, orderId, true);
            //IEnumerable<OrderItemProduct> missingProducts = orderedItems.Where(orderedItem => _context.Products
            //                    .Where(product => product.id == orderedItem.productId)
            //                    .FirstOrDefault().isAvailableToOrder != 0 );

            foreach (OrderItemProduct orderedItem in orderedItems)
            {
                productsIds.Add(orderedItem.productId);

            }

            products = _context.Products
                                .Where(product => productsIds.Contains(product.id))
                                .ToList();
            foreach (Models.Product product in products)
            {
                if(product.isAvailableToOrder != 1 || (product.stock != null && product.stock <= 0) || product.schoolId == 0 || product.schoolId == 445 )
                {
                    missingProductsName.Add(product.productName);
                }

            }
            if(missingProductsName.Count() > 0)
            {
                return (false, missingProductsName);
            }
            else
            {
                foreach (OrderItemProduct orderedItem in orderedItems)
                {
                     
                        AddToCart addTocart = new AddToCart();
                        addTocart.parentId = parentId;
                        addTocart.schoolId = schoolId;
                        addTocart.productId = orderedItem.productId;
                        addTocart.studentIds = new int[] { orderedItem.studentId };
                        addTocart.quantity = orderedItem.quantity;
                        addTocart.optionIds = orderedItem.productOptions;
                        addTocart.deliveryDate = deliveryDate;
                        addTocart.mealName = orderedItem.mealCategory;
                        cartService.AddItemToCart(addTocart, schoolId, parentId);
                }
            }


                
            


            return (true, missingProductsName);
        }
        public OrderResult PlaceRepeatOrder(int orderId, DateTime deliveryDate, bool isCreditOrder, string transactionId, int parentId, int schoolId)
        {
            OrderResult orderResult = new OrderResult();
            Parent parent = _parentRepository.GetParent(schoolId, parentId);
            if (parent == null)
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "Parent not found";
                return orderResult;
            }
            orderResult.isOrderSuccess = false;
            orderResult.message = "Parent not found";
            return orderResult;
            /**SchoolInfo schoolInfo = new SchoolInfo();
            string schoolInfoJson = _schoolRepository.GetSchoolInfo(schoolId);
            if (string.IsNullOrEmpty(schoolInfoJson))
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "School not found. Please contact admin";
                return orderResult;
            }

            schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJson);
            */
            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "School not found. Please contact admin";
                return orderResult;
            }

            //1.Fetch parents_order
            ParentsOrders parentsOrder = _orderRepository.GetParentOrder(orderId, schoolId);
            if (parentsOrder == null)
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "Order not found";
                return orderResult;
            }

            if (parentsOrder.orderAmount.Value <= 0)
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "We cannot process this order. Error Number: $_MISSING.";
                return orderResult;
            }

            if (parent.credit.Value < parentsOrder.orderAmount)
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "We cannot process this order. Error Number: $_NEG";
                return orderResult;
            }

            if (string.IsNullOrEmpty(parentsOrder.orderType))
            {
                orderResult.isOrderSuccess = false;
                orderResult.message = "Order type is missing";
                return orderResult;
            }

            Orders lunchOrder = null;
            if (parentsOrder.orderType.Contains("L"))
            {
                //lunch closures
                if (!_schoolRepository.IsMealAvailableForDate(schoolId, deliveryDate, "LUNCH"))
                {
                    orderResult.isOrderSuccess = false;
                    orderResult.message = "We cannot process this order because " + schoolInfo.meal2 + " is closed on the selected date.";
                    return orderResult;
                }

                lunchOrder = _orderRepository.GetMealWiseOrders("0" + parentsOrder.orderId, schoolId);
            }

            Orders recessOrder = null;
            if (parentsOrder.orderType.Contains("R"))
            {
                //recess closures
                if (_schoolRepository.IsMealAvailableForDate(schoolId, deliveryDate, "RECESS"))
                {
                    orderResult.isOrderSuccess = false;
                    orderResult.message = "We cannot process this order because " + schoolInfo.meal1 + " is closed on the selected date.";
                    return orderResult;
                }

                lunchOrder = _orderRepository.GetMealWiseOrders("R" + parentsOrder.orderId, schoolId);
            }

            Orders thirdOrder = null;
            if (parentsOrder.orderType.Contains("T"))
            {
                //third break closures
                if (string.IsNullOrEmpty(schoolInfo.meal3))
                {
                    orderResult.isOrderSuccess = false;
                    orderResult.message = "We cannot process this order because " + schoolInfo.meal3 + " is closed on the selected date.";
                    return orderResult;
                }

                lunchOrder = _orderRepository.GetMealWiseOrders("T" + parentsOrder.orderId, schoolId);
            }


            //step 1. copy master order - parents_order

            //step 2. copy orders - lunchOrder, recessOrder, thirdOrder

            //step 3. Update parent balace

            //step 4. insert to transaction

            //step 5. send email
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    //deduct stock

                    string orderNumber = Guid.NewGuid().ToString();
                    ParentsOrders newparentsOrder = new ParentsOrders();
                    newparentsOrder.studentId = parentsOrder.studentId;
                    newparentsOrder.schoolId = schoolId;
                    newparentsOrder.orderDate = DateTime.Now;
                    newparentsOrder.orderNumber = orderNumber;
                    newparentsOrder.orderComment = parentsOrder.orderComment;
                    newparentsOrder.orderReason = parentsOrder.orderReason;
                    newparentsOrder.orderShippedDate = deliveryDate;
                    newparentsOrder.orderAmount = parentsOrder.orderAmount;
                    newparentsOrder.orderType = parentsOrder.orderType;
                    newparentsOrder.bag = parentsOrder.bag;
                    newparentsOrder.bagPrice = parentsOrder.bagPrice;
                    newparentsOrder.parentId = parentId;
                    newparentsOrder.schoolModel = parentsOrder.schoolModel;
                    newparentsOrder.schoolPlan = parentsOrder.schoolPlan;
                    newparentsOrder.parentPlan = parent.parentFeePlan;
                    newparentsOrder.orderCancel = false;

                    _orderRepository.AddParentsOrder(parentsOrder);

                    string orderNumberLunch = "0" + newparentsOrder.orderId;
                    string orderNumberRecess = "R" + newparentsOrder.orderId;
                    string orderNumberThird = "T" + newparentsOrder.orderId;

                    //copy order items
                    if (lunchOrder != null)
                    {
                        List<OrderItems> orderItemsList = new List<OrderItems>();
                        List<OrderItems> orderItems = _orderRepository.GetOrderItems(lunchOrder.id, schoolId);
                        foreach (OrderItems orderItem in orderItems)
                        {
                            //insert into oitems
                            OrderItems newOrderItem = new OrderItems();
                            newOrderItem.orderID = lunchOrder.id;
                            newOrderItem.orderIntemNumber = orderItem.orderIntemNumber;
                            newOrderItem.productId = orderItem.productId;
                            newOrderItem.options = orderItem.options;
                            newOrderItem.numerOfItems = orderItem.numerOfItems;
                            newOrderItem.schoolId = schoolId;
                            newOrderItem.extra2 = orderItem.extra2;

                            orderItemsList.Add(newOrderItem);
                        }

                        _orderRepository.InsertLunchOrderItems(orderItemsList);
                    }

                    if (recessOrder != null)
                    {
                        List<OrderItemRecess> orderItemsList = new List<OrderItemRecess>();
                        List<OrderItems> orderItems = _orderRepository.GetOrderItems(recessOrder.id, schoolId);
                        foreach (OrderItems orderItem in orderItems)
                        {
                            OrderItemRecess newOrderItemRecess = new OrderItemRecess();
                            newOrderItemRecess.orderId = recessOrder.id;
                            newOrderItemRecess.orderIntemNumber = orderItem.orderIntemNumber;
                            newOrderItemRecess.productId = orderItem.productId;
                            newOrderItemRecess.options = orderItem.options;
                            newOrderItemRecess.numerOfItems = orderItem.numerOfItems;
                            newOrderItemRecess.schoolId = schoolId;
                            newOrderItemRecess.extra2 = orderItem.extra2;
                            orderItemsList.Add(newOrderItemRecess);
                        }

                        _orderRepository.InsertRecessOrderItems(orderItemsList);
                    }

                    if (thirdOrder != null)
                    {
                        List<OrderItemRecess> orderItemsList = new List<OrderItemRecess>();
                        List<OrderItems> orderItems = _orderRepository.GetOrderItems(thirdOrder.id, schoolId);
                        foreach (OrderItems orderItem in orderItems)
                        {
                            OrderItemRecess newOrderItemRecess = new OrderItemRecess();
                            newOrderItemRecess.orderId = thirdOrder.id;
                            newOrderItemRecess.orderIntemNumber = orderItem.orderIntemNumber;
                            newOrderItemRecess.productId = orderItem.productId;
                            newOrderItemRecess.options = orderItem.options;
                            newOrderItemRecess.numerOfItems = orderItem.numerOfItems;
                            newOrderItemRecess.schoolId = schoolId;
                            newOrderItemRecess.extra2 = orderItem.extra2;
                            //_orderRepo.SaveContext();
                            orderItemsList.Add(newOrderItemRecess);
                        }

                        _orderRepository.InsertRecessOrderItems(orderItemsList);
                    }

                    //Create record in Transactions
                    Transaction transaction = new Transaction();
                    transaction.parentId = parentId;
                    transaction.schoolId = schoolId;
                    transaction.transactionAmount = newparentsOrder.orderAmount;
                    transaction.transactionType = "ORDER:DEBIT";
                    transaction.extra1 = newparentsOrder.orderId.ToString();
                    transaction.balanceBefore = parent.credit;
                    transaction.balanceAfter = isCreditOrder ? parent.credit - newparentsOrder.orderAmount : parent.credit;
                    transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                    transaction.studentId = newparentsOrder.studentId;
                    transaction.transactionId = transactionId;
                    _transactionRepo.AddTransaction(transaction);

                    //Update credit
                    if (isCreditOrder)
                    {
                        if (newparentsOrder.orderAmount > parent.credit)
                        {
                            dbContextTransaction.Rollback();
                            orderResult.isOrderSuccess = false;
                            orderResult.message = "Not enough Credit to complete this order";
                            orderResult.orderIds = null;
                            return orderResult;
                        }
                        else
                        {
                            _parentRepository.DeductParentBalance(parentId, schoolId, newparentsOrder.orderAmount.Value);
                        }
                    }

                    dbContextTransaction.Commit();
                    orderResult.isOrderSuccess = true;
                    orderResult.message = "Order placed successfully";
                    orderResult.orderIds.Add(newparentsOrder.orderId);
                    return orderResult;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    orderResult.isOrderSuccess = false;
                    orderResult.message = "Error while placing order. Please try again";
                    orderResult.orderIds = null;
                    return orderResult;
                }
            }


            //TODO: add category cut off

        }
    }
}
