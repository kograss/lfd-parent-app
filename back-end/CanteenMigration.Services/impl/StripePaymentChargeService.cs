﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services.exception;
using DataDependencies;
using Stripe;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class StripePaymentChargeService : IStripePaymentChargeService
    {
        IStripePaymentChargeRepository _stripePaymentChargeRepository;

        public const string PAYMENTGATEWAYCONNECTIONFAILURE = "Some Products are out of stock";
        static NameValueCollection cardErrors = ConfigurationManager.GetSection("stripeCardErrors") as NameValueCollection;
        IDBContext _context;


        //Stripe old version 34.20.0
        public static String Key
        {
            get
            {
                return "pk_test_51NutyDKMs9a5GaqIjHnY6BmKOPerIb91MQdsXhFojrwpo8JIz9JaLx4te7CK6o2GHrhelaj4lA7Nx1jSPGLmUW2n00BTadpeJO"; 
            }
        }

        public static String Secret
        {
            get
            {
                return "sk_test_51NutyDKMs9a5GaqI7Wu8magrHV0Otjq4zWlbIKcGSlfg9W45ajPLFESXiLDt4KMlJVd7H3ES6v1Vi74nR9d8I67A00cqZSbEPb"; 
            }
        }

        public ILoggerService<StripePaymentChargeService> _loggingService;
        public StripePaymentChargeService(ILoggerService<StripePaymentChargeService> _loggingService, IStripePaymentChargeRepository _stripePaymentChargeRepository)
        {
            this._loggingService = _loggingService;
            this._stripePaymentChargeRepository = _stripePaymentChargeRepository;
        }

        public Customer GetCustomer(int parentId, string parentEmail, string stripeToken, int schoolId)
        {
            
            CustomerStripeDetails customerStripeDetails = _stripePaymentChargeRepository.GetParentStripeDetails(schoolId, parentId);
            //var cust = this.GetCustomer("cus_OhxBhmOKhxbqux");
            var service = new CustomerService();
            Customer customer = new Customer();
            try
            {
                if (customerStripeDetails != null)
                {
                    var options = new CustomerUpdateOptions
                    {
                        Source = stripeToken,
                    };
                    customer = service.Update(customerStripeDetails.customerId, options);
                    var card = GetCard(customer.Id, customer.DefaultSourceId);
                    this._stripePaymentChargeRepository.UpdateParentStripeDetails(schoolId, parentId, customer.DefaultSourceId, card.Last4);

                }
                else
                {
                    var options = new CustomerCreateOptions
                    {
                        Email = parentEmail,
                        Source = stripeToken,
                        Description = "Parent ID: " + parentId,
                    };
                    customer = service.Create(options);
                    var card = GetCard(customer.Id, customer.DefaultSourceId);
                    this._stripePaymentChargeRepository.AddParentStripeDetails(schoolId, parentId, customer.Id, customer.DefaultSourceId, card.Last4);

                }
                
            }
            catch (StripeException exception)
            {

                _loggingService.Debug("---------------------------get stripe customer error into starts here----------------------------------- StripeException");
                _loggingService.Debug(exception.StripeError.ErrorType + " " + exception.StripeError.Error + " " + exception.StripeError.ErrorDescription);
                _loggingService.Debug("--------------------------get stripe customer error ends here------------------------------------");

                switch (exception.StripeError.ErrorType)
                {
                    case "api_connection_error":

                    case "api_error":

                    case "authentication_error":

                    case "rate_limit_error":
                        throw new PaymentException(PAYMENTGATEWAYCONNECTIONFAILURE);

                    default:

                        switch (exception.StripeError.Code)
                        {
                            case "incorrect_cvc":
                                throw new PaymentException("Incorrect CVC: " + exception.StripeError.Message);
                            default:
                                throw new PaymentException(exception.StripeError.Message);
                        }
                }
            }
            catch (Exception e)
            {
                _loggingService.Debug("---------------------------get stripe customer error into starts here------------------------------------ StripeException");
                _loggingService.Debug(e.ToString() + ": " + e.Message );
                _loggingService.Debug("---------------------------get stripe customer error ends here------------------------------------");
            }
            return customer;

        }

        public Card GetCard(string customerId, string cardId)
        {
            var service = new CardService();
            return service.Get(customerId, cardId);
        }



        public CustomerStripeDetails GetStripeDetails(int parentId, int schoolId)
        {
            return _stripePaymentChargeRepository.GetParentStripeDetails(schoolId, parentId);
        }

        public PaymentIntent GetPaymentIntent(int parentId, int schoolId, decimal amount,
                            string logId, string transactionType, string parentEmail,
                            int eventId = 0, int orderId = 0)
        {
            StripeConfiguration.ApiKey = Secret;


            CustomerStripeDetails customerStripeDetails = _stripePaymentChargeRepository.GetParentStripeDetails(schoolId, parentId);
            //var cust = this.GetCustomer("cus_OhxBhmOKhxbqux");
            if (customerStripeDetails != null)
            {
                var paymentIntentService = new PaymentIntentService();

                var options = new PaymentIntentCreateOptions
                {
                    Customer = customerStripeDetails.customerId,
                    Amount = (Int32)(amount * 100),
                    Currency = "AUD",
                    ReceiptEmail = parentEmail,
                    PaymentMethod = customerStripeDetails.cardId,
                    Confirm = true,
                    OffSession = true,
                };

                if (eventId == 0)
                {
                    options.Description = transactionType + "- SchoolId - " + schoolId + " ParentId - " + parentId;
                    if (orderId > 0)
                    {
                        options.Description = options.Description + " OrderId - " + orderId;
                    }
                }
                else
                {
                    options.Description = transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId + " EventId - " + eventId;
                    if (orderId > 0)
                    {
                        options.Description = options.Description + " OrderId - " + orderId;
                    }
                }

                options.Metadata = new Dictionary<string, string>();
                options.Metadata["OurRef"] = logId;

                // Install stripe CLI
                //Charge stripeCharge = new Charge();
                //PaymentResult result = new PaymentResult();
                //result.paymentStatus = false;
                var paymentIntent = paymentIntentService.Create(options);


                return paymentIntent;
            }

            return null;

        }

        public PaymentResult CreatePaymentCharge(int parentId, int schoolId, string stripeToken, decimal amount,
                            string logId, string transactionType, string parentEmail, bool saveCard,
                            int eventId = 0, int orderId = 0)
        {
            StripeConfiguration.ApiKey = Secret;


            

            var myCharge = new ChargeCreateOptions();
            if(saveCard == false)
            {
                myCharge.Source = stripeToken;
            }
            else
            {
                var customer = this.GetCustomer(parentId, parentEmail, stripeToken, schoolId);
                myCharge.Customer = customer.Id;
            }
            
            //myCharge.Customer = "cus_OhxBhmOKhxbqux";
            //myCharge.Source = stripeToken;
            //card_1NuHYfDwUuQChFLdxi2KUJBx
            myCharge.Amount = (Int32)(amount * 100);
            myCharge.Currency = "AUD";
            myCharge.ReceiptEmail = parentEmail;

            //myCharge.Description = eventId == 0 ? transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId
            //  : transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId + " EventId - " + eventId;

            if (eventId == 0)
            {
                myCharge.Description = transactionType + "- SchoolId - " + schoolId + " ParentId - " + parentId;
                if (orderId > 0)
                {
                    myCharge.Description = myCharge.Description + " OrderId - " + orderId;
                }
            }
            else
            {
                myCharge.Description = transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId + " EventId - " + eventId;
                if (orderId > 0)
                {
                    myCharge.Description = myCharge.Description + " OrderId - " + orderId;
                }
            }

            myCharge.Metadata = new Dictionary<string, string>();
            myCharge.Metadata["OurRef"] = logId;

            var chargeService = new ChargeService();
            Charge stripeCharge = new Charge();
            PaymentResult result = new PaymentResult();
            result.paymentStatus = false;

            try
            {
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                //                                      | SecurityProtocolType.Tls11
                //                                    | SecurityProtocolType.Tls
                //                                  | SecurityProtocolType.Ssl3;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                stripeCharge = chargeService.Create(myCharge); //deduct from user card

                if (stripeCharge.Status.Equals("succeeded") && stripeCharge.Paid == true)
                {
                    _loggingService.Debug("---------------------------payment successful for OrderID: " + orderId + " and SchoolId - " + schoolId);
                    result.paymentStatus = true;
                    result.message = "Payment Successful";
                    result.paymentGatewayTransactionId = stripeCharge.Id;


                }
                else
                {
                    _loggingService.Debug("---------------------------payment failed for OrderID: " + orderId + " and SchoolId - " + schoolId);
                }
            }
            catch (StripeException exception)
            {
                result.paymentStatus = false;

                _loggingService.Debug("---------------------------payment error into starts here------------------------------------ StripeException");
                _loggingService.Debug(exception.StripeError.ErrorType + " " + exception.StripeError.Error + " " + exception.StripeError.ErrorDescription);
                _loggingService.Debug("---------------------------payment error ends here------------------------------------");

                switch (exception.StripeError.ErrorType)
                {
                    case "api_connection_error":

                    case "api_error":

                    case "authentication_error":

                    case "rate_limit_error":
                        throw new PaymentException(PAYMENTGATEWAYCONNECTIONFAILURE);

                    default:
                        
                        result.message = exception.StripeError.ErrorType + ": " + exception.StripeError.Message ;
                        switch (exception.StripeError.Code)
                        {
                            case "incorrect_cvc":
                                throw new PaymentException("Incorrect CVC: "+exception.StripeError.Message);
                            default:
                                throw new PaymentException(exception.StripeError.Message);
                        }
                }
            }
            catch (Exception e)
            {
                result.paymentStatus = false;

                _loggingService.Debug("---------------------------payment error into starts here------------------------------------");

                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);

                    }

                    if (e.Message != null)
                    {
                        _loggingService.Debug(e.Message);
                    }
                }
                _loggingService.Debug("---------------------------payment error ends here------------------------------------");
            }

            return result;
        }
    }

    //public PaymentResult CreatePaymentCharge(int parentId, int schoolId, string stripeToken, decimal amount,
    //                    string logId, string transactionType, string parentEmail,
    //                    int eventId = 0, int orderId = 0)
    //{
    //    StripeConfiguration.ApiKey = Secret;
    //    //var options = new CustomerCreateOptions();
    //    //var service = new CustomerService();
    //    //service.Create(options);


    //    var myCharge = new ChargeCreateOptions();
    //    myCharge.Source = stripeToken;
    //    myCharge.Amount = (Int32)(amount * 100);
    //    myCharge.Currency = "AUD";
    //    myCharge.ReceiptEmail = parentEmail;
    //    //myCharge.Description = eventId == 0 ? transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId
    //    //  : transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId + " EventId - " + eventId;

    //    if (eventId == 0)
    //    {
    //        myCharge.Description = transactionType + "- SchoolId - " + schoolId + " ParentId - " + parentId;
    //        if (orderId > 0)
    //        {
    //            myCharge.Description = myCharge.Description + " OrderId - " + orderId;
    //        }
    //    }
    //    else
    //    {
    //        myCharge.Description = transactionType + "- schoolId - " + schoolId + " ParentId - " + parentId + " EventId - " + eventId;
    //        if (orderId > 0)
    //        {
    //            myCharge.Description = myCharge.Description + " OrderId - " + orderId;
    //        }
    //    }

    //    myCharge.Metadata = new Dictionary<string, string>();
    //    myCharge.Metadata["OurRef"] = logId;

    //    var chargeService = new ChargeService();
    //    Charge stripeCharge = new Charge();
    //    PaymentResult result = new PaymentResult();
    //    result.paymentStatus = false;

    //    try
    //    {
    //        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
    //        //                                      | SecurityProtocolType.Tls11
    //        //                                    | SecurityProtocolType.Tls
    //        //                                  | SecurityProtocolType.Ssl3;

    //        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

    //        stripeCharge = chargeService.Create(myCharge); //deduct from user card

    //        if (stripeCharge.Status.Equals("succeeded") && stripeCharge.Paid == true)
    //        {
    //            _loggingService.Debug("---------------------------payment successful for OrderID: " + orderId + " and SchoolId - " + schoolId);
    //            result.paymentStatus = true;
    //            result.message = "Payment Successful";
    //            result.paymentGatewayTransactionId = stripeCharge.Id;
    //        }
    //        else
    //        {
    //            _loggingService.Debug("---------------------------payment failed for OrderID: " + orderId + " and SchoolId - " + schoolId);
    //        }
    //    }
    //    catch (StripeException exception)
    //    {
    //        result.paymentStatus = false;

    //        _loggingService.Debug("---------------------------payment error into starts here------------------------------------ StripeException");
    //        _loggingService.Debug(exception.StripeError.Type + " " + exception.StripeError.Error + " " + exception.StripeError.ErrorDescription);
    //        _loggingService.Debug("---------------------------payment error ends here------------------------------------");

    //        switch (exception.StripeError.Type)
    //        {
    //            case "api_connection_error":

    //            case "api_error":

    //            case "authentication_error":

    //            case "rate_limit_error":
    //                throw new PaymentException(PAYMENTGATEWAYCONNECTIONFAILURE);

    //            case "card_error":
    //                throw new PaymentException(cardErrors[exception.StripeError.DeclineCode]);

    //            default:
    //                result.message = exception.StripeError.Type + " " + exception.StripeError.Error + " " + exception.StripeError.ErrorDescription;
    //                break;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        result.paymentStatus = false;

    //        _loggingService.Debug("---------------------------payment error into starts here------------------------------------");

    //        if (e != null)
    //        {
    //            if (e.InnerException != null)
    //            {
    //                _loggingService.Debug(e.InnerException.ToString());
    //            }

    //            if (e.StackTrace != null)
    //            {
    //                _loggingService.Debug(e.StackTrace);

    //            }

    //            if (e.Message != null)
    //            {
    //                _loggingService.Debug(e.Message);
    //            }
    //        }
    //        _loggingService.Debug("---------------------------payment error ends here------------------------------------");
    //    }

    //    return result;
    //}
}
