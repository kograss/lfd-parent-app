﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using CanteenMigration.Models;
using Newtonsoft.Json;
using CanteenMigration.Commons;

namespace CanteenMigration.Services.impl
{
    public class CouponService : ICouponService
    {
        ICouponRepository _couponRepo;
        IParentRepository _parentsRepo;
        ITransactionRepository _transactionRepo;
        public CouponService(ICouponRepository _couponRepo, IParentRepository _parentsRepo, ITransactionRepository _transactionRepo)
        {
            this._couponRepo = _couponRepo;
            this._parentsRepo = _parentsRepo;
            this._transactionRepo = _transactionRepo;
        }

        public List<CouponsForParent> GetAllCoupons(int parentId, int schoolId)
        {
            List<Coupons> allCoupons = _couponRepo.GetAllCoupons(schoolId);
            List<CouponRedeem> allRedeems = _couponRepo.GetPatentsCoupons(parentId, schoolId);

            List<CouponsForParent> couponsForParent = new List<CouponsForParent>();
            foreach (Coupons coupon in allCoupons)
            {
                CouponsForParent c = new CouponsForParent();
                c.id = coupon.id;
                c.couponName = coupon.CouponName;
                c.couponPrice = coupon.couponPrice;
                c.expiryDate = coupon.expiryDate;
                c.status = coupon.status;

                if (!string.IsNullOrEmpty(coupon.parents))
                {
                    List<int> parentIds = coupon.parents.Split(',').Select(int.Parse).ToList();
                    if(parentIds.Contains(0) || parentIds.Contains(parentId))
                    {
                        var xi = allRedeems.Where(e => e.parentId == parentId && e.couponId == coupon.id).FirstOrDefault();
                        if (xi == null || xi.id == 0)
                        {
                            c.isUsed = false;
                            c.redeemDate = null;
                        }
                        else
                        {
                            c.isUsed = true;
                            c.redeemDate = xi.redeemDate;
                        }

                        couponsForParent.Add(c);
                    }
                }
            }

            return couponsForParent;
        }

        public RedeemResult RedeemCoupon(int couponId, int parentId, int schoolId)
        {
            /*
             1. Check if already used
             2. if not then add entry to redeem table 
             3. update the balance
             4. Add entry to transactions table
             */
            RedeemResult result = new RedeemResult();

            bool isParentUsedThisCoupon = _couponRepo.IsParentUsedThisCoupon(couponId, parentId, schoolId);
            if (isParentUsedThisCoupon)
            {
                result.isSuccess = false;
                result.message = "Coupon already used. You can not use a coupon twice.";
                return result;
            }

            Coupons coupon = _couponRepo.GetCoupon(couponId, schoolId);
            if (coupon == null)
            {
                //error
                result.isSuccess = false;
                result.message = "Coupon not found";
                return result;
            }

            CouponRedeem redeem = new CouponRedeem()
            {
                couponId = couponId,
                parentId = parentId,
                redeemDate = DateTime.Now,
                schoolId = schoolId
            };

            if (_couponRepo.RedeemCoupon(redeem))
            {
                //update balance
                _parentsRepo.AddParentCredit(parentId, schoolId, coupon.couponPrice.Value);
                Parent parent = _parentsRepo.GetParent(schoolId, parentId);

                Transaction transaction = new Transaction();
                transaction.parentId = parentId;
                transaction.schoolId = schoolId;
                transaction.transactionAmount = coupon.couponPrice;
                transaction.transactionType = "COUPON";
                transaction.extra1 = couponId.ToString();
                transaction.balanceBefore = parent.credit;
                transaction.balanceAfter =  parent.credit + coupon.couponPrice;
                transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                //transaction.studentId = order.studentId;
                //transaction.transactionId = transactionId;
                _transactionRepo.AddTransaction(transaction);

                result.isSuccess = true;
                result.message = "Coupon redeemed successfully. Amount has been credited to your account. ";
                return result;
            }

            result.message = "Something went wrong. Please contact admin.";
            return result;
        }
    }
}
