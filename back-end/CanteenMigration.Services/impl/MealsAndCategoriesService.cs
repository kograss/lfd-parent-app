﻿using CanteenMigration.Commons;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{



    public class MealsAndCategoriesService : IMealsAndCategoriesService
    {

        IMealsAndCategoriesRepository _mealsAndCategoriesRepo;
        ISchoolRepository _schoolRepo;
        IProductRepository _productRepo;
        ICategoriesRepository _categoriesRepo;
        ISchoolService _schoolService;
        public MealsAndCategoriesService(IMealsAndCategoriesRepository mealsAndCategoriesRepo, ISchoolRepository _schoolRepo, 
                                            IProductRepository _productRepo, ICategoriesRepository _categoriesRepo,
                                            ISchoolService schoolService)
        {
            this._mealsAndCategoriesRepo = mealsAndCategoriesRepo;
            this._schoolRepo = _schoolRepo;
            this._productRepo = _productRepo;
            this._categoriesRepo = _categoriesRepo;
            _schoolService = schoolService;
        }

        public List<Meal> GetMealsAndCategories(int schoolId, DateTime deliveryDate)
        {
            try
            {
                List<Meal> meals = new List<Meal>();


                string mealCode = "R";
                var recessCategories = _mealsAndCategoriesRepo.GetCategoriesFromDB(schoolId, mealCode);
                if (recessCategories != null && recessCategories.Count > 0)
                {
                    meals.Add(new Meal()
                    {
                        mealCode = mealCode,
                        categories = recessCategories,
                        name = "RECESS"
                    });
                }

                mealCode = "L";
                var lunchCategories = _mealsAndCategoriesRepo.GetCategoriesFromDB(schoolId, mealCode);
                if (lunchCategories != null && lunchCategories.Count > 0)
                {
                    meals.Add(new Meal()
                    {
                        mealCode = mealCode,
                        categories = lunchCategories,
                        name = "LUNCH"
                    });
                }

                

                mealCode = "T";
                var thirdBreakCategories = _mealsAndCategoriesRepo.GetCategoriesFromDB(schoolId, mealCode);
                if(thirdBreakCategories != null && thirdBreakCategories.Count > 0)
                {
                    meals.Add(new Meal()
                    {
                        mealCode = "T",
                        categories = thirdBreakCategories,
                        name = "Third Break"
                    });
                }

                //string mealsAndCategoriesJSON = _mealsAndCategoriesRepo.GetMealsAndCategories(schoolId, deliveryDate);
                //if (string.IsNullOrEmpty(mealsAndCategoriesJSON))
                // {
                //   return null;
                // }

                //List<Meal> meals = JsonConvert.DeserializeObject<List<Meal>>(mealsAndCategoriesJSON);

                /*SchoolInfo schoolInfo = new SchoolInfo();
                string schoolInfoJson = _schoolRepo.GetSchoolInfo(schoolId);
                if (string.IsNullOrEmpty(schoolInfoJson))
                {
                    return null;
                }

                schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJson);
                */

                SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
                if (!schoolInfo.canteenopen.HasValue|| !schoolInfo.canteenopen.Value)
                {
                    return null;
                }

                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
                List<Meal> mealsWithRemovedCategories = new List<Meal>();

                foreach (Meal meal in meals)//checks: iterate each category: 1. Check cutoff 2. check if products are present
                {
                    if (IsThisMealOpenForOrdering(meal.mealCode, schoolInfo, deliveryDate))
                    {
                        Meal newMeal = new Meal();
                        newMeal.name = getMealTypeByMealCode(meal.mealCode, schoolInfo);
                        newMeal.mealCode = meal.mealCode;

                        List<Category> lst = new List<Category>();
                        foreach (Category category in meal.categories)
                        {
                            var products = _productRepo.GetCanteenProductsFromDB(schoolId, meal.mealCode, category.categoryID);
                            if (products.Count > 0  && isCategoryAvailableForOrdering(category, deliveryDate, schoolId, nowInSchoolTimezone, newMeal.name, schoolInfo))
                                //&& IsCategoryShouldBeVisible(category, nowInSchoolTimezone, deliveryDate)) //Category Cuttoff
                            {
                                lst.Add(category);
                            }
                        }
                        newMeal.categories = lst;
                        mealsWithRemovedCategories.Add(newMeal);
                    }
                }

                return mealsWithRemovedCategories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsThisMealOpenForOrdering(string mealCode, SchoolInfo schoolInfo, DateTime deliveryDate)
        {
            string service = "LUNCH";
            if (mealCode == "R")
            {
                service = "RECESS";
                if ((!schoolInfo.isRecessOpen.HasValue) || schoolInfo.isRecessOpen.Value == false)
                    return false;
            }

            if (mealCode == "T")
            {
                service = "THIRD";
                if (string.IsNullOrEmpty(schoolInfo.meal3))
                    return false;
            }

            if (!_schoolRepo.IsMealAvailableForDate(schoolInfo.schoolid, deliveryDate, service)) //TODO: Take info from cache
            {
                return false;
            }

            return true;
        }
        private string getMealTypeByMealCode(string mealCode, SchoolInfo schoolInfo)
        {
            string mealType = string.Empty;
            switch (mealCode)
            {
                case "L":
                    mealType = schoolInfo.meal2; //"LUNCH";
                    break;
                case "R":
                    mealType = schoolInfo.meal1; //"RECESS";
                    break;
                case "T":
                    mealType = schoolInfo.meal3; //"THIRD";
                    break;
                default:
                    mealType = "LUNCH";
                    break;
            }
            return mealType;
        }

        //category cutoff time
        private bool isCategoryAvailableForOrdering(Category category, DateTime orderDate, int schoolId, DateTime nowInSchoolTimezone, string mealName, SchoolInfo schoolInfo)
        {
            //check if category is not available for this date in category_unavailable_4date table
            /*if (_categoriesRepo.IsCategoryUnavailableThisday(orderDate, category.categoryID, mealName, schoolId) == true)
            {
                return false;
            }*/

            /*if (IsCategoryHidden(category) == false)
            {
                return true;
            }*/

            //Check Category Cut Off..
            /*if (_categoriesRepo.IsCategoryCutOff(category.categoryID, nowInSchoolTimezone, orderDate) == true)
            {
                return false;
            }*/

            //Check Daily Cut Off..
            if (isWithinCutOffTime(category, nowInSchoolTimezone, orderDate, schoolInfo) == true)
            {
                return false;
            }

            return true;
        }

        private bool IsCategoryShouldBeVisible(Category category, DateTime nowInSchoolTimezone, DateTime orderDate)//if this returns true - show category
        {
            if (category.hd == null || category.sd == null)
            {
                return true;
            }

            DateTime d = orderDate;
            int diff = d.DayOfWeek - DayOfWeek.Sunday;
            DateTime lastSunday = d.AddDays(-(diff + 1));
            lastSunday = lastSunday.Date;
            lastSunday = lastSunday.AddDays(category.sd.Value);
            lastSunday = lastSunday.AddHours(category.sh.Value);
            lastSunday = lastSunday.AddMinutes(category.sm.Value);

            DateTime d2 = orderDate;
            int diff2 = d2.DayOfWeek - DayOfWeek.Sunday;
            DateTime lastSunday2 = d2.AddDays(-(diff2 + 1));
            lastSunday2 = lastSunday2.Date;
            lastSunday2 = lastSunday2.AddDays(category.hd.Value);
            lastSunday2 = lastSunday2.AddHours(category.hd.Value);
            lastSunday2 = lastSunday2.AddMinutes(category.hm.Value);

            DateTime orderDateTime = new DateTime(year: orderDate.Year,
                    month: orderDate.Month,
                    day: orderDate.Day,
                    hour: nowInSchoolTimezone.Hour,
                    minute: nowInSchoolTimezone.Minute,
                    second: nowInSchoolTimezone.Second);

            if (nowInSchoolTimezone >= lastSunday && nowInSchoolTimezone <= lastSunday2)
            {
                return false;
            }

            return false;
        }

        private bool IsCategoryHidden(Category category)
        {
            if (category.isHide == null || category.isHide.Value == false)
            {
                return false;
            }
            return true;
        }

        private bool isWithinCutOffTime(Category category, DateTime nowInSchoolTimezone, DateTime orderDate, SchoolInfo schoolInfo)
        {
            if (category.daily_hour == null ) //|| category.CutOffType == null) - Not sure if this has to be done
            {
                category.daily_hour = schoolInfo.daily_hour;
                category.daily_min = schoolInfo.daily_min;
            }

            var cutOff = orderDate.AddDays(category.cut_days_before == null ? 0 : -category.cut_days_before.Value);
            cutOff = new DateTime(cutOff.Year,
                                  cutOff.Month,
                                  cutOff.Day,
                                  category.daily_hour == null ? 0 : category.daily_hour.Value,
                                  category.daily_min == null ? 0 : category.daily_min.Value,
                                  0);

            return nowInSchoolTimezone > cutOff;
        }
    }
}
