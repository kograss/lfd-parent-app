﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using DataDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Commons;
using CanteenMigration.Services.Commons;
using SchoolEvents.Services;

namespace CanteenMigration.Services.impl
{
    public class FundRaisingEventService : IFundRaisingEventService
    {
        IFundRaisingEventRepository repo;
        private ITransactionRepository _transactionRepo;
        private IEventService _eventService;
        public FundRaisingEventService(IFundRaisingEventRepository repo, ITransactionRepository _transactionRepo, IEventService _eventService)
        {
            this.repo = repo;
            this._transactionRepo = _transactionRepo;
            this._eventService = _eventService;
        }
        public List<FundRaisingEventOrders> AddDonation(int parentId, int schoolId,  int eventId, List<StripeDonationRequest.StudentAndAmount> students, string status)
        {
            var schoolEvent = _eventService.GetSpecialEvent(schoolId, eventId);

            List<FundRaisingEventOrders> lst = new List<FundRaisingEventOrders>();
            foreach (StripeDonationRequest.StudentAndAmount student in students)
            {
                FundRaisingEventOrders d = new FundRaisingEventOrders()
                {
                    amount = student.amount,
                    parentId = parentId,
                    schoolId = schoolId,
                    studentId = student.studentId,
                    status = status,
                    orderDateTime = DateTime.Now,
                    orderShippedDate = schoolEvent.EventDate,
                    EventId = eventId
                };
                lst.Add(repo.AddDonation(d));
            }

            return lst;
        }

        public bool Update(List<FundRaisingEventOrders> lst, string paymentGatewayTransactionId, string stripeTransactionId, string status)
        {
            foreach (FundRaisingEventOrders order in lst)
            {
                repo.Update(order.id, paymentGatewayTransactionId, stripeTransactionId, status);
            }
            return true;
        }

        public bool AddTransaction(List<FundRaisingEventOrders> lst, int parentId, int schoolId, decimal credit, string transactionId, bool isCreditOrder)
        {
            foreach (FundRaisingEventOrders order in lst)
            {
                //Create record in Transactions
                Transaction transaction = new Transaction();
                transaction.parentId = parentId;
                transaction.schoolId = schoolId;
                transaction.transactionAmount = order.amount;

                transaction.transactionType = isCreditOrder ? "ORDER:DEBIT" : "PayAtCheckout";
                transaction.balanceAfter = credit;

                transaction.extra1 = order.id.ToString();
                transaction.balanceBefore = credit;

                transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                transaction.studentId = order.studentId;
                transaction.transactionId = transactionId;
                _transactionRepo.AddTransaction(transaction);
            }
            return true;
        }
    }
}
