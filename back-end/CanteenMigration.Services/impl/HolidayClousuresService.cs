﻿using DataDependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using Newtonsoft.Json;
using CanteenMigration.Commons;

namespace CanteenMigration.Services.impl
{
    public class HolidayClousuresService: IHolidayClousuresService
    {

        IPublicHolidaysRepository _publicHolidaysRepo;
        IClosuresRepository _clousuresRepository;
        ISchoolRepository _schoolRepository;
        ISchoolService _schoolService;
        IMealsAndCategoriesService _categorieService;

        public HolidayClousuresService(IPublicHolidaysRepository publicHolidaysRepo, IClosuresRepository clousuresRepository,
                                        ISchoolRepository _schoolRepository, ISchoolService schoolService, IMealsAndCategoriesService categorieService)
        {
            this._publicHolidaysRepo = publicHolidaysRepo;
            this._clousuresRepository = clousuresRepository;
            this._schoolRepository = _schoolRepository;
            _schoolService = schoolService;
            this._categorieService = categorieService;
        }

        public HolidayClousures GetHolidayClosures(int schoolId, int year)
        {
            HolidayClousures holidayClousures = new HolidayClousures();
            

            holidayClousures.weeklyClosures = getWeeklyClosures(schoolId);
            holidayClousures.holidayDateRange = getHolidays(schoolId, year);
            holidayClousures.maxDate = new DateTime(year, 12, 31);
            holidayClousures.cutOffDays = getCutoffDates(schoolId);
            return holidayClousures;
        }


        public int[] getWeeklyClosures(int schoolId)
        {
            Closures closures = _clousuresRepository.GetClousures(schoolId);
            string weeklyClousures = closures.closeday;
            return !String.IsNullOrEmpty(weeklyClousures) ? Array.ConvertAll(weeklyClousures.Split('|'), int.Parse) : null;
        }

        public List<HolidayRange> getHolidays(int schoolId, int year)
        {
            List<PublicHolidays> holidays = _publicHolidaysRepo.GetPublicHolidays(schoolId, year);

            List<HolidayRange> holidayDateRange = new List<HolidayRange>();
            foreach (PublicHolidays holiday in holidays)
            {
                HolidayRange hr = new HolidayRange();
                hr.startDate = holiday.holidayDateStart.Value;
                hr.endDate = holiday.holidayDateEnd.Value;

                holidayDateRange.Add(hr);
            }

            return holidayDateRange;
        }

        public List<DateTime> getCutoffDates(int schoolId)
        {
            /*SchoolInfo schoolInfo = new SchoolInfo();

            string schoolInfoJson = _schoolRepository.GetSchoolInfo(schoolId);
            if (!string.IsNullOrEmpty(schoolInfoJson)) //TODO: ELSE - ERROR
            {
                schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJson);
            }
            */

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            TimezoneSettings zoneSettings = new TimezoneSettings();
            DateTime currentDateTimeInSchoolTimeZone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
      

            List<DateTime> cutoffDates = new List<DateTime>();
            DateTime addedDateTime;
            DateTime dateTimeNow = currentDateTimeInSchoolTimeZone;

            addedDateTime = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value);
            
            //if (!schoolInfo.daily_hour.Equals(null))
            //{
            //    TimeSpan timeSpan = new TimeSpan(schoolInfo.daily_hour.Value, schoolInfo.daily_min.Value, 60);
            //    long mi = addedDateTime.Date.Ticks + timeSpan.Ticks;
            //    long addedDateTime1 = addedDateTime.Ticks - mi;
            //    if (addedDateTime1 < 0)
            //    {
            //        for (var dt = dateTimeNow; dt < addedDateTime; dt = dt.AddDays(1))
            //        {
            //            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
            //        }
            //    }
            //    else
            //    {
            //        addedDateTime = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value);
            //        for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
            //        {
            //            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
            //        }
            //    }
            //}
            //else
            //{
            //    DateTime dateTime1 = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value + 1);
            //    long addedDateTime1 = addedDateTime.Ticks - dateTime1.Date.Ticks;
            //    if (addedDateTime1 < 0)
            //    {
            //        for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
            //        {
            //            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
            //        }
            //    }
            //    else
            //    {
            //        addedDateTime = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value - 1);
            //        for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
            //        {
            //            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
            //        }
            //    }
            //}
            if (schoolInfo.cut_days_before != 0)
            {
                DateTime dateTime1 = currentDateTimeInSchoolTimeZone.Date.AddDays(schoolInfo.cut_days_before.Value + 1);
                long addedDateTime1 = addedDateTime.Ticks - dateTime1.Date.Ticks;
                if (addedDateTime1 < 0)
                {
                    for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
                    {
                        cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
                    }
                }
                else
                {
                    addedDateTime = currentDateTimeInSchoolTimeZone.Date.AddDays(schoolInfo.cut_days_before.Value - 1);
                    for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
                    {
                        cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
                    }
                }
                
            }
            else
            {
                bool canOrder = false;
                List<Meal> meals = this._categorieService.GetMealsAndCategories(schoolId, currentDateTimeInSchoolTimeZone);
                foreach(Meal meal in meals)
                {
                    if(meal.categories.Count() > 0)
                    {
                        canOrder = true;
                    }
                }

                if(!canOrder){
                    DateTime dateTime1 = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value + 1);
                    long addedDateTime1 = addedDateTime.Ticks - dateTime1.Date.Ticks;
                    if (addedDateTime1 < 0)
                    {
                        for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
                        {
                            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
                        }
                    }
                    else
                    {
                        addedDateTime = schoolInfo.cut_days_before == null ? currentDateTimeInSchoolTimeZone : currentDateTimeInSchoolTimeZone.AddDays(schoolInfo.cut_days_before.Value - 1);
                        for (var dt = dateTimeNow; dt <= addedDateTime; dt = dt.AddDays(1))
                        {
                            cutoffDates.Add(DateTimeUtils.GetWithoutTime(dt.Date));
                        }
                    }
                }
                
                
            }


            return cutoffDates;
        }
    }
}
