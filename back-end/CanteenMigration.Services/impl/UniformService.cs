﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;
using CanteenMigration.Commons;

namespace CanteenMigration.Services.impl
{
    
    public class UniformService : IUniformService
    {
        public const int STAGE_UNDER_INVESTIAGTION = 3;
        public const int STAGE_PAYMENT_SUCCESSFULL = 0;
        public const int STAGE_AWAITING_PAYMENT_CLEARANCE = 8;
        public const int STAGE_AWAITING_CC_PAYMENT_CLEARANCE = 9;

        public const int PAYMENT_STATUS_NOT_DONE = 0;
        public const int PAYMENT_STATUS_DONE = 1;
        public const int AWAITING_CC_PAYMENT = 9;

        public const String PAYMENT_STRIPE = "stripe";
        public const String TRANSACTION_TYPE_UNIFORM_ORDER = "UNIFORM_ORDER";

        public const decimal UNIFORM_SCHOOL24_FEE = 0.25M;
        public const decimal BANK_FEE_PERCENTAGE = 0.0155M;
        public const decimal EXTRA_FEE = 0.0155M;


        IUniformRepository uniformRepository;
        ITransactionLogRepository _transactionLogRepository;
        private ITransactionService _transactionService;
        ITransactionRepository _transactionRepo;
        public UniformService(IUniformRepository uniformRepository, ITransactionLogRepository _transactionLogRepository,
                              ITransactionService _transactionService, ITransactionRepository _transactionRepo)
        {
            this.uniformRepository = uniformRepository;
            this._transactionLogRepository = _transactionLogRepository;
            this._transactionService = _transactionService;
            this._transactionRepo = _transactionRepo;
        }

        public List<UniformNews> GetUniformNews(int schoolId, int year)
        {
            return uniformRepository.GetUniformNews(schoolId, year);
        }

        public List<UniformCategories> GetUniformCategories(int schoolId)
        {
            var catData = uniformRepository.GetUniformCategories(schoolId);
            /*if (!string.IsNullOrEmpty(catData))
            {
                List<UniformCategories> categories = JsonConvert.DeserializeObject<List<UniformCategories>>(catData);
                return categories;
            }*/

            return catData;
            //return uniformRepository.GetUniformCategories(schoolId);
        }


        public List<UniformProduct> GetUniformProductsByCategory(int schoolId, int categoryId)
        {

            List<UniformProduct> products = uniformRepository.GetUniformProductsByCategory(schoolId, categoryId);
            CanteenMigration.Models.UniformSetup setup = uniformRepository.GetUniformSetup(schoolId);
            foreach (UniformProduct product in products)
            {
                if(product.isUniformPack == false)
                {
                    product.stock = uniformRepository.GetProductStock(schoolId, product.productId);
                }
                
                product.options = uniformRepository.GetProductOptionsFromDB(schoolId, product.productId);
                if(setup!=null)
                {
                    product.TopupMethod = setup.topupmethod;
                    product.Bank = setup.cbank;
                    product.BankAccount = setup.cbankaccount;
                    product.BankBSB = setup.cbankbsb;
                    product.AccountEmail = setup.cacctemail;
                    product.Shop = setup.shop;
                    product.isStudentOptional = setup.isStudentOptional;
                    product.isExternalRecipientRequired = setup.isExternalRecipientRequired;
                }
                //string optionsData = uniformRepository.GetProductOptions(schoolId, product.productId);
                //if (!string.IsNullOrEmpty(optionsData))
                //{
                //    product.options = JsonConvert.DeserializeObject<List<ProductOption>>(optionsData);
                //}
            }
            return products;
        }


        public List<UniformPack> GetUniformPacksByCategory(int schoolId, int categoryId)
        {
            List<UniformPack> uniformPacks = uniformRepository.GetUniformPacksByCategory(schoolId, categoryId);
            CanteenMigration.Models.UniformSetup setup = uniformRepository.GetUniformSetup(schoolId);
            foreach (UniformPack uniformPack in uniformPacks)
            {
                uniformPack.products = uniformRepository.GetUniformPackProducts(schoolId, uniformPack.productId);
                if (setup != null)
                {
                    uniformPack.TopupMethod = setup.topupmethod;
                    uniformPack.Bank = setup.cbank;
                    uniformPack.BankAccount = setup.cbankaccount;
                    uniformPack.BankBSB = setup.cbankbsb;
                    uniformPack.AccountEmail = setup.cacctemail;
                    uniformPack.Shop = setup.shop;
                    uniformPack.isStudentOptional = setup.isStudentOptional;
                    uniformPack.isExternalRecipientRequired = setup.isExternalRecipientRequired;
                }
            }
            return uniformPacks;
        }

        public UniformCheckoutSchoolDetails getUniformSchoolDetails(int schoolId)
        {
            CanteenMigration.Models.UniformSetup setup = uniformRepository.GetUniformSetup(schoolId);
            UniformCheckoutSchoolDetails details = new UniformCheckoutSchoolDetails
            {
                TopupMethod = setup.topupmethod,
                Bank = setup.cbank,
                BankAccount = setup.cbankaccount,
                BankBSB = setup.cbankbsb,
                AccountEmail = setup.cacctemail,
                Shop = setup.shop,
                isStudentOptional = setup.isStudentOptional,
                isExternalRecipientRequired = setup.isExternalRecipientRequired,
                isPayUsingBalanceEnabled = setup.isPayUsingBalanceEnabled
            };

            return details;
        }

        public List<Models.UniformBasketItem> GetBasketItems(int basketId, int parentId, int schoolId)
        {
            return uniformRepository.GetBasketItems(basketId, parentId, schoolId);
        }

        public Models.UniformBasket GetBasket(int basketId, int parentId, int schoolId)
        {
            return uniformRepository.GetBasket(basketId, parentId, schoolId);
        }

        public int AddToBasket(int parentId, int schoolId, UniformCartItem item)
        {
            //check if already present
            item.uniformItem.schoolId = schoolId;
            item.uniformItem.parentId = parentId;

            if (item.uniformItem.basketId == 0 || item.uniformItem.basketId == null)
            {
                //create basket
                item.uniformItem.basketId = uniformRepository.CreateBasket(parentId, schoolId);
            }
            Models.UniformBasketItem oldBasketItem = uniformRepository.GetItemFromBasket(item.uniformItem);

            

            

            if (oldBasketItem == null)
            {
                item.uniformItem = uniformRepository.AddToBasket(parentId, schoolId, item.uniformItem);
                if (item.uniformItem.isUniformPack.GetValueOrDefault() != 0)
                {
                    uniformRepository.AddUniformPackItemProducts(item.uniformPackItemDetails, item.uniformItem.id);
                }
            }
            else
            {
                if (item.uniformItem.isUniformPack.GetValueOrDefault() == 0)
                {
                    uniformRepository.UpdateCartQuantity(oldBasketItem, item.uniformItem.quantity.Value);
                }
                else
                {
                    bool isSameOrderItem = true;
                    foreach (Models.UniformPackItemProduct productItem in item.uniformPackItemDetails)
                    {
                        productItem.itemId = oldBasketItem.id;
                        var uPackBasketOldSubItem = uniformRepository.GetUPackSubItemFromBasket(productItem);
                        if(uPackBasketOldSubItem == null)
                        {
                            isSameOrderItem = false;
                            break;
                        }
                    }
                    if (isSameOrderItem)
                    {
                        uniformRepository.UpdateCartQuantity(oldBasketItem, item.uniformItem.quantity.Value);
                    }
                    else
                    {
                        item.uniformItem = uniformRepository.AddToBasket(parentId, schoolId, item.uniformItem);
                        if (item.uniformItem.isUniformPack.GetValueOrDefault() != 0)
                        {
                            uniformRepository.AddUniformPackItemProducts(item.uniformPackItemDetails, item.uniformItem.id);
                        }
                    }

                }
                
            }

            return item.uniformItem.basketId.Value;
        }



        public bool UpdateBasket(int parentId, int schoolId, UpdateCartInput updateCart)
        {
            switch (updateCart.status)
            {
                case RemoveCartStatus.CART:
                    List<Models.UniformBasketItem> items = uniformRepository.GetBasketItems(updateCart.basketId, parentId, schoolId);
                    if (items == null || items.Count <= 0)
                    {
                        return false;
                    }
                    uniformRepository.EmptyCart(items);
                    return true;
                case RemoveCartStatus.PRODUCT:
                    Models.UniformBasketItem item = uniformRepository.GetBasketItem(parentId, schoolId, updateCart.basketItemId);
                    if (item == null || item.id == 0)
                    {
                        return false;
                    }
                    uniformRepository.RemoveItemFromBasket(item);
                    break;
                case RemoveCartStatus.QUANTITY:
                    Models.UniformBasketItem bItem = uniformRepository.GetBasketItem(parentId, schoolId, updateCart.basketItemId);
                    if (bItem == null || bItem.id == 0)
                    {
                        return false;
                    }
                    uniformRepository.ReduceBasketItemQuantity(bItem, 1);
                    break;
            }
            return true;
        }
        public int GetBasketCount(int basketId, int parentId, int schoolId)
        {
            return uniformRepository.GetBasketCount(basketId, parentId, schoolId);
        }

        public int? GetProductStock(StockInput input)
        {
            return uniformRepository.GetProductStock(input);
        }

        public Models.UniformBasket UpdateBasket(int basketId, int parentId, int schoolId, int studentId, string studentClass, string comment, string firstName)
        {
            Models.UniformBasket basket = uniformRepository.GetParentBasket(basketId, parentId, schoolId);
            if (basket == null)
            {
                return null;
            }

            if (studentId == 0)
            {
                uniformRepository.UpdateBasket(basket, firstName, studentClass, comment);
            }
            else
            {
                uniformRepository.UpdateBasket(basket, studentId.ToString(), studentClass, comment);
            }
            return basket;
        }

        public Models.UniformOrderData CreateOrder(int schoolId, int basketId, Models.Parent parent, string topupMethod, CollectionOption collectionOption)
        {
            Models.UniformOrderData uniformOrderData = new Models.UniformOrderData();
            uniformOrderData.BasketId = basketId;
            uniformOrderData.ShopperId = parent.id;
            uniformOrderData.OrderedDate = DateTimeUtils.GetWithoutTime();
            uniformOrderData.BillFirstName = parent.firstName;
            uniformOrderData.BillLastName = parent.lastName;
            uniformOrderData.BillEmail = parent.email;
            uniformOrderData.SchoolId = schoolId;
            if (collectionOption != null)
            {
                uniformOrderData.collectionOption = collectionOption.type;
                uniformOrderData.deliveryFee = collectionOption.fee;
                if (collectionOption.type.ToLower() == "home delivery")
                {
                    Models.UniformAddress currentAddress = this.GetUniformAddress(parent.id);
                    uniformOrderData.deliveryAddress = currentAddress.address;
                    uniformOrderData.deliverySuburb = currentAddress.suburb;
                    uniformOrderData.deliveryState = currentAddress.state;
                    uniformOrderData.deliveryPostCode = currentAddress.postCode;
                }
            }
            
            

            if (uniformRepository.AddUniformOrderData(uniformOrderData))
            {
                Models.UniformOrderStatus orderStatus = new Models.UniformOrderStatus();
                orderStatus.OrderId = uniformOrderData.OrderId;
                if (topupMethod.Equals("CC"))
                {
                    orderStatus.StageId = STAGE_AWAITING_CC_PAYMENT_CLEARANCE;
                }
                else if (topupMethod.Equals("M"))
                {
                    orderStatus.StageId = STAGE_AWAITING_PAYMENT_CLEARANCE;
                }
                orderStatus.SchooId = schoolId;

                uniformRepository.AddUniformOrderStatus(orderStatus);
                return uniformOrderData;
            }

            return null;
        }

        public Models.UniformOrderData GetUniformOrderData(int basketId, Models.UniformOrderPayment uniformOrderPayment)
        {
            Models.UniformOrderData uniformOrderData = new Models.UniformOrderData();
            uniformOrderData.BasketId = basketId;
            uniformOrderData.ShopperId = uniformOrderPayment.ParentId;
            uniformOrderData.OrderId = uniformOrderPayment.OrderId;

            return uniformRepository.GetUniformOrderData(uniformOrderData);

        }

        private Decimal calculateOrderAmount(int schoolId, List<Models.UniformBasketItem> basketItems)
        {
            return basketItems.Sum(e => (e.price * (decimal)e.quantity));
        }
        public Models.UniformOrderPayment CreatePayment(Models.UniformOrderData uniformOrderData, Models.UniformBasket basket, 
                                                        List<Models.UniformBasketItem> basketItems, CollectionOption collectionOption, bool isCreditOrder)
        {
            decimal deliveryFee = collectionOption != null ? collectionOption.fee : 0;
            decimal orderAmount = basketItems.Sum(e => (e.price * (decimal)e.quantity)) + deliveryFee;
            decimal grossAmount = orderAmount + UNIFORM_SCHOOL24_FEE;
            decimal bankFee = (grossAmount * BANK_FEE_PERCENTAGE) + EXTRA_FEE;
            grossAmount = grossAmount + bankFee;

            Models.UniformOrderPayment uniformOrderPayment = new Models.UniformOrderPayment();
            uniformOrderPayment.SchoolId = uniformOrderData.SchoolId;
            uniformOrderPayment.ParentId = uniformOrderData.ShopperId;
            uniformOrderPayment.TransactionDate = uniformOrderData.OrderedDate;
            uniformOrderPayment.TopupMethod = PAYMENT_STRIPE;
            uniformOrderPayment.OrderId = uniformOrderData.OrderId;

            
            //basket.CalcOrderTotal(Context);

            basket.SubTotal = orderAmount;
            basket.Total = orderAmount;
            uniformOrderPayment.PaymentStatus = PAYMENT_STATUS_NOT_DONE;

            if (isCreditOrder)
            {
                uniformOrderPayment.Extra2 = "Parent Credit";
                // Change this so that admin fee etc will also get added in total.
                uniformOrderPayment.OrderAmount = orderAmount;
                uniformOrderPayment.GrossAmount = orderAmount;
                //Change this to have actual bank fee
                uniformOrderPayment.BankFee = 0;
                // This is obsolete, still added to make it uniform with previous implementation.
                uniformOrderPayment.School24Fee = 0;
            }

            
            

            if (!isCreditOrder)
            {
                // Change this so that admin fee etc will also get added in total.
                uniformOrderPayment.OrderAmount = orderAmount;
                uniformOrderPayment.GrossAmount = grossAmount;
                //Change this to have actual bank fee
                uniformOrderPayment.BankFee = bankFee;
                // This is obsolete, still added to make it uniform with previous implementation.
                uniformOrderPayment.School24Fee = UNIFORM_SCHOOL24_FEE;
                uniformOrderPayment.TransactionId = "STRIPE" + "_UNI_" + Guid.NewGuid().ToString();
            }

            if (uniformRepository.AddUniformOrderPayment(uniformOrderPayment))
            {
                return uniformOrderPayment;
            }

            return null;
        }

        public Models.UniformOrderPayment GetUniformOrderPayment(Models.PaymentGatewayTransactionLog log)
        {
            
            return uniformRepository.GetUniformOrderPayment(log);
        }

        public Models.PaymentGatewayTransactionLog CreatePaymentGateayTransactionLog(Models.UniformOrderPayment uniformOrderPayment)
        {
            Models.PaymentGatewayTransactionLog log = new Models.PaymentGatewayTransactionLog();
            log.OrderAmount = uniformOrderPayment.OrderAmount;
            log.School24Fee = UNIFORM_SCHOOL24_FEE;
            log.BankFee = uniformOrderPayment.BankFee;
            log.GrossAmount = uniformOrderPayment.GrossAmount;

            log.Id = uniformOrderPayment.TransactionId;
            log.Status = Models.PaymentGatewayTransactionLog.TRANSACTION_STATE_START;
            log.Log = "[Transaction Start]";
            log.TransactionDate = DateTime.Now;
            log.ParentId = uniformOrderPayment.ParentId;
            log.SchoolId = uniformOrderPayment.SchoolId.Value;
            log.Type = TRANSACTION_TYPE_UNIFORM_ORDER;

            _transactionLogRepository.AddTransactionLog(log);

            return log;
        }

        public bool MakeOrderSuccess(Models.UniformOrderPayment uniformOrderPayment,
                                     string paymentGatewayTransactionId, Models.PaymentGatewayTransactionLog log, int schoolId)
        {
            uniformOrderPayment.Extra2 = paymentGatewayTransactionId;
            uniformOrderPayment.PaymentStatus = STAGE_PAYMENT_SUCCESSFULL;

            Models.UniformOrderStatus orderStatus = uniformRepository.GetOrderStatusByOrderId(uniformOrderPayment.OrderId.Value);
            orderStatus.StageId = STAGE_PAYMENT_SUCCESSFULL;

            _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentGatewayTransactionId + "]", log, schoolId, paymentGatewayTransactionId);



            return true;
        }


        public bool MakeOrderSuccess(Models.UniformOrderPayment uniformOrderPayment,
                                     int schoolId, Models.Parent parent)
        {
            uniformOrderPayment.PaymentStatus = STAGE_PAYMENT_SUCCESSFULL;

            Models.UniformOrderStatus orderStatus = uniformRepository.GetOrderStatusByOrderId(uniformOrderPayment.OrderId.Value);
            orderStatus.StageId = STAGE_PAYMENT_SUCCESSFULL;

            //Create record in Transactions
            Models.Transaction transaction = new Models.Transaction();
            transaction.parentId = parent.id;
            transaction.schoolId = schoolId;
            transaction.transactionAmount = uniformOrderPayment.OrderAmount;
            transaction.transactionType = "ORDER:DEBIT:UNIFORM";
            transaction.extra1 = uniformOrderPayment.OrderId.ToString();
            transaction.balanceBefore = parent.credit + uniformOrderPayment.OrderAmount;
            transaction.balanceAfter = parent.credit;
            transaction.transactionDate = DateTimeUtils.GetWithoutTime();
            _transactionRepo.AddTransaction(transaction);


            return true;
        }

        public UniformStockCheckResult CheckStock(int schoolId, List<Models.UniformBasketItem> basketItems)
        {
            bool arePrductsOutOfStock = false;
            string checkStockMessage = "";

            Models.UniformAttribute UniformColorAttribute = new Models.UniformAttribute();
            Models.UniformAttribute UniformSizeAttribute = new Models.UniformAttribute();

            foreach (Models.UniformBasketItem eachItem in basketItems)
            {
                Models.UniformProduct uniformProduct = uniformRepository.GetUniformProduct((int)eachItem.productId);

                if (String.IsNullOrEmpty(eachItem.color) && String.IsNullOrEmpty(eachItem.size))
                {
                    
                    if (uniformProduct != null && uniformProduct.ProductStock != null)
                    {
                        int qty = 0;
                        qty = uniformProduct.ProductStock.Value - eachItem.quantity.Value;
                        if(qty < 0)
                        {
                            arePrductsOutOfStock = true;
                            checkStockMessage = checkStockMessage + uniformProduct.Name 
                                                + " went out of stock. Available quantity is: " 
                                                + uniformProduct.ProductStock + ". ";


                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(eachItem.color))
                    {
                        UniformColorAttribute = uniformRepository.GetUniformAttributeByName(eachItem.color);
                    }
                    if (!String.IsNullOrEmpty(eachItem.size))
                    {
                        UniformSizeAttribute = uniformRepository.GetUniformAttributeByName(eachItem.size);
                    }

                    Models.UniformStocks stock = uniformRepository.GetUniformStock(schoolId, eachItem.productId.Value, UniformColorAttribute.Id, UniformSizeAttribute.Id);

                    if (stock != null && stock.AvailableCount != null)
                    {
                        int qty = 0;
                        qty = stock.AvailableCount.Value - eachItem.quantity.Value;
                        if (qty < 0)
                        {
                            arePrductsOutOfStock = true;
                            checkStockMessage = checkStockMessage + uniformProduct.Name
                                                + " went out of stock, available quantity is: "
                                                + stock.AvailableCount.Value + ". ";


                        }
                    }
                }
                
            }

            UniformStockCheckResult result = new UniformStockCheckResult();
            result.arePrductsOutOfStock = arePrductsOutOfStock;
            result.checkStockMessage = checkStockMessage;
            return result;
        }

        public void UpdateStock(int schoolId, Models.UniformOrderData uniformOrderData, List<Models.UniformBasketItem> basketItems)//TODO: handle exceptions
        {
            Models.UniformAttribute UniformColorAttribute = new Models.UniformAttribute();
            Models.UniformAttribute UniformSizeAttribute = new Models.UniformAttribute();

            foreach (Models.UniformBasketItem eachItem in basketItems)
            {
                if(eachItem.isUniformPack.GetValueOrDefault() != 0)
                {
                    List<Models.UniformPackItemProduct> subItems = uniformRepository.GetAllUPackSubItem(eachItem.id);
                    foreach(Models.UniformPackItemProduct item in subItems)
                    {
                        if (String.IsNullOrEmpty(item.color) && String.IsNullOrEmpty(item.size))
                        {
                            Models.UniformProduct uniformProduct = uniformRepository.GetUniformProduct((int)item.productId);

                            if (uniformProduct != null && uniformProduct.ProductStock != null)
                            {
                                uniformProduct.ProductStock = uniformProduct.ProductStock - eachItem.quantity;
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(item.color))
                            {
                                UniformColorAttribute = uniformRepository.GetUniformAttributeByName(item.color);
                            }
                            if (!String.IsNullOrEmpty(item.size))
                            {
                                UniformSizeAttribute = uniformRepository.GetUniformAttributeByName(item.size);
                            }

                            Models.UniformStocks stock = uniformRepository.GetUniformStock(schoolId, item.productId.Value, UniformColorAttribute.Id, UniformSizeAttribute.Id);

                            if (stock != null && stock.AvailableCount != null)
                            {
                                stock.AvailableCount = stock.AvailableCount - eachItem.quantity;
                            }
                        }
                    }
                    Models.UniformPack uniformPack = uniformRepository.GetUniformPack((int)eachItem.productId);
                    if(uniformPack.UniformPackStock != null && uniformPack.UniformPackStock > 0)
                    {
                        uniformPack.UniformPackStock = uniformPack.UniformPackStock - eachItem.quantity;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(eachItem.color) && String.IsNullOrEmpty(eachItem.size))
                    {
                        Models.UniformProduct uniformProduct = uniformRepository.GetUniformProduct((int)eachItem.productId);

                        if (uniformProduct != null && uniformProduct.ProductStock != null)
                        {
                            uniformProduct.ProductStock = uniformProduct.ProductStock - eachItem.quantity;
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(eachItem.color))
                        {
                            UniformColorAttribute = uniformRepository.GetUniformAttributeByName(eachItem.color);
                        }
                        if (!String.IsNullOrEmpty(eachItem.size))
                        {
                            UniformSizeAttribute = uniformRepository.GetUniformAttributeByName(eachItem.size);
                        }

                        Models.UniformStocks stock = uniformRepository.GetUniformStock(schoolId, eachItem.productId.Value, UniformColorAttribute.Id, UniformSizeAttribute.Id);

                        if (stock != null && stock.AvailableCount != null)
                        {
                            stock.AvailableCount = stock.AvailableCount - eachItem.quantity;
                        }
                    }
                }
                
                //save
                uniformRepository.SaveContext();
            }
        }
        public List<UniformOrder> GetParentOrders(int parentId, int schoolId, int year)
        {
            return uniformRepository.GetParentOrders(parentId, schoolId, year);
        }

        public List<OrderDetails> GetUniformOrderDetails(int schoolId, int parentId, int orderId)
        {
            return uniformRepository.GetUniformOrderDetails(schoolId, parentId, orderId);
        }

        public List<JSONModels.UniformPackProduct> GetUniformPackProducts(int schoolId, int uniformPackId)
        {
            return uniformRepository.GetUniformPackProducts(schoolId, uniformPackId);
        }

        public List<Models.UniformPackItemProduct> GetAllUPackSubItem(int id)
        {
            return uniformRepository.GetAllUPackSubItem(id);
        }

        public List<CollectionOption> GetCollectionOptions(int schoolId)
        {
            Models.UniformSetup uniformSetup = uniformRepository.GetUniformSetup(schoolId);
            List<CollectionOption> collectionOptions = new List<CollectionOption>();
            if (uniformSetup.collectFromShop != null && uniformSetup.collectFromShop.Value == true)
            {
                CollectionOption collectionOption = new CollectionOption();
                collectionOption.id = 1;
                collectionOption.type = "Collect from Uniform Shop";
                collectionOption.fee = 0;
                collectionOptions.Add(collectionOption);
            }

            if (uniformSetup.classDelivery != null && uniformSetup.classDelivery.Value == true)
            {
                CollectionOption collectionOption = new CollectionOption();
                collectionOption.id = 2;
                collectionOption.type = "Deliver to class";
                collectionOption.fee = uniformSetup.classDeliveryFee.Value;
                collectionOptions.Add(collectionOption);
            }
            if (uniformSetup.homeDelivery != null && uniformSetup.homeDelivery.Value == true)
            {
                CollectionOption collectionOption = new CollectionOption();
                collectionOption.id = 3;
                collectionOption.type = "Home delivery";
                collectionOption.fee = uniformSetup.homeDeliveryFee.Value;
                collectionOptions.Add(collectionOption);
            }
            return collectionOptions;

        }

        public Models.UniformAddress GetUniformAddress(int parentId)
        {
            return uniformRepository.GetUniformAddress(parentId);
        }

        public Models.UniformAddress AddUniformAddress(Models.UniformAddress uniformAddress)
        {
            uniformRepository.AddUniformAddress(uniformAddress);
            return uniformAddress;
        }

        public Models.UniformAddress UpdateUniformAddress(Models.UniformAddress uniformAddress)
        {
            Models.UniformAddress newUniformAddress = uniformRepository.GetUniformAddress(uniformAddress.parentId);
            newUniformAddress.address = uniformAddress.address;
            newUniformAddress.suburb = uniformAddress.suburb;
            newUniformAddress.state = uniformAddress.state;
            newUniformAddress.postCode = uniformAddress.postCode;
            uniformRepository.SaveContext();
            return newUniformAddress;
        }

    }
}
