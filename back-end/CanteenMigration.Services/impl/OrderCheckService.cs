﻿using CanteenMigration.Commons;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services.exception;
using DataDependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class OrderCheckService : IOrderCheckService
    {
        ISchoolRepository _schoolRepo;
        IClosuresRepository _closureRepo;
        IPublicHolidaysRepository _holidayRepo;
        IProductRepository _productRepo;
        IOptionRepository _optionRepo;
        ICategoriesRepository _categoryRepo;
        IMealsAndCategoriesService categoryService;

       
        public const string ONLINEORDERINGCLOSED = "Online ordering is closed";
        public const string WRONGDELIVERYDATE = "Incorrect Delivery Date";
        public const string CANTEENCLOSEDON = "Canteen Closed On ";
        public const string ORDERTOOLATE = "Too late to make an order";
        public const string CATEGORYORDERTOOLATE = "Too late to make an order. Category Cut off exceeded ";
        public const string CANTEENCLOSEDFORMEAL = "Canteen closed for ";
        public const string PRODUCTNOTAVAILABLEFORMEAL = "Product {0} not available for: ";
        public const string PRODUCTNOTAVAILABLEFORDAYOFWEEK = "Product {0} not available for the delivery date "  ;
        public const string PRODUCTPRICECHANGED = "Price have changed for Product " ;
        public const string PRICESFOROPTIONCHANGED = "Price of options have changed for Product ";
        public const string OPTIONSNOTAVAILABLEFORPRODUCT = "Some of Options are not available for Product ";
        public const string PRODUCTSNOTAVAILABLE = "Product/s not available ";
        public const string INVALIDPARENTPLAN = "Invalid Parent Plan ";
        public const string CREDITUNAVAILABLE = "Order price is greater than available credit";
        public const string LUNCH = "LUNCH";
        public const string RECESS = "RECESS";
        public const string THIRD = "THIRD";

        public OrderCheckService(ISchoolRepository schoolRepo, IClosuresRepository _closureRepo,
                                 IPublicHolidaysRepository _holidayRepo, IProductRepository _productRepo, IOptionRepository _optionRepository,
                                 ICategoriesRepository _categoryRepo, IMealsAndCategoriesService categoryService)
        {
            this._schoolRepo = schoolRepo;
            this._closureRepo = _closureRepo;
            this._holidayRepo = _holidayRepo;
            this._productRepo = _productRepo;
            this._optionRepo = _optionRepository;
            this._categoryRepo = _categoryRepo;
            this.categoryService = categoryService;
        }

        //schoolId, parentId, deliveryDate, cartItems or ordered items
        public bool CheckIfOrderCanBePlace(SchoolInfo schoolInfo, Parent parent, DateTime deliveryDate,
           List<CanteenShopcartItem> cartItems, decimal totalOrderAmount, bool isCreditPayment,
           bool isPayAtPickUpOrder, bool isCutOffCheckRequired)
        {

            if (!schoolInfo.canteenopen.Value)
            {
                throw new PlaceOrderException(ONLINEORDERINGCLOSED);

            }

            if (parent.parentFeePlan == 0 || parent.parentFeePlan == null)
            {
                throw new PlaceOrderException(INVALIDPARENTPLAN);
            }

            if (string.IsNullOrEmpty(deliveryDate.ToString()) || deliveryDate.Year < DateTime.Now.Year || deliveryDate.Month < DateTime.Now.Month )
            {
                throw new PlaceOrderException(WRONGDELIVERYDATE);
            }

            if (isCreditPayment && totalOrderAmount > parent.credit && !isPayAtPickUpOrder)
            {
                throw new PlaceOrderException(CREDITUNAVAILABLE);
            }

            //1. clousures
            int dayOfWeekAsPerASP = (int)deliveryDate.DayOfWeek + 1;
            Closures closures = _closureRepo.GetClousures(schoolInfo.schoolid);
            string weeklyClousures = closures.closeday;

            if (!String.IsNullOrEmpty(weeklyClousures))
            {
                int[] closeDays = Array.ConvertAll(weeklyClousures.Split('|'), int.Parse);
                if (closeDays.Contains(dayOfWeekAsPerASP) && closures != null)
                {
                    throw new PlaceOrderException(CANTEENCLOSEDON + deliveryDate.DayOfWeek);                    
                }
            }

            //2. is holiday
            if (_holidayRepo.IsDateComingIntoHolidays(schoolInfo.schoolid, deliveryDate))
            {
                throw new PlaceOrderException(CANTEENCLOSEDFORMEAL + deliveryDate.Date);
               
            }

          /*  3. Previous date
            if (deliveryDate < DateTime.Now)
            {
                return "Wrong  Date";
            }*/

            


            

            //meals Available
            string meal = mealClosedFor(schoolInfo.schoolid, schoolInfo, cartItems, deliveryDate);
            if (!string.IsNullOrEmpty(meal))
            {
                throw new PlaceOrderException(CANTEENCLOSEDFORMEAL + meal + ". Delivery Date " + deliveryDate.Date);
            }

            //4. Product check
            List<int> productIds = cartItems.Select(item => item.productId.Value).Distinct().ToList();
            List<Models.Product> latestProducts = _productRepo.GetProducts(productIds);
            int dayOfWeek = (int)deliveryDate.DayOfWeek + 1;

            
            //5. cutoff
            TimezoneSettings zoneSettings = new TimezoneSettings();
            DateTime currentDateTimeInSchoolTimeZone = zoneSettings.getDateBySchoolTimeZone(schoolInfo.schoolid, schoolInfo.timeZone);

            if (schoolInfo.cut_days_before == 0)
            {
                //category check
                List<int> categoryIds = latestProducts.Select(e => e.productCategory.Value).Distinct().ToList();
                CheckCategoryCutOff(schoolInfo.schoolid, categoryIds, schoolInfo.timeZone, deliveryDate, currentDateTimeInSchoolTimeZone, schoolInfo);
            }
            else
            {
                if (IsAfterCuttOff(schoolInfo.schoolid, currentDateTimeInSchoolTimeZone, schoolInfo.cut_days_before, schoolInfo.daily_hour, schoolInfo.daily_min, deliveryDate))
                {
                    throw new PlaceOrderException(ORDERTOOLATE);

                }
            }



            if (productIds.Count == latestProducts.Count)
            {
                
                cartItems.ForEach(cartItem =>
                {
                   checkCartItem(cartItem, latestProducts, deliveryDate, schoolInfo);                    
                });

                /*try
                {
                    await Task.WhenAll(lstTasks.ToArray());
                }
                catch (AggregateException e)
                {
                    StringBuilder sb = new StringBuilder();
                    e.InnerExceptions.ToList().ForEach(ex =>
                    {
                        sb.Append(((PlaceOrderException)ex).Message);
                        sb.Append(' ');
                    });
                     throw new PlaceOrderException(sb.ToString());
                }*/
            }
            else
            {
                List<int> missingProductIds = productIds.Except(latestProducts.Select(product => product.id).ToList()).ToList();
                StringBuilder sb=new StringBuilder();
                ISet<String> setProducts = new HashSet<String>();
                cartItems
                    .Where(ci => missingProductIds.Contains(ci.productId.Value))
                        .Select(item => item.productName).ToList()
                            .ForEach(productName => {
                                setProducts.Add(productName);
                            }
                            
                    );
                foreach(string productName in setProducts){
                    sb.Append(productName);
                    sb.Append(' ');
                }
               

                throw new PlaceOrderException(PRODUCTSNOTAVAILABLE + sb);



                // errorProductItems.AddRange(cartItems.FindAll(item => missingProductIds.Contains(item.productId)));
                //throw new Exception(PRODUCTSERROR);
            }

            checkProductStock(cartItems, latestProducts, productIds);

            return true;
        }

        private void checkProductStock(List<CanteenShopcartItem> cartItems, List<Models.Product> latestProducts, List<int> productIds)
        {
            productIds.ForEach(productId =>
            {
                int cartProductCount = cartItems.Where(e => e.productId.Value == productId).Sum(e => e.productQuantity).Value;
                Models.Product product = latestProducts.Where(e => e.id == productId).FirstOrDefault();

                if (cartProductCount > product.stock)
                {
                    throw new PlaceOrderException(
                        "Product " + product.productName +
                        " went out of stock. " + 
                        "Available stock is " +
                        product.stock + ". " +
                        "Your quantity is " + cartProductCount);
                }

            });
        }

        private void checkCartItem(CanteenShopcartItem cartItem, List<Models.Product> latestProducts, DateTime deliveryDate, SchoolInfo schoolInfo)
        {
            string mealCode = cartItem.mealName == LUNCH ? "L" : cartItem.mealName == RECESS ? "R" : cartItem.mealName == THIRD ? "T" : "L";
            string mealName = cartItem.mealName == LUNCH ? schoolInfo.meal2 : cartItem.mealName == RECESS ? schoolInfo.meal1 : cartItem.mealName == THIRD ? schoolInfo.meal3 : schoolInfo.meal1;
            int dayOfWeek = (int)deliveryDate.DayOfWeek + 1;

            //product check
            Models.Product product = latestProducts.Where(e => e.id == cartItem.productId.Value).FirstOrDefault();
            if (!product.mealCode.Contains(mealCode))
                throw new PlaceOrderException(string.Format(PRODUCTNOTAVAILABLEFORMEAL + mealName, product.productName));

            if ((!string.IsNullOrEmpty(product.extra2) && product.extra2.Contains(dayOfWeek.ToString())))
            {
                throw new PlaceOrderException(string.Format(PRODUCTNOTAVAILABLEFORDAYOFWEEK + deliveryDate, product.productName));

            }

            if (product.productPrice != cartItem.productPrice)
            {
                throw new PlaceOrderException(PRODUCTPRICECHANGED + cartItem.productName);

            }

            //options check
            if (!string.IsNullOrEmpty(cartItem.productOptions) && cartItem.productOptions.Trim().Length > 0)
            {

                List<int> options = new List<int>();
                foreach (string id in cartItem.productOptions.Split('|'))
                {
                    options.Add(Int32.Parse(id));
                }

                List<int> latestOptions = _optionRepo.GetOptionIds(options, cartItem.productId.Value);
                if (options.Count != latestOptions.Count)
                {
                    throw new PlaceOrderException(OPTIONSNOTAVAILABLEFORPRODUCT + cartItem.productName);
                    /* double optionsPrice = latestOptions.Select(e => e.optionPrice).Sum().Value;
                     if ((double)cartItem.productPrice + optionsPrice != cartItem.productPriceWithOptionPrice)
                     {

                         throw new PlaceOrderException(PRICESFOROPTIONCHANGED + cartItem.productName);
                     }
                     */
                    //REQUIRED for REPEAT ORDER
                    /*List<JSONModels.OptionDTO> lstOptions = new List<JSONModels.OptionDTO>();
                    latestOptions.ForEach(option => lstOptions.Add(new JSONModels.OptionDTO(option.id, option.optionName1, option.optionPrice)));
                    cartItem.options = lstOptions;
                    allOrderItems.Add(item);*/
                }
               /* else
                {
                    throw new PlaceOrderException(OPTIONSNOTAVAILABLEFORPRODUCT + cartItem.productName);
                }*/
            }
            
        }

        private string mealClosedFor(int schoolId, SchoolInfo schoolInfo, List<CanteenShopcartItem> cartItems,DateTime deliveryDate)
        {
            int lunchItems = cartItems.Where(e => e.mealName == "LUNCH").Count();
            int recessItems = cartItems.Where(e => e.mealName == "RECESS").Count();
            int thirdItems = cartItems.Where(e => e.mealName == "THIRD").Count();

            if(lunchItems > 0)
            {
                //check if lunchopen
                if(!_schoolRepo.IsMealAvailableForDate(schoolId, deliveryDate, "LUNCH"))
                {
                    return schoolInfo.meal2;
                }
            }

            if(recessItems > 0)
            {
                //check if recess open
                if (!_schoolRepo.IsMealAvailableForDate(schoolId, deliveryDate, "RECESS") || !schoolInfo.isRecessOpen.Value)
                {
                    return schoolInfo.meal1;
                }
               
            }

            if(thirdItems > 0)
            {
                //check if third open
                if (!_schoolRepo.IsMealAvailableForDate(schoolId, deliveryDate, "THIRD") || string.IsNullOrEmpty(schoolInfo.meal3))
                {
                    return schoolInfo.meal3;
                }
                
            }

            return null;
        }
        private void CheckCategoryCutOff(int schoolId, List<int> categoryIds, string timeZone, DateTime deliveryDate, DateTime nowInSchoolTimezone, SchoolInfo schoolInfo)
        {
            List<Models.Category> categories = _categoryRepo.GetCategories(schoolId, categoryIds);
            categories.ForEach(category =>
            {
                if (!isWithinCutOffTime(category, nowInSchoolTimezone, deliveryDate, schoolInfo))
                {
                    throw new PlaceOrderException(CATEGORYORDERTOOLATE + category.categoryDescription);
                }
            });
        }

        private bool isWithinCutOffTime(Models.Category category, DateTime nowInSchoolTimezone, DateTime orderDate, SchoolInfo schoolInfo)
        {

            if (category.dailyHour == null || category.dailyMin == null) 
            {
                return true;
            }
            if (category.dailyHour == null) //|| category.CutOffType == null) - Not sure if this has to be done
            {
                category.dailyHour = schoolInfo.daily_hour;
                category.dailyMin = schoolInfo.daily_min;
            }

            var cutOff = orderDate.AddDays(category.cutDaysBefore == null ? 0 : -category.cutDaysBefore.Value);
            cutOff = new DateTime(cutOff.Year,
                                  cutOff.Month,
                                  cutOff.Day,
                                  category.dailyHour == null ? 0 : category.dailyHour.Value,
                                  category.dailyMin == null ? 0 : category.dailyMin.Value,
                                  0);

            return nowInSchoolTimezone < cutOff;
        }

        private bool IsAfterCuttOff(int schoolId, DateTime nowInSchoolTimezone, int? cutOffDays, int? cutOffHours, int? cutOffMins, DateTime deliveryDate)
        {
            var cutOff = nowInSchoolTimezone.AddDays(cutOffDays == null ? 0 : cutOffDays.Value);
            cutOff = new DateTime(cutOff.Year,
                                  cutOff.Month,
                                  cutOff.Day,
                                  cutOffHours == null ? 0 : cutOffHours.Value,
                                  cutOffMins == null ? 0 : cutOffMins.Value,
                                  0);

            //DateTime deliveryDateWithCutoff= new DateTime(deliveryDate.Year, deliveryDate.Month, deliveryDate.Day, cutOffHours == null ? 0 : cutOffHours.Value,
            //                    cutOffMins == null ? 0 : cutOffMins.Value,0
            //                  );

            deliveryDate = deliveryDate.AddHours(nowInSchoolTimezone.Hour);
            deliveryDate = deliveryDate.AddMinutes(nowInSchoolTimezone.Minute);

            if (deliveryDate.Date == cutOff.Date)
            {
                return deliveryDate > cutOff;
            }
            else if (deliveryDate.Date > cutOff.Date)
            {
                return deliveryDate < cutOff;
            }
            else
            {
                return true;
            }
            //nowInSchoolTimezone = nowInSchoolTimezone.AddDays(cutOffDays == null ? 0 : cutOffDays.Value);

        }
    }
}
