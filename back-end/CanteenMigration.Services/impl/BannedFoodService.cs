﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using DataDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Commons;
using CanteenMigration.Services.Commons;

namespace CanteenMigration.Services.impl
{
    public class BannedFoodService: IBannedFood
    {
        IBannedFoodRepository repo;
        public BannedFoodService(IBannedFoodRepository repo)
        {
            this.repo = repo;

        }

        public bool AddBannedProduct(List<int> studentIds, int parentId, int schoolId, int productId)
        {
            return repo.AddBannedProduct(studentIds, parentId, schoolId, productId);
        }

        public bool DeleteBannedProduct(int Id)
        {
            return repo.DeleteBannedProduct(Id);
        }

        public List<Models.Product> GetAllProducts(int schoolId)
        {
            return repo.GetAllProducts(schoolId);
        }

        public List<BannedFood> GetBannedFood(int parentId)
        {
            return repo.GetBannedFood(parentId);
        }
    }
}
