﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using CanteenMigration.Models;
using Newtonsoft.Json;
using CanteenMigration.BusinessDependencies;

namespace CanteenMigration.Services
{
    public class CartService : ICartService
    {
        ISchoolService _schoolServce;
        ICartRepository _cartRepo;
        IOptionRepository _optionRepo;
        IStudentRepository _studentRepo;
        IProductRepository _productRepo;
        ISchoolRepository _schoolRepo;
        Dictionary<int?, string> students;
        private ILoggerService<CartService> _loggingService;


        public CartService(ICartRepository cartRepo, IOptionRepository optionRepo, IStudentRepository studentRepo, 
                           IProductRepository productRepo, ISchoolRepository schoolRepo, 
                           ILoggerService<CartService> loggingService, ISchoolService schoolService)
        {
            this._cartRepo = cartRepo;
            this._optionRepo = optionRepo;
            this._studentRepo = studentRepo;
            this._productRepo = productRepo;
            this._schoolRepo = schoolRepo;
            this._loggingService = loggingService;
            this._schoolServce = schoolService;
        }

        public decimal GetCartAmout(List<CanteenShopcartItem> cartList, int schoolId)
        {
            decimal productsPrice = 0;
            cartList.ForEach(cartItem =>
            {
                decimal allOptionsPrice = 0;
                if (!string.IsNullOrEmpty(cartItem.productOptions))
                {
                    int[] intOptionIds = Array.ConvertAll(cartItem.productOptions.Split('|'), int.Parse);
                    decimal optionPrice = _optionRepo.GetAllOptionsPrice(schoolId, intOptionIds);
                    allOptionsPrice = allOptionsPrice + optionPrice;
                }
                productsPrice = productsPrice + ((cartItem.productPrice.Value + allOptionsPrice) * cartItem.productQuantity.Value);
            }
            );

            return productsPrice;
        }

        public List<CartProduct> GetCart(int parentId, int schoolId, DateTime deliveryDate)
        {
            //string schoolInfoJSON = _schoolRepo.GetSchoolInfo(schoolId);
            // SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);

            SchoolInfo schoolInfo = _schoolServce.GetSchoolInfo(schoolId);

            List <CanteenShopcartItem> lstCanteenShopCart = this._cartRepo.
                GetCart(parentId, schoolId, deliveryDate);
            List<CartProduct> lstCartProduct = new List<CartProduct>();

            students = new Dictionary<int?, string>();

            foreach (CanteenShopcartItem cart in lstCanteenShopCart)
            {
                cart.mealName = cart.mealName == "LUNCH" ? schoolInfo.meal2 : (cart.mealName == "RECESS" ? schoolInfo.meal1 : schoolInfo.meal3);
                decimal productsPrice = 0;
                decimal optionsPrice = 0;
                cart.options = new List<Option>();
                if (!string.IsNullOrEmpty(cart.productOptions))
                {
                    string[] strOptionIds = cart.productOptions.Split('|');
                    int[] optionIds = Array.ConvertAll(strOptionIds, int.Parse);
                    IQueryable<Option> lstOptions = this._optionRepo.GetOptions(optionIds, schoolId);
                    cart.options.AddRange(lstOptions.ToList<Option>());
                    optionsPrice = System.Convert.ToDecimal(lstOptions.Sum(x => x.optionPrice.Value));

                }
                productsPrice = productsPrice + ((cart.productPrice.Value + optionsPrice) * cart.productQuantity.Value);
                cart.productPrice = productsPrice;

                //prepare cart
                if (lstCartProduct.Count > 0)
                {
                    if (lstCartProduct.Where(e => e.studentId == cart.studentId).Count() > 0)
                    {
                        lstCartProduct.Where(e => e.studentId == cart.studentId).FirstOrDefault().canteenShopcarts.Add(cart);
                    }
                    else
                    {
                        CartProduct finalCart = new CartProduct
                        {
                            studentId = cart.studentId.Value,
                            //TODO Student names can be get from the cache
                            studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                        };
                        finalCart.canteenShopcarts = new List<CanteenShopcartItem>();
                        finalCart.canteenShopcarts.Add(cart);
                        lstCartProduct.Add(finalCart);
                    }
                }

                else
                {
                    CartProduct finalCart = new CartProduct
                    {
                        studentId = cart.studentId.Value,
                        studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                    };
                    finalCart.canteenShopcarts = new List<CanteenShopcartItem>();
                    finalCart.canteenShopcarts.Add(cart);
                    lstCartProduct.Add(finalCart);
                }
            }
            return lstCartProduct;
        }

        public bool UpdateCart(RemoveCartModel removeCart, int schoolId, int parentId)
        {
            try
            {
                if (removeCart != null)
                {
                    switch (removeCart.status)
                    {
                        case RemoveCartStatus.CART:
                            _cartRepo.EmptyCart(parentId, schoolId, removeCart.deliveryDate);
                            break;
                        case RemoveCartStatus.PRODUCT:

                            _cartRepo.RemoveCartItem(removeCart.cartId, parentId, schoolId);
                            break;
                        case RemoveCartStatus.QUANTITY:
                            _cartRepo.ReduceCartItemQuantity(removeCart.cartId, parentId, schoolId);
                            break;
                        default:
                            _loggingService.Debug("into default case" + parentId + " schoolid" + schoolId + " status:" + removeCart.status);
                            return false;

                    }
                    return true;
                }
                else
                {
                    _loggingService.Debug("remove cart is null **** parentid " + parentId + " schoolid" + schoolId);
                    return false;
                }
            }
            catch(Exception e)
            {
                _loggingService.Debug("Error while updating cart **** parentid " + parentId + " schoolid" + schoolId);
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                return false;
            }
           
        }

        public AddToCartResult AddItemToCart(AddToCart cart, int schoolId, int parentId)
        {
            AddToCartResult result = new AddToCartResult();
            List<AddToCartView> lst = new List<AddToCartView>();
            bool isQuanityExceeded = false;
            bool isPriceZeroError = false;
            List<ZeroPriceProducts> lstZero = new List<ZeroPriceProducts>();

            decimal optionPrice = 0;
            if (!String.IsNullOrEmpty(cart.optionIds))
            {
                int[] intOptionIds = Array.ConvertAll(cart.optionIds.Split('|'), int.Parse);
                optionPrice = _optionRepo.GetAllOptionsPrice(schoolId, intOptionIds);
                //TODO: Take options info from cache
            }

            Models.Product product = _productRepo.GetProduct(schoolId, cart.productId); //TODO: Get product from cache

            List<CanteenShopcartItem> addCartItemsList = new List<CanteenShopcartItem>();
            List<CanteenShopcartItem> updateCartItemsList = new List<CanteenShopcartItem>();
            if (product.productPrice + optionPrice == 0 && !CanteenMigration.Commons.ProductsWithZeroPrice.ProductIds.Contains(product.id.ToString()))
            {
                //TODO: check for the better option for products with zero price
                //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.OK, "Product price zero not allowed"));
                lstZero.Add(new ZeroPriceProducts
                {
                    productId = product.id,
                    productName = product.productName
                });
                isPriceZeroError = true;
            }
            else
            {
                foreach (int studentId in cart.studentIds)
                {
                    //add to cart
                    //2. Fetch product information
                    //3. if options are not empty then calculate option price 
                    //4. If product price + options price <=0 return with error
                    //5. Check if product exist into cart
                    // yes - increase quantity with quantity limit check 
                    //no - add new entry to c_shopcart table
                    CanteenShopcartItem cShopCart = _cartRepo.GetCartItemWithOption(schoolId, parentId, cart.productId, cart.optionIds, cart.mealName, studentId,
                                                                                cart.deliveryDate);
                    if (cShopCart == null)
                    {
                        cShopCart = new CanteenShopcartItem();
                        cShopCart.parentId = parentId;
                        cShopCart.studentId = studentId;
                        cShopCart.schoolId = schoolId;
                        cShopCart.productId = cart.productId;
                        cShopCart.productOptions = cart.optionIds; //options
                        cShopCart.mealName = cart.mealName;
                        cShopCart.productQuantity = cart.quantity;
                        cShopCart.cartDate = DateTime.Now;
                        cShopCart.cartDeliveryDate = cart.deliveryDate;

                        cShopCart.productPrice = product.productPrice;
                        cShopCart.productName = product.productName;

                        if (product.quantityLimit >= cart.quantity)
                        {
                            addCartItemsList.Add(cShopCart);//_cartRepo.AddItemToCart(cShopCart);//context.CanteenShopCart.Add(cShopCart);
                        }
                        else
                        {
                            lst.Add(new AddToCartView
                            {
                                productId = product.id,
                                productName = product.productName,
                                maximumQuantityAllowed = product.quantityLimit,
                                exceededQuantity = product.quantityLimit - cart.quantity,
                                studentName = _studentRepo.GetStudentName(studentId, schoolId)

                            });
                            isQuanityExceeded = true;
                        }
                    }
                    else
                    {
                        //update cart qty
                        if (product.quantityLimit >= cShopCart.productQuantity + cart.quantity)
                        {
                            cShopCart.productQuantity = cShopCart.productQuantity + cart.quantity;
                            updateCartItemsList.Add(cShopCart);
                        }

                        else if (product.quantityLimit < cShopCart.productQuantity + cart.quantity)
                        {
                            lst.Add(new AddToCartView
                            {
                                productId = product.id,
                                productName = product.productName,
                                maximumQuantityAllowed = product.quantityLimit,
                                exceededQuantity = (cShopCart.productQuantity.Value + cart.quantity) - product.quantityLimit,
                                studentName = _studentRepo.GetStudentName(studentId, schoolId)
                            });
                            isQuanityExceeded = true;
                        }
                    }


                }//foreach
            }
            _cartRepo.AddItemsToCart(addCartItemsList);
            _cartRepo.UpdateCartItems(updateCartItemsList);

            result.isPriceZeroError = isPriceZeroError;
            result.isQuanityExceeded = isQuanityExceeded;
            result.priceZeroProducts = lstZero;
            result.quantityExceedProducts = lst;
            result.cartCount = _cartRepo.GetCartItemsCount(parentId, schoolId, cart.deliveryDate);
            return result;

        }

        public List<CanteenShopcartItem> GetAllCartItems(int parentId, int schoolId, DateTime deliveryDate)
        {
            return _cartRepo.GetCart(parentId, schoolId, deliveryDate);
        }
    }
}



