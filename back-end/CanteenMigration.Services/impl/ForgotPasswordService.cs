﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services.Commons;
using DataDependencies;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class ForgotPasswordService : IForgotPasswordService
    {
        IParentRepository _parentRepo;
        IEmailService _emailService;
        IEncryptionService _encryptionService;
        public ForgotPasswordService(IParentRepository parentRepo, IEmailService emailService, IEncryptionService encryptionService)
        {
            this._parentRepo = parentRepo;
            this._emailService = emailService;
            this._encryptionService = encryptionService;
        }

        public void SendForgotPasswordEmail(Parent parent)
        {
            string linkToSend = string.Empty;
            linkToSend = GetForgotPasswordLink(parent.id, parent.schoolId);
            _emailService.SendForgotPasswordLink(parent.email, "School24 - Password Reset.", linkToSend, parent.firstName + " " + parent.lastName);
            //_emailRepo.AddEmail(parent.email, "School24 - Password Reset.", linkToSend);
        }

        public void ChangePassword(Parent parent, string newPassword)
        {
            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(newPassword);

            //parent.passwordOld = parent.password;
            parent.password = encryptedPassword;
            _parentRepo.SaveUpdatedParent();


        }
        private string GetForgotPasswordLink(int parentId, int? schoolId)
        {
            string randomNumber1 = Guid.NewGuid().ToString("N").Substring(0, 12);
            string randomNumber2 = Guid.NewGuid().ToString("N").Substring(0, 12);
            string timestamp = DateTime.UtcNow.Ticks.ToString();

            string linkRandomNumber = randomNumber1 + "-" + parentId + "-" + randomNumber2 + "-" + schoolId + "-" + timestamp;

            string link = "https://www.school24.net.au/canteenorder/ResetPassword/" + _encryptionService.encrypt(linkRandomNumber);
            //string link = "http://localhost:4200/canteenorder/ResetPassword/" + _encryptionService.encrypt(linkRandomNumber);
            return link;
        }

        private string encrypt(string encryptString)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}
