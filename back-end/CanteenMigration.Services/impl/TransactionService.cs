﻿using CanteenMigration.Models;
using DataDependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class TransactionService : ITransactionService
    {
        ITransactionLogRepository _repo;
        public TransactionService(ITransactionLogRepository repo)
        {
            this._repo = repo;
        }
        public PaymentGatewayTransactionLog CreateTransaction(int parentId, int schoolId, decimal orderAmount, string transactionType, string paymentGatewayName, bool isCCFeeRequired)
        {
            
            return _repo.CreateTransactionLog(parentId, schoolId, orderAmount, transactionType, paymentGatewayName, isCCFeeRequired);
        }

        public PaymentGatewayTransactionLog CreateTransactionForEvent(int parentId, int schoolId, decimal orderAmount, string transactionType, string paymentGatewayName, bool isCCFeeRequired)
        {

            return _repo.CreateTransactionLogForEvent(parentId, schoolId, orderAmount, transactionType, paymentGatewayName, isCCFeeRequired);
        }

        public bool UpdateTransaction(string status, PaymentGatewayTransactionLog log, int schoolId, string paymentIdentifier)
        {
            return _repo.UpdateTransactionLog(status, log, schoolId, paymentIdentifier);
        }

        
    }
}
