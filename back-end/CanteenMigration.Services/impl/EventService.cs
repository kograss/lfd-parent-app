﻿using CanteenMigration.Commons;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using DataDependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolEvents.Services.impl
{
    public class EventService : IEventService
    {
        private IEventRepository _repo;
        private IOptionRepository _optionRepo;
        private IStudentRepository _studentRepo;
        private IProductRepository _productRepo;
        private IDBContext _context;
        private IOrderRepository _orderRepo;
        private ITransactionRepository _transactionRepo;
        private IParentRepository _parentRepo;
        private IEmailService _emailService;
        ISchoolRepository _schoolRepo;
        private ISchoolService _schoolService;
        private ISchoolEventService _schoolEventService;
        public const string PRODUCTSOUTOFSTOCK = "Some Products are out of stock";
        public EventService(IEventRepository repo, IOptionRepository optionsRepo,
                            IStudentRepository studentRepo, IProductRepository productRepo,
                            IDBContext context, IOrderRepository orderRepo, ITransactionRepository transactionRepo,
                            IParentRepository parentRepo, IEmailService emailService, ISchoolRepository _schoolRepo,
                            ISchoolService schoolService, ISchoolEventService _schoolEventService)
        {
            _repo = repo;
            _optionRepo = optionsRepo;
            _studentRepo = studentRepo;
            _productRepo = productRepo;
            _context = context;
            _orderRepo = orderRepo;
            _transactionRepo = transactionRepo;
            _parentRepo = parentRepo;
            _emailService = emailService;
            this._schoolRepo = _schoolRepo;
            _schoolService = schoolService;
            this._schoolEventService = _schoolEventService;
        }

        public SpecialEvent GetSpecialEvent(int schoolId, int eventId)
        {
            return _repo.GetSpecialEvent(schoolId, eventId);
        }
        public List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId)
        {
            List<CanteenMigration.JSONModels.Product> products = _repo.GetAllProductEvents(schoolId, eventId);
            foreach (CanteenMigration.JSONModels.Product product in products)
            {
                List<CanteenMigration.JSONModels.Options> options = _optionRepo.GetAllProductOptions(schoolId, product.catalogID);
                if (options != null && options.Count > 0)
                {
                    List<OptionSubGroup> optionSubGroup = new List<OptionSubGroup>();

                    foreach (Options option in options)
                    {
                        option.Option_SubGroupId = String.IsNullOrEmpty(option.Option_SubGroupId) ? string.Empty : option.Option_SubGroupId;

                        if (optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).Count() > 0)
                        {
                            OptionSubGroup o = optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).FirstOrDefault();
                            o.options.Add(option);
                        }
                        else
                        {
                            OptionSubGroup o = new OptionSubGroup();
                            o.OptionSubGroupName = option.Option_SubGroupId;
                            o.Max_Items = option.Max_Items.GetValueOrDefault();
                            o.Required = option.Required.GetValueOrDefault();
                            o.options = new List<Options>();
                            o.options.Add(option);
                            optionSubGroup.Add(o);
                        }
                    }
                    product.OptionSubGroup = optionSubGroup;
                }//options are there 
            }
            return products;
        }

        public List<SchoolEventCategory> GetEventCategories(int schoolId, int eventId)
        {
            return _repo.GetEventCategories(schoolId, eventId);
        }

        public List<SchoolEventInfo> GetSchoolEvents(int schoolId)
        {
            /*SchoolInfo schoolInfo = new SchoolInfo();
            string schoolInfoJson = _schoolRepo.GetSchoolInfo(schoolId);
            if (string.IsNullOrEmpty(schoolInfoJson))
            {
                return null;
            }
            
            schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJson);
            */

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (!schoolInfo.canteenopen.HasValue || !schoolInfo.canteenopen.Value)
            {
                return null;
            }

            TimezoneSettings zoneSettings = new TimezoneSettings();
            DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
            return _repo.GetSchoolEvents(schoolId, nowInSchoolTimezone);
            /*var canteenEvents = _repo.GetSchoolEvents(schoolId, nowInSchoolTimezone);
            var schoolEvents = _schoolEventService.GetSchoolEvents(schoolId);
            if(canteenEvents == null || canteenEvents.Count == 0 )
            {
                return schoolEvents;
            }
            if(schoolEvents == null || schoolEvents.Count == 0)
            {
                return canteenEvents;
            }

            return canteenEvents.Concat(schoolEvents).ToList();*/
        }

        public List<CanteenMigration.JSONModels.Product> GetCategoriesProductEvents(int schoolId, int eventId, int categoryId)
        {
            return _repo.GetCategoriesProductEvents(schoolId, eventId, categoryId);
        }

        public List<EventCartProduct> GetCart(int parentId, int schoolId, int eventId)
        {
            List<EventShopcart> lstCanteenShopCart = _repo.GetCart(schoolId, parentId, eventId);
            List<EventCartProduct> lstCartProduct = new List<EventCartProduct>();

            Dictionary<int?, string> students;
            students = new Dictionary<int?, string>();

            foreach (EventShopcart cart in lstCanteenShopCart)
            {
                cart.extra3 = "EVENT"; //TODO NAME
                decimal productsPrice = 0;
                decimal optionsPrice = 0;
                cart.options = new List<Option>();
                if (!string.IsNullOrEmpty(cart.productOptions))
                {
                    string[] strOptionIds = cart.productOptions.Split('|');
                    int[] optionIds = Array.ConvertAll(strOptionIds, int.Parse);
                    IQueryable<Option> lstOptions = this._optionRepo.GetOptions(optionIds, schoolId);
                    cart.options.AddRange(lstOptions.ToList<Option>());
                    optionsPrice = System.Convert.ToDecimal(lstOptions.Sum(x => x.optionPrice.Value));

                }
                productsPrice = productsPrice + ((cart.productPrice.Value + optionsPrice) * cart.productQuantity.Value);
                cart.productPrice = productsPrice;

                //prepare cart
                if (lstCartProduct.Count > 0)
                {
                    if (lstCartProduct.Where(e => e.studentId == cart.studentId).Count() > 0)
                    {
                        lstCartProduct.Where(e => e.studentId == cart.studentId).FirstOrDefault().canteenShopcarts.Add(cart);
                    }
                    else
                    {
                        EventCartProduct finalCart = new EventCartProduct
                        {
                            studentId = cart.studentId.Value,
                            //TODO Student names can be get from the cache
                            studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                        };
                        finalCart.canteenShopcarts = new List<EventShopcart>();
                        finalCart.canteenShopcarts.Add(cart);
                        lstCartProduct.Add(finalCart);
                    }
                }

                else
                {
                    EventCartProduct finalCart = new EventCartProduct
                    {
                        studentId = cart.studentId.Value,
                        studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                    };
                    finalCart.canteenShopcarts = new List<EventShopcart>();
                    finalCart.canteenShopcarts.Add(cart);
                    lstCartProduct.Add(finalCart);
                }
            }
            return lstCartProduct;
        }

        public bool UpdateCart(EventCartUpdate removeCart, int schoolId, int parentId)
        {
            switch (removeCart.status)
            {
                case RemoveCartStatus.CART:
                    _repo.EmptyCart(parentId, schoolId, removeCart.eventId);
                    break;
                case RemoveCartStatus.PRODUCT:
                    _repo.RemoveCartItem(removeCart.cartId, parentId, schoolId);
                    break;
                case RemoveCartStatus.QUANTITY:
                    _repo.ReduceCartItemQuantity(removeCart.cartId, parentId, schoolId);
                    break;
            }
            return true;
        }

        public AddToCartResult AddItemToCart(EventAddToCart cart, int schoolId, int parentId, int eventId)
        {
            AddToCartResult result = new AddToCartResult();
            List<AddToCartView> lst = new List<AddToCartView>();
            bool isQuanityExceeded = false;
            bool isPriceZeroError = false;
            List<ZeroPriceProducts> lstZero = new List<ZeroPriceProducts>();

            SpecialEvent specialEvent = _repo.GetSpecialEvent(schoolId, eventId);
            bool isFixedPriceEvent = false;
            if (specialEvent.IsSetPrice.HasValue && specialEvent.IsSetPrice == 1)
            {
                isFixedPriceEvent = true;
            }
            decimal optionPrice = 0;
            if (!String.IsNullOrEmpty(cart.optionIds))
            {
                int[] intOptionIds = Array.ConvertAll(cart.optionIds.Split('|'), int.Parse);
                optionPrice = _optionRepo.GetAllOptionsPrice(schoolId, intOptionIds);
                //TODO: Take options info from cache
            }

            CanteenMigration.Models.Product product = _productRepo.GetProduct(schoolId, cart.productId); //TODO: Get product from cache

            List<EventShopcart> addCartItemsList = new List<EventShopcart>();
            List<EventShopcart> updateCartItemsList = new List<EventShopcart>();
            if (product.productPrice + optionPrice == 0 && !CanteenMigration.Commons.ProductsWithZeroPrice.ProductIds.Contains(product.id.ToString()))
            {
                //TODO: check for the better option for products with zero price
                //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.OK, "Product price zero not allowed"));
                lstZero.Add(new ZeroPriceProducts
                {
                    productId = product.id,
                    productName = product.productName
                });
                isPriceZeroError = true;
            }
            else
            {
                foreach (int studentId in cart.studentIds)
                {
                    //add to cart
                    //2. Fetch product information
                    //3. if options are not empty then calculate option price 
                    //4. If product price + options price <=0 return with error
                    //5. Check if product exist into cart
                    // yes - increase quantity with quantity limit check 
                    //no - add new entry to c_shopcart table
                    EventShopcart cShopCart = _repo.GetCartItemWithOption(schoolId, parentId, cart.productId, cart.optionIds, studentId, cart.eventId);

                    if (cShopCart == null)
                    {
                        cShopCart = new EventShopcart();
                        cShopCart.parentId = parentId;
                        cShopCart.studentId = studentId;
                        cShopCart.schoolId = schoolId;
                        cShopCart.productId = cart.productId;
                        cShopCart.productOptions = cart.optionIds; //options
                        cShopCart.extra3 = "SPECIAL";
                        cShopCart.productQuantity = cart.quantity;
                        cShopCart.cartDate = DateTime.Now;
                        cShopCart.eventId = cart.eventId;

                        cShopCart.productPrice = product.productPrice;
                        cShopCart.productName = product.productName;

                        if (product.quantityLimit >= cart.quantity && !isFixedPriceEvent)
                        {
                            addCartItemsList.Add(cShopCart);//_cartRepo.AddItemToCart(cShopCart);//context.CanteenShopCart.Add(cShopCart);
                        }
                        else
                        {
                            /*if (isFixedPriceEvent && cart.quantity > 1)
                            {
                                lst.Add(new AddToCartView
                                {
                                    productId = product.id,
                                    productName = product.productName,
                                    maximumQuantityAllowed = product.quantityLimit,
                                    exceededQuantity = product.quantityLimit - cart.quantity,
                                    studentName = _studentRepo.GetStudentName(studentId, schoolId)

                                });
                                isQuanityExceeded = true;
                            }
                            else
                            {
                                addCartItemsList.Add(cShopCart);
                            }*/
                            addCartItemsList.Add(cShopCart);
                        }
                    }
                    else
                    {
                        //update cart qty
                        if (product.quantityLimit >= cShopCart.productQuantity + cart.quantity && !isFixedPriceEvent)
                        {
                            cShopCart.productQuantity = cShopCart.productQuantity + cart.quantity;
                            updateCartItemsList.Add(cShopCart);
                        }

                        else if (product.quantityLimit < cShopCart.productQuantity + cart.quantity)
                        {
                            lst.Add(new AddToCartView
                            {
                                productId = product.id,
                                productName = product.productName,
                                maximumQuantityAllowed = product.quantityLimit,
                                exceededQuantity = (cShopCart.productQuantity.Value + cart.quantity) - product.quantityLimit,
                                studentName = _studentRepo.GetStudentName(studentId, schoolId)
                            });
                            isQuanityExceeded = true;
                        }
                    }


                }//foreach
            }
            _repo.AddItemsToCart(addCartItemsList);
            _repo.UpdateCartItems(updateCartItemsList);

            result.isPriceZeroError = isPriceZeroError;
            result.isQuanityExceeded = isQuanityExceeded;
            result.priceZeroProducts = lstZero;
            result.quantityExceedProducts = lst;
            result.cartCount = _repo.GetCartItemsCount(parentId, schoolId, cart.eventId);
            return result;
        }

        public List<EventShopcart> GetAllCartItems(int parentId, int schoolId, int eventId)
        {
            return _repo.GetCart(schoolId, parentId, eventId);
        }

        public decimal GetCartAmout(List<EventShopcart> cartList, int schoolId)
        {
            decimal productsPrice = 0;
            cartList.ForEach(cartItem =>
            {
                decimal allOptionsPrice = 0;
                if (!string.IsNullOrEmpty(cartItem.productOptions))
                {
                    int[] intOptionIds = Array.ConvertAll(cartItem.productOptions.Split('|'), int.Parse);
                    decimal optionPrice = _optionRepo.GetAllOptionsPrice(schoolId, intOptionIds);
                    allOptionsPrice = allOptionsPrice + optionPrice;
                }
                productsPrice = productsPrice + ((cartItem.productPrice.Value + allOptionsPrice) * cartItem.productQuantity.Value);
            }
            );

            return productsPrice;
        }

        public OrderResult PlaceOrder(List<EventShopcart> allCartItems, List<EventOrder> orders, DateTime deliveryDate,
                                      int eventId, decimal bagCharges, decimal chargesPerOrder,
                                      Parent parent, SchoolInfo schoolSetup,
                                      bool isCreditOrder, string transactionId, SpecialEvent specialEvent, bool isBalancePlusCard)
        {
            var allTransactions = new List<Transaction>();
            OrderResult result = new OrderResult();
            decimal grandTotal = 0;
            List<int> orderIds = new List<int>();
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    //deduct stock: TODO
                    if (!_repo.DeductProductStock(allCartItems, schoolSetup.schoolid))
                    {
                        //dbContextTransaction.Rollback();
                        throw new PlaceOrderException(PRODUCTSOUTOFSTOCK);
                    }
                    foreach (EventOrder order in orders)
                    {
                        int numberOfMeals = 1;
                        List<EventShopcart> cartItems = allCartItems.Where(cartItem =>
                              cartItem.studentId == order.studentId
                        ).ToList();

                        string orderNumber = Guid.NewGuid().ToString();

                        string bag = specialEvent.bagName;//"NA";// schoolSetup.chargeforbag == "YES" ? "BUY" : "NA";
                        string orderType = string.Empty;

                        decimal orderAmount = 0;
                        if (specialEvent.IsSetPrice.HasValue && specialEvent.IsSetPrice == 1)
                        {
                            orderAmount = specialEvent.EventPrice.Value + (bagCharges * numberOfMeals) + chargesPerOrder;
                        }
                        else
                        {
                            orderAmount = GetCartAmout(cartItems, schoolSetup.schoolid) + (bagCharges * numberOfMeals) + chargesPerOrder;
                        }

                        grandTotal = grandTotal + orderAmount;
                        orderType = "SPECIAL";

                        ParentsOrders parentsOrder = new ParentsOrders();
                        parentsOrder.studentId = order.studentId;
                        parentsOrder.schoolId = schoolSetup.schoolid;
                        parentsOrder.orderDate = DateTime.Now;
                        parentsOrder.orderNumber = orderNumber;
                        parentsOrder.orderComment = order.comment;
                        parentsOrder.orderReason = order.reason;
                        parentsOrder.orderShippedDate = deliveryDate;
                        parentsOrder.orderAmount = orderAmount;
                        parentsOrder.orderType = orderType;
                        if (specialEvent.isBagAvailable.HasValue && specialEvent.isBagAvailable.Value)
                        {
                            parentsOrder.bag = bag;
                            parentsOrder.bagPrice = specialEvent.BagPrice;
                        }

                        parentsOrder.parentId = parent.id;
                        parentsOrder.schoolModel = schoolSetup.fee_model;
                        parentsOrder.schoolPlan = schoolSetup.s_fee_plan;
                        parentsOrder.parentPlan = parent.parentFeePlan;
                        parentsOrder.orderCancel = false;
                        parentsOrder.orderShipMethod = eventId;

                        _orderRepo.AddParentsOrder(parentsOrder);
                        orderIds.Add(parentsOrder.orderId);

                        string orderNumberSpecial = "S" + parentsOrder.orderId;


                        //process lunch orders
                        processSpecialOrders(order, schoolSetup.schoolid, parent.id, orderNumberSpecial, orderAmount, bag, cartItems, _orderRepo, eventId);

                        //Create record in Transactions
                        Transaction transaction = new Transaction();
                        transaction.parentId = parent.id;
                        transaction.schoolId = schoolSetup.schoolid;
                        transaction.transactionAmount = orderAmount;
                        transaction.transactionType = isCreditOrder ? "ORDER:DEBIT" : "PayAtCheckout";
                        transaction.extra1 = parentsOrder.orderId.ToString();
                        transaction.balanceBefore = parent.credit;
                        transaction.balanceAfter = isCreditOrder ? parent.credit - orderAmount : parent.credit;
                        transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                        transaction.studentId = order.studentId;
                        transaction.transactionId = transactionId;
                        transaction.recyclePrice = specialEvent.BagPrice;
                         if (isBalancePlusCard)
                        {
                            transaction.transactionType = "ORDER:DEBIT:PayAtCheckout";
                            transaction.balanceAfter = 0;
                        }
                        allTransactions.Add(transaction);
                        // _transactionRepo.AddTransaction(transaction);
                    }//foreach

                    //Update credit
                    if (isBalancePlusCard)
                    {
                        var afterBalance = parent.credit - grandTotal;
                        allTransactions.ForEach(t =>
                        {
                            t.balanceAfter = 0;
                            t.balanceBefore = parent.credit;
                            t.balanceDeductedAmount = parent.credit;
                            t.cardPaymentAmount = grandTotal - parent.credit;
                        });
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, parent.credit.Value);
                    }
                    else if(isCreditOrder)
                    {
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, grandTotal);
                    }
                    _transactionRepo.AddTransactions(allTransactions);
                    _repo.EmptyCart(parent.id, schoolSetup.schoolid, eventId);

                    dbContextTransaction.Commit();
                    result.isOrderSuccess = true;
                    result.message = "Order placed successfully";
                    result.orderIds = orderIds;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    /*result.isOrderSuccess = false;
                    result.message = "Error while placing order. Please try again";
                    result.orderIds = null;*/
                    throw e;
                }
            }
            //TODO: send email
            if (result.orderIds != null)
            {
                result.orderIds.ForEach(order =>
                {
                    _emailService.SendEventEmail(parent.email, order, parent.id, schoolSetup.schoolid, parent.firstName + " " + parent.lastName);
                });
            }
            return result;
        }

        public void processSpecialOrders(EventOrder order, int schoolId, int parentId, string orderNumber,
                                       decimal orderAmount, string bag, List<EventShopcart> cartList, IOrderRepository _orderRepo, int eventId)
        {
            Orders o = new Orders();
            o.studentId = order.studentId;
            o.schoolId = schoolId;
            o.orderDate = DateTime.Now;
            o.orderNumber = orderNumber;
            o.orderComment = order.comment;
            o.orderReason = order.reason;
            o.orderShippedDate = order.deliveryDate;
            o.orderShipMethod = eventId;
            o.orderAmount = orderAmount;
            o.orderType = "SPECIAL";
            o.bag = bag;
            o.bagPrice = order.chargeForBag;

            _orderRepo.AddOrders(o);

            int counter = 0;
            List<OrderItems> orderItemsList = new List<OrderItems>();
            foreach (EventShopcart c in cartList)
            {
                //insert into oitems
                OrderItems orderItem = new OrderItems();
                orderItem.orderID = o.id;
                orderItem.orderIntemNumber = counter++;
                orderItem.productId = c.productId;
                orderItem.options = c.productOptions;
                orderItem.numerOfItems = (Int16)c.productQuantity.Value;
                orderItem.schoolId = schoolId;
                orderItem.extra2 = "SPECIAL";

                orderItemsList.Add(orderItem);
            }
            _orderRepo.InsertLunchOrderItems(orderItemsList);
        }

        public decimal GetChargesPerOrderForEvent(SpecialEvent specialEvent, SchoolInfo schoolInfo, Parent parent)
        {
            decimal chargesPerOrder = 0;
            if (specialEvent.whoPays.HasValue && specialEvent.whoPays == 1)
            {
                //school pays
                chargesPerOrder = 0;
            }

            else if (specialEvent.whoPays.HasValue && specialEvent.whoPays == 2)
            {
                //parent pays
                chargesPerOrder = 0.25M;
            }
            else
            {
                //old logic
                if (schoolInfo.fee_model == 1)
                {
                    chargesPerOrder = 0;
                }
                else
                {
                    if (parent.parentFeePlan == 1)
                    {
                        chargesPerOrder = 0.25M;
                    }
                }
            }

            return chargesPerOrder;
        }

        public OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId)
        {
            OrderCancelResult result = new OrderCancelResult();
            //1. check if order exists
            if (!_orderRepo.IsOrderExists(orderId, parentId, schoolId))
            {
                //return with error
                result.isOrderCancelled = false;
                result.message = "Order does not exists! ";
                return result;
            }

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            ParentsOrders parentsOrder = _orderRepo.GetParentOrder(orderId, schoolId);

            if (parentsOrder == null || parentsOrder.orderId == 0)
            {
                result.isOrderCancelled = false;
                result.message = "Order is missing. Please contact admin.";
                return result;
            }

            TimezoneSettings zoneSettings = new TimezoneSettings();
            SpecialEvent canteenEvent = GetSpecialEvent(schoolId, parentsOrder.orderShipMethod.Value);
            if (canteenEvent == null)
            {
                //return error
                result.isOrderCancelled = false;
                result.message = "Event not found. Please contact admin. ";
                return result;
            }

            DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
            if (nowInSchoolTimezone > canteenEvent.specialDateEnd)
            {
                //return error
                result.isOrderCancelled = false;
                result.message = "You cannot cancel order after cutoff. Please contact admin. ";
                return result;
            }

            string orderNumber = parentsOrder.orderNumber;
            string orderNumberSpecial = "S" + parentsOrder.orderId;
            CanteenMigration.Models.Orders orderToDelete = _orderRepo.GetOrderByOrderNumber(orderNumberSpecial, schoolId);
            //3. MealWise cancellation
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    List<Orders> ordersToDelete = new List<Orders>();//from orders table
                    if (!string.IsNullOrEmpty(orderNumber))
                    {
                        //perform lunch cancellation
                        //3.1 insert into orders_deleted table from orders - copy_order()
                        OrdersDeleted ordersDeleted = new OrdersDeleted();
                        ordersDeleted.studentId = parentsOrder.studentId;
                        ordersDeleted.schoolId = schoolId;
                        ordersDeleted.orderDate = parentsOrder.orderDate;
                        ordersDeleted.orderNumber = orderNumber;
                        ordersDeleted.orderComment = parentsOrder.orderComment;
                        ordersDeleted.orderAmount = parentsOrder.orderAmount;
                        ordersDeleted.orderShippedDate = parentsOrder.orderShippedDate;
                        ordersDeleted.orderShipMethod = parentsOrder.orderShipMethod;
                        ordersDeleted.orderType = parentsOrder.orderType;
                        ordersDeleted.bag = parentsOrder.bag;
                        ordersDeleted.bagPrice = parentsOrder.bagPrice;
                        ordersDeleted.oldOrderId = orderId;

                        //3.2 insert oitems to - oitems_deleted and delete etries from oitems
                        List<OrderItems> orderItems = _orderRepo.GetOrderItems(orderToDelete.id, schoolId);
                        List<OrderItemsDeleted> orderItemsDeleted = new List<OrderItemsDeleted>();
                        foreach (OrderItems orderItem in orderItems)
                        {
                            OrderItemsDeleted orderItemDeleted = new OrderItemsDeleted();
                            orderItemDeleted.orderID = orderItem.orderID;
                            orderItemDeleted.orderIntemNumber = orderItem.orderIntemNumber;
                            orderItemDeleted.productId = orderItem.productId;
                            orderItemDeleted.numerOfItems = orderItem.numerOfItems;
                            orderItemDeleted.options = orderItem.options;
                            orderItemDeleted.schoolId = schoolId;
                            orderItemDeleted.extra2 = orderItem.extra2;

                            orderItemsDeleted.Add(orderItemDeleted);
                            //TODO: work on the stock
                            _repo.AddStockToProduct(orderItem.productId.Value, orderItem.numerOfItems, schoolId, canteenEvent.specialId);
                        }

                        _orderRepo.AddToDeletedOrder(ordersDeleted);
                        _orderRepo.AddOrderItemsDeleted(orderItemsDeleted);

                        //3.3 delete from orders table
                        //Orders order = _orderRepo.GetOrder(lunchOrderNumber.Value, schoolId);
                        ordersToDelete.Add(orderToDelete);
                        _orderRepo.DeleteOrderItem(orderItems);
                    }

                    //4. update parents_orders table cancel order - cancel_big_order() 
                    _orderRepo.CancelParentsOrder(parentsOrder);

                    _orderRepo.DeleteOrders(ordersToDelete, schoolId);

                    decimal parentCredit = _parentRepo.GetParentCredit(parentId, schoolId);
                    //5. update parent credit
                    _parentRepo.AddParentCredit(parentId, schoolId, parentsOrder.orderAmount.Value);

                    Parent parent = _parentRepo.GetParent(schoolId, parentId);
                    //6. insert into Transactions table

                    Transaction transaction = new Transaction();
                    transaction.parentId = parentId;
                    transaction.schoolId = schoolId;
                    transaction.transactionAmount = parentsOrder.orderAmount;
                    transaction.transactionType = "CANCELLATION:CREDIT";
                    transaction.extra1 = parentsOrder.orderId.ToString();
                    transaction.balanceBefore = parentCredit;
                    transaction.balanceAfter = parentCredit + parentsOrder.orderAmount;
                    transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                    transaction.studentId = parentsOrder.studentId;
                    _transactionRepo.AddTransaction(transaction);

                    result.isOrderCancelled = true;
                    result.message = "Order cancelled successfully";
                    result.parentCredit = parentCredit + parentsOrder.orderAmount;
                    dbContextTransaction.Commit();
                    _emailService.SendCancelOrderEmail(parent.email, parentCredit, parent.credit.Value, parentsOrder.orderAmount.Value, parent.firstName + " " + parent.lastName, parentsOrder.orderId.ToString());
                    return result;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    result.isOrderCancelled = false;
                    result.message = "Error while cancelling order. Please try again";
                    return result;
                }
            }
        }
    }
}
