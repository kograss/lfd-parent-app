﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services.exception;
using DataDependencies;
using DataDependencies.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class SchoolEventService : ISchoolEventService
    {
        private ISchoolEventRepository _repo;
        private IStudentRepository _studentRepo;
        private IDBContext _context;
        private IEmailService _emailService;
        // public ILoggerService<SchoolEventService> _loggingService;
        private ITransactionRepository _transactionRepo;
        private IParentRepository _parentRepo;
        private ISchoolService _schoolService;
        private IOrderHistoryRepository _orderhistoryRepo;
        public const string PRODUCTSOUTOFSTOCK = "Some Products are out of stock";

        public SchoolEventService(ISchoolEventRepository repo, IStudentRepository studentRepo, IDBContext context, IEmailService emailService,
                ITransactionRepository transactionRepo, IParentRepository parentRepo, ISchoolService _schoolService,
                IOrderHistoryRepository _orderhistoryRepo)
        {
            _repo = repo;
            _studentRepo = studentRepo;
            _context = context;
            _emailService = emailService;
            _transactionRepo = transactionRepo;
            _parentRepo = parentRepo;
            this._schoolService = _schoolService;
            this._orderhistoryRepo = _orderhistoryRepo;
        }

        public List<SchoolEventInfo> GetSchoolEvents(int schoolId)
        {
            return _repo.GetSchoolEvents(schoolId, DateTime.Now);
        }

        public SchoolEvent2 GetSchoolEvent(int schoolId, int eventId)
        {
            return _repo.GetSchoolEvent(schoolId, eventId);
        }
        public int GetSchoolId(int eventId)
        {
            return _repo.GetSchoolId(eventId);
        }
        public List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId)
        {
            List<CanteenMigration.JSONModels.Product> products = _repo.GetAllProductEvents(schoolId, eventId);

            foreach (CanteenMigration.JSONModels.Product product in products)
            {
                List<CanteenMigration.JSONModels.Options> options = _repo.GetAllProductOptions(schoolId, product.catalogID);
                if (options != null && options.Count > 0)
                {
                    List<OptionSubGroup> optionSubGroup = new List<OptionSubGroup>();

                    foreach (Options option in options)
                    {
                        option.Option_SubGroupId = String.IsNullOrEmpty(option.Option_SubGroupId) ? string.Empty : option.Option_SubGroupId;

                        if (optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).Count() > 0)
                        {
                            OptionSubGroup o = optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).FirstOrDefault();
                            o.options.Add(option);
                        }
                        else
                        {
                            OptionSubGroup o = new OptionSubGroup();
                            o.OptionSubGroupName = option.Option_SubGroupId;
                            o.Max_Items = option.Max_Items.GetValueOrDefault();
                            o.Required = option.Required.GetValueOrDefault();
                            o.options = new List<Options>();
                            o.options.Add(option);
                            optionSubGroup.Add(o);
                        }
                    }
                    product.OptionSubGroup = optionSubGroup;
                }//options are there 
            }
            return products;
        }

        public List<SchoolEventCartProduct> GetCart(int parentId, int schoolId, int eventId)
        {
            List<SchoolEventShopcart> lstCanteenShopCart = _repo.GetCart(schoolId, parentId, eventId);
            List<SchoolEventCartProduct> lstCartProduct = new List<SchoolEventCartProduct>();

            Dictionary<int?, string> students;
            students = new Dictionary<int?, string>();

            foreach (SchoolEventShopcart cart in lstCanteenShopCart)
            {
                cart.extra3 = "EVENT"; //TODO NAME
                decimal productsPrice = 0;
                decimal optionsPrice = 0;
                cart.options = new List<EventOption>();
                if (!string.IsNullOrEmpty(cart.productOptions))
                {
                    string[] strOptionIds = cart.productOptions.Split('|');
                    int[] optionIds = Array.ConvertAll(strOptionIds, int.Parse);
                    IQueryable<EventOption> lstOptions = _repo.GetOptions(optionIds, schoolId);
                    cart.options.AddRange(lstOptions.ToList<EventOption>());
                    optionsPrice = System.Convert.ToDecimal(lstOptions.Sum(x => x.optionPrice.Value));

                }
                productsPrice = productsPrice + ((cart.productPrice.Value + optionsPrice) * cart.productQuantity.Value);
                cart.productPrice = productsPrice;

                //prepare cart
                if (lstCartProduct.Count > 0)
                {
                    if (lstCartProduct.Where(e => e.studentId == cart.studentId).Count() > 0)
                    {
                        lstCartProduct.Where(e => e.studentId == cart.studentId).FirstOrDefault().canteenShopcarts.Add(cart);
                    }
                    else
                    {
                        SchoolEventCartProduct finalCart = new SchoolEventCartProduct
                        {
                            studentId = cart.studentId.Value,
                            //TODO Student names can be get from the cache
                            studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                        };
                        finalCart.canteenShopcarts = new List<SchoolEventShopcart>();
                        finalCart.canteenShopcarts.Add(cart);
                        lstCartProduct.Add(finalCart);
                    }
                }

                else
                {
                    SchoolEventCartProduct finalCart = new SchoolEventCartProduct
                    {
                        studentId = cart.studentId.Value,
                        studentName = _studentRepo.GetStudentName(cart.studentId.Value, schoolId)
                    };
                    finalCart.canteenShopcarts = new List<SchoolEventShopcart>();
                    finalCart.canteenShopcarts.Add(cart);
                    lstCartProduct.Add(finalCart);
                }
            }
            return lstCartProduct;
        }

        public bool UpdateCart(EventCartUpdate removeCart, int schoolId, int parentId)
        {
            switch (removeCart.status)
            {
                case RemoveCartStatus.CART:
                    _repo.EmptyCart(parentId, schoolId, removeCart.eventId);
                    break;
                case RemoveCartStatus.PRODUCT:
                    _repo.RemoveCartItem(removeCart.cartId, parentId, schoolId);
                    break;
                case RemoveCartStatus.QUANTITY:
                    _repo.ReduceCartItemQuantity(removeCart.cartId, parentId, schoolId);
                    break;
            }
            return true;
        }

        public AddToCartResult AddItemToCart(EventAddToCart cart, int schoolId, int parentId)
        {
            AddToCartResult result = new AddToCartResult();
            List<AddToCartView> lst = new List<AddToCartView>();
            bool isQuanityExceeded = false;
            bool isPriceZeroError = false;
            List<ZeroPriceProducts> lstZero = new List<ZeroPriceProducts>();

            decimal optionPrice = 0;
            if (!String.IsNullOrEmpty(cart.optionIds))
            {
                int[] intOptionIds = Array.ConvertAll(cart.optionIds.Split('|'), int.Parse);
                optionPrice = _repo.GetAllOptionsPrice(schoolId, intOptionIds);
                //TODO: Take options info from cache
            }

            SchoolEventProduct product = _repo.GetProduct(schoolId, cart.productId); //TODO: Get product from cache

            List<SchoolEventShopcart> addCartItemsList = new List<SchoolEventShopcart>();
            List<SchoolEventShopcart> updateCartItemsList = new List<SchoolEventShopcart>();
            if (product.productPrice + optionPrice == 0)
            {
                //TODO: check for the better option for products with zero price
                //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.OK, "Product price zero not allowed"));
                lstZero.Add(new ZeroPriceProducts
                {
                    productId = product.id,
                    productName = product.productName
                });
                isPriceZeroError = true;
            }
            else
            {
                foreach (int studentId in cart.studentIds)
                {
                    //add to cart
                    //2. Fetch product information
                    //3. if options are not empty then calculate option price 
                    //4. If product price + options price <=0 return with error
                    //5. Check if product exist into cart
                    // yes - increase quantity with quantity limit check 
                    //no - add new entry to c_shopcart table
                    SchoolEventShopcart cShopCart = _repo.GetCartItemWithOption(schoolId, parentId, cart.productId, cart.optionIds, studentId, cart.eventId);

                    if (cShopCart == null)
                    {
                        cShopCart = new SchoolEventShopcart();
                        cShopCart.parentId = parentId;
                        cShopCart.studentId = studentId;
                        cShopCart.schoolId = schoolId;
                        cShopCart.productId = cart.productId;
                        cShopCart.productOptions = cart.optionIds; //options
                        cShopCart.extra3 = "SPECIAL";
                        cShopCart.productQuantity = cart.quantity;
                        cShopCart.cartDate = DateTime.Now;
                        cShopCart.eventId = cart.eventId;

                        cShopCart.productPrice = product.productPrice;
                        cShopCart.productName = product.productName;

                        if (product.quantityLimit >= cart.quantity)
                        {
                            addCartItemsList.Add(cShopCart);//_cartRepo.AddItemToCart(cShopCart);//context.CanteenShopCart.Add(cShopCart);
                        }
                        else
                        {
                            lst.Add(new AddToCartView
                            {
                                productId = product.id,
                                productName = product.productName,
                                maximumQuantityAllowed = product.quantityLimit.Value,
                                exceededQuantity = product.quantityLimit.Value - cart.quantity,
                                studentName = _studentRepo.GetStudentName(studentId, schoolId)

                            });
                            isQuanityExceeded = true;
                        }
                    }
                    else
                    {
                        //update cart qty
                        if (product.quantityLimit >= cShopCart.productQuantity + cart.quantity)
                        {
                            cShopCart.productQuantity = cShopCart.productQuantity + cart.quantity;
                            updateCartItemsList.Add(cShopCart);
                        }

                        else if (product.quantityLimit < cShopCart.productQuantity + cart.quantity)
                        {
                            lst.Add(new AddToCartView
                            {
                                productId = product.id,
                                productName = product.productName,
                                maximumQuantityAllowed = product.quantityLimit.Value,
                                exceededQuantity = (cShopCart.productQuantity.Value + cart.quantity) - product.quantityLimit.Value,
                                studentName = _studentRepo.GetStudentName(studentId, schoolId)
                            });
                            isQuanityExceeded = true;
                        }
                    }


                }//foreach
            }
            _repo.AddItemsToCart(addCartItemsList);
            _repo.UpdateCartItems(updateCartItemsList);

            result.isPriceZeroError = isPriceZeroError;
            result.isQuanityExceeded = isQuanityExceeded;
            result.priceZeroProducts = lstZero;
            result.quantityExceedProducts = lst;
            result.cartCount = _repo.GetCartItemsCount(parentId, schoolId, cart.eventId);
            return result;
        }

        public List<SchoolEventShopcart> GetAllCartItems(int parentId, int schoolId, int eventId)
        {
            return _repo.GetCart(schoolId, parentId, eventId);
        }

        public decimal GetCartAmout(List<SchoolEventShopcart> cartList, int schoolId)
        {
            decimal productsPrice = 0;
            cartList.ForEach(cartItem =>
            {
                decimal allOptionsPrice = 0;
                if (!string.IsNullOrEmpty(cartItem.productOptions))
                {
                    int[] intOptionIds = Array.ConvertAll(cartItem.productOptions.Split('|'), int.Parse);
                    decimal optionPrice = _repo.GetAllOptionsPrice(schoolId, intOptionIds);
                    allOptionsPrice = allOptionsPrice + optionPrice;
                }
                productsPrice = productsPrice + ((cartItem.productPrice.Value + allOptionsPrice) * cartItem.productQuantity.Value);
            }
            );

            return productsPrice;
        }

        public OrderResult PlaceOrder(List<SchoolEventShopcart> allCartItems, List<EventOrder> orders, DateTime deliveryDate,
                                      int eventId, decimal chargesPerOrder,
                                      Parent parent, SchoolInfo schoolSetup,
                                      bool isCreditOrder, string transactionId, decimal school24Fee, bool isBalancePlusCard)
        {
            var allTransactions = new List<Transaction>();
            OrderResult result = new OrderResult();
            decimal grandTotal = 0;
            List<int> orderIds = new List<int>();
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    if (!_repo.DeductProductStock(allCartItems, schoolSetup.schoolid))
                    {
                        //dbContextTransaction.Rollback();
                        throw new PlaceOrderException(PRODUCTSOUTOFSTOCK);
                    }

                    foreach (EventOrder order in orders)
                    {
                        //decimal grandTotal = 0;
                        int numberOfMeals = 0;
                        List<SchoolEventShopcart> cartItems = allCartItems.Where(cartItem =>
                              cartItem.studentId == order.studentId
                        ).ToList();

                        string orderNumber = Guid.NewGuid().ToString();

                        string bag = schoolSetup.chargeforbag == "YES" ? "BUY" : "NA";
                        string orderType = string.Empty;

                        decimal orderAmount = GetCartAmout(cartItems, schoolSetup.schoolid);
                        grandTotal = grandTotal + orderAmount + chargesPerOrder;
                        orderType = "SPECIAL";

                        int orderPaymentMethod = 1;
                        if (isCreditOrder)
                        {
                            orderPaymentMethod = 2;
                        }

                        if (isBalancePlusCard)
                        {
                            orderPaymentMethod = 3;
                        }

                        SchoolOrders schoolOrder = new SchoolOrders();

                        schoolOrder.OrderAmount = orderAmount;
                        schoolOrder.OrderCancel = false;
                        schoolOrder.CustomerId = order.studentId;
                        schoolOrder.OrderComment = order.comment;
                        schoolOrder.OrderDate = cartItems.FirstOrDefault().cartDate;
                        schoolOrder.OrderReason = "Normal";
                        schoolOrder.OrderPaymentMethod = orderPaymentMethod;
                        schoolOrder.OrderShippedDate = deliveryDate;
                        schoolOrder.OrderShippingMethod = eventId;
                        schoolOrder.OrderType = "SPECIAL";
                        schoolOrder.ParentId = parent.id;
                        schoolOrder.OrderNumber = schoolOrder.ParentId.ToString() + DateTime.Now.Second + DateTime.Now.Hour;
                        schoolOrder.SchoolId = schoolSetup.schoolid;


                        _repo.AddSchoolOrder(schoolOrder);
                        orderIds.Add(schoolOrder.Id);

                        string orderNumberSpecial = "S" + schoolOrder.Id;

                        EventOrders eventOrders = new EventOrders();
                        eventOrders.Bag = "NONE";
                        eventOrders.BagPrice = 0;
                        eventOrders.CustomerId = schoolOrder.CustomerId;
                        eventOrders.OrderAmount = schoolOrder.OrderAmount;
                        eventOrders.OrderComment = schoolOrder.OrderComment;
                        eventOrders.OrderDate = schoolOrder.OrderDate;
                        eventOrders.OrderNumber = "S" + schoolOrder.Id;
                        eventOrders.OrderReason = "Normal";
                        eventOrders.OrderShippedDate = schoolOrder.OrderShippedDate;
                        eventOrders.OrderShippingMethod = eventId;
                        eventOrders.SchoolId = schoolOrder.SchoolId;
                        eventOrders.OrderType = "SPECIAL";
                        eventOrders.OrderPaymentMethod = orderPaymentMethod;

                        _repo.AddEventOrders(eventOrders);

                        int eventOrderItemNumber = 0;
                        List<EventOrderItems> eventOrderItems = new List<EventOrderItems>();
                        //Insert items in e_oitems table from e_shopcart table.
                        foreach (SchoolEventShopcart shopcartItem in cartItems)
                        {
                            EventOrderItems eventOrderItem = new EventOrderItems();
                            eventOrderItem.OrderId = eventOrders.Id;
                            eventOrderItem.OrderIntemNumber = eventOrderItemNumber;
                            eventOrderItem.CatalogId = shopcartItem.productId;
                            eventOrderItem.Options = shopcartItem.productOptions; //NEED TO CONFIRM, NOT SURE ABOUT THIS FIELD.
                            eventOrderItem.NumItems = Int16.Parse(shopcartItem.productQuantity.ToString());
                            eventOrderItem.SchoolId = schoolSetup.schoolid;
                            eventOrderItem.Extra2 = shopcartItem.extra3;
                            eventOrderItem.AdultsCount = shopcartItem.adultsCount;
                            eventOrderItem.ChildrenCount = shopcartItem.childrenCount;
                            eventOrderItem.AdditionalInputs = shopcartItem.additionalInputs;
                            eventOrderItem.StdId = shopcartItem.studentId;
                            eventOrderItem.SeatNumber = shopcartItem.seatNumber;
                            eventOrderItem.EventId = eventId;

                            eventOrderItemNumber += 1;
                            eventOrderItems.Add(eventOrderItem);
                        }

                        _repo.InsertEventOrderItems(eventOrderItems);
                        decimal bankFee = GetBankFee(orderAmount);
                        EventTransactions eventTransactions = new EventTransactions();
                        //pid, schoolid,tamount,ttype,extra1,txn_id,tdate,recycleprice,bank_fee,eventid,orderid,calculated_paypalfee, servicefee
                        eventTransactions.ParentId = parent.id;
                        eventTransactions.SchoolId = schoolSetup.schoolid;
                        eventTransactions.TransactionAmount = schoolOrder.OrderAmount;
                        eventTransactions.TransactionType = isCreditOrder ? "ORDER:DEBIT:EVENT" : "PayAtCheckout";
                        eventTransactions.Extra1 = schoolOrder.Id.ToString();
                        eventTransactions.TransactionId = transactionId;
                        eventTransactions.TransactionDate = schoolOrder.OrderDate;
                        eventTransactions.RecyclePrice = 0;
                        eventTransactions.EventId = eventId;
                        eventTransactions.ServiceFee = school24Fee;
                        eventTransactions.CalculatedPaypalFee = bankFee;
                        eventTransactions.BankFee = bankFee;
                        eventTransactions.PaypalTransactionId = transactionId;
                        eventTransactions.OrderID = schoolOrder.Id;

                        _repo.InsertEventTransaction(eventTransactions);


                        //Create record in Transactions
                        if (isCreditOrder)
                        {
                            Transaction transaction = new Transaction();
                            transaction.parentId = parent.id;
                            transaction.schoolId = schoolSetup.schoolid;
                            transaction.transactionAmount = orderAmount;
                            transaction.transactionType = isCreditOrder ? "ORDER:DEBIT" : "PayAtCheckout";
                            transaction.extra1 = schoolOrder.Id.ToString();
                            transaction.balanceBefore = parent.credit;
                            transaction.balanceAfter = isCreditOrder ? parent.credit - orderAmount : parent.credit;
                            transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                            transaction.studentId = order.studentId;
                            transaction.transactionId = transactionId;
                            transaction.recyclePrice = 0;//specialEvent.BagPrice;
                            _transactionRepo.AddTransaction(transaction);
                        }
                        if (isBalancePlusCard)
                        {
                            Transaction transaction = new Transaction();
                            transaction.parentId = parent.id;
                            transaction.schoolId = schoolSetup.schoolid;
                            transaction.transactionAmount = orderAmount;
                            transaction.extra1 = schoolOrder.Id.ToString();
                            transaction.balanceBefore = parent.credit;
                            transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                            transaction.studentId = order.studentId;
                            transaction.transactionId = transactionId;
                            transaction.recyclePrice = 0;//specialEvent.BagPrice;

                            transaction.transactionType = "ORDER:DEBIT:PayAtCheckout";
                            transaction.balanceAfter = 0;
                            allTransactions.Add(transaction);
                        }

                    }//foreach

                    //Update credit
                    if (isBalancePlusCard)
                    {
                        var afterBalance = parent.credit - grandTotal;
                        allTransactions.ForEach(t =>
                        {
                            t.balanceAfter = 0;
                            t.balanceBefore = parent.credit;
                            t.balanceDeductedAmount = parent.credit;
                            t.cardPaymentAmount = grandTotal - parent.credit;
                        });
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, parent.credit.Value);
                    }
                    else if (isCreditOrder)
                    {
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, grandTotal);
                    }
                    _transactionRepo.AddTransactions(allTransactions);
                    _repo.EmptyCart(parent.id, schoolSetup.schoolid, eventId);

                    dbContextTransaction.Commit();
                    result.isOrderSuccess = true;
                    result.message = "Order placed successfully";
                    result.orderIds = orderIds;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    /*result.isOrderSuccess = false;
                    result.message = "Error while placing order. Please try again";
                    result.orderIds = null;*/
                    throw e;
                }
            }
            //TODO: send email
            //try
            //{
            if (result.orderIds != null)
            {
                result.orderIds.ForEach(order =>
                {
                    _emailService.SendSchoolEventEmail(parent.email, order, parent.id, schoolSetup.schoolid, parent.firstName + " " + parent.lastName);
                });
            }
            //}
            //catch (Exception)
            //{

            //}

            return result;
        }

        private decimal GetBankFee(decimal orderAmount) //Temporary method: TODO - Update this from log
        {
            decimal stripeFee = 1.5M;
            decimal BankFee = (orderAmount * stripeFee) / 100 + 0.30M;
            return BankFee;

        }

        public OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId)
        {
            OrderCancelResult result = new OrderCancelResult();
            //1. check if order exists
            if (!_repo.IsOrderExists(orderId, parentId, schoolId))
            {
                //return with error
                result.isOrderCancelled = false;
                result.message = "Order does not exists! ";
                return result;
            }

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            var parentsOrder = _orderhistoryRepo.GetSchoolOrder(schoolId, orderId);

            if (parentsOrder == null || parentsOrder.Id == 0)
            {
                result.isOrderCancelled = false;
                result.message = "Order is missing. Please contact admin.";
                return result;
            }

            TimezoneSettings zoneSettings = new TimezoneSettings();
            var canteenEvent = GetSchoolEvent(schoolId, parentsOrder.OrderShippingMethod.Value);
            if (canteenEvent == null)
            {
                //return error
                result.isOrderCancelled = false;
                result.message = "Event not found. Please contact admin. ";
                return result;
            }

            DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
            if (nowInSchoolTimezone > canteenEvent.eventDateEnd)
            {
                //return error
                result.isOrderCancelled = false;
                result.message = "You cannot cancel order after cutoff. Please contact admin. ";
                return result;
            }

            string orderNumber = parentsOrder.OrderNumber;
            string orderNumberSpecial = "S" + parentsOrder.Id;
            EventOrders orderToDelete = _repo.GetEventOrders(orderNumberSpecial);
            // CanteenMigration.Models.Orders orderToDelete = _orderRepo.GetOrderByOrderNumber(orderNumberSpecial, schoolId);
            //3. MealWise cancellation
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    List<EventOrders> ordersToDelete = new List<EventOrders>();//from orders table
                    if (!string.IsNullOrEmpty(orderNumber))
                    {
                        //perform lunch cancellation
                        //3.1 insert into orders_deleted table from orders - copy_order()
                        SchoolOrdersDeleted ordersDeleted = new SchoolOrdersDeleted();
                        ordersDeleted.CustomerId = parentsOrder.CustomerId;
                        ordersDeleted.SchoolId = schoolId;
                        ordersDeleted.OrderDate = parentsOrder.OrderDate;
                        ordersDeleted.OrderNumber = orderNumber;
                        ordersDeleted.OrderComment = parentsOrder.OrderComment;
                        ordersDeleted.OrderAmount = parentsOrder.OrderAmount;
                        ordersDeleted.OrderShippedDate = parentsOrder.OrderShippedDate;
                        ordersDeleted.OrderShippingMethod = parentsOrder.OrderShippingMethod;
                        ordersDeleted.OrderType = parentsOrder.OrderType;
                        ordersDeleted.Bag = parentsOrder.Bag;
                        ordersDeleted.BagPrice = parentsOrder.BagPrice;
                        ordersDeleted.OrderPaymentMethod = parentsOrder.OrderPaymentMethod;
                        ordersDeleted.OrderReason = parentsOrder.OrderReason;
                        //ordersDeleted.OldOrderId = orderId;

                        //3.2 insert oitems to - oitems_deleted and delete etries from oitems
                        List<EventOrderItems> orderItems = _repo.GetEventOrderItems(schoolId, orderToDelete.Id);
                        // List<OrderItems> orderItems = _orderRepo.GetOrderItems(orderToDelete.id, schoolId);
                        List<EventOrderItemsDeleted> orderItemsDeleted = new List<EventOrderItemsDeleted>();
                        foreach (EventOrderItems orderItem in orderItems)
                        {
                            EventOrderItemsDeleted orderItemDeleted = new EventOrderItemsDeleted();
                            orderItemDeleted.OrderId = orderItem.Id;
                            orderItemDeleted.OrderIntemNumber = orderItem.OrderIntemNumber;
                            orderItemDeleted.CatalogId = orderItem.CatalogId;
                            orderItemDeleted.Options = orderItem.Options; //NEED TO CONFIRM, NOT SURE ABOUT THIS FIELD.
                            orderItemDeleted.NumItems = orderItem.NumItems;
                            orderItemDeleted.SchoolId = orderItem.SchoolId;
                            orderItemDeleted.Extra2 = orderItem.Extra2;
                            orderItemDeleted.AdultsCount = orderItem.AdultsCount;
                            orderItemDeleted.ChildrenCount = orderItem.ChildrenCount;
                            orderItemDeleted.AdditionalInputs = orderItem.AdditionalInputs;
                            orderItemDeleted.StdId = orderItem.StdId;
                            orderItemDeleted.SeatNumber = orderItem.SeatNumber;
                            orderItemDeleted.EventId = orderItem.EventId;

                            orderItemsDeleted.Add(orderItemDeleted);
                            _repo.AddStockToProduct(orderItem.CatalogId.Value, orderItem.NumItems.Value, schoolId);
                        }
                        _repo.AddSchoolOrderDeleted(ordersDeleted);
                        _repo.InsertEventOrderItemsDeleted(orderItemsDeleted);

                        //3.3 delete from orders table
                        ordersToDelete.Add(orderToDelete);
                        _repo.RemoveEventOrderItems(orderItems);
                    }

                    //4. update parents_orders table cancel order - cancel_big_order() 
                    _repo.CanceOrder(parentsOrder);

                    _repo.DeleteEventOrders(ordersToDelete);

                    decimal parentCredit = _parentRepo.GetParentCredit(parentId, schoolId);
                    //5. update parent credit
                    _parentRepo.AddParentCredit(parentId, schoolId, parentsOrder.OrderAmount.Value);

                    Parent parent = _parentRepo.GetParent(schoolId, parentId);
                    //6. insert into Transactions table

                    Transaction transaction = new Transaction();
                    transaction.parentId = parentId;
                    transaction.schoolId = schoolId;
                    transaction.transactionAmount = parentsOrder.OrderAmount;
                    transaction.transactionType = "CANCELLATION:CREDIT";
                    transaction.extra1 = parentsOrder.Id.ToString();
                    transaction.balanceBefore = parentCredit;
                    transaction.balanceAfter = parentCredit + parentsOrder.OrderAmount;
                    transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                    transaction.studentId = parentsOrder.CustomerId;
                    _transactionRepo.AddTransaction(transaction);

                    result.isOrderCancelled = true;
                    result.message = "Order cancelled successfully";
                    result.parentCredit = parentCredit + parentsOrder.OrderAmount;
                    dbContextTransaction.Commit();
                    _emailService.SendCancelOrderEmail(parent.email, parentCredit, parent.credit.Value, parentsOrder.OrderAmount.Value, parent.firstName + " " + parent.lastName, parentsOrder.Id.ToString());
                    return result;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    result.isOrderCancelled = false;
                    result.message = "Error while cancelling order. Please try again";
                    return result;
                }
            }
        }



        public void CheckProductStock(List<SchoolEventShopcart> cartItems)
        {
            List<int> productIds = cartItems.Select(item => item.productId.Value).Distinct().ToList();
            var latestProducts = _repo.GetSchoolEventProducts(productIds);
            productIds.ForEach(productId =>
            {
                int cartProductCount = cartItems.Where(e => e.productId.Value == productId).Sum(e => e.productQuantity).Value;
                var product = latestProducts.Where(e => e.id == productId).FirstOrDefault();

                if (cartProductCount > product.stock)
                {
                    throw new PlaceOrderException(
                        "Product " + product.productName +
                        " went out of stock. " +
                        "Available stock is " +
                        product.stock + ". " +
                        "Your quantity is " + cartProductCount);
                }
            });
        }
    }
}
