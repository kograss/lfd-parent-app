﻿using DataDependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;

namespace CanteenMigration.Services.impl
{
    public class PublicHolidaysService: IPublicHolidaysService
    {
        IPublicHolidaysRepository _publicHolidaysRepo;
        public PublicHolidaysService(IPublicHolidaysRepository publicHolidaysRepo)
        {
            this._publicHolidaysRepo = publicHolidaysRepo;
        }

        public List<PublicHolidays> GetPublicHolidays(int schoolId, int year)
        {
            try
            {
                List<PublicHolidays> holidays = _publicHolidaysRepo.GetPublicHolidays(schoolId, year);
                if(!holidays.Equals(null))
                {
                    return holidays;
                }

                else
                {
                    return null;
                }
            }   
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
