﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;
using CanteenMigration.Models;
using CanteenMigration.Commons;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using System.Diagnostics;
using CanteenMigration.BusinessDependencies;

namespace CanteenMigration.Services.impl
{
    public class OrderService : IOrderService
    {
        public enum ORDER_TYPE { LUNCH, RECESS, THIRD, LR, LT, LRT, RT }

        IProductRepository _productRepository;
        IParentRepository _parentRepo;
        ISchoolRepository _schoolRepo;
        ICartRepository _cartRepo;
        //IOptionRepository _optionsRepo;
        IOrderRepository _orderRepo;
        IDBContext _context;
        IPublicHolidaysRepository _publicHolidaysRepo;
        IClosuresRepository _clousuresRepository;
        ICartService cartService;
        ITransactionRepository _transactionRepo;
        IEmailService _emailService;
        IPlaceOrderService _placeOrderService;
        public ILoggerService<OrderService> _loggingService;
        ISchoolService _schoolService;
        public OrderService(IParentRepository _parentRepo, ISchoolRepository _schoolRepo, ICartRepository _cartRepo,
                           IOrderRepository _orderRepo, IDBContext _context,
                            IPublicHolidaysRepository _publicHolidaysRepo, IClosuresRepository _clousuresRepository,
                            ICartService cartService, ITransactionRepository _transactionRepo, IEmailService _emailService,
                            IProductRepository productRepository, IPlaceOrderService _placeOrderService, 
                            ILoggerService<OrderService> loggingService, ISchoolService schoolService)
        {
            this._parentRepo = _parentRepo;
            this._schoolRepo = _schoolRepo;
            this._cartRepo = _cartRepo;
            //this._optionsRepo = _optionsRepo;
            this._orderRepo = _orderRepo;
            this._context = _context;
            this._publicHolidaysRepo = _publicHolidaysRepo;
            this._clousuresRepository = _clousuresRepository;
            this.cartService = cartService;
            this._transactionRepo = _transactionRepo;
            this._emailService = _emailService;
            this._productRepository = productRepository;
            this._placeOrderService = _placeOrderService;
            this._loggingService = loggingService;
            _schoolService = schoolService;
        }


        private ORDER_TYPE GetOrderType(int lunchCount, int recessCount, int thirdCount)
        {
            ORDER_TYPE orderType = ORDER_TYPE.LUNCH;
            if (lunchCount > 0)
            {
                orderType = ORDER_TYPE.LUNCH;
                if (recessCount > 0)
                {
                    orderType = ORDER_TYPE.LR;
                    if (thirdCount > 0)
                    {
                        orderType = ORDER_TYPE.LRT;
                    }
                }
                else if (thirdCount > 0)
                {
                    orderType = ORDER_TYPE.LT;
                }
            }

            else if (recessCount > 0)
            {
                orderType = ORDER_TYPE.RECESS;
                if (thirdCount > 0)
                {
                    orderType = ORDER_TYPE.RT;
                }
            }

            else if (thirdCount > 0)
            {
                orderType = ORDER_TYPE.THIRD;
            }

            return orderType;
        }

      
         public OrderResult PlaceOrder(List<Order> orders, DateTime deliveryDate, SchoolInfo schoolSetup, Parent parent, bool isCreditOrder,
             string transactionId, List<CanteenShopcartItem> allCartItems, decimal bagCharges, decimal chargesPerOrder, bool isBalancePlusCard)
        {
            OrderResult result = new OrderResult();          
            var watch = System.Diagnostics.Stopwatch.StartNew();
            // the code that you want to measure comes here

            result = _placeOrderService.PlaceOrder(allCartItems, orders, schoolSetup,
                bagCharges, chargesPerOrder, deliveryDate,
             parent, isCreditOrder, transactionId,
               _context, _productRepository, _cartRepo,
              cartService, _orderRepo, _transactionRepo, _parentRepo, isBalancePlusCard);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;


            _loggingService.Info("Elapsed Seconds are " + elapsedMs);
            //Debug.WriteLine("Elapsed Seconds are " + elapsedMs);

            if (result.orderIds != null)
            {
                result.orderIds.ForEach(order =>
                {
                    _loggingService.Info("sending email with orderid" + order);
                    //_emailService.AddEmail(parent.email, order, parent.id, schoolSetup.schoolid, parent.firstName + " " + parent.lastName);
                    _emailService.SendAsynEmail(parent.email, order, parent.id, schoolSetup.schoolid, parent.firstName + " " + parent.lastName);
                });
                //save at a time after loop


            }


            return result;
        }

        public OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId)
        {
            OrderCancelResult result = new OrderCancelResult();
            //1. check if order exists
            if (!_orderRepo.IsOrderExists(orderId, parentId, schoolId))
            {
                //return with error
                result.isOrderCancelled = false;
                result.message = "Order does not exists! ";
                return result;
            }

            //2. Check cutoff
            /*SchoolInfo schoolInfo = new SchoolInfo();
            string schoolInfoJson = _schoolRepo.GetSchoolInfo(schoolId);
            if (!string.IsNullOrEmpty(schoolInfoJson))
            {
                schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJson);
            }*/

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);

            ParentsOrders parentsOrder = _orderRepo.GetParentOrder(orderId, schoolId);

            TimezoneSettings zoneSettings = new TimezoneSettings();
            DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);
            if (IsAfterCuttOff(schoolId, nowInSchoolTimezone, schoolInfo.cut_days_before, schoolInfo.daily_hour, schoolInfo.daily_min, parentsOrder.orderShippedDate.Value))
            {
                //return error
                result.isOrderCancelled = false;
                result.message = "You cannot cancel order after cutoff. Please contact admin. ";
                return result;
            }


            int? lunchOrderNumber = _orderRepo.GetMealWiseOrderNumber("0" + orderId, parentsOrder.studentId.Value, schoolId);
            int? recessOrderNumber = _orderRepo.GetMealWiseOrderNumber("R" + orderId, parentsOrder.studentId.Value, schoolId);
            int? thirdOrderNumber = _orderRepo.GetMealWiseOrderNumber("T" + orderId, parentsOrder.studentId.Value, schoolId);

            //3. MealWise cancellation
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    List<Orders> ordersToDelete = new List<Orders>();//from orders table
                    if (lunchOrderNumber != null && lunchOrderNumber != 0)
                    {
                        //perform lunch cancellation
                        //3.1 insert into orders_deleted table from orders - copy_order()
                        OrdersDeleted ordersDeleted = new OrdersDeleted();
                        ordersDeleted.studentId = parentsOrder.studentId;
                        ordersDeleted.schoolId = schoolId;
                        ordersDeleted.orderDate = parentsOrder.orderDate;
                        ordersDeleted.orderNumber = "0" + orderId;
                        ordersDeleted.orderComment = parentsOrder.orderComment;
                        ordersDeleted.orderAmount = parentsOrder.orderAmount;
                        ordersDeleted.orderShippedDate = parentsOrder.orderShippedDate;
                        ordersDeleted.orderShipMethod = parentsOrder.orderShipMethod;
                        ordersDeleted.orderType = parentsOrder.orderType;
                        ordersDeleted.bag = parentsOrder.bag;
                        ordersDeleted.bagPrice = parentsOrder.bagPrice;
                        ordersDeleted.oldOrderId = lunchOrderNumber;

                        //3.2 insert oitems to - oitems_deleted and delete etries from oitems
                        List<OrderItems> orderItems = _orderRepo.GetOrderItems(lunchOrderNumber.Value, schoolId);
                        List<OrderItemsDeleted> orderItemsDeleted = new List<OrderItemsDeleted>();
                        foreach (OrderItems orderItem in orderItems)
                        {
                            OrderItemsDeleted orderItemDeleted = new OrderItemsDeleted();
                            orderItemDeleted.orderID = orderItem.orderID;
                            orderItemDeleted.orderIntemNumber = orderItem.orderIntemNumber;
                            orderItemDeleted.productId = orderItem.productId;
                            orderItemDeleted.numerOfItems = orderItem.numerOfItems;
                            orderItemDeleted.options = orderItem.options;
                            orderItemDeleted.schoolId = schoolId;
                            orderItemDeleted.extra2 = orderItem.extra2;

                            orderItemsDeleted.Add(orderItemDeleted);
                            _productRepository.AddStockToProduct(orderItem.productId.Value, orderItem.numerOfItems, schoolId);
                        }

                        _orderRepo.AddToDeletedOrder(ordersDeleted);
                        _orderRepo.AddOrderItemsDeleted(orderItemsDeleted);

                        //3.3 delete from orders table
                        Orders order = _orderRepo.GetOrder(lunchOrderNumber.Value, schoolId);
                        ordersToDelete.Add(order);
                        _orderRepo.DeleteOrderItem(orderItems);
                    }

                    if (recessOrderNumber != null && recessOrderNumber != 0)
                    {
                        //perform recess cancellation
                        OrdersDeleted ordersDeleted = new OrdersDeleted();
                        ordersDeleted.studentId = parentsOrder.studentId;
                        ordersDeleted.schoolId = schoolId;
                        ordersDeleted.orderDate = parentsOrder.orderDate;
                        ordersDeleted.orderNumber = "R" + orderId;
                        ordersDeleted.orderComment = parentsOrder.orderComment;
                        ordersDeleted.orderAmount = parentsOrder.orderAmount;
                        ordersDeleted.orderShippedDate = parentsOrder.orderShippedDate;
                        ordersDeleted.orderShipMethod = parentsOrder.orderShipMethod;
                        ordersDeleted.orderType = parentsOrder.orderType;
                        ordersDeleted.bag = parentsOrder.bag;
                        ordersDeleted.bagPrice = parentsOrder.bagPrice;
                        ordersDeleted.oldOrderId = recessOrderNumber;


                        List<OrderItemRecess> orderItems = _orderRepo.GetOrderItemsRecess(recessOrderNumber.Value, schoolId);
                        List<OrderItemRecessDeleted> orderItemsDeleted = new List<OrderItemRecessDeleted>();
                        foreach (OrderItemRecess orderItem in orderItems)
                        {
                            OrderItemRecessDeleted orderItemDeleted = new OrderItemRecessDeleted();
                            orderItemDeleted.orderId = orderItem.orderId;
                            orderItemDeleted.orderIntemNumber = orderItem.orderIntemNumber;
                            orderItemDeleted.productId = orderItem.productId;
                            orderItemDeleted.numerOfItems = orderItem.numerOfItems;
                            orderItemDeleted.options = orderItem.options;
                            orderItemDeleted.schoolId = schoolId;
                            orderItemDeleted.extra2 = orderItem.extra2;

                            orderItemsDeleted.Add(orderItemDeleted);
                            _productRepository.AddStockToProduct(orderItem.productId.Value, orderItem.numerOfItems, schoolId);
                        }

                        _orderRepo.AddToDeletedOrder(ordersDeleted);
                        _orderRepo.AddOrderItemsRecessDeleted(orderItemsDeleted);

                        //3.3 delete from orders table
                        Orders order = _orderRepo.GetOrder(recessOrderNumber.Value, schoolId);
                        ordersToDelete.Add(order);
                        _orderRepo.DeleteOrderItemsRecess(orderItems);
                    }

                    if (thirdOrderNumber != null && thirdOrderNumber != 0)
                    {
                        //perform third cancellation
                        OrdersDeleted ordersDeleted = new OrdersDeleted();
                        ordersDeleted.studentId = parentsOrder.studentId;
                        ordersDeleted.schoolId = schoolId;
                        ordersDeleted.orderDate = parentsOrder.orderDate;
                        ordersDeleted.orderNumber = "T" + orderId;
                        ordersDeleted.orderComment = parentsOrder.orderComment;
                        ordersDeleted.orderAmount = parentsOrder.orderAmount;
                        ordersDeleted.orderShippedDate = parentsOrder.orderShippedDate;
                        ordersDeleted.orderShipMethod = parentsOrder.orderShipMethod;
                        ordersDeleted.orderType = parentsOrder.orderType;
                        ordersDeleted.bag = parentsOrder.bag;
                        ordersDeleted.bagPrice = parentsOrder.bagPrice;
                        ordersDeleted.oldOrderId = thirdOrderNumber;

                        List<OrderItemRecess> orderItems = _orderRepo.GetOrderItemsRecess(thirdOrderNumber.Value, schoolId);
                        List<OrderItemRecessDeleted> orderItemsDeleted = new List<OrderItemRecessDeleted>();
                        foreach (OrderItemRecess orderItem in orderItems)
                        {
                            OrderItemRecessDeleted orderItemDeleted = new OrderItemRecessDeleted();
                            orderItemDeleted.orderId = orderItem.orderId;
                            orderItemDeleted.orderIntemNumber = orderItem.orderIntemNumber;
                            orderItemDeleted.productId = orderItem.productId;
                            orderItemDeleted.numerOfItems = orderItem.numerOfItems;
                            orderItemDeleted.options = orderItem.options;
                            orderItemDeleted.schoolId = schoolId;
                            orderItemDeleted.extra2 = orderItem.extra2;

                            orderItemsDeleted.Add(orderItemDeleted);
                            _productRepository.AddStockToProduct(orderItem.productId.Value, orderItem.numerOfItems, schoolId);
                        }

                        _orderRepo.AddToDeletedOrder(ordersDeleted);
                        //TODO: NOT REQUIRED FOR THIRD BREAK
                        _orderRepo.AddOrderItemsRecessDeleted(orderItemsDeleted);

                        //3.3 delete from orders table
                        Orders order = _orderRepo.GetOrder(thirdOrderNumber.Value, schoolId);
                        ordersToDelete.Add(order);
                        _orderRepo.DeleteOrderItemsRecess(orderItems);
                    }

                    //4. update parents_orders table cancel order - cancel_big_order() 
                    _orderRepo.CancelParentsOrder(parentsOrder);

                    _orderRepo.DeleteOrders(ordersToDelete, schoolId);

                    decimal parentCredit = _parentRepo.GetParentCredit(parentId, schoolId);
                    //5. update parent credit
                    _parentRepo.AddParentCredit(parentId, schoolId, parentsOrder.orderAmount.Value);

                    Parent parent = _parentRepo.GetParent(schoolId, parentId);
                    //6. insert into Transactions table

                    Transaction transaction = new Transaction();
                    transaction.parentId = parentId;
                    transaction.schoolId = schoolId;
                    transaction.transactionAmount = parentsOrder.orderAmount;
                    transaction.transactionType = "CANCELLATION:CREDIT";
                    transaction.extra1 = parentsOrder.orderId.ToString();
                    transaction.balanceBefore = parentCredit;
                    transaction.balanceAfter = parentCredit + parentsOrder.orderAmount;
                    transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                    transaction.studentId = parentsOrder.studentId;
                    _transactionRepo.AddTransaction(transaction);

                    result.isOrderCancelled = true;
                    result.message = "Order cancelled successfully";
                    result.parentCredit = parentCredit + parentsOrder.orderAmount;
                    dbContextTransaction.Commit();
                    _emailService.SendCancelOrderEmail(parent.email, parentCredit, parent.credit.Value, parentsOrder.orderAmount.Value, parent.firstName + " " + parent.lastName, parentsOrder.orderId.ToString());
                    return result;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    result.isOrderCancelled = false;
                    result.message = "Error while cancelling order. Please try again";
                    return result;
                }
            }
        }

        public bool isOrderExists(int orderId, int parentId, int schoolId)
        {
            return _orderRepo.IsOrderExists(orderId, parentId, schoolId);
        }

        private bool IsAfterCuttOff(int schoolId, DateTime nowInSchoolTimezone, int? cutOffDays, int? cutOffHours, int? cutOffMins, DateTime deliveryDate)
        {
            var cutOff = nowInSchoolTimezone.AddDays(cutOffDays == null ? 0 : cutOffDays.Value);
            cutOff = new DateTime(cutOff.Year,
                                  cutOff.Month,
                                  cutOff.Day,
                                  cutOffHours == null ? 0 : cutOffHours.Value,
                                  cutOffMins == null ? 0 : cutOffMins.Value,
                                  0);

            //DateTime deliveryDateWithCutoff= new DateTime(deliveryDate.Year, deliveryDate.Month, deliveryDate.Day, cutOffHours == null ? 0 : cutOffHours.Value,
            //                    cutOffMins == null ? 0 : cutOffMins.Value,0
            //                  );

            deliveryDate = deliveryDate.AddHours(nowInSchoolTimezone.Hour);
            deliveryDate = deliveryDate.AddMinutes(nowInSchoolTimezone.Minute);

            if (deliveryDate.Date == cutOff.Date)
            {
                return deliveryDate > cutOff;
            }
            else if (deliveryDate.Date > cutOff.Date)
            {
                return deliveryDate < cutOff;
            }
            else
            {
                return true;
            }
            //nowInSchoolTimezone = nowInSchoolTimezone.AddDays(cutOffDays == null ? 0 : cutOffDays.Value);

        }

        public string IsOrderCanBePlaced(int schoolId, DateTime deliveryDate)
        {
            //0. iscanteenOpen
            /*string schoolInfoJSON = _schoolRepo.GetSchoolInfo(schoolId);
            if (string.IsNullOrEmpty(schoolInfoJSON))
            {
                return "School not found";
            }

            SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            */
            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return "School not found";
            }

            if (!schoolInfo.canteenopen.Value)
            {
                return "Online ordering is closed";
            }

            if (string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return "Wrong delivery Date";
            }

            //1. clousures
            int dayOfWeekAsPerASP = (int)deliveryDate.DayOfWeek + 1;
            Closures closures = _clousuresRepository.GetClousures(schoolId);
            string weeklyClousures = closures.closeday;

            if (!String.IsNullOrEmpty(weeklyClousures))
            {
                int[] closeDays = Array.ConvertAll(weeklyClousures.Split('|'), int.Parse);
                if (closeDays.Contains(dayOfWeekAsPerASP) && closures != null)
                {
                    return "Canteen is closed on " + deliveryDate.DayOfWeek;
                }
            }

            //2. is holiday
            if (_publicHolidaysRepo.IsDateComingIntoHolidays(schoolId, deliveryDate))
            {
                return "The canteen is closed on " + deliveryDate;
            }

            //3. Previous date
            if (deliveryDate < DateTime.Now)
            {
                return "Wrong Date";
            }

            //4. cutoff
            
            TimezoneSettings zoneSettings = new TimezoneSettings();
            DateTime currentDateTimeInSchoolTimeZone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);


            if (IsAfterCuttOff(schoolId, currentDateTimeInSchoolTimeZone, schoolInfo.cut_days_before, schoolInfo.daily_hour, schoolInfo.daily_min, deliveryDate))
            {
                return "Too late to make an order for today";
            }

            /*if (deliveryDate.Date == currentDateTimeInSchoolTimeZone && currentDateTimeInSchoolTimeZone.Hour > schoolInfo.daily_hour )
            {
                return "Too late to make an order for today";
            }

            if (deliveryDate.Date == currentDateTimeInSchoolTimeZone.Date && currentDateTimeInSchoolTimeZone.Hour == schoolInfo.daily_hour 
                                        && currentDateTimeInSchoolTimeZone.Minute > schoolInfo.daily_min)
            {
                return "Too late to make an order for today";
            }*/

            return "OK";
        }

        public bool SetFavourite(int orderId, int schoolId)
        {
            return _orderRepo.SetFavourite(orderId, schoolId);
        }

        public bool RemoveFavourite(int orderId, int schoolId)
        {
            return _orderRepo.RemoveFavourite(orderId, schoolId);
        }
        /*public void CreateCharge()
        {
            PaymentGatewayTransactionLog log = new PaymentGatewayTransactionLog();
            log.OrderAmount = orderAmount;
            log.CalcOrderTotalCanteenCheckout(context);

            log.Id = PaymentGatewayTransactionLog.GenerateTransactionId("_CanteenCheckout_", "STRIPE");
            log.Status = PaymentGatewayTransactionLog.TRANSACTION_STATE_START;
            log.Log = "[Transaction Start]";
            log.TransactionDate = DateTime.Now;
            log.ParentId = parentId;
            log.SchoolId = schoolId;
            log.Type = PaymentGatewayTransactionLog.TRANSACTION_TYPE_CANTEEN_CHECKOUT;
        }*/
    }
}
