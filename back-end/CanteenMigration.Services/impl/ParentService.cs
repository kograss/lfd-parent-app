﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using DataDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Commons;
using CanteenMigration.Services.Commons;

namespace CanteenMigration.Services
{
    public class ParentService : IParentService
    {
        IParentRepository _parentRepo;
        ITransactionRepository _transactionRepo;
        IEmailService _emailService;
        IEncryptionService _encryptionService;
        IStudentRepository _studentRepo;

        public ParentService(IParentRepository parentRepo, ITransactionRepository transactionRepo, IEmailService emailService,
            IEncryptionService encryptionService, IStudentRepository studentRepo)
        {
            _parentRepo = parentRepo;
            _transactionRepo = transactionRepo;
            _emailService = emailService;
            this._encryptionService = encryptionService;
            _studentRepo = studentRepo;
        }

        // decimal UNLIMITED_PARENT_PLAN_FEE = GetUnlimitedParentPlanFee();
        public ParentInfo GetParentInfo(int schoolId, int parentId)
        {
            try
            {
                Parent parent = _parentRepo.GetParent(schoolId, parentId);
                if (parent.parentFeePlan == null || parent.parentFeePlan.Value == 0)
                {
                    parent.parentFeePlan = 1; //Pay as you go
                }

                _parentRepo.SaveUpdatedParent();

                if (!parent.Equals(null))
                    return new ParentInfo
                    {
                        parentId = parent.id,
                        firstName = parent.firstName,
                        lastName = parent.lastName,
                        mobile = parent.mobile,
                        email = parent.email,
                        credit = parent.credit.Value,
                        parentPlan = parent.parentFeePlan.Value,
                        schoolId = parent.schoolId,
                        isStudentClassSetupRequired = IsClassSetupRequired(schoolId, parentId)
                    };
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateParentPassword(Parent parent, string newPassword)
        {
            Parent oldParent = _parentRepo.GetParent(parent.id);

            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(newPassword);

            //parent.passwordOld = parent.password;
            oldParent.password = encryptedPassword;
            _parentRepo.SaveUpdatedParent();
            return true;
        }
        public bool UpdateParent(ParentInfo parent, int schoolId)
        {
            Parent oldParent = _parentRepo.GetParent(schoolId, parent.parentId);
            if (oldParent == null)
            {
                return false;
            }

            oldParent.firstName = parent.firstName;
            oldParent.lastName = parent.lastName;
            oldParent.mobile = parent.mobile;
            oldParent.email = parent.email;
            if (!String.IsNullOrEmpty(parent.password))
            {
                //oldParent.passwordOld = oldParent.password;

                AesEncryption aesEncryption = new AesEncryption();
                string encryptedPassword = aesEncryption.encrypt(parent.password);

                oldParent.password = encryptedPassword;
            }
            _parentRepo.SaveUpdatedParent();
            return true;
        }

        public Parent GetParent(int schoolId, int parentId)
        {
            return _parentRepo.GetParent(schoolId, parentId);
        }

        public Parent GetParent(int parentId)
        {
            return _parentRepo.GetParent( parentId);
        }

        public Parent GetParent(string email)
        {
            return _parentRepo.GetParent(email);
        }

        public decimal GetParentCredit(int parentId, int schoolId)
        {
            return _parentRepo.GetParentCredit(parentId, schoolId);
        }

        public ParentPlanUpdateResult UpdateParentPlan(int schoolId, int parentId, int plan, bool isCreditCardPayment, string paymentGatewayTransactionId)
        {
            ParentPlanUpdateResult result = new ParentPlanUpdateResult();
            Parent parent = GetParent(schoolId, parentId);

            if (plan == 1)
            {
                //Pay as you go
                parent.parentFeePlan = 1;
                _parentRepo.SaveUpdatedParent();

                result.isPlanUpdated = true;
                result.message = "Plan Updated successfully! You can start ordering";
                result.parentPlan = parent.parentFeePlan.Value;
                result.parentCredit = parent.credit.Value;
                return result;
            }

            if (plan == 2)
            {
                if (parent.credit < GetUnlimitedParentPlanFee() && !isCreditCardPayment)
                {
                    result.isPlanUpdated = false;
                    result.message = "Insufficient funds. We cannot complete this transaction";
                    return result;
                }

                //Operations for unlimited plan
                UpdateParentPlanToUnlimited(parent, isCreditCardPayment, paymentGatewayTransactionId);
                result.isPlanUpdated = true;
                result.message = "Plan Updated successfully! You can start ordering";
                result.parentPlan = parent.parentFeePlan.Value;
                result.parentCredit = parent.credit.Value;
                return result;
            }

            result.isPlanUpdated = false;
            result.message = "Wrong plan";
            return result;
        }

        private void UpdateParentPlanToUnlimited(Parent parent, bool isCreditCardPayment, string paymentGatewayTransactionId)
        {
            decimal UNLIMITED_PARENT_PLAN_FEE = GetUnlimitedParentPlanFee();
            decimal newParentCredit = isCreditCardPayment ? parent.credit.Value : parent.credit.Value - UNLIMITED_PARENT_PLAN_FEE;
            decimal parentCreditBefore = parent.credit.Value;

            //1. Add transaction 2. Update parent credit 3. add to service_fees_paid 4. send email
            Transaction transaction = new Transaction();
            transaction.parentId = parent.id;
            transaction.schoolId = parent.schoolId;
            transaction.transactionAmount = UNLIMITED_PARENT_PLAN_FEE;
            transaction.transactionType = "SERVICEFEE:DEBIT";
            transaction.balanceBefore = parent.credit;
            transaction.balanceAfter = newParentCredit;
            transaction.transactionDate = DateTime.Now;
            if (isCreditCardPayment)
            {
                transaction.transactionId = paymentGatewayTransactionId;
                transaction.extra1 = "PLAN-CC";
            }
            _transactionRepo.AddTransaction(transaction);

            parent.parentFeePlan = 2;
            parent.credit = newParentCredit;

            _parentRepo.SaveUpdatedParent();
            _parentRepo.AddServiceFeePaid(parent.id, parent.schoolId.Value, UNLIMITED_PARENT_PLAN_FEE);
            _emailService.SendParentPlanUpdatedEmail(parent.email, parentCreditBefore, newParentCredit, UNLIMITED_PARENT_PLAN_FEE, parent.firstName + " " + parent.lastName);
        }

        public bool IsClassSetupRequired(int schoolId, int parentId)
        {
            return _parentRepo.IsClassSetupRequired(schoolId, parentId);
        }

        public decimal GetUnlimitedParentPlanFee()
        {
            DateTime now = DateTime.Now;
            int month = now.Month;
            if (month < 4)
            {
                return 13.20M;
            }

            if (month < 7)
            {
                return 9.90M;
            }

            if (month < 10)
            {
                return 6.60M;
            }

            return 3.30M;
        }

        public Parent RegisterParent(RegisterParentInput input)
        {
            string studentClass = string.Empty;
            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(input.password);
            Parent parent = new Parent()
            {
                schoolId = input.schoolId,
                firstName = input.firstName,
                lastName = input.lastName,
                email = input.email,
                mobile = input.mobileNumber,
                password = encryptedPassword,
                userName = input.email,
                registerDate = DateTime.Now,
                receiveStatus = true,
                processed = true,
                extra2 = input.registerAs,
                credit = 0,
                parentFeePlan = 0,
                isEmailVerified = false
            };
            Parent newParent = _parentRepo.AddParent(parent);
            if (newParent != null)
            {
                //customer entry based on the role: STAFF and Senior student
                switch (input.registerAs)
                {
                    case 4: //guest student
                        Customers cg = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = input.schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "guest",
                            studentStatus = "ACTIVE",
                            studentClass = "Guest",
                            studentNotes = "NONE",
                            oldDudId = 1,
                        };
                        _studentRepo.AddStudent(cg);
                        break;
                    case 3: //senior student
                        Customers c = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = input.schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "seniors",
                            studentStatus = "ACTIVE",
                            studentClass = "Senior",
                            studentNotes = "NONE",
                            oldDudId = 1,
                        };
                        _studentRepo.AddStudent(c);
                        break;
                    case 2: //staff
                        Customers cs = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = input.schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "staff",
                            studentStatus = "ACTIVE",
                            studentClass = "Staff",
                            studentNotes = "NONE",
                            oldDudId = 1,
                        };
                        _studentRepo.AddStudent(cs);
                        break;
                }
                return parent;
            }
            return null;
        }
        public bool RegisterParent(RegisterParentInput input, int schoolId)
        {
            int extra2 = 0;
            int role = 0;
            string studentClass = string.Empty;

            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(input.password);

            Parent parent = new Parent()
            {
                schoolId = schoolId,
                firstName = input.firstName,
                lastName = input.lastName,
                email = input.email,
                mobile = input.mobileNumber,
                password = encryptedPassword,
                userName = input.email,
                registerDate = DateTime.Now,
                receiveStatus = true,
                processed = true,
                extra2 = input.registerAs,
                credit = 0,
                parentFeePlan = 0,
                isEmailVerified = false


            };

            string linkToVerify = "https://www.school24.net.au/canteenorder/login?data=" + _encryptionService.encrypt(parent.email);
            //string linkToVerify = "http://localhost:4200/canteenorder/login?data=" + _encryptionService.encrypt(parent.email);

            Parent newParent = _parentRepo.AddParent(parent);
            if (newParent != null)
            {

                //customer entry based on the role: STAFF and Senior student
                switch (input.registerAs)
                {
                    case 4: //guest student
                        Customers cg = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "guest",
                            studentStatus = "ACTIVE",
                            studentClass = "Guest",
                            studentNotes = "NONE",
                            oldDudId = 1,
                        };
                        _studentRepo.AddStudent(cg);
                        break;
                    case 3: //senior student
                        Customers c = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "seniors",
                            studentStatus = "ACTIVE",
                            studentClass = "Senior",
                            studentNotes = "NONE",
                            oldDudId = 1,

                        };
                        _studentRepo.AddStudent(c);
                        break;
                    case 2: //staff
                        Customers cs = new Customers()
                        {
                            parentId = newParent.id,
                            schoolId = schoolId,
                            studentFirstName = input.firstName,
                            studentLastName = input.lastName,
                            studentImage = "staff",
                            studentStatus = "ACTIVE",
                            studentClass = "Staff",
                            studentNotes = "NONE",
                            oldDudId = 1,

                        };
                        _studentRepo.AddStudent(cs);
                        break;
                }

                _emailService.SendRegistrationEmail(parent.email, parent.firstName, linkToVerify);
                return true;
            }
            return false;
        }

        public string sendVerificationEmail(string email)
        {
            Parent parent = _parentRepo.GetParent(email);
            if (parent == null)
            {
                //return error
                return "Parent account not found.";
            }


            string linkToVerify = "https://www.school24.net.au/canteenorder/login?data=" + _encryptionService.encrypt(email);
            _emailService.SendRegistrationEmail(email, parent.firstName, linkToVerify);

            string msg = "Your registration is almost done. We've sent an email to " + email + ". Open the email and click/tap on the activation link to activate your account. If you can't find the email in your inbox, check the junk/spam folder.";
            return msg;
        }

        public string VerifyParentEmail(string data)
        {
            //1. decrypt 2. Find parent by email 3. 
            string email = _encryptionService.decrypt(data);
            Parent parent = _parentRepo.GetParent(email);
            if (parent == null)
            {
                //return error
                return "Parent account not found.";
            }

            if (parent.isEmailVerified != null && parent.isEmailVerified.Value == true)
            {
                return "Email is already verified.";
            }

            parent.processed = true;
            parent.isEmailVerified = true;

            if (_parentRepo.SaveUpdatedParent())
            {
                return "Email verified successfully. Login to continue.";
            }

            return "Error whille updating the account. Please try again.";

        }


        public void updateParentPassword()
        {
            try
            {
                List<Parent> parents = _parentRepo.GetAllParents();
                foreach (Parent p in parents)
                {
                    p.password_before_encryption = p.password;

                    AesEncryption aesEncryption = new AesEncryption();
                    string encryptedPassword = aesEncryption.encrypt(p.password);

                    p.password = encryptedPassword;


                }

                _parentRepo.SaveUpdatedParent();
            }
            catch(Exception e)
            {
                int a = 10;
            }
        }

        public bool DeductParentBalance(int parentId, int schoolId, decimal amountToDeduct)
        {
            return _parentRepo.DeductParentBalance(parentId, schoolId, amountToDeduct);
        }

    }
}
