﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using CanteenMigration.Models;
using Newtonsoft.Json;

namespace CanteenMigration.Services.impl
{
    public class OrderHistoryService : IOrderHistoryService
    {
        IOrderHistoryRepository _orderHistoryRepository;
        IOptionRepository _optionRepository;
        ISchoolRepository _schoolRepository;
        IProductRepository _productRepository;
        private ITransactionRepository _transactionRepository;
        ISchoolService _schoolService;

        public const string OPTIONSERROR = "Options Not Available";
        public const string PRODUCTSERROR = "Products not  Available";

        public OrderHistoryService(IOrderHistoryRepository _orderHistoryRepository, IOptionRepository _optionRepository, ISchoolRepository _schoolRepository,
                                    IProductRepository productRepository, ITransactionRepository _transactionRepository,
                                    ISchoolService schoolService)
        {
            this._orderHistoryRepository = _orderHistoryRepository;
            this._optionRepository = _optionRepository;
            this._schoolRepository = _schoolRepository;
            this._productRepository = productRepository;
            this._transactionRepository = _transactionRepository;
            _schoolService = schoolService;
        }

        public ParentOrder GetParentOrder(int schoolId, int orderId, int parentId)
        {
            return _orderHistoryRepository.GetSingleParentOrder(schoolId, orderId, parentId);
        }
        public List<ParentOrder> GetParentOrder(int schoolId, int parentId, bool orderCancelStatus, int year)
        {
            try
            {
                List<ParentOrder> parentOrders = _orderHistoryRepository.GetParentHistory(schoolId, parentId, orderCancelStatus, year);
                if (!parentOrders.Equals(null))
                {
                    parentOrders = parentOrders.OrderByDescending(e => e.orderId).ToList();
                    return parentOrders;
                }

                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<OrderItemProduct> GetOrderDetails(int schoolId, int orderId, bool isRepeatOrder)
        {
            ParentsOrders parentOrder = _orderHistoryRepository.GetParentsOrder(schoolId, orderId);

            string lunchOrderNumber = "0" + orderId;
            string recessOrderNumber = "R" + orderId;
            string thirdOrderNumber = "T" + orderId;

            //string schoolInfoJSON = _schoolRepository.GetSchoolInfo(schoolId);
            //SchoolInfo schoolSetup = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            SchoolInfo schoolSetup = _schoolService.GetSchoolInfo(schoolId);

            List<Task> lstTasks = new List<Task>();


            Task<List<OrderItemProduct>> lunchOrderItems = _orderHistoryRepository.GetOrderItems(schoolId, orderId, parentOrder.studentId.Value, lunchOrderNumber, "LUNCH", schoolSetup.meal2);
            Task<List<OrderItemProduct>> recessOrderItems = _orderHistoryRepository.GetOrderItems(schoolId, orderId, parentOrder.studentId.Value, recessOrderNumber, "RECESS", schoolSetup.meal1);
            Task<List<OrderItemProduct>> thirdBreakOrderItems = _orderHistoryRepository.GetOrderItems(schoolId, orderId, parentOrder.studentId.Value, thirdOrderNumber, "THIRD", schoolSetup.meal3);

            lstTasks.Add(lunchOrderItems);
            lstTasks.Add(recessOrderItems);
            lstTasks.Add(thirdBreakOrderItems);

            Task.WaitAll(lstTasks.ToArray());


            List<OrderItemProduct> allOrderItems = new List<OrderItemProduct>();
            if (lunchOrderItems.Result != null)
            {
                foreach (OrderItemProduct oItem in lunchOrderItems.Result)
                {
                    allOrderItems.Add(oItem);
                }
            }
            if (recessOrderItems.Result != null)
            {
                foreach (OrderItemProduct oItem in recessOrderItems.Result)
                {
                    allOrderItems.Add(oItem);
                }
            }
            if (thirdBreakOrderItems.Result != null)
            {
                foreach (OrderItemProduct oItem in thirdBreakOrderItems.Result)
                {
                    allOrderItems.Add(oItem);
                }
            }

            if (allOrderItems != null && !isRepeatOrder)
            {
                foreach (OrderItemProduct orderItem in allOrderItems)
                {
                    string optionz = orderItem.productOptions;

                    if (!String.IsNullOrEmpty(optionz))
                    {
                        string optionNames = string.Empty;
                        string[] optionIds = optionz.Split('|');
                        foreach (string optionId in optionIds)
                        {
                            if (optionId.Contains(":")) // Meal Deal condition
                            {
                                string mealDealOptionIdString = optionId.Split(':')[1];
                                string[] mealDealOptionIds = mealDealOptionIdString.Split(',');
                                foreach (string mealDealOptionId in mealDealOptionIds)
                                {
                                    int opt = int.Parse(mealDealOptionId);
                                    string optName = _optionRepository.GetOptionName(opt, schoolId);
                                    optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                                }
                            }
                            else
                            {
                                int opt = int.Parse(optionId);
                                string optName = _optionRepository.GetOptionName(opt, schoolId);
                                optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                            }

                        }

                        orderItem.productOptions = optionNames;
                    }
                }
            }
            return allOrderItems;
        }

        public ReorderOrderItemProducts GetOrderDetailsForReOrder(int schoolId, int orderId)
        {


            ParentsOrders parentOrder = _orderHistoryRepository.GetParentsOrder(schoolId, orderId);

            string lunchOrderNumber = "0" + orderId;
            string recessOrderNumber = "R" + orderId;
            string thirdOrderNumber = "T" + orderId;

            //string schoolInfoJSON = _schoolRepository.GetSchoolInfo(schoolId);
            //SchoolInfo schoolSetup = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);

            SchoolInfo schoolSetup = _schoolService.GetSchoolInfo(schoolId);

            List<OrderItemProduct> allOrderItems = new List<OrderItemProduct>();
            List<OrderItemProduct> errorProductItems = new List<OrderItemProduct>();
            List<OrderItemProduct> errorOptionItems = new List<OrderItemProduct>();

            var tasks = new List<Task>();

            //tasks.Add(checkForProductAndOptionAvailabilty(schoolId, orderId, parentOrder.studentId.Value,
            //                                         lunchOrderNumber, "LUNCH", schoolSetup.meal2, allOrderItems, errorProductItems, errorOptionItems));
            // tasks.Add(checkForProductAndOptionAvailabilty(schoolId, orderId, parentOrder.studentId.Value,
            //                                         recessOrderNumber, "RECESS", schoolSetup.meal1, allOrderItems, errorProductItems, errorOptionItems));
            //tasks.Add(checkForProductAndOptionAvailabilty(schoolId, orderId, parentOrder.studentId.Value,
            //                                         thirdOrderNumber, "THIRD", schoolSetup.meal3, allOrderItems, errorProductItems, errorOptionItems));

            ReorderOrderItemProducts itemProducts = new ReorderOrderItemProducts();

            try
            {
                Task.WaitAll(tasks.ToArray());
            }
            catch (AggregateException e)
            {
                itemProducts.errorOptionOrderitemProducts = errorOptionItems;
                itemProducts.errorProductOrderitemProducts = errorProductItems;
                return itemProducts;
            }


            itemProducts.allOrderitemProducts = convertToCartObject(allOrderItems);
            return itemProducts;




        }

        public IEnumerable<OrderItemProduct> GetOrderDetailsForEvent(int schoolId, int orderId, bool isSpecialOrder)
        {
            ParentsOrders parentOrder = _orderHistoryRepository.GetParentsOrder(schoolId, orderId);

            string specialOrderNumber = "S" + orderId;

            List<Task> lstTasks = new List<Task>();


            List<OrderItemProduct> specialOrderItems = _orderHistoryRepository.GetSpecialOrderItems(schoolId, orderId, parentOrder.studentId.Value, specialOrderNumber);


            if (specialOrderItems != null)
            {
                foreach (OrderItemProduct orderItem in specialOrderItems)
                {
                    string optionz = orderItem.productOptions;

                    if (!String.IsNullOrEmpty(optionz))
                    {
                        string optionNames = string.Empty;
                        string[] optionIds = optionz.Split('|');
                        foreach (string optionId in optionIds)
                        {
                            if (optionId.Contains(":")) // Meal Deal condition
                            {
                                string mealDealOptionIdString = optionId.Split(':')[1];
                                string[] mealDealOptionIds = mealDealOptionIdString.Split(',');
                                foreach (string mealDealOptionId in mealDealOptionIds)
                                {
                                    int opt = int.Parse(mealDealOptionId);
                                    string optName = _optionRepository.GetOptionName(opt, schoolId);
                                    optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                                }
                            }
                            else
                            {
                                int opt = int.Parse(optionId);
                                string optName = _optionRepository.GetOptionName(opt, schoolId);
                                optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                            }

                        }

                        orderItem.productOptions = optionNames;
                    }
                }
            }
            return specialOrderItems;
        }

        public SchoolOrder GetSchoolOrder(int schoolId, int orderId, int parentId)
        {
            return _orderHistoryRepository.GetSingleSchoolOrder(schoolId, orderId, parentId);
        }
        public IEnumerable<OrderItemProduct> GetOrderDetailsForSchoolEvent(int schoolId, int orderId)
        {
            SchoolOrders schoolOrder = _orderHistoryRepository.GetSchoolOrder(schoolId, orderId);

            string specialOrderNumber = "S" + orderId;

            List<Task> lstTasks = new List<Task>();
            List<OrderItemProduct> specialOrderItems = _orderHistoryRepository.GetSchoolEventOrderItems(schoolId, orderId, schoolOrder.CustomerId.Value, specialOrderNumber);

            if (specialOrderItems != null)
            {
                foreach (OrderItemProduct orderItem in specialOrderItems)
                {
                    string optionz = orderItem.productOptions;

                    if (!String.IsNullOrEmpty(optionz))
                    {
                        string optionNames = string.Empty;
                        string[] optionIds = optionz.Split('|');
                        foreach (string optionId in optionIds)
                        {
                            if (optionId.Contains(":")) // Meal Deal condition
                            {
                                string mealDealOptionIdString = optionId.Split(':')[1];
                                string[] mealDealOptionIds = mealDealOptionIdString.Split(',');
                                foreach (string mealDealOptionId in mealDealOptionIds)
                                {
                                    int opt = int.Parse(mealDealOptionId);
                                    string optName = _optionRepository.GetSchoolEventOptionName(opt, schoolId);
                                    optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                                }
                            }
                            else
                            {
                                int opt = int.Parse(optionId);
                                string optName = _optionRepository.GetSchoolEventOptionName(opt, schoolId);
                                optionNames = optionNames.Length > 0 ? optionNames + "," + optName : optName;
                            }

                        }

                        orderItem.productOptions = optionNames;
                    }
                }
            }
            return specialOrderItems;
        }
        private CartProduct convertToCartObject(List<OrderItemProduct> allOrderItems)
        {

            CartProduct cp = new CartProduct();
            cp.canteenShopcarts = new List<CanteenShopcartItem>();
            allOrderItems.ForEach(item =>
            {


                CanteenShopcartItem ci = new CanteenShopcartItem();
                ci.productId = item.productId;
                ci.productName = item.productName;
                ci.productPrice = item.price;
                ci.productQuantity = item.quantity;
                ci.mealName = item.mealType;
                ci.productOptions = item.productOptions;
                if (item.options != null && item.options.Count > 0)
                    ci.productPriceWithOptionPrice = ((double)item.options.Select(option => option.price).Sum() + (double)ci.productPrice) * (double)ci.productQuantity;
                else
                    ci.productPriceWithOptionPrice = (double)ci.productPrice * (double)ci.productQuantity;
                List<Models.CartProductAtrributes> lstOptions = new List<Models.CartProductAtrributes>();
                item.options.ForEach(option =>
                {
                    lstOptions.Add(new Models.CartProductAtrributes(option.name, option.price));
                });
                ci.productOptionsList = lstOptions;
                cp.canteenShopcarts.Add(ci);


            });
            return cp;

        }

        public List<CanteenMigration.JSONModels.ParentTransaction> GetAllParentsTransactions(int parentId, int schoolId, int year)
        {
            return _transactionRepository.GetAllParentsTransactions(parentId, schoolId, year);
        }

        public List<ParentOrder> GetFundRaisingOrder(int schoolId, int parentId, int year)
        {
            try
            {
                List<ParentOrder> parentOrders = _orderHistoryRepository.GetFundRaisingOrder(schoolId, parentId, year);
                if (!parentOrders.Equals(null))
                {
                    parentOrders = parentOrders.OrderByDescending(e => e.orderId).ToList();
                    return parentOrders;
                }

                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
