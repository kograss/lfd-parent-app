﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;

namespace CanteenMigration.Services.impl
{
    public class ProductService : IProductService
    {
        IProductRepository _productRepository;
        IOptionRepository _optionRepository;
        public ProductService(IProductRepository _productRepository, IOptionRepository _optionRepository)
        {
            this._productRepository = _productRepository;
            this._optionRepository = _optionRepository;
        }

        public List<Product> GetProducts(int schoolId, string mealName, int categoryId, DateTime deliveryDate)
        {
            try
            {
                List<Product> availableProducts = new List<Product>();
                List<CanteenMigration.Models.Product> productListForUI = _productRepository.GetCanteenProductsFromDB(schoolId, mealName, categoryId);
                if (productListForUI == null )
                {
                    return null;
                }
                else if(productListForUI.Count == 0)
                {
                    return null;
                }
                else
                {
                    var productIds = productListForUI.Select(e => e.id).ToList();
                    var productAvailability = _productRepository.GetProductAvailability(productIds, schoolId);
                    foreach (CanteenMigration.Models.Product product in productListForUI)
                    {


                        if (isProductAvailableForOrdering(deliveryDate, product, productAvailability))
                        {
                            CanteenMigration.JSONModels.Product newProduct = new Product();
                            newProduct.catalogID = product.id;
                            newProduct.cname = product.productName;
                            newProduct.cprice = product.productPrice;
                            newProduct.cdescription = product.productDescription;
                            newProduct.cimageurl = product.productImageURL;
                            if (product.productDateAvailable.HasValue)
                            {
                                newProduct.cdateavailable = product.productDateAvailable.Value.ToShortDateString();
                            }
                            newProduct.healthcategory = product.healthCategory;
                            newProduct.food_label = product.foodLabel;
                            newProduct.quantity_limit = product.quantityLimit;
                            newProduct.notAvailableDays = product.extra2;
                            newProduct.productClassification = product.productClassification;

                            //string optionsJSON = _optionRepository.GetProductOptions(schoolId, mealName, product.id);
                           // if (!string.IsNullOrEmpty(optionsJSON))
                            {
                             //   List<Options> options = JsonConvert.DeserializeObject<List<Options>>(optionsJSON);
                                List<Options> options = _optionRepository.GetCanteenOptionsFromDB(schoolId, mealName, product.id);
                                List<OptionSubGroup> optionSubGroup = new List<OptionSubGroup>();

                                foreach (Options option in options)
                                {
                                    if (option.schoolId == schoolId)
                                    {
                                        option.Option_SubGroupId = String.IsNullOrEmpty(option.Option_SubGroupId) ? string.Empty : option.Option_SubGroupId;

                                        if (optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).Count() > 0)
                                        {
                                            OptionSubGroup o = optionSubGroup.Where(e => e.OptionSubGroupName == option.Option_SubGroupId).FirstOrDefault();
                                            o.options.Add(option);
                                        }
                                        else
                                        {
                                            OptionSubGroup o = new OptionSubGroup();
                                            o.OptionSubGroupName = option.Option_SubGroupId;
                                            o.Max_Items = option.Max_Items.GetValueOrDefault();
                                            o.Required = option.Required.GetValueOrDefault();
                                            o.options = new List<Options>();
                                            o.options.Add(option);
                                            optionSubGroup.Add(o);
                                        }
                                    }
                                }
                                newProduct.OptionSubGroup = optionSubGroup;
                            }//options

                            availableProducts.Add(newProduct);
                        }
                    }
                    return availableProducts;
                }
                //string productsJSON = _productRepository.GetProducts(schoolId, mealName, categoryId);
                //if (!string.IsNullOrEmpty(productsJSON))
                {
                   // List<Product> products = JsonConvert.DeserializeObject<List<Product>>(productsJSON);
                    

                    
                }
               // else
                //    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool isProductAvailableForOrdering(DateTime deliveryDate, CanteenMigration.Models.Product product,
            List<Models.CanteenProductAvailability> productAvailability)
        {
            //1. 
            int day = (int)deliveryDate.DayOfWeek + 1;
            string notAvailableDays = product.extra2;
            if (!String.IsNullOrEmpty(notAvailableDays))
            {
                int[] intNotAvailableDays = Array.ConvertAll(notAvailableDays.Split('|'), int.Parse);
                if (intNotAvailableDays.Contains(day))
                {
                    return false;
                }
            }

            return checkProductAvailability(deliveryDate, product, productAvailability);
            //2. TODO: cutoff
        }

        private bool checkProductAvailability(DateTime deliveryDate, CanteenMigration.Models.Product product,
            List<Models.CanteenProductAvailability> productAvailability)
        {
            if (productAvailability == null || productAvailability.Count == 0)
            {
                return true; // product is available
            }
            var x = productAvailability.Where(e => e.productId == product.id).ToList();
            if (x == null || x.Count == 0)
            {
                return true; // product is available
            }
            var p = x.Where(e => deliveryDate >= e.fromDate && deliveryDate <= e.toDate).ToList();
            //date >= startDate && date <= EndDate
            if (p != null && p.Count > 0)
            {
                return true;// product is available
            }
            return false; // product not is available
        }

        public List<ProductStock> GetProductsStock(List<int> productsIds)
        {
            return _productRepository.GetProductsStock(productsIds);
        }
    }
}
