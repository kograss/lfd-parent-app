﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;
using CanteenMigration.Models;
using CanteenMigration.Commons;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Services.exception;

namespace CanteenMigration.Services.impl
{


    public class PlaceOrderService : IPlaceOrderService
    {
        private static readonly Object obj = new Object();

        public enum ORDER_TYPE { LUNCH, RECESS, THIRD, LR, LT, LRT, RT };

        public const string PRODUCTSOUTOFSTOCK = "Some Products are out of stock";

        public PlaceOrderService()
        {

        }



        private ORDER_TYPE GetOrderType(int lunchCount, int recessCount, int thirdCount)
        {
            ORDER_TYPE orderType = ORDER_TYPE.LUNCH;
            if (lunchCount > 0)
            {
                orderType = ORDER_TYPE.LUNCH;
                if (recessCount > 0)
                {
                    orderType = ORDER_TYPE.LR;
                    if (thirdCount > 0)
                    {
                        orderType = ORDER_TYPE.LRT;
                    }
                }
                else if (thirdCount > 0)
                {
                    orderType = ORDER_TYPE.LT;
                }
            }

            else if (recessCount > 0)
            {
                orderType = ORDER_TYPE.RECESS;
                if (thirdCount > 0)
                {
                    orderType = ORDER_TYPE.RT;
                }
            }

            else if (thirdCount > 0)
            {
                orderType = ORDER_TYPE.THIRD;
            }

            return orderType;
        }


        public void processLunchOrders(Order order, int schoolId, int parentId, string orderNumberLunch, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList, IOrderRepository _orderRepo)
        {
            Orders o = new Orders();
            o.studentId = order.studentId;
            o.schoolId = schoolId;
            o.orderDate = DateTime.Now;
            o.orderNumber = orderNumberLunch;
            o.orderComment = order.comment;
            o.orderReason = order.reason;
            o.orderShippedDate = order.deliveryDate;
            o.orderShipMethod = 0;
            o.orderAmount = orderAmount;
            o.orderType = "LUNCH";
            o.bag = bag;
            o.bagPrice = order.chargeForBag;

            _orderRepo.AddOrders(o);
            //_orderRepo.SaveContext();

            int counter = 0;
            List<OrderItems> orderItemsList = new List<OrderItems>();
            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "LUNCH"))
            {
                //insert into oitems
                OrderItems orderItem = new OrderItems();
                orderItem.orderID = o.id;
                orderItem.orderIntemNumber = counter++;
                orderItem.productId = c.productId;
                orderItem.options = c.productOptions;
                orderItem.numerOfItems = (Int16)c.productQuantity.Value;
                orderItem.schoolId = schoolId;
                orderItem.extra2 = "LUNCH";

                orderItemsList.Add(orderItem);
            }
            _orderRepo.InsertLunchOrderItems(orderItemsList);
        }

        public void processRecessOrders(Order order, int schoolId, int parentId, string orderNumberRecess, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList, IOrderRepository _orderRepo)
        {
            int counter = 0;
            //process recess orders
            Orders orderRecess = new Orders();
            orderRecess.studentId = order.studentId;
            orderRecess.schoolId = schoolId;
            orderRecess.orderDate = DateTime.Now;
            orderRecess.orderNumber = orderNumberRecess;
            orderRecess.orderComment = order.comment;
            orderRecess.orderReason = order.reason;
            orderRecess.orderShippedDate = order.deliveryDate;
            orderRecess.orderShipMethod = 0;
            orderRecess.orderAmount = orderAmount;
            orderRecess.orderType = "RECESS";
            orderRecess.bag = bag;
            orderRecess.bagPrice = order.chargeForBag; ;
            _orderRepo.AddOrders(orderRecess);

            List<OrderItemRecess> orderItemsList = new List<OrderItemRecess>();
            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "RECESS"))
            {
                OrderItemRecess orderItemRecess = new OrderItemRecess();
                orderItemRecess.orderId = orderRecess.id;
                orderItemRecess.orderIntemNumber = counter++;
                orderItemRecess.productId = c.productId;
                orderItemRecess.options = c.productOptions;
                orderItemRecess.numerOfItems = (Int16)c.productQuantity.Value;
                orderItemRecess.schoolId = schoolId;
                orderItemRecess.extra2 = "RECESS";
                orderItemsList.Add(orderItemRecess);
                //_orderRepo.SaveContext();
            }
            _orderRepo.InsertRecessOrderItems(orderItemsList);

        }

        public void processThirdBreakOrder(Order order, int schoolId, int parentId, string orderNumberThird, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList, IOrderRepository _orderRepo)
        {
            int counter = 0;
            //process third break orders
            Orders orderThird = new Orders();
            orderThird.studentId = order.studentId;
            orderThird.schoolId = schoolId;
            orderThird.orderDate = DateTime.Now;
            orderThird.orderNumber = orderNumberThird;
            orderThird.orderComment = order.comment;
            orderThird.orderReason = order.reason;
            orderThird.orderShippedDate = order.deliveryDate;
            orderThird.orderShipMethod = 0;
            orderThird.orderAmount = orderAmount;
            orderThird.orderType = "THIRD";
            orderThird.bag = bag;
            orderThird.bagPrice = order.chargeForBag; ;
            _orderRepo.AddOrders(orderThird);

            List<OrderItemRecess> orderItemsList = new List<OrderItemRecess>();
            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "THIRD"))
            {
                OrderItemRecess orderItemRecess = new OrderItemRecess();
                orderItemRecess.orderId = orderThird.id;
                orderItemRecess.orderIntemNumber = counter++;
                orderItemRecess.productId = c.productId;
                orderItemRecess.options = c.productOptions;
                orderItemRecess.numerOfItems = (Int16)c.productQuantity.Value;
                orderItemRecess.schoolId = schoolId;
                orderItemRecess.extra2 = "THIRD";
                //_orderRepo.SaveContext();
                orderItemsList.Add(orderItemRecess);
            }
            _orderRepo.InsertRecessOrderItems(orderItemsList);

        }



        public OrderResult PlaceOrder(List<CanteenShopcartItem> allCartItems, List<Order> orders,
            SchoolInfo schoolSetup, decimal bagCharges, decimal chargesPerOrder, DateTime deliveryDate,
            Parent parent, bool isCreditOrder, string transactionId,
            IDBContext _context, IProductRepository _productRepository, ICartRepository _cartRepo,
             ICartService cartService, IOrderRepository _orderRepo, ITransactionRepository _transactionRepo, IParentRepository _parentRepo,
            bool isBalancePlusCard)
        {

            //lock (obj)
            //{
            var allTransactions = new List<Transaction>();
            OrderResult result = new OrderResult();
            decimal grandTotal = 0;
            List<int> orderIds = new List<int>();
            using (var dbContextTransaction = _context.GetDatabase().BeginTransaction())
            {
                try
                {
                    //deduct stock
                    if (!_productRepository.DeductProductStock(allCartItems, schoolSetup.schoolid))
                    {
                        //dbContextTransaction.Rollback();
                        throw new PlaceOrderException(PRODUCTSOUTOFSTOCK);
                    }
                    foreach (Order order in orders)
                    {
                        int numberOfMeals = 0;
                        List<CanteenShopcartItem> cartItems = allCartItems.Where(cartItem =>
                              cartItem.studentId == order.studentId
                        ).ToList();
                        //List<CanteenShopcartItem> cartItems = _cartRepo.GetCartForStudent(order.studentId, parentId, schoolId, deliveryDate);

                        string orderNumber = Guid.NewGuid().ToString();

                        string bag = schoolSetup.chargeforbag == "YES" ? "BUY" : "NA";
                        string orderType = string.Empty;

                        int lunchCount = cartItems.Where(e => e.mealName == "LUNCH").Count();
                        int recessCount = cartItems.Where(e => e.mealName == "RECESS").Count();
                        int thirdCount = cartItems.Where(e => e.mealName == "THIRD").Count();

                        if (lunchCount > 0)
                        {
                            numberOfMeals++;
                        }
                        if (recessCount > 0)
                        {
                            numberOfMeals++;
                        }
                        if (thirdCount > 0)
                        {
                            numberOfMeals++;
                        }

                        decimal bagPriceForThisOrder = 0;
                        string thisOrderBagType = "NA";

                        if (schoolSetup.chargeforbag == "YES")
                        {
                            if (order.bagType == "buy" && order.bagPrice.HasValue)
                            {
                                bagPriceForThisOrder = order.bagPrice.Value;
                            }

                            thisOrderBagType = order.bagType;
                        }

                        decimal orderAmount = cartService.GetCartAmout(cartItems, schoolSetup.schoolid) + bagPriceForThisOrder;
                        decimal netAmount = orderAmount;
                        orderAmount = orderAmount + chargesPerOrder;
                        grandTotal = grandTotal + orderAmount;
                        orderType = GetOrderType(lunchCount, recessCount, thirdCount).ToString(); //TODO: Test and resolve and tested

                        ParentsOrders parentsOrder = new ParentsOrders();
                        parentsOrder.studentId = order.studentId;
                        parentsOrder.schoolId = schoolSetup.schoolid;
                        parentsOrder.orderDate = DateTime.Now;
                        parentsOrder.orderNumber = orderNumber;
                        parentsOrder.orderComment = order.comment;
                        parentsOrder.orderReason = order.reason;
                        parentsOrder.orderShippedDate = deliveryDate;
                        parentsOrder.orderAmount = orderAmount;
                        parentsOrder.orderType = orderType;
                        parentsOrder.bag = thisOrderBagType;
                        parentsOrder.bagPrice = bagPriceForThisOrder;
                        parentsOrder.parentId = parent.id;
                        parentsOrder.schoolModel = schoolSetup.fee_model;
                        parentsOrder.schoolPlan = schoolSetup.s_fee_plan;
                        parentsOrder.parentPlan = parent.parentFeePlan;
                        parentsOrder.orderCancel = false;

                        _orderRepo.AddParentsOrder(parentsOrder);
                        orderIds.Add(parentsOrder.orderId);

                        string orderNumberLunch = "0" + parentsOrder.orderId;
                        string orderNumberRecess = "R" + parentsOrder.orderId;
                        string orderNumberThird = "T" + parentsOrder.orderId;

                        if (lunchCount > 0)
                        {
                            //process lunch orders
                            processLunchOrders(order, schoolSetup.schoolid, parent.id, orderNumberLunch, orderAmount, bag, cartItems, _orderRepo);
                        }
                        if (recessCount > 0)
                        {
                            processRecessOrders(order, schoolSetup.schoolid, parent.id, orderNumberRecess, orderAmount, bag, cartItems, _orderRepo);
                        }
                        if (thirdCount > 0)
                        {
                            processThirdBreakOrder(order, schoolSetup.schoolid, parent.id, orderNumberThird, orderAmount, bag, cartItems, _orderRepo);
                        }

                        //Create record in Transactions
                        Transaction transaction = new Transaction();
                        transaction.parentId = parent.id;
                        transaction.schoolId = schoolSetup.schoolid;
                        transaction.transactionAmount = netAmount;

                        if (order.isPayAtPickup.Value == true)
                        {
                            transaction.transactionType = "ToPayAtPickUp";
                            transaction.balanceAfter = parent.credit;
                        }
                        else
                        {
                            if (isBalancePlusCard)
                            {
                                transaction.transactionType = "ORDER:DEBIT:PayAtCheckout";
                                transaction.balanceAfter = 0;
                                // transaction.balanceAfter = isCreditOrder ? parent.credit - orderAmount : parent.credit;
                            }
                            else
                            {
                                transaction.transactionType = isCreditOrder ? "ORDER:DEBIT" : "PayAtCheckout";
                                transaction.balanceAfter = isCreditOrder ? parent.credit - orderAmount : parent.credit;
                            }
                        }

                        transaction.extra1 = parentsOrder.orderId.ToString();
                        transaction.balanceBefore = parent.credit;

                        //transaction.balanceAfter = isCreditOrder ? parent.credit - orderAmount : parent.credit;

                        transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                        transaction.studentId = order.studentId;
                        transaction.transactionId = transactionId;
                        allTransactions.Add(transaction);
                        //_transactionRepo.AddTransaction(transaction);
                    }//foreach

                    //Update credit
                    if (isBalancePlusCard)
                    {
                        allTransactions.ForEach(t =>
                        {
                            t.balanceAfter = 0;
                            t.balanceBefore = parent.credit;
                            t.balanceDeductedAmount = parent.credit;
                            t.cardPaymentAmount = grandTotal - parent.credit;
                        });
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, parent.credit.Value);
                    }
                    else if (isCreditOrder && !orders.First().isPayAtPickup.Value)
                    {
                        _parentRepo.DeductParentBalance(parent.id, schoolSetup.schoolid, grandTotal);
                    }
                    _transactionRepo.AddTransactions(allTransactions);

                    _cartRepo.EmptyCart(parent.id, schoolSetup.schoolid, deliveryDate);

                    dbContextTransaction.Commit();
                    result.isOrderSuccess = true;
                    result.message = "Order placed successfully";
                    result.orderIds = orderIds;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    /*result.isOrderSuccess = false;
                    result.message = "Error while placing order. Please try again";
                    result.orderIds = null;*/

                    throw e;
                }
            }

            return result;


            //}

        }


    }
}
