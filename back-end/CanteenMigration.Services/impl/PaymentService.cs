﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using Stripe;

namespace CanteenMigration.Services.impl
{
    public class PaymentService : IPaymentService
    {
        IStripePaymentChargeService stripeService;
        public PaymentService(IStripePaymentChargeService stripeService)
        {
            this.stripeService = stripeService;
        }
        public PaymentResult MakePayment(StripePaymentRequest paymentRequest, decimal orderAmount, int schoolId, int parentId, string logId, string transactionType, string parentEmail)
        {
            return stripeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken, orderAmount, logId, transactionType, parentEmail, paymentRequest.saveCard);
        }
    }
}
