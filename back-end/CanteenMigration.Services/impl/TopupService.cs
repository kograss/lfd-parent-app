﻿using CanteenMigration.Commons;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class TopupService : ITopupService
    {
        public const string TRANSTYPE_TOPUP_CREDIT = "TOPUP:CREDIT";
        public const string TOPUP_START = "IN-PROGRESS";
        public const string TOPUP_SUCCESS = "SUCCESS";
        public const string TOPUP_FAILED = "FAILED";

        ITransactionRepository _transactionRepo;
        ITopupRepository _topupRepo;
        IParentRepository _parentRepo;
        IEmailService _emailService;
        ISchoolRepository _schoolRepo;
        public TopupService(ITransactionRepository transactionRepo, ITopupRepository topupRepo, IParentRepository parentRepo, IEmailService emailService, ISchoolRepository schoolRepo)
        {
            _transactionRepo = transactionRepo;
            _topupRepo = topupRepo;
            _parentRepo = parentRepo;
            _schoolRepo = schoolRepo;
            _emailService = emailService;
        }

        public Topup AddTopup(int parentId, int schoolId, decimal topupamount, decimal grossAmount,
                                  decimal bankFee, decimal school24Fee)
        {
            Parent parent = _parentRepo.GetParent(schoolId, parentId);
            //add to topup table
            Topup topup = new Topup();
            topup.parentId = parentId;
            topup.schoolId = schoolId;
            topup.creditLeft = parent.credit;
            topup.topupAmount = topupamount;
            topup.topupMethod = Topup.TOPUP_METHOD_STRIPE_QP;
            topup.parentEmail = parent.email;
            topup.transactionDate = DateTime.Now;
            //topup.transactionId = paymentIdentifier;
            topup.grossAmount = grossAmount;
            topup.bankFee = bankFee;
            topup.school24Fee = school24Fee;
            topup.parentFirstName = parent.firstName;
            topup.parentLastName = parent.lastName;
            topup.topupStatus = TOPUP_START;

            _topupRepo.AddTopup(topup);
            return topup;
        }
        public decimal CompleteTopup(Topup topup, string paymentIdentifier)
        {
            Parent parent = _parentRepo.GetParent(topup.schoolId, topup.parentId);
            //add to transactions table
            //Create record in Transactions
            Transaction transaction = new Transaction();
            transaction.parentId = topup.parentId;
            transaction.schoolId = topup.schoolId;
            transaction.transactionAmount = topup.topupAmount;
            transaction.transactionType = TRANSTYPE_TOPUP_CREDIT;
            transaction.extra1 = "TOPUP-CC";
            transaction.balanceBefore = parent.credit;
            transaction.balanceAfter = parent.credit + topup.topupAmount;
            transaction.transactionDate = DateTime.Now;
            transaction.transactionId = paymentIdentifier;

            _transactionRepo.AddTransaction(transaction);

            //update topup table
            _topupRepo.UpdateTopupStatus(topup, paymentIdentifier, TOPUP_SUCCESS);

            // _topupRepo.AddTopup(topup);

            //add to parents credit
            _parentRepo.AddParentCredit(topup.parentId, topup.schoolId, topup.topupAmount.Value);

            return parent.credit.Value;
        }

        public void FailedTopup(Topup topup, string paymentIdentifier)
        {
            //update topup table
            _topupRepo.UpdateTopupStatus(topup, paymentIdentifier, TOPUP_FAILED);
        }

        public bool CompleteManulTopup(ManualTopupInput manualTopupInput)
        {
            /*1. add entry to parents_self_topup
              2. send email to manager
              3.*/

            Parent parent = _parentRepo.GetParent(manualTopupInput.schoolId, manualTopupInput.parentId);

            ParentSelfTopup parentSelfTopup = new ParentSelfTopup()
            {
                parentId = manualTopupInput.parentId,
                schoolId = manualTopupInput.schoolId,
                amount = manualTopupInput.amount,
                methodPay = manualTopupInput.methodPay,
                dateYouPaid = manualTopupInput.dateYouPaid,
                approved = manualTopupInput.approved,
                adminAction = manualTopupInput.adminAction,
                comment = manualTopupInput.comment,
                dateSubmit = manualTopupInput.dateSubmit
            };

            _topupRepo.CompleteManulTopup(parentSelfTopup);
            string adminEmail = _schoolRepo.GetCanteenAdminEmail(manualTopupInput.schoolId);
            if (!string.IsNullOrEmpty(adminEmail))
            {
                _emailService.SendManultopupEmailToAdmin(adminEmail, manualTopupInput.amount, parent.firstName + " " + parent.lastName, manualTopupInput.comment);
            }

            return true;
        }
    }
}
