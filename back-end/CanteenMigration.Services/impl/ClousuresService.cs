﻿using DataDependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;

namespace CanteenMigration.Services.impl
{
    public class ClousuresService: IClousuresService
    {
        IClosuresRepository _closuresRepository;
        public ClousuresService(IClosuresRepository closuresRepository)
        {
            this._closuresRepository = closuresRepository;
        }

        public Closures GetClosures(int schoolId)
        {
            try
            {
                Closures closures = _closuresRepository.GetClousures(schoolId);
                if (!closures.Equals(null))
                {
                    return closures;
                }

                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
