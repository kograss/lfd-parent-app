﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.impl
{
    public class EncryptionService : IEncryptionService
    {
        string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Aes encryptor = Aes.Create();
        Rfc2898DeriveBytes pdb;
       

       public EncryptionService()
        {
            pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
            });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
        }

        public string decrypt(string encrypytedString)
        {
            encrypytedString = Base64UrlEncoder.Decode(encrypytedString).Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(encrypytedString);
            string decryptedSting;
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                decryptedSting = Encoding.Unicode.GetString(ms.ToArray());
            }
            return decryptedSting;

        }

        public string encrypt(string encryptString)
        {

           
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
          
               
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
           
            return Base64UrlEncoder.Encode(encryptString);

        }
    }
}
