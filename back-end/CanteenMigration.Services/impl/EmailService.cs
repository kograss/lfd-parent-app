﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Threading;
using System.Net.Mail;
using System.Net;


namespace CanteenMigration.Services.impl
{
    public class EmailDoc
    {
        public string to { get; set; }
        public string subject { get; set; }
        public string emailBody { get; set; }
    }
    public class EmailService : IEmailService
    {
        IEmailRepository _repo;
        IOrderHistoryService _orderHistoryService;
        public const string SUBJECT = "School24 Order Confirmation: Order #";
        public const string TEMPLATENAME = "CanteenMigration.Services.Resources.CanteenOrderTemplate.html";
        public const string PRODUCTTEMPLATENAME = "CanteenMigration.Services.Resources.CanteenOrderTemplateProducts.html";
        public const string BASE_NOTIFICATION_URL = "http://13.236.94.111:5000/";
        public const string API_NOTIFICATION_URL = "notifications/api/email/send-canteen-order-confirmation-email";

        static string msgBody;
        static string prodBody;
        IUniformService _uniformService;
        IUniformRepository _uniformRepo;
        public ILoggerService<EmailService> _loggingService;
        ISchoolRepository _schoolRepo;

        static EmailService()
        {

            var assembly = Assembly.GetExecutingAssembly();


            using (Stream stream = assembly.GetManifestResourceStream(TEMPLATENAME))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    msgBody = reader.ReadToEnd();
                }
            }

            using (Stream stream = assembly.GetManifestResourceStream(PRODUCTTEMPLATENAME))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    prodBody = reader.ReadToEnd();
                }
            }
        }


        public EmailService(IEmailRepository repo, IOrderHistoryService _orderHistoryRepo, IUniformService _uniformService,
                            IUniformRepository _uniformRepo, ILoggerService<EmailService> _loggingService, ISchoolRepository _schoolRepo)
        {
            _repo = repo;
            this._orderHistoryService = _orderHistoryRepo;
            this._uniformService = _uniformService;
            this._uniformRepo = _uniformRepo;
            this._loggingService = _loggingService;
            this._schoolRepo = _schoolRepo;
        }
        public bool AddEmail(string to, int orderId, int parentId, int schoolId, string parentName)
        {
            try
            {
                string subject = SUBJECT + orderId;

                ParentOrder parentsOrder = _orderHistoryService.GetParentOrder(schoolId, orderId, parentId);
                IEnumerable<OrderItemProduct> oitems = _orderHistoryService.GetOrderDetails(schoolId, orderId, false);

                StringBuilder allProducts = new StringBuilder();

                foreach (OrderItemProduct item in oitems)
                {
                    string temp = String.Format(prodBody, item.productName, item.productOptions, item.quantity, item.mealType);
                    allProducts.Append(temp);
                }

                string deliveryDate = parentsOrder.deliveryDate.Day + " " + parentsOrder.deliveryDate.ToString("MMMM", CultureInfo.InvariantCulture) + " " + parentsOrder.deliveryDate.Year;

                String messageBody = String.Format(@msgBody, parentName, parentsOrder.studentName, orderId,
                    parentsOrder.deliveryDate.DayOfWeek + ", " + deliveryDate, allProducts.ToString());

                return _repo.AddEmail(to, subject, messageBody);
            }
            catch (Exception e)
            {
                _loggingService.Info("Error while sending an email with orderId:" + orderId);
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }

                    if (e.Message != null)
                    {
                        _loggingService.Info(e.Message);
                    }
                }


                return false;
            }

        }

        //only for canteen orders to avoid slow emails being sent
        public bool SendAsynEmail(string to, int orderId, int parentId, int schoolId, string parentName)
        {

            try
            {
                string subject = SUBJECT + orderId;

                ParentOrder parentsOrder = _orderHistoryService.GetParentOrder(schoolId, orderId, parentId);
                IEnumerable<OrderItemProduct> oitems = _orderHistoryService.GetOrderDetails(schoolId, orderId, false);

                StringBuilder allProducts = new StringBuilder();

                foreach (OrderItemProduct item in oitems)
                {
                    string temp = String.Format(prodBody, item.productName, item.productOptions, item.quantity, item.mealType);
                    allProducts.Append(temp);
                }

                string deliveryDate = parentsOrder.deliveryDate.Day + " " + parentsOrder.deliveryDate.ToString("MMMM", CultureInfo.InvariantCulture) + " " + parentsOrder.deliveryDate.Year;

                String messageBody = string.Empty;

                string emailBody = _schoolRepo.GetCanteenOrderConfirmationEmailText(schoolId);

                if (string.IsNullOrEmpty(emailBody))
                {
                    messageBody = String.Format(@msgBody, parentName, parentsOrder.studentName, orderId,
                       parentsOrder.deliveryDate.DayOfWeek + ", " + deliveryDate, allProducts.ToString());
                }
                else
                {
                    messageBody = emailBody;
                    messageBody = messageBody.Replace("{parentName}", parentName);
                    messageBody = messageBody.Replace("{studentName}", parentsOrder.studentName);
                    messageBody = messageBody.Replace("{orderNumber}", orderId.ToString());
                    messageBody = messageBody.Replace("{deliveryDate}", parentsOrder.deliveryDate.DayOfWeek + ", " + deliveryDate);
                    messageBody = messageBody.Replace("{foodItemsTable}", allProducts.ToString());
                }


                EmailViewModel emailViewModel = new EmailViewModel();
                emailViewModel.To = new List<string>();
                emailViewModel.To.Add(to);
                emailViewModel.Subject = subject;
                emailViewModel.EmailBody = messageBody;

                ProcessEmail(emailViewModel);

                //using (var client = new HttpClient())
                //{
                //    EmailDoc doc = new EmailDoc { to = to, subject = subject, emailBody = messageBody };
                //    client.BaseAddress = new Uri(BASE_NOTIFICATION_URL);

                //    var response = client.PostAsJsonAsync(API_NOTIFICATION_URL, doc).Result;
                //    if (response.IsSuccessStatusCode)
                //    {
                //        _loggingService.Info("Email sent for orderid:" + orderId);
                //    }
                //    else
                //    {
                //        _loggingService.Info("Error while sending an email for orderid:" + orderId);
                //    }
                //}

                return true;

                // return _repo.AddEmail(to, subject, messageBody);
            }
            catch (Exception e)
            {
                _loggingService.Info("Error while sending an email with orderId:" + orderId);

                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                return false;
            }

        }

        //async send email for donation events
        public bool SendAsynEmailDonationEvent(string to, string parentName, string eventName, int amount)
        {
            try
            {
                string subject = SUBJECT + " " + eventName;
                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "CanteenMigration.Services.Resources.EventOrderDonationTemplate.html";

                String messageBody = string.Empty;
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (stream != null)
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            if (reader != null)
                            {
                                messageBody = reader.ReadToEnd();
                                messageBody = messageBody.Replace("{0}", parentName);
                                messageBody = messageBody.Replace("{1}", eventName);
                                messageBody = messageBody.Replace("{2}", "$" + amount.ToString());

                                _loggingService.Info("Sending an donation event email with parentName:" + parentName);

                                EmailViewModel emailViewModel = new EmailViewModel();
                                emailViewModel.To = new List<string>();
                                emailViewModel.To.Add(to);
                                emailViewModel.Subject = subject;
                                emailViewModel.EmailBody = messageBody;
                                ProcessEmail(emailViewModel);
                            }
                        }
                    }
                }


                return true;
            }
            catch (Exception e)
            {
                _loggingService.Info("Error while sending an email with donation event parentName:" + parentName);

                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                return false;
            }

        }

        public bool SendSchoolEventEmail(string to, int orderId, int parentId, int schoolId, string parentName)
        {
            string subject = SUBJECT + orderId;

            SchoolOrder parentsOrder = _orderHistoryService.GetSchoolOrder(schoolId, orderId, parentId);
            if (parentsOrder != null)
            {
                _loggingService.Info("Event order found for email id:" + to);
                IEnumerable<OrderItemProduct> oitems = _orderHistoryService.GetOrderDetailsForSchoolEvent(schoolId, orderId);
                StringBuilder allProducts = new StringBuilder();
                string messageBody = string.Empty;

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "CanteenMigration.Services.Resources.EventOrderTemplate.html";
                foreach (OrderItemProduct item in oitems)
                {
                    if (item != null)
                    {
                        string temp = String.Format(prodBody, item.productName, item.productOptions, item.quantity, item.mealType);
                        allProducts.Append(temp);
                    }
                }

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (stream != null)
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            if (reader != null)
                            {
                                messageBody = reader.ReadToEnd();
                                messageBody = messageBody.Replace("{0}", parentName);
                                messageBody = messageBody.Replace("{2}", orderId.ToString());
                                messageBody = messageBody.Replace("{4}", allProducts.ToString());

                                _loggingService.Info("Sending an event email with orderId:" + orderId);

                                EmailViewModel emailViewModel = new EmailViewModel();
                                emailViewModel.To = new List<string>();
                                emailViewModel.To.Add(to);
                                emailViewModel.Subject = subject;
                                emailViewModel.EmailBody = messageBody;
                                ProcessEmail(emailViewModel);
                            }
                        }
                    }
                }

            }

            return true;

        }
        public bool SendEventEmail(string to, int orderId, int parentId, int schoolId, string parentName)
        {
            string subject = SUBJECT + orderId;

            ParentOrder parentsOrder = _orderHistoryService.GetParentOrder(schoolId, orderId, parentId);
            if (parentsOrder != null)
            {
                IEnumerable<OrderItemProduct> oitems = _orderHistoryService.GetOrderDetailsForEvent(schoolId, orderId, true);
                StringBuilder allProducts = new StringBuilder();
                foreach (OrderItemProduct item in oitems)
                {
                    if (item != null)
                    {
                        string temp = String.Format(prodBody, item.productName, item.productOptions, item.quantity, item.mealType);
                        allProducts.Append(temp);
                    }
                }
                _loggingService.Info("Sending an event email with all Products:" + allProducts.ToString());
                string deliveryDate = parentsOrder.deliveryDate.Day + " " + parentsOrder.deliveryDate.ToString("MMMM", CultureInfo.InvariantCulture) + " " + parentsOrder.deliveryDate.Year;
                String messageBody = String.Format(@msgBody, parentName, parentsOrder.studentName, orderId,
                    parentsOrder.deliveryDate.DayOfWeek + ", " + deliveryDate, allProducts.ToString());

                //return _repo.AddEmail(to, subject, messageBody);
                _loggingService.Info("Sending an event email with orderId:" + orderId);

                EmailViewModel emailViewModel = new EmailViewModel();
                emailViewModel.To = new List<string>();
                emailViewModel.To.Add(to);
                emailViewModel.Subject = subject;
                emailViewModel.EmailBody = messageBody;

                ProcessEmail(emailViewModel);
            }

            return true;

        }
        public bool SendForgotPasswordLink(string toEmail, string subject, string linkToSend, string parentName)
        {
            string messageBody = string.Empty;
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "CanteenMigration.Services.Resources.ForgotPasswordLink.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    messageBody = reader.ReadToEnd();
                }
            }

            messageBody = messageBody.Replace("{resetLink}", linkToSend);
            messageBody = messageBody.Replace("{ParentName}", parentName);

            EmailViewModel emailViewModel = new EmailViewModel();
            emailViewModel.To = new List<string>();
            emailViewModel.To.Add(toEmail);
            emailViewModel.Subject = subject;
            emailViewModel.EmailBody = messageBody;
            ProcessEmail(emailViewModel);

            //EmailViewModel emailViewModel = new EmailViewModel();
            //emailViewModel.To = new List<string>();
            //emailViewModel.To.Add(toEmail);
            //emailViewModel.Subject = subject;
            //emailViewModel.EmailBody = messageBody;

            //ProcessEmail(emailViewModel);

            //return true;
            _loggingService.Debug("Sending forgot password email to: " + toEmail + " Link:" + linkToSend);
            return _repo.AddEmail(toEmail, subject, messageBody);

        }

        public bool SendParentPlanUpdatedEmail(string toEmail, decimal balanceBefore, decimal balanceAfter, decimal debitedAmount, string parentName)
        {
            string subject = "Your School24 account has been debited.";
            string messageBody = string.Empty;

            var assembly = Assembly.GetExecutingAssembly();
            DateTime currentDate = DateTime.Now;
            string date = currentDate.Day + "/" + currentDate.Month + "/" + currentDate.Year;
            var resourceName = "CanteenMigration.Services.Resources.PlanChange.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    messageBody = reader.ReadToEnd();
                }
            }
            messageBody = messageBody.Replace("{ParentName}", parentName);
            messageBody = messageBody.Replace("{initialBalance}", "$" + System.Math.Round(balanceBefore, 2));
            messageBody = messageBody.Replace("{debitAmount}", "$" + debitedAmount);
            messageBody = messageBody.Replace("{currentBalance}", "$" + System.Math.Round(balanceAfter, 2));
            messageBody = messageBody.Replace("{date}", date);

            EmailViewModel emailViewModel = new EmailViewModel();
            emailViewModel.To = new List<string>();
            emailViewModel.To.Add(toEmail);
            emailViewModel.Subject = subject;
            emailViewModel.EmailBody = messageBody;

            ProcessEmail(emailViewModel);

            return true;
            //return _repo.AddEmail(toEmail, subject, messageBody);
        }

        public bool SendCancelOrderEmail(string toEmail, decimal balanceBefore, decimal balaceAfter, decimal orderAmount, string parentName, string orderId)
        {
            string subject = "Order cancellation confirmation";
            string messageBody = string.Empty;

            var assembly = Assembly.GetExecutingAssembly();
            DateTime currentDate = DateTime.Now;
            string date = currentDate.Day + "/" + currentDate.Month + "/" + currentDate.Year;
            var resourceName = "CanteenMigration.Services.Resources.CancelOrder.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    messageBody = reader.ReadToEnd();
                }
            }

            messageBody = messageBody.Replace("{ParentName}", parentName);
            messageBody = messageBody.Replace("{orderId}", orderId);
            messageBody = messageBody.Replace("{initialBalance}", "$" + System.Math.Round(balanceBefore, 2));
            messageBody = messageBody.Replace("{orderAmount}", "$" + System.Math.Round(orderAmount, 2));
            messageBody = messageBody.Replace("{currentBalance}", "$" + System.Math.Round(balaceAfter, 2));

            EmailViewModel emailViewModel = new EmailViewModel();
            emailViewModel.To = new List<string>();
            emailViewModel.To.Add(toEmail);
            emailViewModel.Subject = subject;
            emailViewModel.EmailBody = messageBody;

            ProcessEmail(emailViewModel);

            return true;

            //return _repo.AddEmail(toEmail, subject, messageBody);
        }

        public bool SendManultopupEmailToAdmin(string toEmail, decimal topupAmount, string parentName, string comment)
        {
            string subject = "Top-up request received from " + parentName;
            string messageBody = string.Empty;

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "CanteenMigration.Services.Resources.Topup.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    messageBody = reader.ReadToEnd();
                }
            }

            messageBody = messageBody.Replace("{ParentName}", parentName);
            messageBody = messageBody.Replace("{topupAmount}", "$" + System.Math.Round(topupAmount, 2));
            messageBody = messageBody.Replace("{comment}", comment);

            EmailViewModel emailViewModel = new EmailViewModel();
            emailViewModel.To = new List<string>();
            emailViewModel.To.Add(toEmail);
            emailViewModel.Subject = subject;
            emailViewModel.EmailBody = messageBody;

            ProcessEmail(emailViewModel);

            return true;
            //return _repo.AddEmail(toEmail, subject, messageBody);
        }

        public bool SendUniformOrderEmail(int orderId, int parentId, int schoolId, string parentEmail, string parentName, DateTime orderdate, string studentClass, string comment, CollectionOption collectionOption = null)
        {
            try
            {
                string subject = SUBJECT + orderId;
                List<OrderDetails> orderItems = _uniformService.GetUniformOrderDetails(schoolId, parentId, orderId);
                string messageBody = string.Empty;
                string adminMessageBody = string.Empty;

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "CanteenMigration.Services.Resources.UniformOrderTemplate.html";

                StringBuilder allProducts = new StringBuilder();

                foreach (OrderDetails item in orderItems)
                {
                    string temp;
                    if (item.isUniformPack == 0)
                    {
                        temp = String.Format(prodBody, item.productName, item.sizeOptions + " " + item.colorOptions, item.quantity, "$" + System.Math.Round(item.price, 2));
                    }
                    else
                    {
                        var uPackProductItems = _uniformRepo.GetAllUPackSubItem(item.basketItemId);
                        string options = "";
                        foreach(var productItem in uPackProductItems)
                        {
                            options += productItem.name;
                            options += productItem.size != null || productItem.color != null? ": "  : " ";
                            options += productItem.size + " " + productItem.color + "<br>";
                        }
                        temp = String.Format(prodBody, item.productName, options, item.quantity, "$" + System.Math.Round(item.price, 2));

                    }
                    
                    allProducts.Append(temp);
                }
                if (collectionOption != null && collectionOption.fee != 0)
                {
                    string temp = String.Format(prodBody, "Delivery Fee", "", "", "$" + System.Math.Round(collectionOption.fee, 2));
                    allProducts.Append(temp);

                }

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (stream != null)
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            if (reader != null)
                            {
                                messageBody = reader.ReadToEnd();
                                messageBody = messageBody.Replace("{0}", parentName);
                                messageBody = messageBody.Replace("{2}", orderId.ToString());
                                messageBody = messageBody.Replace("{4}", allProducts.ToString());

                                _loggingService.Info("Sending an uniform email with orderId:" + orderId);

                                EmailViewModel emailViewModel = new EmailViewModel();
                                emailViewModel.To = new List<string>();
                                emailViewModel.To.Add(parentEmail);
                                emailViewModel.Subject = subject;
                                emailViewModel.EmailBody = messageBody;
                                ProcessEmail(emailViewModel);

                                //_repo.AddEmail(parentEmail, subject, messageBody);

                            }
                        }
                    }
                }


                var assembly1 = Assembly.GetExecutingAssembly();
                var resourceName1 = "CanteenMigration.Services.Resources.UniformOrderTemplateAdmin.html";

                using (Stream stream = assembly1.GetManifestResourceStream(resourceName1))
                {
                    if (stream != null)
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            if (reader != null)
                            {
                                adminMessageBody = reader.ReadToEnd();

                                UniformSetup admin = _uniformRepo.GetUniformSetup(schoolId);
                                if (!string.IsNullOrEmpty(admin.cmgremail))
                                {

                                    adminMessageBody = adminMessageBody.Replace("{0}", parentName);
                                    adminMessageBody = adminMessageBody.Replace("{2}", orderId.ToString());
                                    adminMessageBody = adminMessageBody.Replace("{4}", allProducts.ToString());

                                    adminMessageBody = string.IsNullOrEmpty(parentEmail)
                                                       ? adminMessageBody.Replace("{parent-email}", "")
                                                       : adminMessageBody.Replace("{parent-email}", parentEmail);

                                    adminMessageBody = string.IsNullOrEmpty(studentClass)
                                                       ? adminMessageBody.Replace("{student-class}", "")
                                                       : adminMessageBody.Replace("{student-class}", studentClass);

                                    adminMessageBody = string.IsNullOrEmpty(comment)
                                                       ? adminMessageBody.Replace("{comment}", "")
                                                       : adminMessageBody.Replace("{comment}", comment);



                                    _loggingService.Info("Sending an uniform email with orderId:" + orderId + "and admin email id :" + admin.cmgremail);

                                    EmailViewModel emailViewModel = new EmailViewModel();
                                    emailViewModel.To = new List<string>();
                                    emailViewModel.To.Add(admin.cmgremail);
                                    emailViewModel.Subject = subject;
                                    emailViewModel.EmailBody = adminMessageBody;
                                    ProcessEmail(emailViewModel);

                                    // _repo.AddEmail(admin.cmgremail, subject, adminMessageBody);
                                }
                            }
                        }
                    }
                }


                return true;
            }
            catch (Exception e)
            {
                _loggingService.Info("Error while sending an uniform email with orderId:" + orderId);
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }

                return false;
            }
        }


        public bool SendRegistrationEmail(string toEmail, string parentName, string linkToVerify)
        {
            string subject = "Confirm your school24 Account";
            string messageBody = string.Empty;

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "CanteenMigration.Services.Resources.RegistrationSuccess.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    messageBody = reader.ReadToEnd();
                }
            }


            messageBody = messageBody.Replace("{ParentName}", parentName);
            //messageBody = messageBody.Replace("{email}", toEmail);
            messageBody = messageBody.Replace("{link}", "<a href='" + linkToVerify + "'> Click here </a>");

            EmailViewModel emailViewModel = new EmailViewModel();
            emailViewModel.To = new List<string>();
            emailViewModel.To.Add(toEmail);
            emailViewModel.Subject = subject;
            emailViewModel.EmailBody = messageBody;
            ProcessEmail(emailViewModel);

            return _repo.AddEmail(toEmail, subject, messageBody);
        }

        #region Email Util Methods

        public class EmailViewModel
        {
            public List<string> BCC { get; set; }
            public string Subject { get; set; }
            public string EmailBody { get; set; }

            public List<string> To { get; set; }

        }

        public bool ProcessEmail(EmailViewModel emailViewModel)
        {

            Thread thread = new Thread(new ParameterizedThreadStart(SendMail));
            thread.Start(emailViewModel as object);
            return true;
        }
        public void SendMail(object parameter)
        {
            try
            {
                EmailViewModel emailViewModel = parameter as EmailViewModel;
                using (SmtpClient email = new SmtpClient())
                using (MailMessage mailMessage = new MailMessage())
                {

                    email.DeliveryMethod = SmtpDeliveryMethod.Network;
                    email.UseDefaultCredentials = false;
                    NetworkCredential credential = new NetworkCredential("AKIATYOQEKYFQ6L2U4CM", "BBXaZ4OkwPAuHmhwLz027Aut8iiguGH7GZj0pMXT8zmU");
                    email.Credentials = credential;
                    email.Port = 587;
                    email.Host = "email-smtp.us-west-2.amazonaws.com";
                    email.Timeout = 60000;
                    email.EnableSsl = true;
                    MailMessage oMessage = new MailMessage();
                    string fromEmailId = "support@school24.com.au";
                    mailMessage.From = new MailAddress(fromEmailId);
                    mailMessage.Subject = emailViewModel.Subject;
                    if (emailViewModel.To != null)
                    {
                        mailMessage.Body = emailViewModel.EmailBody;
                    }

                    mailMessage.IsBodyHtml = true;
                    mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    if (emailViewModel.To != null)
                    {
                        foreach (var sendersEmailAddress in emailViewModel.To)
                        {
                            mailMessage.To.Add(sendersEmailAddress);
                        }
                    }
                    email.Send(mailMessage);

                }
            }
            catch (Exception e)
            {
                _loggingService.Info("Error while processing auto-email: " + e.Message);
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
            }
        }

        #endregion
    }
}

