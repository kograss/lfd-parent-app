﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using DataDependencies;
using Newtonsoft.Json;
using CanteenMigration.BusinessDependencies;

namespace CanteenMigration.Services.impl
{
    public class SchoolInfoService : ISchoolService
    {
        ISchoolRepository _schoolRepo;
        public ILoggerService<SchoolInfoService> _loggingService;
        public SchoolInfoService(ISchoolRepository schoolRepo, ILoggerService<SchoolInfoService> _loggingService)
        {
            this._schoolRepo = schoolRepo;
            this._loggingService = _loggingService;

        }

        /*public SchoolInfo GetSchoolInfo(int schoolId)
        {
            try
            {
                string schoolInfoJSON = _schoolRepo.GetSchoolInfo(schoolId);
                if (!string.IsNullOrEmpty(schoolInfoJSON))
                    return JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
                else
                    return null;
            }
            catch (JsonException ex)
            {
                throw ex;
            }
        }*/

        public SchoolInfo GetSchoolInfo(int schoolId)
        {
            try
            {
                SchoolInfo schoolInfo = _schoolRepo.GetSchoolInfo(schoolId);

                return schoolInfo;
            }
            catch (Exception e)
            {
                //throw ex;
                _loggingService.Debug("Start Error while fetching schoolInfo: " + schoolId);
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);

                    }

                    if (e.Message != null)
                    {
                        _loggingService.Debug(e.Message);
                    }
                }
                _loggingService.Debug("Error while fetching schoolInfo Ends here ");
                return null;
            }
        }
        public CanteenMigration.JSONModels.SchoolShops GetSchoolShops(int schoolId)
        {
            return _schoolRepo.GetSchoolShops(schoolId);
        }
    }
}
