﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using DataDependencies;
using CanteenMigration.JSONModels;

namespace CanteenMigration.Services.impl
{
    public class StudentService : IStudentService
    {
        IStudentRepository _studentRepository;
        ISchoolRepository _schoolRepository;
        public StudentService(IStudentRepository _studentRepository, ISchoolRepository _schoolRepository)
        {
            this._studentRepository = _studentRepository;
            this._schoolRepository = _schoolRepository;
        }

        public bool AddNewStudent(Student newStudent, int schoolId)
        {
            Customers student = new Customers();
            student.oldDudId = 1;
            student.parentId = newStudent.parentId;
            student.schoolId = schoolId;
            student.studentFirstName = newStudent.firstName;
            student.studentLastName = newStudent.lastName;
            student.studentStatus = newStudent.status;
            student.studentClass = newStudent.studentClass;
            student.calergy = newStudent.allergy;
            student.studentCardId = newStudent.studentCardId;
            student.maxAllow = newStudent.maxAllow;

            _studentRepository.AddStudent(student);
            return true;
        }

        public bool EditStudent(Student student, int schoolId)
        {
            Customers oldStudent = _studentRepository.GetStudent(student.studentId, schoolId);
            if (oldStudent == null)
            {
                return false;
            }

            oldStudent.studentFirstName = student.firstName;
            oldStudent.studentLastName = student.lastName;
            oldStudent.studentClass = student.studentClass;
            oldStudent.studentStatus = student.status;
            oldStudent.calergy = student.allergy;
            oldStudent.studentCardId = student.studentCardId;
            oldStudent.maxAllow = student.maxAllow;

            _studentRepository.SaveUpdatedStudent();
            return true;
        }

        public List<StudentWithKlass> GetStudentsByParent(int schoolId, int parentId, bool status)
        {
            try
            {
                List<StudentWithKlass> students = new List<StudentWithKlass>();
                if (status)
                {
                    //students = _studentRepository.GetStudentsByParent(schoolId, parentId);
                    var std = _studentRepository.GetActiveStudents(parentId, schoolId);
                    if (std == null)
                    {
                        return null;
                    }
                    foreach (Customers student in std)
                    {
                        students.Add(new StudentWithKlass()
                        {
                            studentId = student.id,
                            parentId = parentId,
                            schoolId = schoolId,
                            firstName = student.studentFirstName,
                            lastName = student.studentLastName,
                            studentClass = student.studentClass,
                            //klass = stu,
                            maxAllow = student.maxAllow,
                            studentCardId = student.studentCardId,
                            classForClassification = GetStudentKlassClassification(_studentRepository.GetStudentClassification(schoolId, student.studentClass))
                        });
                    }
                    return students;
                }

                else
                {
                    students = _studentRepository.GetAllStudentsByParent(schoolId, parentId);
                    if (!students.Equals(null))
                    {
                        foreach (StudentWithKlass student in students)
                        {
                            student.maxAllow = student.maxAllow;
                            student.studentCardId = student.studentCardId;
                            student.classForClassification = GetStudentKlassClassification(_studentRepository.GetStudentClassification(schoolId, student.studentClass));
                        }
                        return students;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<KlassModel> GetAllClasses(int schoolId)
        {
            List<Klass> lst = _schoolRepository.GetKlass(schoolId);
            return lst.Select(e => new KlassModel { id = e.id, name = e.className }).ToList();
        }

        private string GetStudentKlassClassification(string klass)
        {
            string c = string.Empty;
            switch (klass)
            {
                case "Junior":
                    c = "ju";
                    break;
                case "Senior":
                    c = "se";
                    break;
                case "Staff":
                    c = "st";
                    break;
                case "Preschool":
                    c = "pre";
                    break;
                default:
                    c = "ju";
                    break;
            }

            return c;
        }
    }
}
