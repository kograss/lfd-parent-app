﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using DataDependencies;
using System;
using System.Collections.Generic;


namespace CanteenMigration.Services
{
    public interface IPlaceOrderService
    {
        OrderResult PlaceOrder(List<CanteenShopcartItem> allCartItems, List<Order> orders,
           SchoolInfo schoolSetup, decimal bagCharges, decimal chargesPerOrder, DateTime deliveryDate,
           Parent parent, bool isCreditOrder, string transactionId, IDBContext _context, IProductRepository _productRepository, ICartRepository _cartRepo,
            ICartService cartService, IOrderRepository _orderRepo, ITransactionRepository _transactionRepo, IParentRepository _parentRepo, bool isBalancePlusCard);

    }
}
