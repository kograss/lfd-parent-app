﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface ITopupService
    {
        decimal CompleteTopup(Topup topup, string paymentIdentifier);
        void FailedTopup(Topup topup, string paymentIdentifier);
        Topup AddTopup(int parentId, int schoolId, decimal topupamount, decimal grossAmount,
                                  decimal bankFee, decimal school24Fee);
        bool CompleteManulTopup(ManualTopupInput manualTopupInput);
    }
}
