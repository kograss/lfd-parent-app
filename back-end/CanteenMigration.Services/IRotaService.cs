﻿using CanteenMigration.JSONModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IRotaService
    {
        List<ParentsShift> GetParentShifts(int parentId, int schoolId);
        List<RosterNews> GetRosterNews(int schoolId);
        List<ShiftsByDate> GetMonthsShiftsAndVolunteers(int schoolId, int month, int year);
        bool RegisterVolunteerToShift(int schoolId, int shiftId, int parentId, DateTime date, string shiftName, ref string registerMessage);
    }
}
