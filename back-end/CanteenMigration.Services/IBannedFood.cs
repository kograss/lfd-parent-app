﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IBannedFood
    {
        List<BannedFood> GetBannedFood(int parentId);
        List<Models.Product> GetAllProducts(int schoolId);
        bool AddBannedProduct(List<int> studentIds, int parentId, int schoolId, int productId);
        bool DeleteBannedProduct(int Id);
    }
}
