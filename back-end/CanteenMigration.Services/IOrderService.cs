﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IOrderService
    {
        OrderResult PlaceOrder(List<Order> orders, DateTime deliveryDate, SchoolInfo schoolInfo, Parent parent,
            bool isCreditOrder, string transactionId, List<CanteenShopcartItem> cartItems, decimal bagCharges,
            decimal chargesPerOrder, bool isBalancePlusCard);
        OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId);
        bool isOrderExists(int orderId, int parentId, int schoolId);
        string IsOrderCanBePlaced(int schoolId, DateTime deliveryDate);
        bool SetFavourite(int orderId, int schoolId);
        bool RemoveFavourite(int orderId, int schoolId);
    }
}
