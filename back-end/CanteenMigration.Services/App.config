﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    <section name="stripeCardErrors" type="System.Configuration.NameValueSectionHandler" />
  </configSections>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="mssqllocaldb" />
      </parameters>
    </defaultConnectionFactory>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
  </entityFramework>
  
  <stripeCardErrors>
    <add key="approve_with_id" value="The payment cannot be authorized." />
    <add key="call_issuer" value="The card has been declined for an unknown reason." />
    <add key="card_not_supported" value="The card does not support this type of purchase." />
    <add key="card_velocity_exceeded" value="The customer has exceeded the balance or credit limit available on their card." />
    <add key="currency_not_supported" value="The card does not support the specified currency." />
    <add key="do_not_honor" value="The card has been declined for an unknown reason." />
    <add key="do_not_try_again" value="The card has been declined for an unknown reason." />
    <add key="duplicate_transaction" value="A transaction with identical amount and credit card information was submitted very recently." />
    <add key="expired_card" value="The card has expired." />
    <add key="fraudulent" value="The payment has been declined as Stripe suspects it is fraudulent." />
    <add key="generic_decline" value="The card has been declined for an unknown reason." />
    <add key="incorrect_number" value="The card number is incorrect." />
    <add key="incorrect_cvc" value="The CVC number is incorrect." />
    <add key="incorrect_pin" value="The PIN entered is incorrect. This decline code only applies to payments made with a card reader." />
    <add key="incorrect_zip" value="The ZIP/postal code is incorrect." />
    <add key="insufficient_funds" value="The card has insufficient funds to complete the purchase." />
    <add key="invalid_account" value="The card, or account the card is connected to, is invalid." />
    <add key="invalid_amount" value="The payment amount is invalid, or exceeds the amount that is allowed." />
    <add key="invalid_cvc" value="The CVC number is incorrect." />
    <add key="invalid_expiry_year" value="The expiration year invalid." />
    <add key="invalid_number" value="The card number is incorrect." />
    <add key="invalid_pin" value="The PIN entered is incorrect. This decline code only applies to payments made with a card reader." />
    <add key="issuer_not_available" value="The card issuer could not be reached, so the payment could not be authorized." />
    <add key="lost_card" value="The payment has been declined because the card is reported lost." />
    <add key="new_account_information_available" value="The card, or account the card is connected to, is invalid." />
    <add key="no_action_taken" value="The card has been declined for an unknown reason." />
    <add key="not_permitted" value="The payment is not permitted." />
    <add key="pickup_card" value="The card cannot be used to make this payment (it is possible it has been reported lost or stolen)." />
    <add key="pin_try_exceeded" value="The allowable number of PIN tries has been exceeded." />
    <add key="processing_error" value="An error occurred while processing the card." />
    <add key="reenter_transaction" value="The payment could not be processed by the issuer for an unknown reason." />
    <add key="restricted_card" value="The card cannot be used to make this payment (it is possible it has been reported lost or stolen)." />
    <add key="revocation_of_all_authorizations" value="The card has been declined for an unknown reason." />
    <add key="revocation_of_authorization" value="The card has been declined for an unknown reason." />
    <add key="security_violation" value="The card has been declined for an unknown reason." />
    <add key="service_not_allowed" value="The card has been declined for an unknown reason." />
    <add key="stolen_card" value="The payment has been declined because the card is reported stolen." />
    <add key="stop_payment_order" value="The card has been declined for an unknown reason." />
    <add key="testmode_decline" value="A Stripe test card number was used." />
    <add key="transaction_not_allowed" value="The card has been declined for an unknown reason." />
    <add key="try_again_later" value="The card has been declined for an unknown reason." />
    <add key="withdrawal_count_limit_exceeded" value="The customer has exceeded the balance or credit limit available on their card." />
  </stripeCardErrors>
  
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-10.0.0.0" newVersion="10.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
	  <AppContextSwitchOverrides value="Switch.System.Net.DontEnableSystemDefaultTlsVersions=false"/>
  </runtime>
</configuration>