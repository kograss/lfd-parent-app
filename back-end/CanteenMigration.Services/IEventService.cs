﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolEvents.Services
{
    public interface IEventService
    {
        SpecialEvent GetSpecialEvent(int schoolId, int eventId);
        List<SchoolEventInfo> GetSchoolEvents(int schoolId);
        List<SchoolEventCategory> GetEventCategories(int schoolId, int eventId);
        List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId);
        List<CanteenMigration.JSONModels.Product> GetCategoriesProductEvents(int schoolId, int eventId, int categoryId);
        List<EventCartProduct> GetCart(int parentId, int schoolId, int eventId);
        bool UpdateCart(EventCartUpdate removeCart, int schoolId, int parentId);
        AddToCartResult AddItemToCart(EventAddToCart cart, int schoolId, int parentId, int eventId);
        List<EventShopcart> GetAllCartItems(int parentId, int schoolId, int eventId);
        decimal GetCartAmout(List<EventShopcart> cartList, int schoolId);
        OrderResult PlaceOrder(List<EventShopcart> allCartItems, List<EventOrder> orders, DateTime deliveryDate,
                                      int eventId, decimal bagCharges, decimal chargesPerOrder,
                                      Parent parent, SchoolInfo schoolSetup,
                                      bool isCreditOrder, string transactionId, SpecialEvent specialEvent, bool isBalancePlusCard);
        decimal GetChargesPerOrderForEvent(SpecialEvent specialEvent, SchoolInfo schoolInfo, Parent parent);
        OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId);
    }
}
