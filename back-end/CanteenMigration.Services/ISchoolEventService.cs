﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface ISchoolEventService
    {
        int GetSchoolId(int eventId);
        List<SchoolEventInfo> GetSchoolEvents(int schoolId);
        List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId);
        List<SchoolEventCartProduct> GetCart(int parentId, int schoolId, int eventId);
        bool UpdateCart(EventCartUpdate removeCart, int schoolId, int parentId);
        AddToCartResult AddItemToCart(EventAddToCart cart, int schoolId, int parentId);
        List<SchoolEventShopcart> GetAllCartItems(int parentId, int schoolId, int eventId);
        decimal GetCartAmout(List<SchoolEventShopcart> cartList, int schoolId);
        OrderResult PlaceOrder(List<SchoolEventShopcart> allCartItems, List<EventOrder> orders, DateTime deliveryDate,
                                      int eventId, decimal chargesPerOrder,
                                      Parent parent, SchoolInfo schoolSetup,
                                      bool isCreditOrder, string transactionId, decimal school24Fee, bool isBalancePlusCard);

        SchoolEvent2 GetSchoolEvent(int schoolId, int eventId);
        void CheckProductStock(List<SchoolEventShopcart> cartItems);
        OrderCancelResult CancelOrder(int orderId, int parentId, int schoolId);
    }
}
