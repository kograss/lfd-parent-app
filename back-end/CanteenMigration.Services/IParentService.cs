﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface IParentService
    {
        Parent RegisterParent(RegisterParentInput input);
        ParentInfo GetParentInfo(int schoolId, int parentId);
        bool UpdateParent(ParentInfo parent, int schoolId);
        Parent GetParent(int schoolId, int parentId);
        Parent GetParent(int parentId);
        bool UpdateParentPassword(Parent parent, string newPassword);
        decimal GetParentCredit(int parentId, int schoolId);
        Parent GetParent(string email);
        ParentPlanUpdateResult UpdateParentPlan(int schoolId, int parentId, int plan, bool isCreditCardPayment, string paymentGatewayTransactionId);
        bool IsClassSetupRequired(int schoolId, int parentId);
        decimal GetUnlimitedParentPlanFee();
        bool RegisterParent(RegisterParentInput input, int schoolId);
        string VerifyParentEmail(string data);
        void updateParentPassword();
        bool DeductParentBalance(int parentId, int schoolId, decimal amountToDeduct);
        string sendVerificationEmail(string email);
    }
}
