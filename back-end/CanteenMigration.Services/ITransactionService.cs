﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services
{
    public interface ITransactionService
    {
        PaymentGatewayTransactionLog CreateTransaction(int parentId, int schoolId, decimal orderAmount, string transactionType, string paymentGatewayName, bool isCCFeeRequired);
        bool UpdateTransaction(string status, PaymentGatewayTransactionLog log, int schoolId, string paymentIdentifier);
        PaymentGatewayTransactionLog CreateTransactionForEvent(int parentId, int schoolId, decimal orderAmount, string transactionType, string paymentGatewayName, bool isCCFeeRequired);
    }
}
