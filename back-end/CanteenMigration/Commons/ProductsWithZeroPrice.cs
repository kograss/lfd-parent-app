﻿using System;
using System.Configuration;

namespace CanteenMigration.Commons
{
    public class ProductsWithZeroPrice
    {
        public static String[] ProductIds
        {
            get
            {
                return ConfigurationManager.AppSettings["ProdcutIdsToAllowWithZeroPrice"].Split(',');
            }
        }
    }
}