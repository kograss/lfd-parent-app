﻿using System.Security.Claims;
using System.Security.Principal;

namespace CanteenMigration.IdentityHelper
{
    public static class IdentityHelper
    {
        public static string GetSchoolId(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;
            return claimIdent != null
                && claimIdent.HasClaim(c => c.Type == "schoolId")
                ? claimIdent.FindFirst("schoolId").Value
                : string.Empty;
        }

        public static string GetParentId(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;
            return claimIdent != null
                && claimIdent.HasClaim(c => c.Type == "parentId")
                ? claimIdent.FindFirst("parentId").Value
                : string.Empty;
        }
    }
}