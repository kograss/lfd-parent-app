﻿using Autofac;
using Autofac.Integration.WebApi;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using CanteenMigration.DataDependencies;
using CanteenMigration.DataDependencies.impl;
using System.Reflection;
using System.Web.Http;
using CanteenMigration.Aspect;
using Autofac.Extras.DynamicProxy;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using DataDependencies;
using DataDependencies.impl;
using CanteenMigration.Services.impl;
using CanteenMigration.filters;
using CanteenMigration.Controllers;
using CanteenMigration.DataDependencies.connector.redis;
using SchoolEvents.Services;
using SchoolEvents.Services.impl;

namespace IOCAutofac.API
{
    public class IOCConfig
    {
        public static void configure()
        {
            var builder = new Autofac.ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);


            builder.RegisterType<RedisConnectorHelper>().SingleInstance();

            #region
            builder.RegisterType<School24Context>().As<IDBContext>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            //TODO: Check for load testing- for single instance
            //builder.Register(c => new NewsRepository(c.Resolve<RedisConnectorHelper>())).As<INewsRepository>().EnableInterfaceInterceptors().SingleInstance().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<NewsRepository>().As<INewsRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OptionRepository>().As<IOptionRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<StudentRepository>().As<IStudentRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<CartRepository>().As<ICartRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            //builder.Register(c => new MealsAndCategoriesRepository(c.Resolve<RedisConnectorHelper>())).As<IMealsAndCategoriesRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<MealsAndCategoriesRepository>().As<IMealsAndCategoriesRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ParentRepository>().As<IParentRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<PublicHolidaysRepository>().As<IPublicHolidaysRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ClosuresRepository>().As<IClosuresRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OrderHistoryRepository>().As<IOrderHistoryRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<SchoolRepository>().As<ISchoolRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<StudentRepository>().As<IStudentRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ProductRepository>().As<IProductRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<CategoriesRepository>().As<ICategoriesRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OrderRepository>().As<IOrderRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<TransactionLogRepository>().As<ITransactionLogRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<TopupRepository>().As<ITopupRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<TransactionRepository>().As<ITransactionRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<EmailRepository>().As<IEmailRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<RotaRepository>().As<IRotaRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<EventRepository>().As<IEventRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<SchoolEventRepository>().As<ISchoolEventRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<UniformRepository>().As<IUniformRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<CouponRepository>().As<ICouponRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<BannedFoodRepository>().As<IBannedFoodRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<FundRaisingEventRepository>().As<IFundRaisingEventRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));

            builder.RegisterType<StripePaymentChargeRepository>().As<IStripePaymentChargeRepository>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));

            #endregion

            #region
            builder.RegisterType<CanteenNewsService>().As<ICanteenNewsService>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<CartService>().As<ICartService>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<MealsAndCategoriesService>().As<IMealsAndCategoriesService>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ParentService>().As<IParentService>().EnableInterfaceInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<HolidayClousuresService>().As<IHolidayClousuresService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OrderHistoryService>().As<IOrderHistoryService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<SchoolInfoService>().As<ISchoolService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<StudentService>().As<IStudentService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ProductService>().As<IProductService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OrderService>().As<IOrderService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<PaymentService>().As<IPaymentService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<TransactionService>().As<ITransactionService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<TopupService>().As<ITopupService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<StripePaymentChargeService>().As<IStripePaymentChargeService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<EmailService>().As<IEmailService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<PlaceOrderService>().As<IPlaceOrderService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<ForgotPasswordService>().As<IForgotPasswordService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().EnableClassInterceptors().SingleInstance().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<RepeatOrderService>().As<IRepeatOrderService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<SchoolInfoService>().As<ISchoolService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<OrderCheckService>().As<IOrderCheckService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<RotaService>().As<IRotaService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<EventService>().As<IEventService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<SchoolEventService>().As<ISchoolEventService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<UniformService>().As<IUniformService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<CouponService>().As<ICouponService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<BannedFoodService>().As<IBannedFood>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));
            builder.RegisterType<FundRaisingEventService>().As<IFundRaisingEventService>().EnableClassInterceptors().InstancePerRequest().InterceptedBy(typeof(LoggerAspect));

            #endregion

            //builder.RegisterType<LoggerAspect>();            

            builder.RegisterGeneric(typeof(NLoggerService<>)).As(typeof(ILoggerService<>)).InstancePerLifetimeScope();
            builder.Register(c => new LoggerAspect(c.Resolve<ILoggerService<LoggerAspect>>()));

            #region
            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
             .AsWebApiExceptionFilterFor<CanteenNewsController>()
            .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<AccountController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<CartController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<HolidayController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<MealController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<OrderController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<OrderHistoryController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<ParentController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<ProductController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<SchoolController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<StripeController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<StudentController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
            .AsWebApiExceptionFilterFor<TopupController>()
            .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
          .AsWebApiExceptionFilterFor<ValuesController>()
         .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
          .AsWebApiExceptionFilterFor<StockController>()
         .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
        .AsWebApiExceptionFilterFor<ForgotPasswordController>()
       .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
                .AsWebApiExceptionFilterFor<RotaController>()
               .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
           .AsWebApiExceptionFilterFor<EventController>()
          .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
          .AsWebApiExceptionFilterFor<StripeEventController>()
         .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
         .AsWebApiExceptionFilterFor<SchoolEventController>()
        .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
         .AsWebApiExceptionFilterFor<UniformController>()
        .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
         .AsWebApiExceptionFilterFor<CouponController>()
        .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
   .AsWebApiExceptionFilterFor<EncryptionController>()
  .InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
.AsWebApiExceptionFilterFor<BannedFoodController>()
.InstancePerRequest();

            builder.Register(c => new ExceptionFilter(c.Resolve<ILoggerService<ExceptionFilter>>()))
.AsWebApiExceptionFilterFor<DonationStripeController>()
.InstancePerRequest();

            #endregion

            //builder.RegisterType<CanteenNewsCacheReader>().SingleInstance();


            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

    }
}