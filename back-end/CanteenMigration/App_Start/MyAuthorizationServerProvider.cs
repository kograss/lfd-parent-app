﻿using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Queries;
using CanteenMigration.Models;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CanteenMigration.IdentityHelper
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            using (School24Context databaseContext = new School24Context())
            {

                ParentQueries parentQueries = new ParentQueries(databaseContext);
                Parent parent = parentQueries.GetParentnByUsernameAndPassword(context.UserName, context.Password);

                try
                {
                    if (parent != null)
                    {
                        if (parent.processed != null && parent.processed.Value)
                        {
                            if (parent.isEmailVerified != null && parent.isEmailVerified.Value)
                            {
                                if (parentQueries.IsSchoolActive(parent.schoolId.Value))
                                {
                                    identity.AddClaim(new Claim(ClaimTypes.Role, "parent"));
                                    identity.AddClaim(new Claim("username", parent.userName));
                                    identity.AddClaim(new Claim(ClaimTypes.Name, parent.firstName + " " + parent.lastName));
                                    identity.AddClaim(new Claim("schoolId", parent.schoolId.ToString()));
                                    identity.AddClaim(new Claim("parentId", parent.id.ToString()));
                                    context.Validated(identity);
                                }
                                else
                                {
                                    context.SetError("School is inactive please contact the school for further questions.", "invalid_grant");
                                    return;
                                }
                            }
                            else
                            {
                                context.SetError("Your email is not verified. Click on send verification link button to get verification email.", "invalid_grant");
                                return;
                            }
                        }
                        else
                        {
                            context.SetError("Your account is INACTIVE please contact School24 for further questions.", "invalid_grant");
                            return;
                        }
                    }
                    else
                    {
                        context.SetError("Invalid Username Or Password", "invalid_grant");
                        return;
                    }

                }
                catch (Exception e)
                {
                    int a = 10;

                }
            }
        }
    }

}