﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
namespace CanteenMigration.Utils
{
    public class JsonHelper
    {
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string JsonSerializer<T>(List<T> t)
        {
            var ser = new JavaScriptSerializer();
            return ser.Serialize(t);
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static List<T> JsonDeserialize<T>(string jsonString)
        {
            var ser = new JavaScriptSerializer();
            return ser.Deserialize<List<T>>(jsonString);
        }
    }
}