﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Commands;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CanteenMigration.Controllers
{


    public class OrderController : ApiController
    {
        private IOrderService _service;
        private IOrderCheckService _orderCheckService;
        private IRepeatOrderService _repeatOrderService;
        public ILoggerService<OrderController> _loggingService;
        public ICartService _cartService;
        public ISchoolService _schoolService;
        public IParentService _parentService;
        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";


        public OrderController(IOrderService service, ILoggerService<OrderController> loggingService,
            IRepeatOrderService _repeatOrderService, ICartService cartService, ISchoolService schoolService,
            IParentService parentService, IOrderCheckService orderCheckService)
        {
            this._service = service;
            this._loggingService = loggingService;
            this._repeatOrderService = _repeatOrderService;
            this._cartService = cartService;
            this._schoolService = schoolService;
            this._parentService = parentService;
            this._orderCheckService = orderCheckService;
        }

        [Authorize]
        [Route("api/CheckIfOrderCanbePlaced")]
        public HttpResponseMessage CheckIfOrderCanbePlaced(HttpRequestMessage request, List<Order> orders)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into  CheckIfOrderCanbePlaced");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            DateTime deliveryDate = orders.First().deliveryDate;
            bool isPayAtPickupOrder = orders.First().isPayAtPickup.Value;
            bool isCreditPaymentOrder = false;
            try
            {
                if (orders.First().isCreditPayment.Value)
                {
                    isCreditPaymentOrder = orders.First().isCreditPayment.Value;
                }
                
            } catch(Exception e)
            {

            }
            

            List<int> orderIds = new List<int>();
            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);

            }
            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);

            }

            List<CanteenShopcartItem> cartItems = _cartService.GetAllCartItems(parentId, schoolId, deliveryDate);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);

            }

            //decimal bagCharges = schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            decimal bagCharges = 0;
            decimal chargesPerOrder = 0;
            if (schoolInfo.fee_model == 1)
            {
                chargesPerOrder = 0;
            }
            else
            {
                if (parent.parentFeePlan == 1)
                {
                    chargesPerOrder = 0.25M;
                    if (schoolInfo.canteenOrderServiceFee != null)
                    {
                        chargesPerOrder = schoolInfo.canteenOrderServiceFee.Value;
                    }
                }
            }
            //calculate bag charges per student 
            if (schoolInfo.chargeforbag == "YES")
            {
                orders.ForEach(order =>
                {
                    if (order.bagType == "buy" && order.bagPrice.HasValue)
                    {
                        bagCharges = bagCharges + order.bagPrice.Value;
                    }
                });

            }

            decimal totalOrderAmount = calculateTotalAmount(schoolInfo, parent, deliveryDate, orders.Count, cartItems, bagCharges, chargesPerOrder);
            try
            {
                if (_orderCheckService.CheckIfOrderCanBePlace(schoolInfo, parent, deliveryDate, cartItems, totalOrderAmount, isCreditPaymentOrder, isPayAtPickupOrder, true))
                {
                    return request.CreateResponse(HttpStatusCode.OK, true);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, false);
                }
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
        }


        [Authorize]
        [Route("api/PlaceOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, List<Order> orders)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into  PlaceOrder");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            DateTime deliveryDate = orders.First().deliveryDate;
            bool isPayAtPickupOrder = orders.First().isPayAtPickup.Value;

            List<int> orderIds = new List<int>();
            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);

            }
            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);

            }

            List<CanteenShopcartItem> cartItems = _cartService.GetAllCartItems(parentId, schoolId, deliveryDate);

            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);

            }

            //decimal bagCharges = schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            decimal bagCharges = 0;
            decimal chargesPerOrder = 0;
            if (schoolInfo.fee_model == 1)
            {
                chargesPerOrder = 0;
            }
            else
            {
                if (parent.parentFeePlan == 1)
                {
                    chargesPerOrder = 0.25M;
                    if (schoolInfo.canteenOrderServiceFee != null)
                    {
                        chargesPerOrder = schoolInfo.canteenOrderServiceFee.Value;
                    }
                }

            }

            //calculate bag charges per student 
            if (schoolInfo.chargeforbag == "YES")
            {
                orders.ForEach(order =>
                {
                    if (order.bagType == "buy" && order.bagPrice.HasValue)
                    {
                        bagCharges = bagCharges + order.bagPrice.Value;
                    }
                });

            }

            decimal totalOrderAmount = calculateTotalAmount(schoolInfo, parent, deliveryDate, orders.Count, cartItems, bagCharges, chargesPerOrder);
            try
            {


                if (_orderCheckService.CheckIfOrderCanBePlace(schoolInfo, parent, deliveryDate, cartItems, totalOrderAmount, true, isPayAtPickupOrder, true))
                {
                    OrderResult result = _service.PlaceOrder(orders, deliveryDate, schoolInfo, parent, true, string.Empty, cartItems, bagCharges, chargesPerOrder, false);
                    return request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            return null;





        }

        private decimal calculateTotalAmount(SchoolInfo schoolInfo, Parent parent, DateTime deliveryDate, int numberOfStudents, List<CanteenShopcartItem> cartItems, decimal bagCharges, decimal chargesPerOrder)
        {


            int dayOfWeek = (int)deliveryDate.DayOfWeek + 1;
            return _cartService.GetCartAmout(cartItems, schoolInfo.schoolid) + (bagCharges) + (chargesPerOrder * numberOfStudents);

        }



        [Authorize]
        [Route("api/Order/CancelOrder/{orderId}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int orderId)
        {
            _loggingService.Debug("into Order/CancelOrder/");
            _loggingService.Info("orderId:" + orderId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            OrderCancelResult result = _service.CancelOrder(orderId, parentId, schoolId);
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [Route("api/Order/RepeatOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, int orderId, DateTime deliveryDate)
        {
            _loggingService.Debug("into Order/RepeatOrder/");
            _loggingService.Info("orderId:" + orderId + " Delivery Date" + deliveryDate);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            OrderResult result = new OrderResult();
            //checks before placing order
            string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);
            if (checksBeforePlacingOrder == "OK")
            {
                result = _repeatOrderService.PlaceRepeatOrder(orderId, deliveryDate, true, string.Empty, parentId, schoolId); //_service.PlaceOrder(null, deliveryDate, schoolId, parentId, true, string.Empty);
                return request.CreateResponse(HttpStatusCode.OK, result);
            }

            else
            {
                result.orderIds = null;
                result.message = checksBeforePlacingOrder;
                result.isOrderSuccess = false;
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Authorize]
        [Route("api/Order/SetOrRemoveFavourite")]
        public HttpResponseMessage Put(HttpRequestMessage request, FavouriteUpdateInput favouriteUpdateInput) //setOrRemove TRUE: SET, FALSE: REMOVE
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into Order/SetFavourite");
            _loggingService.Info("SchoolId" + schoolId);

            bool updateResult = false;
            updateResult = favouriteUpdateInput.setOrRemove == true ?
                                _service.SetFavourite(favouriteUpdateInput.orderId, schoolId) :
                                _service.RemoveFavourite(favouriteUpdateInput.orderId, schoolId);

            if (updateResult)
            {
                return request.CreateResponse(HttpStatusCode.OK, true);
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        [Authorize]
        [Route("api/Order/AddOrderToCart")]
        public HttpResponseMessage Post(HttpRequestMessage request, AddOrderToCartInput addOrderToCartInput)
        {
            _loggingService.Debug("into Order/AddOrderToCart/");
            _loggingService.Info("orderId:" + addOrderToCartInput.orderId + " Delivery Date" + addOrderToCartInput.deliveryDate);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            var flag = _repeatOrderService.AddOrderToCart(parentId, schoolId, addOrderToCartInput.orderId, addOrderToCartInput.deliveryDate);
            return request.CreateResponse(HttpStatusCode.OK, flag);
        }

        public class FavouriteUpdateInput
        {
            public int orderId { get; set; }
            public bool setOrRemove { get; set; }
        }

        public class AddOrderToCartInput
        {
            public int orderId { get; set; }
            public DateTime deliveryDate { get; set; }
        }
    }
}
