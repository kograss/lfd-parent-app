﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class EncryptionController : ApiController
    {
        public ILoggerService<EncryptionController> _loggingService;
        public IEncryptionService _encryptionService;


        public EncryptionController(ILoggerService<EncryptionController> loggingService, IEncryptionService encryptionService)
        {
           
            this._loggingService = loggingService;
            _encryptionService = encryptionService;

        }

        [Route("api/Encryption")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            _loggingService.Debug("Into Enyrption");

            string password = "zL09WsXoZXyqnW5sCNFWEzbQNL3yHGSjhgqhNi6OL4E=";
           // string encryptedPassword = _encryptionService.encrypt(password);
            string decryptedPassword = _encryptionService.decrypt(password);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
           

        }

    }
}
