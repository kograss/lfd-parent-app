﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.DataAccess;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace CanteenMigration.Controllers
{

    public class SchoolController : ApiController
    {
        private ISchoolService _service;
        public ILoggerService<SchoolController> _loggingService;


        public SchoolController(ISchoolService service, ILoggerService<SchoolController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/School/GetSchoolShops/")]
        [ResponseType(typeof(List<Meal>))]
        public HttpResponseMessage GetSchoolShops(HttpRequestMessage request)
        {
            int schoolId = int.Parse(User.Identity.GetSchoolId());
            int parentId = int.Parse(User.Identity.GetParentId());
            _loggingService.Debug("Into School/GetSchoolShops");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);
            try
            {
                var shops = _service.GetSchoolShops(schoolId);
                return request.CreateResponse(HttpStatusCode.OK, shops);
            }
            catch (Exception e)
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.InnerException.Message);
            }
        }

        [Authorize]
        [Route("api/School/GetSchoolInfo/")]
        [ResponseType(typeof(List<Meal>))]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into School/GetSchoolInfo");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            try
            {
                SchoolInfo schoolInfo = _service.GetSchoolInfo(schoolId);
                if (schoolInfo == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, schoolInfo);
                }
            }
            catch(Exception e)
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.InnerException.Message);
            }
        }

        [Authorize]
        [Route("api/School/GetCurrentTime/")]
        [ResponseType(typeof(List<Meal>))]
        public HttpResponseMessage GetCurrentTime(HttpRequestMessage request)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into School/GetCurrentTime");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            SchoolInfo schoolInfo = _service.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                return request.CreateResponse(HttpStatusCode.OK, nowInSchoolTimezone);
            }
        }
    }
}
