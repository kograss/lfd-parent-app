﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.IdentityHelper;
using CanteenMigration.Models;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace CanteenMigration.Controllers
{
    public class AddBannedFoodInput
    {
        public List<int> studentIds { get; set; }
        public int productId { get; set; }
    }

    public class GetAllProductOutput
    {
        public List<Product> allProduct;
        public List<BannedFood> allBannedProducts;
    }

    public class RemoveBannedProductInput
    {
        public int Id { get; set; }
    }
    public class BannedFoodController : ApiController
    {
        private IBannedFood _service;
        public ILoggerService<BannedFoodController> _loggingService;

        public BannedFoodController(ILoggerService<BannedFoodController> loggingService, IBannedFood _service)
        {
            this._loggingService = loggingService;
            this._service = _service;
        }

        [Authorize]
        [Route("api/BannedFood/GetAllItems")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into BannedFood/GetALLITEMS");
            _loggingService.Info("parentid " + parentId);

            List <BannedFood> bannedFood = _service.GetBannedFood(parentId);
            if (bannedFood == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, bannedFood);
            }

        }


        [Authorize]
        [Route("api/BannedFood/GetAllProducts")]
        public HttpResponseMessage GetAllProducts(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into BannedFood/GetAllProducts");
            _loggingService.Info("parentid " + parentId);

            List<Product> products = _service.GetAllProducts(schoolId);
            List<BannedFood> bannedFood = _service.GetBannedFood(parentId);

            if (products == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                var x = new GetAllProductOutput()
                {
                    allProduct = products,
                    allBannedProducts = bannedFood
                };

                return request.CreateResponse(HttpStatusCode.OK, x);
            }

        }

        [Authorize]
        [HttpPost]
        [Route("api/BannedFood/AddBannedProduct")]
        public HttpResponseMessage Post(HttpRequestMessage request, AddBannedFoodInput input )
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into BannedFood/AddBannedProduct");
            _loggingService.Info("parentid " + parentId);

            bool result = _service.AddBannedProduct(input.studentIds, parentId, schoolId, input.productId);
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [HttpPost]
        [Route("api/BannedFood/RemoveBannedProduct")]
        public HttpResponseMessage Post(HttpRequestMessage request, RemoveBannedProductInput input)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into BannedFood/RemoveBannedProduct");
            _loggingService.Info("parentid " + parentId);

            bool result = _service.DeleteBannedProduct(input.Id);
            if (result)
            {
                return request.CreateResponse(HttpStatusCode.OK);

            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }

    }
}
