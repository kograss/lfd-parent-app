﻿using System;
using System.Collections.Generic;
using CanteenMigration.IdentityHelper;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CanteenMigration.JSONModels;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using System.Web.Http.Description;
using CanteenMigration.DataDependencies.DataException;

namespace CanteenMigration.Controllers
{
    public class MealController : ApiController
    {
        private IMealsAndCategoriesService _service;
        public ILoggerService<MealController> _loggingService;


        public MealController(IMealsAndCategoriesService service, ILoggerService<MealController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/Meal/{deliveryDate}")]
        [ResponseType(typeof(List<Meal>))]
        public HttpResponseMessage Get(HttpRequestMessage request, DateTime deliveryDate)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into GetMeal");
            _loggingService.Info("schoolId" + schoolId + " deliveryDate:" + deliveryDate);

            var mealsAndCategories = _service.GetMealsAndCategories(schoolId, deliveryDate);
            if (mealsAndCategories == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, mealsAndCategories);
            }
        }

        [Route("api/MealAdmin/{deliveryDate}/{SchoolId}")]
        [ResponseType(typeof(List<Meal>))]
        public HttpResponseMessage Get(HttpRequestMessage request, DateTime deliveryDate, int schoolId)
        {
            //int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into GetMeal");
            _loggingService.Info("schoolId" + schoolId + " deliveryDate:" + deliveryDate);

            var mealsAndCategories = _service.GetMealsAndCategories(schoolId, deliveryDate);
            if (mealsAndCategories == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, mealsAndCategories);
            }
        }

    }
}
