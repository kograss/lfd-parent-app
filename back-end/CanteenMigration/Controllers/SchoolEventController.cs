﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using Newtonsoft.Json;
using SchoolEvents.Services;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class SchoolEventController : ApiController
    {
        private ISchoolEventService _service;
        public ILoggerService<EventController> _loggingService;
        private ISchoolService _schoolService;
        private IParentService _parentService;
        private ITransactionService _transactionService;
        private IStripePaymentChargeService _stripechargeService;
        public IEmailService _emailService;


        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";
        public const string CREDITUNAVAILABLE = "Order price is greater than available credit";
        public const string EVENTNOTFOUND = "Event is not available";
        public const string INCORRECTEVENTDATE = "Event is not available";
        public const string ORDERINGAFTERCUTOFF = "Sorry, it's past the cut-off time!";

        //public const decimal CHARGES_PER_ORDER = 0.25M;
        public const decimal CHARGES_PER_ORDER = 0.0M;

        public SchoolEventController(ISchoolEventService service, ILoggerService<EventController> loggingService,
                               ISchoolService schoolService, IParentService parentService,
                               ITransactionService transactionService, IStripePaymentChargeService stripechargeService, IEmailService emailService)
        {
            this._service = service;
            this._loggingService = loggingService;
            _schoolService = schoolService;
            _parentService = parentService;
            _transactionService = transactionService;
            _stripechargeService = stripechargeService;
            this._emailService = emailService;
        }

        [Authorize]
        [Route("api/SchoolEvent/GetAllEvents")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());


            _loggingService.Debug("Into SpecialEvent/GetAllEvents");
            _loggingService.Info("SchoolId:" + schoolId);

            List<SchoolEventInfo> schoolEvents = _service.GetSchoolEvents(schoolId);

            if (schoolEvents == null || schoolEvents.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, schoolEvents);
            }
        }

        [Authorize]
        [Route("api/SchoolEvent/GetAllProducts/{eventId}")]
        public HttpResponseMessage GetProducts(HttpRequestMessage request, int eventId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());

            _loggingService.Debug("Into SchoolEvent/GetAllProducts");
            _loggingService.Info("SchoolId:" + schoolId);

            List<CanteenMigration.JSONModels.Product> products = _service.GetAllProductEvents(schoolId, eventId);

            if (products == null || products.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, products);
            }
        }

        [Authorize]
        [Route("api/SchoolEvent/GetParentCart/{eventId}/")]
        public HttpResponseMessage GetParentCart(HttpRequestMessage request, int eventId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Events/GetParentCart");
            _loggingService.Info("SchoolId:" + schoolId);

            List<SchoolEventCartProduct> cart = _service.GetCart(parentId, schoolId, eventId);
            if (cart == null || cart.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cart);
            }
        }

        [Authorize]
        [Route("api/SchoolEvent/UpdateCart")]
        public HttpResponseMessage Delete(HttpRequestMessage request, EventCartUpdate cartUpdate)
        {
            _loggingService.Debug("Into Cart/Delete ");
            _loggingService.Info("SchoolId" + cartUpdate.schoolId + " ParentId:" + cartUpdate.parentId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            bool isCartUpdated = _service.UpdateCart(cartUpdate, schoolId, parentId);
            if (!isCartUpdated)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            List<SchoolEventCartProduct> cartData = _service.GetCart(parentId, schoolId, cartUpdate.eventId);
            if (cartData == null || cartData.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cartData);
            }
        }

        [Authorize]
        [Route("api/SchoolEvent/addToCart")]
        public HttpResponseMessage Post(HttpRequestMessage request, EventAddToCart newCart)
        {
            _loggingService.Debug("Into events/addToCart");
            _loggingService.Info("Parent Id:" + newCart.parentId + " schoolId:" + newCart.schoolId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            AddToCartResult result = _service.AddItemToCart(newCart, schoolId, parentId);
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Authorize]
        [Route("api/SchoolEvent/Order/CancelOrder/{orderId}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int orderId)
        {
            _loggingService.Debug("into Order/CancelOrder/");
            _loggingService.Info("orderId:" + orderId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            OrderCancelResult result = _service.CancelOrder(orderId, parentId, schoolId);
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [HttpPost]
        [Route("api/SchoolEvent/GetPaymentIntent")]
        public HttpResponseMessage GetPaymentIntent(HttpRequestMessage request, [FromBody] StripeEventPaymentRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into SchoolEvent/GetPaymentIntent");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

           
            List<EventOrder> orders = paymentRequest.orderItems;
            int eventId = orders.First().eventId;
            if (eventId <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, EVENTNOTFOUND);
            }

            DateTime deliveryDate = orders.First().deliveryDate.Value;
            if (deliveryDate == null || string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return request.CreateResponse(HttpStatusCode.NotFound, INCORRECTEVENTDATE);
            }

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);
            }

            SchoolEvent2 specialEvent = _service.GetSchoolEvent(schoolId, eventId);

            try
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                if (nowInSchoolTimezone > specialEvent.eventDateEnd)
                {
                    throw new PlaceOrderException(ORDERINGAFTERCUTOFF);
                }
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            List<SchoolEventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            try
            {
                _service.CheckProductStock(cartItems);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            decimal totalOrderAmount = _service.GetCartAmout(cartItems, schoolId) + (CHARGES_PER_ORDER * orders.Count);

            if (totalOrderAmount < parent.credit && paymentRequest.isBalancePlusCard == true)
            {

                try
                {
                    OrderResult result = _service.PlaceOrder(cartItems,
                                            orders, deliveryDate, eventId, CHARGES_PER_ORDER,
                                            parent, schoolInfo, true,
                                            string.Empty, CHARGES_PER_ORDER, false);
                    return request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (PlaceOrderException ex)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                }
            }

            if (paymentRequest.isBalancePlusCard)
            {
                totalOrderAmount = totalOrderAmount - parent.credit.Value;
            }
            // stripe payment
            try
            {

                PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "_School_Event_Checkout", "STRIPE", true);
                PaymentIntent paymentIntent = _stripechargeService.GetPaymentIntent(parentId, schoolId, log.GrossAmount.Value, log.Id, "_School_Event_Checkout", parent.email);
                IDictionary<string, PaymentGatewayTransactionLog> result = new Dictionary<string, PaymentGatewayTransactionLog>(); ;
                result.Add(JsonConvert.SerializeObject(paymentIntent), log);
                return request.CreateResponse(HttpStatusCode.OK, result);

            }

            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
            

        }

        [Authorize]
        [Route("api/SchoolEvent/PayAndPlaceOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]StripeEventPaymentRequest paymentRequest)
        {
            int schoolId = int.Parse(User.Identity.GetSchoolId());
            int parentId = int.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into  SchoolEvent/PlaceOrder");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);
            List<EventOrder> orders = paymentRequest.orderItems;
            int eventId = orders.First().eventId;
            if (eventId <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, EVENTNOTFOUND);
            }

            DateTime deliveryDate = orders.First().deliveryDate.Value;
            if (deliveryDate == null || string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return request.CreateResponse(HttpStatusCode.NotFound, INCORRECTEVENTDATE);
            }

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);
            }

            SchoolEvent2 specialEvent = _service.GetSchoolEvent(schoolId, eventId);

            try
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                if (nowInSchoolTimezone > specialEvent.eventDateEnd)
                {
                    throw new PlaceOrderException(ORDERINGAFTERCUTOFF);
                }
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            List<SchoolEventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            // check stock
            try
            {
                _service.CheckProductStock(cartItems);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            decimal totalOrderAmount = _service.GetCartAmout(cartItems, schoolId) + (CHARGES_PER_ORDER * orders.Count);

            if (string.IsNullOrEmpty(paymentRequest.stripeToken))
            {
                // credit payment
                if (totalOrderAmount > parent.credit)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Not enough credit to complete the order");
                }
                try
                {
                    OrderResult result = _service.PlaceOrder(cartItems,
                                            orders, deliveryDate, eventId, CHARGES_PER_ORDER,
                                            parent, schoolInfo, true,
                                            string.Empty, CHARGES_PER_ORDER, false);
                    return request.CreateResponse(HttpStatusCode.OK, result);
                }
                catch (PlaceOrderException ex)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                }
            }
            else
            {
                if (paymentRequest.isBalancePlusCard)
                {
                    totalOrderAmount = totalOrderAmount - parent.credit.Value;
                }
                // stripe payment
                try
                {
                    PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "_School_Event_Checkout", "STRIPE", true);
                    PaymentResult paymentResult = _stripechargeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken,
                       log.GrossAmount.Value, log.Id, "_School_Event_Checkout", parent.email, paymentRequest.saveCard, eventId);

                    if (paymentResult.paymentStatus)
                    {
                        try
                        {
                            OrderResult result = _service.PlaceOrder(cartItems, orders, deliveryDate, eventId, CHARGES_PER_ORDER, parent,
                                schoolInfo, false, paymentResult.paymentGatewayTransactionId, CHARGES_PER_ORDER,
                                paymentRequest.isBalancePlusCard);
                            _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);

                            try
                            {
                                if (result.orderIds != null)
                                {
                                    _loggingService.Debug("Event order found for ids:" + result.orderIds.ToString());
                                    result.orderIds.ForEach(order =>
                                    {
                                        _emailService.SendSchoolEventEmail(parent.email, order, parent.id, schoolId, parent.firstName + " " + parent.lastName);
                                    });
                                }
                            }
                            catch (Exception)
                            {


                            }

                            return request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        catch (PlaceOrderException ex)
                        {
                            _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                            return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                        }
                        catch (Exception ex)
                        {
                            _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                            throw ex;
                        }


                    }
                    else
                    {
                        _transactionService.UpdateTransaction("[Payment Failure]" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        return request.CreateResponse(HttpStatusCode.PaymentRequired, "Payment Has Failed");
                    }
                }
                catch (PaymentException ex)
                {
                    return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
                }
                catch (PlaceOrderException ex)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                }
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/SchoolEvent/placeOrderAfterPayment")]
        public HttpResponseMessage placeOrderAfterPayment(HttpRequestMessage request, [FromBody] EventOrderAfterPaymentParams reqParams)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            StripeEventPaymentRequest paymentRequest = reqParams.paymentRequest;
            PaymentGatewayTransactionLog log = reqParams.log;
            DateTime deliveryDate = paymentRequest.orderItems.First().deliveryDate.Value;
            List<int> orderIds = new List<int>();
            int eventId = paymentRequest.orderItems.First().eventId;

            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);


            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);

            Parent parent = _parentService.GetParent(schoolId, parentId);


            //SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            List<SchoolEventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);

            SchoolEvent2 specialEvent = _service.GetSchoolEvent(schoolId, eventId);
            try
            {
                
                OrderResult result = _service.PlaceOrder(cartItems, paymentRequest.orderItems, deliveryDate, eventId, CHARGES_PER_ORDER, parent,
                                schoolInfo, false, log.PGTransId, CHARGES_PER_ORDER,
                                paymentRequest.isBalancePlusCard);
                _transactionService.UpdateTransaction("[Success]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);
                try
                {
                    if (result.orderIds != null)
                    {
                        _loggingService.Debug("Event order found for ids:" + result.orderIds.ToString());
                        result.orderIds.ForEach(order =>
                        {
                            _emailService.SendSchoolEventEmail(parent.email, order, parent.id, schoolId, parent.firstName + " " + parent.lastName);
                        });
                    }
                }
                catch (Exception)
                {


                }
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (PlaceOrderException ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }
                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                //return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }
            catch (Exception ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }

                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }

        }
    }

}
