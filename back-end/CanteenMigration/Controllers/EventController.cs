﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using SchoolEvents.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class EventController : ApiController
    {
        private IEventService _service;
        public ILoggerService<EventController> _loggingService;
        private ISchoolService _schoolService;
        private IParentService _parentSercie;
        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";
        public const string CREDITUNAVAILABLE = "Order price is greater than available credit";
        public const string EVENTNOTFOUND = "Event is not available";
        public const string INCORRECTEVENTDATE = "Event is not available";
        public const string INCORRECTEVENTPRICE = "Event price is incorrect";
        public const string ORDERINGAFTERCUTOFF = "Sorry, it's past the cut-off time!";

        public EventController(IEventService service, ILoggerService<EventController> loggingService,
                               ISchoolService schoolService, IParentService parentService)
        {
            this._service = service;
            this._loggingService = loggingService;
            _schoolService = schoolService;
            _parentSercie = parentService;
        }

        [Authorize]
        [Route("api/Events/GetAllEvents")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());


            _loggingService.Debug("Into Events/GetAllEvents");
            _loggingService.Info("SchoolId:" + schoolId);

            List<SchoolEventInfo> schoolEvents = _service.GetSchoolEvents(schoolId);

            if (schoolEvents == null || schoolEvents.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, schoolEvents);
            }
        }

        [Authorize]
        [Route("api/Events/GetAllEventCategories/{eventId}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int eventId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());


            _loggingService.Debug("Into Events/GetAllEventCategories");
            _loggingService.Info("SchoolId:" + schoolId);

            List<SchoolEventCategory> cat = _service.GetEventCategories(schoolId, eventId);

            if (cat == null || cat.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cat);
            }
        }

        [Authorize]
        [Route("api/Events/GetAllProducts/{eventId}")]
        public HttpResponseMessage GetProducts(HttpRequestMessage request, int eventId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());


            _loggingService.Debug("Into Events/GetAllProducts");
            _loggingService.Info("SchoolId:" + schoolId);

            List<CanteenMigration.JSONModels.Product> products = _service.GetAllProductEvents(schoolId, eventId);

            if (products == null || products.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, products);
            }
        }


        [Authorize]
        [Route("api/Events/GetCategoriesProducts/{eventId}/{categoryId}")]
        public HttpResponseMessage GetCategoriesProducts(HttpRequestMessage request, int eventId, int categoryId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());

            _loggingService.Debug("Into Events/GetCategoriesProducts");
            _loggingService.Info("SchoolId:" + schoolId);

            List<CanteenMigration.JSONModels.Product> products = _service.GetCategoriesProductEvents(schoolId, eventId, categoryId);

            if (products == null || products.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, products);
            }
        }

        [Authorize]
        [Route("api/Events/GetParentCart/{eventId}/")]
        public HttpResponseMessage GetParentCart(HttpRequestMessage request, int eventId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Events/GetParentCart");
            _loggingService.Info("SchoolId:" + schoolId);

            List<EventCartProduct> cart = _service.GetCart(parentId, schoolId, eventId);
            if (cart == null || cart.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cart);
            }
        }

        [Authorize]
        [Route("api/Events/UpdateCart")]
        public HttpResponseMessage Delete(HttpRequestMessage request, EventCartUpdate cartUpdate)
        {
            _loggingService.Debug("Into Cart/Delete ");
            _loggingService.Info("SchoolId" + cartUpdate.schoolId + " ParentId:" + cartUpdate.parentId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            bool isCartUpdated = _service.UpdateCart(cartUpdate, schoolId, parentId);
            if (!isCartUpdated)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            List<EventCartProduct> cartData = _service.GetCart(parentId, schoolId, cartUpdate.eventId);
            if (cartData == null || cartData.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cartData);
            }
        }

        [Authorize]
        [Route("api/CanteenEventOrder/CancelOrder/{orderId}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int orderId)
        {
            _loggingService.Debug("into Order/CancelOrder/");
            _loggingService.Info("orderId:" + orderId);

            int schoolId = int.Parse(User.Identity.GetSchoolId());
            int parentId = int.Parse(User.Identity.GetParentId());

            OrderCancelResult result = _service.CancelOrder(orderId, parentId, schoolId);
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [Route("api/Events/addToCart")]
        public HttpResponseMessage Post(HttpRequestMessage request, EventAddToCart newCart)
        {
            _loggingService.Debug("Into events/addToCart");
            _loggingService.Info("Parent Id:" + newCart.parentId + " schoolId:" + newCart.schoolId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            AddToCartResult result = _service.AddItemToCart(newCart, schoolId, parentId, newCart.eventId);
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Authorize]
        [Route("api/Events/PlaceOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, List<EventOrder> orders)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into  PlaceOrder");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            int eventId = orders.First().eventId;
            if (eventId <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, EVENTNOTFOUND);
            }

            SpecialEvent specialEvent = _service.GetSpecialEvent(schoolId, eventId);

            DateTime deliveryDate = orders.First().deliveryDate.Value;
            if (deliveryDate == null || string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return request.CreateResponse(HttpStatusCode.NotFound, INCORRECTEVENTDATE);
            }

            List<int> orderIds = new List<int>();

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);
            }

            try
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                if (nowInSchoolTimezone > specialEvent.specialDateEnd)
                {
                    throw new PlaceOrderException(ORDERINGAFTERCUTOFF);
                }

            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            Parent parent = _parentSercie.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            List<EventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            decimal bagCharges = 0;// schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            if (specialEvent.isBagAvailable.HasValue && specialEvent.isBagAvailable.Value)
            {
                bagCharges = specialEvent.BagPrice.Value;
            }


            decimal chargesPerOrder = _service.GetChargesPerOrderForEvent(specialEvent, schoolInfo, parent);

            decimal totalOrderAmount = 0;
            if (specialEvent.IsSetPrice.HasValue && specialEvent.IsSetPrice == 1)
            {
                if (specialEvent.EventPrice.HasValue && specialEvent.EventPrice.Value > 0)
                {
                    totalOrderAmount = specialEvent.EventPrice.Value + (bagCharges * orders.Count) + (chargesPerOrder * orders.Count);
                }
                else
                {
                    throw new PlaceOrderException(INCORRECTEVENTPRICE);
                }
            }
            else
            {
                totalOrderAmount = _service.GetCartAmout(cartItems, schoolId) + (bagCharges * orders.Count) + (chargesPerOrder * orders.Count);
            }

            if (totalOrderAmount > parent.credit)
            {
                throw new PlaceOrderException(CREDITUNAVAILABLE);
            }

            try
            {
                OrderResult result = _service.PlaceOrder(cartItems, orders, deliveryDate, eventId, bagCharges, chargesPerOrder, parent,
                    schoolInfo, true, string.Empty, specialEvent, false);
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
        }
    }
}
