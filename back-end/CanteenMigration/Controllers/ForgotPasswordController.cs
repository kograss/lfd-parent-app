﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class ForgotPasswordController : ApiController
    {
        private IForgotPasswordService _iForgotPasswordService;
        private IEncryptionService _encryptionService;
        public ILoggerService<ForgotPasswordController> _loggingService;
        public IParentService _parentService;


        public ForgotPasswordController(IForgotPasswordService iForgotPasswordService, ILoggerService<ForgotPasswordController> loggingService, IParentService parentService, IEncryptionService encryptionService)
        {
            this._iForgotPasswordService = iForgotPasswordService;
            this._loggingService = loggingService;
            this._parentService = parentService;
            this._encryptionService = encryptionService;
        }

       public class Email
        {
            public string email { get; set; }
        }

 
        [Route("api/ForgotPassword/SendLink")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Email obj)
        {


            Parent parent = _parentService.GetParent(obj.email);
            if (parent != null)
            {
                _iForgotPasswordService.SendForgotPasswordEmail(parent);
                
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            string msg = "Please check your email. A link to reset your password has been sent to: " + obj.email + ". If you don't receive an email shortly, check your 'bulk email' or 'junk email' folders. To make sure you receive email from School24 in the future, add the 'school24.com.au' domain to your email safe list.";

            return request.CreateResponse(HttpStatusCode.OK, msg);
            
        }


        public class ResetPasswordInfo
        {
            public string password { get; set; }
            public string randomString { get; set; }
        }
        [Route("api/ResetPassword/")]
        public HttpResponseMessage put(HttpRequestMessage request, [FromBody]ResetPasswordInfo obj)
        {
            string decryptedString=null;
            try
            {
                decryptedString = _encryptionService.decrypt(obj.randomString);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
               
            string[] randomNumberTkns = decryptedString.Split('-');
            if (randomNumberTkns.Length == 5)
            {
                int parentId;
                int schoolId;
                long timestamp;
                try
                {
                    parentId = Int32.Parse(randomNumberTkns[1]);
                    schoolId = Int32.Parse(randomNumberTkns[3]);
                    timestamp = long.Parse(randomNumberTkns[4]);
                }
                catch
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
               

                if(DateTime.UtcNow.Ticks- timestamp> TimeSpan.TicksPerDay)
                {
                    return new HttpResponseMessage(HttpStatusCode.Gone);
                }

                Parent parent = _parentService.GetParent(schoolId,parentId);
                if (parent != null)
                {
                    _iForgotPasswordService.ChangePassword(parent, obj.password);

                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);






        }

    }
}
