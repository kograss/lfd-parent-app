﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.Commons;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using Newtonsoft.Json;
using SchoolEvents.Services;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class StripeEventController : ApiController
    {
        private IEventService _service;
        public ILoggerService<StripeEventController> _loggingService;
        private ISchoolService _schoolService;
        private IParentService _parentService;
        private IPaymentService _paymentService;
        private ITransactionService _transactionService;
        private IStripePaymentChargeService _stripechargeService;

        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";
        public const string CREDITUNAVAILABLE = "Order price is greater than available credit";
        public const string EVENTNOTFOUND = "Event is not available";
        public const string INCORRECTEVENTDATE = "Event is not available";
        public const string INCORRECTEVENTPRICE = "Event price is incorrect";
        public const string ORDERINGAFTERCUTOFF = "Sorry, it's past the cut-off time!";
        public StripeEventController(IEventService service, ILoggerService<StripeEventController> loggingService,
                               ISchoolService schoolService, IParentService parentService, IPaymentService paymentService,
                               ITransactionService transactionService, IStripePaymentChargeService stripechargeService)
        {
            _service = service;
            _loggingService = loggingService;
            _schoolService = schoolService;
            _parentService = parentService;
            _paymentService = paymentService;
            _transactionService = transactionService;
            _stripechargeService = stripechargeService;
        }

        [Authorize]
        [HttpPost]
        [Route("api/Events/GetPaymentIntent")]
        public HttpResponseMessage GetPaymentIntent(HttpRequestMessage request, [FromBody] StripeEventPaymentRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into SchoolEvent/GetPaymentIntent");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);


            List<EventOrder> orders = paymentRequest.orderItems;
            int eventId = orders.First().eventId;
            if (eventId <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, EVENTNOTFOUND);
            }

            SpecialEvent specialEvent = _service.GetSpecialEvent(schoolId, eventId);

            DateTime deliveryDate = orders.First().deliveryDate.Value;
            if (deliveryDate == null || string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return request.CreateResponse(HttpStatusCode.NotFound, INCORRECTEVENTDATE);
            }

            List<int> orderIds = new List<int>();

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);
            }

            try
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                if (nowInSchoolTimezone > specialEvent.specialDateEnd)
                {
                    throw new PlaceOrderException(ORDERINGAFTERCUTOFF);
                }

            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            List<EventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            decimal bagCharges = 0;//schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            if (specialEvent.isBagAvailable.HasValue && specialEvent.isBagAvailable.Value)
            {
                bagCharges = specialEvent.BagPrice.Value;
            }

            decimal chargesPerOrder = _service.GetChargesPerOrderForEvent(specialEvent, schoolInfo, parent);

            decimal totalOrderAmount = 0;
            if (specialEvent.IsSetPrice.HasValue && specialEvent.IsSetPrice == 1)
            {
                if (specialEvent.EventPrice.HasValue && specialEvent.EventPrice.Value > 0)
                {
                    totalOrderAmount = specialEvent.EventPrice.Value;
                }
                else
                {
                    throw new PlaceOrderException(INCORRECTEVENTPRICE);
                }
            }
            else
            {
                totalOrderAmount = _service.GetCartAmout(cartItems, schoolId) + (bagCharges * orders.Count) + (chargesPerOrder * orders.Count);
            }
            if (paymentRequest.isBalancePlusCard)
            {
                totalOrderAmount = totalOrderAmount - parent.credit.Value;
            }
            // stripe payment
            try
            {

                PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "Canteen_Event_Checkout", "STRIPE", true);
                PaymentIntent paymentIntent = _stripechargeService.GetPaymentIntent(parentId, schoolId, log.GrossAmount.Value, log.Id, "Canteen_Event_Checkout", parent.email);
                IDictionary<string, PaymentGatewayTransactionLog> result = new Dictionary<string, PaymentGatewayTransactionLog>(); ;
                result.Add(JsonConvert.SerializeObject(paymentIntent), log);
                return request.CreateResponse(HttpStatusCode.OK, result);

            }

            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }


        }

        [Authorize]
        [Route("api/Events/PayAndPlaceOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]StripeEventPaymentRequest paymentRequest)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into  PlaceOrder");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);
            List<EventOrder> orders = paymentRequest.orderItems;
            int eventId = orders.First().eventId;
            if (eventId <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, EVENTNOTFOUND);
            }

            SpecialEvent specialEvent = _service.GetSpecialEvent(schoolId, eventId);

            DateTime deliveryDate = orders.First().deliveryDate.Value;
            if (deliveryDate == null || string.IsNullOrEmpty(deliveryDate.ToString()))
            {
                return request.CreateResponse(HttpStatusCode.NotFound, INCORRECTEVENTDATE);
            }

            List<int> orderIds = new List<int>();

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);
            }

            try
            {
                TimezoneSettings zoneSettings = new TimezoneSettings();
                DateTime nowInSchoolTimezone = zoneSettings.getDateBySchoolTimeZone(schoolId, schoolInfo.timeZone);

                if (nowInSchoolTimezone > specialEvent.specialDateEnd)
                {
                    throw new PlaceOrderException(ORDERINGAFTERCUTOFF);
                }

            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            List<EventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);
            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            decimal bagCharges = 0;//schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            if (specialEvent.isBagAvailable.HasValue && specialEvent.isBagAvailable.Value)
            {
                bagCharges = specialEvent.BagPrice.Value;
            }

            decimal chargesPerOrder = _service.GetChargesPerOrderForEvent(specialEvent, schoolInfo, parent);

            decimal totalOrderAmount = 0;
            if (specialEvent.IsSetPrice.HasValue && specialEvent.IsSetPrice == 1)
            {
                if (specialEvent.EventPrice.HasValue && specialEvent.EventPrice.Value > 0)
                {
                    totalOrderAmount = specialEvent.EventPrice.Value;
                }
                else
                {
                    throw new PlaceOrderException(INCORRECTEVENTPRICE);
                }
            }
            else
            {
                totalOrderAmount = _service.GetCartAmout(cartItems, schoolId) + (bagCharges * orders.Count) + (chargesPerOrder * orders.Count);
            }
            if (paymentRequest.isBalancePlusCard)
            {
                totalOrderAmount = totalOrderAmount - parent.credit.Value;
            }
            try
            {
                PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "Canteen_Event_Checkout", "STRIPE", true);
                PaymentResult paymentResult = _stripechargeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken, log.GrossAmount.Value, log.Id, "Canteen_Event_Checkout",
                    parent.email, paymentRequest.saveCard, eventId);
                if (paymentResult.paymentStatus)
                {
                    try
                    {
                        OrderResult result = _service.PlaceOrder(cartItems, orders, deliveryDate, eventId, bagCharges, chargesPerOrder,
                                                parent, schoolInfo, false, paymentResult.paymentGatewayTransactionId, specialEvent, paymentRequest.isBalancePlusCard);
                        _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        return request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    catch (PlaceOrderException ex)
                    {
                        _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        throw ex;
                    }
                }
                else
                {
                    _transactionService.UpdateTransaction("[Payment Failure]" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                    return request.CreateResponse(HttpStatusCode.PaymentRequired, "Payment Has Failed");
                }
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Events/placeOrderAfterPayment")]
        public HttpResponseMessage placeOrderAfterPayment(HttpRequestMessage request, [FromBody] EventOrderAfterPaymentParams reqParams)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            StripeEventPaymentRequest paymentRequest = reqParams.paymentRequest;
            PaymentGatewayTransactionLog log = reqParams.log;
            DateTime deliveryDate = paymentRequest.orderItems.First().deliveryDate.Value;
            List<int> orderIds = new List<int>();
            int eventId = paymentRequest.orderItems.First().eventId;

            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);


            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);

            Parent parent = _parentService.GetParent(schoolId, parentId);


            //SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            List<EventShopcart> cartItems = _service.GetAllCartItems(parentId, schoolId, eventId);

            SpecialEvent specialEvent = _service.GetSpecialEvent(schoolId, eventId);
            decimal bagCharges = 0;//schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            if (specialEvent.isBagAvailable.HasValue && specialEvent.isBagAvailable.Value)
            {
                bagCharges = specialEvent.BagPrice.Value;
            }

            decimal chargesPerOrder = _service.GetChargesPerOrderForEvent(specialEvent, schoolInfo, parent);
            try
            {
                OrderResult result = _service.PlaceOrder(cartItems, paymentRequest.orderItems, deliveryDate, eventId, bagCharges, chargesPerOrder,
                                                parent, schoolInfo, false, log.PGTransId, specialEvent, paymentRequest.isBalancePlusCard);
                _transactionService.UpdateTransaction("[Success]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);
                
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (PlaceOrderException ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }
                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                //return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }
            catch (Exception ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }

                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }

        }
    }
}
