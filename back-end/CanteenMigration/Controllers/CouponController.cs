﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CanteenMigration.JSONModels;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using System.Web.Http.Description;
using CanteenMigration.DataDependencies.DataException;
using System;
using CanteenMigration.IdentityHelper;
using System.Net;

namespace CanteenMigration.Controllers
{
    public class CouponController : ApiController
    {
        public ILoggerService<CouponController> _loggingService;
        public ICouponService _couponService;

        public CouponController(ILoggerService<CouponController> loggingService, ICouponService _couponService)
        {

            this._loggingService = loggingService;
            this._couponService = _couponService;
        }


        [Authorize]
        [Route("api/Coupon/GetAllCoupons")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Coupon/GetAllCoupons");
            _loggingService.Info("schoolId" + schoolId + " parentId" + parentId);

            var coupons = _couponService.GetAllCoupons(parentId, schoolId);

            if (coupons == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {

                return request.CreateResponse(HttpStatusCode.OK, coupons);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Coupon/RedeemCoupon")]
        public HttpResponseMessage Post(HttpRequestMessage request, RedeemCouponInput input)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Coupon/RedeemCoupon");
            _loggingService.Info("schoolId" + schoolId + " parentId" + parentId);

            var redeemResult = _couponService.RedeemCoupon(input.couponId, parentId, schoolId);

            if (redeemResult == null)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            else
            {

                return request.CreateResponse(HttpStatusCode.OK, redeemResult);
            }
        }
    }
}
