﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataAccess;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace CanteenMigration.Controllers
{
    public class HolidayController : ApiController
    {
        private IHolidayClousuresService _holidayClosuresService;
        public ILoggerService<HolidayController> _loggingService;


        public HolidayController(IHolidayClousuresService holidayClousuresService, ILoggerService<HolidayController> loggingService)
        {
            this._holidayClosuresService = holidayClousuresService;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/Holiday/GetSchoolHolidays/")]
        [ResponseType(typeof(HolidayClousures))]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into Holiday/GetSchoolHolidays");
            _loggingService.Info("SchoolId:" + schoolId);

            int currentYear = DateTime.Now.Year;
            HolidayClousures holidayClosures = _holidayClosuresService.GetHolidayClosures(schoolId, currentYear);

            if (holidayClosures.Equals(null))
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, holidayClosures);
            }
        }
    }
}
