﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.Services.exception;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class UniformController : ApiController
    {
        private IUniformService _service;
        private IParentService _parentService;
        private ITransactionService _transactionService;
        private IStripePaymentChargeService _stripechargeService;
        public ILoggerService<UniformController> _loggingService;
        public IEmailService _emailService;

        public UniformController(IUniformService service, IParentService parentService,
                                 ITransactionService transactionService, IStripePaymentChargeService _stripechargeService,
                                 ILoggerService<UniformController> loggingService, IEmailService emailService)
        {
            this._service = service;
            this._parentService = parentService;
            this._transactionService = transactionService;
            this._stripechargeService = _stripechargeService;
            this._loggingService = loggingService;
            this._emailService = emailService;
        }

        [Authorize]
        [Route("api/Uniform/GetUniformCategories/")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into Uniform/GetUniformCategories");
            _loggingService.Info("SchoolId:" + schoolId);

            List<UniformCategories> categories = _service.GetUniformCategories(schoolId);

            if (categories == null || categories.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, categories);
            }
        }

        [Authorize]
        [Route("api/Uniform/GetUniformNews/")]
        public HttpResponseMessage GetUniformNews(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into Uniform/GetUniformNews");
            _loggingService.Info("SchoolId:" + schoolId);

            List<UniformNews> news = _service.GetUniformNews(schoolId, DateTime.Now.Year);

            if (news == null || news.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, news);
            }
        }

        [Authorize]
        [Route("api/Uniform/GetProducts/{categoryId}")]
        public HttpResponseMessage GetProducts(HttpRequestMessage request, int categoryId)
        {
            try
            {
                int schoolId = Int32.Parse(User.Identity.GetSchoolId());
                _loggingService.Debug("Into Uniform/GetProducts");
                _loggingService.Info("SchoolId:" + schoolId);

                List<UniformProduct> products = _service.GetUniformProductsByCategory(schoolId, categoryId);

                if (products == null || products.Count <= 0)
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, products);
                }
            }
            catch (Exception ex)
            {
                _loggingService.Debug("Exception Occurred:" + ex.Message + "Inner Exception:" + ex.InnerException);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize]
        [Route("api/Uniform/GetUniformPacks/{categoryId}")]
        public HttpResponseMessage GetUniformPacks(HttpRequestMessage request, int categoryId)
        {
            try
            {
                int schoolId = Int32.Parse(User.Identity.GetSchoolId());
                _loggingService.Debug("Into Uniform/GetUniformPacks");
                _loggingService.Info("SchoolId:" + schoolId);

                List<UniformPack> products = _service.GetUniformPacksByCategory(schoolId, categoryId);

                if (products == null || products.Count <= 0)
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, products);
                }
            }
            catch (Exception ex)
            {
                _loggingService.Debug("Exception Occurred:" + ex.Message + "Inner Exception:" + ex.InnerException);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize]
        [Route("api/Uniform/GetBasket/{basketId}")]
        public HttpResponseMessage GetCart(HttpRequestMessage request, int basketId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetCart");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            List<Models.UniformBasketItem> basket = _service.GetBasketItems(basketId, parentId, schoolId);

            if (basket == null || basket.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, basket);
            }
        }

        [Authorize]
        [Route("api/Uniform/AddToBasket/")]
        public HttpResponseMessage Post(HttpRequestMessage request, UniformCartItem item)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/AddToBasket");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            int basketId = _service.AddToBasket(parentId, schoolId, item);
            if (basketId != 0)
            {
                //fetch count
                BasketOperationResult result = new BasketOperationResult();
                result.basketId = basketId;
                result.currentBasketCount = _service.GetBasketCount(basketId, parentId, schoolId);
                return request.CreateResponse(HttpStatusCode.OK, result);
            }

            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [Authorize]
        [Route("api/Uniform/UpdateBasket/")]
        public HttpResponseMessage Put(HttpRequestMessage request, UpdateCartInput updateCart)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/UpdateBasket");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            bool updateResult = _service.UpdateBasket(parentId, schoolId, updateCart);
            if (updateResult)
            {
                //fetch count
                //int currentBasketCount = updateCart.status == RemoveCartStatus.CART ? 0 : _service.GetBasketCount(updateCart.basketId, parentId, schoolId);
                //return request.CreateResponse(HttpStatusCode.OK, currentBasketCount);

                List<Models.UniformBasketItem> basket = _service.GetBasketItems(updateCart.basketId, parentId, schoolId);

                if (basket == null || basket.Count <= 0)
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, basket);
                }
            }

            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [Authorize]
        [Route("api/Uniform/GetProductStock/{productId}/{sizeAttributeId}/{colorAttributeId}")]
        public HttpResponseMessage GetProductStock(HttpRequestMessage request, int productId, int? sizeAttributeId, int? colorAttributeId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetProductStock");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            StockInput input = new StockInput();
            input.schoolId = schoolId;
            input.productId = productId;
            input.colorAttributeId = colorAttributeId;
            input.sizeAttributeId = sizeAttributeId;

            if (productId == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            int? stock = _service.GetProductStock(input);
            return request.CreateResponse(HttpStatusCode.OK, stock);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Uniform/GetPaymentIntent")]
        public HttpResponseMessage GetPaymentIntent(HttpRequestMessage request, [FromBody] StripeUniformPaymentRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetPaymentIntent");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            _loggingService.Debug("Into Uniform/StripePayment");
            _loggingService.Info("SchoolId:" + schoolId + " ParentId:" + parentId);

            //1. Get Basket
            List<Models.UniformBasketItem> basketItems = _service.GetBasketItems(paymentRequest.basketId, parentId, schoolId);

            if (basketItems == null || basketItems.Count <= 0)
            {
                _loggingService.Info("BasketItems not found while ordering uniform :: SchoolId:" + schoolId + " ParentId:" + parentId + " BasketId:" + paymentRequest.basketId);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }


            //check stock
            UniformStockCheckResult checkStockResult = _service.CheckStock(schoolId, basketItems);
            if (checkStockResult.arePrductsOutOfStock)
            {
                _loggingService.Debug("Proucts are out of stock while ordering uniform : " + checkStockResult.checkStockMessage);
                return request.CreateResponse(HttpStatusCode.BadRequest, checkStockResult.checkStockMessage);
            }

            //2. Update basket with student Id class and comment
            Models.UniformBasket basket = _service
                                            .UpdateBasket(
                                                paymentRequest.basketId,
                                                parentId, schoolId,
                                                paymentRequest.studentId,
                                                paymentRequest.studentClass,
                                                paymentRequest.comments,
                                                 paymentRequest.firstName);
            if (basket == null)
            {
                _loggingService.Info("Basket update error while ordering uniform :: SchoolId:" + schoolId + " ParentId:" + parentId + " BasketId:" + paymentRequest.basketId);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            //3. create order
            Models.Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null || parent.id == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            Models.UniformOrderData uniformOrderData = null;
            try
            {
                uniformOrderData = _service.CreateOrder(schoolId, paymentRequest.basketId, parent, paymentRequest.topupMethod, paymentRequest.collectionOption);
                _loggingService.Debug("Uniform order created with order id : " + uniformOrderData.OrderId);
            }
            catch (Exception ex)
            {
                _loggingService.Debug("Exception occurred while ordering uniform : " + ex.Message + "InnerException:" + ex.InnerException);
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            if (paymentRequest.topupMethod.Equals("CC"))
            {
                if (uniformOrderData != null)
                {
                    //4. create payment
                    Models.UniformOrderPayment uniformOrderPayment = _service.CreatePayment(uniformOrderData, basket, basketItems, paymentRequest.collectionOption, false);
                    if (uniformOrderPayment == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }

                    Models.PaymentGatewayTransactionLog log = _service.CreatePaymentGateayTransactionLog(uniformOrderPayment);

                    
                    try
                    {
                        PaymentIntent paymentIntent = _stripechargeService.GetPaymentIntent(parentId, schoolId, log.GrossAmount.Value, log.Id, "_Uniform_Checkout", parent.email);
                        IDictionary<string, Models.PaymentGatewayTransactionLog> result = new Dictionary<string, Models.PaymentGatewayTransactionLog>(); ;
                        result.Add(JsonConvert.SerializeObject(paymentIntent), log);
                        return request.CreateResponse(HttpStatusCode.OK, result);

                    }
                    catch (PaymentException ex)
                    {
                        return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        _loggingService.Debug("Exception occurred while ordering uniform : " + ex.Message + "InnerException:" + ex.InnerException);
                        return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    }
                }
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, uniformOrderData.OrderId);
            }
            return null;
        }


        [Authorize]
        [Route("api/Uniform/StripePayment")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]StripeUniformPaymentRequest paymentRequest)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/StripePayment");
            _loggingService.Info("SchoolId:" + schoolId + " ParentId:" + parentId);

            //1. Get Basket
            List<Models.UniformBasketItem> basketItems = _service.GetBasketItems(paymentRequest.basketId, parentId, schoolId);

            if (basketItems == null || basketItems.Count <= 0)
            {
                _loggingService.Info("BasketItems not found while ordering uniform :: SchoolId:" + schoolId + " ParentId:" + parentId + " BasketId:" + paymentRequest.basketId);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }


            //check stock
            UniformStockCheckResult checkStockResult = _service.CheckStock(schoolId, basketItems);
            if (checkStockResult.arePrductsOutOfStock)
            {
                _loggingService.Debug("Proucts are out of stock while ordering uniform : " + checkStockResult.checkStockMessage);
                return request.CreateResponse(HttpStatusCode.BadRequest, checkStockResult.checkStockMessage);
            }

            //2. Update basket with student Id class and comment
            Models.UniformBasket basket = _service
                                            .UpdateBasket(
                                                paymentRequest.basketId,
                                                parentId, schoolId,
                                                paymentRequest.studentId,
                                                paymentRequest.studentClass,
                                                paymentRequest.comments,
                                                 paymentRequest.firstName);
            if (basket == null)
            {
                _loggingService.Info("Basket update error while ordering uniform :: SchoolId:" + schoolId + " ParentId:" + parentId + " BasketId:" + paymentRequest.basketId);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            //3. create order
            Models.Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null || parent.id == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            Models.UniformOrderData uniformOrderData = null;
            try
            {
                uniformOrderData = _service.CreateOrder(schoolId, paymentRequest.basketId, parent, paymentRequest.topupMethod, paymentRequest.collectionOption);
                _loggingService.Debug("Uniform order created with order id : " + uniformOrderData.OrderId);
            }
            catch (Exception ex)
            {
                _loggingService.Debug("Exception occurred while ordering uniform : " + ex.Message + "InnerException:" + ex.InnerException);
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            if (paymentRequest.topupMethod.Equals("CC"))
            {
                if (uniformOrderData != null)
                {
                    //4. create payment
                    Models.UniformOrderPayment uniformOrderPayment = _service.CreatePayment(uniformOrderData, basket, basketItems, paymentRequest.collectionOption, false);
                    if (uniformOrderPayment == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }

                    Models.PaymentGatewayTransactionLog log = _service.CreatePaymentGateayTransactionLog(uniformOrderPayment);

                    //5.make Payment and place order
                    try
                    {
                        PaymentResult paymentResult = _stripechargeService
                                                        .CreatePaymentCharge(parentId,
                                                                             schoolId,
                                                                             paymentRequest.stripeToken,
                                                                             uniformOrderPayment.GrossAmount.Value,
                                                                             log.Id,
                                                                             "_Uniform_Checkout", parent.email, paymentRequest.saveCard, uniformOrderData.OrderId.Value);
                        if (paymentResult.paymentStatus)
                        {
                            //place order
                            _service.MakeOrderSuccess(uniformOrderPayment, paymentResult.paymentGatewayTransactionId, log, schoolId);
                            //UpdateStock
                            _service.UpdateStock(schoolId, uniformOrderData, basketItems);
                            _emailService.SendUniformOrderEmail(uniformOrderData.OrderId.Value, parentId, schoolId, parent.email, parent.firstName + ' ' + parent.lastName, uniformOrderData.OrderedDate.Value, paymentRequest.studentClass, paymentRequest.comments, paymentRequest.collectionOption);

                            return request.CreateResponse(HttpStatusCode.OK, uniformOrderData.OrderId);
                        }
                        else
                        {
                            _transactionService.UpdateTransaction("[Payment Failure]" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                            return request.CreateResponse(HttpStatusCode.PaymentRequired, "Payment Has Failed");
                        }

                    }
                    catch (PaymentException ex)
                    {
                        return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
                    }
                    catch (PlaceOrderException ex)
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        _loggingService.Debug("Exception occurred while ordering uniform : " + ex.Message + "InnerException:" + ex.InnerException);
                        return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    }
                }
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, uniformOrderData.OrderId);
            }
            return null;
        }

        [Authorize]
        [HttpPost]
        [Route("api/Uniform/placeOrderAfterPayment")]
        public HttpResponseMessage placeOrderAfterPayment(HttpRequestMessage request, [FromBody] OrderAfterPaymentParams reqParams)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            StripeUniformPaymentRequest paymentRequest = reqParams.uniformPaymentRequest;
            Models.PaymentGatewayTransactionLog log = reqParams.log;

            Models.Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null || parent.id == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            List<Models.UniformBasketItem> basketItems = _service.GetBasketItems(paymentRequest.basketId, parentId, schoolId);

            
            Models.UniformOrderPayment uniformOrderPayment = _service.GetUniformOrderPayment(log);

            Models.UniformOrderData uniformOrderData = _service.GetUniformOrderData(paymentRequest.basketId, uniformOrderPayment);
            


            try
            {

                //place order
                _service.MakeOrderSuccess(uniformOrderPayment, log.PGTransId, log, schoolId);
                //UpdateStock
                _service.UpdateStock(schoolId, uniformOrderData, basketItems);
                _emailService.SendUniformOrderEmail(uniformOrderData.OrderId.Value, parentId, schoolId, parent.email, parent.firstName + ' ' + parent.lastName, uniformOrderData.OrderedDate.Value, paymentRequest.studentClass, paymentRequest.comments, paymentRequest.collectionOption);

                return request.CreateResponse(HttpStatusCode.OK, uniformOrderData.OrderId);


            }
            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
            catch (Exception ex)
            {
                _loggingService.Debug("Exception occurred while ordering uniform : " + ex.Message + "InnerException:" + ex.InnerException);
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        [Authorize]
        [Route("api/Uniform/GetUniformOrders/{year}")]
        public HttpResponseMessage GetUniformOrders(HttpRequestMessage request, int year)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());



            _loggingService.Debug("Into Uniform/GetUniformOrders");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            List<UniformOrder> orders = _service.GetParentOrders(parentId, schoolId, year);
            if (orders == null || orders.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            return request.CreateResponse(HttpStatusCode.OK, orders);
        }

        [Authorize]
        [Route("api/Uniform/GetUniformOrderDetails/{orderId}")]
        public HttpResponseMessage GetUniformOrderDetails(HttpRequestMessage request, int orderId)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetUniformOrderDetails");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            List<OrderDetails> orderDetails = _service.GetUniformOrderDetails(schoolId, parentId, orderId);
            if (orderDetails == null || orderDetails.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            return request.CreateResponse(HttpStatusCode.OK, orderDetails);
        }


        [Authorize]
        [Route("api/Uniform/getUniformSchoolDetails")]
        public HttpResponseMessage getUniformSchoolDetails(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetUniformOrderDetails");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            UniformCheckoutSchoolDetails details = _service.getUniformSchoolDetails(schoolId);
            if (details == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            return request.CreateResponse(HttpStatusCode.OK, details);
        }

        [Authorize]
        [HttpPost]
        [Route("api/Uniform/PayUsingBalance")]
        public HttpResponseMessage PayUsingBalance(HttpRequestMessage request, [FromBody]StripeUniformPaymentRequest paymentRequest)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/StripePayment");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            //1. Get Basket
            List<Models.UniformBasketItem> basketItems = _service.GetBasketItems(paymentRequest.basketId, parentId, schoolId);

            if (basketItems == null || basketItems.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            Models.Parent parent = _parentService.GetParent(schoolId, parentId);
            if (parent == null || parent.id == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            decimal orderAmount = basketItems.Sum(e => (e.price * (decimal)e.quantity));

            if (orderAmount > parent.credit)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Order price is greater than available credit");
            }

            //2. Update basket with student Id class and comment
            Models.UniformBasket basket = _service
                                            .UpdateBasket(
                                                paymentRequest.basketId,
                                                parentId, schoolId,
                                                paymentRequest.studentId,
                                                paymentRequest.studentClass,
                                                paymentRequest.comments,
                                                paymentRequest.firstName);
            if (basket == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            paymentRequest.topupMethod = "CC";
            //3. create order
            Models.UniformOrderData uniformOrderData = _service.CreateOrder(schoolId, paymentRequest.basketId, parent, paymentRequest.topupMethod, paymentRequest.collectionOption);

            //4. create payment
            Models.UniformOrderPayment uniformOrderPayment = _service.CreatePayment(uniformOrderData, basket, basketItems, paymentRequest.collectionOption, true);
            if (uniformOrderPayment == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            // Models.PaymentGatewayTransactionLog log = _service.CreatePaymentGateayTransactionLog(uniformOrderPayment);

            //5.make Payment and place order
            try
            {
                //add entry to transaction table: TODO

                if (_parentService.DeductParentBalance(parent.id, schoolId, uniformOrderPayment.GrossAmount.Value))
                {
                    //place order
                    _service.MakeOrderSuccess(uniformOrderPayment, schoolId, parent);
                    _emailService.SendUniformOrderEmail(uniformOrderData.OrderId.Value, parentId, schoolId, parent.email, parent.firstName + ' ' + parent.lastName, uniformOrderData.OrderedDate.Value, paymentRequest.studentClass, paymentRequest.comments, paymentRequest.collectionOption);
                    //UpdateStock
                    _service.UpdateStock(schoolId, uniformOrderData, basketItems);
                    return request.CreateResponse(HttpStatusCode.OK, uniformOrderData.OrderId);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.PaymentRequired, "Payment Has Failed");
                }

            }
            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
            return null;
        }

        [Authorize]
        [Route("api/Uniform/GetUniformPackProducts/{id}")]
        public HttpResponseMessage GetUniformPackProducts(HttpRequestMessage request, int id)
        {
            int schoolId = int.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("api/Uniform/GetUniformPackProducts/{id}");
            _loggingService.Info("schoolId" + schoolId);
            var result = _service.GetUniformPackProducts(schoolId, id);
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [Route("api/Uniform/GetUniformPackOrderItemDetails/{itemId}")]
        public HttpResponseMessage GetUniformPackOrderItemDetails(HttpRequestMessage request, int itemId)
        {
            int schoolId = int.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("api/Uniform/GetUniformPackOrderItemDetails/{itemId}");
            
            List<Models.UniformPackItemProduct> uPackDetails = _service.GetAllUPackSubItem(itemId);
            if (uPackDetails == null || uPackDetails.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            return request.CreateResponse(HttpStatusCode.OK, uPackDetails);
        }

        [Authorize]
        [Route("api/Uniform/GetCollectionOptions")]
        public HttpResponseMessage GetCollectionOptions(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetCollectionOptions");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            List<CollectionOption> collectionOptions = _service.GetCollectionOptions(schoolId);
            return request.CreateResponse(HttpStatusCode.OK, collectionOptions);

        }

        [Authorize]
        [Route("api/Uniform/GetUniformAddress")]
        public HttpResponseMessage GetUniformAddress(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/GetUniformAddress");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);

            Models.UniformAddress uniformAddress = _service.GetUniformAddress(parentId);
            return request.CreateResponse(HttpStatusCode.OK, uniformAddress);

        }

        [Authorize]
        [HttpPost]
        [Route("api/Uniform/AddUniformAddress")]
        public HttpResponseMessage AddUniformAddress(HttpRequestMessage request, [FromBody]Models.UniformAddress uniformAddress)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/AddUniformAddress");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);
            uniformAddress.schoolId = schoolId;
            uniformAddress.parentId = parentId;

            uniformAddress = _service.AddUniformAddress(uniformAddress);
            return request.CreateResponse(HttpStatusCode.OK, uniformAddress);

        }

        [Authorize]
        [HttpPost]
        [Route("api/Uniform/UpdateUniformAddress")]
        public HttpResponseMessage UpdateUniformAddress(HttpRequestMessage request, [FromBody] Models.UniformAddress uniformAddress)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Uniform/AddUniformAddress");
            _loggingService.Info("SchoolId:" + schoolId + " ParentID" + parentId);
            uniformAddress.schoolId = schoolId;
            uniformAddress.parentId = parentId;

            uniformAddress = _service.UpdateUniformAddress(uniformAddress);
            return request.CreateResponse(HttpStatusCode.OK, uniformAddress);

        }

    }
}
