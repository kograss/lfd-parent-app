﻿using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Commands;
using CanteenMigration.Models;
using CanteenMigration.IdentityHelper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services.exception;
using Newtonsoft.Json;

namespace CanteenMigration.Controllers
{
    public class StripeController : ApiController
    {
        private IOrderService _service;
        private ILoggerService<StripeController> _loggingService;
        private IPaymentService _paymentService;
        private ITransactionService _transactionService;
        private ICartService _cartService;
        private ISchoolService _schoolService;
        private IParentService _parentService;
        private IStripePaymentChargeService _stripechargeService;
        private IOrderCheckService _orderCheckService;

        private const String CANTEEN_ORDER_TRANSACTION_TYPE = "_Canteen_Checkout";
        private const String PAYMENT_GATEWAY = "Stripe";

        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";


        public StripeController(IOrderService service, ILoggerService<StripeController> loggingService, IPaymentService _paymentService,
                                ITransactionService _transactionService, ICartService _cartService, ISchoolService _schoolService,
                                IParentService _parentService, IStripePaymentChargeService _stripechargeService, IOrderCheckService orderCheckService)
        {
            this._service = service;
            this._loggingService = loggingService;
            this._paymentService = _paymentService;
            this._transactionService = _transactionService;
            this._cartService = _cartService;
            this._schoolService = _schoolService;
            this._parentService = _parentService;
            this._stripechargeService = _stripechargeService;
            this._orderCheckService = orderCheckService;
        }

        [Authorize]
        [Route("api/GetCustomerStripeDetails")]
        public HttpResponseMessage GetCustomerStripeDetails(HttpRequestMessage request)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            _loggingService.Debug("Into Stripe/GetCustomerStripeDetails");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);
            Models.CustomerStripeDetails cust = this._stripechargeService.GetStripeDetails(parentId, schoolId);
            return request.CreateResponse(HttpStatusCode.OK, cust);
        }

        [Authorize]
        [HttpPost]
        [Route("api/GetPaymentIntent")]
        public HttpResponseMessage GetPaymentIntent(HttpRequestMessage request, [FromBody] StripePaymentRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Stripe/GetPaymentIntent");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            DateTime deliveryDate = paymentRequest.orderItems.First().deliveryDate;
            List<int> orderIds = new List<int>();
            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);

            }
            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);

            }

            //SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            List<CanteenShopcartItem> cartItems = _cartService.GetAllCartItems(parentId, schoolId, deliveryDate);

            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            //decimal bagCharges = schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            decimal bagCharges = 0;
            decimal chargesPerOrder = 0;
            if (schoolInfo.fee_model == 1)
            {
                chargesPerOrder = 0;
            }
            else
            {
                if (parent.parentFeePlan == 1)
                {
                    chargesPerOrder = 0.25M;
                    if (schoolInfo.canteenOrderServiceFee != null)
                    {
                        chargesPerOrder = schoolInfo.canteenOrderServiceFee.Value;
                    }
                }

            }

            if (schoolInfo.chargeforbag == "YES")
            {
                paymentRequest.orderItems.ForEach(order =>
                {
                    if (order.bagType == "buy" && order.bagPrice.HasValue)
                    {
                        bagCharges = bagCharges + order.bagPrice.Value;
                    }
                });

            }

            decimal totalOrderAmount = calculateTotalAmount(schoolInfo, parent, deliveryDate, paymentRequest.orderItems.Count, cartItems, bagCharges, chargesPerOrder);
            if (paymentRequest.isBalancePlusCard)
            {
                totalOrderAmount = totalOrderAmount - parent.credit.Value;
            }
            try
            {
                if (_orderCheckService.CheckIfOrderCanBePlace(schoolInfo, parent, deliveryDate, cartItems, totalOrderAmount, false, false, true))
                {
                    PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "_Canteen_Checkout", "STRIPE", true);
                    PaymentIntent paymentIntent = _stripechargeService.GetPaymentIntent(parentId, schoolId, log.GrossAmount.Value, log.Id, "_Canteen_Checkout", parent.email);
                    IDictionary<string, PaymentGatewayTransactionLog> result = new Dictionary<string, PaymentGatewayTransactionLog>(); ;
                    result.Add(JsonConvert.SerializeObject(paymentIntent), log);
                    return request.CreateResponse(HttpStatusCode.OK, result);

                }
            }
            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            return null;
        }

        [Authorize]
        [Route("api/PayAndPlaceOrder")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody] StripePaymentRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Stripe/PayAndPlaceOrder");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            DateTime deliveryDate = paymentRequest.orderItems.First().deliveryDate;
            List<int> orderIds = new List<int>();
            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);

            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);

            }
            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);

            }

            //SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            List<CanteenShopcartItem> cartItems = _cartService.GetAllCartItems(parentId, schoolId, deliveryDate);

            if (cartItems == null || cartItems.Count() <= 0)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, CARTNOTFOUND);
            }

            //decimal bagCharges = schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            decimal bagCharges = 0;
            decimal chargesPerOrder = 0;
            if (schoolInfo.fee_model == 1)
            {
                chargesPerOrder = 0;
            }
            else
            {
                if (parent.parentFeePlan == 1)
                {
                    chargesPerOrder = 0.25M;
                    if (schoolInfo.canteenOrderServiceFee != null)
                    {
                        chargesPerOrder = schoolInfo.canteenOrderServiceFee.Value;
                    }
                }

            }

            if (schoolInfo.chargeforbag == "YES")
            {
                paymentRequest.orderItems.ForEach(order =>
                {
                    if (order.bagType == "buy" && order.bagPrice.HasValue)
                    {
                        bagCharges = bagCharges + order.bagPrice.Value;
                    }
                });

            }

            decimal totalOrderAmount = calculateTotalAmount(schoolInfo, parent, deliveryDate, paymentRequest.orderItems.Count, cartItems, bagCharges, chargesPerOrder);
            if (paymentRequest.isBalancePlusCard)
            {
                totalOrderAmount = totalOrderAmount - parent.credit.Value;
            }
            try
            {
                if (_orderCheckService.CheckIfOrderCanBePlace(schoolInfo, parent, deliveryDate, cartItems, totalOrderAmount, false, false, true))
                {
                    PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, totalOrderAmount, "_Canteen_Checkout", "STRIPE", true);
                    PaymentResult paymentResult = _stripechargeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken,
                        log.GrossAmount.Value, log.Id, "_Canteen_Checkout", parent.email, paymentRequest.saveCard);

                    if (paymentResult.paymentStatus)
                    {
                        try
                        {
                            OrderResult result = _service.PlaceOrder(paymentRequest.orderItems, deliveryDate, schoolInfo, parent, false,
                                                    paymentResult.paymentGatewayTransactionId, cartItems, bagCharges, chargesPerOrder,
                                                    paymentRequest.isBalancePlusCard);
                            _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                            return request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        catch (PlaceOrderException ex)
                        {
                            _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);

                            if (ex.InnerException != null)
                            {
                                _loggingService.Debug(ex.InnerException.ToString());
                            }

                            if (ex.StackTrace != null)
                            {
                                _loggingService.Debug(ex.StackTrace);
                            }

                            if (!string.IsNullOrEmpty(ex.Message))
                            {
                                _loggingService.Debug(ex.Message);
                            }
                            //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                            //return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);

                            return request.CreateResponse(
                                HttpStatusCode.PaymentRequired,
                                "Payment was successful. There was an issue while placing the order. Please check the status of your order "
                                + paymentResult.paymentGatewayTransactionId + " with School24 before you make another payment."
                                );
                        }
                        catch (Exception ex)
                        {
                            _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);

                            if (ex.InnerException != null)
                            {
                                _loggingService.Debug(ex.InnerException.ToString());
                            }

                            if (ex.StackTrace != null)
                            {
                                _loggingService.Debug(ex.StackTrace);
                            }

                            if (!string.IsNullOrEmpty(ex.Message))
                            {
                                _loggingService.Debug(ex.Message);
                            }

                            //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                            return request.CreateResponse(
                                HttpStatusCode.PaymentRequired,
                                "Payment was successful. There was an issue while placing the order. Please check the status of your order "
                                + paymentResult.paymentGatewayTransactionId + " with School 24 before you make another payment."
                                );
                        }
                    }
                    else
                    {
                        _transactionService.UpdateTransaction("[Payment Failure]" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        // return request.CreateResponse(HttpStatusCode.PaymentRequired, "Payment has Failed. Note down the Transaction Id for reference " + log.Id);
                        return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + paymentResult.paymentGatewayTransactionId + " with School 24 before you make another payment."
                                 );
                    }
                }
            }
            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }

            return null;
        }

        [Authorize]
        [HttpPost]
        [Route("api/placeOrderAfterPayment")]
        public HttpResponseMessage placeOrderAfterPayment(HttpRequestMessage request, [FromBody] OrderAfterPaymentParams reqParams)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            StripePaymentRequest paymentRequest = reqParams.paymentRequest;
            PaymentGatewayTransactionLog log = reqParams.log;
            DateTime deliveryDate = paymentRequest.orderItems.First().deliveryDate;
            List<int> orderIds = new List<int>();
            // OrderResult result = new OrderResult();

            //checks before placing order
            //string checksBeforePlacingOrder = _service.IsOrderCanBePlaced(schoolId, deliveryDate);


            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);

            Parent parent = _parentService.GetParent(schoolId, parentId);


            //SchoolInfo schoolInfo = JsonConvert.DeserializeObject<SchoolInfo>(schoolInfoJSON);
            List<CanteenShopcartItem> cartItems = _cartService.GetAllCartItems(parentId, schoolId, deliveryDate);

            //decimal bagCharges = schoolInfo.chargeforbag == "YES" ? schoolInfo.bagprice.Value : 0;
            decimal bagCharges = 0;
            decimal chargesPerOrder = 0;
            if (schoolInfo.fee_model == 1)
            {
                chargesPerOrder = 0;
            }
            else
            {
                if (parent.parentFeePlan == 1)
                {
                    chargesPerOrder = 0.25M;
                    if (schoolInfo.canteenOrderServiceFee != null)
                    {
                        chargesPerOrder = schoolInfo.canteenOrderServiceFee.Value;
                    }
                }

            }

            if (schoolInfo.chargeforbag == "YES")
            {
                paymentRequest.orderItems.ForEach(order =>
                {
                    if (order.bagType == "buy" && order.bagPrice.HasValue)
                    {
                        bagCharges = bagCharges + order.bagPrice.Value;
                    }
                });

            }


            try
            {
                OrderResult result = _service.PlaceOrder(paymentRequest.orderItems, deliveryDate, schoolInfo, parent, false,
                                        log.PGTransId, cartItems, bagCharges, chargesPerOrder,
                                        paymentRequest.isBalancePlusCard);
                _transactionService.UpdateTransaction("[Success]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (PlaceOrderException ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }
                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                //return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }
            catch (Exception ex)
            {
                _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + log.PGTransId + "]", log, schoolId, log.PGTransId);

                if (ex.InnerException != null)
                {
                    _loggingService.Debug(ex.InnerException.ToString());
                }

                if (ex.StackTrace != null)
                {
                    _loggingService.Debug(ex.StackTrace);
                }

                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _loggingService.Debug(ex.Message);
                }

                //_loggingService.Debug("STACK:" + ex.StackTrace + " MESSAGE:" + ex.Message + " INNER-EXCEPTION:" + ex.InnerException);

                return request.CreateResponse(
                                 HttpStatusCode.PaymentRequired,
                                 "There was an issue while making the payment. Please check the status of your order "
                                 + log.PGTransId + " with School24 before you make another payment."
                                 );
            }

        }

        private decimal calculateTotalAmount(SchoolInfo schoolInfo, Parent parent, DateTime deliveryDate, int numberOfStudents, List<CanteenShopcartItem> cartItems, decimal bagCharges, decimal chargesPerOrder)
        {
            int dayOfWeek = (int)deliveryDate.DayOfWeek + 1;
            return _cartService.GetCartAmout(cartItems, schoolInfo.schoolid) + (bagCharges) + (chargesPerOrder * numberOfStudents);
        }
    }
}
