﻿using CanteenMigration.Services.Commons;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
       

        // GET api/values/5
        [AllowAnonymous]
        [HttpGet]
        public string Get(string pwd)
        {
            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(pwd);

            return encryptedPassword;
        }

        [AllowAnonymous]
        [HttpGet]
        public string Get()
        {
            var version = ConfigurationManager.AppSettings["mobileappversion"];
            return version;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Values/androidmobileappversion/")]
        public string androidmobileappversion()
        {
            var version = ConfigurationManager.AppSettings["androidmobileappversion"];
            return version;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Values/iosmobileappversion/")]
        public string iosmobileappversion()
        {
            var version = ConfigurationManager.AppSettings["iosmobileappversion"];
            return version;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
