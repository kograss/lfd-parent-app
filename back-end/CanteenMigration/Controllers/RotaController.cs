﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class RotaController : ApiController
    {
        private IRotaService _service;
        public ILoggerService<RotaController> _loggingService;

        public RotaController(IRotaService service, ILoggerService<RotaController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;
        }

        [Authorize]
        [Route("api/Rota/GetParentsShifts")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Rota/GetParentsShifts");
            _loggingService.Info("Parent Id:" + parentId + " schoolId:" + schoolId);

            List<ParentsShift> parentsShifts = _service.GetParentShifts(parentId, schoolId);
            if (parentsShifts == null || parentsShifts.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, parentsShifts);
            }
        }

        [Authorize]
        [Route("api/Rota/GetRotaNews")]
        public HttpResponseMessage GetNews(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Rota/GetRotaNews");
            _loggingService.Info("Parent Id:" + parentId + " schoolId:" + schoolId);

            List<RosterNews> rosterNews = _service.GetRosterNews(schoolId);
            if (rosterNews == null || rosterNews.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, rosterNews);
            }
        }

        [Authorize]
        [Route("api/Rota/GetMonthsShifts/{month}/{year}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int month, int year)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Rota/GetRotaNews");
            _loggingService.Info("Parent Id:" + parentId + " schoolId:" + schoolId);

            List<ShiftsByDate> shiftsByDate = _service.GetMonthsShiftsAndVolunteers(schoolId, month, year);
            if (shiftsByDate == null || shiftsByDate.Count <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, shiftsByDate);
            }
        }

        [Authorize]
        [Route("api/Rota/RegisterForShift/")]
        public HttpResponseMessage Post(HttpRequestMessage request, RegisterVolunteerInput registerVolInput)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Rota/RegisterForShift");
            _loggingService.Info("Parent Id:" + parentId + " schoolId:" + schoolId);

            string registerMessage = string.Empty;
            bool registerResult = _service
                                        .RegisterVolunteerToShift
                                        (schoolId, registerVolInput.shiftId, parentId, registerVolInput.date, registerVolInput.shiftName, ref registerMessage);

                                   

            if (registerResult)
            {
                return request.CreateResponse(HttpStatusCode.OK,registerResult);
            }

            else
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, registerMessage);
            }
        }
    }
}
