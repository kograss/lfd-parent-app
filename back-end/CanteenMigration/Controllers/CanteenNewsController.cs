﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.IdentityHelper;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace CanteenMigration.Controllers
{


    public class CanteenNewsController : ApiController
    {
        private ICanteenNewsService _service;
        public ILoggerService<CanteenNewsController> _loggingService;


        public CanteenNewsController(ICanteenNewsService service, ILoggerService<CanteenNewsController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/CanteenNews")]
        [ResponseType(typeof(List<JSONModels.CanteenNewsModel>))]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            _loggingService.Debug("Into CanteenNews");
            _loggingService.Info("SchoolId " + schoolId);

            var news = _service.GetCanteenNews(schoolId);
            if (news == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, news);
            }

        }
    }
}

