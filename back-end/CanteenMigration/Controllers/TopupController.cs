﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using CanteenMigration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CanteenMigration.Controllers
{
    public class TopupController : ApiController
    {
        private const String CANTEEN_TOPUP_TRANSACTION_TYPE = "TOPUP";
        private const String PAYMENT_GATEWAY = "STRIPE";

        private IStripePaymentChargeService _stripeService;
        public ILoggerService<StudentController> _loggingService;
        private ITransactionService _transactionService;
        private ITopupService _topupService;
        private IParentService _parentSerice;
        public TopupController(IStripePaymentChargeService service, ILoggerService<StudentController> loggingService, ITransactionService transactionService,
                                ITopupService topupService, IParentService _parentSerice)
        {
            this._stripeService = service;
            this._loggingService = loggingService;
            this._transactionService = transactionService;
            this._topupService = topupService;
            this._parentSerice = _parentSerice;
        }


        [Authorize]
        [Route("api/Topup")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]StripePaymentRequest paymentRequest)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());
            var parent = _parentSerice.GetParent(parentId);
            _loggingService.Debug("Into Topup");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, paymentRequest.amount,
                                                                       CANTEEN_TOPUP_TRANSACTION_TYPE, PAYMENT_GATEWAY, true
                                                                       );
            //add topup with status - START
            Topup topup = _topupService.AddTopup(parentId, schoolId, paymentRequest.amount,
                                                 log.GrossAmount.Value, log.BankFee.Value, log.School24Fee.Value
                                                 );

            PaymentResult paymentResult = _stripeService.CreatePaymentCharge(parentId, schoolId,
                                                                             paymentRequest.stripeToken, log.GrossAmount.Value,
                                                                             log.Id, "Topup", parent.email, false);


            TopupResult result = new TopupResult();

            if (paymentResult.paymentStatus)
            {

                //complete topup and update transaction log
                _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]",
                                                      log, schoolId, paymentResult.paymentGatewayTransactionId);

                result.parentCredit = _topupService.CompleteTopup(topup, paymentResult.paymentGatewayTransactionId);
                result.isSucess = true;
                result.message = "Topup successful";
            }
            else
            {
                //update transction log and return error message
                _transactionService.UpdateTransaction("Failed" + "[FAILED: " + paymentResult.message + "]",
                                                      log, schoolId, paymentResult.paymentGatewayTransactionId);

                _topupService.FailedTopup(topup, paymentResult.paymentGatewayTransactionId);

                result.isSucess = false;
                result.message = paymentResult.message;
            }

            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [Route("api/Topup/Manualtopup")]
        public HttpResponseMessage Post(HttpRequestMessage request, ManualTopupInput manualTopupInput)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Topup/manul");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            manualTopupInput.schoolId = schoolId;
            manualTopupInput.parentId = parentId;

            if (_topupService.CompleteManulTopup(manualTopupInput))
            {
                return request.CreateResponse(HttpStatusCode.OK, "Topup has been done successfully");
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, "Error while completing topup. Please contact admin.");
            }
        }
    }
}
