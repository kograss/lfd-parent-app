﻿using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Commands;
using CanteenMigration.Models;
using CanteenMigration.IdentityHelper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services.exception;
using CanteenMigration.Services.impl;

namespace CanteenMigration.Controllers
{
    public class DonationStripeController : ApiController
    {
        private ILoggerService<DonationStripeController> _loggingService;
        private ITransactionService _transactionService;
        private IStripePaymentChargeService _stripechargeService;
        private IFundRaisingEventService _fundRaisingEventService;
        private ISchoolService _schoolService;
        private IParentService _parentService;
        private IEmailService _emailService;

        private const String CANTEEN_ORDER_TRANSACTION_TYPE = "_Canteen_Checkout";
        private const String PAYMENT_GATEWAY = "Stripe";

        public const string SCHOOLNOTFOUND = "School Not Found";
        public const string PARENTNOTFOUND = "Parent Not Found";
        public const string CARTNOTFOUND = "Cart Not Found";

        public DonationStripeController(ILoggerService<DonationStripeController> loggingService,
                                ITransactionService _transactionService, IStripePaymentChargeService _stripechargeService,
                                IFundRaisingEventService _fundRaisingEventService,
                                ISchoolService _schoolService, IParentService _parentService, IEmailService _emailService)
        {
            this._loggingService = loggingService;
            this._transactionService = _transactionService;
            this._stripechargeService = _stripechargeService;
            this._fundRaisingEventService = _fundRaisingEventService;
            this._schoolService = _schoolService;
            this._parentService = _parentService;
            this._emailService = _emailService;
        }
        [Authorize]
        [Route("api/Stripe/MakeDonation")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]StripeDonationRequest paymentRequest)
        {

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Stripe/MakeDonation");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);
            SchoolInfo schoolInfo = _schoolService.GetSchoolInfo(schoolId);
            if (schoolInfo == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, SCHOOLNOTFOUND);

            }
            Parent parent = _parentService.GetParent(schoolId, parentId);

            if (parent == null)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, PARENTNOTFOUND);
            }

            //1. Add donation with status INPROGREE
            //2. Add payment log 
            //3. Make payment 
            //4. if payment is successful - i) update payment log ii) update donation with status Complete and stripe id and logid
            //5. Else i) updaye payment log ii) update donation with with status Failed and stripe id and logid
            try
            {
                var donations = _fundRaisingEventService.AddDonation(parentId, schoolId, paymentRequest.eventId, paymentRequest.students, "INPROGRESS");
                PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, paymentRequest.amount, "_Donation_Checkout", "STRIPE", false);
                PaymentResult paymentResult = _stripechargeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken, log.GrossAmount.Value, log.Id, "_Donation_Checkout", parent.email, false, paymentRequest.eventId);

                if (paymentResult.paymentStatus)
                {
                    try
                    {
                        var result = _fundRaisingEventService.Update(donations, log.Id, paymentResult.paymentGatewayTransactionId, "COMPLETE");
                        _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);

                        _fundRaisingEventService.AddTransaction(donations, parentId, schoolId, parent.credit.Value, paymentResult.paymentGatewayTransactionId, false);
                        _emailService.SendAsynEmailDonationEvent(parent.email, parent.firstName + ' ' + parent.lastName, paymentRequest.eventName, paymentRequest.amount);

                        return request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    catch (PlaceOrderException ex)
                    {
                        _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        var result = _fundRaisingEventService.Update(donations, log.Id, paymentResult.paymentGatewayTransactionId, "FAILED");

                        if (ex.InnerException != null)
                        {
                            _loggingService.Debug(ex.InnerException.ToString());
                        }

                        if (ex.StackTrace != null)
                        {
                            _loggingService.Debug(ex.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(ex.Message))
                        {
                            _loggingService.Debug(ex.Message);
                        }

                        return request.CreateResponse(
                            HttpStatusCode.PaymentRequired,
                            "Payment was successful. There was an issue while creating a donation. Please check the status of your donation "
                            + paymentResult.paymentGatewayTransactionId + " with School 24 before you make another payment."
                            );
                    }
                    catch (Exception ex)
                    {
                        _transactionService.UpdateTransaction("[Order Failure]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                        var result = _fundRaisingEventService.Update(donations, log.Id, paymentResult.paymentGatewayTransactionId, "FAILED");

                        if (ex.InnerException != null)
                        {
                            _loggingService.Debug(ex.InnerException.ToString());
                        }

                        if (ex.StackTrace != null)
                        {
                            _loggingService.Debug(ex.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(ex.Message))
                        {
                            _loggingService.Debug(ex.Message);
                        }

                        return request.CreateResponse(
                            HttpStatusCode.PaymentRequired,
                            "Payment was successful. There was an issue while creating a donation. Please check the status of your donation "
                            + paymentResult.paymentGatewayTransactionId + " with School 24 before you make another payment."
                            );
                    }
                }
                else
                {
                    _transactionService.UpdateTransaction("[Payment Failure]" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                    var result = _fundRaisingEventService.Update(donations, log.Id, paymentResult.paymentGatewayTransactionId, "FAILED");

                    return request.CreateResponse(
                             HttpStatusCode.PaymentRequired,
                             "There was an issue while making the payment. Please check the status of your donation "
                             + paymentResult.paymentGatewayTransactionId + " with School 24 before you make another payment."
                             );
                }
            }
            catch (PaymentException ex)
            {
                return request.CreateResponse(HttpStatusCode.PaymentRequired, ex.ErrorMessage);
            }
            catch (PlaceOrderException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.ErrorMessage);
            }
        }
    }
}
