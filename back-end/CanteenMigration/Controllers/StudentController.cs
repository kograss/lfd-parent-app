﻿using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Queries;
using CanteenMigration.Models;
using CanteenMigration.IdentityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataDependencies.DataException;

namespace CanteenMigration.Controllers
{

    public class StudentController : ApiController
    {
        private IStudentService _service;
        public ILoggerService<StudentController> _loggingService;

        public StudentController(IStudentService service, ILoggerService<StudentController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/Student/GetStudents/{status}")]
        public HttpResponseMessage Get(HttpRequestMessage request, bool status)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Student/GetStudents");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            List<StudentWithKlass> students = _service.GetStudentsByParent(schoolId, parentId, status);
            if (students == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, students);
            }
        }

        [Authorize]
        [Route("api/Student/GetAllClasses")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());

            _loggingService.Debug("Student/GetAllClasses");
            _loggingService.Info("SchoolId:" + schoolId);

            List<KlassModel> classes = _service.GetAllClasses(schoolId);
            if (classes == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, classes);
            }
        }

        [Authorize]
        [Route("api/Student/AddNew")]
        public HttpResponseMessage Post(HttpRequestMessage request, Student newStudent)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Student/AddNew");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            _service.AddNewStudent(newStudent, schoolId);
            List<StudentWithKlass> students = _service.GetStudentsByParent(schoolId, parentId, false);
            if (students == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, students);
            }
        }

        [Authorize]
        [Route("api/Student/EditStudent")]
        public HttpResponseMessage Put(HttpRequestMessage request, Student newStudent)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Student/EditStudent");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            _service.EditStudent(newStudent, schoolId);
            List<StudentWithKlass> students = _service.GetStudentsByParent(schoolId, parentId, false);
            if (students == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, students);
            }
        }

        [Authorize]
        [Route("api/Student/GetStudentAvailabilityMessage")]
        public HttpResponseMessage GetStudentAvailabilityMessage(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Student/GetStudentAvailabilityMessage");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            List<StudentWithKlass> students = _service.GetStudentsByParent(schoolId, parentId, false);
            GetStudentAvailabilityMessage msg = new GetStudentAvailabilityMessage();

            //var activeStudentCount = students.Where(e => e.status == "ACTIVE").Count();
            //1. Check for students. If no students exist display a message/alert "Please set up students before your order"
            var studentCount = students.Count;
            if (students == null || studentCount <= 0)
            {
                msg.disableOrders = true;
                msg.message = "Please set up students before your order";
                return request.CreateResponse(HttpStatusCode.OK, msg);
            }

            //2. Check status of each student. If all student inactive,  display a message/alert "Please make sure at least one student is ACTIVE before your order"
            var inActiveStudentCount = students.Where(e => e.status == "INACTIVE").Count();
            if (inActiveStudentCount == studentCount)
            {
                msg.disableOrders = true;
                msg.message = "Please make sure at least one student is ACTIVE before your order";
                return request.CreateResponse(HttpStatusCode.OK, msg);
            }

            //3. Checkclass. If all student have class set to none, display a message/alert "Please make sure at least students classes are setup correctly before your order"
            var incorrectClassCount = students.Where(e => e.studentClass == "NONE").Count();
            if (students.Count == incorrectClassCount)
            {
                msg.disableOrders = true;
                msg.message = "Please make sure at least students classes are setup correctly before your order";
                return request.CreateResponse(HttpStatusCode.OK, msg);
            }

            var correctStudentCount = students.Where(e => e.status == "ACTIVE" && e.studentClass != "NONE").Count();
            if (correctStudentCount == 0)
            {
                msg.disableOrders = true;
                msg.message = "Please make sure at least students classes are setup correctly before your order";
                return request.CreateResponse(HttpStatusCode.OK, msg);
            }

            msg.disableOrders = false;
            msg.message = "";
            return request.CreateResponse(HttpStatusCode.OK, msg);


        }
    }
}
