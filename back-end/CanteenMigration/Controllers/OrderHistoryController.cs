﻿using CanteenMigration.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using CanteenMigration.IdentityHelper;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.JSONModels;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using CanteenMigration.DataDependencies.DataException;
using System.Web.Http.Description;

namespace CanteenMigration.Controllers
{

    public class OrderHistoryController : ApiController
    {

        private IOrderHistoryService _service;
        public ILoggerService<OrderHistoryController> _loggingService;

        public OrderHistoryController(IOrderHistoryService service, ILoggerService<OrderHistoryController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/OrderHistory/GetOrderHistory/")]
        [ResponseType(typeof(List<ParentOrder>))]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into OrderHistory/GetOrderHistory");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            int year = DateTime.Now.Year;

            List<ParentOrder> orders = _service.GetParentOrder(schoolId, parentId, true, year);
            List<ParentOrder> ordersFund = null;

            if (schoolId == 72 || schoolId == 37)
            {
                ordersFund = _service.GetFundRaisingOrder(schoolId, parentId, year);
            }

            if (orders == null && ordersFund == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                if (ordersFund != null)
                {
                    List<ParentOrder> allOrders = orders.Concat(ordersFund).ToList();
                    return request.CreateResponse(HttpStatusCode.OK, allOrders);
                }

                return request.CreateResponse(HttpStatusCode.OK, orders);
            }
        }

        [Authorize]
        [Route("api/OrderHistory/GetOrderDetails/{orderId}/{isSpecialOrder}/{eventType}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int orderId, bool isSpecialOrder, string eventType)
        {
            int currentYear = DateTime.Now.Year;
            //TODO: check if schoolid match
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into OrderHistory/GetOrderDetails");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId + " orderId" + orderId);

            int year = DateTime.Now.Year;
            IEnumerable<OrderItemProduct> orderDetails = null;
            if (isSpecialOrder)
            {
                if (eventType == "EVENT")
                {
                    orderDetails = _service.GetOrderDetailsForEvent(schoolId, orderId, true);
                }
                if (eventType == "SCHOOL")
                {
                    orderDetails = _service.GetOrderDetailsForSchoolEvent(schoolId, orderId);
                }
            }
            else
            {
                orderDetails = _service.GetOrderDetails(schoolId, orderId, false);
            }

            if (orderDetails == null || orderDetails.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, orderDetails);
            }
        }

        [Authorize]
        [Route("api/OrderHistory/GetOrderDetailsWithPrice/{orderId}")]
        public HttpResponseMessage GetOrderDetailsWithPrice(HttpRequestMessage request, int orderId)
        {
            int currentYear = DateTime.Now.Year;
            //TODO: check if schoolid match
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into OrderHistory/GetOrderDetailsWithPrice");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId + " orderId" + orderId);

            int year = DateTime.Now.Year;
            ReorderOrderItemProducts orderDetails = _service.GetOrderDetailsForReOrder(schoolId, orderId);
            if (orderDetails == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                if (orderDetails.errorOptionOrderitemProducts != null || orderDetails.errorProductOrderitemProducts != null)
                {
                    return request.CreateResponse(HttpStatusCode.NotFound, orderDetails);
                }
                else
                    return request.CreateResponse(HttpStatusCode.OK, orderDetails);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/OrderHistory/GetTransactionHistory/")]
        [ResponseType(typeof(List<ParentTransaction>))]
        public HttpResponseMessage GetTransactionHistory(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into api/OrderHistory/GetTransactionHistory/");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            int year = DateTime.Now.Year;

            List<ParentTransaction> transactions = _service.GetAllParentsTransactions(parentId, schoolId, year);
            if (transactions == null || transactions.Count() <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, transactions);
            }
        }

        [Authorize]
        [Route("api/OrderHistory/GetFundRaisingEventOrderHistory/")]
        [ResponseType(typeof(List<ParentOrder>))]
        public HttpResponseMessage GetFundRaisingEventOrderHistory(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into OrderHistory/GetFundRaisingEventOrderHistory");
            _loggingService.Info("schoolId:" + schoolId + "parentId: " + parentId);

            int year = DateTime.Now.Year;

            List<ParentOrder> orders = _service.GetParentOrder(schoolId, parentId, true, year);
            if (orders == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, orders);
            }
        }
    }
}
