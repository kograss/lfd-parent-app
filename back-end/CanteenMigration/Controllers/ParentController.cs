﻿using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using System.Web.Http.Description;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.Services;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.Models;
using CanteenMigration.Services.Commons;
using System.Configuration;

namespace CanteenMigration.Controllers
{
    public class UpdateParentPasswordTeamInput
    {
        public string newPassword;
        public string secretKey;
        public int parentId;
    }
    public class ParentController : ApiController
    {

        private IParentService _service;
        public ILoggerService<ParentController> _loggingService;
        private ITransactionService _transactionService;
        private IStripePaymentChargeService _stripechargeService;
        private ISchoolService _schoolService;
        private IParentService _parentSerice;
        public ParentController(IParentService service, ILoggerService<ParentController> loggingService,
                                IStripePaymentChargeService _stripechargeService, ITransactionService _transactionService,
                                ISchoolService _schoolService, IParentService _parentSerice)
        {
            this._service = service;
            this._loggingService = loggingService;
            this._stripechargeService = _stripechargeService;
            this._transactionService = _transactionService;
            this._schoolService = _schoolService;
            this._parentSerice = _parentSerice;
        }

        /*[HttpGet]
        [Route("api/Parent/UpdateAllParentPasswords/")]
        public HttpResponseMessage UpdateAllParentPasswords(HttpRequestMessage request)
        {
            
            return request.CreateResponse(HttpStatusCode.OK);
        }*/

        /*[HttpGet]
        [Route("api/Encryption/EncryptString/{pwd}")]
        public HttpResponseMessage EncryptString(HttpRequestMessage request, string pwd)
        {
            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.encrypt(pwd);

           
            return request.CreateResponse(HttpStatusCode.OK, encryptedPassword);
        }*/

        [HttpGet]
        [Route("api/Encryption/DecString/{pwd}")]
        public HttpResponseMessage EncryptString(HttpRequestMessage request, string pwd)
        {
            AesEncryption aesEncryption = new AesEncryption();
            string encryptedPassword = aesEncryption.Decrypt("7WEaB7NCTniEcsNfYiLByetmEJLGdCNO9Di44iKatns=");


            return request.CreateResponse(HttpStatusCode.OK, encryptedPassword);
        }

        [HttpGet]
        [Route("api/Parent/UpdatePassword/{newPassword}/{secretKey}/{parentId}")]
        public HttpResponseMessage UpdatePassword(HttpRequestMessage request, string newPassword, string secretKey, int parentId)
        {
            string passwordUpdateSecret = ConfigurationManager.AppSettings["passwordUpdateSecret"];
            if (secretKey != passwordUpdateSecret)
            {
                return request.CreateResponse(HttpStatusCode.OK, "Incorrect Secret Key.");
            }

            Parent parent = _service.GetParent(parentId);
            if(parent == null || parent.id == 0)
            {
                return request.CreateResponse(HttpStatusCode.OK, "Parent Not Found");
            }

            if(_service.UpdateParentPassword(parent, newPassword))
            {
                return request.CreateResponse(HttpStatusCode.OK, "Password Updated Successfully!");
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, "Something went wrong! Try Again.");
            }


           
        }

        [Authorize]
        [Route("api/Parent/GetParentInfo/")]
        [ResponseType(typeof(ParentInfo))]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Parent/GetParentInfo");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            ParentInfo parent = _service.GetParentInfo(schoolId, parentId);
            if (parent.Equals(null))
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, parent);
            }
        }

        [Authorize]
        [Route("api/Parent/UpdateProfile/")]
        [ResponseType(typeof(ParentInfo))]
        public HttpResponseMessage Put(HttpRequestMessage request, ParentInfo parentProfile)
        {
            _loggingService.Debug("Into Parent/UpdateProfile");
            _loggingService.Info("schoolId:" + parentProfile.schoolId + " parentId: " + parentProfile.parentId);

            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _service.UpdateParent(parentProfile, schoolId);
            ParentInfo parent = _service.GetParentInfo(schoolId, parentId);
            if (parent == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, parent);
            }
        }

        [Authorize]
        [Route("api/Parent/UpdatePlan/")]
        public HttpResponseMessage Put(HttpRequestMessage request, Plan plan)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Parent/UpdatePlan");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);

            ParentPlanUpdateResult result = _service.UpdateParentPlan(schoolId, parentId, plan.plan, false, string.Empty);
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [Authorize]
        [Route("api/Parent/PayAndUpdatePlanToUnlimited/")]
        public HttpResponseMessage Put(HttpRequestMessage request, [FromBody]StripePaymentRequest paymentRequest)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Debug("Into Parent/PayAndUpdatePlanToUnlimited");
            _loggingService.Info("schoolId:" + schoolId + " parentId: " + parentId);
            var parent = _parentSerice.GetParent(parentId);
            decimal orderAmount = _service.GetUnlimitedParentPlanFee();
            PaymentGatewayTransactionLog log = _transactionService.CreateTransaction(parentId, schoolId, orderAmount, "_Plan_Change", "STRIPE", false);
            PaymentResult paymentResult = _stripechargeService.CreatePaymentCharge(parentId, schoolId, paymentRequest.stripeToken, orderAmount, log.Id, "_Plan_Change", parent.email, false);
            ParentPlanUpdateResult planUpdateResult = new ParentPlanUpdateResult();
            if (paymentResult.paymentStatus)
            {
                //change and update transaction log
                planUpdateResult = _service.UpdateParentPlan(schoolId, parentId, 2, true, paymentResult.paymentGatewayTransactionId);
                if (planUpdateResult.isPlanUpdated)
                {
                    _transactionService.UpdateTransaction("[Success]" + "[PGID: " + paymentResult.paymentGatewayTransactionId + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                }
                else
                {
                    _transactionService.UpdateTransaction("Failed" + "[FAILED: " + planUpdateResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                }
            }
            else
            {
                //update transction log and return error message
                _transactionService.UpdateTransaction("Failed" + "[FAILED: " + paymentResult.message + "]", log, schoolId, paymentResult.paymentGatewayTransactionId);
                planUpdateResult.isPlanUpdated = false;
                planUpdateResult.message = paymentResult.message;
            }

            if (planUpdateResult == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, planUpdateResult);
            }
        }

        [HttpPost]
        [Route("api/Parent/VerifyParentEmail/")]
        public HttpResponseMessage VerifyParentEmail(HttpRequestMessage request, VerifyParentEmailInput input)
        {
            if (string.IsNullOrEmpty(input.data) || input.data.Length < 5)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong data! ");
            }

            string result = _service.VerifyParentEmail(input.data);
            return request.CreateResponse(HttpStatusCode.OK, result);

        }


        [HttpPost]
        [Route("api/Parent/SendEmailVerificationLink/")]
        public HttpResponseMessage SendEmailVerificationLink(HttpRequestMessage request, VerifyParentEmailInput input)
        {
            if (string.IsNullOrEmpty(input.data))
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong data! ");
            }

            string result = _service.sendVerificationEmail(input.data);
            return request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [Route("api/Parent/RegisterParent/")]
        public HttpResponseMessage RegisterParent(HttpRequestMessage request, RegisterParentInput input)
        {
            string schoolStr = input.schoolId.ToString();

            if (string.IsNullOrEmpty(schoolStr) || schoolStr.Length < 5)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong School Registration ID");
            }

            string skid1 = schoolStr.Substring(0, 2);
            string skid2 = schoolStr.Substring(schoolStr.Length - 3, 3);

            if (skid1 != "25" && skid1 != "73" && skid1 != "99")
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong School Registration ID");
            }

            if (skid2 != "963" && skid2 != "270" && skid2 != "020")
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong School Registration ID");
            }

            string xp = input.schoolId.ToString().Substring(2, input.schoolId.ToString().Length - 2);
            string xpp = xp.Remove(xp.Length - 3);

            int schoolId = Int32.Parse(xpp);
            input.schoolId = schoolId;
            SchoolInfo school = _schoolService.GetSchoolInfo(schoolId);
            if (school == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "Wrong School Registration ID.");
            }

            Parent parent = _service.GetParent(input.email);
            if (parent != null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "It seems that you are already registered. Use Recover Password Tab.");
            }



            if (_service.RegisterParent(input, schoolId))
            {
                string msg = "Your registration is almost done. We've sent an email to " + input.email + ". Open the email and click/tap on the activation link to activate your account. If you can't find the email in your inbox, check the junk/spam folder.";
                return request.CreateResponse(HttpStatusCode.OK, msg);
            }

            return request.CreateResponse(HttpStatusCode.BadRequest, "Internal server error. Please contact school24");

        }

    }
}
