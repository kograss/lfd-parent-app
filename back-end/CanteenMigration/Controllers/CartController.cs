﻿using CanteenMigration.Commons;
using CanteenMigration.DataAccess;
using CanteenMigration.DataAccess.Queries;
using CanteenMigration.Models;
using CanteenMigration.IdentityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.BusinessDependencies;
using CanteenMigration.DataDependencies.DataException;

namespace CanteenMigration.Controllers
{
    public class CartController : ApiController
    {

        private ICartService _service;
        public ILoggerService<CartController> _loggingService;

        public CartController(ICartService service, ILoggerService<CartController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        // GET: api/Cart/5
        [Authorize]
        [Route("api/getCart/{deliveryDate}")]
        public List<CartProduct> Get(DateTime deliveryDate)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            return _service.GetCart(parentId, schoolId, deliveryDate);

        }

        // POST: api/Cart
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newCart"></param>
        [Authorize]
        [Route("api/addToCart")]
        public HttpResponseMessage Post(HttpRequestMessage request, AddToCart newCart)
        {
            _loggingService.Debug("Into addToCart");
            _loggingService.Info("Parent Id:" + newCart.parentId + " schoolId:" + newCart.schoolId);
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            AddToCartResult result = _service.AddItemToCart(newCart, schoolId, parentId);
            if (result.Equals(null))
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
        }


        // DELETE: api/Cart/5
        /// <summary>
        /// operations: 
        ///     1. reduce quantity of product from cart when deleteProduct = false (qty reduce)
        ///     2. delete product from cart when deleteProduct = true (delete product)
        ///     3. Empty cart when delivery date is given
        /// </summary>
        /// <param name="removeCart"></param>
        [Authorize]
        [Route("api/Cart/Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, RemoveCartModel removeCart)
        {
            _loggingService.Debug("Into Cart/Delete ");
            _loggingService.Info("SchoolId" + removeCart.schoolId + " ParentId:" + removeCart.parentId);
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());
            int parentId = Int32.Parse(User.Identity.GetParentId());

            _loggingService.Info("SchoolId" + schoolId + " ParentId:" + parentId);

            bool isCartUpdated = _service.UpdateCart(removeCart, schoolId, parentId);
            if (!isCartUpdated)
            {
                _loggingService.Info("***** isCartUpdated is false: " + schoolId + " " + parentId);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            List<CartProduct> cartData = _service.GetCart(parentId, schoolId, removeCart.deliveryDate);
            if (cartData == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, cartData);
            }
        }
    }
}
