﻿using CanteenMigration.BusinessDependencies;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace CanteenMigration.Controllers
{
    public class StockController : ApiController
    {
        private IProductService _service;
        public ILoggerService<ProductController> _loggingService;


        public StockController(IProductService service, ILoggerService<ProductController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }


        [Authorize]
        [Route("api/GetProductStock/{productIds}")]
        [ResponseType(typeof(List<ProductStock>))]
        public HttpResponseMessage Get(HttpRequestMessage request, [FromUri] string productIds)
        {
            _loggingService.Debug("Into GetProductStock");
            _loggingService.Info("productIds:" + productIds);

            JavaScriptSerializer js = new JavaScriptSerializer();
            List<int> ids = js.Deserialize<List<int>>(productIds);

            List<ProductStock> productsStock = _service.GetProductsStock(ids);
            if (productsStock == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, productsStock);
            }
        }
    }
}