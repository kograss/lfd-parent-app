﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using CanteenMigration.IdentityHelper;
using CanteenMigration.JSONModels;
using CanteenMigration.Services;
using CanteenMigration.BusinessDependencies;
using System.Web.Http.Description;
using CanteenMigration.DataDependencies.DataException;

namespace CanteenMigration.Controllers
{
    public class ProductController : ApiController
    {
        private IProductService _service;
        public ILoggerService<ProductController> _loggingService;


        public ProductController(IProductService service, ILoggerService<ProductController> loggingService)
        {
            this._service = service;
            this._loggingService = loggingService;

        }

        [Authorize]
        [Route("api/Product/{mealName}/{categoryId}/{deliveryDate}")]
        [ResponseType(typeof(List<Product>))]
        public HttpResponseMessage Get(HttpRequestMessage request, string mealName, int categoryId, DateTime deliveryDate)
        {
            int schoolId = Int32.Parse(User.Identity.GetSchoolId());

            _loggingService.Debug("Into Get Product ");
            _loggingService.Info("MealName:" + mealName + " categoryId" + categoryId + " deliveryDate:" + deliveryDate + " schoolId: " + schoolId);

            List<Product> products = _service.GetProducts(schoolId, mealName, categoryId, deliveryDate);
            if (products == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, products);
            }
        }


        [Route("api/ProductAdmin/{mealName}/{categoryId}/{deliveryDate}/{SchoolId}")]
        [ResponseType(typeof(List<Product>))]
        public HttpResponseMessage Get(HttpRequestMessage request, string mealName, int categoryId, DateTime deliveryDate, int schoolId)
        {
            //int schoolId = Int32.Parse(User.Identity.GetSchoolId());

            _loggingService.Debug("Into Get Product ");
            _loggingService.Info("MealName:" + mealName + " categoryId" + categoryId + " deliveryDate:" + deliveryDate + " schoolId: " + schoolId);

            List<Product> products = _service.GetProducts(schoolId, mealName, categoryId, deliveryDate);
            if (products == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, products);
            }
        }
    }
}