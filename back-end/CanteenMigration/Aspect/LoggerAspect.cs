﻿

using System;
using Castle.DynamicProxy;
using CanteenMigration.BusinessDependencies;
using System.Linq;

namespace CanteenMigration.Aspect
{
    public class LoggerAspect : IInterceptor
    {
        public ILoggerService<LoggerAspect> _loggingService;
        public LoggerAspect(ILoggerService<LoggerAspect> loggingService)
        {
            this._loggingService = loggingService;
        }
        public void Intercept(IInvocation invocation)
        {

            this._loggingService.Debug(
                string.Format("Calling Class {0} Calling method {1} with parameters {2}... ",
                    invocation.TargetType.Name,
                    invocation.Method.Name,
                    string.Join(
                            ", ",
                            invocation.Arguments.Select(
                                    a => (a ?? "").ToString()
                             ).ToArray()
                     )
                 )

            );
            //try
           // {
                invocation.Proceed();
                this._loggingService.Debug(string.Format("Done: result was {0}.", invocation.ReturnValue));
           // }
           // catch (Exception ex)
            //{
              //  this._loggingService.Debug(
              // string.Format("Calling Class {0} Exception method {1} with stack trace {2}... ",
               //    invocation.TargetType.Name,
                //   invocation.Method.Name,
                 //  "Exception is " + ex.StackTrace
               // )

           //);
           // }

        }
    }
}