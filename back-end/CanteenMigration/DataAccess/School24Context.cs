﻿
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess
{
    public class School24Context: DbContext
    {
        public School24Context()
            : base("name=School24.DB")
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CuttOff> CuttOff { get; set; }
        public DbSet<PublicHolidays> PublicHolidays { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Schools> Schools { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }
        public DbSet<OrderItemRecess> OrderItemRecess { get; set; }
        public DbSet<CanteenShopcartItem> CanteenShopCart { get; set; }
        public DbSet<ParentsOrders> ParentsOrder { get; set; }
        public DbSet<CanteenNews> CanteenNews { get; set; }
        public DbSet<LunchRecessClosures> LunchRecessClosures { get; set; }
        public DbSet<SchoolSetup> SchoolSetup { get; set; }
        public DbSet<DaysOfWeek> DaysOfWeek { get; set; }
        public DbSet<ConfigCanteen> ConfigCanteen { get; set; }
        public DbSet<CategoryUnavailableForDate> CategoryUnavailableForDate { get; set; }
        public DbSet<Klass> Klass { get; set; }
        public DbSet<Parent> Parent { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<PaymentGatewayTransactionLog> TransactionLogs { get; set; }
        public DbSet<Closures> Closures { get; set; }

    }
}