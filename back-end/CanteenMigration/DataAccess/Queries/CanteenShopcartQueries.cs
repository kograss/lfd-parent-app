﻿using CanteenMigration.Commons;
using CanteenMigration.Controllers;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess.Queries
{
    public class CanteenShopcartQueries
    {
        private School24Context context;
        public CanteenShopcartQueries(School24Context context)
        {
            this.context = context;
        }

        public List<CanteenShopcartItem> GetCanteenShopCart(int parentId, int schoolId, DateTime deliveryDate)
        {
            //return this.context.CanteenShopCart.
            // Where(e => e.parentId == parentId && e.schoolId == schoolId && e.mealName != "SPECIAL" &&
            //e.cartDeliveryDate == deliveryDateTime).OrderByDescending(e => e.studentId).ThenByDescending(e => e.mealName).ToList();
            List<CanteenShopcartItem> lst = this.context.CanteenShopCart.Where(
                e => e.schoolId == schoolId && e.parentId == parentId && e.cartDeliveryDate == deliveryDate).ToList();

            return lst;
            //&& e.CartDeliveryDate == cartDeliveryDate .ThenByDescending(e => e.CartExtra3)
        }

        public void AddToCart(AddToCart newCart)
        {

        }

        public List<CartProduct> GetCart(int parentId, int schoolId, DateTime deliveryDate)
        {
            CanteenShopcartQueries queries = new CanteenShopcartQueries(this.context);
            List<CanteenShopcartItem> cartList = queries.GetCanteenShopCart(parentId, schoolId, deliveryDate);
            List<CartProduct> lstCartProduct = new List<CartProduct>();

            foreach (CanteenShopcartItem cart in cartList)
            {
                decimal productsPrice = 0;
                decimal optionsPrice = 0;
                cart.options = new List<Option>();
                if (!string.IsNullOrEmpty(cart.productOptions))
                {
                    string[] optionIds = cart.productOptions.Split('|');
                    foreach (string optionId in optionIds)
                    {
                        int opt = int.Parse(optionId);
                        Option option = this.context.Options.Where(e => e.id == opt && e.schoolId == schoolId).FirstOrDefault();
                        decimal optionPrice = (decimal) option.optionPrice.Value; //(decimal)this.context.Options.Where(e => e.id == opt).Select(e => e.optionPrice).FirstOrDefault().Value;
                        cart.options.Add(option);
                        optionsPrice = optionsPrice + optionPrice;
                    }
                }
                productsPrice = productsPrice + ((cart.productPrice.Value + optionsPrice) * cart.productQuantity.Value);
                cart.productPrice = productsPrice;

                //prepare cart
                if (lstCartProduct.Count > 0)
                {
                    if(lstCartProduct.Where(e => e.studentId == cart.studentId).Count() > 0)
                    {
                        lstCartProduct.Where(e => e.studentId == cart.studentId).FirstOrDefault().canteenShopcarts.Add(cart);
                    }
                    else
                    {
                        CartProduct finalCart = new CartProduct
                        {
                            studentId = cart.studentId.Value,
                            studentName = this.context.Customers.Where(e => e.schoolId == schoolId && e.id == cart.studentId).FirstOrDefault().studentFirstName
                        };
                        finalCart.canteenShopcarts = new List<CanteenShopcartItem>();
                        finalCart.canteenShopcarts.Add(cart);
                        lstCartProduct.Add(finalCart);
                    }
                }

                else
                {
                    CartProduct finalCart = new CartProduct
                    {
                        studentId = cart.studentId.Value,
                        studentName = this.context.Customers.Where(e => e.schoolId == schoolId && e.id == cart.studentId).FirstOrDefault().studentFirstName
                    };
                    finalCart.canteenShopcarts = new List<CanteenShopcartItem>();
                    finalCart.canteenShopcarts.Add(cart);
                    lstCartProduct.Add(finalCart);
                }
            }
            return lstCartProduct;
        }
    }
}