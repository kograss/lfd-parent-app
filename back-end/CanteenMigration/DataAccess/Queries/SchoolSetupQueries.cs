﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess.Queries
{
    public class SchoolSetupQueries
    {
        private School24Context context;
        public SchoolSetupQueries(School24Context context)
        {
            this.context = context;
        }

        public SchoolSetup getSchoolSetupBySchoolId(int schoolId)
        {
            return this.context.SchoolSetup.Where(e => e.schoolId == schoolId).FirstOrDefault();
        }

        public String GetMealOneBySchoolId(int schoolId)
        {
            String mealOne = this.context.SchoolSetup.
                Where(e => e.schoolId == schoolId).FirstOrDefault().meal1;
            return mealOne;
        }

        public String GetMealTwoBySchoolId(int schoolId)
        {
            String mealTwo = this.context.SchoolSetup.
                Where(e => e.schoolId == schoolId).FirstOrDefault().meal2;
            return mealTwo;
        }

        public String GetMealThreeBySchoolId(int schoolId)
        {
            String mealThree = this.context.SchoolSetup.
                Where(e => e.schoolId == schoolId).FirstOrDefault().meal3;
            return mealThree;
        }
    }
}
