﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess.Queries
{
    public class StudentQueries
    {
        private School24Context context;
        public StudentQueries(School24Context context)
        {
            this.context = context;
        }

        public List<Customers> GetAllStudentsBySchoolIdParentId(int schoolId, int parentId)
        {
            return this.context.Customers.Where(e => e.schoolId == schoolId && e.parentId == parentId).ToList();
        }
    }
}