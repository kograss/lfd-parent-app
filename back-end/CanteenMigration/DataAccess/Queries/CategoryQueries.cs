﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess.Queries
{
    public class CategoryQueries
    {
        private School24Context context;
        public CategoryQueries(School24Context context)
        {
            this.context = context;
        }

        public List<Category> GetAllCategoriesBySchoolId(int? schoolId)
        {
            List<Category> categories = new List<Category>();
            categories = this.context.Categories.
                Where(e => e.schoolId == schoolId && e.categoryStatus == 1).OrderBy(e => e.categoryDescription).ToList();
            return categories;
        }
    }
}