﻿using CanteenMigration.Models;
using CanteenMigration.Services;
using CanteenMigration.Services.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataAccess.Queries
{
    public class ParentQueries
    {
        private School24Context context;
        public ParentQueries(School24Context context)
        {
            this.context = context;
        }

        public decimal GetParentCredit(int parentId, int schoolId)
        {
            decimal balance = this.context.Parent.Where(e => e.id == parentId && e.schoolId == schoolId).Select(e => e.credit).FirstOrDefault().Value;
            return Math.Truncate(100 * balance) / 100;
        }

        public Parent GetParentnByUsernameAndPassword(string username, string password)
        {
            AesEncryption aesEncryption = new AesEncryption();

            string encryptedPassword = aesEncryption.encrypt(password);

            Parent parent = this.context.Parent
                           .Where(
                            e => (e.userName.ToUpper() == username.ToUpper() || 
                                    e.mobile.ToUpper() == username.ToUpper() || 
                                    e.email .ToUpper() == username.ToUpper())
                                && (e.password.ToUpper() == encryptedPassword.ToUpper()
                                )
                                ).FirstOrDefault();
            if (parent != null)
                return parent;
            else
                return null;
        }


        public bool IsSchoolActive(int schoolId)
        {
            return this.context.Schools.Where(e => e.schoolId == schoolId).Select(e => e.isProcessed.Value).FirstOrDefault();
        }
    }
}