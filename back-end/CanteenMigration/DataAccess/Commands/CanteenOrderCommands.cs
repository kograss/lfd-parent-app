﻿using CanteenMigration.Commons;
using CanteenMigration.Controllers;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CanteenMigration.DataAccess.Commands
{
    public class CanteenOrderCommands
    {
        private School24Context context;
        public CanteenOrderCommands(School24Context context)
        {
            this.context = context;
        }

        public void processLunchOrders(Order order, int schoolId, int parentId, string orderNumberLunch, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList)
        {
            Orders o = new Orders();
            o.studentId = order.studentId;
            o.schoolId = schoolId;
            o.orderDate = DateTime.Now;
            o.orderNumber = orderNumberLunch;
            o.orderComment = order.comment;
            o.orderReason = order.reason;
            o.orderShippedDate = order.deliveryDate;
            o.orderShipMethod = 0;
            o.orderAmount = orderAmount;
            o.orderType = "LUNCH";
            o.bag = bag;
            o.bagPrice = order.chargeForBag; ;
            context.Orders.Add(o);
            context.SaveChanges();

            int counter = 0;
            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "LUNCH"))
            {
                //insert into oitems
                OrderItems orderItem = new OrderItems();
                orderItem.orderID = o.id;
                orderItem.orderIntemNumber = counter++;
                orderItem.productId = c.productId;
                orderItem.options = c.productOptions;
                orderItem.numerOfItems = (Int16) c.productQuantity.Value;
                orderItem.schoolId = schoolId;
                orderItem.extra2 = "LUNCH";
                context.OrderItems.Add(orderItem);
                context.SaveChanges();
            }
        }

        public void processRecessOrders(Order order, int schoolId, int parentId, string orderNumberRecess, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList)
        {
            int counter = 0;
            //process recess orders
            Orders orderRecess = new Orders();
            orderRecess.studentId = order.studentId;
            orderRecess.schoolId = schoolId;
            orderRecess.orderDate = DateTime.Now;
            orderRecess.orderNumber = orderNumberRecess;
            orderRecess.orderComment = order.comment;
            orderRecess.orderReason = order.reason;
            orderRecess.orderShippedDate = order.deliveryDate;
            orderRecess.orderShipMethod = 0;
            orderRecess.orderAmount = orderAmount;
            orderRecess.orderType = "RECESS";
            orderRecess.bag = bag;
            orderRecess.bagPrice = order.chargeForBag; ;
            context.Orders.Add(orderRecess);
            context.SaveChanges();

            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "RECESS"))
            {
                OrderItemRecess orderItemRecess = new OrderItemRecess();
                orderItemRecess.orderId = orderRecess.id;
                orderItemRecess.orderIntemNumber = counter++;
                orderItemRecess.productId = c.productId;
                orderItemRecess.options = c.productOptions;
                orderItemRecess.numerOfItems = (Int16) c.productQuantity.Value;
                orderItemRecess.schoolId = schoolId;
                orderItemRecess.extra2 = "RECESS";
                context.OrderItemRecess.Add(orderItemRecess);
            }
        }

        public void processThirdBreakOrder(Order order, int schoolId, int parentId, string orderNumberThird, decimal orderAmount, string bag, List<CanteenShopcartItem> cartList)
        {
            int counter = 0;
            //process third break orders
            Orders orderThird = new Orders();
            orderThird.studentId = order.studentId;
            orderThird.schoolId = schoolId;
            orderThird.orderDate = DateTime.Now;
            orderThird.orderNumber = orderNumberThird;
            orderThird.orderComment = order.comment;
            orderThird.orderReason = order.reason;
            orderThird.orderShippedDate = order.deliveryDate;
            orderThird.orderShipMethod = 0;
            orderThird.orderAmount = orderAmount;
            orderThird.orderType = "THIRD";
            orderThird.bag = bag;
            orderThird.bagPrice = order.chargeForBag; ;
            context.Orders.Add(orderThird);
            context.SaveChanges();

            foreach (CanteenShopcartItem c in cartList.Where(e => e.mealName == "THIRD"))
            {
                OrderItemRecess orderItemRecess = new OrderItemRecess();
                orderItemRecess.orderId = orderThird.id;
                orderItemRecess.orderIntemNumber = counter++;
                orderItemRecess.productId = c.productId;
                orderItemRecess.options = c.productOptions;
                orderItemRecess.numerOfItems = (Int16) c.productQuantity.Value;
                orderItemRecess.schoolId = schoolId;
                orderItemRecess.extra2 = "THIRD";
                context.OrderItemRecess.Add(orderItemRecess);
            }
        }

        public decimal getCartAmout(List<CanteenShopcartItem> cartList)
        {
            decimal productsPrice = 0;
            foreach (CanteenShopcartItem cart in cartList)
            {
                decimal optionsPrice = 0;
                if (!string.IsNullOrEmpty(cart.productOptions))
                {
                    string[] optionIds = cart.productOptions.Split('|');
                    foreach (string optionId in optionIds)
                    {
                        int opt = int.Parse(optionId);
                        decimal optionPrice = (decimal)context.Options.Where(e => e.id == opt).Select(e => e.optionPrice).FirstOrDefault().Value;
                        optionsPrice = optionsPrice + optionPrice;
                    }
                }
                productsPrice = productsPrice + ((cart.productPrice.Value + optionsPrice) * cart.productQuantity.Value);
            }

            return productsPrice;
        }

        public string getOrderType(int lunchCount, int recessCount, int thirdCount)
        {
            string orderType = string.Empty;
            if (lunchCount > 0)
            {
                orderType = "LUNCH";
                if (recessCount > 0)
                {
                    orderType = "LR";
                    if (thirdCount > 0)
                    {
                        orderType = "LRT";
                    }
                }
                else if (thirdCount > 0)
                {
                    orderType = "LT";
                }
            }

            else if (recessCount > 0)
            {
                orderType = "RECESS";
                if (thirdCount > 0)
                {
                    orderType = "RT";
                }
            }

            else if (thirdCount > 0)
            {
                orderType = "Third";
            }

            return orderType;
        }

        public List<int> placeStudentOrder(List<Order> orders, DateTime deliveryDate, int schoolId, int parentId, bool isCreditOrder)
        {
            List<int> orderIds = new List<int>();
            SchoolSetup schoolSetup = context.SchoolSetup.Where(e => e.schoolId == schoolId).FirstOrDefault();
            Parent parent = context.Parent.Where(e => e.id == parentId).FirstOrDefault();

            foreach (Order order in orders)
            {
                //1. Fetch student wise cart
                //2. 
                List<CanteenShopcartItem> cartList = context.CanteenShopCart
                                                        .Where(e => e.parentId == parentId && e.schoolId == schoolId
                                                            && e.cartDeliveryDate == deliveryDate && e.studentId == order.studentId
                                                        ).ToList();

                CanteenOrderCommands canteenOrderCommands = new CanteenOrderCommands(context);

                decimal bagCharges = schoolSetup.chargeForBag == "YES" ? schoolSetup.bagPrice.Value : 0;
                decimal chargesPerOrder = 0;
                if (schoolSetup.feeModel == 1)
                {
                    chargesPerOrder = 0;
                }
                else
                {
                    if (parent.parentFeePlan == 1)
                    {
                        chargesPerOrder = 0.25M;
                    }
                    else if (parent.parentFeePlan == 0 || parent.parentFeePlan == null)
                    {
                        //error condition
                    }
                }

                decimal orderAmount = canteenOrderCommands.getCartAmout(cartList) + bagCharges + chargesPerOrder;
                var now = DateTime.Now;
                var zeroDate = DateTime.MinValue.AddHours(now.Hour).AddMinutes(now.Minute).AddSeconds(now.Second).AddMilliseconds(now.Millisecond);
                string orderNumber = order.schoolId + order.parentId + order.studentId + (zeroDate.Ticks / 10000).ToString();

                string bag = schoolSetup.chargeForBag == "YES" ? "BUY" : "NA";
                string orderType = string.Empty;

                int lunchCount = cartList.Where(e => e.mealName == "LUNCH").Count();
                int recessCount = cartList.Where(e => e.mealName == "RECESS").Count();
                int thirdCount = cartList.Where(e => e.mealName == "THIRD").Count();

                orderType = getOrderType(lunchCount, recessCount, thirdCount);

                ParentsOrders parentsOrder = new ParentsOrders();
                parentsOrder.studentId = order.studentId;
                parentsOrder.schoolId = schoolId;
                parentsOrder.orderDate = DateTime.Now;
                parentsOrder.orderNumber = orderNumber;
                parentsOrder.orderComment = order.comment;
                parentsOrder.orderReason = order.reason;
                parentsOrder.orderShippedDate = deliveryDate;
                parentsOrder.orderAmount = orderAmount;
                parentsOrder.orderType = orderType;
                parentsOrder.bag = bag;
                parentsOrder.bagPrice = order.chargeForBag;
                parentsOrder.parentId = parentId;
                parentsOrder.schoolModel = schoolSetup.feeModel;
                parentsOrder.schoolPlan = schoolSetup.sFeePlan;
                parentsOrder.parentPlan = parent.parentFeePlan;
                parentsOrder.orderCancel = false;
                context.ParentsOrder.Add(parentsOrder);
                context.SaveChanges();

                orderIds.Add(parentsOrder.orderId);
                string orderNumberLunch = "0" + parentsOrder.orderId;
                string orderNumberRecess = "R" + parentsOrder.orderId;
                string orderNumberThird = "T" + parentsOrder.orderId;

                if (lunchCount > 0)
                {
                    //process lunch orders
                    canteenOrderCommands.processLunchOrders(order, schoolId, parentId, orderNumberLunch, orderAmount, bag, cartList);
                }
                if (recessCount > 0)
                {
                    canteenOrderCommands.processRecessOrders(order, schoolId, parentId, orderNumberRecess, orderAmount, bag, cartList);
                }
                if (thirdCount > 0)
                {
                    canteenOrderCommands.processThirdBreakOrder(order, schoolId, parentId, orderNumberThird, orderAmount, bag, cartList);
                }

                //Create record in Transactions
                Transaction transaction = new Transaction();
                transaction.parentId = parentId;
                transaction.schoolId = schoolId;
                transaction.transactionAmount = orderAmount;
                transaction.transactionType = "ORDER:DEBIT";
                transaction.extra1 = parentsOrder.orderId.ToString();
                transaction.balanceBefore = parent.credit;
                transaction.balanceAfter = parent.credit - orderAmount;
                transaction.transactionDate = DateTimeUtils.GetWithoutTime();
                transaction.studentId = order.studentId;
                //transaction.TransactionId = input.PGTransactionId;
                context.Transaction.Add(transaction);

                //Update credit
                if (isCreditOrder)
                {
                    parent.credit = parent.credit - orderAmount;
                    context.SaveChanges();
                }
                //remove from cart
                context.CanteenShopCart.RemoveRange(cartList);
                context.SaveChanges();
                }
            return orderIds;
        }
    }
}