﻿using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Threading;
using System.Threading.Tasks;
using CanteenMigration.BusinessDependencies;
using System.Net.Http;

namespace CanteenMigration.filters
{
    public class ExceptionFilter : IAutofacExceptionFilter
    {
        ILoggerService<ExceptionFilter> _loggerService;
        
        
        // throw new NotImplementedException();
        public ExceptionFilter(ILoggerService<ExceptionFilter> loggerService)
        {
            this._loggerService = loggerService;
        }

        public Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            _loggerService.Debug("Error message is " + actionExecutedContext.Exception.Message);
            _loggerService.Debug("Error type  is " + actionExecutedContext.Exception.GetType());
            _loggerService.Debug("Error stack trace is " + actionExecutedContext.Exception.StackTrace);
            actionExecutedContext.Response = new HttpResponseMessage();
            actionExecutedContext.Response.StatusCode = HttpStatusCode.InternalServerError;
            actionExecutedContext.Response.Content = new StringContent("Server Error");
            return Task.FromResult(0);
        }
    }
}
