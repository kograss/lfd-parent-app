﻿using CanteenMigration.JSONModels;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IRotaRepository
    {
        List<ParentsShift> GetParentShifts(int parentId, int schoolId);
        List<CanteenMigration.JSONModels.RosterNews> GetRosterNews(int schoolId);
        List<RosterHolidays> GetMonthsHoliday(int schoolId, DateTime startDate, DateTime endDate);
        IEnumerable<Shift> GetSchoolShifts(int schoolId);
        IEnumerable<VolsAndShift> GetShiftsWithVolunteers(int schoolId, DateTime shiftDate);
        bool RegisterVolunteerToShift(int schoolId, int shiftId, int parentId, DateTime regDate, string name, ref string _message);
        List<RosterHolidays> GetHolidayByMonth(int schoolId, int month);
    }
}
