﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ISchoolEventRepository
    {
        int GetSchoolId(int eventId);
        List<SchoolEventInfo> GetSchoolEvents(int schoolId, DateTime currentDateInSchoolTimeZone);
        List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId);
        List<CanteenMigration.JSONModels.Options> GetAllProductOptions(int schoolId, int productId);
        List<SchoolEventShopcart> GetCart(int schoolId, int parentId, int eventId);
        IQueryable<EventOption> GetOptions(int[] optionIds, int schoolId);
        bool EmptyCart(int parentId, int schoolId, int eventId);
        bool RemoveCartItem(int cartId, int parentId, int schoolId);
        bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId);
        decimal GetAllOptionsPrice(int schoolId, int[] intOptionIds);
        CanteenMigration.Models.SchoolEventProduct GetProduct(int schoolId, int productId);
        SchoolEventShopcart GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions,
                                                  int studentId, int eventId);
        int GetCartItemsCount(int parentId, int schoolId, int eventId);
        bool AddItemsToCart(List<SchoolEventShopcart> cartItems);
        bool UpdateCartItems(List<SchoolEventShopcart> cartItems);
        void AddSchoolOrder(SchoolOrders schoolOrder);
        void AddEventOrders(EventOrders eventOrder);
        void InsertEventOrderItems(List<EventOrderItems> orderItems);
        void InsertEventTransaction(EventTransactions eventTransactions);
        SchoolEvent2 GetSchoolEvent(int schoolId, int eventId);
        bool DeductProductStock(List<SchoolEventShopcart> allCartItems, int schoolId);
        List<SchoolEventProduct> GetSchoolEventProducts(IEnumerable<int> productIds); //for stock check
        bool IsOrderExists(int orderId, int parentId, int schoolId);
        EventOrders GetEventOrders(string orderNumber);
        List<EventOrderItems> GetEventOrderItems(int schoolId, int orderId);
        void AddSchoolOrderDeleted(SchoolOrdersDeleted schoolOrder);
        void InsertEventOrderItemsDeleted(List<EventOrderItemsDeleted> orderItems);
        bool CanceOrder(SchoolOrders parentsOrder);
        bool DeleteEventOrders(List<EventOrders> orders);
        void RemoveEventOrderItems(List<EventOrderItems> orderItems);
        bool AddStockToProduct(int productId, int stockToAdd, int schoolId);
    }
}
