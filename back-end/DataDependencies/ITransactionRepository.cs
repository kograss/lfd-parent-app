﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ITransactionRepository
    {
        void AddTransaction(Transaction transaction);
        List<CanteenMigration.JSONModels.ParentTransaction> GetAllParentsTransactions(int parentId, int schoolId, int year);
        void AddTransactions(List<Transaction> transactions);
    }
}
