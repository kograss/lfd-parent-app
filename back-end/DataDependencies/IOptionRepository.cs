﻿


using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IOptionRepository:IDisposable
    {
        Option GetOption(int optionId, int schoolId);
        IQueryable<Option> GetOptions(int[] optionIds, int schoolId);
        List<Option> GetOptions(List<int> optionIds, int productId);
        List<int> GetOptionIds(List<int> optionIds, int productId);
        string GetProductOptions(int schoolId, string mealName, int productId);
        List<Options> GetCanteenOptionsFromDB(int schoolId, string mealCode, int productId);
        string GetOptionName(int optionId, int schoolId);
        string GetSchoolEventOptionName(int optionId, int schoolId);
        decimal GetAllOptionsPrice(int schoolId, int[] intOptionIds);
        decimal GetOptionPrice(int optionId, int schoolId);
        List<CanteenMigration.JSONModels.Options> GetAllProductOptions(int schoolId, int productId);


    }
}
