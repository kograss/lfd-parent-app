﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IParentRepository
    {
        Parent GetParent(int schoolId, int parentId);
        Parent GetParent(string email);
        Parent GetParent(int parentId);
        bool DeductParentBalance(int parentId, int schoolId, decimal amountToDeduct);
        bool AddParentCredit(int parentId, int schoolId, decimal amountToAdd);
        decimal GetParentCredit(int parentId, int schoolId);
        bool SaveUpdatedParent();
        void AddServiceFeePaid(int parentId, int schoolId, decimal amount);
        bool IsClassSetupRequired(int schoolId, int parentId);
        Parent AddParent(Parent parent);
        List<Parent> GetAllParents();
    }
}
