﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IOrderHistoryRepository
    {
        List<ParentOrder> GetParentHistory(int schoolId, int parentId, bool orderCancelStatus, int year);
        ParentsOrders GetParentsOrder(int schoolId, int orderId); //parents_orders table
        Orders GetOrder(int schoolId, int studentId, string orderNumber); //orders table    
        Task<List<OrderItemProduct>> GetOrderItems(int schoolId, int orderId, int studentId,
                                                    string orderNumber, string mealType, string mealName);      

        ParentOrder GetSingleParentOrder(int schoolId, int orderId, int parentId);

        SchoolOrder GetSingleSchoolOrder(int schoolId, int orderId, int parentId);
        List<OrderItemProduct> GetSpecialOrderItems(int schoolId, int orderId,
                                                           int studentId, string orderNumber);
        SchoolOrders GetSchoolOrder(int schoolId, int orderId);
        List<OrderItemProduct> GetSchoolEventOrderItems(int schoolId, int orderId,
                                                           int studentId, string orderNumber);

        List<CanteenNews> GetCanteenNews(int schoolId);
        List<ParentOrder> GetFundRaisingOrder(int schoolId, int parentId, int year);
    }
}
