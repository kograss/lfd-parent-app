﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ITransactionLogRepository
    {
        PaymentGatewayTransactionLog CreateTransactionLog(int parentId, int schoolId, decimal orderAmount, string transactionType, string paymentGatewayName, bool isCCFeeRequired);
        bool UpdateTransactionLog(string status, PaymentGatewayTransactionLog paymentLog, int schoolId, string paymentIdentifier);
        PaymentGatewayTransactionLog CreateTransactionLogForEvent(int parentId, int schoolId, decimal orderAmount,
                                                        string transactionType, string paymentGatewayName, bool isCCFeeRequired);

        bool AddTransactionLog(PaymentGatewayTransactionLog log);
    }
}
