﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IEventRepository
    {
        SpecialEvent GetSpecialEvent(int schoolId, int eventId);
        List<SchoolEventInfo> GetSchoolEvents(int schoolId, DateTime currentDateInSchoolTimeZone);
        List<SchoolEventCategory> GetEventCategories(int schoolId, int eventId);
        List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId);
        List<CanteenMigration.JSONModels.Product> GetCategoriesProductEvents(int schoolId, int eventId, int categoryId);

        //CART:
        List<EventShopcart> GetCart(int schoolId, int parentId, int eventId);

        bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId);
        bool RemoveCartItem(int cartId, int parentId, int schoolId);
        bool EmptyCart(int parentId, int schoolId, int eventId);
        EventShopcart GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions,
                                                   int studentId, int eventId);
        bool AddItemToCart(EventShopcart cartItem);
        bool AddItemsToCart(List<EventShopcart> cartItems);
        bool UpdateCartItems(List<EventShopcart> cartItems);
        int GetCartItemsCount(int parentId, int schoolId, int eventId);
        bool DeductProductStock(List<EventShopcart> allCartItems, int schoolId);
        bool AddStockToProduct(int catalogId, int stockToAdd, int schoolId, int eventId);
    }
}
