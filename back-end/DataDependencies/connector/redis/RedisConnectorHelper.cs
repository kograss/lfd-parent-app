using StackExchange.Redis;
using System;


namespace CanteenMigration.DataDependencies.connector.redis
{
    public class RedisConnectorHelper
    {
        public RedisConnectorHelper()
        {
            lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                //TODO make redis server secure
                return ConnectionMultiplexer.Connect("52.64.191.90:6379");
                //return ConnectionMultiplexer.Connect("127.0.0.1:6379");
                
            });
        }

        private Lazy<ConnectionMultiplexer> lazyConnection;

        public  ConnectionMultiplexer Connection
        {
           
            get
            {
                try
                {
                    return lazyConnection.Value;
                }
                catch (Exception ex)
                {
                    lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
                    {
                        //TODO make redis server secure
                        return ConnectionMultiplexer.Connect("52.64.191.90:6379");
                        //return ConnectionMultiplexer.Connect("127.0.0.1:6379");
                    });
                    return lazyConnection.Value;
                }
            }
           

           

        }
    }
}