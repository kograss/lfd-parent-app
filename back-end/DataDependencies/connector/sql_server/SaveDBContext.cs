﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.connector.sql_server
{
    public class SaveDBContext
    {
        protected int SaveDbContext(IDBContext _dbContext, Object obj)
        {
            try
            {
                // Try to save changes, which may cause a conflict.
                int num = _dbContext.SaveChanges();
                return num;
            }
            catch (OptimisticConcurrencyException exception)
            {
                // Resolve the concurrency conflict by refreshing the 
                // object context before re-saving changes. 
                _dbContext.Entry(obj).Reload();
                // Save changes.
                _dbContext.SaveChanges();
                throw exception;
            }
        }
    }
}
