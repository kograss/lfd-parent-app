﻿
using CanteenMigration.Models;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess
{
    public interface IDBContext
    {

        int SaveChanges();
        Database GetDatabase();
        DbEntityEntry Entry(object entity);
        DbSet<Category> Categories { get; set; }
        DbSet<CuttOff> CuttOff { get; set; }
        DbSet<PublicHolidays> Holidays { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Option> Options { get; set; }
        DbSet<Customers> Customers { get; set; }
        DbSet<Schools> Schools { get; set; }
        DbSet<OrderItems> OrderItems { get; set; }
        DbSet<OrderItemRecess> OrderItemRecess { get; set; }
        DbSet<OrderItemsDeleted> OrderItemsDeleted { get; set; }
        DbSet<OrderItemRecessDeleted> OrderItemRecessDeleted { get; set; }
        DbSet<CanteenShopcartItem> CanteenShopCart { get; set; }



        DbSet<ParentsOrders> ParentsOrder { get; set; }
        DbSet<CanteenNews> CanteenNews { get; set; }
        DbSet<LunchRecessClosures> LunchRecessClosures { get; set; }
        DbSet<SchoolSetup> SchoolSetup { get; set; }
        DbSet<DaysOfWeek> DaysOfWeek { get; set; }
        DbSet<ConfigCanteen> ConfigCanteen { get; set; }
        DbSet<CategoryUnavailableForDate> CategoryUnavailableForDate { get; set; }
        DbSet<Klass> Klass { get; set; }
        DbSet<Parent> Parent { get; set; }
        DbSet<Orders> Orders { get; set; }
        DbSet<OrdersDeleted> OrdersDeleted { get; set; }
        DbSet<Transaction> Transaction { get; set; }
        DbSet<PaymentGatewayTransactionLog> TransactionLogs { get; set; }
        DbSet<Closures> Closures { get; set; }
        DbSet<Topup> Topup { get; set; }
        DbSet<Email> Email { get; set; }
        DbSet<ServiceFeePaid> ServiceFeePaid { get; set; }

        DbSet<RosterNews> RosterNews { get; set; }
        DbSet<Shift> Shifts { get; set; }
        DbSet<VolsAndShift> VolsAndShifts { get; set; }
        DbSet<RosterHolidays> RosterHolidays { get; set; }
        DbSet<SpecialEvent> SchoolEvent { get; set; }
        DbSet<CategoriesAndEvents> CategoriesAndEvents { get; set; }
        DbSet<ProductsAndEvents> ProductsAndEvents { get; set; }
        DbSet<EventShopcart> EventShopcart { get; set; }


        DbSet<SchoolEvent2> SchoolEvent2 { get; set; }
        DbSet<EventProductAndEvents> EventProductAndEvents { get; set; }
        DbSet<SchoolEventProduct> SchoolEventProduct { get; set; }
        DbSet<EventOption> EventOption { get; set; }
        DbSet<SchoolEventShopcart> SchoolEventShopcart { get; set; }
        DbSet<SchoolOrders> SchoolOrders { get; set; }
        DbSet<EventOrders> EventOrders { get; set; }
        DbSet<EventOrderItems> EventOrderItems { get; set; }
        DbSet<EventTransactions> EventTransactions { get; set; }

        DbSet<ParentSelfTopup> ParentSelfTopup { get; set; }

        //uniform 
        DbSet<UniformDepartment> UniformDepartment { get; set; }
        DbSet<UniformNews> UniformNews { get; set; }
        DbSet<UniformProduct> UniformProduct { get; set; }
        DbSet<UniformPack> UniformPack { get; set; }
        DbSet<UniformDepartmentProduct> UniformDepartmentProduct { get; set; }
        DbSet<UniformStocks> UniformStocks { get; set; }
        DbSet<UniformBasketItem> UniformBasketItem { get; set; }
        DbSet<UniformPackItemProduct> UniformPackItemProduct { get; set; }
        DbSet<UniformPackProduct> UniformPackProduct { get; set; }
        DbSet<UniformOrderData> UniformOrderData { get; set; }
        DbSet<UniformOrderStatus> UniformOrderStatus { get; set; }
        DbSet<UniformOrderPayment> UniformOrderPayment { get; set; }
        DbSet<UniformBasket> UniformBasket { get; set; }
        DbSet<UniformAttribute> UniformAttribute { get; set; }


        DbSet<UniformAdmin> UniformAdmin { get; set; }

        DbSet<UniformSetup> UniformSetup { get; set; }

        DbSet<UniformAddress> UniformAddress { get; set; }



        //coupons
        DbSet<Coupons> Coupons { get; set; }
        DbSet<CouponRedeem> CouponRedeem { get; set; }

        DbSet<BannedFood> BannedFood { get; set; }

        //Donation
        DbSet<FundRaisingEventOrders> FundRaisingEventOrders { get; set; }
        DbSet<SchoolOrdersDeleted> SchoolOrdersDeleted { get; set; }
        DbSet<EventOrderItemsDeleted> EventOrderItemsDeleted { get; set; }
        DbSet<CanteenProductAvailability> CanteenProductAvailability { get; set; }

        DbSet<CustomerStripeDetails> CustomerStripeDetails { get; set; }

    }
}