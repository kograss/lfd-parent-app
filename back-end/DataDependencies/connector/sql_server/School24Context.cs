﻿
using CanteenMigration.Models;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess
{
    public class School24Context : DbContext, IDBContext
    {
        public School24Context()
            : base("name=School24.DB")
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<CuttOff> CuttOff { get; set; }
        public DbSet<PublicHolidays> Holidays { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Schools> Schools { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }
        public DbSet<OrderItemRecess> OrderItemRecess { get; set; }
        public DbSet<CanteenShopcartItem> CanteenShopCart { get; set; }
        public DbSet<ParentsOrders> ParentsOrder { get; set; }
        public DbSet<CanteenNews> CanteenNews { get; set; }
        public DbSet<LunchRecessClosures> LunchRecessClosures { get; set; }
        public DbSet<SchoolSetup> SchoolSetup { get; set; }
        public DbSet<DaysOfWeek> DaysOfWeek { get; set; }
        public DbSet<ConfigCanteen> ConfigCanteen { get; set; }
        public DbSet<CategoryUnavailableForDate> CategoryUnavailableForDate { get; set; }
        public DbSet<Klass> Klass { get; set; }
        public DbSet<Parent> Parent { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrdersDeleted> OrdersDeleted { get; set; }
        public DbSet<OrderItemsDeleted> OrderItemsDeleted { get; set; }
        public DbSet<OrderItemRecessDeleted> OrderItemRecessDeleted { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<PaymentGatewayTransactionLog> TransactionLogs { get; set; }
        public DbSet<Closures> Closures { get; set; }
        public DbSet<Topup> Topup { get; set; }
        public DbSet<Email> Email { get; set; }
        public DbSet<ServiceFeePaid> ServiceFeePaid { get; set; }
        public virtual DbSet<RosterNews> RosterNews { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<VolsAndShift> VolsAndShifts { get; set; }
        public virtual DbSet<RosterHolidays> RosterHolidays { get; set; }
        public DbSet<SpecialEvent> SchoolEvent { get; set; }
        public DbSet<CategoriesAndEvents> CategoriesAndEvents { get; set; }

        public DbSet<ProductsAndEvents> ProductsAndEvents { get; set; }
        public DbSet<EventShopcart> EventShopcart { get; set; }

        public DbSet<SchoolEvent2> SchoolEvent2 { get; set; }
        public DbSet<EventProductAndEvents> EventProductAndEvents { get; set; }
        public DbSet<SchoolEventProduct> SchoolEventProduct { get; set; }
        public DbSet<EventOption> EventOption { get; set; }

        public DbSet<SchoolEventShopcart> SchoolEventShopcart { get; set; }
        public DbSet<SchoolOrders> SchoolOrders { get; set; }
        public DbSet<EventOrders> EventOrders { get; set; }
        public DbSet<EventOrderItems> EventOrderItems { get; set; }
        public DbSet<EventTransactions> EventTransactions { get; set; }
        public DbSet<ParentSelfTopup> ParentSelfTopup { get; set; }


        //uniform
        public DbSet<UniformDepartment> UniformDepartment { get; set; }
        public DbSet<UniformNews> UniformNews { get; set; }
        public DbSet<UniformProduct> UniformProduct { get; set; }
        public DbSet<UniformPack> UniformPack { get; set; }
        public DbSet<UniformDepartmentProduct> UniformDepartmentProduct { get; set; }
        public DbSet<UniformStocks> UniformStocks { get; set; }
        public DbSet<UniformBasketItem> UniformBasketItem { get; set; }
        public DbSet<UniformPackItemProduct> UniformPackItemProduct { get; set; }
        public DbSet<UniformPackProduct> UniformPackProduct { get; set; }
        public DbSet<UniformOrderData> UniformOrderData { get; set; }
        public DbSet<UniformOrderStatus> UniformOrderStatus { get; set; }
        public DbSet<UniformOrderPayment> UniformOrderPayment { get; set; }
        public DbSet<UniformBasket> UniformBasket { get; set; }
        public DbSet<UniformAttribute> UniformAttribute { get; set; }

        public DbSet<UniformAdmin> UniformAdmin { get; set; }

        public DbSet<UniformSetup> UniformSetup { get; set; }
        public DbSet<UniformAddress> UniformAddress { get; set; }

        //coupons
        public DbSet<Coupons> Coupons { get; set; }
        public DbSet<CouponRedeem> CouponRedeem { get; set; }

        public DbSet<BannedFood> BannedFood { get; set; }

        //Donation
        public DbSet<FundRaisingEventOrders> FundRaisingEventOrders { get; set; }
        public DbSet<SchoolOrdersDeleted> SchoolOrdersDeleted { get; set; }
        public DbSet<EventOrderItemsDeleted> EventOrderItemsDeleted { get; set; }
        public DbSet<CanteenProductAvailability> CanteenProductAvailability { get; set; }

        public DbSet<CustomerStripeDetails> CustomerStripeDetails { get; set; }

        public Database GetDatabase()
        {
            return this.Database;
        }
    }
}