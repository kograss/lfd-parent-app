﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IProductRepository
    {
        string GetProducts(int schoolId, string mealCode, int categoryId);

        List<CanteenMigration.Models.Product> GetCanteenProductsFromDB(int schoolId, string mealCode, int categoryId);
        CanteenMigration.Models.Product GetProduct(int schoolId, int productId);
        List<ProductStock> GetProductsStock(List<int> productsIds);
        bool DeductProductStock(List<CanteenShopcartItem> allCartItems, int schoolId);
        bool AddStockToProduct(int productId, int stockToAdd, int schoolId);
        List<CanteenMigration.Models.Product> GetProducts(IEnumerable<int> productIds);
        List<CanteenProductAvailability> GetProductAvailability(List<int> productIds, int schoolId);

    }
}
