﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ICategoriesRepository
    {
        bool IsCategoryCutOff(int categoryId, DateTime nowInSchoolTimezone, DateTime orderDate);
        bool IsCategoryUnavailableThisday(DateTime forWhatDay, int categoryId, string service, int schoolId);
        List<Category> GetCategories(int schoolId, List<int> categoryIds);
    }
}
