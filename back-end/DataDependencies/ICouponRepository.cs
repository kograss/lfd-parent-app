﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ICouponRepository
    {
        List<Coupons> GetAllCoupons(int schoolId);
        List<CouponRedeem> GetPatentsCoupons(int parentId, int schoolId);
        bool IsParentUsedThisCoupon(int couponId, int parentId, int schoolId);
        bool RedeemCoupon(CouponRedeem redeem);
        Coupons GetCoupon(int couponId, int schoolId);
    }
}
