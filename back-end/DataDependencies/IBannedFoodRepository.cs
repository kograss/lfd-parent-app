﻿
using CanteenMigration.Models;
using System;
using System.Collections.Generic;

namespace DataDependencies
{
    public interface IBannedFoodRepository
    {
        List<BannedFood> GetBannedFood(int parentId);
        List<CanteenMigration.Models.Product> GetAllProducts(int schoolId);
        bool AddBannedProduct(List<int> studentIds, int parentId, int schoolId, int productId);
        bool DeleteBannedProduct(int Id);
    }
}
