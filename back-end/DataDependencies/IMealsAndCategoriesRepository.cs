﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IMealsAndCategoriesRepository: IDisposable
    {
        string GetMealsAndCategories(int schoolId, DateTime deliveryTime);
        List<CanteenMigration.JSONModels.Category> GetCategoriesFromDB(int schoolId, string mealCode);
    }
}
