﻿


using CanteenMigration.Models;
using System;
using System.Collections.Generic;

namespace DataDependencies
{
    public interface ICartRepository:IDisposable
    {
        List<CanteenShopcartItem> GetCart(int parentId, int schoolId, DateTime deliveryDate);
        CanteenShopcartItem GetCartItem(int cartId, int parentId, int schoolId);
        bool EmptyCart(int parentId, int schoolId, DateTime deliveryDate);
        bool RemoveCartItem(int cartId, int parentId, int schoolId);
        bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId);
        CanteenShopcartItem GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions, string mealName, int studentId, DateTime deliveryDate);
        bool AddItemToCart(CanteenShopcartItem cartItem);
       //bool SaveContext();
        List<CanteenShopcartItem> GetCartForStudent(int studentId, int parentId, int schoolId, DateTime deliveryDate);
        bool AddItemsToCart(List<CanteenShopcartItem> cartItems);
        bool UpdateCartItems(List<CanteenShopcartItem> cartItems);
        int GetCartItemsCount(int parentId, int schoolId, DateTime deliveryDate);
    }
}
