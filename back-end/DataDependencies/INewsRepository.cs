﻿
namespace CanteenMigration.DataDependencies
{
    public interface INewsRepository: System.IDisposable
    {
        string GetCanteenNews(int schoolId);
    }
}
