﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;


namespace DataDependencies
{
    public interface IFundRaisingEventRepository
    {
        FundRaisingEventOrders AddDonation(FundRaisingEventOrders donaion);
        bool Update(int id, string paymentGatewayTransactionId, string stripeTransactionId, string status);
    }
}
