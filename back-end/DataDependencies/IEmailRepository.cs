﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IEmailRepository
    {
        bool AddEmail(string to, string subject, string messageBody);
    }
}
