﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataDependencies
{
    public interface IStudentRepository:IDisposable
    {
        Customers GetStudent(int studentId, int schoolId);
        List<StudentWithKlass> GetStudentsByParent(int schoolId, int parentId);
        string GetStudentName(int studentId, int schoolId);
        bool AddStudent(Customers newStudent);
        bool SaveUpdatedStudent();
        List<StudentWithKlass> GetAllStudentsByParent(int schoolId, int parentId);
        string GetStudentClassification(int schoolId, string studentClass);
        List<Customers> GetActiveStudents(int parentId, int schoolId);
    }
}
