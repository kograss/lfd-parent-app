﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IPublicHolidaysRepository
    {
        List<PublicHolidays> GetPublicHolidays(int schoolId, int year);
        bool IsDateComingIntoHolidays(int schoolId, DateTime deliveryDate);
        List<PublicHolidays> GetPublicHolidaysByMonth(int schoolId, int month);
    }
}
