﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IOrderRepository
    {
        void AddParentsOrder(ParentsOrders parentsOrders);
        void AddOrders(Orders order);
        //bool SaveContext();
        void InsertLunchOrderItems(List<OrderItems> orderItems);
        void InsertRecessOrderItems(List<OrderItemRecess> orderItem);
        void AddPaymentGatewayTransactionLog(PaymentGatewayTransactionLog log);
        bool CancelOrder(int orderId, int schoolId);
        bool IsOrderExists(int orderId, int parentId, int schoolId);
        int? GetMealWiseOrderNumber(string ordersOrderId, int studentId, int schoolId);
        ParentsOrders GetParentOrder(int orderId, int schoolId);
        bool CancelParentsOrder(ParentsOrders parentsOrder);
        bool AddToDeletedOrder(OrdersDeleted ordersDeleted);
        List<OrderItems> GetOrderItems(int orderId, int schoolId);
        List<OrderItemRecess> GetOrderItemsRecess(int orderId, int schoolId);
        bool AddOrderItemsRecessDeleted(List<OrderItemRecessDeleted> orderItemsRecess);
        bool AddOrderItemsDeleted(List<OrderItemsDeleted> orderItems);
        bool DeleteOrderItem(List<OrderItems> orderitems);
        bool DeleteOrderItemsRecess(List<OrderItemRecess> orderitemsrecess);
        Orders GetOrderByOrderNumber(string orderid, int schoolId);
        Orders GetOrder(int orderid, int schoolId);
        bool DeleteOrders(List<Orders> orders, int schoolId);
        Orders GetMealWiseOrders(string ordersOrderId, int schoolId);
        bool SetFavourite(int orderId, int schoolId);
        bool RemoveFavourite(int orderId, int schoolId);
    }
}
