﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IStripePaymentChargeRepository
    {
        CustomerStripeDetails GetParentStripeDetails(int schoolId, int parentId);
        bool AddParentStripeDetails(int schoolId, int parentId, string customerId, string cardId, string cardLast4);
        bool UpdateParentStripeDetails(int schoolId, int parentId, string cardId, string cardLast4);
    }
}
