﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ITopupRepository
    {
        void AddTopup(Topup topup);
        void UpdateTopupStatus(Topup topup, string paymentIdentifier, string status);
        bool CompleteManulTopup(ParentSelfTopup parentSelfTopup);
    }
}
