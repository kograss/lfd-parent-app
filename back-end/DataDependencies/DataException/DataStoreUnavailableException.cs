﻿
using System;

namespace CanteenMigration.DataDependencies.DataException
{
    public class DataStoreUnavailableException: Exception
    {
        public DataStoreUnavailableException() : base() { }
        public DataStoreUnavailableException(string message) : base(message) { }
        public DataStoreUnavailableException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected DataStoreUnavailableException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }
    }
}
