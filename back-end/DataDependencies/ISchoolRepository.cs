﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface ISchoolRepository
    {
        CanteenMigration.JSONModels.SchoolInfo GetSchoolInfo(int schoolId);
        //string GetSchoolInfo(int schoolId);
        List<Klass> GetKlass(int schoolId);
        bool IsMealAvailableForDate(int schoolId, DateTime deliveryDate, string mealType);
        string GetCanteenAdminEmail(int schoolId);
        string GetCanteenOrderConfirmationEmailText(int schoolId);
        CanteenMigration.JSONModels.SchoolShops GetSchoolShops(int schoolId);
        CanteenMigration.Models.SchoolSetup GetSchoolSetup(int schoolId);
    }
}
