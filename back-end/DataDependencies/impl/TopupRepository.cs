﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class TopupRepository : ITopupRepository
    {
        IDBContext _dbContext;
        public TopupRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public void AddTopup(Topup topup)
        {
            _dbContext.Topup.Add(topup);
            _dbContext.SaveChanges();
        }

        public void UpdateTopupStatus(Topup topup, string paymentIdentifier, string status)
        {
            topup.transactionId = paymentIdentifier;
            topup.topupStatus = status;
            _dbContext.SaveChanges();
        }

        public bool CompleteManulTopup(ParentSelfTopup parentSelfTopup)
        {
            _dbContext.ParentSelfTopup.Add(parentSelfTopup);
            _dbContext.SaveChanges();
            return true;
        }
    }
}
