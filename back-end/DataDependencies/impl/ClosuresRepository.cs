﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;

namespace DataDependencies.impl
{
    public class ClosuresRepository: IClosuresRepository
    {
        IDBContext _dbContext;
        public ClosuresRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Closures GetClousures(int schoolId)
        {
            return _dbContext.Closures.Where(e => e.schoolID == schoolId).FirstOrDefault();
        }
    }
}
