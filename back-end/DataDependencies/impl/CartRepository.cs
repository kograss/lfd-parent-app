﻿using System;
using System.Collections.Generic;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Linq;
using DataDependencies.connector.sql_server;
using CanteenMigration.BusinessDependencies;

namespace DataDependencies.impl
{
    public class CartRepository : SaveDBContext, ICartRepository
    {
        IDBContext _dbContext;
        private ILoggerService<CartRepository> _loggingService;
        public CartRepository(IDBContext dbContext, ILoggerService<CartRepository> _loggingService)
        {
            this._dbContext = dbContext;
            this._loggingService = _loggingService;
        }

        public bool AddItemToCart(CanteenShopcartItem cartItem)
        {
            _dbContext.CanteenShopCart.Add(cartItem);
            //SaveDbContext(_dbContext, cartItem);
            _dbContext.SaveChanges();
            return true;
        }

        public bool AddItemsToCart(List<CanteenShopcartItem> cartItems)
        {
            try
            {
                _dbContext.CanteenShopCart.AddRange(cartItems);
                //SaveDbContext(_dbContext, cartItems);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _loggingService.Debug("exception in the repo method is EmptyCart");
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                _loggingService.Debug("----------");
                throw e;
            }
        }

        public bool UpdateCartItems(List<CanteenShopcartItem> cartItems)
        {
            _dbContext.SaveChanges();
            //SaveDbContext(_dbContext, cartItems);
            return true;
        }
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public bool EmptyCart(int parentId, int schoolId, DateTime deliveryDate)
        {
            try
            {
                List<CanteenShopcartItem> cart = GetCart(parentId, schoolId, deliveryDate);
                _dbContext.CanteenShopCart.RemoveRange(cart);
                _dbContext.SaveChanges();
                //SaveDbContext(_dbContext, cart);
                return true;
            }
            catch (Exception e)
            {
                _loggingService.Debug("exception in the repo method is EmptyCart");
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                _loggingService.Debug("----------");
                throw e;
            }
        }

        public List<CanteenShopcartItem> GetCart(int parentId, int schoolId, DateTime deliveryDate)
        {
            List<CanteenShopcartItem> lst = this._dbContext.CanteenShopCart.Where(
                  e => e.schoolId == schoolId && e.parentId == parentId && e.cartDeliveryDate == deliveryDate).ToList();

            return lst;
        }

        public List<CanteenShopcartItem> GetCartForStudent(int studentId, int parentId, int schoolId, DateTime deliveryDate)
        {
            List<CanteenShopcartItem> lst = this._dbContext.CanteenShopCart
                                                        .Where(e => e.parentId == parentId && e.schoolId == schoolId
                                                            && e.cartDeliveryDate == deliveryDate && e.studentId == studentId
                                                        ).ToList();
            return lst;
        }

        public CanteenShopcartItem GetCartItem(int cartId, int parentId, int schoolId)
        {
            return _dbContext
                    .CanteenShopCart
                    .Where(e => e.id == cartId && e.parentId == parentId && e.schoolId == schoolId)
                    .FirstOrDefault();
        }

        public CanteenShopcartItem GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions,
                                                     string mealName, int studentId, DateTime deliveryDate)
        {
            return _dbContext.CanteenShopCart
                        .Where(e => e.parentId == parentId && e.productId == productId &&
                        e.productOptions == productOptions && e.mealName == mealName &&
                        e.schoolId == schoolId && e.studentId == studentId &&
                        e.cartDeliveryDate == deliveryDate).FirstOrDefault();
        }

        public bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId)
        {
            try
            {
                CanteenShopcartItem cartItem = GetCartItem(cartId, parentId, schoolId);
                cartItem.productQuantity = cartItem.productQuantity - 1;
                _dbContext.SaveChanges();
                //SaveDbContext(_dbContext, cartItem);
                return true;
            }
            catch (Exception e)
            {
                _loggingService.Debug("exception in the repo method is ReduceCartItemQuantity");
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                _loggingService.Debug("----------");
                throw e;
            }

        }

        public bool RemoveCartItem(int cartId, int parentId, int schoolId)
        {
            try
            {
                CanteenShopcartItem cartItem = GetCartItem(cartId, parentId, schoolId);
                _dbContext.CanteenShopCart.Remove(cartItem);
                _dbContext.SaveChanges();
                //SaveDbContext(_dbContext, cartItem);
                return true;
            }
            catch (Exception e)
            {
                _loggingService.Debug("exception in the repo method is RemoveCartItem");
                if (e != null)
                {
                    if (e.InnerException != null)
                    {
                        _loggingService.Debug(e.InnerException.ToString());
                    }

                    if (e.StackTrace != null)
                    {
                        _loggingService.Debug(e.StackTrace);
                    }
                }
                _loggingService.Debug("----------");
                throw e;
            }
        }

        public int GetCartItemsCount(int parentId, int schoolId, DateTime deliveryDate)
        {
            int? cnt = 0;
            cnt = _dbContext.CanteenShopCart.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.cartDeliveryDate == deliveryDate)
                .Sum(e => (int?)e.productQuantity.Value) ?? 0;
            return cnt.Value;
        }
    }
}
