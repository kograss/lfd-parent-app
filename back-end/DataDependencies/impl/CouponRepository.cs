﻿using System;
using System.Collections.Generic;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Linq;
using DataDependencies.connector.sql_server;

namespace DataDependencies.impl
{
    public class CouponRepository : ICouponRepository
    {
        IDBContext _dbContext;
        public CouponRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public List<Coupons> GetAllCoupons(int schoolId)
        {
            return
                _dbContext.Coupons.Where(e => e.schoolId == schoolId && e.status == true && e.expiryDate >= DateTime.Now).ToList();
        }

        public List<CouponRedeem> GetPatentsCoupons(int parentId, int schoolId)
        {
            return
                 _dbContext.CouponRedeem.Where(e => e.schoolId == schoolId && e.parentId == parentId).ToList();
        }

        public bool IsParentUsedThisCoupon(int couponId, int parentId, int schoolId)
        {
            var c = _dbContext.CouponRedeem.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.couponId == couponId).FirstOrDefault();
            if (c == null)
            {
                return false;
            }

            return true;
        }

        public bool RedeemCoupon(CouponRedeem redeem)
        {
            _dbContext.CouponRedeem.Add(redeem);
            _dbContext.SaveChanges();

            return true;

        }

        public Coupons GetCoupon(int couponId, int schoolId)
        {
            return 
                _dbContext.Coupons.Where(e => e.id == couponId && e.schoolId == schoolId).FirstOrDefault();
        }
    }
}
