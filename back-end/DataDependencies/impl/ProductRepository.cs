﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using StackExchange.Redis;
using CanteenMigration.DataDependencies.connector.redis;
using CanteenMigration.DataDependencies.DataException;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace DataDependencies.impl
{
    public class ProductRepository : IProductRepository
    {
        string PRODUCT_CACHE_KEY = "S{0}-{1}-C{2}";
        IDBContext _dbContext;
        RedisConnectorHelper _redisConnecterHelper;

        public ProductRepository(IDBContext dbContext, RedisConnectorHelper redisConnecterHelper)
        {
            this._dbContext = dbContext;
            _redisConnecterHelper = redisConnecterHelper;
        }

        public CanteenMigration.Models.Product GetProduct(int schoolId, int productId)
        {
            return _dbContext.Products.Where(e => e.schoolId == schoolId && e.id == productId).FirstOrDefault();
        }



        public List<CanteenMigration.Models.Product> GetCanteenProductsFromDB(int schoolId, string mealCode, int categoryId)
        {

            //select catalogID as id, ccode as mealCode, cname as productName, cprice as productPrice, cimageurl as productImageURL, cdateavailable as productDateAvailable, healthcategory as healthCategory, ccategory as productCategory,
            //food_label as foodLabel, quantity_limit as quantityLimit, cdescription as productDescription, productClassification as ProductClassification, extra2, cstock as isAvailableToOrder, optgID as optGId, optId2 as optID2,Fork as fork,
            //Spoon as spoon,Tspoon as tspoon, Knife as knife, Serviette as serviette, Straw as straw, Sauce as sauce, Stick as stick, pickingtype as pickingType,schoolid,special_event_id as specialEventId,ctemperature as cTemperature,
            //CreatedOn as createdOn,LastModified as lastModified,DeletedOn as deletedOn, exta1,extra1,stock,RowVersion
            //from products where schoolid =  @schoolId and ccategory = @categoryId
            //and ccode like '%'+@mealCode+'%' and cstock = 1 and (special_event_id <> 1 or special_event_id is null) and ccode <> 'NA'

            SqlParameter paramSchoolId = new SqlParameter("@schoolId", schoolId);
            SqlParameter paramMealCode = new SqlParameter("@mealCode", mealCode);
            SqlParameter paramCategId = new SqlParameter("@categoryId", categoryId);
            List<CanteenMigration.Models.Product> products = _dbContext.GetDatabase().SqlQuery<CanteenMigration.Models.Product>("exec dbo.SP_GetCanteenProductsFromDB @schoolId,@mealCode,@categoryId", paramSchoolId, paramMealCode, paramCategId).ToList();
            return products;
        }

        public string GetProducts(int schoolId, string mealCode, int categoryId)
        {
            IDatabase cache = null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();


            if (!cache.Equals(null))
            {
                string products = cache.StringGet(string.Format(PRODUCT_CACHE_KEY, schoolId, mealCode, categoryId));
                return products;
            }
            else
            {
                return null;
            }
        }

        public List<ProductStock> GetProductsStock(List<int> productsIds)
        {
            //_dbContext.Products.Where(e=> productsIds.Contains(e.id)).Select
            List<ProductStock> stock = (from product in _dbContext.Products
                                        where productsIds.Contains(product.id)
                                        select new ProductStock
                                        {
                                            productId = product.id,
                                            stock = product.stock
                                        }
             ).ToList();
            return stock;
        }
        public bool DeductProductStock(List<CanteenShopcartItem> allCartItems, int schoolId)
        {

            foreach (CanteenShopcartItem cartItem in allCartItems)
            {

                bool saveFailed;
                CanteenMigration.Models.Product product = _dbContext.Products
                                          .Where(e => e.id == cartItem.productId)
                                          .FirstOrDefault();
                do
                {
                    saveFailed = false;

                    try
                    {

                        if (product.stock - cartItem.productQuantity < 0)
                        {
                            return false;
                        }

                        product.stock = product.stock - cartItem.productQuantity;
                        _dbContext.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);



            }
            return true;
        }

        public bool AddStockToProduct(int productId, int stockToAdd, int schoolId)
        {

            bool saveFailed;
            CanteenMigration.Models.Product product = _dbContext.Products
                                      .Where(e => e.id == productId)
                                      .FirstOrDefault();
            do
            {
                saveFailed = false;

                try
                {
                    product.stock = product.stock + stockToAdd;
                    _dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                }

            } while (saveFailed);


            return true;
        }


        public List<CanteenMigration.Models.Product> GetProducts(IEnumerable<int> productIds)
        {
            return _dbContext.Products.Where(e => productIds.Contains(e.id) && e.isAvailableToOrder == 1 && e.mealCode != "NA"
                                            && e.specialEventId != 1 && e.stock != 0).ToList();
        }

        public List<CanteenProductAvailability> GetProductAvailability(List<int> productIds, int schoolId)
        {
            return _dbContext.CanteenProductAvailability.Where(e => productIds.Contains(e.productId.Value)).ToList();
        }
    }
}
