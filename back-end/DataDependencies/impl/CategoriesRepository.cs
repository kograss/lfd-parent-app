﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class CategoriesRepository: ICategoriesRepository
    {
        IDBContext _dbContext;

        public CategoriesRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public bool IsCategoryCutOff(int categoryId, DateTime nowInSchoolTimezone, DateTime orderDate)
        {
            var dd = nowInSchoolTimezone.Day;
            var mm = nowInSchoolTimezone.Month;
            var yyyy = nowInSchoolTimezone.Year;
            var hday = mm + "/" + dd + "/" + yyyy;//date.ToString("MM-dd-yyyy");
            var hours = nowInSchoolTimezone.Hour;
            var minutes = nowInSchoolTimezone.Minute;

            var ampm = "AM";
            if (hours >= 12)
                ampm = "PM";

            var hday_f = hday + " " + hours + ":" + minutes + ":" + "00" + ampm;

            using (School24Context context = new School24Context())
            {
                // 2. execute SP_CheckCategoryAvailableForOrder
                SqlParameter categoryIdParam = new SqlParameter("@CategoryId", categoryId);
                SqlParameter timenowInSchoolTimezone = new SqlParameter("@TimeNowInSchoolTimezone", hday_f);

                return context.Database.SqlQuery<bool>("SP_CheckCategoryAvailableForOrder", categoryIdParam, timenowInSchoolTimezone).FirstOrDefault();
            }
        }

        public bool IsCategoryUnavailableThisday(DateTime forWhatDay, int categoryId, string service, int schoolId)
        {
            return _dbContext.CategoryUnavailableForDate.
                Where(e => e.schoolId == schoolId && e.closeDate == forWhatDay && e.serviceToClose == service && e.categoryID == categoryId).Count() > 0;
        }

        public List<Category> GetCategories(int schoolId, List<int> categoryIds)
        {
            return _dbContext.Categories.Where(e => e.schoolId == schoolId && categoryIds.Contains(e.id) && e.isHide.Value != true).ToList();
        }
    }
}
