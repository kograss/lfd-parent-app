﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using MoreLinq;

namespace DataDependencies.impl
{
    public class StudentRepository : IStudentRepository
    {
        IDBContext _dbContext;
        public StudentRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public Customers GetStudent(int studentId, int schoolId)
        {
            return this._dbContext.Customers.Where(e => e.schoolId == schoolId && e.id == studentId).FirstOrDefault();
        }

        public List<Customers> GetActiveStudents(int parentId, int schoolId)
        {
            return this._dbContext.Customers.Where(e => 
                        e.schoolId == schoolId && e.parentId == parentId
                         && e.oldDudId == 1 && e.studentStatus == "ACTIVE" && e.studentClass != "NONE")
                         .ToList();
        }

        public List<StudentWithKlass> GetStudentsByParent(int schoolId, int parentId)
        {
            List<StudentWithKlass> students;
            students = (from student in _dbContext.Customers
                        join klass in _dbContext.Klass on student.studentClass equals klass.className into classes
                        from x in classes.DefaultIfEmpty()
                        where (student.schoolId == schoolId && student.parentId == parentId //&& x.schoolId == schoolId
                                && student.oldDudId == 1 && student.studentStatus == "ACTIVE" && student.studentClass != "NONE")
                        select new StudentWithKlass
                        {
                            studentId = student.id,
                            parentId = parentId,
                            schoolId = schoolId,
                            firstName = student.studentFirstName,
                            lastName = student.studentLastName,
                            studentClass = student.studentClass,
                            klass = x.classType,
                            //classForClassification = "ju",//GetStudentKlassClassification(klass.classType)
                            classForClassification = "ju",//GetStudentKlassClassification(klass.classType)
                            maxAllow = student.maxAllow,
                            studentCardId = student.studentCardId
                        }
                        ).DistinctBy(e => e.studentId).ToList();
            return students;
        }

        public string GetStudentClassification(int schoolId, string studentClass)
        {
            var klass = _dbContext.Klass.Where(e => e.schoolId == schoolId && e.className == studentClass).FirstOrDefault();
            if(klass != null && klass.classType != null)
            {
                return klass.classType;
            }
            return string.Empty;
        }
        public List<StudentWithKlass> GetAllStudentsByParent(int schoolId, int parentId)
        {
            List<StudentWithKlass> students;
            students = (from student in _dbContext.Customers
                        //join klass in _dbContext.Klass on student.studentClass equals klass.className 
                        where (student.schoolId == schoolId && student.parentId == parentId)
                        select new StudentWithKlass
                        {
                            studentId = student.id,
                            parentId = parentId,
                            schoolId = schoolId,
                            firstName = student.studentFirstName,
                            lastName = student.studentLastName,
                            studentClass = student.studentClass,
                            //klass = klass.classType,
                            classForClassification = "ju",//GetStudentKlassClassification(klass.classType)
                            status = student.studentStatus,
                            allergy = student.calergy,
                            maxAllow = student.maxAllow,
                            studentCardId = student.studentCardId
                        }
                        ).Distinct().ToList();
            return students;
        }

        public string GetStudentName(int studentId, int schoolId)
        {
            return _dbContext.Customers.Where(e => e.schoolId == schoolId && e.id == studentId).Select(e => e.studentFirstName).FirstOrDefault();
        }

        public bool AddStudent(Customers newStudent)
        {
            _dbContext.Customers.Add(newStudent);
            _dbContext.SaveChanges();
            return true;
        }

        public bool SaveUpdatedStudent()
        {
            _dbContext.SaveChanges();
            return true;
        }
    }
}
