﻿using CanteenMigration.DataDependencies.connector.redis;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class UniformRepository : IUniformRepository
    {
        string CATEGORY_KEY = "S{0}-Uniform";
        string PRODUCT_KEY = "S{0}-Uniform-C{1}";
        string OPTION_KEY = "S{0}-Uniform-P{1}";

        IDBContext _dbContext;
        RedisConnectorHelper _redisConnecterHelper;

        public UniformRepository(IDBContext dbContext, RedisConnectorHelper redisConnecterHelper)
        {
            _dbContext = dbContext;
            _redisConnecterHelper = redisConnecterHelper;
        }

        public List<UniformCategories> GetUniformCategories(int schoolId)
        {
            var uniformCategories = (from category in _dbContext.UniformDepartment
                                     where category.schoolId == schoolId
                                     select new UniformCategories
                                     {
                                         categoryId = category.id,
                                         description = category.description,
                                         image = category.image,
                                         name = category.name
                                     }
                                     ).ToList();
            return uniformCategories;
        }

        public List<CanteenMigration.JSONModels.UniformNews> GetUniformNews(int schoolId, int year)
        {
            var uniformnews = (from news in _dbContext.UniformNews
                               where news.status == "OPEN" && news.schoolId == schoolId
                                       && (news.dateEntered.Value.Year == year || news.dateEntered.Value.Year == year - 1)
                               select new CanteenMigration.JSONModels.UniformNews
                               {
                                   newsID = news.newsId,
                                   dateEntered = news.dateEntered,
                                   description = news.description,
                                   subject = news.subject
                               }
                        ).OrderByDescending(e => e.dateEntered).ToList(); 
            return uniformnews;
        }

        public List<CanteenMigration.JSONModels.UniformProduct> GetUniformProductsByCategory(int schoolId, int categoryId)
        {
            var products = (from product in _dbContext.UniformProduct
                            join dept in _dbContext.UniformDepartmentProduct on product.Id equals dept.productId
                            where product.SchoolId == schoolId && dept.departmentId == categoryId
                            && product.Active == 1
                            select new CanteenMigration.JSONModels.UniformProduct
                            {
                                productId = product.Id,
                                description = product.Description,
                                image = product.ImageUrl,
                                name = product.Name,
                                price = product.Price,
                                stock = product.ProductStock,
                                isUniformPack = false
                            }
                            ).ToList();

            var uniformPackProduct = (from uniformPack in _dbContext.UniformPack
                                      join dept in _dbContext.UniformDepartmentProduct on uniformPack.idForCategory equals dept.productId
                                      where uniformPack.SchoolId == schoolId && dept.departmentId == categoryId
                                      && uniformPack.Active == 1
                                      select new CanteenMigration.JSONModels.UniformPack
                                      {
                                          productId = uniformPack.Id,
                                          description = uniformPack.Description,
                                          image = uniformPack.ImageUrl,
                                          name = uniformPack.Name,
                                          price = uniformPack.Price,
                                          stock = uniformPack.UniformPackStock,
                                          idForCategory = uniformPack.idForCategory,
                                          isUniformPack = true
                                      }
                            ).ToList();
            products.AddRange(uniformPackProduct);
            return products;
        }

        public List<CanteenMigration.JSONModels.UniformProduct> GetProducts(int schoolId, int uniformPackId)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@schoolId", schoolId));
            sqlParameters.Add(new SqlParameter("@uniformPackId", uniformPackId));
            var data = _dbContext.GetDatabase()
                    .SqlQuery<CanteenMigration.JSONModels.UniformProduct>("exec Sp_Uniform_Admin_GetUniformPackProducts @schoolId, @uniformPackId", sqlParameters.ToArray())
                    .ToList();
            sqlParameters.Clear();
            return data;
        }

        public List<CanteenMigration.JSONModels.UniformPack> GetUniformPacksByCategory(int schoolId, int categoryId)
        {
            var uniformPackProduct = (from uniformPack in _dbContext.UniformPack
                            join dept in _dbContext.UniformDepartmentProduct on uniformPack.idForCategory equals dept.productId
                            where uniformPack.SchoolId == schoolId && dept.departmentId == categoryId
                            && uniformPack.Active == 1
                            select new CanteenMigration.JSONModels.UniformPack
                            {
                                productId = uniformPack.Id,
                                description = uniformPack.Description,
                                image = uniformPack.ImageUrl,
                                name = uniformPack.Name,
                                price = uniformPack.Price,
                                stock = uniformPack.UniformPackStock,
                                idForCategory = uniformPack.idForCategory
                            }
                            ).ToList();
            return uniformPackProduct;
        }

        //public List<CanteenMigration.JSONModels.UniformProduct> GetUniformPackProducts(int schoolId, int uniformPackId)
        //{
        //    List<SqlParameter> sqlParameters = new List<SqlParameter>();
        //    sqlParameters.Add(new SqlParameter("@schoolId", schoolId));
        //    sqlParameters.Add(new SqlParameter("@uniformPackId", uniformPackId));
        //    var data = _dbContext.GetDatabase()
        //            .SqlQuery<CanteenMigration.JSONModels.UniformProduct>("exec Sp_Uniform_Admin_GetUniformPackProducts @schoolId, @uniformPackId", sqlParameters.ToArray())
        //            .ToList();
        //    sqlParameters.Clear();

        //    foreach (CanteenMigration.JSONModels.UniformProduct product in data)
        //    {
        //        product.stock = this.GetProductStock(schoolId, product.productId);
        //        product.options = this.GetProductOptionsFromDB(schoolId, product.productId);
        //    }

        //    return data;
        //}

        public List<UniformBasketItem> GetBasketItems(int basketId, int parentId, int schoolId)
        {
            var basketItems = _dbContext.UniformBasketItem.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.basketId == basketId).ToList();
            foreach (var item in basketItems)
            {
                if (item.isUniformPack.GetValueOrDefault() != 0)
                {

                    item.uniformPackItemProducts = new List<UniformPackItemProduct>();
                    int id = item.id;
                    SqlParameter itemId = new SqlParameter("@itemId", item.id);
                    item.uniformPackItemProducts = _dbContext.GetDatabase().SqlQuery<UniformPackItemProduct>
                                    ("exec Sp_UniformAdmin_GetUniformPackItemProducts @itemId", itemId)
                                    .ToList();
                }
            }
            return basketItems;
        }

        public UniformBasketItem GetBasketItem(int parentId, int schoolId, int basketItemId)
        {
            return _dbContext.UniformBasketItem.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.id == basketItemId).FirstOrDefault();
        }
        public UniformBasketItem AddToBasket(int parentId, int schoolId, UniformBasketItem item)
        {
            UniformBasketItem itemAdded = _dbContext.UniformBasketItem.Add(item);
            _dbContext.SaveChanges();
            return itemAdded;
        }

        public UniformBasket GetBasket(int basketId, int parentId, int schoolId)
        {
            return _dbContext.UniformBasket
                    .Where(e => e.SchoolId == schoolId && e.ParentId == parentId && e.Id == basketId)
                    .FirstOrDefault();
                    
        }

        public bool AddUniformPackItemProducts(List<UniformPackItemProduct> products, int itemId)
        {
            //UniformBasketItem last = _dbContext.UniformBasketItem.ToList().LastOrDefault();
            foreach (var product in products)
            {
                product.itemId = itemId;
                _dbContext.UniformPackItemProduct.Add(product);
            }
            
            _dbContext.SaveChanges();
            return true;
        }



        public bool UpdateCartQuantity(UniformBasketItem item, int quantityToAdd)
        {
            item.quantity = item.quantity + quantityToAdd;
            _dbContext.SaveChanges();
            return true;
        }

        public bool EmptyCart(List<UniformBasketItem> items)
        {
            foreach(var item in items)
            {
                if (item.isUniformPack.GetValueOrDefault() != 0)
                {
                    List<UniformPackItemProduct> products = _dbContext.UniformPackItemProduct.Where(e => e.itemId == item.id ).ToList();
                    _dbContext.UniformPackItemProduct.RemoveRange(products);
                }
            }
            _dbContext.UniformBasketItem.RemoveRange(items);
            _dbContext.SaveChanges();
            return true;
        }

        public bool ReduceBasketItemQuantity(UniformBasketItem item, int quantityToReduce)
        {
            item.quantity = item.quantity - quantityToReduce;
            if (item.quantity <= 0)
            {
                _dbContext.UniformBasketItem.Remove(item);
            }

            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveItemFromBasket(UniformBasketItem item)
        {
            if (item.isUniformPack.GetValueOrDefault() != 0)
            {
                List<UniformPackItemProduct> products = _dbContext.UniformPackItemProduct.Where(e => e.itemId == item.id).ToList();
                _dbContext.UniformPackItemProduct.RemoveRange(products);
            }
            _dbContext.UniformBasketItem.Remove(item);
            _dbContext.SaveChanges();
            return true;
        }
        public UniformBasketItem GetItemFromBasket(UniformBasketItem item)
        {
            
                return _dbContext.UniformBasketItem.Where(e => e.schoolId == item.schoolId && e.color == item.color
                                                      && e.parentId == item.parentId && e.productId == item.productId
                                                      && e.name == item.name && e.size == item.size && e.basketId == item.basketId
                                                      ).FirstOrDefault();
            
        }

        public UniformPackItemProduct GetUPackSubItemFromBasket(UniformPackItemProduct item)
        {
            return _dbContext.UniformPackItemProduct.Where(e =>  e.color == item.color && e.productId == item.productId
                                                  && e.name == item.name && e.size == item.size && e.itemId == item.itemId
                                                  ).FirstOrDefault();
        }

        public List<UniformPackItemProduct> GetAllUPackSubItem(int id)
        {
            var uPackItemProducts = new List<UniformPackItemProduct>();
            SqlParameter itemId = new SqlParameter("@itemId", id);
            uPackItemProducts = _dbContext.GetDatabase().SqlQuery<UniformPackItemProduct>
                            ("exec Sp_UniformAdmin_GetUniformPackItemProducts @itemId", itemId)
                            .ToList();
            return uPackItemProducts;
        }

        public int GetBasketCount(int basketId, int parentId, int schoolId)
        {
            return _dbContext.UniformBasketItem
                    .Where(e => e.schoolId == schoolId && e.parentId == parentId && e.basketId == basketId)
                    .Select(e => e.quantity.Value)
                    .DefaultIfEmpty(0)
                    .Sum();
        }

        public int? GetProductStock(StockInput input)
        {
            return _dbContext.UniformStocks.Where(e => e.SchoolId == input.schoolId && e.ProductId == input.productId
                                                && e.SizeAttributeId == input.sizeAttributeId && e.ColorAttributeId == input.colorAttributeId
                                          ).Select(e => e.AvailableCount).FirstOrDefault();
        }

        public int? GetProductStock(int schoolId, int productId)
        {
            return _dbContext.UniformProduct.Where(e => e.SchoolId == schoolId && e.Id == productId).Select(e => e.ProductStock).FirstOrDefault();
        }
        //# cache methods
        public string GetCategories(int schoolId)
        {
            IDatabase cache = null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();


            if (!cache.Equals(null))
            {
                string categoriesData = cache.StringGet(string.Format(CATEGORY_KEY, schoolId));
                return categoriesData;
            }
            else
            {
                return null;
            }

        }

        public string GetCategoryProducts(int schoolId, int categoryId)
        {
            IDatabase cache = null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();


            if (!cache.Equals(null))
            {
                string productsData = cache.StringGet(string.Format(PRODUCT_KEY, schoolId, categoryId));
                return productsData;
            }
            else
            {
                return null;
            }

        }

        public string GetProductOptions(int schoolId, int productId)
        {
            IDatabase cache = null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();


            if (!cache.Equals(null))
            {
                string optionsData = cache.StringGet(string.Format(OPTION_KEY, schoolId, productId));
                return optionsData;
            }
            else
            {
                return null;
            }
        }

        public List<ProductOption> GetProductOptionsFromDB(int schoolId, int productId)
        {
            //                select uniform_products.idproduct as productId, uniform_attribute.chrattributename as optionName ,
            //uniform_attributecategory.chrCategoryName as categoryName, uniform_attribute.idattribute as optionId 
            //from uniform_products, uniform_productattribute, uniform_attribute, uniform_attributecategory 
            //where uniform_products.schoolid = 167 and uniform_products.idproduct = 2454 
            //and uniform_productattribute.idproduct = 2454 
            //and uniform_productattribute.idattribute = uniform_attribute.idattribute 
            //and uniform_attribute.idattributecategory = uniform_attributecategory.idattributecategory
            SqlParameter paramSchoolId = new SqlParameter("@schoolId", schoolId);
            SqlParameter paramProductId = new SqlParameter("@productId", productId);
            List<ProductOption> options = _dbContext.GetDatabase().SqlQuery<ProductOption>("exec dbo.SP_GetProductOptionsFromDB @schoolId,@productId", paramSchoolId, paramProductId).ToList();
            
            return options;
        }

        public int CreateBasket(int parentId, int schoolId)
        {
            UniformBasket basket = new UniformBasket();
            basket.SchoolId = schoolId;
            basket.ParentId = parentId;
            _dbContext.UniformBasket.Add(basket);
            _dbContext.SaveChanges();
            return basket.Id;
        }
        public bool UpdateBasket(UniformBasket basket, string studentId, string studentClass, string comment)
        {
            _dbContext.UniformPackItemProduct.Local.Clear();
            basket.Student = studentId;
            basket.Class = studentClass;
            basket.Comment = comment;
            _dbContext.SaveChanges();
            return true;
        }

        public UniformBasket GetParentBasket(int basketId, int parentId, int schoolId)
        {
            return _dbContext.UniformBasket.Where(e => e.Id == basketId && e.ParentId == parentId && e.SchoolId == schoolId).FirstOrDefault();
        }

        public bool AddUniformOrderData(UniformOrderData uniformOrderData)
        {
            _dbContext.UniformOrderData.Add(uniformOrderData);
            _dbContext.SaveChanges();
            return true;
        }

        public UniformOrderData GetUniformOrderData(UniformOrderData uniformOrderData)
        {
            return _dbContext.UniformOrderData.Where(u => u.BasketId == uniformOrderData.BasketId && u.OrderId == uniformOrderData.OrderId && u.ShopperId == uniformOrderData.ShopperId).FirstOrDefault();
        }

        public bool AddUniformOrderStatus(UniformOrderStatus uniformOrderStatus)
        {
            _dbContext.UniformOrderStatus.Add(uniformOrderStatus);
            _dbContext.SaveChanges();
            return true;
        }

        public bool AddUniformOrderPayment(UniformOrderPayment uniformOrderPayment)
        {
            _dbContext.UniformOrderPayment.Add(uniformOrderPayment);
            _dbContext.SaveChanges();
            return true;
        }

        public UniformOrderPayment GetUniformOrderPayment(PaymentGatewayTransactionLog log)
        {
            return _dbContext.UniformOrderPayment.Where(u => u.TransactionId == log.Id && u.ParentId == log.ParentId).FirstOrDefault();
        }

        public CanteenMigration.Models.UniformProduct GetUniformProduct(int productId)
        {
            return _dbContext.UniformProduct.Where(e => e.Id == productId).FirstOrDefault();
        }


        public CanteenMigration.Models.UniformPack GetUniformPack(int productId)
        {
            return _dbContext.UniformPack.Where(e => e.Id == productId).FirstOrDefault();
        }

        public UniformAttribute GetUniformAttributeByName(string attribName)
        {
            return _dbContext.
                   UniformAttribute.
                   Where(e => e.attributeName == attribName).
                   FirstOrDefault();
        }
        public UniformOrderStatus GetOrderStatusByOrderId(int orderId)
        {
            return _dbContext.
                   UniformOrderStatus.
                   Where(e => e.OrderId == orderId).
                   FirstOrDefault();
        }

        public CanteenMigration.Models.UniformStocks GetUniformStock(int schoolId, int productId, int? colorAttributeId, int? sizeAttributeId)
        {
            return
            _dbContext.UniformStocks.
                    Where(e => e.ColorAttributeId == (colorAttributeId == 0 ? (int?)null : colorAttributeId) &&
                        e.SizeAttributeId == (sizeAttributeId == 0 ? (int?)null : sizeAttributeId) &&
                        e.ProductId == productId &&
                        e.SchoolId == schoolId).
                    FirstOrDefault();
        }

        public void SaveContext()
        {
            _dbContext.SaveChanges();
        }
        public List<UniformOrder> GetParentOrders(int parentId, int schoolId, int year)
        {
            var orders = (from order in _dbContext.UniformOrderData
                          join basket in _dbContext.UniformBasket on order.BasketId equals basket.Id
                          join basketItem in _dbContext.UniformBasketItem on basket.Id equals basketItem.basketId
                          join orderStatus in _dbContext.UniformOrderStatus on order.OrderId equals orderStatus.OrderId
                          where orderStatus.StageId != 9 && order.OrderedDate.Value.Year >= year && order.ShopperId == parentId &&
                            order.SchoolId == schoolId
                          select new UniformOrder
                          {
                              orderId = order.OrderId.Value,
                              orderAmount = basket.Total.Value,
                              stage = orderStatus.StageId.Value,
                              comment = basket.Comment,
                              orderDate = order.OrderedDate,
                              adminComments = orderStatus.Notes
                              //parentEMail = 
                          }

                          ).Distinct().OrderByDescending(e => e.orderId).ToList();

            foreach(var o in orders)
            {
                if(o.orderAmount == null || o.orderAmount == 0)
                {
                    var orDetails = GetUniformOrderDetails(schoolId, parentId, o.orderId);
                    var xp = orDetails.Sum(e => (e.price * (decimal) e.quantity));

                    o.orderAmount = xp;
                }
            }
            
            return orders;
        }


        public List<OrderDetails> GetUniformOrderDetails(int schoolId, int parentId, int orderId)
        {
            var orderDetails = (from order in _dbContext.UniformOrderData
                                join basketItem in _dbContext.UniformBasketItem on order.BasketId equals basketItem.basketId
                                where order.OrderId == orderId && order.ShopperId.Value == parentId && order.SchoolId == schoolId
                                select new OrderDetails
                                {
                                    orderId = orderId,
                                    productId = basketItem.productId.Value,
                                    productName = basketItem.name,
                                    quantity = basketItem.quantity.Value,
                                    price = basketItem.price,
                                    comment = order.OrderNote,
                                    sizeOptions = basketItem.size,
                                    colorOptions = basketItem.color,
                                    isUniformPack = basketItem.isUniformPack != null? basketItem.isUniformPack.Value : 0,
                                    basketItemId = basketItem.id,
                                    deliveryFee = order.deliveryFee,
                                    collectionOptionType = order.collectionOption,
                                    deliverySuburb = order.deliverySuburb,
                                    deliveryAddress = order.deliveryAddress,
                                    deliveryPostCode = order.deliveryPostCode,
                                    deliveryState = order.deliveryState

        //subTotal = order.sub TODO: check if this is available in DB
    }
                                ).ToList();
            return orderDetails;
        }

        public CanteenMigration.Models.UniformAdmin GetUniformAdmin(int schoolId)
        {
            return _dbContext.UniformAdmin.Where(e => e.schoolId == schoolId).FirstOrDefault();
        }

        public CanteenMigration.Models.UniformSetup GetUniformSetup(int schoolId)
        {
            return _dbContext.UniformSetup.Where(e => e.schoolId == schoolId).FirstOrDefault();
        }

        public bool AddUniformAddress(UniformAddress uniformAddress)
        {
            _dbContext.UniformAddress.Add(uniformAddress);
            _dbContext.SaveChanges();
            return true;
        }

        public CanteenMigration.Models.UniformAddress GetUniformAddress(int parentId)
        {
            return _dbContext.UniformAddress.Where(e => e.parentId == parentId).FirstOrDefault();
       
        }

        public List<CanteenMigration.JSONModels.UniformPackProduct> GetUniformPackProducts(int schoolId, int uniformPackId)
        {

            List<CanteenMigration.JSONModels.UniformPackProduct> uPackProducts = new List<CanteenMigration.JSONModels.UniformPackProduct>();
            List<CanteenMigration.Models.UniformPackProduct> uPackProductsNext = new List<CanteenMigration.Models.UniformPackProduct>();
            uPackProductsNext = _dbContext.UniformPackProduct.Where(p => p.uniformPackId == uniformPackId).ToList();
            foreach (CanteenMigration.Models.UniformPackProduct product in uPackProductsNext)
            {
                CanteenMigration.JSONModels.UniformPackProduct uPackProduct = new CanteenMigration.JSONModels.UniformPackProduct();
                uPackProduct.id = product.id;
                uPackProduct.productId = product.productId;
                uPackProduct.name = product.name;
                uPackProduct.quantity = product.quantity;
                uPackProduct.uniformPackId = uniformPackId;
                uPackProduct.stock = this.GetProductStock(schoolId, product.productId);
                uPackProduct.options = this.GetProductOptionsFromDB(schoolId, product.productId);
                uPackProducts.Add(uPackProduct);
            }

            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@schoolId", schoolId));
            sqlParameters.Add(new SqlParameter("@uniformPackId", uniformPackId));
            var data = _dbContext.GetDatabase()
                    .SqlQuery<CanteenMigration.JSONModels.UniformPackProduct>("exec Sp_Uniform_Admin_GetUniformPackProducts @schoolId, @uniformPackId", sqlParameters.ToArray())
                    .ToList();
            sqlParameters.Clear();

            foreach (CanteenMigration.JSONModels.UniformPackProduct product in data)
            {
                product.stock = this.GetProductStock(schoolId, product.productId);
                product.options = this.GetProductOptionsFromDB(schoolId, product.productId);
                product.quantity = 1;
                product.uniformPackId = uniformPackId;
            }

            uPackProducts.AddRange(data);


            return uPackProducts;
        }

    }
}
