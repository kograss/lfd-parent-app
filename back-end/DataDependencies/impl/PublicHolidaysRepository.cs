﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;

namespace DataDependencies.impl
{
    public class PublicHolidaysRepository : IPublicHolidaysRepository
    {
        IDBContext _dbContext;
        public PublicHolidaysRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public List<PublicHolidays> GetPublicHolidays(int schoolId, int year)
        {
            return this._dbContext.Holidays
                                            .Where(e => e.schoolId == schoolId && 
                                            (e.holidayDateEnd.Value.Year == year || e.holidayDateEnd.Value.Year == year + 1))
                                            .ToList();
        }
        
        public bool IsDateComingIntoHolidays(int schoolId, DateTime deliveryDate)
        {
            bool b = false;
            b = _dbContext.Holidays.Where(e => e.schoolId == schoolId && e.holidayDateStart.Value <= deliveryDate && e.holidayDateEnd.Value >= deliveryDate)
                                      .Count() > 0;
            return b;
        }

        public List<PublicHolidays> GetPublicHolidaysByMonth(int schoolId, int month)
        {
            var hol =
              _dbContext.Holidays.Where(e => e.schoolId == schoolId
                                        && (e.holidayDateStart.Value.Month == month || e.holidayDateEnd.Value.Month == month)).ToList();
            return hol;
        }
    }
}
