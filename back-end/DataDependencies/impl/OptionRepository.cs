﻿using System;
using System.Collections.Generic;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Linq;
using StackExchange.Redis;
using CanteenMigration.DataDependencies.connector.redis;
using CanteenMigration.DataDependencies.DataException;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using CanteenMigration.JSONModels;

namespace DataDependencies.impl
{
    public class OptionRepository : IOptionRepository
    {
        string OPTION_CACHE_KEY = "S{0}-{1}-P{2}";
        IDBContext _dbContext;
        RedisConnectorHelper _redisConnecterHelper;


        public OptionRepository(IDBContext dbContext, RedisConnectorHelper redisConnecterHelper)
        {
            this._dbContext = dbContext;
            _redisConnecterHelper = redisConnecterHelper;
        }
        public void Dispose()
        {
            //throw new NotImplementedException();
        }


        public Option GetOption(int optionId, int schoolId)
        {
            return this._dbContext.Options.Where(e => e.id == optionId && e.schoolId == schoolId).FirstOrDefault();
        }

        public IQueryable<Option> GetOptions(int[] optionIds, int schoolId)
        {
            return this._dbContext.Options.Where(e => optionIds.Contains(e.id) && e.schoolId == schoolId);
        }

        public string GetProductOptions(int schoolId, string mealName, int productId)
        {
            IDatabase cache = null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();


            if (!cache.Equals(null))
            {
                string products = cache.StringGet(string.Format(OPTION_CACHE_KEY, schoolId, mealName, productId));
                return products;
            }
            else
            {
                return null;
            }
        }

        public List<Options> GetCanteenOptionsFromDB(int schoolId, string mealCode, int productId)
        {
            // select OPT_ID as id, OPT_Name1 as optionName1, OPT_Name2 as optionName2, OPT_Name3 as optionName3,OPT_Name4 as optionName4, 
            //OPT_Name5 as optionName5, OPT_CheckBoxValue as optionCheckBoxValue, OPT_PriceAdd as optionPrice, opt_stock as optionStock, 
            //OPT_OptionGroupId as optionGroupId, schoolid as schoolId,Option_SubGroupId as optionSubGroupId, Max_Items as maxItems, Required as isRequired,
            //OPT_DefWeightChange as optionDefWeightChange,OPT_DefOrderByValue as optionDefOrderByValue,Position as position
            //from options where OPT_OptionGroupID = @productId and schoolid = @schoolId and opt_stock = 1 

            SqlParameter paramSchoolId = new SqlParameter("@schoolId", schoolId);
            SqlParameter paramProductId = new SqlParameter("@productId", productId);
            List<CanteenMigration.Models.Option> optionsFromDB = _dbContext.GetDatabase().SqlQuery<CanteenMigration.Models.Option>("exec dbo.SP_GetCanteenOptionsFromDB @schoolId,@productId", paramSchoolId, paramProductId).ToList();
            List<Options> options = new List<Options>();
            foreach (var item in optionsFromDB)
            {
                Options newOption = new Options();
                newOption.OPT_ID = item.id;
                newOption.OPT_Name1 = item.optionName1;
                newOption.OPT_Name5 = item.optionName5;
                newOption.OPT_PriceAdd = item.optionPrice;
                newOption.opt_stock = item.optionStock;
                newOption.Option_SubGroupId = item.optionSubGroupId;
                newOption.Max_Items = item.maxItems;
                newOption.Required = item.isRequired;
                newOption.schoolId = item.schoolId.Value;

                options.Add(newOption);
            }

            return options;
        }

        public string GetOptionName(int optionId, int schoolId)
        {
            string optionName = string.Empty;
            optionName = _dbContext.Options.Where(e => e.schoolId == schoolId && e.id == optionId).Select(e => e.optionName1).FirstOrDefault();
            return optionName;
        }

        public string GetSchoolEventOptionName(int optionId, int schoolId)
        {
            string optionName = string.Empty;
            optionName = _dbContext.EventOption.Where(e => e.schoolId == schoolId && e.id == optionId).Select(e => e.optionName1).FirstOrDefault();
            return optionName;
        }

        public decimal GetAllOptionsPrice(int schoolId, int[] intOptionIds)
        {
            decimal optionPrice = 0;
            optionPrice = (decimal)_dbContext.Options.Where(e => intOptionIds.Contains(e.id) && e.schoolId == schoolId).Select(e => e.optionPrice).Sum();
            return optionPrice;
        }
        public decimal GetOptionPrice(int optionId, int schoolId)
        {
            decimal optionPrice = 0;
            optionPrice = (decimal)_dbContext.Options.Where(e => e.schoolId == schoolId && e.id == optionId).Select(e => e.optionPrice).FirstOrDefault();
            return optionPrice;
        }

        public List<Option> GetOptions(List<int> optionIds, int productId)
        {
            return _dbContext.Options.Where(e => optionIds.Contains(e.id) && e.optionGroupId == productId && e.optionStock == 1).ToList();
        }

        public List<int> GetOptionIds(List<int> optionIds, int productId)
        {
            return _dbContext.Options.Where(e => optionIds.Contains(e.id) && e.optionGroupId == productId && e.optionStock == 1)
                .Select(e => e.id).ToList();
        }

        public List<CanteenMigration.JSONModels.Options> GetAllProductOptions(int schoolId, int productId)
        {
            List<CanteenMigration.JSONModels.Options> options = (from option in _dbContext.Options
                                                                 where option.optionGroupId == productId && option.schoolId == schoolId
                                                                 && option.optionStock == 1
                                                                 select new CanteenMigration.JSONModels.Options
                                                                 {
                                                                     OPT_ID = option.id,
                                                                     Max_Items = option.maxItems,
                                                                     Option_SubGroupId = option.optionSubGroupId,
                                                                     OPT_Name1 = option.optionName1,
                                                                     OPT_Name5 = option.optionName5,
                                                                     OPT_PriceAdd = option.optionPrice.Value,
                                                                     opt_stock = option.optionStock,
                                                                     Required = option.isRequired
                                                                 }
                     ).ToList();
            return options;
        }
    }
}
