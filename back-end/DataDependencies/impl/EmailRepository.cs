﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class EmailRepository : IEmailRepository
    {

        IDBContext _dbContext;
        public EmailRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public bool AddEmail(string to, string subject, string messageBody)
        {
            Email email = new Email();
            email.to = to;
            email.subject = subject;
            email.messageBody = messageBody;
            email.addedDateTime = DateTime.Now;
            email.failedEmailSendAtteptCount = 0;
            _dbContext.Email.Add(email);
            try
            {
                _dbContext.SaveChanges();
            }
            catch (DbUpdateException dbu)
            {
                var exception = HandleDbUpdateException(dbu);
                throw exception;
            }


            return true;
        }

        private Exception HandleDbUpdateException(DbUpdateException dbu)
        {
            var builder = new StringBuilder("A DbUpdateException was caught while saving changes. ");

            try
            {
                foreach (var result in dbu.Entries)
                {
                    builder.AppendFormat("Type: {0} was part of the problem. ", result.Entity.GetType().Name);
                }
            }
            catch (Exception e)
            {
                builder.Append("Error parsing DbUpdateException: " + e.ToString());
            }

            string message = builder.ToString();
            return new Exception(message, dbu);
        }
    }
}
