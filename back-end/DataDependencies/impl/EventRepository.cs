﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DataDependencies.connector.sql_server;
using System.Data.Entity.Infrastructure;

namespace DataDependencies.impl
{
    public class EventRepository : SaveDBContext, IEventRepository
    {
        IDBContext _dbContext;
        public EventRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public SpecialEvent GetSpecialEvent(int schoolId, int eventId)
        {
            return _dbContext.SchoolEvent.Where(e => e.SchoolId == schoolId && e.specialId == eventId).FirstOrDefault();
        }

        public List<SchoolEventInfo> GetSchoolEvents(int schoolId, DateTime currentDateInSchoolTimeZone)
        {
            var schoolEvents = (from schoolEvent in _dbContext.SchoolEvent
                                where schoolEvent.SchoolId == schoolId
                                      && schoolEvent.specialDateStart.Value <= currentDateInSchoolTimeZone
                                      && schoolEvent.specialDateEnd.Value >= currentDateInSchoolTimeZone
                                      && schoolEvent.IsWeekly != true
                                select new SchoolEventInfo
                                {
                                    eventId = schoolEvent.specialId,
                                    eventDate = schoolEvent.EventDate.Value,
                                    eventName = schoolEvent.EventName,
                                    orderingCloseDate = schoolEvent.specialDateEnd.Value,
                                    bagName = schoolEvent.bagName,
                                    bagPrice = schoolEvent.BagPrice,
                                    isBagAvailable = schoolEvent.isBagAvailable,
                                    IsSetPrice = schoolEvent.IsSetPrice,
                                    eventPrice = schoolEvent.EventPrice,
                                    whoPays = schoolEvent.whoPays,
                                    description = schoolEvent.description,
                                    documents = schoolEvent.documents,
                                    images = schoolEvent.images,
                                    event_type = schoolEvent.EventType,
                                    eventType = "CANTEEN",
                                    isFundRaisingEvent = schoolEvent.isFundRaisingEvent
                                }).ToList();

            return schoolEvents;
        }

        public List<SchoolEventCategory> GetEventCategories(int schoolId, int eventId)
        {
            var categories = (from category in _dbContext.CategoriesAndEvents
                              where category.schoolId == schoolId && category.eventId == eventId
                              select new SchoolEventCategory
                              {
                                  categoryId = category.categoryId,
                                  eventId = category.eventId.Value,
                                  categoryName = category.categoryName
                              }
                              ).ToList();
            return categories;
        }

        public List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId)
        {
            List<CanteenMigration.JSONModels.Product> products = (from product in _dbContext.Products
                                                                  join productEvent in _dbContext.ProductsAndEvents on product.id equals productEvent.catalogId
                                                                  where productEvent.eventId == eventId && product.schoolId == schoolId
                                                                  select new CanteenMigration.JSONModels.Product
                                                                  {
                                                                      ccategory = product.productCategory != null ? product.productCategory.Value : 0,
                                                                      cname = product.productName,
                                                                      cdescription = product.productDescription,
                                                                      healthcategory = product.healthCategory,
                                                                      cprice = product.productPrice,
                                                                      catalogID = product.id,
                                                                      cimageurl = product.productImageURL,
                                                                      quantity_limit = product.quantityLimit,
                                                                      stock = productEvent.stock,
                                                                  }
                     ).ToList();
            return products;
        }

        public List<CanteenMigration.JSONModels.Product> GetCategoriesProductEvents(int schoolId, int eventId, int categoryId)
        {
            List<CanteenMigration.JSONModels.Product> products = (from product in _dbContext.Products
                                                                  join productEvent in _dbContext.ProductsAndEvents on product.id equals productEvent.catalogId
                                                                  where productEvent.eventId == eventId && product.schoolId == schoolId
                                                                        && product.productCategory == categoryId
                                                                  select new CanteenMigration.JSONModels.Product
                                                                  {
                                                                      ccategory = product.productCategory != null ? product.productCategory.Value : 0,
                                                                      cname = product.productName,
                                                                      cdescription = product.productDescription,
                                                                      healthcategory = product.healthCategory,
                                                                      cprice = product.productPrice,
                                                                      catalogID = product.id,
                                                                      cimageurl = product.productImageURL,
                                                                      quantity_limit = product.quantityLimit,
                                                                      stock = productEvent.stock,
                                                                  }
                     ).ToList();
            return products;
        }

        public List<EventShopcart> GetCart(int schoolId, int parentId, int eventId)
        {
            return _dbContext.EventShopcart.Where(e => e.schoolId == schoolId && e.eventId == eventId && e.parentId == parentId).ToList();
        }

        public EventShopcart GetCartItem(int cartId, int parentId, int schoolId)
        {
            return _dbContext
                    .EventShopcart
                    .Where(e => e.id == cartId && e.parentId == parentId && e.schoolId == schoolId)
                    .FirstOrDefault();
        }

        public EventShopcart GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions,
                                                   int studentId, int eventId)
        {
            return _dbContext.EventShopcart
                        .Where(e => e.parentId == parentId && e.productId == productId &&
                        e.productOptions == productOptions && e.extra3 == "SPECIAL" &&
                        e.schoolId == schoolId && e.studentId == studentId &&
                        e.eventId == eventId).FirstOrDefault();
        }

        public bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId)
        {
            EventShopcart cartItem = GetCartItem(cartId, parentId, schoolId);
            cartItem.productQuantity = cartItem.productQuantity - 1;
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveCartItem(int cartId, int parentId, int schoolId)
        {
            EventShopcart cartItem = GetCartItem(cartId, parentId, schoolId);
            _dbContext.EventShopcart.Remove(cartItem);
            _dbContext.SaveChanges();
            return true;
        }

        public bool EmptyCart(int parentId, int schoolId, int eventId)
        {
            List<EventShopcart> cart = GetCart(schoolId, parentId, eventId);
            _dbContext.EventShopcart.RemoveRange(cart);
            _dbContext.SaveChanges();
            return true;
        }

        public bool AddItemToCart(EventShopcart cartItem)
        {
            _dbContext.EventShopcart.Add(cartItem);
            _dbContext.SaveChanges();
            return true;
        }

        public bool AddItemsToCart(List<EventShopcart> cartItems)
        {
            _dbContext.EventShopcart.AddRange(cartItems);
            _dbContext.SaveChanges();
            return true;
        }

        public bool UpdateCartItems(List<EventShopcart> cartItems)
        {
            SaveDbContext(_dbContext, cartItems);
            return true;
        }

        public int GetCartItemsCount(int parentId, int schoolId, int eventId)
        {
            return _dbContext.EventShopcart.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.eventId == eventId)
                                            .Select(e => e.productQuantity.Value)
                                            .DefaultIfEmpty(0)
                                            .Sum();

        }

        public bool AddStockToProduct(int catalogId, int stockToAdd, int schoolId, int eventId)
        {

            bool saveFailed;
            var product = _dbContext.ProductsAndEvents
                                      .Where(e => e.catalogId == catalogId && e.schoolId == schoolId && e.eventId == eventId)
                                      .FirstOrDefault();
            do
            {
                saveFailed = false;

                try
                {
                    product.stock = product.stock + stockToAdd;
                    _dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                }

            } while (saveFailed);


            return true;
        }
        public bool DeductProductStock(List<EventShopcart> allCartItems, int schoolId)
        {
            foreach (EventShopcart cartItem in allCartItems)
            {
                bool saveFailed;
                CanteenMigration.Models.ProductsAndEvents product = _dbContext.ProductsAndEvents
                                          .Where(e => e.catalogId == cartItem.productId && e.eventId == cartItem.eventId)
                                          .FirstOrDefault();
                do
                {
                    saveFailed = false;
                    try
                    {
                        if (product.stock != null && product.stock.HasValue)
                        {
                            if (product.stock - cartItem.productQuantity < 0)
                            {
                                return false;
                            }
                            product.stock = product.stock - cartItem.productQuantity;
                            _dbContext.SaveChanges();

                        }
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;
                        ex.Entries.Single().Reload();
                    }
                    catch (Exception e)
                    {
                        var x = e.InnerException;
                    }

                } while (saveFailed);
            }
            return true;
        }
    }
}
