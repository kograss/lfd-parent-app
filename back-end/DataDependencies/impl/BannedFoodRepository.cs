﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class BannedFoodRepository : IBannedFoodRepository
    {
        IDBContext _dbContext;
        public BannedFoodRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public bool AddBannedProduct(List<int> studentIds, int parentId, int schoolId, int productId)
        {
            /*List<BannedFood> old = _dbContext.BannedFood.Where(e => e.parentId == parentId && e.productId == productId).ToList();
            _dbContext.BannedFood.RemoveRange(old);*/

            foreach (int studentId in studentIds)
            {
                BannedFood b = new BannedFood()
                {
                    parentId = parentId,
                    productId = productId,
                    studentId = studentId
                };

                _dbContext.BannedFood.Add(b);
            }

            _dbContext.SaveChanges();
            return true;
        }

        public bool DeleteBannedProduct(int Id)
        {
            BannedFood b = _dbContext.BannedFood.Where(e => e.Id == Id).FirstOrDefault();
            if(b != null)
            {
                _dbContext.BannedFood.Remove(b);
                _dbContext.SaveChanges();
                return true;
            }

            return false;
        }

        public List<CanteenMigration.Models.Product> GetAllProducts(int schoolId)
        {
            List<CanteenMigration.Models.Product> products = _dbContext.Products.Where(e => e.schoolId == schoolId && e.isAvailableToOrder != 0).ToList();
            return products;
        }

        public List<BannedFood> GetBannedFood(int parentId)
        {
            List<BannedFood> lst = _dbContext.BannedFood.Where(e => e.parentId == parentId).ToList();
            return lst;
        }
    }
}
