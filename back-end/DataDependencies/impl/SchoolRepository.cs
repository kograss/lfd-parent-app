﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using StackExchange.Redis;
using CanteenMigration.DataDependencies.connector.redis;
using CanteenMigration.DataDependencies.DataException;
using System.Data.SqlClient;

namespace DataDependencies.impl
{
    public class SchoolRepository : ISchoolRepository
    {
        IDBContext _dbContext;
        string SETUP_CACHE_KEY = "S{0}-SchoolDetails";

        RedisConnectorHelper _redisConnecterHelper;

        public SchoolRepository(RedisConnectorHelper redisConnecterHelper, IDBContext dbContext)
        {
            _redisConnecterHelper = redisConnecterHelper;
            _dbContext = dbContext;
        }

        public CanteenMigration.JSONModels.SchoolInfo GetSchoolInfo(int schoolId)
        {
            SqlParameter paramSchoolId = new SqlParameter("@schoolId", schoolId);
            CanteenMigration.JSONModels.SchoolInfo schoolInfo = _dbContext.GetDatabase().
                                                SqlQuery<CanteenMigration.JSONModels.SchoolInfo>
                                                ("exec dbo.SP_GetSchoolInfoFromDB @schoolId", paramSchoolId).FirstOrDefault();


            return schoolInfo;

        }

        /* public string GetSchoolInfo(int schoolId)
         {
             IDatabase cache=null;

             if (_redisConnecterHelper.Connection.IsConnected)
                 cache = _redisConnecterHelper.Connection.GetDatabase();



             if (!cache.Equals(null))
             {
                 string schoolInfo = cache.StringGet(string.Format(SETUP_CACHE_KEY, schoolId));
                 return schoolInfo;
             }
             else
             {
                 return null;
             }
         }*/

        public List<Klass> GetKlass(int schoolId)
        {

            return _dbContext.Klass.Where(e => e.schoolId == schoolId).ToList();
        }

        public bool IsMealAvailableForDate(int schoolId, DateTime deliveryDate, string mealType)
        {
            int cnt = _dbContext.LunchRecessClosures.Where(e => e.schoolId == schoolId
                    && e.closeDate.Value == deliveryDate && e.serviceToClose == mealType).Count();
            if (cnt > 0)
            {
                return false;
            }

            return true;
        }

        public string GetCanteenAdminEmail(int schoolId)
        {
            return _dbContext.SchoolSetup.Where(e => e.schoolId == schoolId).Select(e => e.managerEmail).FirstOrDefault();
        }

        public string GetCanteenOrderConfirmationEmailText(int schoolId)
        {
            return _dbContext.Schools.Where(e => e.schoolId == schoolId).Select(e => e.CanteenOrderConfirmationEmailText).FirstOrDefault();
        }
        public CanteenMigration.JSONModels.SchoolShops GetSchoolShops(int schoolId)
        {
            return (from schoolSetup in _dbContext.SchoolSetup
                    join uniformSetup in _dbContext.UniformSetup
                    on schoolSetup.schoolId equals uniformSetup.schoolId
                    where
                    schoolSetup.schoolId == schoolId
                    && uniformSetup.schoolId == schoolId
                    select new CanteenMigration.JSONModels.SchoolShops
                    {
                        canteenShopName = schoolSetup.shop,
                        uniformShopName = uniformSetup.shop
                    }
                     ).FirstOrDefault();
        }

        public SchoolSetup GetSchoolSetup(int schoolId)
        {
            SchoolSetup schoolSetup = _dbContext.SchoolSetup.Where(e => e.schoolId == schoolId).FirstOrDefault();
            return schoolSetup;
        }
    }
}
