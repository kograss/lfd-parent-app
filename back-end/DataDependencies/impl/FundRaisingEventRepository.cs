﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class FundRaisingEventRepository : IFundRaisingEventRepository
    {
        IDBContext _dbContext;
        public FundRaisingEventRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public FundRaisingEventOrders AddDonation(FundRaisingEventOrders donation)
        {
            try
            {
                var d = _dbContext.FundRaisingEventOrders.Add(donation);
                _dbContext.SaveChanges();
                return d;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public bool Update(int id, string paymentGatewayTransactionId, string stripeTransactionId, string status)
        {
            FundRaisingEventOrders p = _dbContext.FundRaisingEventOrders.Where(e => e.id == id).FirstOrDefault();
            p.paymentGatewayTransactionId = paymentGatewayTransactionId;
            p.stripeTransactionId = stripeTransactionId;
            p.status = status;
            _dbContext.SaveChanges();
            return true;
        }
    }
}
