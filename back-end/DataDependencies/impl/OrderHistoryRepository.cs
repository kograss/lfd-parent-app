﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.JSONModels;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Data.Entity;

namespace DataDependencies.impl
{
    public class OrderHistoryRepository : IOrderHistoryRepository
    {
        IDBContext _dbContext;
        public OrderHistoryRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public List<ParentOrder> GetFundRaisingOrder(int schoolId, int parentId, int year)
        {
            List<ParentOrder> orders = (from order in _dbContext.FundRaisingEventOrders
                                        join customer in _dbContext.Customers on order.studentId equals customer.id
                                        where order.schoolId == schoolId && order.parentId == parentId && order.status == "COMPLETE"
                                        select new ParentOrder
                                        {
                                            orderId = order.id,
                                            studentName = customer.studentFirstName + " " + customer.studentLastName,
                                            orderAmount = order.amount.Value,
                                            orderDate = order.orderDateTime.Value,
                                            deliveryDate = order.orderShippedDate.Value,
                                            isCancelledOrder = false,
                                            isFavouriteOrder = false,
                                            isSpecialOrder = true,
                                            eventType = "CANTEEN",
                                            isFundRaisingEvent = true
                                        }
                                        ).ToList();
            return orders;
        }
        public List<ParentOrder> GetParentHistory(int schoolId, int parentId, bool orderCancelStatus, int year)
        {
            List<ParentOrder> canteenOrders = (from parentOder in _dbContext.ParentsOrder
                                               join customer in _dbContext.Customers on parentOder.studentId equals customer.id
                                               where parentOder.schoolId == schoolId && parentOder.parentId == parentId &&
                                               parentOder.orderCancel != true && customer.schoolId == schoolId && customer.parentId == parentId
                                               && parentOder.orderShippedDate.Value.Year == year
                                               // && parentOder.orderType != "SPECIAL"
                                               select new ParentOrder
                                               {
                                                   orderId = parentOder.orderId,
                                                   studentName = customer.studentFirstName + " " + customer.studentLastName,
                                                   studentClass = customer.studentClass,
                                                   orderComment = parentOder.orderComment,
                                                   orderAmount = parentOder.orderAmount.Value,
                                                   orderDate = parentOder.orderDate.Value,
                                                   deliveryDate = parentOder.orderShippedDate.Value,
                                                   isCancelledOrder = orderCancelStatus,
                                                   isFavouriteOrder = parentOder.orderCardType == "SAVED" ? true : false,
                                                   isSpecialOrder = parentOder.orderType == "SPECIAL" ? true : false,
                                                   eventType = parentOder.orderType == "SPECIAL" ? "EVENT" : "CANTEEN"
                                               }
                         ).ToList();

            List<ParentOrder> schoolOrders = (from schoolOrder in _dbContext.SchoolOrders
                                              join customer in _dbContext.Customers on schoolOrder.CustomerId equals customer.id
                                              join schoolEvent in _dbContext.SchoolEvent2 on schoolOrder.OrderShippingMethod equals schoolEvent.schoolEventId
                                              where schoolOrder.SchoolId == schoolId && schoolOrder.ParentId == parentId &&
                                              schoolOrder.OrderCancel != true && customer.schoolId == schoolId && customer.parentId == parentId
                                              && schoolOrder.OrderShippedDate.Value.Year == year
                                              select new ParentOrder
                                              {
                                                  orderId = schoolOrder.Id,
                                                  studentName = customer.studentFirstName + " " + customer.studentLastName,
                                                  studentClass = customer.studentClass,
                                                  orderComment = schoolOrder.OrderComment,
                                                  orderDate = schoolOrder.OrderDate.Value,
                                                  orderAmount = schoolOrder.OrderAmount.Value,
                                                  deliveryDate = schoolOrder.OrderShippedDate.Value,
                                                  isCancelledOrder = orderCancelStatus,
                                                  isFavouriteOrder = schoolOrder.OrderCardType == "SAVED" ? true : false,
                                                  isSpecialOrder = true,
                                                  eventType = "SCHOOL",
                                                  eventName = schoolEvent.eventName

                                              }
                                ).ToList();

            List<ParentOrder> allOrders = canteenOrders.Concat(schoolOrders).ToList();
            return allOrders;
        }

        public ParentOrder GetSingleParentOrder(int schoolId, int orderId, int parentId)
        {
            return (from parentOder in _dbContext.ParentsOrder
                    join customer in _dbContext.Customers on parentOder.studentId equals customer.id
                    where parentOder.schoolId == schoolId && parentOder.parentId == parentId &&
                    parentOder.orderCancel != true && customer.schoolId == schoolId && customer.parentId == parentId
                    && parentOder.orderId == orderId
                    select new ParentOrder
                    {
                        orderId = parentOder.orderId,
                        studentName = customer.studentFirstName + " " + customer.studentLastName,
                        studentClass = customer.studentClass,
                        orderComment = parentOder.orderComment,
                        orderAmount = parentOder.orderAmount.Value,
                        orderDate = parentOder.orderDate.Value,
                        deliveryDate = parentOder.orderShippedDate.Value
                    }
                ).FirstOrDefault();
        }
        public ParentsOrders GetParentsOrder(int schoolId, int orderId)
        {
            return _dbContext.ParentsOrder.Where(e => e.schoolId == schoolId && e.orderId == orderId)
                                                                .FirstOrDefault();
        }

        public Orders GetOrder(int schoolId, int studentId, string orderNumber)
        {
            //Orders order = new Orders();
            return _dbContext.Orders.Where(e => e.orderNumber == orderNumber).FirstOrDefault();
            //return order;
        }

        public async Task<List<OrderItemProduct>> GetOrderItems(int schoolId, int orderId, int studentId,
                                                    string orderNumber, string mealType, string mealName)
        {
            Orders order = GetOrder(schoolId, studentId, orderNumber);


            if (order != null)
            {

                int orderIdToFetchProducts = order.id;


                //TODO: Check if order is null
                if (mealType == "LUNCH")
                {
                    var orderItems = (from orderItem in _dbContext.OrderItems
                                      join product in _dbContext.Products on orderItem.productId equals product.id
                                      where orderItem.schoolId == schoolId && orderItem.extra2 == mealType && orderItem.orderID == orderIdToFetchProducts
                                      select new OrderItemProduct
                                      {
                                          productId = product.id,
                                          studentId = studentId,
                                          productName = product.productName,
                                          productImage = product.productImageURL,
                                          price = product.productPrice,
                                          quantity = orderItem.numerOfItems,
                                          mealType = mealName,
                                          productOptions = orderItem.options,
                                          ccode = product.mealCode,
                                          cstock = product.isAvailableToOrder,
                                          mealCategory = mealType
                                      }
                                      ).ToList();
                    return orderItems;
                }

                else
                {
                    var orderItems = (from orderItem in _dbContext.OrderItemRecess
                                      join product in _dbContext.Products on orderItem.productId equals product.id
                                      where orderItem.schoolId == schoolId && orderItem.extra2 == mealType
                                            && orderItem.orderId == orderIdToFetchProducts
                                      select new OrderItemProduct
                                      {
                                          productId = product.id,
                                          studentId = studentId,
                                          productName = product.productName,
                                          productImage = product.productImageURL,
                                          price = product.productPrice,
                                          quantity = orderItem.numerOfItems,
                                          mealType = mealName,
                                          productOptions = orderItem.options,
                                          ccode = product.mealCode,
                                          cstock = product.isAvailableToOrder,
                                          mealCategory = mealType
                                      }
                                      ).ToList();
                    return orderItems;
                }
            }
            return null;
        }


        public List<OrderItemProduct> GetSpecialOrderItems(int schoolId, int orderId,
                                                           int studentId, string orderNumber)
        {
            Orders order = GetOrder(schoolId, studentId, orderNumber);
            if (order != null)
            {
                int orderIdToFetchProducts = order.id;

                var orderItems = (from orderItem in _dbContext.OrderItems
                                  join product in _dbContext.Products on orderItem.productId equals product.id
                                  where orderItem.schoolId == schoolId && orderItem.extra2 == "SPECIAL" && orderItem.orderID == orderIdToFetchProducts
                                  select new OrderItemProduct
                                  {
                                      productId = product.id,
                                      studentId = studentId,
                                      productName = product.productName,
                                      productImage = product.productImageURL,
                                      price = product.productPrice,
                                      quantity = orderItem.numerOfItems,
                                      mealType = "EVENT",
                                      productOptions = orderItem.options,
                                      ccode = product.mealCode,
                                      cstock = product.isAvailableToOrder,
                                      mealCategory = "EVENT"
                                  }
                                  ).ToList();
                return orderItems;
            }
            return null;
        }

        public List<OrderItemProduct> GetSchoolEventOrderItems(int schoolId, int orderId,
                                                           int studentId, string orderNumber)
        {
            EventOrders order = _dbContext.EventOrders.Where(e => e.OrderNumber == orderNumber).FirstOrDefault();

            if (order != null)
            {
                int orderIdToFetchProducts = order.Id;

                var orderItems = (from orderItem in _dbContext.EventOrderItems
                                  join product in _dbContext.SchoolEventProduct on orderItem.CatalogId equals product.id
                                  where orderItem.SchoolId == schoolId && orderItem.Extra2 == "SPECIAL" && orderItem.OrderId == orderIdToFetchProducts
                                  select new OrderItemProduct
                                  {
                                      productId = product.id,
                                      studentId = studentId,
                                      productName = product.productName,
                                      productImage = product.productImageURL,
                                      price = product.productPrice,
                                      quantity = (Int16)orderItem.NumItems.Value,
                                      mealType = "EVENT",
                                      productOptions = orderItem.Options,
                                      ccode = product.mealCode,
                                     // cstock = product.isAvailableToOrder,
                                      mealCategory = "EVENT"
                                  }
                          ).ToList();
                return orderItems;
            }
            return null;
        }
        public SchoolOrders GetSchoolOrder(int schoolId, int orderId)
        {
            return _dbContext.SchoolOrders.Where(e => e.SchoolId == schoolId && e.Id == orderId)
                                                                .FirstOrDefault();
        }

        public SchoolOrder GetSingleSchoolOrder(int schoolId, int orderId, int parentId)
        {
            return (from schoolOrder in _dbContext.SchoolOrders
                    join customer in _dbContext.Customers on schoolOrder.CustomerId equals customer.id
                    where schoolOrder.SchoolId == schoolId && schoolOrder.ParentId == parentId &&
                      customer.schoolId == schoolId && customer.parentId == parentId
                    && schoolOrder.Id == orderId
                    select new SchoolOrder
                    {
                        orderId = schoolOrder.Id,
                        studentName = customer.studentFirstName + " " + customer.studentLastName,
                        studentClass = customer.studentClass,
                        orderComment = schoolOrder.OrderComment,
                        orderAmount = schoolOrder.OrderAmount.Value,
                        orderDate = schoolOrder.OrderDate.Value,
                        deliveryDate = schoolOrder.OrderShippedDate.Value
                    }
                ).FirstOrDefault();
        }

        public List<CanteenNews> GetCanteenNews(int schoolId)
        {
            return _dbContext.CanteenNews.Where(entry => entry.SchoolId == schoolId && entry.Status.Equals("OPEN")).ToList();




        }
    }
}
