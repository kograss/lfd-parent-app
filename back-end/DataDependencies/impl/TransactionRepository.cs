﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class TransactionRepository : ITransactionRepository
    {
        IDBContext _dbContext;
        public TransactionRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public void AddTransaction(Transaction transaction)
        {
            _dbContext.Transaction.Add(transaction);
            _dbContext.SaveChanges();
        }

        public List<CanteenMigration.JSONModels.ParentTransaction> GetAllParentsTransactions(int parentId, int schoolId, int year)
        {
            var transactions = (from transaction in _dbContext.Transaction
                                where transaction.parentId == parentId &&
                                      transaction.schoolId == schoolId &&
                                      transaction.transactionDate.Value.Year == year
                                select new CanteenMigration.JSONModels.ParentTransaction
                                {
                                    id = transaction.id,
                                    amount = transaction.transactionAmount.Value,
                                    type = transaction.transactionType,
                                    date = transaction.transactionDate.Value,
                                    commentOrReason = transaction.comment
                                }
                                ).OrderByDescending(e => e.id).ToList();
            return transactions;
        }
        public void AddTransactions(List<Transaction> transactions)
        {
            _dbContext.Transaction.AddRange(transactions);
            _dbContext.SaveChanges();
        }
    }
}
