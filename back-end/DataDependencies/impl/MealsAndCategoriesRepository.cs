﻿using CanteenMigration.DataDependencies.connector.redis;
using StackExchange.Redis;
using System;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace DataDependencies.impl
{
    public class MealsAndCategoriesRepository : IMealsAndCategoriesRepository
    {
        string MEAL_CACHE_KEY = "S{0}";

        RedisConnectorHelper _redisConnecterHelper;
        IDBContext _dbContext;

        public MealsAndCategoriesRepository(IDBContext dbContext, RedisConnectorHelper redisConnecterHelper)
        {
            _redisConnecterHelper = redisConnecterHelper;
            this._dbContext = dbContext;
        }

        public void Dispose()
        {
            
        }


        public List<CanteenMigration.JSONModels.Category> GetCategoriesFromDB(int schoolId, string mealCode)
        {
            try
            {
                SqlParameter paramSchoolId = new SqlParameter("@schoolId", schoolId);
                SqlParameter paramMealCode = new SqlParameter("@mealCode", mealCode);
                var meals = _dbContext.GetDatabase().SqlQuery< CanteenMigration.JSONModels.Category>
                    ("exec dbo.SP_GetCanteenCategoriesFromDB @schoolId,@mealCode", paramSchoolId, paramMealCode).ToList();

                return meals;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public string GetMealsAndCategories(int schoolId, DateTime deliveryTime)
        {
            IDatabase cache=null;

            if (_redisConnecterHelper.Connection.IsConnected)
                cache = _redisConnecterHelper.Connection.GetDatabase();
           

            if (!cache.Equals(null))
            {
                string mealData = cache.StringGet(string.Format(MEAL_CACHE_KEY, schoolId));
                return mealData;
            }
            else
            {
                return null;
            }

        }

        
    }


}
