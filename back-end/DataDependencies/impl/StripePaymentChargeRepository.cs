﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;

namespace DataDependencies.impl
{
    public class StripePaymentChargeRepository : IStripePaymentChargeRepository
    {
        IDBContext _context;

        public StripePaymentChargeRepository(IDBContext context)
        {
            _context = context;
        }
        public CustomerStripeDetails GetParentStripeDetails(int schoolId, int parentId)
        {
            return _context.CustomerStripeDetails.Where(cust => cust.parentId == parentId && cust.schoolId == schoolId).FirstOrDefault();
        }
        public bool AddParentStripeDetails(int schoolId, int parentId, string customerId, string cardId, string cardLast4)
        {
            try
            {
                _context.CustomerStripeDetails.Add(new CustomerStripeDetails()
                {
                    schoolId = schoolId,
                    parentId = parentId,
                    customerId = customerId,
                    cardId = cardId,
                    cardLast4 = cardLast4,

                });
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public bool UpdateParentStripeDetails(int schoolId, int parentId, string cardId, string cardLast4)
        {
            try
            {
                var custo = _context.CustomerStripeDetails.Where(cust => cust.parentId == parentId && cust.schoolId == schoolId).FirstOrDefault();
                custo.cardId = cardId;
                custo.cardLast4 = cardLast4;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }
    }
}
