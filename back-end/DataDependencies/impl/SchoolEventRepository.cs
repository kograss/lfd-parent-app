﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DataDependencies.connector.sql_server;
using System.Data.Entity.Infrastructure;

namespace DataDependencies.impl
{
    public class SchoolEventRepository : SaveDBContext, ISchoolEventRepository
    {
        IDBContext _dbContext;
        public SchoolEventRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public int GetSchoolId(int eventId)
        {
            var e1 = _dbContext.SchoolEvent2.Where(e => e.schoolEventId == eventId).FirstOrDefault();
            if (e1 != null)
            {
                return e1.schoolId.Value;
            }
            else { return 0; }

        }
        public SchoolEvent2 GetSchoolEvent(int schoolId, int eventId)
        {
            return _dbContext.SchoolEvent2.Where(e => e.schoolId == schoolId && e.schoolEventId == eventId).FirstOrDefault();

        }

        public List<SchoolEventInfo> GetSchoolEvents(int schoolId, DateTime currentDateInSchoolTimeZone)
        {
            var schoolEvents = (from schoolEvent in _dbContext.SchoolEvent2
                                where schoolEvent.schoolId == schoolId
                                      && schoolEvent.eventDateStart.Value <= currentDateInSchoolTimeZone
                                      && schoolEvent.eventDateEnd.Value >= currentDateInSchoolTimeZone
                                //&& schoolEvent.isWeekly != true
                                select new SchoolEventInfo
                                {
                                    eventId = schoolEvent.schoolEventId,
                                    eventDate = schoolEvent.eventDate.Value,
                                    eventName = schoolEvent.eventName,
                                    orderingCloseDate = schoolEvent.eventDateEnd.Value,
                                    description = schoolEvent.description,
                                    documents = schoolEvent.documents,
                                    images = schoolEvent.images,
                                    eventType = "SCHOOL"
                                }).ToList();
            return schoolEvents;
        }

        public List<CanteenMigration.JSONModels.Product> GetAllProductEvents(int schoolId, int eventId)
        {
            var schoolEventProducts = (from product in _dbContext.SchoolEventProduct
                                       join productEvent in _dbContext.EventProductAndEvents on product.id equals productEvent.catalogId
                                       where productEvent.eventId == eventId && product.schoolId == schoolId
                                       select new CanteenMigration.JSONModels.Product
                                       {
                                           ccategory = product.productCategory != null ? product.productCategory.Value : 0,
                                           cname = product.productName,
                                           cdescription = product.productDescription,
                                           healthcategory = product.healthCategory,
                                           cprice = product.productPrice,
                                           catalogID = product.id,
                                           cimageurl = product.productImageURL,
                                           quantity_limit = product.quantityLimit,
                                           stock = product.stock,
                                           allowedClasses = product.allowedClasses
                                       }
                                        ).ToList();
            return schoolEventProducts;

        }

        public List<SchoolEventProduct> GetSchoolEventProducts(IEnumerable<int> productIds)
        {
            return _dbContext.SchoolEventProduct.Where(e => productIds.Contains(e.id)).ToList();
        }

        public List<CanteenMigration.JSONModels.Options> GetAllProductOptions(int schoolId, int productId)
        {
            List<CanteenMigration.JSONModels.Options> options = (from option in _dbContext.EventOption
                                                                 where option.optionGroupId == productId && option.schoolId == schoolId
                                                                 select new CanteenMigration.JSONModels.Options
                                                                 {
                                                                     OPT_ID = option.id,
                                                                     // Max_Items = option.maxItems,
                                                                     //Option_SubGroupId = option.optionSubGroupId,
                                                                     OPT_Name1 = option.optionName1,
                                                                     OPT_Name5 = option.optionName5,
                                                                     OPT_PriceAdd = option.optionPrice.Value,
                                                                     opt_stock = option.optionStock,
                                                                     // Required = option.isRequired
                                                                 }
                     ).ToList();
            return options;
        }

        public List<SchoolEventShopcart> GetCart(int schoolId, int parentId, int eventId)
        {
            return _dbContext.SchoolEventShopcart.Where(e => e.schoolId == schoolId && e.eventId == eventId && e.parentId == parentId).ToList();
        }

        public IQueryable<EventOption> GetOptions(int[] optionIds, int schoolId)
        {
            return this._dbContext.EventOption.Where(e => optionIds.Contains(e.id) && e.schoolId == schoolId);
        }

        public SchoolEventShopcart GetCartItem(int cartId, int parentId, int schoolId)
        {
            return _dbContext
                    .SchoolEventShopcart
                    .Where(e => e.id == cartId && e.parentId == parentId && e.schoolId == schoolId)
                    .FirstOrDefault();
        }
        public bool ReduceCartItemQuantity(int cartId, int parentId, int schoolId)
        {
            SchoolEventShopcart cartItem = GetCartItem(cartId, parentId, schoolId);
            cartItem.productQuantity = cartItem.productQuantity - 1;
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveCartItem(int cartId, int parentId, int schoolId)
        {
            SchoolEventShopcart cartItem = GetCartItem(cartId, parentId, schoolId);
            _dbContext.SchoolEventShopcart.Remove(cartItem);
            _dbContext.SaveChanges();
            return true;
        }

        public bool EmptyCart(int parentId, int schoolId, int eventId)
        {
            List<SchoolEventShopcart> cart = GetCart(schoolId, parentId, eventId);
            _dbContext.SchoolEventShopcart.RemoveRange(cart);
            _dbContext.SaveChanges();
            return true;
        }

        public decimal GetAllOptionsPrice(int schoolId, int[] intOptionIds)
        {
            decimal optionPrice = 0;
            optionPrice = (decimal)_dbContext.EventOption.Where(e => intOptionIds.Contains(e.id) && e.schoolId == schoolId).Select(e => e.optionPrice).Sum();
            return optionPrice;
        }

        public CanteenMigration.Models.SchoolEventProduct GetProduct(int schoolId, int productId)
        {
            return _dbContext.SchoolEventProduct.Where(e => e.schoolId == schoolId && e.id == productId).FirstOrDefault();
        }

        public SchoolEventShopcart GetCartItemWithOption(int schoolId, int parentId, int productId, string productOptions,
                                                  int studentId, int eventId)
        {
            return _dbContext.SchoolEventShopcart
                        .Where(e => e.parentId == parentId && e.productId == productId &&
                        e.productOptions == productOptions && e.extra3 == "SPECIAL" &&
                        e.schoolId == schoolId && e.studentId == studentId &&
                        e.eventId == eventId).FirstOrDefault();
        }

        public int GetCartItemsCount(int parentId, int schoolId, int eventId)
        {
            return _dbContext.SchoolEventShopcart.Where(e => e.schoolId == schoolId && e.parentId == parentId && e.eventId == eventId)
                                           .Sum(e => e.productQuantity.Value);
        }

        public bool AddItemsToCart(List<SchoolEventShopcart> cartItems)
        {
            _dbContext.SchoolEventShopcart.AddRange(cartItems);
            _dbContext.SaveChanges();
            return true;
        }

        public bool UpdateCartItems(List<SchoolEventShopcart> cartItems)
        {
            SaveDbContext(_dbContext, cartItems);
            return true;
        }

        public void AddSchoolOrder(SchoolOrders schoolOrder)
        {
            _dbContext.SchoolOrders.Add(schoolOrder);
            _dbContext.SaveChanges();
        }

        public bool CanceOrder(SchoolOrders parentsOrder)
        {
            parentsOrder.OrderCancel = true;
            parentsOrder.OrderCancelDate = DateTime.Now;
            _dbContext.SaveChanges();
            return true;
        }

        public void AddSchoolOrderDeleted(SchoolOrdersDeleted schoolOrder)
        {
            _dbContext.SchoolOrdersDeleted.Add(schoolOrder);
            _dbContext.SaveChanges();
        }

        public void AddEventOrders(EventOrders eventOrder)
        {
            _dbContext.EventOrders.Add(eventOrder);
            _dbContext.SaveChanges();
        }

        public bool DeleteEventOrders(List<EventOrders> orders)
        {
            _dbContext.EventOrders.RemoveRange(orders);
            _dbContext.SaveChanges();
            return true;
        }

        public EventOrders GetEventOrders(string orderNumber)
        {
            return _dbContext.EventOrders.Where(e => e.OrderNumber == orderNumber).FirstOrDefault();
        }

        public void InsertEventOrderItems(List<EventOrderItems> orderItems)
        {
            _dbContext.EventOrderItems.AddRange(orderItems);
            _dbContext.SaveChanges();
        }
        public void RemoveEventOrderItems(List<EventOrderItems> orderItems)
        {
            _dbContext.EventOrderItems.RemoveRange(orderItems);
            _dbContext.SaveChanges();
        }
        public void InsertEventOrderItemsDeleted(List<EventOrderItemsDeleted> orderItems)
        {
            _dbContext.EventOrderItemsDeleted.AddRange(orderItems);
            _dbContext.SaveChanges();
        }

        public void InsertEventTransaction(EventTransactions eventTransactions)
        {
            _dbContext.EventTransactions.Add(eventTransactions);
            _dbContext.SaveChanges();

        }

        public bool AddStockToProduct(int productId, int stockToAdd, int schoolId)
        {

            bool saveFailed;
            var product = _dbContext.SchoolEventProduct
                                      .Where(e => e.id == productId)
                                      .FirstOrDefault();
            do
            {
                saveFailed = false;

                try
                {
                    product.stock = product.stock + stockToAdd;
                    _dbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                }

            } while (saveFailed);


            return true;
        }

        public bool DeductProductStock(List<SchoolEventShopcart> allCartItems, int schoolId)
        {
            foreach (SchoolEventShopcart cartItem in allCartItems)
            {
                bool saveFailed;
                var product = _dbContext.SchoolEventProduct
                                          .Where(e => e.id == cartItem.productId)
                                          .FirstOrDefault();
                do
                {
                    saveFailed = false;
                    try
                    {
                        if (product.stock != null && product.stock.HasValue)
                        {
                            if (product.stock - cartItem.productQuantity < 0)
                            {
                                return false;
                            }
                            product.stock = product.stock - cartItem.productQuantity;
                            _dbContext.SaveChanges();

                        }
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;
                        ex.Entries.Single().Reload();
                    }
                    catch (Exception e)
                    {
                        var x = e.InnerException;
                    }

                } while (saveFailed);
            }
            return true;
        }

        public bool IsOrderExists(int orderId, int parentId, int schoolId)
        {
            var dbOrderId = (from parentsOrder in _dbContext.SchoolOrders
                             join customer in _dbContext.Customers on parentsOrder.CustomerId equals customer.id
                             where parentsOrder.Id == orderId && customer.parentId.Value == parentId && parentsOrder.SchoolId == schoolId
                             select new
                             {
                                 parentsOrder.Id
                             }
                     ).FirstOrDefault();
            return dbOrderId != null ? true : false;
        }

        public List<EventOrderItems> GetEventOrderItems(int schoolId, int orderId)
        {
            return _dbContext.EventOrderItems.Where(e => e.SchoolId == schoolId && e.OrderId == orderId).ToList();
        }
    }
}
