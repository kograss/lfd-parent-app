﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;

namespace DataDependencies
{
   public class ParentRepository: IParentRepository
    {
        IDBContext _context;

        public ParentRepository(IDBContext context)
        {
            _context = context;
        }

        public bool DeductParentBalance(int parentId, int schoolId, decimal amountToDeduct)
        {
            Parent p = _context.Parent.Where(e => e.schoolId == schoolId && e.id == parentId).FirstOrDefault();
            p.credit = p.credit - amountToDeduct;
            _context.SaveChanges();
            return true;
        }

        public Parent GetParent(int schoolId, int parentId)
        {
            return _context.Parent.Where(e => e.id == parentId && e.schoolId == schoolId).FirstOrDefault();
        }

        public Parent GetParent(int parentId)
        {
            return _context.Parent.Where(e => e.id == parentId ).FirstOrDefault();
        }

        public Parent GetParent(string email)
        {
            return _context.Parent.Where(e =>  e.email == email).FirstOrDefault();
        }

        public bool AddParentCredit(int parentId, int schoolId, decimal amountToAdd)
        {
            Parent p = _context.Parent.Where(e => e.schoolId == schoolId && e.id == parentId).FirstOrDefault();
            p.credit = p.credit +  amountToAdd;
            _context.SaveChanges();
            return true;
        }

        public decimal GetParentCredit(int parentId, int schoolId)
        {
            return _context.Parent.Where(e => e.id == parentId && e.schoolId == schoolId).Select(e => e.credit.Value).FirstOrDefault();
        }

        public bool SaveUpdatedParent()
        {
            _context.SaveChanges();
            return true;
        }

        public void AddServiceFeePaid(int parentId, int schoolId, decimal amount)
        {
            ServiceFeePaid feePaid = new ServiceFeePaid();
            feePaid.parentId = parentId;
            feePaid.schoolId = schoolId;
            feePaid.transactionAmount = amount;
            _context.ServiceFeePaid.Add(feePaid);
            _context.SaveChanges();
        }


        public bool IsClassSetupRequired(int schoolId, int parentId)
        {
            return _context.Customers.Where(
                            e => e.schoolId == schoolId
                            && e.parentId == parentId
                            && e.studentStatus == "ACTIVE"
                            && e.studentClass == "NONE").Count() > 0;

        }

        public Parent AddParent(Parent parent)
        {
            _context.Parent.Add(parent);
            _context.SaveChanges();
            return parent;
        }

        public List<Parent> GetAllParents()
        {
            return _context.Parent.Where(e => e.processed == true).ToList();
            //return _context.Parent.ToList();
            //return _context.Parent.Where(e => e.id == 47734).ToList();
        }
    }
}
