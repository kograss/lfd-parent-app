﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanteenMigration.Models;
using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;

namespace DataDependencies.impl
{
    public class OrderRepository : IOrderRepository
    {
        IDBContext _context;

        public OrderRepository(IDBContext context)
        {
            _context = context;
        }

        public void AddOrders(Orders order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
        }

        public void AddParentsOrder(ParentsOrders parentsOrders)
        {
            _context.ParentsOrder.Add(parentsOrders);
            _context.SaveChanges();
        }

        public void AddPaymentGatewayTransactionLog(PaymentGatewayTransactionLog log)
        {
            _context.TransactionLogs.Add(log);
            _context.SaveChanges();
        }

        public bool CancelOrder(int orderId, int schoolId)
        {
            ParentsOrders order = _context.ParentsOrder.Where(e => e.schoolId == schoolId && e.orderId == orderId)
                                                                .FirstOrDefault();
            order.orderCancel = true;
            _context.SaveChanges();
            //update remaining four tables
            return true;
        }

        public void InsertLunchOrderItems(List<OrderItems> orderItems)
        {
            _context.OrderItems.AddRange(orderItems);
            _context.SaveChanges();
        }

        public void InsertRecessOrderItems(List<OrderItemRecess> orderItems)
        {
            _context.OrderItemRecess.AddRange(orderItems);
            _context.SaveChanges();
        }

        public bool IsOrderExists(int orderId, int parentId, int schoolId)
        {
            var dbOrderId = (from parentsOrder in _context.ParentsOrder
                     join customer in _context.Customers on parentsOrder.studentId equals customer.id
                     where parentsOrder.orderId == orderId && customer.parentId.Value == parentId && parentsOrder.schoolId == schoolId
                     select new
                     {
                         parentsOrder.orderId
                     }
                     ).FirstOrDefault();
            return dbOrderId != null ? true : false;
        }

        public int? GetMealWiseOrderNumber(string ordersOrderId, int studentId, int schoolId)
        {
            return _context.Orders.Where(e => e.orderNumber == ordersOrderId && e.studentId == studentId && e.schoolId == schoolId)
                                  .Select(e => e.id).FirstOrDefault();
        }
        
        public Orders GetMealWiseOrders(string ordersOrderId, int schoolId)
        {
            return _context.Orders.Where(e => e.orderNumber == ordersOrderId && e.schoolId == schoolId).FirstOrDefault();
        }
        public bool CancelParentsOrder(ParentsOrders parentsOrder)
        {
            parentsOrder.orderCancel = true;
            parentsOrder.orderCancelDate = DateTime.Now;
            _context.SaveChanges();
            return true;
        }

        public ParentsOrders GetParentOrder(int orderId, int schoolId)
        {
            return _context.ParentsOrder.Where(e => e.orderId == orderId && e.schoolId == schoolId).FirstOrDefault();
        }

        public bool AddToDeletedOrder(OrdersDeleted ordersDeleted)
        {
            _context.OrdersDeleted.Add(ordersDeleted);
            _context.SaveChanges();
            return true;
        }

        public List<OrderItems> GetOrderItems(int orderId, int schoolId)
        {
            return _context.OrderItems.Where(e => e.orderID == orderId && e.schoolId == schoolId).ToList();
        }

        public List<OrderItemRecess> GetOrderItemsRecess(int orderId, int schoolId)
        {
            return _context.OrderItemRecess.Where(e => e.orderId == orderId && e.schoolId == schoolId).ToList();
        }

        public bool AddOrderItemsDeleted(List<OrderItemsDeleted> orderItems)
        {
            _context.OrderItemsDeleted.AddRange(orderItems);
            _context.SaveChanges();
            return true;
        }

        public bool AddOrderItemsRecessDeleted(List<OrderItemRecessDeleted> orderItemsRecess)
        {
            _context.OrderItemRecessDeleted.AddRange(orderItemsRecess);
            _context.SaveChanges();
            return true;
        }

        public bool DeleteOrderItem(List<OrderItems> orderitems)
        {
            _context.OrderItems.RemoveRange(orderitems);
            _context.SaveChanges();
            return true;
        }

        public bool DeleteOrderItemsRecess(List<OrderItemRecess> orderitemsrecess)
        {
            _context.OrderItemRecess.RemoveRange(orderitemsrecess);
            _context.SaveChanges();
            return true;
        }

        public bool DeleteOrders(List<Orders> orders, int schoolId)
        {
            _context.Orders.RemoveRange(orders);
            _context.SaveChanges();
            return true;             
        }

        public Orders GetOrder(int orderid, int schoolId)
        {
            Orders order = _context.Orders.Where(e => e.schoolId == schoolId && e.id == orderid).FirstOrDefault();
            return order;
        }


        public Orders GetOrderByOrderNumber(string orderid, int schoolId)
        {
            Orders order = _context.Orders.Where(e => e.schoolId == schoolId && e.orderNumber == orderid).FirstOrDefault();
            return order;
        }

        public bool SetFavourite(int orderId, int schoolId)
        {
            ParentsOrders parentsOrder = _context.ParentsOrder.Where(e => e.schoolId == schoolId && e.orderId == orderId)
                                                    .FirstOrDefault();
            if (parentsOrder == null)
            {
                return false;
            }

            parentsOrder.orderCardType = "SAVED";
            _context.SaveChanges();
            return true;
        }

        public bool RemoveFavourite(int orderId, int schoolId)
        {
            ParentsOrders parentsOrder = _context.ParentsOrder.Where(e => e.schoolId == schoolId && e.orderId == orderId)
                                                    .FirstOrDefault();
            if (parentsOrder == null)
            {
                return false;
            }

            parentsOrder.orderCardType = "";
            _context.SaveChanges();
            return true;
        }
        /*public bool isOrderExistsLinqer(int orderId, int parentId, int schoolId)
        {
            var x = from Parents_orders in
            (from Parents_orders in _context.ParentsOrder
             join Customers in _context.Customers on new
             { Ocustomerid = Convert.ToInt32(Parents_orders.studentId) }
             equals new { Ocustomerid = Customers.id }
             where
               Parents_orders.orderId == orderId &&
               Customers.parentId == parentId &&
               Parents_orders.schoolId == schoolId
             select new
             {
                 Parents_orders.orderId,
                 Dummy = "x"
             })
            group Parents_orders by new { Parents_orders.Dummy } into g
            select new
            {
                num_of_ord = g.Count(p => p.orderId != null)
            };
            if (x != null)
                return true;
            else
                return false;
        }*/
    }
}
