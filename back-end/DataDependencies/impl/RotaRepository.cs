﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using Roster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class RotaRepository : IRotaRepository
    {
        IDBContext _dbContext;
        public RotaRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public List<ParentsShift> GetParentShifts(int parentId, int schoolId)
        {
            List<ParentsShift> parentsShifts = new List<ParentsShift>();
            List<ParentsShift> p = (from volsandshifts in _dbContext.VolsAndShifts
                                    where volsandshifts.ParentId == parentId && volsandshifts.Day2Work >= DateTime.Now && volsandshifts.SchoolId == schoolId
                                    select new ParentsShift
                                    {
                                        shiftId = volsandshifts.ShiftId.Value,
                                        shiftDate = volsandshifts.Day2Work.Value,
                                        shiftName = volsandshifts.ShiftName,
                                    }
                              ).ToList();

            foreach (ParentsShift ps in p)
            {
                List<VolunteersForShift> v = (from volsandshifts in _dbContext.VolsAndShifts
                                              where volsandshifts.Day2Work == ps.shiftDate && volsandshifts.ShiftId == ps.shiftId && volsandshifts.ShiftName == ps.shiftName
                                              select new VolunteersForShift
                                              {
                                                  firstName = volsandshifts.FirstName,
                                                  lastName = volsandshifts.LastName,
                                                  parentId = volsandshifts.ParentId.Value
                                              }
                         ).ToList();
                ps.volunteers = v;
                parentsShifts.Add(ps);
            }

            return parentsShifts;
        }

        public List<CanteenMigration.JSONModels.RosterNews> GetRosterNews(int schoolId)
        {
            List<CanteenMigration.JSONModels.RosterNews> news = (from rosterNews in _dbContext.RosterNews
                                                                 where rosterNews.SchoolId == schoolId && rosterNews.Status == "OPEN"
                                                                 select new CanteenMigration.JSONModels.RosterNews
                                                                 {
                                                                     id = rosterNews.NewsId,
                                                                     comment = rosterNews.Comment,
                                                                     dateEntered = rosterNews.DateEntered.Value,
                                                                     description = rosterNews.Description,
                                                                     subject = rosterNews.Subject
                                                                 }
                                     ).ToList();
            return news;
        }

        public List<RosterHolidays> GetMonthsHoliday(int schoolId, DateTime startDate, DateTime endDate)
        {
            var hol =
              _dbContext.RosterHolidays.Where(e => ((e.holidayStartDate >= startDate && e.holidayEndDate <= endDate) ||
                                                    (e.holidayEndDate >= startDate && e.holidayEndDate <= endDate)) && e.schoolId == schoolId
                                                    ).ToList();
            return hol;
        }

        public List<RosterHolidays> GetHolidayByMonth(int schoolId, int month)
        {
            var hol =
              _dbContext.RosterHolidays.Where(e => e.schoolId == schoolId
                                                &&
                                                (e.holidayStartDate.Value.Month == month || e.holidayEndDate.Value.Month == month)).ToList();
            return hol;
        }

        public IEnumerable<Shift> GetSchoolShifts(int schoolId)
        {
            return _dbContext.Shifts.Where(e => e.schoolId == schoolId.ToString() && (e.isActive == true || e.isActive == null)).ToList();
        }

        public IEnumerable<VolsAndShift> GetShiftsWithVolunteers(int schoolId, DateTime shiftDate)
        {
            return _dbContext.VolsAndShifts.Where(e => e.Day2Work == shiftDate && e.SchoolId.Value == schoolId);
        }

        public bool RegisterVolunteerToShift(int schoolId, int shiftId, int parentId, DateTime regDate, string name, ref string _message)
        {
            Parent parent = _dbContext.Parent.Where(e => e.id == parentId).FirstOrDefault();
            VolsAndShift volShift = new VolsAndShift()
            {
                ParentId = parentId,
                SchoolId = schoolId,
                FirstName = parent.firstName,
                LastName = parent.lastName,
                Mobile = parent.mobile,
                Email = parent.email,
                ShiftId = shiftId,
                ShiftName = name,
                Day2Work = regDate,
                AllDay = false,
                Volid = parentId
            };

            int _max = GetMax(shiftId, schoolId);

            if (IsFull(shiftId, schoolId, _max, regDate))
            {
                _message = "Shift Full !";
                return false;
            }

            if (IsRegistered(shiftId, schoolId, parentId, regDate))
            {
                _message = "Already Registered !";
                return false;
            }

            _dbContext.VolsAndShifts.Add(volShift);
            _dbContext.SaveChanges();

            return true;
        }

        public int GetMax(int shiftId, int schoolId)
        {
            return _dbContext.Shifts.Where(e => e.shiftId == shiftId && e.schoolId == schoolId.ToString())
                .FirstOrDefault().shiftMax.Value;
        }

        public bool IsFull(int shiftId, int schoolId, int max, DateTime regDate)
        {
            return _dbContext.VolsAndShifts.Where(e => e.ShiftId == shiftId && e.SchoolId == schoolId && e.Day2Work == regDate)
                .Count() < max ? false : true;
        }

        public bool IsRegistered(int shiftId, int schoolId, int parentId, DateTime regDate)
        {
            return _dbContext.VolsAndShifts.Where(e => e.ShiftId == shiftId && e.SchoolId == schoolId && e.ParentId == parentId && e.Day2Work == regDate)
                .Count() >= 1 ? true : false;
        }
    }
}
