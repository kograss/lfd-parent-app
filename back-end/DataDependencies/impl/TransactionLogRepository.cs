﻿using CanteenMigration.DataDependencies.Connector.SQLServer.DataAccess;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies.impl
{
    public class TransactionLogRepository: ITransactionLogRepository
    {
        IDBContext _dbContext;
        public TransactionLogRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public PaymentGatewayTransactionLog CreateTransactionLog(int parentId, int schoolId, decimal orderAmount, 
                                                                string transactionType, string paymentGatewayName, bool isCCFeeRequired)
        {
            /*Decimal stripeFee = 2.1M;
            Decimal BankFee = (orderAmount * stripeFee) / 100;
            Decimal School24Fee = 0;
            Decimal GrossAmount = orderAmount + BankFee;
            */
            Decimal stripeFee = 1.5M;
            Decimal BankFee = isCCFeeRequired ? (orderAmount * stripeFee) / 100 + 0.30M : 0;
            Decimal School24Fee = 0;
            Decimal GrossAmount = orderAmount + BankFee;

            PaymentGatewayTransactionLog log = new PaymentGatewayTransactionLog();
            log.OrderAmount = orderAmount;

            log.Id = PaymentGatewayTransactionLog.GenerateTransactionId(transactionType, paymentGatewayName);
            log.Status = PaymentGatewayTransactionLog.TRANSACTION_STATE_START;
            log.Log = "[Transaction Start]";
            log.TransactionDate = DateTime.Now;
            log.ParentId = parentId;
            log.SchoolId = schoolId;
            log.Type = transactionType;
            log.BankFee = BankFee;
            log.School24Fee = School24Fee;
            log.GrossAmount = GrossAmount;
            _dbContext.TransactionLogs.Add(log);
            _dbContext.SaveChanges();
            
            return log;
        }

        public PaymentGatewayTransactionLog CreateTransactionLogForEvent(int parentId, int schoolId, decimal orderAmount,
                                                        string transactionType, string paymentGatewayName, bool isCCFeeRequired)
        {
            Decimal stripeFee = 1.5M;
            Decimal School24Fee = 0.25M;

            Decimal GrossAmount = orderAmount + School24Fee;
            Decimal BankFee = ((GrossAmount * stripeFee) / 100 )+ 0.30M;
                        
            PaymentGatewayTransactionLog log = new PaymentGatewayTransactionLog();
            log.OrderAmount = orderAmount;

            log.Id = PaymentGatewayTransactionLog.GenerateTransactionId(transactionType, paymentGatewayName);
            log.Status = PaymentGatewayTransactionLog.TRANSACTION_STATE_START;
            log.Log = "[Transaction Start]";
            log.TransactionDate = DateTime.Now;
            log.ParentId = parentId;
            log.SchoolId = schoolId;
            log.Type = transactionType;
            log.BankFee = BankFee;
            log.School24Fee = School24Fee;
            log.GrossAmount = GrossAmount;
            _dbContext.TransactionLogs.Add(log);
            _dbContext.SaveChanges();

            return log;
        }
        public bool UpdateTransactionLog(string status, PaymentGatewayTransactionLog paymentLog, int schoolId, string paymentIdentifier)
        {
            //PaymentGatewayTransactionLog paymentLog = _dbContext.TransactionLogs.Where(e => e.Id == transactionId && e.SchoolId == schoolId).FirstOrDefault();
            paymentLog.PGTransId = paymentIdentifier;
            paymentLog.Status = PaymentGatewayTransactionLog.TRANSACTION_STATE_SUCCESS;
            paymentLog.Log += status;
            _dbContext.SaveChanges();
            return true;
        }

        public bool AddTransactionLog(PaymentGatewayTransactionLog log)
        {
            _dbContext.TransactionLogs.Add(log);
            _dbContext.SaveChanges();
            return true;
        }
    }
}
