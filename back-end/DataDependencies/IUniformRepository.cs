﻿using CanteenMigration.JSONModels;
using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDependencies
{
    public interface IUniformRepository
    {
        List<UniformCategories> GetUniformCategories(int schoolId);
        List<CanteenMigration.JSONModels.UniformNews> GetUniformNews(int schoolId, int year);
        List<CanteenMigration.JSONModels.UniformProduct> GetUniformProductsByCategory(int schoolId, int categoryId);
        List<CanteenMigration.JSONModels.UniformPack> GetUniformPacksByCategory(int schoolId, int categoryId);
        List<CanteenMigration.JSONModels.UniformPackProduct> GetUniformPackProducts(int schoolId, int uniformId);
        string GetCategories(int schoolId);
        string GetCategoryProducts(int schoolId, int categoryId);
        string GetProductOptions(int schoolId, int productId);
        List<UniformBasketItem> GetBasketItems(int basketId, int parentId, int schoolId);
        UniformBasket GetBasket(int basketId, int parentId, int schoolId);
        
        UniformBasketItem AddToBasket(int parentId, int schoolId, UniformBasketItem item);
        bool AddUniformPackItemProducts(List<UniformPackItemProduct> products, int itemId);
        int GetBasketCount(int basketId, int parentId, int schoolId);
        bool UpdateCartQuantity(UniformBasketItem item, int quantityToAdd);
        UniformBasketItem GetItemFromBasket(UniformBasketItem item);
        UniformPackItemProduct GetUPackSubItemFromBasket(UniformPackItemProduct item);
        List<UniformPackItemProduct> GetAllUPackSubItem(int id);
        bool EmptyCart(List<UniformBasketItem> items);
        bool ReduceBasketItemQuantity(UniformBasketItem item, int quantityToReduce);
        bool RemoveItemFromBasket(UniformBasketItem item);
        UniformBasketItem GetBasketItem(int parentId, int schoolId, int basketItemId);
        int? GetProductStock(StockInput input);
        int CreateBasket(int parentId, int schoolId);
        bool UpdateBasket(UniformBasket basket, string studentId, string studentClass, string comment);
        UniformBasket GetParentBasket(int basketId, int parentId, int schoolId);
        bool AddUniformOrderData(UniformOrderData uniformOrderData);
        UniformOrderData GetUniformOrderData(UniformOrderData uniformOrderData);
        bool AddUniformOrderStatus(UniformOrderStatus uniformOrderStatus);
        bool AddUniformOrderPayment(UniformOrderPayment uniformOrderPayment);
        UniformOrderPayment GetUniformOrderPayment(PaymentGatewayTransactionLog log);
        List<UniformOrder> GetParentOrders(int parentId, int schoolId, int year);
        List<OrderDetails> GetUniformOrderDetails(int schoolId, int parentId, int orderId);
        UniformOrderStatus GetOrderStatusByOrderId(int orderId);
        CanteenMigration.Models.UniformProduct GetUniformProduct(int productId);
        CanteenMigration.Models.UniformPack GetUniformPack(int productId);
        UniformAttribute GetUniformAttributeByName(string color);
        UniformStocks GetUniformStock(int schoolId, int value, int? id1, int? id2);
        void SaveContext();
        int? GetProductStock(int schoolId, int productId);

       List<ProductOption> GetProductOptionsFromDB(int schoolId, int productId);
        CanteenMigration.Models.UniformAdmin GetUniformAdmin(int schoolId);
        CanteenMigration.Models.UniformSetup GetUniformSetup(int schoolId);
        bool AddUniformAddress(UniformAddress uniformAddress);
        UniformAddress GetUniformAddress(int parentId);


    }
}
