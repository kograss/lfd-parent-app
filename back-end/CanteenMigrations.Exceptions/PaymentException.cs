﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.exception
{
    public class PaymentException : Exception
    {
        public string ErrorMessage { get; set; }
        public PaymentException(string Message)
        {
            this.ErrorMessage = Message;
        }

    }
}
