﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Services.exception
{
    public class PlaceOrderException :Exception
    {
        public string ErrorMessage { get; set; }
        public PlaceOrderException(string Message)
        {
            this.ErrorMessage = Message;
        }

    }
}
