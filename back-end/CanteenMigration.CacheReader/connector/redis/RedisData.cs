
namespace CanteenCacheReader.connector.redis
{
    public class RedisData
    {
        string CATEGORY_CACHE_KEY = "S{0}";
        string PRODUCT_CACHE_KEY = "S{0}-{1}-C{2}";//S2-L-C278

        public string ReadCategoriesBySchoolId(int schoolId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string categoriesData = cache.StringGet(string.Format(CATEGORY_CACHE_KEY, schoolId));
            return categoriesData;
        }

        public string ReadProductsBySchoolIdMealAndCategoryId(int schoolId, string mealName, int categoryId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string productsData = cache.StringGet(string.Format(PRODUCT_CACHE_KEY, schoolId, mealName, categoryId));
            return productsData;
        }
    }
}