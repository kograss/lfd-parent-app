﻿using CanteenCacheReader.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenCacheReader
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Diagnostics.Debug.WriteLine("Cache Reader");
            //new CanteenCacheReader.api.CategoryCacheReader().GetMealsAndCategories(2, new DateTime(2017, 8, 15));
            //Console.WriteLine(new CanteenNewsCacheReader().GetCanteenNews(2));
            //Console.WriteLine(new SchoolSetupCacheReader().GetCanteenSchoolSetup(2));
            Console.WriteLine(new OptionCacheReader().GetProductOptions(4, "L", 48));
            Console.ReadLine();
        }
    }
}
