﻿using CanteenCacheReader.connector.redis;

namespace CanteenCacheReader.api
{
    public class ProductCacheReader
    {
        string PRODUCT_CACHE_KEY = "S{0}-{1}-C{2}";

        public string GetCanteenProducts(int schoolId, string mealName, int categoryId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string productsData = cache.StringGet(string.Format(PRODUCT_CACHE_KEY, schoolId, mealName, categoryId));
            return productsData;
        }
    }
}
