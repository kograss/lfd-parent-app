﻿using CanteenCacheReader.connector.redis;

namespace CanteenCacheReader.api
{
    public class OptionCacheReader
    {
        string OPTION_CACHE_KEY = "S{0}-{1}-P{2}";

        public string GetProductOptions(int schoolId, string mealName, int productId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string optionsData = cache.StringGet(string.Format(OPTION_CACHE_KEY, schoolId, mealName, productId));
            return optionsData;
        }
    }
}
