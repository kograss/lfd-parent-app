﻿using CanteenCacheReader.connector.redis;

namespace CanteenCacheReader.api
{
    public class SchoolSetupCacheReader
    {
        string SETUP_CACHE_KEY = "S{0}-SchoolDetails";

        public string GetCanteenSchoolSetup(int schoolId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string schoolSetupData = cache.StringGet(string.Format(SETUP_CACHE_KEY, schoolId));
            //JArray json = JArray.Parse(newsData);
            //System.Diagnostics.Debug.WriteLine(schoolSetupData);
            return schoolSetupData;
        }
    }
}
