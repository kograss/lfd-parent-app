using CanteenCacheReader.connector.redis;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace CanteenCacheReader.api
{
    public class CategoryCacheReader
    {
        string CATEGORY_CACHE_KEY = "S{0}";

        public string GetMealsAndCategories(int schoolId, DateTime orderDate)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            double orderDateMsOrig = orderDate.Ticks;


            string categoriesData = cache.StringGet(string.Format(CATEGORY_CACHE_KEY, schoolId));
            if (categoriesData != null)
            {

                JArray jArray = JArray.Parse(categoriesData);
                //List<JArray> newCategoryArrays = new List<JArray>();
                foreach (JObject meal in jArray)
                {
                    
                    //System.Diagnostics.Debug.WriteLine(item);
                    System.Diagnostics.Debug.WriteLine(meal.Property("categories").Value.ToString());
                    JArray categories = JArray.Parse(meal.Property("categories").Value.ToString());

                    meal.Remove("categories");
                    meal.Add("categories", categories);
                   


                    List<JObject> catToBeRemoved = new List<JObject>();
                    


                    foreach (JObject category in categories)
                    {

                        JProperty pCutOffDays = category.Property("cut_days_before");
                        if (!String.IsNullOrEmpty(pCutOffDays.Value.ToString()))
                        {
                            double orderDateMsCpy = orderDateMsOrig;
                            int cutOffDays = Int16.Parse(pCutOffDays.Value.ToString());

                            JProperty pDailyHour = category.Property("daily_hour");
                            if (!String.IsNullOrEmpty(pDailyHour.Value.ToString()))
                            {
                                Double dailyHourMs = Double.Parse(pDailyHour.Value.ToString())*36000000000;
                                orderDateMsCpy = orderDateMsCpy + dailyHourMs;                      
                            }
                           
                            JProperty pDailyMinutes = category.Property("daily_min");
                            if (!String.IsNullOrEmpty(pDailyMinutes.Value.ToString()))
                            {
                                Double dailyMinutesMs = Double.Parse(pDailyMinutes.Value.ToString()) * 600000000;
                                orderDateMsCpy = orderDateMsCpy + dailyMinutesMs;

                            }
                            double d1 =(DateTime.Now.Ticks) +(cutOffDays * 864000000000);
                            if (d1 > orderDateMsCpy)
                                catToBeRemoved.Add(category);
                            orderDateMsCpy = 0;

                        }

                        System.Diagnostics.Debug.WriteLine(category.Property("catdescription").Value.ToString());
                       
                    }

                    catToBeRemoved.ForEach(category => categories.Remove(category));
                   // newCategoryArrays.Add(jArray1);




                    //jArray.Remove(item);
                    // ...
                }
                //jArray.RemoveAll();
                //newCategoryArrays.ForEach(category => jArray.Add(category));



                return jArray.ToString();
            }
            else
            {
                throw new System.Exception("Data not found");
            }
        }

        
        
    }
}

/*public IHttpActionResult Get(int schoolId, string orderDate)
{
    var cache = RedisConnectorHelper.Connection.GetDatabase();
    string categoriesData = cache.StringGet(string.Format(CATEGORY_CACHE_KEY, schoolId));
    if (categoriesData == null)
    {
        //return NotFound();
        throw new HttpResponseException(HttpStatusCode.NotFound);
    }

    String[] inputDate = orderDate.Split(new char[] { '/' });
    DateTime deliveryDate = new DateTime(Int32.Parse(inputDate[2]), Int32.Parse(inputDate[1]), Int32.Parse(inputDate[0]));
    DateTime currentDateTime = DateTime.Now;
    int noOfDays = (int)(deliveryDate - currentDateTime).TotalDays;

    JObject json = JObject.Parse(categoriesData);
    foreach (var x in json)
    {
        string key = x.Key;
        List<CategoryModel> value = x.Value.ToObject<List<CategoryModel>>();

        foreach (CategoryModel c in value)
        {
            DateTime addedDateTime = currentDateTime.AddDays(c.cutDaysBefore);
            if (deliveryDate < addedDateTime)
            {
                //category is available
            }

            else if (c.dailyHours > currentDateTime.Hour)
            {
                //category is available
            }

            else
            {
                //category is not available
            }

        }

        //string timeString24Hour = localTime.ToString("HH", CultureInfo.CurrentCulture);
        //string timeString24Hours_ = localTime.ToString("mm", CultureInfo.CurrentCulture);
        value.Where(e => e.cutDaysBefore < noOfDays);
        int cp = 10;
    }




    JavaScriptSerializer json_serializer = new JavaScriptSerializer();

    //List<Dictionary<string, List<CategoryModel>>> dct = (List<Dictionary <string, List< CategoryModel >>>) json_serializer.DeserializeObject(categoriesData);


    //List<Meal> meals = JsonHelper.JsonDeserialize(categoriesData);
    //var recess = json[0];
    //var lst = json.ToObject< List<Dictionary<string, List<CategoryModel>>>>();

    var meals = json_serializer.DeserializeObject(categoriesData);
    //var lunchCategories = meals[0];
    //json.Where()
    //JObject c =  (JObject) json["_data"];

    /* 
     1. Parse category
     2. Find a model from JSON
     3. run LINQ to find single category
     */

   /* return Ok(new { Categories = json });
}
    }*/