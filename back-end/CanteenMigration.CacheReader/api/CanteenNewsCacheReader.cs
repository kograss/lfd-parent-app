﻿using CanteenCacheReader.connector.redis;

namespace CanteenCacheReader.api
{
    public class CanteenNewsCacheReader
    {
        string NEWS_CACHE_KEY = "S{0}-CanteenNews";

        public CanteenNewsCacheReader()
        {

        }

        public string GetCanteenNews(int schoolId)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            string newsData = cache.StringGet(string.Format(NEWS_CACHE_KEY, schoolId));
            //JArray json = JArray.Parse(newsData);
            System.Diagnostics.Debug.WriteLine(newsData);
            return newsData;
        }
    }
}
