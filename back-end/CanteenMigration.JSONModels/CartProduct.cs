﻿
using CanteenMigration.Models;
using System.Collections.Generic;

namespace CanteenMigration.JSONModels
{
    public class CartProduct
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public List<CanteenShopcartItem> canteenShopcarts { get; set; }
    }
}
