﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class StripePaymentRequest
    {
        public string stripeToken { get; set; }
        public int amount { get; set; }
        public List<Order> orderItems { get; set; }
        public bool isBalancePlusCard { get; set; }
        public bool saveCard { get; set; }
        
    }

    public class StripeDonationRequest
    {
        public string stripeToken { get; set; }
        public int amount { get; set; }
        public List<StudentAndAmount> students { get; set; }
        public int eventId { get; set; }
        public string eventName { get; set; }

        public class StudentAndAmount
        {
            public int studentId { get; set; }
            public int amount { get; set; }
        }
    }


    public class PaymentResult
    {
        public bool paymentStatus { get; set; }
        public string message { get; set; }
        public string paymentGatewayTransactionId { get; set; }
    }
    public class Order
    {
        public int studentId { get; set; }
        public int parentId { get; set; }
        public int schoolId { get; set; }
        public DateTime deliveryDate { get; set; }
        public decimal totalServiceCharge { get; set; }
        public decimal chargeForBag { get; set; }
        public string comment { get; set; }
        public string reason { get; set; }
        public string bagType { get; set; }
        public decimal? bagPrice { get; set; }
        public bool? isPayAtPickup { get; set; }
        public bool? isCreditPayment { get; set; }

    }
    public class OrderResult
    {
        public List<int> orderIds { get; set; }
        public string message { get; set; }
        public bool? isOrderSuccess { get; set; }
        public bool? isProductsNotAvailable { get; set; }
        public List<CanteenMigration.JSONModels.AvailableProduct> notAvailableProducts { get; set; }
    }
}
