﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class Product
    {
        public int ccategory { get; set; }
        public int catalogID { get; set; }
        public string ccode { get; set; }
        public decimal? cprice { get; set; }
        public string cimageurl { get; set; }
        public string cdateavailable { get; set; }
        public int? healthcategory { get; set; }
        public string food_label { get; set; }
        public int? quantity_limit { get; set; }
        public string cdescription { get; set; }
        public string cname { get; set; }
        public List<Int32> studentIds { get; set; }
        public string productClassification { get; set; }
        public List<OptionSubGroup> OptionSubGroup { get; set; }
        public string notAvailableDays { get; set; }
        public int? stock { get; set; }
        public string allowedClasses { get; set; }
    }

    public class OptionSubGroup
    {
        public string OptionSubGroupName { get; set; }
        public int? Max_Items { get; set; }
        public bool? Required { get; set; }
        public List<Options> options { get; set; }
    }

    public class Options
    {
        public int OPT_ID { get; set; }
        public string OPT_Name1 { get; set; }
        public string OPT_Name5 { get; set; }
        public double? OPT_PriceAdd { get; set; }
        public int? opt_stock { get; set; }
        public string Option_SubGroupId { get; set; }
        public int? Max_Items { get; set; }
        public bool? Required { get; set; }
        public int schoolId { get; set; }
    }

    public class ProductStock
    {
        public int productId { get; set; }
        public int? stock { get; set; }
    }
}
