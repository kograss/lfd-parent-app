﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class Student
    {
        public int studentId { get; set; }
        public int? parentId { get; set; }
        public int? schoolId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string status { get; set; }
        public string studentClass { get; set; }
        public string allergy { get; set; }
        public string studentCardId { get; set; }
        public decimal? maxAllow { get; set; }
    }
    public class StudentWithKlass
    {
        public int studentId { get; set; }
        public int parentId { get; set; }
        public int schoolId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string studentClass { get; set; }
        public string klass { get; set; }
        public string classForClassification { get; set; }
        public string status { get; set; }
        public string allergy { get; set; }
        public string studentCardId { get; set; }
        public decimal? maxAllow { get; set; }
    }

    public class KlassModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class GetStudentAvailabilityMessage
    {
        public bool disableOrders { get; set; }
        public string message { get; set; }
    }

}
