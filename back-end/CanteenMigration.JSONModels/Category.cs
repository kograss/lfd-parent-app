﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class Category
    {
        public int categoryID { set; get; }
        public string catdescription { set; get; }
        public int? daily_hour { set; get; }
        public int? daily_min { set; get; }
        public int? cut_days_before { set; get; }
        public bool? isHide { get; set; }

        public int? hd { get; set; }
        public int? hh { get; set; }
        public int? hm { get; set; }
        public int? sd { get; set; }
        public int? sh { get; set; }
        public int? sm { get; set; }
    }
}
