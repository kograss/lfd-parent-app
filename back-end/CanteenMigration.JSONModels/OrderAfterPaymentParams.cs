﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class OrderAfterPaymentParams
    {
        public StripePaymentRequest paymentRequest { get; set; }
        public PaymentGatewayTransactionLog log { get; set; }
        public StripeUniformPaymentRequest uniformPaymentRequest { get; set; }
    }
}
