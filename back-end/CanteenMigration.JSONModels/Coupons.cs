﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class CouponsForParent
    {
        public int id { get; set; }
        public string couponName { get; set; }
        public Decimal? couponPrice { get; set; }
        public DateTime? expiryDate { get; set; }
        public bool? status { get; set; }

        public bool? isUsed { get; set; }
        public DateTime? redeemDate { get; set; }
    }

    public class RedeemResult
    {
        public bool isSuccess { get; set; }
        public string message { get; set; }
    }

    public class RedeemCouponInput
    {
        public int couponId { get; set; }
    }
}
