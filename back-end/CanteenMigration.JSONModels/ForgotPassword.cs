﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class SendForgotPasswordLinkResult
    {
        public bool isLinkSent { get; set; }
        public string emailId { get; set; }
        public string message { get; set; }

    }

    public class ChangePasswordResult
    {
        public bool isPasswordChanged { get; set; }
        public string message { get; set; }
    }
}
