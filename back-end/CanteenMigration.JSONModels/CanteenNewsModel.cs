﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CanteenMigration.JSONModels
{
    public class CanteenNewsModel
    {
        public int NewsID { get; set; }
        public string Description { get; set; }
        public DateTime DateEntered { get; set; }
        public string Subject { get; set; }
        public int Priority { get; set; }
        public string EnteredBy { get; set; }
        public string dayid { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public int schoolid { get; set; }
    }
}
