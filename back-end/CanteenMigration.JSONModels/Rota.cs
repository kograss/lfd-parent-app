﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class ParentsShift
    {
        public int shiftId { get; set; }
        public string shiftName { get; set; }
        public DateTime shiftDate { get; set; }
        public List<VolunteersForShift> volunteers { get; set; }
    }

    public class VolunteersForShift
    {
        public int parentId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }

    public class RosterNews
    {
        public int id { get; set; }
        public string description { get; set; }
        public string subject { get; set; }
        public DateTime dateEntered { get; set; }
        public string comment { get; set; }
    }

    public class ShiftsByDate
    {
        public DateTime date { get; set; }
        public bool isHoliday { get; set; }
        public bool isNonWorkingday { get; set; }
        public List<ShiftInfo> shifts { get; set; }
    }

    public class ShiftInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public int shiftMax { get; set; }
        public Int32 leader { get; set; }
        public int noOfVolReq { get; set; }
        public DateTime date { get; set; }
        public List<Volunteer> volunteers { get; set; }
        public bool isPastDay { get; set; }
    }

    public class Volunteer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }

    }

    public class RegisterVolunteerInput
    {
        public int schoolId { get; set; }
        public int shiftId { get; set; }
        public int parentId { get; set; }
        public DateTime date { get; set; }
        public string shiftName { get; set; }
    }
}
