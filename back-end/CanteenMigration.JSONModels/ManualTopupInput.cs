﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class ManualTopupInput
    {
        //ParentID, schoolid, Amount, method_pay, date_you_paid, approved, admin_action, comment, date_submit

        public int parentId { get; set; }
        public int schoolId { get; set; }
        public decimal amount { get; set; }
        public string methodPay { get; set; }
        public string dateYouPaid { get; set; }
        public string approved { get; set; }
        public string adminAction { get; set; }
        public string comment { get; set; }
        public DateTime? dateSubmit { get; set; }

    }
}
