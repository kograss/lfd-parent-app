﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class TopupResult
    {
        public bool isSucess { get; set; }
        public string message { get; set;}
        public decimal parentCredit { get; set; }
    }
}
