﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class HolidayClousures
    {
        public DateTime maxDate { get; set; }
        public int[] weeklyClosures { get; set; }
        public List<HolidayRange> holidayDateRange { get; set; }
        public List<DateTime> cutOffDays { get; set; }
    }

    public class HolidayRange
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

    }
}
