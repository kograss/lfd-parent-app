﻿using CanteenMigration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class SchoolEventInfo
    {
        public int eventId { get; set; }
        public string eventName { get; set; }
        public DateTime eventDate { get; set; }
        public DateTime orderingCloseDate { get; set; }
        public bool? isBagAvailable { get; set; }
        public string bagName { get; set; }
        public Decimal? bagPrice { get; set; }
        public int? IsSetPrice { get; set; }
        public Decimal? eventPrice { get; set; }
        public int? whoPays { get; set; }

        public string description { get; set; }

        public string images { get; set; }

        public string documents { get; set; }

        public int? event_type { get; set; } //
        public string eventType { get; set; }//CANTEEN or SCHOOL
        public bool? isFundRaisingEvent { get; set; }
    }

    public class SchoolEventCategory
    {
        public int eventId { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
    }

    public class EventProduct
    {
        public int ccategory { get; set; }
        public int catalogID { get; set; }
        public string ccode { get; set; }
        public decimal? cprice { get; set; }
        public string cimageurl { get; set; }
        public string cdateavailable { get; set; }
        public int? healthcategory { get; set; }
        public string food_label { get; set; }
        public int? quantity_limit { get; set; }
        public string cdescription { get; set; }
        public string cname { get; set; }
        public List<Int32> studentIds { get; set; }
        public string productClassification { get; set; }
        public List<OptionSubGroup> OptionSubGroup { get; set; }
        public string notAvailableDays { get; set; }
        public int? stock { get; set; }
    }

    public class EventCartProduct
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public List<EventShopcart> canteenShopcarts { get; set; }
    }

    public class SchoolEventCartProduct
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public List<SchoolEventShopcart> canteenShopcarts { get; set; }
    }
    public class EventCartUpdate
    {
        public int schoolId { get; set; }
        public int parentId { get; set; }
        public int cartId { get; set; } // will be null when empty cart 
        public int eventId { get; set; }
        public bool deleteProduct { get; set; } //specifies if need to remove the entry from c_shopcart -> quantity is zero and want to remove product
        public RemoveCartStatus status { get; set; }
    }

    public class EventAddToCart
    {
        public int parentId { get; set; }
        public int schoolId { get; set; }
        public int categoryId { get; set; }
        public int productId { get; set; }
        public int productName { get; set; }
        public int[] studentIds { get; set; }
        public int quantity { get; set; }
        public string optionIds { get; set; }
        public int eventId { get; set; }
        public int eventType { get; set; }
    }
    public class EventOrder
    {
        public int studentId { get; set; }
        public int parentId { get; set; }
        public int schoolId { get; set; }
        public DateTime? deliveryDate { get; set; }
        public int eventId { get; set; }
        public decimal totalServiceCharge { get; set; }
        public decimal chargeForBag { get; set; }
        public string comment { get; set; }
        public string reason { get; set; }

    }

    public class StripeEventPaymentRequest
    {
        public string stripeToken { get; set; }
        public int amount { get; set; }
        public List<EventOrder> orderItems { get; set; }
        public bool isBalancePlusCard { get; set; }
        public bool saveCard { get; set; }
    }

    public class EventOrderAfterPaymentParams
    {
        public StripeEventPaymentRequest paymentRequest { get; set; }
        public PaymentGatewayTransactionLog log { get; set; }
    }
}

