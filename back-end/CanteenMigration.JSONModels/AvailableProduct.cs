﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class AvailableProduct
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public string mealCode { get; set; }
        public string notAvailableDays { get; set; }
    }
}
