﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class SchoolInfo
    {
        public int schoolid { get; set; }
        public string shop { get; set; }
        public string chargeforbag { get; set; }
        public decimal? bagprice { get; set; }
        public string chargeforbaglunch { get; set; }
        public decimal? bagpricelunch { get; set; }
        public bool? allow_recycle { get; set; }
        public decimal? recycleprice { get; set; }
        public bool? canteen_status { get; set; }
        public int s_fee_plan { get; set; }
        public int fee_model { get; set; }
        public string bag_policy { get; set; }
        public string bag_policy_lunch { get; set; }
        public decimal? quartly_service_fee { get; set; }
        public decimal? order_service_fee { get; set; }
        public bool? canteenopen { get; set; }
        public bool? allowcomments { get; set; }
        public bool? allowprocearly { get; set; }
        public int? daily_hour { get; set; }
        public int? daily_min { get; set; }
        public int? cut_days_before { get; set; }
        public string timeZone { get; set; }
        public int? open_howmany { get; set; }
        public bool? isRecessOpen { get; set; }
        public string meal1 { get; set; }
        public string meal2 { get; set; }
        public string meal3 { get; set; }
        public bool? roster_status { get; set; }
        public bool? uniform_status { get; set; } 
        public bool? isPayAtPickUpAvailable { get; set; }
        public string cbank { get; set; }
        public string cbankaccountname { get; set; }
        public string cbankaccount { get; set; }
        public string cbankbsb { get; set; }
        public string paymentmethod1 { get; set; }
        public string topup_method { get; set; }
        public string cbankswiftcode { get; set; }

        public string cmgremail { get; set; }

        public bool? processed { get; set; }

        public bool? uniform_open { get; set; }

        public bool? isSplitMenuActivated { get; set; }
        public bool? transfer_weekly_sales_only { get; set; }
        public decimal? canteenOrderServiceFee { get; set; }
    }
    public class SchoolShops
    {
        public string canteenShopName { get; set; }
        public string uniformShopName { get; set; }
    }
}
