﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class ParentInfo
    {
        public int parentId { get; set; }
        public int? schoolId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public decimal credit { get; set; }
        public int parentPlan { get; set; }
        public string password { get; set; }
        public bool isStudentClassSetupRequired { get; set; }
    }

    public class RegisterParentInput
    {
        public int schoolId { get; set; }
        public int registerAs { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobileNumber { get; set; }
        public string password { get; set; }

    }

    public class VerifyParentEmailInput
    {
        public string data;
    }
}
