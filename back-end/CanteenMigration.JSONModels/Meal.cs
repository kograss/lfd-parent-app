﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class Meal
    {
        public string name { set; get; }
        public string mealCode { set; get; }
        public List<Category> categories { set; get; }
    }
}
