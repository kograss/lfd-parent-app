﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class UniformCategories
    {
        public int categoryId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public int schoolId { get; set; }

    }

    public class UniformNews
    {
        public int newsID { get; set; }
        public string description { get; set; }
        public DateTime? dateEntered { get; set; }
        public string subject { get; set; }
    }


    public class UniformProduct
    {
        public int productId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public int? stock { get; set; }
        public decimal? price { get; set; }
        //options
        public List<ProductOption> options { get; set; }

        public string TopupMethod { get; set; }

        public string Bank { get; set; }

        public string BankAccount { get; set; }

        public string BankBSB { get; set; }

        public string Shop { get; set; }

        public string AccountEmail { get; set; }

        public bool? isStudentOptional { get; set; }
        public bool? isExternalRecipientRequired { get; set; }
        public String UniformPackIds { get; set; }
        public bool isUniformPack { get; set; }

    }

    public class UniformPack : UniformProduct
    {
        public int idForCategory { get; set; }
        public List<UniformPackProduct> products { get; set; }

    }

    public class UniformPackProduct
    {
        public int? id { get; set; }
        public string name { get; set; }
        public int productId { get; set; }
        public int? quantity { get; set; }
        public int? uniformPackId { get; set; }
        public List<ProductOption> options { get; set; }
        public int? stock { get; set; }
    }

    public class UniformCartItem
    {
        public Models.UniformBasketItem uniformItem { get; set; }
        public List<Models.UniformPackItemProduct> uniformPackItemDetails { get; set; }

    }

    public class CollectionOption
    {
        public int id { get; set; }
        public string type { get; set; }
        public decimal fee { get; set; }
    }

    public class UniformCheckoutSchoolDetails
    {
        public string TopupMethod { get; set; }
        public string Bank { get; set; }
        public string BankAccount { get; set; }
        public string BankBSB { get; set; }
        public string Shop { get; set; }
        public string AccountEmail { get; set; }
        public bool? isStudentOptional { get; set; }
        public bool? isExternalRecipientRequired { get; set; }
        public bool? isPayUsingBalanceEnabled { get; set; }

    }

    public class ProductOption
    {
        public int optionId { get; set; }
        public int productId { get; set; }
        public string optionName { get; set; }
        public string categoryName { get; set; }
    }

    public class UpdateCartInput
    {
        public int basketId { get; set; }
        public int basketItemId { get; set; } // will be null when empty cart 
        public bool deleteProduct { get; set; } //specifies if need to remove the entry from uniform_basketitem -> quantity is zero and want to remove product
        public RemoveCartStatus status { get; set; }
    }
    public class StockInput
    {
        public int? sizeAttributeId { get; set; }

        public int? colorAttributeId { get; set; }

        public int productId { get; set; }

        public int? schoolId { get; set; }
    }

    public class StripeUniformPaymentRequest
    {
        public string stripeToken { get; set; }
        public int amount { get; set; }
        public int basketId { get; set; }
        public int studentId { get; set; }
        public string studentClass { get; set; }
        public string comments { get; set; }
        public string firstName { get; set; }
        public string topupMethod { get; set; }
        public bool saveCard { get; set; }
        public CollectionOption collectionOption { get; set; }

    }

    public class BasketOperationResult
    {
        public int basketId { get; set; }
        public int currentBasketCount { get; set; }
    }

    public class UniformOrder
    {
        public int orderId { get; set; }
        public decimal? orderAmount { get; set; }
        public int stage { get; set; }
        public string parentName { get; set; }
        public string parentEMail { get; set; }
        public string comment { get; set; }
        public DateTime? orderDate { get; set; }
        public string studentName { get; set; }
        public string adminComments { get; set; }
    }

    public class OrderDetails
    {
        public int orderId { get; set; }
        public int productId { get; set; }
        public string productName { get; set; }
        public string sizeOptions { get; set; }
        public string colorOptions { get; set; }
        public int isUniformPack { get; set; }
        public int basketItemId { get; set; }

        public int quantity { get; set; }
        public decimal price { get; set; }
        public decimal subTotal { get; set; }
        public string comment { get; set; }
        public string adminComments { get; set; }
        public string collectionOptionType { get; set; }
        public decimal? deliveryFee { get; set; }
        public String deliveryAddress { get; set; }
        public String deliveryState { get; set; }
        public String deliveryPostCode { get; set; }
        public String deliverySuburb { get; set; }


    }

    public class UniformStockCheckResult
    {
        public bool arePrductsOutOfStock;
        public string checkStockMessage;
    }
}
