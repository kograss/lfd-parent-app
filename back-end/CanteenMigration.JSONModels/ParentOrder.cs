﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class ParentOrder
    {
        //parents_orders
        public int orderId { get; set; }
        public string studentName { get; set; }
        public string studentClass { get; set; }
        public string orderComment { get; set; }
        public decimal orderAmount { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime deliveryDate { get; set; }
        public bool? isCancelledOrder { get; set; }
        public string parentName { get; set; }
        public bool? isFavouriteOrder { get; set; }
        public bool? isSpecialOrder { get; set; }
        public string eventType { get; set; }
        public string eventName { get; set; }
        public bool? isFundRaisingEvent { get; set; }
    }

    public class SchoolOrder
    {
        //parents_orders
        public int orderId { get; set; }
        public string studentName { get; set; }
        public string studentClass { get; set; }
        public string orderComment { get; set; }
        public decimal orderAmount { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime deliveryDate { get; set; }
        public bool? isCancelledOrder { get; set; }
        public string parentName { get; set; }
        public bool? isFavouriteOrder { get; set; }
        public bool? isSpecialOrder { get; set; }
        public string eventType { get; set; }
        public string eventName { get; set; }
    }


    public class OrderItemProduct
    {
        public int productId { get; set; }
        public int studentId { get; set; }
        public string productName { get; set; }
        public string productImage { get; set; }
        public decimal? price { get; set; }
        public Int16 quantity { get; set; }
        public string mealType { get; set; }
        public string productOptions { get; set; }
        public string ccode { get; set; }
        public int? cstock { get; set; }
        public List<OptionDTO> options;
        public string mealCategory { get; set; }//LUNCH, RECESS or THIRD
    }

    public class ReorderOrderItemProducts
    {
        public CartProduct allOrderitemProducts { get; set; }
        public List<OrderItemProduct> errorProductOrderitemProducts { get; set; }
        public List<OrderItemProduct> errorOptionOrderitemProducts { get; set; }

    }



    public class OptionDTO
    {


        public int id { get; set; }
        public string name { get; set; }
        public double? price { get; set; }

        public OptionDTO(int id, string name, double? price)
        {
            this.id = id;
            this.name = name;
            this.price = price;
        }


    }

    public class ParentTransaction
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string commentOrReason { get; set; }
    }

}
