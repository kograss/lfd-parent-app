﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public enum RemoveCartStatus { CART, PRODUCT, QUANTITY }

    public class RemoveCartModel
    {
        public int schoolId { get; set; }
        public int parentId { get; set; }

        public int cartId { get; set; } // will be null when empty cart 
        public bool deleteProduct { get; set; } //specifies if need to remove the entry from c_shopcart -> quantity is zero and want to remove product
        public DateTime deliveryDate { get; set; } // required at empty cart only

        public RemoveCartStatus status { get; set; }
    }

    public class AddToCart
    {
        public int parentId { get; set; }
        public int schoolId { get; set; }
        public int categoryId { get; set; }
        public int productId { get; set; }
        public int productName { get; set; }
        public int[] studentIds { get; set; }
        public int quantity { get; set; }
        public string optionIds { get; set; }
        public DateTime deliveryDate { get; set; }
        public string mealName { get; set; }
    }


    public class CartProductAtrributes
    {
        public string optionName { get; set; }
        public Double? optionPrice { get; set; }

    }

    public class AddToCartView
    {
        public int productId;
        public string productName;
        public int maximumQuantityAllowed;
        public int exceededQuantity;
        public string studentName;
    }

    public class ZeroPriceProducts
    {
        public int productId;
        public string productName;
    }

    public class AddToCartResult
    {
        public bool isQuanityExceeded;
        public List<AddToCartView> quantityExceedProducts;
        public bool isPriceZeroError;
        public List<ZeroPriceProducts> priceZeroProducts;
        public int cartCount;
    }
}
