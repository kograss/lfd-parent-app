﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.JSONModels
{
    public class ParentPlanUpdateResult
    {
        public bool isPlanUpdated { set; get; }
        public string message { get; set; }
        public int parentPlan {get;set;}
        public decimal parentCredit { get; set; }
    }

    public class Plan
    {
        public int plan { get; set; }
    }
}
