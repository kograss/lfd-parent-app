﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.BusinessDependencies
{
    public interface ILoggerService<T>
    {
        void Info(string message);

        void Debug(string message);
    }
}
