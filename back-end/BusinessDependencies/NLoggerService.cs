﻿using NLog;

namespace CanteenMigration.BusinessDependencies
{
    public class NLoggerService<T> : ILoggerService<T>
    {
        public ILogger logger { get; set; }

        public NLoggerService()
        {
            logger = LogManager.GetLogger(typeof(T).FullName);
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }
    }

   
}
