﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        [Column("orderID")]
        public int id { get; set; }

        [Column("ocustomerid")]
        public int? studentId { get; set; }

        [Column("schoolid")]
	    public int? schoolId { get; set; }

        [Column("odate")]
	    public DateTime? orderDate { get; set; }

        [Column("ordernumber")]
        public string orderNumber { get; set; }

        [Column("ocomment")]
        public string orderComment { get; set; }
	    
        [Column("oreason")]
        public string orderReason { get; set; }
        
        [Column("oshipstate")]
        public string orderShippedState { get; set; }
	    
        [Column("oshipcountry")]
        public string orderShippedCountry { get; set; }
	    
        [Column("opaymethod")]
        public int? orderPaymentMethod { get; set; }
        
        [Column("orderamount")]
        public Decimal? orderAmount { get; set; }
        
        [Column("opromisedshipdate")]
        public DateTime? orderPromisedShipDate { get; set; }
        
        [Column("oshippeddate")]
        public DateTime? orderShippedDate { get; set; }
        
        [Column("oshipmethod")]
        public int? orderShipMethod { get; set; }
        
        [Column("ocardtype")]
        public string orderCardType { get; set; }
        
        [Column("ocardno")]
        public string orderCardNo { get; set; }
        
        [Column("ocardname")]
        public string orderCardName { get; set; }
        
        [Column("ocardexpires")]
        public DateTime? orderCardExpires { get; set; }
        
        [Column("ocardaddress")]
        public string orderCardAddress { get; set; }
        
        [Column("ordertype")]
        public string orderType { get; set; }
        
        [Column("bag")]
        public string bag { get; set; }
	    
        [Column("bagprice")]
        public Decimal? bagPrice { get; set; }
	    
        [Column("order_status")]
        public string orderStatus { get; set; }
	    
        [Column("order_ref")]
        public string orderRef { get; set; }
	    
        [Column("order_field1")]
        public int? orderField1 { get; set; }
	    
        
    }
}