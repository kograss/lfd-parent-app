﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CanteenMigration.Models
{
    [Table("PaymentGateway_TransactionLog")]
    public class PaymentGatewayTransactionLog
    {
        public const String TRANSACTION_TYPE_CANTEEN_CHECKOUT = "PAY AT CHECKOUT";

        public const String TRANSACTION_STATE_START = "START";
        public const String TRANSACTION_STATE_SUCCESS = "SUCCESS";
        public const String TRANSACTION_STATE_FAIL = "FAIL";
        public const String TRANSACTION_STATE_CANCEL = "CANCEL";

        public static String GenerateTransactionId(String transactionType, String paymentGatewayName = "STRIPE")
        {
            return paymentGatewayName + transactionType + Guid.NewGuid().ToString();
        }

        [Key]
        [Column("TransactionId")]
        public String Id { get; set; }

        [Column("PaymentGatewayTransactionId")]
        public String PGTransId { get; set; }

        [Column("Date")]
        public DateTime? TransactionDate { get; set; }

        [Column("School24Fee")]
        public Decimal? School24Fee { get; set; }

        [Column("BankFee")]
        public Decimal? BankFee { get; set; }

        [Column("GrossAmount")]
        public Decimal? GrossAmount { get; set; }

        [Column("OrderAmount")]
        public Decimal? OrderAmount { get; set; }

        [Column("Type")]
        public String Type { get; set; }

        [Column("Status")]
        public String Status { get; set; }

        [Column("Log")]
        public String Log { get; set; }

        [Column("SchoolId")]
        public int SchoolId { get; set; }

        [Column("ParentId")]
        public int? ParentId { get; set; }

        [Column("StudentId")]
        public int? StudentId { get; set; }

        [Column("EventId")]
        public int? EventId { get; set; }

        private class PaymentComponents
        {
            public Decimal School24Fee { get; set; }

            public Decimal GrossAmount { get; set; }

            public Decimal BankFee { get; set; }
        }

        public void CalcOrderTotalCanteenCheckout()
        {
            Decimal stripeFee = 2.1M;
            BankFee = (OrderAmount * stripeFee) / 100;
            School24Fee = 0;
            GrossAmount = OrderAmount + BankFee;
        }
    }
}