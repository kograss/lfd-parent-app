﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace CanteenMigration.Models
{
    [Table("event_products")]
    public class SchoolEventProduct
    {
        [Key]
        [Column("catalogID")]
        public int id { get; set; }

        [Column("ccode")]
        public string mealCode { get; set; }

        [Column("cname")]
        public string productName { get; set; }

        [Column("cdescription")]
        public string productDescription { get; set; }

        [Column("cprice")]
        public Decimal? productPrice { get; set; }

        [Column("cimageurl")]
        public string productImageURL { get; set; }

        [Column("cdateavailable")]
        public DateTime? productDateAvailable { get; set; }

        [Column("cstock")]
        public int? stock { get; set; }

        [Column("ccategory")]
        public int? productCategory { get; set; }

        [Column("optgID")]
        public int? optGId { get; set; }

        [Column("optID2")]
        public int? optId2 { get; set; }

        [Column("allow_comments")]
        public bool? AllowComments { get; set; }

        [Column("is_bystudent")]
        public bool? isByStudent { get; set; }

        [Column("quantity_selection")]
        public bool? quantitySelection { get; set; }

        [Column("Knife")]
        public bool? knife { get; set; }

        [Column("Serviette")]
        public bool? serviette { get; set; }

        [Column("Straw")]
        public bool? straw { get; set; }

        [Column("Sauce")]
        public bool? sauce { get; set; }

        [Column("count_stock")]
        public bool? countStock { get; set; }

        [Column("pickingtype")]
        public int? pickingType { get; set; }

        [Column("extra2")]
        public string extra2 { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

        [Column("healthcategory")]
        public int? healthCategory { get; set; }

        [Column("special_event_id")]
        public int? specialEventId { get; set; }

        [Column("ctemperature")]
        public string ctemperature { get; set; }

        [Column("food_Label")]
        public string foodLabel { get; set; }

        [Column("additionalInputs")]
        public string additionalInputs { get; set; }

        [Column("ProductType")]
        public int? productType { get; set; }

        [Column("MaxAdult")]
        public int? maxAdult { get; set; }

        [Column("MaxChildren")]
        public int? maxChildren { get; set; }

        [Column("isFamily")]
        public bool? isFamily { get; set; }

        [Column("cprice_sibling")]
        public decimal? cpriceSibling { get; set; }

        [Column("discount_code")]
        public int? discountCode { get; set; }

        [Column("max_quantity")]
        public int? maxQuantity { get; set; }

        [Column("cprice_sibling1")]
        public decimal? cpriceSibling1 { get; set; }

        [Column("cprice_sibling2")]
        public decimal? cpriceSibling2 { get; set; }

        [Column("cprice_sibling3")]
        public decimal? cpriceSibling3 { get; set; }

        [Column("tstatus")]
        public bool? tstatus { get; set; }

        [Column("quantity_limit")]
        public int? quantityLimit { get; set; }

        [Column("allowedClasses")]
        public string allowedClasses { get; set; }

    }
}
