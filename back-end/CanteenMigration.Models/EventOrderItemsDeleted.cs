﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("e_oitems_deleted")]
    public class EventOrderItemsDeleted
    {
        [Key]
        [Column("orderitemID")]
        public int Id { get; set; }

        [Column("orderid")]
        public int? OrderId { get; set; }

        [Column("orderintemnumber")]
        public int? OrderIntemNumber { get; set; }

        [Column("catalogid")]
        public int? CatalogId { get; set; }

        [Column("numitems")]
        public Int16? NumItems { get; set; }

        [Column("optionz")]
        public String Options { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("extra2")]
        public String Extra2 { get; set; }

        [Column("additionalInputs")]
        public String AdditionalInputs { get; set; }

        [Column("adultscount")]
        public int? AdultsCount { get; set; }

        [Column("childrencount")]
        public int? ChildrenCount { get; set; }

        [Column("stdid")]
        public int? StdId { get; set; }

        [Column("seatnumber")]
        public String SeatNumber { get; set; }

        [Column("eventid")]
        public int? EventId { get; set; }

        [Column("myseatnumber")]
        public int? MySeatNumber { get; set; }

    }
}