﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("closures")]
    public class Closures
    {
        [Key]
        [Column("closedid")]
        public int id { get; set; }

        [Column("closedayint")]
        public int? closedayint { get; set; }

        [Column("schoolid")]
        public int? schoolID { get; set; }

        [Column("closeday")]
        public string closeday { get; set; }

        [Column("extra2")]
        public string extra2 { get; set; }

    }
}