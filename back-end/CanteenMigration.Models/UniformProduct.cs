﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_products")]
    public class UniformProduct
    {
        [Key]
        [Column("idProduct")]
        public int Id { get; set; }

        [Column("chrProductName")]
        public String Name { get; set; }

        [Column("txtDescription")]
        public String Description { get; set; }

        [Column("chrProductImage")]
        public String ImageUrl { get; set; }

        [Column("intPrice")]
        public Decimal? Price { get; set; }

        [Column("dtSaleStart")]
        public DateTime? SaleStart { get; set; }

        [Column("dtSaleEnd")]
        public DateTime? SaleEnd { get; set; }

        [Column("intSalePrice")]
        public Decimal? SalePrice { get; set; }

        [Column("intActive")]
        public int? Active { get; set; }

        [Column("intFeatured")]
        public byte? Featured { get; set; }

        [Column("dtFeatureStart")]
        public DateTime? FeatureStart { get; set; }

        [Column("dtFeatureEnd")]
        public DateTime? FeatureEnd { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("chrProductCode")]
        public String ProductCode { get; set; }

        [Column("chrSKU")]
        public String SKU { get; set; }

        [Column("condition")]
        public String Condition { get; set; }

        [Column("buy_price")]
        public Decimal? BuyPrice { get; set; }

        [Column("sellerID")]
        public int? SellerId { get; set; }

        [Column("attract_gst")]
        public String AttractGst { get; set; }

        [Column("gst")]
        public int? Gst { get; set; }

        [Column("sell_type")]
        public String SellType { get; set; }

        [Column("product_stock")]
        public int? ProductStock { get; set; }

        [Column("uniformPacksIds")]
        public String UniformPackIds { get; set; }
    }
}
