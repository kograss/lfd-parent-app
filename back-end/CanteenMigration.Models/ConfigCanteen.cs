﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("config_canteen")]
    public class ConfigCanteen
    {
        [Key]
        [Column("configid")]
        public int id { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }
        
        [Column("canteenopen")]
        public bool? isCanteenOpen { get; set; }
	    
        [Column("opensaturday")]
        public bool? isOpenOnSaturday { get; set; }
	    
        [Column("opensunday")]
        public bool? isOpenOnSunday { get; set; }
	    
        [Column("recess")]
        public bool? isRecessAvailable { get; set; }
	    
        [Column("allowcomments")]
        public bool? isAllowComments { get; set; }
	    
        [Column("allowprocearly")]
        public bool? isAllowProcearly { get; set; }

	    [Column("extra3")]
        public bool? extra3 { get; set; }
	    
        [Column("extra4")]
        public string extra4 { get; set; }
	    
        [Column("allowbusdirectory")]
        public bool? isAllowBusDirectory { get; set; }
	    
        [Column("allowbboard")]
        public bool? isAllowBBoard { get; set; }
	    
        [Column("allowforum")]
        public bool? isAllowForum { get; set; }
	    
        [Column("allowvolunteers")]
        public bool? isAllowVolunteers { get; set; }
	    
        [Column("menu_share")]
        public bool? isMenuShare { get; set; }
	    
        [Column("logo_img")]
        public string logoImage { get; set; }
	    
        [Column("meal1")]
        public string meal1 { get; set; }
	    
        [Column("meal2")]
        public string meal2 { get; set; }
	    
        [Column("shop")]
        public string shopName { get; set; }
	    
        [Column("comment_max")]
        public int? commentMax { get; set; }
	    
    }

}