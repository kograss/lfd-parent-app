﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("CustomerStripeDetails")]
    public class CustomerStripeDetails
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Column("schoolId")]
        public int schoolId { get; set; }


        [Column("parentId")]
        public int parentId { get; set; }


        [Column("customerId")]
        public string customerId { get; set; }


        [Column("cardId")]
        public string cardId { get; set; }


        [Column("cardLast4")]
        public string cardLast4 { get; set; }
    }
}
