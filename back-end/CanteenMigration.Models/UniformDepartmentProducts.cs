﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CanteenMigration.Models
{
    [Table("uniform_departmentproducts")]
    public class UniformDepartmentProduct
    {
        [Key]
        [Column("idDepartmentProduct")]
        public int Id { get; set; }

        [Column("idDepartment")]
        public int? departmentId { get; set; }

        [Column("idProduct")]
        public int? productId { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

    }
}
