﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("parent")]
    public class Parent
    {
        [Key]
        [Column("parentid")]
        public int id { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("firstname")]
        public string firstName { get; set; }

        [Column("lastname")]
        public string lastName { get; set; }

        [Column("homephone")]
        public string homePhone { get; set; }

        [Column("mobile")]
        public string mobile { get; set; }

        [Column("username")]
        public string userName { get; set; }

        [Column("credit")]
        public Decimal? credit { get; set; }

        [Column("familymembers")]
        public int? familyMember { get; set; }

        [Column("email")]
        public string email { get; set; }

        [Column("comments")]
        public string comments { get; set; }

        [Column("logincount")]
        public int? loginCount { get; set; }

        [Column("lastlogindate")]
        public DateTime? lastLoginDate { get; set; }

        [Column("password_old")]
        public string passwordOld { get; set; }

        [Column("eventdate")]
        public DateTime? eventDate { get; set; }

        [Column("eventtype")]
        public string eventType { get; set; }

        [Column("eventdateend")]
        public DateTime? eventDateEnd { get; set; }

        [Column("coemail")]
        public string coEmail { get; set; }

        [Column("regdate")]
        public DateTime? registerDate { get; set; }

        [Column("regtime")]
        public DateTime? registerTime { get; set; }

        [Column("thankyounote")]
        public string thankyouNote { get; set; }

        [Column("extra1")]
        public string extra1 { get; set; }

        [Column("extra2")]
        public int? extra2 { get; set; }

        [Column("extra3")]
        public string extra3 { get; set; }

        [Column("canteenrole")]
        public string canteenRole { get; set; }

        [Column("processed")]
        public bool? processed { get; set; }

        [Column("vipstatus")]
        public string vipStatus { get; set; }

        [Column("password")]
        public string password { get; set; }

        [Column("p_fee_plan")]
        public int? parentFeePlan { get; set; }

        [Column("lastcreditupdate")]
        public DateTime? lastCreditUpdate { get; set; }

        [Column("lastdeductedamount")]
        public Decimal? lastDeductedAmount { get; set; }

        [Column("pencrypt")]
        public string pEncrypt { get; set; }

        [Column("logintype")]
        public int? loginType { get; set; }

        [Column("receive_status")]
        public bool? receiveStatus { get; set; }

        [Column("password_before_encryption")]
        public string password_before_encryption { get;set;}

        [Column("isEmailVerified")]
        public bool? isEmailVerified { get; set; }
        
    }
}