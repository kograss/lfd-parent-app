﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("event_options")]
    public class EventOption
    {
        [Key]
        [Column("OPT_ID")]
        public int id { get; set; }

        [Column("OPT_OptionGroupID")]
        public int? optionGroupId { get; set; }

        [Column("OPT_Name1")]
        public string optionName1 { get; set; }

        [Column("OPT_Name2")]
        public string optionName2 { get; set; }

        [Column("OPT_Name3")]
        public string optionName3 { get; set; }

        [Column("OPT_Name4")]
        public string optionName4 { get; set; }

        [Column("OPT_Name5")]
        public string optionName5 { get; set; }

        [Column("OPT_CheckBoxValue")]
        public string optionCheckBoxValue { get; set; }

        [Column("OPT_PriceAdd")]
        public Double? optionPrice { get; set; }

        [Column("OPT_DefWeightChange")]
        public Double? optionDefWeightChange { get; set; }

        [Column("OPT_DefOrderByValue")]
        public Single? optionDefOrderByValue { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("option_stock")]
        public int? optionStock { get; set; }
    }
}
