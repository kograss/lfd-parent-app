﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_pack_item_product")]
    public class UniformPackItemProduct
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Column("idProduct")]
        public int? productId { get; set; }

        [Column("intPrice")]
        public Decimal price { get; set; }

        [Column("chrName")]
        public String name { get; set; }

        [Column("itemId")]
        public int? itemId { get; set; }

        [Column("chrSize")]
        public string size { get; set; }

        [Column("chrColor")]
        public string color { get; set; }

        [Column("quantity")]
        public int quantity { get; set; }

    }
}
