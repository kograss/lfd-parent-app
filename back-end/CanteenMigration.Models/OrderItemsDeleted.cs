﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("oitems_deleted")]
    public class OrderItemsDeleted
    {
        [Key]
        [Column("orderitemID")]
        public int id { get; set; }

        [Column("orderid")]
        public int? orderID { get; set; }

        [Column("orderintemnumber")]
        public int? orderIntemNumber { get; set; }

        [Column("catalogid")]
        public int? productId { get; set; }

        [Column("numitems")]
        public Int16 numerOfItems { get; set; }

        [Column("optionz")]
        public string options { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("extra2")]
        public string extra2 { get; set; }

    }
}