﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("themes")]
    public class Themes
    {
        [Key]
        [Column("themeId")]
        public int themeId { get; set; }

        [Column("theme_color")]
        public string themeColor { get; set; }

        [Column("theme_sub_header_bg")]
        public string themeSubHeaderBg { get; set; }

        [Column("theme_search_go")]
        public string theme_Search_Go { get; set; }

        [Column("theme_sub_header_text")]
        public string themeSubHeaderText { get; set; }

        [Column("theme_sub_animated_img")]
        public string themeSubAnimatedImg { get; set; }

        [Column("theme_name")]
        public string themeName { get; set; }

    }
}