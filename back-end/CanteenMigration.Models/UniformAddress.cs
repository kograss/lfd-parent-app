﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace CanteenMigration.Models
{
    [Table("UniformAddress")]
    public class UniformAddress
    {
        [Key]
        [Column("Id")]
        public int? id { get; set; }

        [Column("SchoolId")]
        public int? schoolId { get; set; }  //ParentId

        [Column("ParentId")]
        public int parentId { get; set; }

        [Column("Address")]
        public String address { get; set; }

        [Column("Suburb")]
        public String suburb { get; set; }

        [Column("State")]
        public String state { get; set; }

        [Column("PostCode")]
        public String postCode { get; set; }

        [Column("IsDeleted")]
        public int? isDeleted { get; set; }
    }
}
