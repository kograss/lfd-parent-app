﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("BannedFood")]
    public class BannedFood
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("studentId")]
        public int studentId { get; set; }

        [Column("productId")]
        public int productId { get; set; }

        [Column("parentId")]
        public int parentId { get; set; }
    }
}
