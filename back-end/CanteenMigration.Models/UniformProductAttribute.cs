﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_ProductAttribute")]
    public class UniformProductAttribute
    {
        [Key]
        [Column("idProductAttribute")]
        public int Id { get; set; }

        [Column("idAttribute")]
        public int? attributeId { get; set; }

        [Column("idProduct")]
        public int? productId { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }
    }
}
