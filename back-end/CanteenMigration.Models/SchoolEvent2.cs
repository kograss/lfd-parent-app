﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CanteenMigration.Models
{
    [Table("school_events")]
    public class SchoolEvent2
    {
        [Key]
        [Column("specialID")]
        public int schoolEventId { get; set; }

        [Column("specialDateStart")]
        public DateTime? eventDateStart { get; set; }

        [Column("specialDateEnd")]
        public DateTime? eventDateEnd { get; set; }

        [Column("special_name")]
        public String eventName { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("specialOrderDate")]
        public DateTime? eventDate { get; set; }

        [Column("specialPrice")]
        public Decimal? eventPrice { get; set; }

        [Column("is_set_price")]
        public int? isSetPrice { get; set; }

        [Column("is_weekly")]
        public bool? isWeekly { get; set; }

        [Column("event_day")]
        public int? eventDay { get; set; }

        [Column("event_cutday")]
        public int? eventCutDay { get; set; }

        [Column("event_cuthour")]
        public int? eventCutHour { get; set; }

        [Column("event_cutmin")]
        public int? eventCutMin { get; set; }

        [Column("event_type")]
        public int? eventType { get; set; }

        [Column("event_type_name")]
        public String eventTypeName { get; set; }

        [Column("chargeforbag")]
        public String chargeForBag { get; set; }

        [Column("bagprice")]
        public Decimal? bagPrice { get; set; }

        [Column("event_image")]
        public String eventImage { get; set; }

        [Column("event_location")]
        public String eventLocation { get; set; }

        [Column("event_bank")]
        public String eventBank { get; set; }

        [Column("event_bank_bsb")]
        public String eventBankBSB { get; set; }

        [Column("event_bank_accountname")]
        public String eventBankAccountName { get; set; }

        [Column("event_bank_accountnumber")]
        public String eventBankAccountNumber { get; set; }

        [Column("event_admin_name")]
        public String eventAdminName { get; set; }

        [Column("event_admin_email")]
        public String eventAdminEmail { get; set; }

        [Column("event_admin_mobile")]
        public String eventAdminMobile { get; set; }

        [Column("event_description")]
        public String eventDescription { get; set; }

        [Column("event_additionalInfo")]
        public String eventAdditionalInfo { get; set; }

        [Column("is_print_date")]
        public bool? isPrintDate { get; set; }

        [Column("is_print_student")]
        public bool? isPrintStudent { get; set; }

        [Column("is_ticket_type")]
        public bool? isTicketType { get; set; }

        [Column("select_options_phrase")]
        public String selectOptionsPhrase { get; set; }

        [Column("event_admin_fee")]
        public Decimal? eventAdminFee { get; set; }

        [Column("has_roster")]
        public bool? hasRoster { get; set; }

        [Column("whatfor")]
        public int? whatFor { get; set; }

        [Column("tstatus")]
        public bool? tStatus { get; set; }

        [Column("bagname")]
        public String bagName { get; set; }

        [Column("description")]
        public string description { get; set; }

        [Column("images")]
        public string images { get; set; }

        [Column("documents")]
        public string documents { get; set; }

    }
}
