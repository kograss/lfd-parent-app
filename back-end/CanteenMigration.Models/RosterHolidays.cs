﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Roster.Models
{
    [Table("RosterHolidays")]
    public class RosterHolidays
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("SchoolId")]
        public int schoolId { get; set; }

        [Column("RosterId")]
        public int rosterId { get; set; }

        [Column("HolidayStartDate")]
        public Nullable<System.DateTime> holidayStartDate { get; set; }

        [Column("HolidayEndDate")]
        public Nullable<System.DateTime> holidayEndDate { get; set; }

        [Column("HolidayName")]
        public string holidayName { get; set; }

    }
}