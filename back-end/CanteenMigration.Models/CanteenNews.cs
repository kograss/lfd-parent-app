﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{

    [Table("canteen_news")]
    public class CanteenNews
    {
        [Key]
        [Column("NewsID")]
        public int NewsId { get; set; }

        [Column("Description")]
        public String Description { get; set; }

        [Column("DateEntered")]
        public DateTime? DateEntered { get; set; }

        [Column("Subject")]
        public String Subject { get; set; }

        [Column("Priority")]
        public String Priority { get; set; }

        [Column("EnteredBy")]
        public String EnteredBy { get; set; }

        [Column("dayid")]
        public String DayId { get; set; }

        [Column("Status")]
        public String Status { get; set; }

        [Column("Comment")]
        public String Comment { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

    }
}