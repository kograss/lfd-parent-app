﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CanteenMigration.Models
{
    [Table("special_events")]
    public class SpecialEvent
    {
        [Key]
        [Column("specialID")]
        public int specialId { get; set; }

        [Column("specialDateStart")]
        public DateTime? specialDateStart { get; set; }

        [Column("specialDateEnd")]
        public DateTime? specialDateEnd { get; set; }

        [Column("special_name")]
        public String EventName { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("specialOrderDate")]
        public DateTime? EventDate { get; set; }

        [Column("specialPrice")]
        public Decimal? EventPrice { get; set; }

        [Column("is_set_price")]
        public int? IsSetPrice { get; set; }

        [Column("is_weekly")]
        public bool? IsWeekly { get; set; }

        [Column("event_day")]
        public int? EventDay { get; set; }

        [Column("event_cutday")]
        public int? EventCutDay { get; set; }

        [Column("event_cuthour")]
        public int? EventCutHour { get; set; }

        [Column("event_cutmin")]
        public int? EventCutMin { get; set; }

        [Column("event_type")]
        public int? EventType { get; set; }

        [Column("chargeforbag")]
        public String ChargeForBag { get; set; }

        [Column("bagprice")]
        public Decimal? BagPrice { get; set; }

        [Column("event_image")]
        public String EventImage { get; set; }

        [Column("tstatus")]
        public bool? TStatus { get; set; }

        [Column("event_description")]
        public String EventDescription { get; set; }


        [Column("is_print_date")]
        public bool? IsPrintDate { get; set; }

        [Column("is_print_student")]
        public bool? IsPrintStudent { get; set; }

        /*new columns after discussion with Mike*/
        [Column("isBagAvailable")]
        public bool? isBagAvailable { get; set; }

        [Column("bagName")]
        public string bagName { get; set; }

        [Column("WhoPays")]
        public int? whoPays { get; set; }

        [Column("description")]
        public string description { get; set; }

        [Column("images")]
        public string images { get; set; }

        [Column("documents")]
        public string documents { get; set; }

        [Column("isFundRaisingEvent")]
        public bool? isFundRaisingEvent { get; set; }
    }
}
