﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("transactions")]
    public class Transaction
    {
        [Key]
        [Column("tid")]
        public int id { get; set; }

        [Column("ocustomerid")]
        public int? studentId { get; set; }

        [Column("pid")]
        public int? parentId { get; set; }
        
        [Column("tdate")]
        public DateTime? transactionDate { get; set; }

        [Column("tamount")]
        public Decimal? transactionAmount { get; set; }

        [Column("ttype")]
        public string transactionType { get; set; }

        [Column("extra1")]
        public string extra1 { get; set; }

        [Column("comment")]
        public string comment { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("extra4")]
        public int? extra4 { get; set; }

        [Column("balancebefore")]
        public Decimal? balanceBefore { get; set; }

        [Column("balanceafter")]
        public Decimal? balanceAfter { get; set; }

        [Column("recycleprice")]
        public Decimal? recyclePrice { get; set; }

        [Column("torderdesc")]
        public string transactionOrderDesc { get; set; }

        [Column("txn_id")]
        public string transactionId { get; set; }

        [Column("oid1")]
        public string orderId1 { get; set; }

        [Column("oid2")]
        public string orderId2 { get; set; }

        [Column("oid3")]
        public string orderId3 { get; set; }

        [Column("oid4")]
        public string orderId4 { get; set; }

        [Column("BalanceDeductedAmount")]
        public decimal? balanceDeductedAmount { get; set; }

        [Column("CardPaymentAmount")]
        public decimal? cardPaymentAmount { get; set; }
    }
}