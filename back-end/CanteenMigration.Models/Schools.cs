﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("schools")]
    public class Schools
    {
        [Key]
        [Column("schoolid")]
        public int schoolId { get; set; }
 
        [Column("schoolname")]
        public string schoolName { get; set; }

        [Column("schooltype")]
        public string schoolType { get; set; }
 
        [Column("schooldeg")]
        public string schoolDeg { get; set; }
 
        [Column("schoolregion")]
        public string schoolRegion { get; set; }
 
        [Column("schoolstate")]
        public string schoolState { get; set; }
 
        [Column("schoollogo")]
        public string schoolLogo { get; set; }
 
        [Column("comment")]
        public string comment { get; set; }
 
        [Column("postalAddress1")]
        public string postalAddress1 { get; set; }
 
        [Column("postalAddress2")]
        public string postalAddress2 { get; set; }

         [Column("PostalSuburb")]
        public string postalSuburb { get; set; }
 
        [Column("phone")]
        public string phone { get; set; }
 
        [Column("email")]
        public string email { get; set; }
 
        [Column("mobile")]
        public string mobile { get; set; }

        [Column("schoolcity")]
        public string schoolCity { get; set; }

        [Column("regtime")]
        public DateTime? regTime { get; set; }
 
        [Column("processed")]
        public bool? isProcessed { get; set; }
 
        [Column("postcode")]
        public string postCode { get; set; }

        [Column("status")]
        public string status { get; set; }
 
        [Column("logo_trans")]
        public string logoTrans { get; set; }

        [Column("portal")]
        public string portal { get; set; }
 
        [Column("skidencrypt")]
        public string skidEncrypt { get; set; }
 
        [Column("country")]
        public string country { get; set; }

        [Column("locale_id")]
        public string localeId { get; set; }

        [Column("centre_code")]
        public int? centreCode { get; set; }

        [Column("CanteenOrderConfirmationEmailText")]
        public string CanteenOrderConfirmationEmailText { get; set; }
        [Column("canteen_provider")]
        public int? canteen_provider { get; set; }
    }
}