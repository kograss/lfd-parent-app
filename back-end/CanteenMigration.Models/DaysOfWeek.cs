﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("days_of_week")]
    public class DaysOfWeek
    {
         [Key]
         [Column("dayid")]
         public int id { get; set; }
           
         [Column("day_number")]
         public int? dayNumber { get; set; }
          
         [Column("day_name")]
         public string dayName { get; set; }
          
    }
}