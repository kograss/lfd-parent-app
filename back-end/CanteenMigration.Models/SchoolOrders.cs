﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("school_orders")]
    public class SchoolOrders
    {
        [Key]
        [Column("orderID")]
        public int Id { get; set; }

        [Column("ocustomerid")]
        public int? CustomerId { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("odate")]
        public DateTime? OrderDate { get; set; }

        [Column("ordernumber")]
        public String OrderNumber { get; set; }

        [Column("ocomment")]
        public String OrderComment { get; set; }

        [Column("oreason")]
        public String OrderReason { get; set; }

        [Column("oshipstate")]
        public String OrderShippingState { get; set; }

        [Column("oshipcountry")]
        public String OrderShippingCountry { get; set; }

        [Column("opaymethod")]
        public int? OrderPaymentMethod { get; set; }

        [Column("orderamount")]
        public Decimal? OrderAmount { get; set; }

        [Column("order_cancel_date")]
        public DateTime? OrderCancelDate { get; set; }

        [Column("oshippeddate")]
        public DateTime? OrderShippedDate { get; set; }

        [Column("oshipmethod")]
        public int? OrderShippingMethod { get; set; }

        [Column("ocardtype")]
        public String OrderCardType { get; set; }

        [Column("ocardno")]
        public String OrderCardNumber { get; set; }

        [Column("ocardname")]
        public String OrderCardName { get; set; }

        [Column("ocardexpires")]
        public DateTime? OrderCardExpiryDate { get; set; }

        [Column("ocardaddress")]
        public String OrderCardAddress { get; set; }

        [Column("ordertype")]
        public String OrderType { get; set; }

        [Column("bag")]
        public String Bag { get; set; }

        [Column("bagprice")]
        public Decimal? BagPrice { get; set; }

        [Column("order_status")]
        public String OrderStatus { get; set; }

        [Column("order_ref")]
        public String OrderRef { get; set; }

        [Column("order_field1")]
        public int? OrderField1 { get; set; }

        [Column("parentid")]
        public int? ParentId { get; set; }

        [Column("school_model")]
        public int? SchoolModel { get; set; }

        [Column("school_plan")]
        public int? SchoolPlan { get; set; }

        [Column("parent_plan")]
        public int? ParentPlan { get; set; }

        [Column("order_cancel")]
        public bool? OrderCancel { get; set; }

    }
}
