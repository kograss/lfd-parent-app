﻿namespace CanteenMigration.Models
{
    public class StudentClassType
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public string classType { get; set; }
    }
}