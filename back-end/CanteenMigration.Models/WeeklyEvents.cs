﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("weekly_events")]
    public class WeeklyEvents
    {
        [Key]
        [Column("specialID")]
        public int id { get; set; }

        [Column("specialDateStart")]
        public int? specialDateStart { get; set; }
        
        [Column("specialDateEnd")]
        public DateTime? specialDateEnd { get; set; }
        
        [Column("special_name")]
        public string specialName { get; set; }
	    
	    [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("specialOrderDate")]
        public DateTime? specialOrderDate { get; set; }

        [Column("specialPrice")]
        public Decimal? specialPrice { get; set; }

        [Column("is_set_price")]
        public int? isSetPrice { get; set; }

        [Column("event_day")]
        public int? eventDay { get; set; }

        [Column("event_cutday")]
        public int? eventCutDay { get; set; }
	    
        [Column("event_cuthour")]
        public int? eventCutHour { get; set; }

        [Column("event_cutmin")]
        public int? eventCutMin { get; set; }
	    
        [Column("is_weekly")]
        public bool? isWeekly { get; set; }
	}
}