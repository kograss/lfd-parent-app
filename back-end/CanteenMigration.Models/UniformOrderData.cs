﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CanteenMigration.Models
{
    [Table("uniform_OrderData")]
    public class UniformOrderData
    {
        [Key]
        [Column("idOrder")]
        public int? OrderId { get; set; }

        [Column("idShopper")]
        public int? ShopperId { get; set; }  //ParentId

        [Column("chrShipFirstName")]
        public String ShipFirstName { get; set; }

        [Column("chrShipLastName")]
        public String ShipLastName { get; set; }

        [Column("chrShipAddress")]
        public String deliveryAddress { get; set; }

        [Column("chrShipCity")]
        public String ShipCity { get; set; }

        [Column("chrShipState")]
        public String deliveryState { get; set; }

        [Column("chrShipZipCode")]
        public String deliveryPostCode { get; set; }

        [Column("chrShipSuburb")]
        public String deliverySuburb { get; set; }

        [Column("chrShipPhone")]
        public String ShipPhone { get; set; }

        [Column("chrShipMobile")]
        public String ShipMobile { get; set; }

        [Column("chrShipEmail")]
        public String ShipEmail { get; set; }

        [Column("chrBillFirstName")]
        public String BillFirstName { get; set; }

        [Column("chrBillLastName")]
        public String BillLastName { get; set; }

        [Column("chrBillAddress")]
        public String BillAddress { get; set; }

        [Column("chrBillCity")]
        public String BillCity { get; set; }

        [Column("chrBillState")]
        public String BillState { get; set; }

        [Column("chrBillZipCode")]
        public String BillZipCode { get; set; }

        [Column("chrBillPhone")]
        public String BillPhone { get; set; }

        [Column("chrBillMobile")]
        public String BillMobile { get; set; }

        [Column("chrBillEmail")]
        public String BillEmail { get; set; }

        [Column("dtOrdered")]
        public DateTime? OrderedDate { get; set; }

        [Column("chrShipProvince")]
        public String ShipProvince { get; set; }

        [Column("chrShipCountry")]
        public String ShipCountry { get; set; }

        [Column("chrBillProvince")]
        public String BillProvince { get; set; }

        [Column("chrBillCountry")]
        public String BillCountry { get; set; }

        [Column("idBasket")]
        public int? BasketId { get; set; }

        [Column("intFreeShipping")]
        public int? FreeShipping { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("order_note")]
        public String OrderNote { get; set; }

        [Column("collectionOption")]
        public String collectionOption { get; set; }

        [Column("deliveryFee")]
        public Decimal? deliveryFee { get; set; }
    }
}
