﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_packs")]
    public class UniformPack
    {
        [Key]
        [Column("uniformPackId")]
        public int Id { get; set; }

        [Column("uniformPackName")]
        public String Name { get; set; }

        [Column("uniformPackDescription")]
        public String Description { get; set; }

        [Column("uniformPackImage")]
        public String ImageUrl { get; set; }

        [Column("intPrice")]
        public Decimal? Price { get; set; }

        [Column("dtSaleStart")]
        public DateTime? SaleStart { get; set; }

        [Column("dtSaleEnd")]
        public DateTime? SaleEnd { get; set; }

        [Column("intSalePrice")]
        public Decimal? SalePrice { get; set; }

        [Column("intActive")]
        public int? Active { get; set; }

        [Column("intFeatured")]
        public byte? Featured { get; set; }

        [Column("dtFeatureStart")]
        public DateTime? FeatureStart { get; set; }

        [Column("dtFeatureEnd")]
        public DateTime? FeatureEnd { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("uniformPackCode")]
        public String uniformPackCode { get; set; }

        [Column("uniformPackSKU")]
        public String SKU { get; set; }

        [Column("condition")]
        public String Condition { get; set; }

        [Column("buy_price")]
        public Decimal? BuyPrice { get; set; }

        [Column("sellerID")]
        public int? SellerId { get; set; }

        [Column("id")]
        public int idForCategory { get; set; }

        [Column("attract_gst")]
        public String AttractGst { get; set; }

        [Column("gst")]
        public int? Gst { get; set; }

        [Column("sell_type")]
        public String SellType { get; set; }

        [Column("uniformPackStock")]
        public int? UniformPackStock { get; set; }
    }
}