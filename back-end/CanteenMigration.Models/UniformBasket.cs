﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_Basket")]
    public class UniformBasket
    {
        [Key]
        [Column("idBasket")]
        public int Id { get; set; }

        //Maps to intQuantity
        [Column("intQuantity")]
        public int? Quantity { get; set; }

        //Maps to idShopper
        [Column("idShopper")]
        public int? ParentId { get; set; }

        [Column("intOrderPlaced")]
        public int? OrderPlaced { get; set; }

        [Column("intSubTotal")]
        public Decimal? SubTotal { get; set; }

        [Column("intTotal")]
        public Decimal? Total { get; set; }

        [Column("intShipping")]
        public int? Shipping { get; set; }

        [Column("intTax")]
        public int? Tax { get; set; }

        [Column("dtCreated")]
        public DateTime? CreatedOn { get; set; }

        [Column("intFreeShipping")]
        public int? FreeShipping { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("parentid")]
        public int? NotUsedParentId { get; set; }

        [Column("comment")]
        public String Comment { get; set; }

        [Column("class")]
        public String Class { get; set; }

        [Column("student")]
        public String Student { get; set; }
    }
}
