﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CanteenMigration.Models
{
    [Table("e_transactions")]
    public class EventTransactions
    {
        [Key]
        [Column("tid")]
        public int Id { get; set; }

        [Column("ocustomerid")]
        public int? CustomerId { get; set; }

        [Column("pid")]
        public int? ParentId { get; set; }

        [Column("tdate")]
        public DateTime? TransactionDate { get; set; }

        [Column("tamount")]
        public Decimal? TransactionAmount { get; set; }

        [Column("ttype")]
        public String TransactionType { get; set; }

        [Column("extra1")]
        public String Extra1 { get; set; }

        [Column("comment")]
        public String Comment { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("orderID")]
        public int? OrderID { get; set; }

        [Column("balancebefore")]
        public Decimal? BalanceBefore { get; set; }

        [Column("balanceafter")]
        public Decimal? BalanceAfter { get; set; }

        [Column("recycleprice")]
        public Decimal? RecyclePrice { get; set; }

        [Column("torderdesc")]
        public String TransactionOrderDesc { get; set; }

        [Column("txn_id")]
        public String TransactionId { get; set; }

        [Column("paypal_payment_status")]
        public String PaypalPaymentStatus { get; set; }

        [Column("paypal_pending_reason")]
        public String PaypalPendingReason { get; set; }

        [Column("paypal_transactionid")]
        public String PaypalTransactionId { get; set; }

        [Column("oid4")]
        public String OId4 { get; set; }

        [Column("bank_fee")]
        public Decimal? BankFee { get; set; }

        [Column("eventid")]
        public int? EventId { get; set; }

        [Column("servicefee")]
        public Decimal ServiceFee { get; set; }

        [Column("calculated_paypalfee")]
        public Decimal? CalculatedPaypalFee { get; set; }
    }
}