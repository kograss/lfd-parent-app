﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_Attribute")]
    public class UniformAttribute
    {
        [Key]
        [Column("idAttribute")]
        public int Id { get; set; }

        [Column("chrAttributeName")]
        public string attributeName { get; set; }

        [Column("idAttributeCategory")]
        public int? attributeCategoryId { get; set; }

        [Column("size_seq")]
        public int? sizeSeq { get; set; }

        [Column("school_wants_size")]
        public int? schoolWantsSize { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }
    }
}
