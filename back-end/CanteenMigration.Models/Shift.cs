﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Roster.Models
{
    [Table("shifts")]
    public class Shift
    {
        [Key]
        [Column("shift_id")]
        public int shiftId { get; set; }

        [Column("shift_name")]
        public string shiftName { get; set; }

        [Column("shift_max")]
        public Nullable<int> shiftMax { get; set; }

        [Column("schoolid")]
        public string schoolId { get; set; }

        [Column("shift_day")]
        public Nullable<int> day { get; set; }

        [Column("shift_comment")]
        public string comments { get; set; }

        [Column("shift_number")]
        public Nullable<int> shiftNumber { get; set; }

        [Column("isActive")]
        public Nullable<Boolean> isActive { get; set; }

        [Column("isSingleDateShift")]
        public Nullable<Boolean> isSingleDateShift { get; set; }

        [Column("day2work")]
        public Nullable<System.DateTime> day2Work { get; set; }
        //[Column("isDeleted")]
        //public Nullable<Boolean> isDeleted { get; set; }
    }
}