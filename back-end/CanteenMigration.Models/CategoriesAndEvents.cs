﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Models
{
    [Table("categoriesandevents")]
    public class CategoriesAndEvents
    {
        [Key]
        [Column("categoryID")]
        public int categoryId { get; set; }

        [Column("eventID")]
        public int? eventId { get; set; }

        [Column("categoryName")]
        public string categoryName { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

        [Column("tstatus")]
        public bool? tStatus { get; set; }

        [Column("cat_position")]
        public int? categoryPosition { get; set; }


    }
}
