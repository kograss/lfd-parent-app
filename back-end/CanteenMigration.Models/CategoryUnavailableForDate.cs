﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("category_unavailable_4date")]
    public class CategoryUnavailableForDate
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("CloseDateStart")]
        public DateTime? closeDateStart { get; set; }

        [Column("ClosedateEnd")]
        public DateTime? closeDateEnd { get; set; }

        [Column("ServiceToClose")]
        public string serviceToClose { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("CloseDate")]
        public DateTime? closeDate { get; set; }

        [Column("categoryID")]
        public int? categoryID { get; set; }
    }
}