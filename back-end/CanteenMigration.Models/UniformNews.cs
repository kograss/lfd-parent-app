﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_news")]
    public class UniformNews
    {
        [Key]
        [Column("NewsID")]
        public int newsId { get; set; }

        [Column("Description")]
        public string description { get; set; }

        [Column("DateEntered")]
        public DateTime? dateEntered { get; set; }

        [Column("Subject")]
        public string subject { get; set; }

        [Column("Priority")]
        public string priority { get; set; }

        [Column("EnteredBy")]
        public string enteredBy { get; set; }

        [Column("dayid")]
        public string dayId { get; set; }

        [Column("Status")]
        public string status { get; set; }

        [Column("Comment")]
        public string comment { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("date_edit")]
        public DateTime? dateEdit { get; set; }

    }
}
