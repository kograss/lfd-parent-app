﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CanteenMigration.Models
{
    [Table("FundRaisingEventOrders")]
    public class FundRaisingEventOrders
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("ParentId")]
        public int? parentId { get; set; }

        [Column("SchoolId")]
        public int? schoolId { get; set; }

        [Column("StudentId")]
        public int? studentId { get; set; }

        [Column("PaymentGatewayTransactionId")]
        public string paymentGatewayTransactionId { get; set; }

        [Column("StripeTransactionId")]
        public string stripeTransactionId { get; set; }

        [Column("Status")]
        public string status { get; set; }

        [Column("OrderDateTime")]
        public DateTime? orderDateTime { get; set; }

        [Column("Amount")]
        public int? amount { get; set; }

        [Column("oshippeddate")]
        public DateTime? orderShippedDate { get; set; }

        [Column("EventId")]
        public int? EventId { get; set; }
    }
}
