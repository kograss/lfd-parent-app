﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_Department")]
    public class UniformDepartment
    {
        [Key]
        [Column("idDepartment")]
        public int id { get; set; }

        [Column("chrDeptName")]
        public string name { get; set; }

        [Column("txtDeptDesc")]
        public string description { get; set; }

        [Column("chrDeptImage")]
        public string image { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("dept_number")]
        public string number { get; set; }
    }
}
