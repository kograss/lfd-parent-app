﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("customers")]
    public class Customers
    {
            [Key]
            [Column("custID")]
            public int id { get; set; }

            [Column("parentid")]
            public int? parentId { get; set; }

            [Column("schoolid")]
            public int? schoolId { get; set; }
           
            [Column("cfirstname")]
            public string studentFirstName { get; set; }
            
            [Column("clastname")]
            public string studentLastName { get; set; }
            
            /*[Column("cemail")]
            public string CEmail { get; set; }*/
            
            [Column("cimage")]
            public string studentImage { get; set; }
            
            [Column("extra2")]
            public string extra2 { get; set; }
            
            /*[Column("extra3")]
            public string extra3 { get; set; }*/
            
            [Column("cstatus")]
            public string studentStatus { get; set; }
            
            [Column("calergy")]
            public string calergy { get; set; }
            
            [Column("cclass")]
            public string studentClass { get; set; }
            
            [Column("cnotes")]
            public string studentNotes { get; set; }
            
            [Column("cusername")]
            public string studentUserName { get; set; }
            
            [Column("cpass")]
            public string studentPass { get; set; }
            
            [Column("cphone")]
            public string studentPhone { get; set; }
 
            [Column("maxallow")]
            public decimal? maxAllow { get; set; }

            [Column("oldpid")]
            public int? oldParentId { get; set; }

            [Column("olddudid")]
            public int? oldDudId { get; set; } //customers will be fetched only if this is 1

            [Column("role")]
            public int? role { get; set; }

            [Column("ccreated")]
            public DateTime? studentCreatedDate { get; set; }

            [Column("cupdated")]
            public DateTime? studentUpdatedDate { get; set; }

            [Column("studentcardid")]
            public string studentCardId { get; set; }
        }
}