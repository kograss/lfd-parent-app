﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("parents_orders")]
    public class ParentsOrders
    {
        [Key]
        [Column("orderID")]
        public int orderId { get; set; }

        [Column("ocustomerid")]
        public int? studentId { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("odate")]
        public DateTime? orderDate { get; set; }

        [Column("ordernumber")]
        public string orderNumber { get; set; }

        [Column("ocomment")]
        public string orderComment { get; set; }

        [Column("oreason")]
        public string orderReason { get; set; }

        [Column("oshipstate")]
        public string orderShipState { get; set; }

        [Column("oshipcountry")]
        public string orderShipCountry { get; set; }

        [Column("opaymethod")]
        public int? orderPaymentMethod { get; set; }

        [Column("orderamount")]
        public Decimal? orderAmount { get; set; }

        [Column("order_cancel_date")]
        public DateTime? orderCancelDate { get; set; }

        [Column("oshippeddate")]
        public DateTime? orderShippedDate { get; set; }

        [Column("oshipmethod")]
        public int? orderShipMethod { get; set; }

        [Column("ocardtype")]
        public string orderCardType { get; set; }

        [Column("ocardno")]
        public string orderCardNo { get; set; }

        [Column("ocardname")]
        public string orderCardName { get; set; }

        [Column("ocardexpires")]
        public DateTime? orderCardExpires { get; set; }

        [Column("ocardaddress")]
        public string orderCardAddress { get; set; }

        [Column("ordertype")]
        public string orderType { get; set; }

        [Column("bag")]
        public string bag { get; set; }

        [Column("bagprice")]
        public Decimal? bagPrice { get; set; }

        [Column("order_status")]
        public string orderStatus { get; set; }

        [Column("order_ref")]
        public string orderRef { get; set; }

        [Column("order_field1")]
        public int? orderField1 { get; set; }

        [Column("parentid")]
        public int? parentId { get; set; }

        [Column("school_model")]
        public int? schoolModel { get; set; }

        [Column("school_plan")]
        public int? schoolPlan { get; set; }

        [Column("parent_plan")]
        public int? parentPlan { get; set; }

        [Column("order_cancel")]
        public bool? orderCancel { get; set; }

        [Column("createdon")]
        public DateTime? createdOn { get; set; }

        [Column("current_total")]
        public Decimal? current_total { get; set; }

        //[Column("CreatedOnSydney")]
        //public DateTime? createdOnSydney { get; set; }

    }
}