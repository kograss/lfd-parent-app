﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("roster_closures")]
    public class RosterClosures
    {
        [Key]
        [Column("closedid")]
        public int closedId { get; set; }

        [Column("closedayint")]
        public int? closeDayInt { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("closeday")]
        public string closeDay { get; set; }

        [Column("extra2")]
        public string extra2 { get; set; }
    }
}