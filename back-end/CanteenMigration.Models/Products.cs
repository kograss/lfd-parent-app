﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("products")]
    public class Product
    {
        [Key]
        [Column("catalogID")]
        public int id { get; set; }

        [Column("ccode")]
        public string mealCode { get; set; }

        [Column("cname")]
        public string productName { get; set; }

        [Column("cdescription")]
        public string productDescription { get; set; }

        [Column("cprice")]
        public Decimal? productPrice { get; set; }

        [Column("cimageurl")]
        public string productImageURL { get; set; }

        [Column("cdateavailable")]
        public DateTime? productDateAvailable { get; set; }

        [Column("cstock")]
        public int? isAvailableToOrder { get; set; }

        [Column("ccategory")]
        public int? productCategory { get; set; }

        [Column("optgID")]
        public int? optGId { get; set; }

        [Column("optID2")]
        public int? optId2 { get; set; }

        [Column("Fork")]
        public bool? fork { get; set; }

        [Column("Spoon")]
        public bool? spoon { get; set; }

        [Column("Tspoon")]
        public bool? tSpoon { get; set; }

        [Column("Knife")]
        public bool? knife { get; set; }

        [Column("Serviette")]
        public bool? serviette { get; set; }

        [Column("Straw")]
        public bool? straw { get; set; }

        [Column("Sauce")]
        public bool? sauce { get; set; }

        [Column("Stick")]
        public bool? stick { get; set; }

        [Column("pickingtype")]
        public int? pickingType { get; set; }

        [Column("exta1")]
        public string exta1 { get; set; }

        [Column("extra2")]
        public string extra2 { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("healthcategory")]
        public int? healthCategory { get; set; }

        [Column("special_event_id")]
        public int? specialEventId { get; set; }

        [Column("ctemperature")]
        public string cTemperature { get; set; }

        [Column("food_label")]
        public string foodLabel { get; set; }

        [Column("extra1")]
        public int? extra1 { get; set; }

        [Column("ProductClassification")]
        public string productClassification { get; set; }

        [Column("CreatedOn")]
        public DateTime? createdOn { get; set; }

        [Column("LastModified")]
        public DateTime? lastModified { get; set; }

        [Column("DeletedOn")]
        public DateTime? deletedOn { get; set; }

        [Column("quantity_limit")]
        public int quantityLimit { get; set; }

        [Column("stock")]
        public int? stock { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

    }
}