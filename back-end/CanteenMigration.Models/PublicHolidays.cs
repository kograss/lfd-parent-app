﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("PUBLIC_HOLS")]
    public class PublicHolidays
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("HolDateStart")]
        public DateTime? holidayDateStart { get; set; }

        [Column("HoldateEnd")]
        public DateTime? holidayDateEnd { get; set; }

        [Column("Holiday")]
        public string holiday { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }
    }
}