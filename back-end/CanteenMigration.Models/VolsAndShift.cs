﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Roster.Models
{
    [Table("volsandshifts")]
    public class VolsAndShift
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("parentid")]
        public Nullable<int> ParentId { get; set; }

        [Column("schoolid")]
        public Nullable<int> SchoolId { get; set; }

        [Column("firstname")]
        public string FirstName { get; set; }

        [Column("lastname")]
        public string LastName { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        [Column("mobile")]
        public string Mobile { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("shiftid")]
        public Nullable<int> ShiftId { get; set; }

        [Column("shiftname")]
        public string ShiftName { get; set; }

        [Column("day2work")]
        public Nullable<System.DateTime> Day2Work { get; set; }

        [Column("allday")]
        public Nullable<bool> AllDay { get; set; }

        [Column("extra1")]
        public string Extra1 { get; set; }

        [Column("extra2")]
        public string Extra2 { get; set; }

        [Column("extra3")]
        public Nullable<int> Extra3 { get; set; }

        [Column("volid")]
        public Nullable<int> Volid { get; set; }

        /*[Column("ReminderEmailId")]
        public Nullable<int> ReminderEmailId { get; set; }*/
    }
}