﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CanteenMigration.Models
{
    [Table("topups")]
    public class Topup
    {
        //public const string TOPUP_METHOD_STRIPE_QP = "STRIPE";
        public const string TOPUP_METHOD_STRIPE_QP = "MSA"; //though it is STRIPE, we have to add entry as MSA due to reports in .asp
        [Key]
        [Column("topid")]
        public int id { get; set; }

        [Column("pid")]
        public int parentId { get; set; }

        [Column("schoolid")]
        public int schoolId { get; set; }

        [Column("tdate")]
        public DateTime? transactionDate { get; set; }

        [Column("creditleft")]
        public Decimal? creditLeft { get; set; }

        [Column("topupamount")]
        public Decimal? topupAmount { get; set; }

        [Column("topupmethod")]
        public String topupMethod { get; set; }

        [Column("extra1")]
        public String extra1 { get; set; }

        [Column("extra2")]
        public String parentEmail { get; set; }

        [Column("extra3")]
        public String extra3 { get; set; }

        [Column("extra4")]
        public String extra4 { get; set; }

        [Column("txn_id")]
        public String transactionId { get; set; }

        [Column("gross_amnt")]
        public Decimal? grossAmount { get; set; }

        [Column("bank_fee")]
        public Decimal? bankFee { get; set; }

        [Column("s24_fee")]
        public Decimal? school24Fee { get; set; }

        [Column("firstname")]
        public String parentFirstName { get; set; }

        [Column("lastname")]
        public String parentLastName { get; set; }

        [Column("token")]
        public String token { get; set; }

        [Column("topupStatus")]
        public String topupStatus { get; set; }
    }
}