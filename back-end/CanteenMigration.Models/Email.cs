﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("Emails")]
    public class Email
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("To")]
        public string to { get; set; }

	    [Column("Subject")]
        public string subject { get; set; }

	    [Column("CC")]
        public string cc { get; set; }

	    [Column("BCC")]
        public string bcc { get; set; }

	    [Column("MessageBody")]
        public string messageBody { get; set; }
	    
        [Column("AddedDateTime")]
        public DateTime addedDateTime { get; set; }
	    
        [Column("SentDateTime")]
        public DateTime? sentDateTime { get; set; }
	    
        [Column("SendStatus")]
        public string sendStatus { get; set; }
	    
        [Column("FailedEmailSendAtteptCount")]
        public int failedEmailSendAtteptCount { get; set; }
	    
        [Column("Error")]
        public int? error { get; set; }
	    
        [Column("OrderId")]
        public string orderId { get; set; }
	    
        [Column("EmailCategoryType")]
        public string emailCategoryType { get; set; }
    }
}