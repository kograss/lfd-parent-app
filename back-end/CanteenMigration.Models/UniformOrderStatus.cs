﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace CanteenMigration.Models
{
    [Table("uniform_OrderStatus")]
    public class UniformOrderStatus
    {
        [Key]
        [Column("idOrderStatus")]
        public int OrderStatusId { get; set; }

        [Column("idOrder")]
        public int? OrderId { get; set; }

        [Column("idStage")]
        public int? StageId { get; set; }

        [Column("dtShipped")]
        public DateTime? ShippedDate { get; set; }

        [Column("dtFulfilled")]
        public DateTime? FulfilledDate { get; set; }

        [Column("dtProcessed")]
        public DateTime? PrcessedDate { get; set; }

        [Column("txtNotes")]
        public string Notes { get; set; }

        [Column("chrShippingNum")]
        public String ShippingNumber { get; set; }

        [Column("intProcessed")]
        public int? Processed { get; set; }

        [Column("schoolid")]
        public int? SchooId { get; set; }

        [Column("tracknumber")]
        public String TrackNumber { get; set; }
    }
}
