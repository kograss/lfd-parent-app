﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("Categories")]
    public class Category
    {
        [Key]
        [Column("categoryID")]
        public int id { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("catdescription")]
        public string categoryDescription { get; set; }

        [Column("oday")]
        public string oDay { get; set; }

        [Column("obe4day")]
        public string obeForDay { get; set; }

        [Column("cstatus")]
        public int? categoryStatus { get; set; }

        [Column("startshowdate")]
        public DateTime? startShowDate { get; set; }

        [Column("endshowdate")]
        public DateTime? endShowDate { get; set; }

        [Column("ordstart")]
        public DateTime? orderStart { get; set; }

        [Column("ordend")]
        public DateTime? orderEnd { get; set; }

        [Column("cattype")]
        public int? categoryType { get; set; }

        [Column("hd")]
        public int? hd { get; set; }

        [Column("hh")]
        public int? hh { get; set; }

        [Column("hm")]
        public int? hm { get; set; }

        [Column("sd")]
        public int? sd { get; set; }

        [Column("Sh")]
        public int? sh { get; set; }

        [Column("Sm")]
        public int? sm { get; set; }

        [Column("is_hide")]
        public bool? isHide { get; set; }

        [Column("catID_copy")]
        public int? categoryIdCopy { get; set; }

        [Column("staff_only")]
        public bool? isStaffOnly { get; set; }

        [Column("catPosition")]
        public int? categoryPosition { get; set; }

        [Column("daily_hour")]
        public int? dailyHour { get; set; }

        [Column("daily_min")]
        public int? dailyMin { get; set; }

        [Column("cut_days_before")]
        public int? cutDaysBefore { get; set; }

        [Column("cutoff_type")]
        public int? cutOffType { get; set; }
        
    }
}