﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace CanteenMigration.Models
{
    [Table("uniform_basketitem")]
    public class UniformBasketItem
    {
        [Key]
        [Column("idBasketItem")]
        public int id { get; set; }

        [Column("idProduct")]
        public int? productId { get; set; }

        [Column("intPrice")]
        public Decimal price { get; set; }

        [Column("chrName")]
        public String name { get; set; }

        [Column("intQuantity")]
        public int? quantity { get; set; }

        [Column("idBasket")]
        public int? basketId { get; set; }

        [Column("chrSize")]
        public string size { get; set; }

        [Column("chrColor")]
        public string color { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("parentid")]
        public int? parentId { get; set; }

        [Column("isUniformPack")]
        public int? isUniformPack { get; set; }

        public List<UniformPackItemProduct> uniformPackItemProducts { get; set; }
    }
}
