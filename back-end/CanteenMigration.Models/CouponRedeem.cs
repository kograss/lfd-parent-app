﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("CouponRedeem")]
    public class CouponRedeem
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("couponId")]
        public int couponId { get; set; }

        [Column("parentId")]
        public int? parentId { get; set; }

        [Column("RedeemDate")]
        public DateTime? redeemDate { get; set; }
    }
}
