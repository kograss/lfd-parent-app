﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("CanteenProductAvailability")]
    public class CanteenProductAvailability
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("schoolId")]
        public int schoolId { get; set; }

        [Column("fromDate")]
        public DateTime? fromDate { get; set; }

        [Column("toDate")]
        public DateTime? toDate { get; set; }

        [Column("productId")]
        public int? productId { get; set; }

        [Column("description")]
        public string description { get; set; }
    }
}
