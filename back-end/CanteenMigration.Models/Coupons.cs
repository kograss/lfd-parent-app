﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("Coupons")]
    public class Coupons
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("CouponName")]
        public string CouponName { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("couponPrice")]
        public Decimal? couponPrice { get; set; }

        [Column("expiryDate")]
        public DateTime? expiryDate { get; set; }

        [Column("status")]
        public bool? status { get; set; }

        [Column("parents")]
        public string parents { get; set; }
    }
}
