﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_AttributeCategory")]
    public class UniformAttributeCategory
    {
        [Key]
        [Column("idAttributeCategory")]
        public int Id { get; set; }

        [Column("chrCategoryName")]
        public string categoryName { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

    }
}
