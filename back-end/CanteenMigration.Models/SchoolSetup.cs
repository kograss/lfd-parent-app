﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("schoolsetup")]
    public class SchoolSetup
    {
        [Key]
        [Column("skl")]
        public int id { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("cmgrfname")]
        public string managerFirstName { get; set; }

        [Column("cmgrlname")]
        public string managerLastName { get; set; }
	    
        [Column("cmgrPhone")]
        public string managerPhone { get; set; }
	    
        [Column("cmgrmobile")]
        public string managerMobile { get; set; }
	    
        [Column("cmgremail")]
        public string managerEmail { get; set; }

        [Column("cacctfname")]
        public string canteenAccountFirstName { get; set; }
	    
        [Column("shop")]
        public string shop { get; set; }
	    
        [Column("cacctphone")]
        public string canteenAccountPhone { get; set; }
	    
        [Column("cacctmobile")]
        public string canteenAccountMobile { get; set; }
	    
        [Column("cacctemail")]
        public string canteenAccountEmail { get; set; }
	    
        [Column("cbank")]
        public string canteenBank { get; set; }
	    
        [Column("cbankaccount")]
        public string canteenBankAccount { get; set; }
	    
        [Column("cbankbsb")]
        public string canteenBankBSB { get; set; }
	    
        [Column("paymentmethod1")]
        public string paymentMethod1 { get; set; }
	    
        [Column("paymentmethod2")]
        public string paymentMethod2 { get; set; }

	    [Column("paymentmethod3")]
        public string paymentMethod3 { get; set; }

	    [Column("comment1")]
        public string comment1 { get; set; }

	    [Column("comment2")]
        public string comment2 { get; set; }

        [Column("meal1")]
        public string meal1 { get; set; }

        [Column("meal2")]
        public string meal2 { get; set; }

        [Column("meal3")]
        public string meal3 { get; set; }
	    
	    [Column("themeid")]
        public int? themeID { get; set; }

        [Column("chargeforbag")]
        public string chargeForBag { get; set; }
	    
        [Column("bagprice")]
        public Decimal? bagPrice { get; set; }
	    
        [Column("chargeforbaglunch")]
        public string chargeForBagLunch { get; set; }
	    
        [Column("bagpricelunch")]
        public Decimal? bagPriceLunch { get; set; }

	    [Column("roster")]
        public string roster { get; set; }
	    
        [Column("roster_status")]
        public bool? rosterStatus { get; set; }
	    
        [Column("label_printer")]
        public string labelPrinter { get; set; }
	    
        [Column("admin_site")]
        public string adminSite { get; set; }

        [Column("uniform_status")]
        public bool? uniformStatus { get; set; }
	    
        [Column("allow_recycle")]
        public bool? allowRecycle { get; set; }
	    
        [Column("recycleprice")]
        public Decimal? recyclePrice { get; set; }
	    
        [Column("paypal")]
        public bool? payPal { get; set; }
	    
        [Column("manual_topup")]
        public bool? isManualTopup { get; set; }
	    
        [Column("parents_version")]
        public string parentsVersion { get; set; }
	    
        [Column("topup_method")]
        public string topupMethod { get; set; }
	    
        [Column("topup_alert")]
        public bool? isTopupAlert { get; set; }
	    
        [Column("canteen_status")]
        public bool? canteenStatus { get; set; }
	    
        [Column("uniform_open")]
        public bool? uniformOpen { get; set; }
	    
        [Column("label_location")]
        public string labelLocation { get; set; }
	    
        [Column("quick_order")]
        public bool? quickOrder { get; set; }
	    
        [Column("lunch")]
        public bool? isLunchOpen { get; set; }
	    
        [Column("recess")]
        public bool? isRecessOpen { get; set; }
	    
        [Column("extra_meal")]
        public bool? extraMeal { get; set; }
	    
        [Column("s_fee_plan")]
        public int? sFeePlan { get; set; }
	    
        [Column("fee_model")]
        public int? feeModel { get; set; }
	    
        [Column("cancel_event_order")]
        public bool? cancelEventOrder { get; set; }
	    
        [Column("cbankaccountname")]
        public string canteenBankAccountName { get; set; }
	    
        [Column("bag_policy")]
        public string bagPolicy { get; set; }
	    
        [Column("bag_policy_lunch")]
        public string bagPolicyLunch { get; set; }
	    
        [Column("has_event_roster")]
        public bool? hasEventRoster { get; set; }
	    
        [Column("cbankswiftcode")]
        public string canteenBankSwiftCode { get; set; }

        [Column("CanteenOrderServiceFee")]
        public decimal? canteenOrderServiceFee { get; set; }
        //[Column("change_class")]
        //public bool? ChangeClass { get; set; }

        //[Column("quartly_service_fee")]
        //public Decimal? QuartlyServiceFee { get; set; }

        //[Column("order_service_fee")]
        //public Decimal? OrderServiceFee { get; set; }

    }
}