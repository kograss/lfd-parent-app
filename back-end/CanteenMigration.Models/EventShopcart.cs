﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Models
{
    [Table("e_shopcart")]
    public class EventShopcart
    {
        [Key]
        [Column("cart_id")]
        public int id { get; set; }

        [Column("cart_pid")]
        public int? parentId { get; set; }

        [Column("cart_stid")]
        public int? studentId { get; set; }

        [Column("cart_prodid")]
        public int? productId { get; set; }

        [Column("cart_prodname")]
        public String productName { get; set; }

        [Column("cart_prodattrib")]
        public String productOptions { get; set; }

        [Column("cart_prodprice")]
        public Decimal? productPrice { get; set; }

        [Column("cart_prodqty")]
        public int? productQuantity { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("cart_extra3")]
        public String extra3 { get; set; }

        [Column("cart_delivery_day")]
        public int? deliveryDay { get; set; }

        [Column("cart_date")]
        public DateTime? cartDate { get; set; }

        [Column("cart_delivery_date")]
        public DateTime? deliveryDate { get; set; }

        [Column("cart_eventid")]
        public int? eventId { get; set; }

        [Column("additionalInputs")]
        public String additionalInputs { get; set; }

        [Column("adultscount")]
        public int? adultsCount { get; set; }

        [Column("childrencount")]
        public int? childrenCount { get; set; }

        [Column("seatnumber")]
        public String seatNumber { get; set; }

        [Column("myseatnumber")]
        public int? mySeatNumber { get; set; }
        [NotMapped]
        public string studentName { get; set; }

        [NotMapped]
        public List<CartProductAtrributes> productOptionsList { get; set; }

        [NotMapped]
        public Double productPriceWithOptionPrice { get; set; }

        [NotMapped]
        public string MealNameToDisplayInCart
        {
            get;
            set;
        }

        [NotMapped]
        public List<Option> options { get; set; }
    }
}
