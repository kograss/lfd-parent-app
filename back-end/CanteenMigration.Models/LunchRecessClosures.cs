﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("lunch_recess_closures")]
    public class LunchRecessClosures
    {
        [Key]
        [Column("Id")]
        public int id { get; set; }

        [Column("CloseDateStart")]
        public DateTime? closeDateStart { get; set; }

        [Column("ClosedateEnd")]
        public DateTime? closedateEnd { get; set; }

        [Column("ServiceToClose")]
        public string serviceToClose { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("CloseDate")]
        public DateTime? closeDate { get; set; }

    }
}