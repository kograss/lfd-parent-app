﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanteenMigration.Models
{
    [Table("productsandevents")]
    public class ProductsAndEvents
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

        [Column("food_type")]
        public string foodType { get; set; }

        [Column("description")]
        public string description { get; set; }

        [Column("extra1")]
        public string extra1 { get; set; }

        [Column("catalogID")]
        public int? catalogId { get; set; }

        [Column("eventID")]
        public int? eventId { get; set; }

        [Column("categoryID")]
        public int? categoryId { get; set; }

        [Column("stock")]
        public int? stock { get; set; }

        [Column("tstatus")]
        public bool? tStatus { get; set; }


    }
}
