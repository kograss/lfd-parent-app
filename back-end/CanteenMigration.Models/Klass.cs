﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("klass")]
    public class Klass
    {
        [Key]
        [Column("classID")]
        public int id { get; set; }

        [Column("ClassName")]
        public string className { get; set; }

        [Column("Classyear")]
        public string classYear { get; set; }

        [Column("extra1")]
        public string extra1 { get; set; }

        [Column("extra2")]
        public int? extra2 { get; set; }

        [Column("teacher1")]
        public string teacher1 { get; set; }

        [Column("teacher2")]
        public string teacher2 { get; set; }

        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("classmum")]
        public string classMum { get; set; }

        [Column("classmum_email")]
        public string classMumEmail { get; set; }

        [Column("extra3")]
        public string extra3 { get; set; }

        [Column("ClassType")]
        public string classType { get; set; }

    }
}