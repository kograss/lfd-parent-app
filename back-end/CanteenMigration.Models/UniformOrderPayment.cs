﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_payment")]
    public class UniformOrderPayment
    {
        [Key]
        [Column("topid")]
        public int Id { get; set; }

        [Column("pid")]
        public int? ParentId { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("tdate")]
        public DateTime? TransactionDate { get; set; }

        [Column("creditleft")]
        public Decimal? CreditLeft { get; set; }

        [Column("topupamount")]
        public Decimal? TopupAmount { get; set; }

        [Column("topupmethod")]
        public String TopupMethod { get; set; }

        [Column("extra1")]
        public String Extra1 { get; set; }

        [Column("extra2")]
        public String Extra2 { get; set; }

        [Column("orderid")]
        public int? OrderId { get; set; }

        [Column("extra4")]
        public String Extra4 { get; set; }

        [Column("txn_id")]
        public String TransactionId { get; set; }

        [Column("gross_amnt")]
        public Decimal? GrossAmount { get; set; }

        [Column("bank_fee")]
        public Decimal? BankFee { get; set; }

        [Column("s24_fee")]
        public Decimal? School24Fee { get; set; }

        [Column("firstname")]
        public String FirstName { get; set; }

        [Column("lastname")]
        public String LastName { get; set; }

        [Column("phone")]
        public String Phone { get; set; }

        [Column("orderamount")]
        public Decimal OrderAmount { get; set; }

        [Column("payment_status")]
        public int PaymentStatus { get; set; }
    }
}
