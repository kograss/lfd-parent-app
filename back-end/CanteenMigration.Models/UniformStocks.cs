﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CanteenMigration.Models
{
    [Table("uniform_stocks")]
    public class UniformStocks
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("SizeAttributeId")]
        public int? SizeAttributeId { get; set; }

        [Column("ColorAttributeId")]
        public int? ColorAttributeId { get; set; }

        [Column("idProduct")]
        public int? ProductId { get; set; }

        [Column("schoolid")]
        public int? SchoolId { get; set; }

        [Column("AvailableCount")]
        public int? AvailableCount { get; set; }
    }
}
