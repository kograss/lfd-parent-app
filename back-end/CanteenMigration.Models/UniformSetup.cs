﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniformsetup")]
    public class UniformSetup
    {
        [Key]
        [Column("skl")]
        public int skl { get; set; }

        [Column("schoolid")]
        public int schoolId { get; set; }

        [Column("cmgrfname")]
        public string cmgrfname { get; set; }

        [Column("cmgremail")]
        public string cmgremail { get; set; }

        [Column("topup_method")]
        public string topupmethod { get; set; }

         [Column("cbank")]
        public string cbank { get; set; }

         [Column("cbankaccount")]
         public string cbankaccount { get; set; }

         [Column("cbankbsb")]
         public string cbankbsb { get; set; }

          [Column("shop")]
         public string shop { get; set; }

          [Column("cacctemail")]
          public string cacctemail { get; set; }


        [Column("isStudentOptional")]
        public bool? isStudentOptional { get; set; }

        [Column("isExternalRecipientRequired")]
        public bool? isExternalRecipientRequired { get; set; }

        [Column("isPayUsingBalanceEnabled")]
        public bool? isPayUsingBalanceEnabled { get; set; }

        // delivery
        [Column("class_delivery")]
        public bool? classDelivery { get; set; }

        [Column("class_delivery_fee")]
        public decimal? classDeliveryFee { get; set; }

        [Column("home_delivery")]
        public bool? homeDelivery { get; set; }

        [Column("home_delivery_fee")]
        public decimal? homeDeliveryFee { get; set; }

        [Column("collect_from_shop")]
        public bool? collectFromShop { get; set; }

    }
}
