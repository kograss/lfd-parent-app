﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniform_pack_product")]
    public class UniformPackProduct
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Column("productName")]
        public string name { get; set; }

        [Column("productId")]
        public int productId { get; set; }

        [Column("quantity")]
        public int quantity { get; set; }

        [Column("uniformPackId")]
        public int uniformPackId { get; set; }
    }
}
