﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace Roster.Models
{
    [Table("roster_news")]
    public class RosterNews
    {
        [Key]
        [Column("NewsID")]
        public int NewsId { get; set; }

        [Column("Description")]
        public string Description { get; set; }

        [Column("DateEntered")]
        public Nullable<System.DateTime> DateEntered { get; set; }

        [Column("Subject")]
        public string Subject { get; set; }

        [Column("Priority")]
        public string Priority { get; set; }

        [Column("EnteredBy")]
        public string EnteredBy { get; set; }

        [Column("dayid")]
        public string DayId { get; set; }

        [Column("Status")]
        public string Status { get; set; }

        [Column("Comment")]
        public string Comment { get; set; }

        [Column("schoolid")]
        public Nullable<int> SchoolId { get; set; }

        [Column("date_edit")]
        public Nullable<System.DateTime> DateEdit { get; set; }
    }
}