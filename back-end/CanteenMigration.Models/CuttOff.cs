﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("cuttoff")]
    public class CuttOff
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Column("daily_hour")]
        public int? dailyHour { get; set; }
        
        [Column("daily_min")]
        public int? dailyMin { get; set; }
	    
        [Column("sushi_day")]
        public int? sushiDay { get; set; }
	    
        [Column("sushi_for_day")]
        public int? sushiForDay { get; set; }
	    
        [Column("sushi_hour")]
        public int? sushiHour { get; set; }
	    
        [Column("sushi_min")]
        public int? sushiMin { get; set; }
	    
        [Column("extra1")]
        public string extra1 { get; set; }
	    
        [Column("schoolid")]
        public int? schoolId { get; set; }
	    
        [Column("productid")]
        public int? productId { get; set; }
	    
        [Column("cut_day")]
        public int? cutDay { get; set; }
	    
        [Column("open_howmany")]
        public int? openHowMany { get; set; }
	    
        [Column("cut_days_before")]
        public int? cutDaysBefore { get; set; }
	    
        [Column("order_day")]
        public int? orderDay { get; set; }
	}
}