﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CanteenMigration.Models
{
    [Table("c_shopcart")]
    public class CanteenShopcartItem
    {
        [Key]
        [Column("cart_id")]
        public int id { get; set; }
        
        [Column("cart_pid")]
        public int? parentId { get; set; }

        [Column("cart_stid")]
        public int? studentId { get; set; }

        [Column("cart_prodid")]
        public int? productId { get; set; }

        [Column("cart_prodname")]
        public string productName { get; set; }

        [Column("cart_prodattrib")]
        public string productOptions { get; set; }

        [Column("cart_prodprice")]
        public Decimal? productPrice { get; set; }

        [Column("cart_prodqty")]
        public int? productQuantity { get; set; }
    
        [Column("schoolid")]
        public int? schoolId { get; set; }

        [Column("cart_extra3")]
        public string mealName { get; set; }

        [Column("cart_delivery_day")]
        public int? cartDeliveryDay { get; set; }
        
        [Column("cart_date")]
        public DateTime? cartDate { get; set; }

        [Column("cart_delivery_date")]
        public DateTime? cartDeliveryDate { get; set; }

        [Column("cart_eventid")]
        public int? eventId { get; set; }

        [NotMapped]
        public string studentName { get; set; }

        [NotMapped]
        public List<CartProductAtrributes> productOptionsList { get; set; }

        [NotMapped]
        public Double productPriceWithOptionPrice { get; set; }

        [NotMapped]
        public string MealNameToDisplayInCart
        {
            get;
            set;
        }

        [NotMapped]
        public List<Option> options { get; set; }
    }

    public class CartProductAtrributes
    {
       

        public CartProductAtrributes(string optionName, double? optionPrice)
        {
            this.optionName = optionName;
            this.optionPrice = optionPrice;
        }

        public string optionName { get; set; }
        public Double? optionPrice { get; set; }

    }
}