﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("parents_self_topup")]
    public class ParentSelfTopup
    {
        [Key]
        [Column("stpid")]
        public int? stpId { get; set; }

        [Column("ParentID")]
        public int? parentId { get; set; }

        [Column("SchoolID")]
        public int? schoolId { get; set; }

        [Column("Amount")]
        public Decimal? amount { get; set; }

        [Column("Method_pay")]
        public string methodPay { get; set; }

        [Column("date_submit")]
        public DateTime? dateSubmit { get; set; }


        [Column("date_you_paid")]
        public string dateYouPaid { get; set; }


        [Column("approved")]
        public string approved { get; set; }


        [Column("admin_action")]
        public string adminAction { get; set; }


        [Column("date_action")]
        public DateTime? dateAction { get; set; }

        [Column("comment")]
        public string comment { get; set; }



    }
}
