﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CanteenMigration.Models
{
    [Table("uniformadmins")]
    public class UniformAdmin
    {
        [Key]
        [Column("adminid")]
        public int adminid { get; set; }

        [Column("firstname")]
        public string firstname { get; set; }

        [Column("lastname")]
        public string lastname { get; set; }

        [Column("email")]
        public string email { get; set; }

        [Column("schoolId")]
        public int? schoolId { get; set; }

    }
}
