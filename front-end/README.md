# School24Canteen

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Angular calendar issue:
Add below line to node_modules\angular-calendar\dist\esm\src\index.d.ts. and index.ts
export * from './components/month/calendarMonthCell.component';

## links
https://medium.com/@christof.thalmann/convert-angular-project-to-android-apk-in-10-steps-c49e2fddd29
https://medium.com/@EliaPalme/how-to-wrap-an-angular-app-with-apache-cordova-909024a25d79

## Add platform
cordova platform add android

## Remove platform
cordova platform remove android

## Build debug apk:
ng build --prod --aot
cordova build android

## Build release apk:
cordova build --release android

## Generate key store:
keytool -genkey -v -keystore school24.keystore -alias school24 -keyalg RSA -keysize 2048 -validity 10000
Schoo0987l24K^^yP@s0or897
Abder Bloul

## jar  
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore school24.keystore app-release-unsigned.apk school24

## zip
cd F:\android-sdk-windows\build-tools\29.0.2
zipalign -v 4 app-release-unsigned.apk school24.apk


## iOS:
cordova platform add ios
cordova emulate ios
ng build --prod --aot
cordova build ios
Xcode -> product -> archive --> distribute --> ad hoc --> save .ipk

