import { PaymentService } from './payment-service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as TakePaymentActions from './actions/take-payment-action';
import * as CheckoutActions from '../checkout/actions/checkout-actions';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class PaymentEffects {

  constructor(
    private actions$: Actions,
    private paymentService: PaymentService
  ) { }

  @Effect()
  takePayment$: Observable<Action> = this.actions$.pipe(ofType<TakePaymentActions.TakePayment>
    (TakePaymentActions.TAKEPAYMENT)
    , map(action => action)
    , switchMap(
      payload => this.paymentService.takePayment(payload.orderItems, payload.amount, payload.token, payload.isBalancePlusCard, payload.saveCard).pipe(
        map(results => new CheckoutActions.PlaceOrderSuccess(results))
        , catchError(err =>
          of(new CheckoutActions.PlaceOrderFailure(err))
        )))
  );
}