import { OrderResult } from './../../checkout/model/order-result';
import { OrderDetails } from '../../checkout/model/oreder-details';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const TAKEPAYMENT = 'TAKE PAYMENT ACTION';
export const TAKEPAYMENT_SUCCESS = 'TAKE PAYMENT SUCCESS';
export const TAKEPAYMENT_FAILURE = 'TAKE PAYMENT FAILURE';
export const CLEARPAYMENTRESULT = 'CLEAR PAYMENT RESULT';



export class TakePayment {
  amount: number
  token: any
  orderItems: OrderDetails[];
  isBalancePlusCard: boolean;
  saveCard: boolean;

  readonly type = TAKEPAYMENT;
  constructor(orderItems: OrderDetails[], amount, token, isBalancePlusCard: boolean, saveCard: boolean) {
    this.amount = amount;
    this.token = token;
    this.orderItems = orderItems;
    this.isBalancePlusCard = isBalancePlusCard;
    this.saveCard = saveCard;
  }
}

export class TakePaymentSuccess implements Action {
  readonly type = TAKEPAYMENT_SUCCESS;
  constructor(public orderResult: OrderResult) {
  }

}

export class TakePaymentFailure implements Action {
  readonly type = TAKEPAYMENT_FAILURE;
  constructor(public err: any) {
  }
}

export class ClearPaymentResult implements Action {
  readonly type = CLEARPAYMENTRESULT;
  constructor() {
  }

}


export type All = TakePayment
  | TakePaymentSuccess | TakePaymentFailure | ClearPaymentResult
