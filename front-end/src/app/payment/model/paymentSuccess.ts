
export class PaymentSuccess{
    grandTotal:number;
    isPaymentCompleted:boolean;
    message:string;
    orderId:number;
}