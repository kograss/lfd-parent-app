import { OrderDetails } from './../checkout/model/oreder-details';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable()
export class PaymentService {
    public _stripePaymentUrl = 'PayAndPlaceOrder';
    public _stripePaymentIntentUrl = 'GetPaymentIntent';
    public _schoolEventStripePaymentIntentUrl = 'SchoolEvent/GetPaymentIntent';
    public _schoolEventPlaceOrderAfterPaymentUrl = 'SchoolEvent/placeOrderAfterPayment';
    public _canteenEventStripePaymentIntentUrl = 'Events/GetPaymentIntent';
    public _canteenEventPlaceOrderAfterPaymentUrl = 'Events/placeOrderAfterPayment';
    public _placeOrderAfterPaymentUrl = 'placeOrderAfterPayment';
    public _stripeCustomerDetailsUrl = 'GetCustomerStripeDetails';
    public _canteenEventStripePaymentUrl = "Events/PayAndPlaceOrder";
    public _schoolEventStripePaymentUrl = "SchoolEvent/PayAndPlaceOrder";

    constructor(private _http: HttpClient) { }
    
    takePayment(orderItems: OrderDetails[], amount: number, token: any, isBalancePlusCard: boolean, saveCard: boolean) {
        let eventType = localStorage.getItem('eventType');
        let body = {
            orderItems: orderItems,
            stripeToken: token.id,
            amount: amount,
            isBalancePlusCard: isBalancePlusCard,
            saveCard: saveCard
        };
        let bodyString = JSON.stringify(body);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        let orderType = localStorage.getItem('orderType');
        if (orderType == 'event') {
            if (eventType == 'CANTEEN') {
                return this._http.post(this._canteenEventStripePaymentUrl, bodyString, options)
                    ;
            }
            else if (eventType == 'SCHOOL') {
                return this._http.post(this._schoolEventStripePaymentUrl, bodyString, options)
                    ;
            }
        }
        else {
            return this._http.post(this._stripePaymentUrl, bodyString, options);
        }
    }

    GetPaymentIntent(orderItems: OrderDetails[], amount: number, isBalancePlusCard: boolean) {
        let eventType = localStorage.getItem('eventType');
        let body = {
            orderItems: orderItems,
            stripeToken: null,
            amount: amount,
            isBalancePlusCard: isBalancePlusCard
        };
        let bodyString = JSON.stringify(body);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        let orderType = localStorage.getItem('orderType');
        if (orderType == 'event') {
            if (eventType == 'CANTEEN') {
                return this._http.post(this._canteenEventStripePaymentIntentUrl, bodyString, options)
                    ;
            }
            else if (eventType == 'SCHOOL') {
                return this._http.post(this._schoolEventStripePaymentIntentUrl, bodyString, options)
                    ;
            }
        }
        else {
            return this._http.post(this._stripePaymentIntentUrl, bodyString, options);
        }
       
    }

    placeOrderAfterPayment(orderItems: OrderDetails[], amount: number, isBalancePlusCard: boolean, transLog: any){
        let eventType = localStorage.getItem('eventType');
        let req = {
            orderItems: orderItems,
            stripeToken: null,
            amount: amount,
            isBalancePlusCard: isBalancePlusCard,
            
        };
        let body = {
            paymentRequest: req,
            log: transLog,
        }
        
        let bodyString = JSON.stringify(body);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        let orderType = localStorage.getItem('orderType');
        if (orderType == 'event') {
            if (eventType == 'CANTEEN') {
                return this._http.post(this._canteenEventPlaceOrderAfterPaymentUrl, bodyString, options)
                    ;
            }
            else if (eventType == 'SCHOOL') {
                return this._http.post(this._schoolEventPlaceOrderAfterPaymentUrl, bodyString, options)
                    ;
            }
        }
        else {
            return this._http.post(this._placeOrderAfterPaymentUrl, bodyString, options);
        }
        
        
    }

    GetCustomerStripeDetails() {
        return this._http.get(this._stripeCustomerDetailsUrl)
    }
}