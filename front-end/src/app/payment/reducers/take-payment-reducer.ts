import { OrderResult } from './../../checkout/model/order-result';
import { PaymentSuccess } from './../model/paymentSuccess';
import * as PaymentAction from '../actions/take-payment-action'

export interface State {
    result: OrderResult,
    err: any
}

const initialState: State = {
    result:new OrderResult(),
    err: {}
};

export function reducer(state = initialState, action: PaymentAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case PaymentAction.TAKEPAYMENT_SUCCESS: {
            return {
                ...state,
                result: action.orderResult
            }
        }
        case PaymentAction.TAKEPAYMENT_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        case PaymentAction.CLEARPAYMENTRESULT: {
            return {
                ...state,
                result: new OrderResult()
            }
        }

        default: {
            return state;
        }

    }

}
