import { OrderHistoryService } from './order-history.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListOrderHistoryActions from './actions/list-order-history.action';
import * as CancelOrderActions from './actions/cancel-order.action';
import * as FavouriteOrdreActions from './actions/favourite-actions.action';
import * as RemoveFavouriteActions from './actions/remove-favourite.actions';
import * as RepeatOrderActions from './actions/repeat-order.actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class OrderHistoryEffects {

    constructor(
        private actions$: Actions,
        private orderHistoryService: OrderHistoryService
    ) { }

    @Effect()
    listOrderHistory$: Observable<Action> = this.actions$.pipe(ofType<ListOrderHistoryActions.ListOrderHistory>
        (ListOrderHistoryActions.LISTORDERHISTORY)
        , map(action => action)
        , switchMap(
            payload => this.orderHistoryService.getOrderHistory().pipe(
                map(results => new ListOrderHistoryActions.ListOrderHistorySuccess(results))
                , catchError(err =>
                    of({ type: ListOrderHistoryActions.LISTORDERHISTORY_FAILURE, exception: err })
                )))
    );

    @Effect()
    cancelOrder$: Observable<Action> = this.actions$.pipe(ofType<CancelOrderActions.CancelOrder>
        (CancelOrderActions.CANCELORDER)
        , map(action => action)
        , switchMap(
            payload => this.orderHistoryService.cancelOrder(payload.orderId).pipe(
                map(results => new CancelOrderActions.CancelOrderSuccess(results))
                , catchError(err =>
                    of({ type: CancelOrderActions.CANCELORDER_FAILURE, exception: err })
                )))
    );

    @Effect()
    setFavouriteOrder$: Observable<Action> = this.actions$.pipe(ofType<FavouriteOrdreActions.FavOrder>
        (FavouriteOrdreActions.FAVORDER)
        , map(action => action)
        , switchMap(
            payload => this.orderHistoryService.favouriteOrder(payload.orderId, payload.favourite).pipe(
                map(results => new FavouriteOrdreActions.FavOrderSuccess(results))
                , catchError(err =>
                    of({ type: FavouriteOrdreActions.FAVORDER_FAILURE, exception: err })
                )))
    );

    @Effect()
    removeFavourite$: Observable<Action> = this.actions$.pipe(ofType<RemoveFavouriteActions.RemoveFav>
        (RemoveFavouriteActions.REMOVEFAV)
        , map(action => action)
        , switchMap(
            payload => this.orderHistoryService.favouriteOrder(payload.orderId, payload.favourite).pipe(
                map(results => new RemoveFavouriteActions.RemoveFavSuccess(results))
                , catchError(err =>
                    of({ type: RemoveFavouriteActions.REMOVEFAV_FAILURE, exception: err })
                )))
    );

    @Effect()
    canRepeat$: Observable<Action> = this.actions$.pipe(ofType<RepeatOrderActions.RepeatOrder>
        (RepeatOrderActions.REPEATORDER)
        ,map(action => action)
        ,switchMap(
            payload => this.orderHistoryService.repeatOrder(payload.orderId, payload.deliveryDate).pipe(
                map(results => new RepeatOrderActions.RepeatOrderSuccess(results)),
                catchError(err =>
                    of({ type: RepeatOrderActions.REPEATORDER_FAILURE, exception: err })
                )))
        );
}
