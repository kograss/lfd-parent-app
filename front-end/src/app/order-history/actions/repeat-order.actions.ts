import { Action } from '@ngrx/store';

export const REPEATORDER = 'REPEAT ORDER';
export const REPEATORDER_SUCCESS = '[REPEATORDER] SUCCESS';
export const REPEATORDER_FAILURE = '[REPEATORDER] FAILURE';

export class RepeatOrder {
    readonly type = REPEATORDER;
    constructor(public orderId: number, public deliveryDate: any) {
    }
}

export class RepeatOrderSuccess implements Action {

    readonly type = REPEATORDER_SUCCESS;
    constructor(public canRepeat: [Boolean, Array<string>]) {
    }

}

export class RepeatOrderFailure implements Action {

    readonly type = REPEATORDER_FAILURE;
    constructor(public err: any) {

    }
}

export type All = RepeatOrder
    | RepeatOrderSuccess | RepeatOrderFailure 