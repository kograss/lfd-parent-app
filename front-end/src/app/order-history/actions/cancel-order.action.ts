import { CancelOrderResult } from './../model/cancelOrderResult';
import { ParentOrder } from './../model/parent-order';
import { Action } from '@ngrx/store';

export const CANCELORDER = 'CANCELORDER';
export const CANCELORDER_SUCCESS = '[CANCELORDER] SUCCESS';
export const CANCELORDER_FAILURE = '[CANCELORDER] FAILURE';

export class CancelOrder {
    readonly type = CANCELORDER;
    constructor(public orderId:number) {
    }
}

export class CancelOrderSuccess implements Action {

    readonly type = CANCELORDER_SUCCESS;
    constructor(public cancelOrderResult: CancelOrderResult) {
    }

}

export class CancelOrderFailure implements Action {

    readonly type = CANCELORDER_FAILURE;
    constructor(public err: any) {

    }
}

export type All = CancelOrder
    | CancelOrderSuccess | CancelOrderFailure 