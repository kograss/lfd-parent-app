import { Action } from '@ngrx/store';

export const FAVORDER = 'FAVORDER';
export const FAVORDER_SUCCESS = '[FAVORDER] SUCCESS';
export const FAVORDER_FAILURE = '[FAVORDER] FAILURE';

export class FavOrder {
    readonly type = FAVORDER;
    constructor(public orderId:number, public favourite:boolean) {
    }
}

export class FavOrderSuccess implements Action {

    readonly type = FAVORDER_SUCCESS;
    constructor(public FavOrderResult: Boolean) {
    }

}

export class FavOrderFailure implements Action {

    readonly type = FAVORDER_FAILURE;
    constructor(public err: any) {

    }
}

export type All = FavOrder
    | FavOrderSuccess | FavOrderFailure 