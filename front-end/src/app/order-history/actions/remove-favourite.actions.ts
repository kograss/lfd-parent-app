import { Action } from '@ngrx/store';

export const REMOVEFAV = 'REMOVEFAV';
export const REMOVEFAV_SUCCESS = '[REMOVEFAV] SUCCESS';
export const REMOVEFAV_FAILURE = '[REMOVEFAV] FAILURE';

export class RemoveFav {
    readonly type = REMOVEFAV;
    constructor(public orderId:number, public favourite:boolean) {
    }
}

export class RemoveFavSuccess implements Action {

    readonly type = REMOVEFAV_SUCCESS;
    constructor(public removeFavResult: Boolean) {
    }

}

export class RemoveFavFailure implements Action {

    readonly type = REMOVEFAV_FAILURE;
    constructor(public err: any) {

    }
}

export type All = RemoveFav
    | RemoveFavSuccess | RemoveFavFailure 