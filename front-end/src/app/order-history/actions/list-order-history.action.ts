import { ParentOrder } from './../model/parent-order';
import { Action } from '@ngrx/store';

export const LISTORDERHISTORY = '[ORDERHISTORY] LIST';
export const LISTORDERHISTORY_SUCCESS = '[ORDERHISTORY] LIST SUCCESS';
export const LISTORDERHISTORY_FAILURE = '[ORDERHISTORY] LIST FAILURE';

export class ListOrderHistory {
    readonly type = LISTORDERHISTORY;
    constructor() {
    }
}

export class ListOrderHistorySuccess implements Action {

    readonly type = LISTORDERHISTORY_SUCCESS;
    constructor(public ordersHistory: ParentOrder[]) {
        if(this.ordersHistory==null){
            this.ordersHistory = [];
        }
    }

}

export class ListOrderHistoryFailure implements Action {

    readonly type = LISTORDERHISTORY_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListOrderHistory
    | ListOrderHistorySuccess | ListOrderHistoryFailure 