import { RepeatOrderItemProduct } from './../model/repeatOrderItemProduct';
import { Action } from '@ngrx/store';

export const SETREPEATORDERITEMS = 'SET REPEAT ORDER ITEMS';
export const REMOVEREPEATORDERITEMS = 'REMOVE REPEAT ORDER ITEMS';

export class SetRepeatOrderItems {
    readonly type = SETREPEATORDERITEMS;
    constructor(public repeatOrderItems:RepeatOrderItemProduct) {
    }
}

export class RemoveRepeatOrderItems implements Action {

    readonly type = REMOVEREPEATORDERITEMS;
    constructor() {
    }

}


export type All = SetRepeatOrderItems | RemoveRepeatOrderItems