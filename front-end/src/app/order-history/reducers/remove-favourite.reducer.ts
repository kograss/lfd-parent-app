import * as RemoveFavouriteActions from '../actions/remove-favourite.actions';

export interface State{
    result : Boolean,
    err:any
}

const initialState: State = {
    result : false,
    err : {}
}

export function reducer(state = initialState, action: RemoveFavouriteActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case RemoveFavouriteActions.REMOVEFAV_SUCCESS: {
            return {
                ...state,
                result: new Boolean(action.removeFavResult)
            }
        }
        case RemoveFavouriteActions.REMOVEFAV_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
