import * as RepeatOrderActions from '../actions/repeat-order.actions';

export interface State{
    result : [Boolean, Array<string>],
    err:any
}

const initialState: State = {
    result : [false, []],
    err : {}
}

export function reducer(state = initialState, action: RepeatOrderActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case RepeatOrderActions.REPEATORDER_SUCCESS: {
            return {
                ...state,
                result: action.canRepeat
            }
        }
        case RepeatOrderActions.REPEATORDER_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
