import { ParentOrder } from './../model/parent-order';
import * as ListOrderHistoryActions from '../actions/list-order-history.action';

export interface State{
    result : ParentOrder[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListOrderHistoryActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListOrderHistoryActions.LISTORDERHISTORY_SUCCESS: {
            return {
                ...state,
                result: action.ordersHistory
            }
        }
        case ListOrderHistoryActions.LISTORDERHISTORY_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
