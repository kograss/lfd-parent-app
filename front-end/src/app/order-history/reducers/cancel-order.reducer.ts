import { CancelOrderResult } from './../model/cancelOrderResult';
import * as CancelOrderActions from '../actions/cancel-order.action';

export interface State{
    result : CancelOrderResult,
    err:any
}

const initialState: State = {
    result : new CancelOrderResult,
    err : {}
}

export function reducer(state = initialState, action: CancelOrderActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case CancelOrderActions.CANCELORDER_SUCCESS: {
            return {
                ...state,
                result: action.cancelOrderResult
            }
        }
        case CancelOrderActions.CANCELORDER_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
