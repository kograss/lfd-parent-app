import { RepeatOrderItemProduct } from './../model/repeatOrderItemProduct';
import * as SetRepeatOrderProducts from './../actions/set-repeat-order-items.actions';

export interface State{
    result : RepeatOrderItemProduct,
    err:any
}

const initialState: State = {
    result : new RepeatOrderItemProduct,
    err : {}
}

export function reducer(state = initialState, action: SetRepeatOrderProducts.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case SetRepeatOrderProducts.SETREPEATORDERITEMS: {
            return {
                ...state,
                result: action.repeatOrderItems
            }
        }
        case SetRepeatOrderProducts.REMOVEREPEATORDERITEMS: {
            return {
                ...state,
                err: state.result
            }
        }    
          

        default: {
            return state;
        }

    }

}
