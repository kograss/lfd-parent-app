import * as FavouriteOrderActions from '../actions/favourite-actions.action';

export interface State{
    result : Boolean,
    err:any
}

const initialState: State = {
    result : false,
    err : {}
}

export function reducer(state = initialState, action: FavouriteOrderActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case FavouriteOrderActions.FAVORDER_SUCCESS: {
            return {
                ...state,
                result: new Boolean(action.FavOrderResult)
            }
        }
        case FavouriteOrderActions.FAVORDER_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
