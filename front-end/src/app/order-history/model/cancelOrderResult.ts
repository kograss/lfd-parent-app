export class CancelOrderResult{
    isOrderCancelled:boolean;
    message:string;
    parentCredit: number;
}