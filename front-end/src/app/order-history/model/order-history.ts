export class UniformOrderHistory {
    orderId : number;
    orderAmount : number;
    stage : number;
    parentName : string;
    parentEMail : string;
    stageStatus : string;
    comment : string;
    orderDate : string;
}