import { OrderItem } from './../../order-details/model/order-item';
import { CartResult } from './../../cart/model/cartResult';
export class RepeatOrderItemProduct {
    allOrderitemProducts: CartResult;
    errorProductOrderitemProducts: OrderItem;
    errorOptionOrderitemProducts: OrderItem;
}