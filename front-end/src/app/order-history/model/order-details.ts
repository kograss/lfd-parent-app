import { CollectionOption } from '../../uniform/cart/model/collection-option';
import { uniformPackItemProduct } from '../../uniform/cart/model/uniform_basketitem';
export class OrderDetails {
    orderId : number;
    productId : number;
    productName : string;
    sizeOptions : string;
    colorOptions : string;
    quantity : number;
    price : number;
    subTotal : number;
    comment : string;
    total : number;
    isUniformPack: number;
    basketItemId: number;
    collectionOptionType: string;
    deliveryAddress: string;
    deliveryState: string;
    deliveryPostCode: string;
    deliverySuburb: string;
    deliveryFee: number;
    uPackDetails: uniformPackItemProduct[] ;
}