export class ParentOrder {
    orderId: number;
    studentName: string;
    studentClass: string;
    orderComment: string;
    orderAmount: number;
    orderDate: Date;
    deliveryDate: Date;
    isCacelledOrder: boolean;
    isFavouriteOrder: boolean;
    isSpecialOrder: boolean;
    eventType:string;
    eventName:string;
}