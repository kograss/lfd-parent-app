import { Parent } from './../user-profile/model/parent';
import { OrderItem } from './../order-details/model/order-item';
import { AlertService } from './../alert/alertService';
import { CartResult } from './../cart/model/cartResult';
import { RepeatOrderItemProduct } from './model/repeatOrderItemProduct';
import { Holidays } from './../home/model/holidays';
import { Config } from './../config/config';
import { CancelOrderResult } from './model/cancelOrderResult';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { ParentOrder } from './model/parent-order';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListOrderHistoryActions from './actions/list-order-history.action';
import * as CancelOrderActions from './actions/cancel-order.action';
import * as  AlertActions from '../alert/actions/alert-action';
import * as FavOrderActions from './actions/favourite-actions.action';
import * as RemoveFavouriteActions from './actions/remove-favourite.actions';
import * as RepeatOrderActions from './actions/repeat-order.actions';
import { IMyDpOptions, IMyOptions } from 'mydatepicker';
import * as SetRepeatOrderActions from './actions/set-repeat-order-items.actions';
import * as ListCartActions from './../cart/actions/list-cart-items-action';
import * as ListHolidaysActions from './../home/actions/list-holidays.action';
import * as ListOrderDetailsActions from '../order-details/actions/order-details.action';
import { OrderHistoryService } from './order-history.service';
import { UniformOrderHistory } from './model/order-history';
import { OrderDetails } from './model/order-details';
import { SchoolInfo } from '../common/school-info/model/school-info';
import * as ListUserActions from './../user-profile/actions/list-user-action';
import { delay } from 'rxjs-compat/operator/delay';

@Component({
  selector: 'order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit, OnDestroy {

  message: string;

  isSpecialOrder: boolean;
  orderHistory: any = [];
  orderHistoryCopy: ParentOrder[] = [];
  showDetails: boolean = false;
  order: ParentOrder = new ParentOrder();
  filterQuery = '';
  rowsOnPage = 10;
  showSpinner: boolean;
  orderIdToCancel: number;
  currency: string;
  favOrderId: number;
  showFavourites: boolean;
  showFavouriteBtnText = "Show Favourites Only";
  holidays: Holidays;
  datePopup: boolean;
  orderDate: any;
  repeatOrderClicked: boolean;
  eventId: number = +localStorage.getItem('eventId');
  orderIdString: string;
  orderItems: OrderItem[] = [];
  printClicked: boolean;
  parent: Parent = new Parent();

  // datepicker options
  disableDateRanges: any[] = [];
  disableDays: any[] = [];
  disableWeekdays: string[] = [];
  minDate = new Date();
  d = new Date();
  currentYear = this.d.getFullYear();
  maxDate = new Date('31-11-' + this.currentYear);
  disableUntil: any = {
    year: this.minDate.getFullYear(),
    month: this.minDate.getMonth() + 1,
    day: this.minDate.getDate() - 1
  };

  disableSince: any = {
    year: this.currentYear,
    month: 12,
    day: 31
  };
  dateOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    inline: true,
    //openSelectorOnInputClick: true,
    // editableDateField: false,
    disableUntil: this.disableUntil,
    disableSince: this.disableSince,
    disableDateRanges: this.disableDateRanges,
    disableDays: this.disableDays,
    disableWeekdays: this.disableWeekdays
  };


  obsOrderHistory: Observable<ParentOrder[]>;
  subObsOrderHistory: Subscription;
  obsOrderHistoryError: Observable<any>;
  subObsOrderHistoryError: Subscription;

  obsCancelOrder: Observable<CancelOrderResult>;
  subObsCancelOrder: Subscription;
  obsCancelOrderError: Observable<any>;
  subObsCancelOrderError: Subscription;


  obsCancelConfirmAlert: Observable<Boolean>;
  subCancelConfirmAlert: Subscription;

  obsSetFavourite: Observable<Boolean>;
  subObsSetFavourite: Subscription;
  obsSetFavouriteFailure: Observable<any>;
  subObsSetFavouriteFailure: Subscription;

  obsRemoveFavourite: Observable<Boolean>;
  subObsRemoveFavourite: Subscription;
  obsRemoveFavouriteFailure: Observable<any>;
  subObsRemoveFavouriteFailure: Subscription;

  obsRepeatOrder: Observable<[Boolean, Array<string>]>;
  subObsRepeatOrder: Subscription;
  obsRepeatOrderFailure: Observable<any>;
  subObsRepeatOrderFailure: Subscription;

  obsHolidays: Observable<Holidays>;
  subObsHolidays: Subscription;

  obsListCartItems: Observable<CartResult[]>;
  subObsListCart: Subscription;
  obsListCartError: Observable<any>;
  subObsListCartError: Subscription;


  obsOrderDetails: Observable<OrderItem[]>;
  subObsOrderDetails: Subscription;
  obsOrderDetailsError: Observable<any>;
  subObsOrderDetailsError: Subscription;

  obsParent: Observable<Parent>;
  subObsParent: Subscription;

  obsSchoolInfo: Observable<SchoolInfo>;
  subObsSchoolInfo: Subscription;

  CONFIRMDELETE = "Are you sure you want to cancel the order ?"
  isUniformSelected: boolean = false;
  isCanteenOnlySelected: boolean = false;
  isEventOnlySelected: boolean = false;

  uniformOrders: UniformOrderHistory[];
  uniformOrderDetails: OrderDetails[];
  uniforOrderHistory: UniformOrderHistory;
  uniformOrderId: number;
  uniformOrderComment: string;
  orderDetails: OrderDetails[];
  showUniformDetails: boolean = false;

  combinedOrders: CombinedOrder[] = [];
  combinedOrdersCopy: CombinedOrder[] = [];

  orderHistoryType: string = "CANTEEN AND EVENT ORDERS";
  constructor(
    private _store: Store<fromRoot.State>,
    private _route: Router, private config: Config,
    private alertService: AlertService,
    public orderHistoryService: OrderHistoryService,
    public _changeDetectionRef: ChangeDetectorRef, public schoolInfo: SchoolInfo) {

    this.obsOrderHistory = this._store.select(fromRoot.selectListOrderHistorySuccess);
    this.obsOrderHistoryError = this._store.select(fromRoot.selectListOrderHistoryFailure);
    this.obsCancelOrder = this._store.select(fromRoot.selectCancelOrderSuccess);
    this.obsCancelOrderError = this._store.select(fromRoot.selectCancelOrderFailure);
    this.obsSetFavourite = this._store.select(fromRoot.selectFavouriteOrderSuccess);
    this.obsSetFavouriteFailure = this._store.select(fromRoot.selectRemoveFavouriteOrderFailure);
    this.obsRemoveFavourite = this._store.select(fromRoot.selectRemoveFavouriteOrderSuccess);
    this.obsRemoveFavouriteFailure = this._store.select(fromRoot.selectRemoveFavouriteOrderFailure);
    this.obsRepeatOrder = this._store.select(fromRoot.selectCanRepeatOrderSuccess);
    this.obsRepeatOrderFailure = this._store.select(fromRoot.selectCanRepeatOrderFailure);
    this.obsHolidays = this._store.select(fromRoot.selectListHolidaysSuccess);
    this.obsListCartItems = this._store.select(fromRoot.selectListCartItemsSuccess);
    this.obsListCartError = this._store.select(fromRoot.selectListCartItemsFailure);
    this.obsCancelConfirmAlert = this._store.select(fromRoot.getAlertCancelConfirmStatus);
    this.obsOrderDetails = this._store.select(fromRoot.selectListOrderDetailsSuccess);
    this.obsOrderDetailsError = this._store.select(fromRoot.selectListOrderDetailsFailure);
    this.obsParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.currency = this.config.getCurrency();

    this.obsSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
  }

  ngOnChanges() {

  }

  getAndPrepareOrder() {
    this.orderHistoryService.getParentOrdersAndUniformOrders().subscribe(data => {
      let parentOrders = data[0];
      let uniformOrders = data[1];
      if (parentOrders.length > 0) {

        parentOrders.forEach(element => {
          let order: CombinedOrder = new CombinedOrder();
          order.orderId = element.orderId;
          order.studentName = element.studentName;
          order.studentClass = element.studentClass;
          order.orderComment = element.orderComment;
          order.orderAmount = element.orderAmount;
          order.orderDate = element.orderDate;
          order.deliveryDate = element.deliveryDate;
          order.isCacelledOrder = element.isCacelledOrder;
          order.isFavouriteOrder = element.isFavouriteOrder;
          order.isSpecialOrder = element.isSpecialOrder;
          order.eventType = element.eventType;
          order.eventName = element.eventName;
          order.combinedOrderType = 1;
          this.combinedOrders.push(order);
        });
      }

      if (uniformOrders.length > 0) {
        uniformOrders.forEach(element => {
          let order: CombinedOrder = new CombinedOrder();
          order.orderId = element.orderId;
          order.studentName = element.studentName;
          order.studentClass = element.studentClass;
          order.orderComment = element.orderComment;
          order.orderAmount = element.orderAmount;
          order.orderDate = element.orderDate;
          order.deliveryDate = element.deliveryDate;
          order.isCacelledOrder = element.isCacelledOrder;
          order.isFavouriteOrder = element.isFavouriteOrder;
          order.isSpecialOrder = element.isSpecialOrder;
          order.eventType = element.eventType;
          order.eventName = element.eventName;
          order.combinedOrderType = 2;
          order.adminComments = element.adminComments;
          if (element.adminComments != null && element.adminComments != undefined) {
            let c = element.adminComments.split("  Comment:");
            
            order.adminComments = c[1];
          }
          switch (element.stage) {
            case 0:
              order.stageStatus = " RECEIVED AWAITING PROCESSING ";
              break;
            case 1:
              order.stageStatus = " PACKED AWAITING COLLECTION-DELIVERY ";
              break;
            case 2:
              order.stageStatus = " DELIVERED-CLOSED ";
              break;
            case 3:
              order.stageStatus = " UNDER INVESTIGATION ";
              break;
            case 4:
              order.stageStatus = " AWAITING STOCK ";
              break;
            case 5:
              order.stageStatus = " PARTIALLY DELIVERED ";
              break;
            case 6:
              order.stageStatus = " PACKED AWAITING PAYMENT ON COLLECTION ";
              break;
            case 7:
              order.stageStatus = "  CANCELLED ";
              break;
            case 8:
              order.stageStatus = " AWAITING PAYMENT CLEARANCE ";
              break;
          }
          this.combinedOrders.push(order);
        });
      }
      this.combinedOrders = this.combinedOrders.sort(this.sortByDate);
      this.combinedOrdersCopy = this.combinedOrders;
    });
    this.showSpinner = false;
  }

  sortByDate(a, b) {
    if (a.orderDate < b.orderDate) {
      return 1;
    }
    if (a.orderDate > b.orderDate) {
      return -1;
    }
    return 0;
  }

  ngOnInit() {
    let isOnInit = true;
    this.showSpinner = true;
    
    this.subObsSchoolInfo = this.obsSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
      }
    })
    this.getAndPrepareOrder();
    this.subObsCancelOrder = this.obsCancelOrder.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.isOrderCancelled) {
            // this.message = result.message;
            this.alertService.success(result.message);
            if (this.orderIdToCancel) {
              let index = this.combinedOrders.findIndex(order => order.orderId == this.orderIdToCancel)
              if (index >= 0) {
                this.combinedOrders.splice(index, 1);
              }
            }
          }
          else {
            // this.message = result.message;
            this.alertService.error(result.message);
            //alert(result.message);
          }
        }
        this.showSpinner = false;
      }
    })

    this.subObsCancelOrderError = this.obsCancelOrderError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsSetFavourite = this.obsSetFavourite.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          let index = this.orderHistory.filter(
            order => order.orderId == this.favOrderId ? order.isFavouriteOrder = true : order.isFavouriteOrder);
        }
        else {

          this.message = 'Oops something went wrong..';
          this.alertService.error(this.message);
        }
        this.showSpinner = false;
      }
    })

    this.subObsSetFavouriteFailure = this.obsSetFavouriteFailure.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsRemoveFavourite = this.obsRemoveFavourite.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.orderHistory.filter(order => order.orderId == this.favOrderId ? order.isFavouriteOrder = false : order.isFavouriteOrder)
        }
        else {

          this.message = 'Oops something went wrong..';
          this.alertService.error(this.message);
          //alert('Oops something went wrong..')
        }
        this.showSpinner = false;
      }
    })

    this.subObsRemoveFavouriteFailure = this.obsRemoveFavouriteFailure.subscribe(
      err => {
        if (!isOnInit) {
          this.showSpinner = false;
        }
      }
    )

    this.subObsRepeatOrder = this.obsRepeatOrder.subscribe( result => {
      let tmp = [];
      let missingProducts : Array<string> = [];
      if (!isOnInit) {
        if (result) {
          Object.keys(result).forEach(key => {
            
            if(key == "Item2"){
              tmp = result[key]
              Object.keys(tmp).forEach(key => {
                missingProducts.push(tmp[key].toString())
              });
            }

          });
        
          if(missingProducts.length != 0){
            this.alertService.error("You cannnot re-order as the following products are either out of stock or have been removed: "+ missingProducts.map(x => " " + x));
            this.showSpinner = false;
          }else{
            let deliveryDate = localStorage.getItem('deliveryDate');
            if (deliveryDate != null || deliveryDate.trim() != '') {
              this._store.dispatch(new ListCartActions.ListCartItems(deliveryDate, this.eventId));
            }
            else {
              this.showSpinner = false;
            }
          }
          
        }
        else {
          this.datePopup = false;
          
        }
      }
    })

    this.subObsListCart = this.obsListCartItems.subscribe(result => {
      if (this.repeatOrderClicked) {
        this.repeatOrderClicked = false;
        this.showSpinner = false;
        this._route.navigate(['canteenorder/dashboard/checkout']);
      }
    })

    this.subObsListCartError = this.obsListCartError.subscribe(err => {
      this.repeatOrderClicked = false;
    })

    this.subObsRepeatOrderFailure = this.obsRepeatOrderFailure.subscribe(err => {
      if (!isOnInit) {
        this.repeatOrderClicked = false;
        this.datePopup = false;
        this.showSpinner = false;
      }
    })

    this.subObsHolidays = this.obsHolidays.subscribe(result => {
      if (result) {
        this.holidays = result;
        this.maxDate = new Date(result.maxDate);
        this.holidays = result;
        this.disableDates();
      }
    })


    this.subCancelConfirmAlert = this.obsCancelConfirmAlert.subscribe(result => {
      if (result == true) {
        if (this.orderIdToCancel) {
          this.showSpinner = true;
          this._store.dispatch(new CancelOrderActions.CancelOrder(this.orderIdToCancel));
        }

      }
    })
    if (!this.holidays.maxDate) {
      this._store.dispatch(new ListHolidaysActions.ListHolidays);
    }

    this.subObsOrderDetails = this.obsOrderDetails.subscribe(result => {
      if (!isOnInit && this.printClicked) {
        this.printClicked = false;
        this.orderItems = [];
        if (result) {
          this.orderItems = result;
          this._changeDetectionRef.detectChanges();
          this.showSpinner = false;
          this.print();
        }
        else {
          this.showSpinner = false;
        }
      }
    })

    this.subObsOrderDetailsError = this.obsOrderDetailsError.subscribe(error => {
      if (!isOnInit)
        this.showSpinner = false;
    })

    this.subObsParent = this.obsParent.subscribe(parent => {
      if (parent) {
        this.parent = parent;
      }
    })
    isOnInit = false;
  }


  disableDates() {
    if (this.holidays) {
      this.dateOptions.disableSince = this.disableSince = {
        year: this.maxDate.getFullYear(),
        month: this.maxDate.getMonth() + 1,
        day: this.maxDate.getDate()
      }

      if (this.holidays.holidayDateRange) {
        for (let i = 0; i < this.holidays.holidayDateRange.length; i++) {
          let startDate = new Date(this.holidays.holidayDateRange[i].startDate);
          let endDate = new Date(this.holidays.holidayDateRange[i].endDate);
          var date = {
            begin: {
              year: startDate.getFullYear(),
              month: startDate.getMonth() + 1,
              day: startDate.getDate()
            },
            end: {
              year: endDate.getFullYear(),
              month: endDate.getMonth() + 1,
              day: endDate.getDate()
            }
          }
          this.disableDateRanges.push(date);
        }
        this.dateOptions.disableDateRanges = this.disableDateRanges;
      }

      if (this.holidays.cutOffDays) {
        //cutoff date
        for (let j = 0; j < this.holidays.cutOffDays.length; j++) {
          let newDateObj = new Date(this.holidays.cutOffDays[j]);
          let nYear = newDateObj.getFullYear();
          let nMonth = newDateObj.getMonth();
          let nDay = newDateObj.getDate();
          var disableDate = { year: nYear, month: nMonth + 1, day: nDay }
          this.disableDays.push(disableDate);
        }
        this.dateOptions.disableDays = this.disableDays;
      }

      if (this.holidays.weeklyClosures) {
        this.holidays.weeklyClosures.forEach(day => {
          switch (day) {
            case 1:
              this.disableWeekdays.push('su');
              break;
            case 2:
              this.disableWeekdays.push('mo');
              break;
            case 3:
              this.disableWeekdays.push('tu');
              break;
            case 4:
              this.disableWeekdays.push('we');
              break;
            case 5:
              this.disableWeekdays.push('th');
              break;
            case 6:
              this.disableWeekdays.push('fr');
              break;
            case 7:
              this.disableWeekdays.push('sa');
              break;
          }
        })
      }
    }
  }

  dateChanged(selectedDate: any) {
    console.log(selectedDate);
    this.orderDate = selectedDate;
  }

  showDatePopUp(order: ParentOrder) {
    this.datePopup = true;
    this.order = order;
  }

  repeatOrder() {
    if (this.order.orderId && this.orderDate) {
      let date = this.orderDate.date.month + "-" + this.orderDate.date.day + "-" + this.orderDate.date.year;

      localStorage.removeItem('eventId');
      localStorage.removeItem('eventName');
      localStorage.removeItem('bagName');
      localStorage.removeItem('bagPrice');
      localStorage.removeItem('isBagAvailable');

      localStorage.removeItem('isSetPrice');
      localStorage.removeItem('eventPrice');
      localStorage.removeItem('whoPays');
      localStorage.removeItem('isFundRaising');

      localStorage.setItem('deliveryDate', date);
      localStorage.setItem('showDate', this.orderDate.jsdate.toDateString());
      localStorage.setItem('orderType', 'canteen');

      this.datePopup = false;
      this.repeatOrderClicked = true;
      this.showSpinner = true;
      this._store.dispatch(new RepeatOrderActions.RepeatOrder(this.order.orderId, date));
    }
  }

  hideDatePopup() {
    this.orderDate = null;
    this.datePopup = false;
  }

  /*
  showFavouritesOnly() {
    this.showFavourites = !this.showFavourites;
    if (this.showFavourites) {
      this.showFavouriteBtnText = "Show All";
      this.orderHistory = this.orderHistory.filter(order => order.isFavouriteOrder == true);
    }
    else {
      this.showFavouriteBtnText = "Show Favourites Only";
      this.orderHistory = this.orderHistoryCopy;
    }
  }
  */

  showFavouritesOnly() {
    this.isUniformSelected = false;
    this.orderHistoryType = "FAVOURITE ORDERS";
    this.orderHistory = this.orderHistoryCopy.filter(order => order.isFavouriteOrder == true);
  }

  showAll() {
    this.orderHistoryType = "ALL ORDERS";
    this.orderHistory = this.orderHistoryCopy;
  }

  showCanteenOnly() {
    this.orderHistoryType = "CANTEEN ORDERS";
    this.isUniformSelected = false;
    this.isCanteenOnlySelected = true;
    this.isEventOnlySelected = false;
    this.orderHistory = this.orderHistoryCopy.filter(order => order.isSpecialOrder != true);
  }

  showEventsOnly() {
    this.orderHistoryType = "EVENT ORDERS";
    this.isUniformSelected = false;
    this.isCanteenOnlySelected = false;
    this.isEventOnlySelected = true;
    this.orderHistory = this.orderHistoryCopy.filter(order => order.isSpecialOrder == true);
  }

  showSchoolEventsOnly() {
    this.orderHistoryType = "SCHOOL EVENT ORDERS";
    this.isUniformSelected = false;
    this.orderHistory = this.orderHistoryCopy.filter(order => order.isSpecialOrder == true && order.eventType.toUpperCase() == 'SCHOOL');
  }

  showCanteenEventsOnly() {
    this.orderHistoryType = "CANTEEN EVENT ORDERS";
    this.isUniformSelected = false;
    this.orderHistory = this.orderHistoryCopy.filter(order => order.isSpecialOrder == true && order.eventType.toUpperCase() == 'EVENT');
  }

  showUniformOrdersOnly() {
    this.orderHistoryType = "UNIFORM ORDERS";
    this.isUniformSelected = true;
    this.isCanteenOnlySelected = false;
    this.isEventOnlySelected = false;
    this.showSpinner = true;
  }

  hideUniformDetails() {
    this.showUniformDetails = false;
  }

  displayUniformOrderDetails(uniformOrder) {
    this.showSpinner = true;
    this.uniformOrderId = uniformOrder.orderId;
    this.uniformOrderComment = uniformOrder.comment;
    this.showUniformDetails = true;

    this.orderHistoryService.getUniformOrderDetails(this.uniformOrderId).subscribe(res => {

      if (res) {
        this.showSpinner = false;
        this.orderDetails = res;
        this.orderDetails.forEach(orderDetail => {
          console.log(orderDetail)
          if(orderDetail.isUniformPack != 0){
            this.showSpinner = true;
            this.orderHistoryService.getUniformPackOrderItemDetails(orderDetail.basketItemId).subscribe(res => {
              this.showSpinner = false;
              if (res) {
                orderDetail.uPackDetails = res;
              }
             })
          }
          orderDetail.total = orderDetail.price * orderDetail.quantity;
        })
      }


    }, err => {
      console.log("ERROR");
      this.showSpinner = false;
    });

  }

  displayDetails(order) {
    this.showSpinner = true;
    this.order = order;
    this.showDetails = true;
  }

  hideDetails() {
    this.showDetails = false;
  }

  cancelOrder(orderId) {

    this.orderIdToCancel = orderId;
    this.alertService.confirm(this.CONFIRMDELETE);
    //this._store.dispatch(new AlertActions.CancelConfirmAlert());

    /*
    
*/

  }

  stopSpinner() {
    this.showSpinner = false;
  }

  saveFavourite(orderId) {
    this.showSpinner = true;
    this.favOrderId = orderId;
    this._store.dispatch(new FavOrderActions.FavOrder(orderId, true));
  }

  removeFromFavourite(orderId) {
    this.showSpinner = true;
    this.favOrderId = orderId;
    this._store.dispatch(new RemoveFavouriteActions.RemoveFav(orderId, false));
  }

  getDetailsAndPrint(order: ParentOrder) {
    this.showSpinner = true;
    this.printClicked = true;
    this.order = order;
    this.orderIdString = order.orderId.toString();
    this._store.dispatch(new ListOrderDetailsActions.ListOrderDetails(this.order));
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>School24 Events Online :: Order Print</title>
          <style>
            .table-border table,.table-border th,.table-border td{
              border: 1px solid #ddd;
            }

            th, td {
              padding: 5px;
              text-align: left;
            }
            table{
              margin-top:30px;
              width:100%;
              border-collapse: collapse;
            }
            .margin-top{
              margin-top:30px;
            }

            .left-padding{
              padding-left:17px;
            }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    //popupWin.document.close();
  }


  onSearchChange(value) {
    if (value) {
      this.combinedOrders = this.combinedOrdersCopy.filter(e => (e.studentName && e.studentName.toLowerCase().includes(value.toLowerCase()))
        || ((e.orderId + '').indexOf(value) > -1)
      );
    }
    else {
      this.combinedOrders = this.combinedOrdersCopy;
    }
  }

  filterChanged(value) {
    if (value == 'all') {
      this.combinedOrders = this.combinedOrdersCopy;
    }
    if (value == 'canteen') {
      this.combinedOrders = this.combinedOrdersCopy.filter(order => order.combinedOrderType == 1 && order.isSpecialOrder == false);
    }
    if (value == 'event') {
      this.combinedOrders = this.combinedOrdersCopy.filter(order => order.combinedOrderType == 1 && order.isSpecialOrder == true);
    }
    if (value == 'uniform') {
      this.combinedOrders = this.combinedOrdersCopy.filter(order => order.combinedOrderType == 2);
    }
  }

  cancelCanteenEventOrder(orderId) {
    if (confirm('Are you sure you want to cancel the order?')) {
      this.showSpinner = true;
      this.orderHistoryService.cancelCanteenEventOrder(orderId).subscribe(result => {
        // console.log(result);
        if (result) {
          if (result.isOrderCancelled) {
            // this.message = result.message;
            this.alertService.success(result.message);
            if (orderId) {
              let index = this.combinedOrders.findIndex(order => order.orderId == orderId)
              if (index >= 0) {
                this.combinedOrders.splice(index, 1);
              }
            }
            this._store.dispatch(new ListUserActions.ListParent);
          }
          else {
            // this.message = result.message;
            this.alertService.error(result.message);
            //alert(result.message);
          }
        }
        this.showSpinner = false;
      });
    }
  }

  cancelSchoolEventOrder(orderId) {
    if (confirm('Are you sure you want to cancel the order?')) {
      this.showSpinner = true;
      this.orderHistoryService.cancelSchoolEventOrder(orderId).subscribe(result => {
        if (result) {
          if (result.isOrderCancelled) {
            // this.message = result.message;
            this.alertService.success(result.message);
            if (orderId) {
              let index = this.combinedOrders.findIndex(order => order.orderId == orderId)
              if (index >= 0) {
                this.combinedOrders.splice(index, 1);
              }
            }
            this._store.dispatch(new ListUserActions.ListParent);
          }
          else {
            // this.message = result.message;
            this.alertService.error(result.message);
            //alert(result.message);
          }
        }
        this.showSpinner = false;
      });
    }
  }

  ngOnDestroy() {
    if (this.subObsOrderHistory)
      this.subObsOrderHistory.unsubscribe();
    if (this.subObsCancelOrder)
      this.subObsCancelOrder.unsubscribe();
    if (this.subObsCancelOrderError)
      this.subObsCancelOrderError.unsubscribe();
    if (this.subObsOrderHistoryError)
      this.subObsOrderHistoryError.unsubscribe();
    if (this.subObsRemoveFavourite)
      this.subObsRemoveFavourite.unsubscribe();
    if (this.subObsRemoveFavouriteFailure)
      this.subObsRemoveFavouriteFailure.unsubscribe();
    if (this.subObsSetFavourite)
      this.subObsSetFavourite.unsubscribe();
    if (this.subObsSetFavouriteFailure)
      this.subObsSetFavouriteFailure.unsubscribe();
    if (this.subObsRepeatOrder)
      this.subObsRepeatOrder.unsubscribe();
    if (this.subObsRepeatOrderFailure)
      this.subObsRepeatOrderFailure.unsubscribe();
    if (this.subObsHolidays)
      this.subObsHolidays.unsubscribe();
    if (this.subObsListCart)
      this.subObsListCart.unsubscribe();
    if (this.subObsListCartError)
      this.subObsListCartError.unsubscribe();
    if (this.subObsOrderDetails)
      this.subObsOrderDetails.unsubscribe();
    if (this.subObsOrderDetailsError)
      this.subObsOrderDetailsError.unsubscribe();
    if (this.subCancelConfirmAlert)
      this.subCancelConfirmAlert.unsubscribe();
    if (this.subObsSchoolInfo)
      this.subObsSchoolInfo.unsubscribe();
  }
}

export class CombinedOrder {
  orderId: number;
  studentName: string;
  studentClass: string;
  orderComment: string;
  orderAmount: number;
  orderDate: Date;
  deliveryDate: Date;
  isCacelledOrder: boolean;
  isFavouriteOrder: boolean;
  isSpecialOrder: boolean;
  eventType: string;
  eventName: string;
  // orderId: number;
  // orderAmount: number;
  stage: number;
  parentName: string;
  parentEMail: string;
  stageStatus: string;
  comment: string;
  // orderDate: string;
  combinedOrderType: number; // 2 - uniform 1- other orders
  adminComments: string;
}
