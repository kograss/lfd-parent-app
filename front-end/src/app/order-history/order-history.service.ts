import { RepeatOrderItemProduct } from './model/repeatOrderItemProduct';
import { CancelOrderResult } from './model/cancelOrderResult';
import { ParentOrder } from './model/parent-order';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { OrderDetails } from './model/order-details';
import { UniformOrderHistory } from './model/order-history';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Injectable()
export class OrderHistoryService {

  private _listOrderHistoryUrl = "OrderHistory/GetOrderHistory";
  private _cancelOrderUrl = "Order/CancelOrder/{orderId}";
  private _setOrRemFavUrl = "Order/SetOrRemoveFavourite";
  private _checkRepeatUrl = "Order/AddOrderToCart";
  private _getOrderHistoryUrl = "Uniform/GetUniformOrders/{year}";
  private _getOrderDetails = "Uniform/GetUniformOrderDetails/{orderId}";
  private _getUpackOrderItemDetails = "Uniform/GetUniformPackOrderItemDetails/{itemId}";
  private _cancelCanteenEventOrderUrl = "CanteenEventOrder/CancelOrder/{orderId}";
  private _cancelSchoolEventOrderUrl = "SchoolEvent/Order/CancelOrder/{orderId}";

  constructor(private _http: HttpClient) {
  }

  getOrderHistory(): Observable<any> {
    return this._http
      .get(this._listOrderHistoryUrl)
      ;
  }

  cancelOrder(orderId: number): Observable<any> {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      });
    let body = JSON.stringify(orderId);
    let options = { headers: headers };
    return this._http.delete(this._cancelOrderUrl.replace("{orderId}", orderId.toString()), options);
  }

  favouriteOrder(orderId: number, favourite: boolean): Observable<any> {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      });
    let options = { headers: headers };
    var favouriteUpdateInput = { orderId: orderId, setOrRemove: favourite };
    let body = JSON.stringify(favouriteUpdateInput);
    return this._http.put(this._setOrRemFavUrl, body, options);
  }

  repeatOrder(orderId: number, deliveryDate: any): Observable<any> {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      });
    let options = { headers: headers };
    let body = JSON.stringify({ orderId: orderId, deliveryDate: deliveryDate });
    return this._http.post(this._checkRepeatUrl, body, options);
  }

  getUniformOrderHistory(): Observable<any> {
    return this._http
      .get(this._getOrderHistoryUrl.replace("{year}", '2018'))
      ;
  }

  getUniformOrderDetails(orderId: number): Observable<any> {
    return this._http.get(this._getOrderDetails.replace('{orderId}', orderId.toString()));
  }

  getUniformPackOrderItemDetails(itemId: number): Observable<any> {
    return this._http.get(this._getUpackOrderItemDetails.replace('{itemId}', itemId.toString()));
  }

  cancelSchoolEventOrder(orderId: number): Observable<any> {
    return this._http.delete(this._cancelSchoolEventOrderUrl.replace("{orderId}", orderId.toString()));
  }

  cancelCanteenEventOrder(orderId: number): Observable<any> {
    return this._http.delete(this._cancelCanteenEventOrderUrl.replace("{orderId}", orderId.toString()));
  }

  public getParentOrdersAndUniformOrders(): Observable<any[]> {
    let res1 = this.getOrderHistory();
    let res2 = this.getUniformOrderHistory();
    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return forkJoin([res1, res2]);
  }
}

