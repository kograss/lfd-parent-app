import { AlertService } from './../alert/alertService';
import { DatePickerModule } from './../date-picker/date-picker.module';
import { LoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { DataFilterPipe } from './../common/data-filter.pipe';
import { OrderDetailsModule } from './../order-details/order-details.module';
import { BrowserModule } from '@angular/platform-browser';
import { OrderHistoryService } from './order-history.service';
import { OrderHistoryComponent } from './order-history.component';
import { OrderHistoryEffects } from './order-history.effects';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AlertComponent } from '../alert/alert.component';
import { AlertModule } from '../alert/alert.module';
import { QRCodeModule } from 'angularx-qrcode';
import { NgxBarcodeModule } from 'ngx-barcode';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';


@NgModule({
  imports: [
    QRCodeModule,
    NgxBarcodeModule,
    FormsModule,
    EffectsModule.forFeature([OrderHistoryEffects]),
    BrowserModule,
    OrderDetailsModule,
    DataTableModule,
    CommonModule,
    DatePickerModule,
    CommonMenuModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    RouterModule.forRoot([{ path: 'history', component: OrderHistoryComponent }])
  ],
  declarations: [OrderHistoryComponent, DataFilterPipe],
  exports: [OrderHistoryComponent],
  providers: [OrderHistoryService, AlertService]
})
export class OrderHistoryModule { }
