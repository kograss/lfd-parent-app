import { UniformService } from './uniform.service';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects'
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

@Injectable()
export class UniformEffects {

    constructor(
        private actions$: Actions,
        private uniformService: UniformService
    ) { }

}