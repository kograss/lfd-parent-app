import { OrderHistoryModule } from './../history/order-history.module';
import { UniformProductsModule } from './../products/products.module';
import { UniformCategoriesModule } from './../categories/categories.module';
import { reducers } from './../../reducers';
import { UniformNewsModule } from './../uniform-news/uniform-news.module';
import { UniformService } from './uniform.service';
import { UniformEffects } from './uniform.effects';
import { UniformComponent } from './uniform.component';

import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptedHttp } from '../../interceptor/InterceptedHttp.interceptor';
import { UniformCartModule } from '../cart/cart.module';
import { UniformCheckoutModule } from '../checkout/uniform-checkout.module';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    UniformNewsModule,
    UniformCategoriesModule,
    UniformProductsModule,
    OrderHistoryModule,
    UniformCartModule,
    UniformCheckoutModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    EffectsModule.forFeature([UniformEffects]),
  ],
  declarations: [UniformComponent],
  exports: [UniformComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptedHttp, multi: true },
    UniformService]
})

export class UniformModule {

}