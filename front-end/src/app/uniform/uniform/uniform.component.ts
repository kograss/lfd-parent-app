import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import * as GetBasketItemAction from './../cart/action/get-cart-item.action';
import * as ShowHideSidebarActions from './../cart/action/show-hide-cart.action';
import { UniformBasketItem } from './../cart/model/uniform_basketitem';
import * as fromRoot from './../../reducers';
import { AddToCartResult } from '../cart/model/add-to-cart-result';
import { DeleteCartModel } from '../cart/model/update-cart';

@Component({
  selector: 'uniform',
  templateUrl: './uniform.component.html',
  styleUrls: ['./uniform.component.css']
})
export class UniformComponent implements OnInit, OnDestroy {

  categoryName: string;
  showCart: boolean = false;
  basketItems: UniformBasketItem[] = [];

  cartResult: AddToCartResult;

  deleteItem: DeleteCartModel;
  deleteType: string;

  obsUniformBasketItem: Observable<UniformBasketItem[]>;
  subObsUniformBasketItem: Subscription;

  obsShowHideCart: Observable<Boolean>;
  subObsShowHideCart: Subscription;

  obsAddUniformToCart: Observable<AddToCartResult>;
  subObsAddUniformToCart: Subscription;

  obsDeleteItem: Observable<UniformBasketItem[]>;
  subObsDeleteItem: Subscription;

  obsPlaceOrder: Observable<number>;
  subObsPlaceOrder: Subscription;

  obsPlaceOrderUsingBalance: Observable<number>;
  subObsPlaceOrderUsingBalance: Subscription;

  displayCartIcon: boolean;
  obsDisplayCartIcon: Observable<boolean>;

  cartCount: string;

  constructor(private _store: Store<fromRoot.State>, private _router: Router,
    private route: ActivatedRoute) {
    this.obsUniformBasketItem = this._store.select(fromRoot.selectFromGetBasketItemSuccess);
    this.obsShowHideCart = this._store.select(fromRoot.selectFromSelectShowHideCartSuccess);
    this.obsAddUniformToCart = this._store.select(fromRoot.selectFromAddToCartStatusSuccess);
    this.obsDeleteItem = this._store.select(fromRoot.selectFromDeleteItemSuccess);
    this.obsPlaceOrder = this._store.select(fromRoot.selectFromCheckotSuccess)
    this.obsDisplayCartIcon = this._store.select(fromRoot.selectFromDisplayCartIcon);
    this.obsPlaceOrderUsingBalance = this._store.select(fromRoot.selectFromPayUsingBalanceSuccess);
  }

  ngOnInit() {
    

    this.route.params.subscribe(param => {
      this.categoryName = param['catName'];
    });


    this.cartCount = "0";
    this.subObsShowHideCart = this.obsShowHideCart.subscribe(result => {

      this.showCart = result.valueOf();
    })

    let basketId = localStorage.getItem("basketId");
    if (basketId)
      this._store.dispatch(new GetBasketItemAction.GetBasketItem(+basketId));

    this.subObsAddUniformToCart = this.obsAddUniformToCart.subscribe(result => {
      if (result) {
        if (result.currentBasketCount) {
          this.cartCount = result.currentBasketCount.toString();
        }
      }
    })

    this.subObsUniformBasketItem = this.obsUniformBasketItem.subscribe(result => {
      if (result) {
        //this.cartCount = result.length.toString();
        let cnt = 0;
        result.forEach(e => {
          cnt = cnt + e.quantity;
        });
        this.cartCount = cnt.toString();
      }
      else {
        this.cartCount = "0";
        this.basketItems = [];
      }
    })

    this.subObsPlaceOrder = this.obsPlaceOrder.subscribe(result => {
      if (result && result != 0) {
        this.cartCount = "0";
      }
    })

    this.subObsPlaceOrderUsingBalance = this.obsPlaceOrderUsingBalance.subscribe(result => {
      if (result && result != 0) {
        this.cartCount = "0";
      }
    })

  }

  displayHistory() {
    this._router.navigate(['uniformorder/history']);
  }

  displayCart() {
    this._store.dispatch(new ShowHideSidebarActions.UniformShowHideSidebar(!this.showCart));
  }

  ngOnDestroy() {
    if (this.subObsUniformBasketItem) {
      this.subObsUniformBasketItem.unsubscribe();
    }
    if (this.subObsShowHideCart) {
      this.subObsShowHideCart.unsubscribe();
    }
    if (this.subObsAddUniformToCart) {
      this.subObsAddUniformToCart.unsubscribe();
    }
    if (this.obsPlaceOrderUsingBalance) {
      this.subObsPlaceOrderUsingBalance.unsubscribe();
    }
  }
}
