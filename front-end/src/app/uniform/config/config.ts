import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class Config {
  private _config: Object
  private _currency = 'USD';
  constructor(private http: HttpClient) {

  }

  getCurrency() {
    return this._currency;
  }
};