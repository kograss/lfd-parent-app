import { Injectable } from '@angular/core';

@Injectable()
export class MessageConfig {
  private _config: Object
  public static STOCK_EXCEEDED_WARNING = 'Quantity selected exceeds stock available';
  public static OPTIONS_ARE_NOT_SELECTED = 'You must select product size and color.';
  public static PRODUCT_SIZE_IS_NOT_SELECTED = 'You must select product size.';
  public static PRODUCT_COLOR_IS_NOT_SELECTED = 'You must select product color.';
};