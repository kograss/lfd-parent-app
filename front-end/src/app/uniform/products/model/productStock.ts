export class ProductStock {
    sizeAttributeId: number;
    colorAttributeId: number;
    productId: number;
    schoolId: number;
}