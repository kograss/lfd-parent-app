import { option } from './options';

export class UniformProduct {
    productId: number;
    name: string;
    description: string;
    image: string;
    stock: any ;
    price: number;
    options: option[] = [];
    colorOptions: option[] = [];
    sizeOptions: option[] = [];
    quantity: number;
    size: string;
    color: string;
    TopupMethod:string;
    Bank:string;
    BankAccount:string;
    BankBSB:string;
    AccountEmail :string;
    Shop:string;
    isStudentOptional: boolean;
    isExternalRecipientRequired: boolean;
    isUniformPack: boolean;
}

export class UniformPack extends UniformProduct {
    products: UniformPackProduct[] = [];
}

export class UniformPackProduct {
    id: number;
    name: string;
    productId: number;
    quantity: number;
    uniformPackId: number;
    options: option[] = [];
    colorOptions: option[] = [];
    sizeOptions: option[] = [];
    size: string;
    color: string;
    stock: any ;
  }