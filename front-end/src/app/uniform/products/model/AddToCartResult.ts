export class AddToCartResult {
    basketId : number;
    currentBasketCount : number;
}