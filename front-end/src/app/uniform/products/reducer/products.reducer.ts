import { UniformProduct } from './../model/products';
import * as GetProductsAction from './../action/get-products.action';

export interface State {    
   
    result: UniformProduct[],
    err: any
}

const initialState: State = {    
   
    result: [],
    err: {}
};

export function reducer(state = initialState, action: GetProductsAction.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetProductsAction.GETPRODUCTS_SUCCESS: {
            return {
                ...state,
                result: action.uniformProduct
            }
        }
        case GetProductsAction.GETPRODUCTS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }
    }
}
