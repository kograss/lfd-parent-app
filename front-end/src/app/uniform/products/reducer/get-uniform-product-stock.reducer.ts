import * as ProductStockActions from '../action/get-uniform-product-stock.action'
import { ProductStock } from '../model/productStock';

export interface State {

  result: any,
  err: any
}

const initialState: State = {

  result: {},
  err: {}
};

export function reducer(state = initialState, action: ProductStockActions.All): State {
  //console.log("Action Type" + action.type);
  switch (action.type) {

    case ProductStockActions.GETUNIFORMPRODUCTSSTOCK_SUCCESS: {
      return {
        ...state,
        result: action.payload
      }
    }
    case ProductStockActions.GETUNIFORMPRODUCTSSTOCK_FAILURE: {
      return {
        ...state,
        err: action.payload
      }
    }

    default: {
      return state;
    }

  }

}
