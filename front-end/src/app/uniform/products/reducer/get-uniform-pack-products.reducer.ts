import { UniformProduct } from '../model/products';
import * as GetProductsAction from '../action/get-uniform-pack-products.action';

export interface State {    
   
    result: UniformProduct[],
    err: any
}

const initialState: State = {    
   
    result: [],
    err: {}
};

export function reducer(state = initialState, action: GetProductsAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetProductsAction.GETUNIFORMPACKPRODUCTS_SUCCESS: {
            return {
                ...state,
                result: action.uniformProduct
            }
        }
        case GetProductsAction.GETUNIFORMPACKPRODUCTS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }
    }
}
