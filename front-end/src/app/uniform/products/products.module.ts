import { UniformProductsEffects } from './products.effects';
import { UniformProductsService } from './products.service';
import { UniformProductsComponent } from './products.component';
import { reducers } from './../../reducers';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { UniformCartModule} from './../cart/cart.module';
import { AlertService } from './../../alert/alertService';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        UniformCartModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        
        EffectsModule.forFeature([UniformProductsEffects]),
    ],
    declarations: [UniformProductsComponent],
    exports: [UniformProductsComponent],
    providers: [UniformProductsService, AlertService]
})
export class UniformProductsModule {

}