import { AlertComponent } from './../../alert/alert.component';
import { AlertService } from './../../alert/alertService';
import { AddToCartResult } from './../cart/model/add-to-cart-result';
import { AddToCartModel, UniformCartItem, UniformPackItemProduct } from './../cart/model/add-to-cart';
import { option } from './model/options';
import { UniformProduct, UniformPack, UniformPackProduct } from './model/products';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Config } from './../config/config';
import { Observable } from 'rxjs/Rx';
import { Store, Action } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from './../../reducers';
import * as GetProductAction from './action/get-products.action';
import * as AddToCartAction from './../cart/action/add-to-cart.action';
import * as ProductStockActions from './action/get-uniform-product-stock.action'
import { UniformBasketItem } from '../cart/model/uniform_basketitem';
import { ProductStock } from './model/productStock';
import { UniformProductsService } from './products.service'
// import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { MessageConfig } from '../config/message-config';
import * as GetBasketItemAction from './../cart/action/get-cart-item.action';
import { SchoolInfo } from '../../common/school-info/model/school-info';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})


export class UniformProductsComponent implements OnInit, OnDestroy {

  products: UniformProduct[] = [];
  productIndex: number;
  product: UniformProduct;
  cartItem: AddToCartModel;
  cartResult: AddToCartResult;
  uniformPacks: UniformPack[] = [];
  productStock: ProductStock;
  uniformPackItemProducts: UniformPackItemProduct[] = []

  obsGetProducts: Observable<UniformProduct[]>;
  subObsGetProducts: Subscription;
  uPackProductToDisplay: number ;

  obsGetUniformPackProducts: Observable<UniformProduct[]>;
  subObsGetUniformPackProducts: Subscription;

  obsAddUniformToCart: Observable<AddToCartResult>;
  subObsAddUniformToCart: Subscription;

  obsGetProductStock: Observable<any>;
  subObsGetProductStock: Subscription;

  obsUniformBasketItem: Observable<UniformBasketItem[]>;
  subObsUniformBasketItem: Subscription;

  schoolInfo: SchoolInfo;
  displayQuantity = false;
  showDetails = false;
  isOnInit: boolean = false;

  quantity: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  sizeOption: option[] = [];
  colorOption: option[] = [];
  message: string;
  showSpinner: boolean;
  

  basketItem: UniformBasketItem[];
  displayUniformPackDetails: boolean;
  uniformPackDetails: UniformPackProduct[];

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;
  obsSchoolInfo: Observable<SchoolInfo>;

  uPackProductsMissingSelection:  [number, string][] = [];
  uPackProductsOutStock:  [number, string, string][] = [];

  constructor(private _store: Store<fromRoot.State>, private _router: Router,
    private route: ActivatedRoute, private alertService: AlertService, private uniformProductsService: UniformProductsService) {
      this.obsGetProducts = this._store.select(fromRoot.selectFromUniformProductStatusSuccess);
      this.obsGetUniformPackProducts = this._store.select(fromRoot.selectFromUniformPackProductStatusSuccess);
      this.obsAddUniformToCart = this._store.select(fromRoot.selectFromAddToCartStatusSuccess);
      this.obsGetProductStock = this._store.select(fromRoot.selectFromGetProductStockSuccess);
      this.obsUniformBasketItem = this._store.select(fromRoot.selectFromGetBasketItemSuccess);
      this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
  }


  ngOnInit() {
    this.showSpinner = true;
    let isOnInit = true;
    this.subObsGetProducts = this.obsGetProducts.subscribe(result => {
      if (result) {
        this.products = result;
        this.products.forEach(product => {
          if (product.options) {
            localStorage.setItem("TopupMethod", product.TopupMethod);
            localStorage.setItem("BankName", product.Bank);
            localStorage.setItem("BankAccount", product.BankAccount);
            localStorage.setItem("BankBSB", product.BankBSB);
            localStorage.setItem("Shop", product.Shop);
            localStorage.setItem("AccountEmail", product.AccountEmail);
            
            if(product.isStudentOptional){
              localStorage.setItem("isStudentOptional", product.isStudentOptional.toString());
            }
            if(product.isExternalRecipientRequired){
              localStorage.setItem("isExternalRecipientRequired", product.isExternalRecipientRequired.toString());
            }

            product.sizeOptions = product.options.filter(option => option.categoryName == 'Size');
            product.colorOptions = product.options.filter(option => option.categoryName == 'Color');
          }
          else{
            product.sizeOptions = [];
            product.colorOptions = [];
          }
        }
        );
        this.showSpinner = false;
      }
    })

    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;

        if(!this.schoolInfo.uniform_status){
          this._router.navigate(['canteenorder/dashboard/home']);
        }
      }
    })

    this.subObsAddUniformToCart = this.obsAddUniformToCart.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.showSpinner = false;
          this.cartResult = result;
          if (result.basketId) {
            localStorage.setItem("basketId", result.basketId.toString());
            localStorage.setItem("cartCount", result.currentBasketCount.toString());//TODO: check if this is required
            this.message = 'Product added successfully..!';
            // this.alertService.success(this.message);
          }
        }
      }

    })


    this.subObsGetProductStock = this.obsGetProductStock.subscribe(productsStock => {
      if (this.product)
        this.product.stock = productsStock;
    })

    this.subObsUniformBasketItem = this.obsUniformBasketItem.subscribe(result => {
      if (result) {
        this.basketItem = result;
      }
      else {
        this.basketItem = [];
      }
    })

    this.subObsAddUniformToCart = this.obsAddUniformToCart.subscribe(result => {
      if (result && result.basketId && this.cartItem) {
        this.basketItem.push(this.cartItem);
      }
    })

    isOnInit = false;
  }


  showQuantity() {
    this.displayQuantity = !this.displayQuantity;
  }

  selectSize(optionName, isChecked: boolean, product: UniformProduct) {
    product.size = optionName;
  }

  selectColor(optionName, isChecked: boolean, product: UniformProduct) {
    product.color = optionName;
  }
  
  setUPackProductToDisplay(id: number){
    this.uPackProductToDisplay = id;
  }

  unsetUPackProductToDisplay(){
    this.uPackProductToDisplay = null;
  }
  addUniformToCart(product: UniformProduct, productIndex: number) {
    this.product = product;
    this.productIndex = productIndex;
    this.cartItem = new AddToCartModel();
    this.cartItem.productId = product.productId;
    this.cartItem.name = product.name;
    this.cartItem.quantity = product.quantity || 1;
    this.cartItem.price = product.price;
    this.cartItem.color = product.color;
    this.cartItem.size = product.size;
    this.cartItem.isUniformPack = product.isUniformPack? 1 : 0

    if (localStorage.getItem("basketId") != null) {
      this.cartItem.basketId = parseInt(localStorage.getItem("basketId"));
    }

    let cartItem = this.basketItem.filter(item =>(item.productId == product.productId && item.color == product.color && item.size == product.size));

    if (cartItem && cartItem[0]) {
      if (product.stock > 0 && product.stock < this.cartItem.quantity + cartItem[0].quantity) {
        this.alertService.warn(MessageConfig.STOCK_EXCEEDED_WARNING);
        return;
      }
    }
    else if (product.stock > 0 && product.stock < product.quantity) {
      this.alertService.warn(MessageConfig.STOCK_EXCEEDED_WARNING);
      return;
    }
  // }
  if(product.isUniformPack){
    this.showSpinner = true;
    this.uniformProductsService.getUniformPackProduct(product.productId).subscribe(result => {
      this.uniformPackDetails = result;
      for(var product of this.uniformPackDetails){
        if (product.options) {
          product.sizeOptions = product.options.filter(option => option.categoryName == 'Size');
          product.colorOptions = product.options.filter(option => option.categoryName == 'Color');
        }
        else{
          product.sizeOptions = [];
          product.colorOptions = [];
        }
      }
      if(this.uniformPackDetails.length > 0){
        this.displayUniformPackDetails = true;
        this.uPackProductsMissingSelection = [];
      }
      else {
        this.addToCart();
      }
      this.showSpinner = false;
    })
    

  }else {
    if (product.sizeOptions.length > 0 && product.colorOptions.length > 0) {
      (product.color != undefined && product.size != undefined && product.color != "undefined" && product.size != "undefined")
        ? this.addToCart() : this.alertService.warn(MessageConfig.OPTIONS_ARE_NOT_SELECTED);

    }
    else if (product.sizeOptions.length > 0) {
      (product.size != undefined && product.size != "undefined")
        ? this.addToCart()
        : this.alertService.warn(MessageConfig.PRODUCT_SIZE_IS_NOT_SELECTED);
    }
    else if (product.colorOptions.length > 0) {
      (product.color != undefined && product.size != "undefined")
        ? this.addToCart()
        : this.alertService.warn(MessageConfig.PRODUCT_COLOR_IS_NOT_SELECTED);
    }
    else {
      this.addToCart();
    }
  }
  }
  hideUniformPackDetails() {
    this.displayUniformPackDetails = false;
    this.uPackProductsMissingSelection = [];
    this.uPackProductsOutStock = [];
  }

  uniformPackSetup() {
    for(var product of this.uniformPackDetails){
      const colorMissing: boolean = product.colorOptions.length> 0 && (product.color == null || product.color == "")
      const sizeMissing: boolean = product.sizeOptions.length> 0 && (product.size == null || product.size == "")
      if(colorMissing || sizeMissing){
        this.uPackProductsMissingSelection.push([product.id, product.name]);//product id in case they put different products
      }
    }
    if(this.uPackProductsMissingSelection.length > 0){
      return;
    }
    this.displayUniformPackDetails = false;
    this.uPackProductsMissingSelection = [];
    this.addToCart();
  }

  addToCart() {
    console.log(this.uniformPackItemProducts)
    this.showSpinner = true;
    let item : UniformCartItem = new UniformCartItem();
    console.log(this.cartItem)
    item.uniformItem = this.cartItem;
    if(this.cartItem.isUniformPack != 0){
      this.uniformPackDetails.forEach(product => {
        let productItem : UniformPackItemProduct = new UniformPackItemProduct()
        productItem.productId = product.productId
        productItem.name = product.name
        productItem.size = product.size;
        productItem.itemId = this.cartItem.id;
        productItem.color = product.color;
        productItem.quantity = product.quantity;
        this.uniformPackItemProducts.push(productItem)
      });
    }
    item.uniformPackItemDetails = this.uniformPackItemProducts
    this.uniformPackItemProducts = [];
    console.log(item)
    this._store.dispatch(new AddToCartAction.AddUniformToCart(item))
  }

  displayDetails(product) {
    this.showDetails = true;
    this.product = product;
  }

  hideDetails() {
    this.showDetails = false;
  }

  checkQuantityLimit(product: UniformProduct) {
    if (product.stock > 0 && product.stock < product.quantity) {
      this.alertService.warn(MessageConfig.STOCK_EXCEEDED_WARNING);
    }
  }

  changeStock(product: UniformProduct) {
    this.product = product;
    let productStock = new ProductStock;
    productStock.productId = product.productId;

    if (product.colorOptions.length > 0) {
      let option = product.colorOptions.find(e => e.optionName == product.color);
      if (option) {
        productStock.colorAttributeId = option.optionId;
      }
    }

    if (product.sizeOptions.length > 0) {
      let option = product.sizeOptions.find(e => e.optionName == product.size);
      if (option) {
        productStock.sizeAttributeId = option.optionId;
      }
    }

    if ((productStock.sizeAttributeId || product.sizeOptions.length <= 0) && (productStock.colorAttributeId || product.colorOptions.length <= 0))
      this._store.dispatch(new ProductStockActions.GetUniformProductsStock(productStock))
  }

  changeUniformPackProductStock(product: UniformPackProduct) {
    
    let productStock = new ProductStock;
    productStock.productId = product.productId;
    

    if (product.colorOptions.length > 0) {
      let option = product.colorOptions.find(e => e.optionName == product.color);
      if (option) {
        productStock.colorAttributeId = option.optionId;
      }
    }

    if (product.sizeOptions.length > 0) {
      let option = product.sizeOptions.find(e => e.optionName == product.size);
      if (option) {
        productStock.sizeAttributeId = option.optionId;
      }
    }
    
    if ((productStock.sizeAttributeId || product.sizeOptions.length <= 0) && (productStock.colorAttributeId || product.colorOptions.length <= 0)){
      this.uniformProductsService.getProductsStock(productStock).subscribe(result => {
        console.log("Result: "+result)
        let uPackProductStock: number = result - product.quantity + 1
        console.log("uPackResult: "+uPackProductStock)
        let op: string = "";
        op += product.size? product.size: ""
        op += product.size && product.color? ", ": ""
        op += product.color? product.color: ""
        this.uPackProductsOutStock = this.uPackProductsOutStock.filter(osP => osP[0] != product.id)
        if(uPackProductStock <= 0){
          this.uPackProductsOutStock.push([product.id, product.name, op])
        }
        this.uPackProductsMissingSelection = this.uPackProductsMissingSelection.filter(mProduct => mProduct[0] != product.id)
      })
    }
      // this._store.dispatch(new ProductStockActions.GetUniformProductsStock(productStock))
  }



  ngOnDestroy() {
    if (this.subObsGetProducts) {
      this.subObsGetProducts.unsubscribe();
    }

    if (this.subObsAddUniformToCart) {
      this.subObsAddUniformToCart.unsubscribe();
    }
    if (this.subObsAddUniformToCart) {
      this.subObsAddUniformToCart.unsubscribe();
    }
    if (this.subObsGetProductStock) {
      this.subObsGetProductStock.unsubscribe();
    }
    if (this.subObsUniformBasketItem) {
      this.subObsUniformBasketItem.unsubscribe();
    }
  }
}
