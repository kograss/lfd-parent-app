import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { UniformProductsService } from './products.service'
import * as GetProductsAction from './action/get-products.action';
import * as GetUniformPackProductsAction from './action/get-uniform-pack-products.action';
import * as ProductStockActions from './action/get-uniform-product-stock.action'
import { map, switchMap } from 'rxjs/operators';


@Injectable()
export class UniformProductsEffects {

  constructor(
    private actions$: Actions,
    private uniformProductsService: UniformProductsService
  ) { }

  @Effect()
  getProducts$: Observable<Action> = this.actions$.pipe(ofType<GetProductsAction.GetProducts>
    (GetProductsAction.GETPRODUCTS)
    , map(action => action)
    , switchMap(
      payload => this.uniformProductsService.getProducts(payload.categoryId)
        .map(results => new GetProductsAction.GetProductsSuccess(results))
        .catch(err => Observable.of({ type: GetProductsAction.GETPRODUCTS_FAILURE, exception: err })
        ))
  );

  @Effect()
  getProductsStock$: Observable<Action> = this.actions$.pipe(ofType<ProductStockActions.GetUniformProductsStock>
    (ProductStockActions.GETUNIFORMPRODUCTSSTOCK)
    , map(action => action.payload)
    , switchMap(
      payload => this.uniformProductsService.getProductsStock(payload)
        .map(results => new ProductStockActions.GetUniformProductsStockSuccess(results))
        .catch(err =>
          Observable.of({ type: ProductStockActions.GETUNIFORMPRODUCTSSTOCK_FAILURE, exception: err })
        ))
  );

  @Effect()
  getUniformPackProducts$: Observable<Action> = this.actions$.pipe(ofType<GetUniformPackProductsAction.GetUniformPackProducts>
    (GetUniformPackProductsAction.GETUNIFORMPACKPRODUCTS)
    , map(action => action)
    , switchMap(
      payload => this.uniformProductsService.getUniformPackProduct(payload.uniformPackId)
        .map(results => new GetProductsAction.GetProductsSuccess(results))
        .catch(err => Observable.of({ type: GetProductsAction.GETPRODUCTS_FAILURE, exception: err })
        ))
  );
}