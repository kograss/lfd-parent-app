import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ProductStock } from '../model/productStock';



export const GETUNIFORMPRODUCTSSTOCK = '[PRODUCTS] GET UNIFORM STOCK';

export const GETUNIFORMPRODUCTSSTOCK_SUCCESS = '[PRODUCTS] GET UNIFORM STOCK SUCCESS';

export const GETUNIFORMPRODUCTSSTOCK_FAILURE = '[PRODUCTS] GET UNIFORM STOCK FAILURE';


export class GetUniformProductsStock implements Action {
  readonly type = GETUNIFORMPRODUCTSSTOCK;
  constructor(public payload: ProductStock) {
  }
}

export class GetUniformProductsStockSuccess implements Action {

  readonly type = GETUNIFORMPRODUCTSSTOCK_SUCCESS;
  constructor(public payload: any) {
    if (this.payload == null) {
    }
  }
}

export class GetUniformProductsStockFailure implements Action {

  readonly type = GETUNIFORMPRODUCTSSTOCK_FAILURE;
  constructor(public payload: any) {
  }

}

export type All = GetUniformProductsStock
  | GetUniformProductsStockSuccess | GetUniformProductsStockFailure 