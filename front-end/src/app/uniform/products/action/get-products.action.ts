import { UniformProduct } from './../model/products';
import { Action } from '@ngrx/store';

export const GETPRODUCTS = 'GET PRODUCTS';
export const GETPRODUCTS_SUCCESS = 'GET PRODUCTS SUCCESS';
export const GETPRODUCTS_FAILURE = 'GET PRODUCTS FAILURE';

export class GetProducts {
    readonly type = GETPRODUCTS;
    constructor(public categoryId:number) {
    }
}

export class GetProductsSuccess implements Action {
    readonly type = GETPRODUCTS_SUCCESS ;
    constructor(public uniformProduct : UniformProduct[] ) { 
        if(uniformProduct == null){
            this.uniformProduct = [];
        }
    }
}

export class GetProductsFailure implements Action {
    readonly type = GETPRODUCTS_FAILURE;
    constructor(public err: any) { }
}

export type All =  GetProducts 
    | GetProductsSuccess | GetProductsFailure