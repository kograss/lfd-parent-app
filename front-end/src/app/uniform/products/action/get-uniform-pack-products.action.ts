import { UniformPack, UniformProduct } from '../model/products';
import { Action } from '@ngrx/store';

export const GETUNIFORMPACKPRODUCTS = 'GET UNIFORMPACK PRODUCTS';
export const GETUNIFORMPACKPRODUCTS_SUCCESS = 'GET UNIFORMPACKS PRODUCTS SUCCESS';
export const GETUNIFORMPACKPRODUCTS_FAILURE = 'GET UNIFORMPACKS PRODUCTS FAILURE';

export class GetUniformPackProducts {
    readonly type = GETUNIFORMPACKPRODUCTS;
    constructor(public uniformPackId:number) {
    }
}

export class GetUniformPackProductsSuccess implements Action {
    readonly type = GETUNIFORMPACKPRODUCTS_SUCCESS ;
    constructor(public uniformProduct : UniformProduct[] ) { 
        if(uniformProduct == null){
            this.uniformProduct = [];
        }
    }
}

export class GetUniformPackProductsFailure implements Action {
    readonly type = GETUNIFORMPACKPRODUCTS_FAILURE;
    constructor(public err: any) { }
}

export type All =  GetUniformPackProducts 
    | GetUniformPackProductsSuccess | GetUniformPackProductsFailure