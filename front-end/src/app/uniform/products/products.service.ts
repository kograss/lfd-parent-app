import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductStock } from './model/productStock';

@Injectable()
export class UniformProductsService {
  private _getProducts = 'Uniform/GetProducts/{categoryId}';
  private _getUniformPackProducts = 'Uniform/GetUniformPackProducts/{id}';
  private _getProductStock = 'Uniform/GetProductStock/{productId}/{sizeAttributeId}/{colorAttributeId}';

  constructor(private _http: HttpClient) { }

  getProducts(categoryId: number): Observable<any> {
    return this._http.get(this._getProducts.replace('{categoryId}', categoryId.toString()));
  }
  
  getUniformPackProduct(id: number): Observable<any> {
    return this._http.get(this._getUniformPackProducts.replace('{id}', id.toString()));
  }

  // getProductsStock(getStock: ProductStock) {
  //   let headers = new Headers(
  //     {
  //       'Content-Type': 'application/json',
  //       'Access-Control-Allow-Origin': "true"
  //     });

  //   let options = { headers: headers };

  //   let body = JSON.stringify(getStock);
  //   var productsStock = this._http
  //     .get(this._getProductStock.replace("{productId}", getStock.productId.toString())
  //       .replace("{sizeAttributeId}", getStock.sizeAttributeId ? getStock.sizeAttributeId.toString() : null)
  //       .replace("{colorAttributeId}", getStock.colorAttributeId ? getStock.colorAttributeId.toString() : null)
  //       , options)
  //   return productsStock;
  // }
  getProductsStock(getStock: ProductStock): Observable<any> {
    
    var xp = this._getProductStock.replace("{productId}", getStock.productId.toString())
      .replace("{sizeAttributeId}", getStock.sizeAttributeId ? getStock.sizeAttributeId.toString() : null)
      .replace("{colorAttributeId}", getStock.colorAttributeId ? getStock.colorAttributeId.toString() : null);

    return this._http.get(xp);
  }
}
