import { Action } from '@ngrx/store';

export const DISPLAY_CART_ICON = 'DISPLAY CART ICON';
export const HIDE_CART_ICON = "HIDE CART ICON";

export class DisplayCartIcon {
    readonly type = DISPLAY_CART_ICON;
    constructor() {
    }
}

export class HideCartIcon{
    readonly type = HIDE_CART_ICON;
    constructor(){

    }
}


export type All =  DisplayCartIcon 
    | HideCartIcon