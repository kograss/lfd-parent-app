import { UniformCategoriesService } from './categories.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as GetCategoriesAction from './action/get-categories.action';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class UniformCategoriesEffects {

  constructor(
    private actions$: Actions,
    private uniformCategoriesService: UniformCategoriesService
  ) { }

  @Effect()
  getCategories$: Observable<Action> = this.actions$.pipe(ofType<GetCategoriesAction.GetCategories>
    (GetCategoriesAction.GETCATEGORIES)
    , map(action => action)
    , switchMap(
      payload => this.uniformCategoriesService.getCategories()
        .map(results => {
          var x = new GetCategoriesAction.GetCategoriesSuccess(results)
          return x;
        }
        )
        .catch(err => {
          return Observable.of({ type: GetCategoriesAction.GETCATEGORIES_FAILURE, exception: err })
        }
        ))
  );
}
