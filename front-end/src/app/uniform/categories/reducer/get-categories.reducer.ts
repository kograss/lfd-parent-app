import { UniformCategories } from './../model/categories';
import * as GetCategoriesAction from './../action/get-categories.action';

export interface State {    
   
    result: UniformCategories[],
    err: any
}

const initialState: State = {    
   
    result: [],
    err: {}
};

export function reducer(state = initialState, action: GetCategoriesAction.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetCategoriesAction.GETCATEGORIES_SUCCESS: {
            return {
                ...state,
                result: action.uniformCategories
            }
        }
        case GetCategoriesAction.GETCATEGORIES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
