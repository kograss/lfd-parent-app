import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UniformCategoriesService {
  private _getCategories = 'Uniform/GetUniformCategories/';

  constructor(private _http: HttpClient) { }

  getCategories(): Observable<any> {
    return this._http.get(this._getCategories);
  }
}
