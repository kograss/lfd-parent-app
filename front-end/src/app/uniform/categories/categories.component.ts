import { UniformCategories } from './model/categories';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Config } from './../config/config';
import { Observable } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from './../../reducers';
import * as GetCategoriesAction from './action/get-categories.action';
import * as GetProductAction from './../products/action/get-products.action';
import { UniformProduct } from './../products/model/products';

@Component({
  selector: 'uniform-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class UniformCategoriesComponent implements OnInit, OnDestroy {
  selectedCategoryId = 0;
  Categories: UniformCategories[] = [];

  obsGetCategories: Observable<UniformCategories[]>;
  subObsGetCategories: Subscription;
  obsGetCategoriesError: Observable<any>;
  subObsGetCategoriesError: Subscription;

  obsLoadProducts: Observable<UniformProduct[]>;
  subObsLoadProducts: Subscription;
  obsLoadProductsError: Observable<any>;
  subObsLoadProductsError: Subscription;

  quantity: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  categoryId: number;
  categoryName: string;
  name: string;
  displayCategories = true;
  showSpinner: boolean;

  constructor(private _store: Store<fromRoot.State>, private _router: Router,
    private route: ActivatedRoute) {
    this.obsGetCategories = this._store.select(fromRoot.selectFromUniformCategoriesStatusSuccess);
    this.obsGetCategoriesError = this._store.select(fromRoot.selectFromUniformCategoriesStatusFailure);
    this.obsLoadProducts = this._store.select(fromRoot.selectFromUniformProductStatusSuccess);
    this.obsLoadProductsError = this._store.select(fromRoot.selectFromUniformProductStatusFailure);
  }


  ngOnInit() {
    let isOnInit = true;
    this.showSpinner = true;
    this.route.params.subscribe(param => {
      this.categoryId = param['catId'];
      this.categoryName = param['catName'];
    })
    this._store.dispatch(new GetCategoriesAction.GetCategories);

    this.subObsGetCategories = this.obsGetCategories.subscribe(result => {
      if (!isOnInit && result) {
        this.Categories = result;
        this.showSpinner = false;
        if (this.Categories.length > 0) {
          let firstCategory = this.Categories[0];
          this.loadProducts(firstCategory.categoryId, firstCategory.name);
        }
      }
      this.showSpinner = false;
    })

    this.subObsGetCategoriesError = this.obsGetCategoriesError.subscribe(err => {
      if (!isOnInit && err) {
        this.showSpinner = false;
      }
    })

    this.subObsLoadProducts = this.obsLoadProducts.subscribe(uniform => {
      if (!isOnInit && uniform) {
        this.showSpinner = false;
      }
    })

    this.subObsLoadProductsError = this.obsLoadProductsError.subscribe(err => {
      if (!isOnInit && err) {
        this.showSpinner = false;
      }
    })

    if (this.categoryId && this.categoryName) {
      this.showSpinner = true;
      this.loadProducts(this.categoryId, this.categoryName);
    }

    isOnInit = false;
  }

  showCategories() {
    this.displayCategories = !this.displayCategories;
  }

  loadProducts(categoryId, name) {
    this.selectedCategoryId = categoryId;
    this.showSpinner = true;
    this._store.dispatch(new GetProductAction.GetProducts(categoryId));
    this._router.navigate(['uniformorder/home', { catId: categoryId, catName: name }]);
  }

  ngOnDestroy() {
    if (this.subObsGetCategories) {
      this.subObsGetCategories.unsubscribe();
    }
    if (this.subObsGetCategoriesError)
      this.subObsGetCategoriesError.unsubscribe();
    if (this.subObsLoadProducts)
      this.subObsLoadProducts.unsubscribe();
    if (this.subObsLoadProductsError)
      this.subObsLoadProductsError.unsubscribe();
  }
}
