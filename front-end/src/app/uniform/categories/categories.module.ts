import { UniformCategoriesEffects } from './categories.effects';
import { UniformCategoriesService } from './categories.service';
import { UniformCategoriesComponent } from './categories.component';
import { reducers } from './../../reducers';

import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        
        EffectsModule.forFeature([UniformCategoriesEffects]),
    ],
    declarations: [UniformCategoriesComponent],
    exports: [UniformCategoriesComponent],
    providers: [UniformCategoriesService]
})
export class UniformCategoriesModule {

}