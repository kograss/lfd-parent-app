import { UniformCategories } from './../model/categories';
import { Action } from '@ngrx/store';

export const GETCATEGORIES = 'GET CATEGORIES';
export const GETCATEGORIES_SUCCESS = 'GET CATEGORIES SUCCESS';
export const GETCATEGORIES_FAILURE = 'GET CATEGORIES FAILURE';

export class GetCategories {
    readonly type = GETCATEGORIES;
    constructor() {
    }
}

export class GetCategoriesSuccess implements Action {
    readonly type = GETCATEGORIES_SUCCESS ;
    constructor(public uniformCategories : UniformCategories[] ) { }
}

export class GetCategoriesFailure implements Action {
    readonly type = GETCATEGORIES_FAILURE;
    constructor(public err: any) { }
}

export type All =  GetCategories 
    | GetCategoriesSuccess | GetCategoriesFailure