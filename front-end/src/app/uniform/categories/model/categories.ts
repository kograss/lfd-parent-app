export class UniformCategories {
    categoryId : number;
    name : string;
    description : string;
    image : string;
    schoolId: number;
}