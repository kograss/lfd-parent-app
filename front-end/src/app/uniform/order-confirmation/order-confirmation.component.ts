import { Router } from '@angular/router';
import { OrderResult } from './../checkout/model/order-result';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../../reducers';
import * as ListParentActions from './../../user-profile/actions/list-user-action';
import * as DisplayCartIconAction from '../action/display-cart-icon-action';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})

export class UniformOrderConfirmationComponent implements OnInit, OnDestroy {
  orderId: number;
  obsPlaceOrder: Observable<number>;
  subObsPlaceOrder: Subscription;

  obsPlaceOrderUsingBalance: Observable<number>;
  subObsPlaceOrderUsingBalance: Subscription;


  constructor(private _store: Store<fromRoot.State>, private _route: Router) {
    this.obsPlaceOrder = this._store.select(fromRoot.selectFromCheckotSuccess);
    this.obsPlaceOrderUsingBalance = this._store.select(fromRoot.selectFromPayUsingBalanceSuccess);
  }

  ngOnInit() {
    this._store.dispatch(new DisplayCartIconAction.HideCartIcon());
    let isOnInit = true;

    this.subObsPlaceOrder = this.obsPlaceOrder.subscribe(result => {
      if (result) {
        this.orderId = result;
        this._store.dispatch(new ListParentActions.ListParent());
        localStorage.setItem('orderResult', result.toString());
        //console.log(result);
      }
      else {
        var orderResult: number = +localStorage.getItem('orderResult');
        if (orderResult) {
          this.orderId = orderResult;
        }
        else {
          this._route.navigate(['uniformorder/home']);
        }
      }
    })

    this.subObsPlaceOrderUsingBalance = this.obsPlaceOrderUsingBalance.subscribe(result => {
      if (result) {
        this.orderId = result;
        this._store.dispatch(new ListParentActions.ListParent());
        localStorage.setItem('orderResult', result.toString());
        //console.log(result);
      }
      else {
        var orderResult: number = +localStorage.getItem('orderResult');
        if (orderResult) {
          this.orderId = orderResult;
        }
        else {
          this._route.navigate(['uniformorder/home']);
        }
      }
    })

  }

  ngOnDestroy() {
    localStorage.removeItem('orderResult')
    this._store.dispatch(new DisplayCartIconAction.DisplayCartIcon());
    if (this.subObsPlaceOrder)
      this.subObsPlaceOrder.unsubscribe();
    
    if (this.subObsPlaceOrderUsingBalance)
      this.subObsPlaceOrderUsingBalance.unsubscribe();

  }
}
