import { UniformNews } from './../model/news';
import * as GetNewsAction from './../action/uniform-news.action';

export interface State {    
   
    result: UniformNews[],
    err: any
}

const initialState: State = {    
   
    result: [],
    err: {}
};

export function reducer(state = initialState, action: GetNewsAction.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetNewsAction.GETNEWS_SUCCESS: {
            return {
                ...state,
                result: action.uniformNews
            }
        }
        case GetNewsAction.GETNEWS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
