import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action, } from '@ngrx/store';
import { UniformNewsService } from './uniform-news.service'
import { Actions, Effect, ofType } from '@ngrx/effects'
import * as GetNewsAction from './action/uniform-news.action';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class UniformNewsEffects {

  constructor(
    private actions$: Actions,
    private uniformNewsService: UniformNewsService
  ) { }

  @Effect()
  getNews$: Observable<Action> = this.actions$.pipe(ofType<GetNewsAction.GetNews>
    (GetNewsAction.GETNEWS)
    , map(action => action)
    , switchMap(
      payload => this.uniformNewsService.getNews()
        .map(results => new GetNewsAction.GetNewsSuccess(results))
        .catch(err => Observable.of({ type: GetNewsAction.GETNEWS_FAILURE, exception: err })
        ))
  );
}