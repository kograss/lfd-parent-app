import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UniformNewsService {
  private _getNewsURL = 'Uniform/GetUniformNews/';

  constructor(private _http: HttpClient) { }

  getNews(): Observable<any> {
    return this._http.get(this._getNewsURL);
  }
}