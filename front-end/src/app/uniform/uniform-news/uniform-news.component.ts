

import { UniformNews } from './model/news';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Config } from './../config/config';
import { Observable } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from './../../reducers';
import * as GetNewsAction from './action/uniform-news.action';


@Component({
  selector: 'uniform-news',
  templateUrl: './uniform-news.component.html',
  styleUrls: ['./uniform-news.component.css']
})
export class UniformNewsComponent implements OnInit, OnDestroy {

  uniformNews: UniformNews[] = [];

  obsGetNews: Observable<UniformNews[]>;
  subObsGetNews: Subscription;



  isNewsEmpty = false;
  hideNews = true;
  newsContent: any;
  showSpinner: boolean;
  showDetails = false;

  constructor(private _store: Store<fromRoot.State>, private _router: Router,
    private route: ActivatedRoute) {
    this.obsGetNews = this._store.select(fromRoot.selectFromUniformNewsStatusSuccess);

  }


  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new GetNewsAction.GetNews);



    this.subObsGetNews = this.obsGetNews.subscribe(result => {
      if (!isOnInit)

        if (result.length == 0) {
          this.isNewsEmpty = true;
        }
        else {
          this.isNewsEmpty = false;
        }

      this.uniformNews = result;
    })

    isOnInit = false;
  }


  ngOnDestroy() {
    if (this.subObsGetNews) {
      this.subObsGetNews.unsubscribe();
    }
  }

  displayDetails(news) {
    this.newsContent = news.description;
    this.showSpinner = false;
    this.showDetails = true;
  }

  hideDetails() {
    this.showDetails = false;
  }
}
