import { reducers } from './../../reducers';
import { UniformNewsComponent } from './uniform-news.component';
import { UniformNewsService } from './uniform-news.service';
import { UniformNewsEffects } from './uniform-news.effects';

import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        
        EffectsModule.forFeature([UniformNewsEffects]),
    ],
    declarations: [UniformNewsComponent],
    exports: [UniformNewsComponent],
    providers: [UniformNewsService]
})
export class UniformNewsModule {

}