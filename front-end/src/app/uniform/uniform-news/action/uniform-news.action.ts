import { UniformNews } from './../model/news';
import { Action } from '@ngrx/store';

export const GETNEWS = 'GET NEWS';
export const GETNEWS_SUCCESS = 'GET NEWS SUCCESS';
export const GETNEWS_FAILURE = 'GET NEWS FAILURE';

export class GetNews {
    readonly type = GETNEWS;
    constructor() {
    }
}

export class GetNewsSuccess implements Action {
    readonly type = GETNEWS_SUCCESS ;
    constructor(public uniformNews : UniformNews[] ) { 
        if(this.uniformNews == null){
            this.uniformNews = [];
        }
    }
}

export class GetNewsFailure implements Action {
    readonly type = GETNEWS_FAILURE;
    constructor(public err: any) { }
}

export type All =  GetNews 
    | GetNewsSuccess | GetNewsFailure