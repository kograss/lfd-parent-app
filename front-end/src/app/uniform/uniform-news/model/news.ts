export class UniformNews{
    newsID : number;
    description : string;
    dateEntered : Date;
    subject : string;
}