import { CollectionOption } from "../../cart/model/collection-option";

export class OrderItems {
    stripeToken: string;
    amount: number;
    basketId: number;
    firstName: string;
    studentId: number;
    studentClass: string;
    comments: string;
    topupMethod:string;
    saveCard: boolean;
    collectionOption: CollectionOption;
}