import { DeleteCartModel } from '../cart/model/update-cart';
import { Parent } from '../../user-profile/model/parent';
import { OrderResult } from './model/order-result';
import { Children } from '../../children/model/children';
import { OrderItems } from './model/order-Items';
import { StripeComponent } from '../../stripe/stripe.component';
import { UniformBasketItem } from '../cart/model/uniform_basketitem';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Config } from '../../config/config';
import { Observable } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, Output, Input, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import * as fromRoot from '../../reducers';
import * as CheckoutActions from './action/uniform-checkout.action';
import * as ListCartItemsActions from '../cart/action/get-cart-item.action';
import * as DeleteCartItemActions from '../cart/action/delete-cart-item.acion';
import { AlertComponent } from '../../alert/alert.component';
import { AlertService } from '../../alert/alertService';
import * as DisplayCartIconAction from '../action/display-cart-icon-action';
import * as GetBasketItemAction from '../cart/action/get-cart-item.action';
import * as AddToCartAction from '../cart/action/add-to-cart.action';
import { CheckoutService } from './uniform-checkout.service';
import * as PayUsingBalanceActions from './action/payUsingBalance.action';
import { CollectionOption, UniformAddress } from '../cart/model/collection-option';
import { UniformCartService } from '../cart/cart.service';
import { resolve } from 'path';

@Component({
  selector: 'uniform-checkout',
  templateUrl: './uniform-checkout.component.html',
  styleUrls: ['./uniform-checkout.component.css']
})
export class UniformCheckoutComponent implements OnInit, OnDestroy {

  @Input()
  commentsParent: string;

  @Input()
  selectedStudentParent: Children;

  @Input()
  selectedCollectionOption: CollectionOption;

  @Input()
  homeDeliveryAddress: UniformAddress;

  basketItems: UniformBasketItem[] = [];
  basketItem: UniformBasketItem;
  obsUniformBasketItem: Observable<UniformBasketItem[]>;
  subObsUniformBasketItem: Subscription;
  orderItems: OrderItems = new OrderItems;
  basketId: number;


  cartTotal: number = 0;
  parent: Parent;
  payButtonClicked: boolean;
  deleteItem: DeleteCartModel;
  deleteType: string;

  obsPlaceOrder: Observable<number>;
  subObsPlaceOrder: Subscription;
  obsPlaceOrderError: Observable<any>;
  subObsPlaceOrderError: Subscription;

  obsParent: Observable<Parent>;
  subObsParent: Subscription;
  obsParentError: Observable<any>;
  subObsParentError: Subscription;

  obsStripePopUp: Observable<boolean>;
  subObsStripePopUp: Subscription;

  obsDeleteItem: Observable<UniformBasketItem[]>;
  subObsDeleteItem: Subscription;

  message: string;
  cartMode = "CHECKOUT";
  currency: string;
  showSpinner: boolean;
  topupmethod: string;
  bank: string;
  bankAccount: string;
  bankBSB: string;
  shop: string;
  accountEmail: string;
  showManual: boolean;
  showDirectDeposite: boolean;
  isStudentOptional: any;
  isExternalRecipientRequired: any;
  isPayUsingBalanceEnabled: boolean = false;

  paymentAmount: number;

  obsCancelConfirmAlert: Observable<Boolean>;
  subCancelConfirmAlert: Subscription;

  cartTotalLessThanCredit: boolean;
  obsPlaceOrderUsingBalance: Observable<number>;
  subObsPlaceOrderUsingBalance: Subscription;
  obsPlaceOrderErrorUsingBalance: Observable<any>;
  subObsPlaceOrderErrorUsingBalance: Subscription;

  showStripeModal: boolean = false;
  stripePaymentData: any;
  amount: number;

  customerStripeDetails: any;
  translog: any;
  stripeToken: any;
  cardSavmentConfirmation: boolean = false;
  paymentIntent: any;

  constructor(
    private _store: Store<fromRoot.State>,
    private _router: Router,
    private route: ActivatedRoute,
    private config: Config,
    private alertService: AlertService,
    private checkoutService: CheckoutService,
    private uniformCartService: UniformCartService
  ) {

    this.obsUniformBasketItem = this._store.select(fromRoot.selectFromGetBasketItemSuccess);

    this.obsPlaceOrder = this._store.select(fromRoot.selectFromCheckotSuccess)
    this.obsPlaceOrderError = this._store.select(fromRoot.selectFromCheckotFailure);
    this.obsParent = this._store.select(fromRoot.selectListUserSuccess);
    this.obsParentError = this._store.select(fromRoot.selectListUserFailure);
    this.obsStripePopUp = this._store.select(fromRoot.selectStripePopupClosed);
    this.obsDeleteItem = this._store.select(fromRoot.selectFromDeleteItemSuccess);

    this.currency = this.config.getCurrency();
    this._store.dispatch(new DisplayCartIconAction.HideCartIcon());

    this.obsCancelConfirmAlert = this._store.select(fromRoot.getAlertCancelConfirmStatus);
    this.obsPlaceOrderUsingBalance = this._store.select(fromRoot.selectFromPayUsingBalanceSuccess);
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new DisplayCartIconAction.HideCartIcon());

    this.basketId = +localStorage.getItem("basketId");

    this.showManual = false;
    this.showDirectDeposite = false;
    if (this.basketId) {
      this._store.dispatch(new ListCartItemsActions.GetBasketItem(this.basketId));
    }

    this.getSchoolDetails();
    this.getRequiredDetails();

    this.subObsParent = this.obsParent.subscribe(result => {
      if (result) {
        this.parent = result;
        if (this.cartTotal > this.parent.credit) {
          this.cartTotalLessThanCredit = false;
        }
        else {
          this.cartTotalLessThanCredit = true;
        }
      }
    })

    this.subObsParentError = this.obsParentError.subscribe(err => {
      if (err) {

      }
    })

    this.subObsUniformBasketItem = this.obsUniformBasketItem.subscribe(result => {
      if (result) {
        this.cartTotal = 0;
        this.basketItems = result;
        this.basketItems.forEach(basketItem => {
          this.cartTotal += basketItem.price * basketItem.quantity
        })
      }
      else {
        this.cartTotal = 0;
        this.basketItems = [];
      }

      if (this.cartTotal > this.parent.credit) {
        this.cartTotalLessThanCredit = false;
      }
      else {
        this.cartTotalLessThanCredit = true;
      }

    })

    this.subObsPlaceOrder = this.obsPlaceOrder.subscribe(result => {
      if (result && result != 0 && !isOnInit) {
        this.showSpinner = false;
        localStorage.removeItem('basketId');
        this.basketItems = [];
        localStorage.setItem('orderResult', result.toString());

        this._store.dispatch(new AddToCartAction.ClearCart());

        this._router.navigate(['uniformorder/order-confirmation']);
        //  this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate,this.eventId));
      }
    })

    this.subObsPlaceOrderError = this.obsPlaceOrderError.subscribe(err => {
      if (!isOnInit && err) {
        console.log(err);
        if (err.error) {
          this.alertService.error(err.error);
        }
        else {
          this.alertService.error("Something went wrong. Please try again");
        }
        this.showSpinner = false;
      }
    })



    this.subObsPlaceOrderUsingBalance = this.obsPlaceOrderUsingBalance.subscribe(result => {
      if (result && result != 0 && !isOnInit) {
        this.showSpinner = false;
        this.basketItems = [];
        this._store.dispatch(new AddToCartAction.ClearCart());
        localStorage.removeItem('basketId');
        localStorage.setItem('orderResult', result.toString());
        this._router.navigate(['uniformorder/order-confirmation']);
        //  this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate,this.eventId));
      }
    })


    this.subObsStripePopUp = this.obsStripePopUp.subscribe(res => {
      if (res) {
        this.payButtonClicked = false;
      }
    })

    this.subCancelConfirmAlert = this.obsCancelConfirmAlert.subscribe(result => {
      if (!isOnInit && this.cardSavmentConfirmation == true) {
        this.placeOrder(this.amount, this.stripeToken, "CC", result.valueOf());
        this.cardSavmentConfirmation = null;
      }
      else if (!isOnInit && this.cardSavmentConfirmation == false) {
        this.cardSavmentConfirmation = null;
        if(result == true){
          this.getPaymentIntent(this.amount, "CC")
        }else{
          this.showStripeModal = true
        }
        
      }
      
    })

    isOnInit = false;
  }

  onStudentChanged(selectedStudent: Children): void {
    this.selectedStudentParent = selectedStudent;
    console.log(this.selectedStudentParent);
  }

  onCommentUpdate(comments: string): void {
    this.commentsParent = comments;
  }

  onCollectionOptionChanged(selectedCollectionOption: CollectionOption): void {
    this.cartTotal =  this.cartTotal - (this.selectedCollectionOption?.fee | 0) + selectedCollectionOption.fee;
    this.selectedCollectionOption = selectedCollectionOption;
    
  }

  onUniformAddressChanged(homeDeliveryAddress: UniformAddress): void{
    this.homeDeliveryAddress = homeDeliveryAddress;
  }

  stripePay() {
    this.showManual = false;
    this.showDirectDeposite = false;
    let amount = this.cartTotal;
    this.payButtonClicked = true;
    amount += amount * 0.015 + 0.30;
    amount = Math.trunc((amount + 0.00001) * 100) / 100;
    amount = amount * 100;

    this.amount = amount;
    this.stripePaymentData = {
      name: "Uniform",
      amount: this.amount,
      email: this.parent.email
    };

    this.checkoutService.GetCustomerStripeDetails().subscribe(result => {
      this.customerStripeDetails = result
      this.showSpinner = false;
      if(this.customerStripeDetails != null){
        this.cardSavmentConfirmation = false;
        this.alertService.confirm("Would you like to use the saved card XXXX XXXX XXXX " + this.customerStripeDetails.cardLast4
        + " for this payment?")
        this.payButtonClicked = false;
      }else{
        this.showStripeModal = true;
      }
      
    });

  }

  

  stripeCallback(token) {
    this.showStripeModal = false;
    this.stripeToken = token
    this.cardSavmentConfirmation = true
    if(this.customerStripeDetails != null)
      this.alertService.confirm("Would to like to replace your saved card details with these card details?");
    else
      this.alertService.confirm("Would you like to save this card for future payment?");
  }

  showManualPay() {
    this.showManual = true;
  }

  hideManualPay() {
    this.showManual = false;
  }

  showDirectDeposit() {
    this.showDirectDeposite = true;
  }

  hideDirectDeposit() {
    this.showDirectDeposite = false;
  }

  manualPay() {
    let amount = this.cartTotal;
    this.payButtonClicked = true;
    amount += amount * 0.015 + 0.30;
    amount = Math.trunc((amount + 0.00001) * 100) / 100;
    amount = amount * 100;

    if (this.selectedStudentParent) {
      if (this.parent) {
        this.placeOrder(amount, "", "M");
      }
    }
  }

  updateAddress(){
    return new Promise(resolve => {
      if(this.selectedCollectionOption?.type.toLowerCase() == 'home delivery'){
        let oldAddress = localStorage.getItem('oldDeliveryAddress')
      console.log(this.homeDeliveryAddress)
      if(!oldAddress){
        this.showSpinner = true
        this.uniformCartService.addUniformAddress(this.homeDeliveryAddress).subscribe(result => {
          this.homeDeliveryAddress = result
          this.showSpinner = false
          resolve(true)
        })
      } 
      else if(oldAddress != JSON.stringify(this.homeDeliveryAddress)){
        this.showSpinner = true
        this.uniformCartService.updateUniformAddress(this.homeDeliveryAddress).subscribe(result => {
          this.homeDeliveryAddress = result
          this.showSpinner = false
          resolve(true)
        })
      }else{
        resolve(true)
      }
      }else{
        resolve(true)
      }
      
    })
    
    
  }

  async placeOrder(amount: number, token: any, topup: string, saveCard: boolean = false) {
    //   this.showSpinner = true;
    await this.updateAddress();
    let student: Children[];
    if (this.selectedStudentParent) {
      this.orderItems.amount = amount;
      this.orderItems.basketId = this.basketId;
      this.orderItems.comments = this.commentsParent;
      this.orderItems.stripeToken = token.id;
      this.orderItems.studentClass = this.selectedStudentParent.studentClass;
      this.orderItems.studentId = this.selectedStudentParent.studentId;
      this.orderItems.firstName = this.selectedStudentParent.firstName;
      this.orderItems.collectionOption = this.selectedCollectionOption;
      this.orderItems.topupMethod = topup;
      this.orderItems.saveCard = saveCard;
      this.showSpinner = true;
      this._store.dispatch(new CheckoutActions.UniformPlaceOrder(this.orderItems));
    }
  }

  async getPaymentIntent(amount: number, topup: string){
    if (this.selectedStudentParent) {
      await this.updateAddress();
      this.orderItems.amount = amount;
      this.orderItems.basketId = this.basketId;
      this.orderItems.comments = this.commentsParent;
      this.orderItems.studentClass = this.selectedStudentParent.studentClass;
      this.orderItems.studentId = this.selectedStudentParent.studentId;
      this.orderItems.firstName = this.selectedStudentParent.firstName;
      this.orderItems.collectionOption = this.selectedCollectionOption;
      this.orderItems.topupMethod = topup;
      this.showSpinner = true;
      
      this.checkoutService.GetPaymentIntent(this.orderItems).subscribe(result => {
        this.translog = result[Object.keys(result)[0]];      
        this.paymentIntent = JSON.parse(Object.keys(result)[0])
        this.translog.PGTransId = this.paymentIntent.id
        this.checkoutService.placeOrderAfterPayment(this.orderItems, this.translog).subscribe(result => {
          this.showSpinner = false;
          if (result && result != 0) {
            this.basketItems = [];
            this._store.dispatch(new AddToCartAction.ClearCart());
            localStorage.removeItem('basketId');
            localStorage.setItem('orderResult', result.toString());
            this._router.navigate(['uniformorder/order-confirmation']);
          }
          else {
            if (result._body) {
              this.alertService.error(result._body);
            }
            else {
              this.alertService.error("Something went wrong. Please try again");
            }
            this.showSpinner = false;
          }
        });
      })
    }
  }

  deleteCartItem(basketItem: UniformBasketItem, deleteProduct: boolean, status: string,) {
    this.deleteType = status;
    this.basketItem = basketItem;

    let item = new DeleteCartModel();
    item.deleteProduct = deleteProduct;
    item.status = status;
    item.basketId = this.basketId;
    item.basketItemId = basketItem.id;
    this._store.dispatch(new DeleteCartItemActions.UniformDeleteCartItem(item))
  }

  getSchoolDetails() {

    this.checkoutService.getSchoolDetails().subscribe(result => {
      if (result) {
        localStorage.setItem("TopupMethod", result.TopupMethod);
        localStorage.setItem("BankName", result.Bank);
        localStorage.setItem("BankAccount", result.BankAccount);
        localStorage.setItem("BankBSB", result.BankBSB);
        localStorage.setItem("Shop", result.Shop);
        localStorage.setItem("AccountEmail", result.AccountEmail);

        if (result.isStudentOptional) {
          localStorage.setItem("isStudentOptional", result.isStudentOptional.toString());
        }
        if (result.isExternalRecipientRequired) {
          localStorage.setItem("isExternalRecipientRequired", result.isExternalRecipientRequired.toString());
        }
        if (result.isPayUsingBalanceEnabled) {
          this.isPayUsingBalanceEnabled = true;
        }
        else {
          this.isPayUsingBalanceEnabled = false;
        }

        this.getRequiredDetails();
      }
    },
      err => {
        console.log(err);
      }
    )
  }

  getRequiredDetails() {
    this.topupmethod = localStorage.getItem("TopupMethod");
    this.bank = localStorage.getItem("BankName");
    this.bankAccount = localStorage.getItem("BankAccount");
    this.bankBSB = localStorage.getItem("BankBSB");
    this.shop = localStorage.getItem("Shop");
    this.accountEmail = localStorage.getItem("AccountEmail");
    this.isExternalRecipientRequired = localStorage.getItem("isExternalRecipientRequired");
    this.isStudentOptional = localStorage.getItem("isStudentOptional");

  }

  async pay() {
    await this.updateAddress();
    this.payButtonClicked = true;
    this.showSpinner = true;
    let amount = this.cartTotal;
    this.orderItems.amount = amount;
    this.orderItems.basketId = this.basketId;
    this.orderItems.comments = this.commentsParent;
    this.orderItems.collectionOption = this.selectedCollectionOption;
    if (this.selectedStudentParent) {
      this.orderItems.studentClass = this.selectedStudentParent.studentClass;
    }

    if (this.selectedStudentParent) {
      this.orderItems.studentId = this.selectedStudentParent.studentId;
    }
    this._store.dispatch(new PayUsingBalanceActions.PayUsingBalance(this.orderItems));
  }

  ngOnDestroy() {
    this._store.dispatch(new DisplayCartIconAction.DisplayCartIcon());
    if (this.subObsUniformBasketItem)
      this.subObsUniformBasketItem.unsubscribe();
    if (this.subObsParent)
      this.subObsParent.unsubscribe();
    if (this.subObsParentError)
      this.subObsParentError.unsubscribe();
    if (this.subObsPlaceOrder)
      this.subObsPlaceOrder.unsubscribe();
    if (this.subObsPlaceOrderError)
      this.subObsPlaceOrderError.unsubscribe();
    if (this.subObsDeleteItem)
      this.subObsDeleteItem.unsubscribe();
    if (this.subCancelConfirmAlert)
      this.subCancelConfirmAlert.unsubscribe();
    if (this.subObsPlaceOrderUsingBalance)
      this.subObsPlaceOrderUsingBalance.unsubscribe();
    if (this.subObsPlaceOrderErrorUsingBalance)
      this.subObsPlaceOrderErrorUsingBalance.unsubscribe();
  }
}
