import { OrderItems } from './../model/order-Items';
import { OrderResult } from './../model/order-result';
import { Action } from '@ngrx/store';


export const PLACEORDERUSINGBALANCE = 'PLACEORDERUSINGBALANCE ACTION';
export const PLACEORDER_SUCCESSUSINGBALANCE = 'PLACEORDERUSINGBALANCE SUCCESS';
export const PLACEORDER_FAILUREUSINGBALANCE = 'PLACEORDERUSINGBALANCE FAILURE';
export const CLEARORDERRESULTUSINGBALANCE = 'CLEAR ORDER RESULT USINGBALANCE';



export class PayUsingBalance {
    orderItems:OrderItems;
    readonly type = PLACEORDERUSINGBALANCE;
    constructor(orderItems: OrderItems) {
        this.orderItems = orderItems;
    }
}

export class PayUsingBalanceSuccess implements Action {
    readonly type = PLACEORDER_SUCCESSUSINGBALANCE;
    constructor(public paymentResult: number) {
    }
}

export class PayUsingBalanceFailure implements Action {
    readonly type = PLACEORDER_FAILUREUSINGBALANCE;
    constructor(public err: any) {
    }

}

export class ClearOrderResult implements Action {
    readonly type = CLEARORDERRESULTUSINGBALANCE;
    constructor() {
    }

}


export type All = PayUsingBalance
    | PayUsingBalanceSuccess | PayUsingBalanceFailure | ClearOrderResult
