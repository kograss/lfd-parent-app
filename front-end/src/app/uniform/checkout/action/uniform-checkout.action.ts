import { OrderItems } from './../model/order-Items';
import { OrderResult } from './../model/order-result';
import { Action } from '@ngrx/store';


export const UNIFORM_PLACEORDER = 'UNIFORM PLACEORDER ACTION';
export const UNIFORM_PLACEORDER_SUCCESS = 'UNIFORM PLACEORDER SUCCESS';
export const UNIFORM_PLACEORDER_FAILURE = 'UNIFORM PLACEORDER FAILURE';
export const UNIFORM_CLEARORDERRESULT = 'UNIFORM CLEAR ORDER RESULT';



export class UniformPlaceOrder {
    orderItems:OrderItems;
    readonly type = UNIFORM_PLACEORDER;
    constructor(orderItems: OrderItems) {
        this.orderItems = orderItems;
    }
}

export class UniformPlaceOrderSuccess implements Action {
    readonly type = UNIFORM_PLACEORDER_SUCCESS;
    constructor(public paymentResult: number) {
    }
}

export class UniformPlaceOrderFailure implements Action {
    readonly type = UNIFORM_PLACEORDER_FAILURE;
    constructor(public err: any) {
    }

}

export class UniformClearOrderResult implements Action {
    readonly type = UNIFORM_CLEARORDERRESULT;
    constructor() {
    }

}


export type All = UniformPlaceOrder
    | UniformPlaceOrderSuccess | UniformPlaceOrderFailure | UniformClearOrderResult
