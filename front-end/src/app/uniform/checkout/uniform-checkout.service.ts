import { OrderItems } from './model/order-Items';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CheckoutService {
  private _checkoutUrl = "Uniform/StripePayment";
  private _getSchool = "Uniform/getUniformSchoolDetails";
  private _payUsingBalanceUrl = 'Uniform/PayUsingBalance';
  private _getPaymentIntentUrl = 'Uniform/GetPaymentIntent';
  private _placeOrderAfterPaymentUrl = 'Uniform/PlaceOrderAfterPayment';
  public _stripeCustomerDetailsUrl = 'GetCustomerStripeDetails';

  constructor(private _http: HttpClient) { }

  placeOrder(orders: OrderItems): Observable<any> {
    let body = orders;
    let bodyString = JSON.stringify(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._http
      .post(this._checkoutUrl, bodyString, options);
  }

  getSchoolDetails(): Observable<any> {
    return this._http.get(this._getSchool);
  }

  placeOrderUsingBalance(orders: OrderItems): Observable<any> {
    let body = orders;
    let bodyString = JSON.stringify(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._http
      .post(this._payUsingBalanceUrl, bodyString, options);
  }

  GetPaymentIntent(orders: OrderItems): Observable<any>  {
    let body = orders;
    let bodyString = JSON.stringify(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._http
      .post(this._getPaymentIntentUrl, bodyString, options);
}

GetCustomerStripeDetails() {
  return this._http.get(this._stripeCustomerDetailsUrl)
}
placeOrderAfterPayment(orders: OrderItems, transLog:any): Observable<any> {
  let body = {
    uniformPaymentRequest: orders,
    log: transLog,
}
  let bodyString = JSON.stringify(body);
  let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  let options = { headers: headers };
  return this._http
    .post(this._placeOrderAfterPaymentUrl, bodyString, options);
}
}