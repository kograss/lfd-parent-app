import { CheckoutService } from './uniform-checkout.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as PayUsingBalanceActions from './action/payUsingBalance.action';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class PayUsingBalanceEffects {
  constructor(
    private actions$: Actions,
    private checkoutService: CheckoutService
  ) { }

  @Effect()
  placeOrder$: Observable<Action> = this.actions$.pipe(ofType<PayUsingBalanceActions.PayUsingBalance>
    (PayUsingBalanceActions.PLACEORDERUSINGBALANCE)
    , map(action => action)
    , switchMap(
      payload => this.checkoutService.placeOrderUsingBalance(payload.orderItems)
        .map(results => new PayUsingBalanceActions.PayUsingBalanceSuccess(results))
        .catch(err =>
          Observable.of(new PayUsingBalanceActions.PayUsingBalanceFailure(err))
        ))
  );
}
