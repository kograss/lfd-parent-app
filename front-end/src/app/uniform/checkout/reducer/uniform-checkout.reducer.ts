import { OrderResult } from '../model/order-result';
import * as CheckoutActions from './../action/uniform-checkout.action';
export interface State {
    result: number,
    err: any
}

const initialState: State = {
    result: 0,
    err: {}
};

export function reducer(state = initialState, action: CheckoutActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case CheckoutActions.UNIFORM_PLACEORDER_SUCCESS: {
            return {
                ...state,
                result: action.paymentResult
            }
        }
        case CheckoutActions.UNIFORM_PLACEORDER_FAILURE: {
            return {
                ...state,
                err: action.err 
            }
        }

        case CheckoutActions.UNIFORM_CLEARORDERRESULT: {
            return {
                ...state,
                result: 0
            }
        }

        default: {
            return state;
        }

    }

}
