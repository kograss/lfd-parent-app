import { OrderResult } from './../model/order-result';
import * as PayUsingBalanceActions from './../action/payUsingBalance.action';
export interface State {
    result: number,
    err: any
}

const initialState: State = {
    result: 0,
    err: {}
};

export function reducer(state = initialState, action: PayUsingBalanceActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case PayUsingBalanceActions.PLACEORDER_SUCCESSUSINGBALANCE: {
            return {
                ...state,
                result: action.paymentResult
            }
        }
        case PayUsingBalanceActions.PLACEORDER_FAILUREUSINGBALANCE: {
            return {
                ...state,
                err: action.err 
            }
        }

        case PayUsingBalanceActions.CLEARORDERRESULTUSINGBALANCE: {
            return {
                ...state,
                result: 0
            }
        }

        default: {
            return state;
        }

    }

}
