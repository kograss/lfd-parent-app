import { CheckoutService } from './uniform-checkout.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as CheckoutActions from './action/uniform-checkout.action';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class CheckoutEffects {

  constructor(
    private actions$: Actions,
    private checkoutService: CheckoutService
  ) { }

  @Effect()
  placeOrder$: Observable<Action> = this.actions$.pipe(ofType<CheckoutActions.UniformPlaceOrder>
    (CheckoutActions.UNIFORM_PLACEORDER),
    map(action => action),
    switchMap(
      payload => this.checkoutService.placeOrder(payload.orderItems)
        .map(results => new CheckoutActions.UniformPlaceOrderSuccess(results))
        .catch(err =>
          Observable.of(new CheckoutActions.UniformPlaceOrderFailure(err))
        ))
  );
}