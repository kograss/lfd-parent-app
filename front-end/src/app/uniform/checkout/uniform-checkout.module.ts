import { UniformOrderConfirmationComponent } from '../order-confirmation/order-confirmation.component';
import { UniformCheckoutComponent } from './uniform-checkout.component';
import { CheckoutEffects } from './uniform-checkout.effects';
import { CheckoutService } from './uniform-checkout.service';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AlertService } from '../../alert/alertService';
import { UniformCartModule } from '../cart/cart.module';
import { PayUsingBalanceEffects } from './payUsingBalance.effects';
import { StripeModule } from '../../stripe/stripe.module';
import { Config } from '../../config/config';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    StripeModule,
    UniformCartModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    EffectsModule.forFeature([CheckoutEffects, PayUsingBalanceEffects]),
  ],
  declarations: [UniformCheckoutComponent, UniformOrderConfirmationComponent],
  exports: [UniformCheckoutComponent],
  providers: [CheckoutService, AlertService]
})

export class UniformCheckoutModule {

}
