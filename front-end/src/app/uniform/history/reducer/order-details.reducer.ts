import { OrderDetails } from './../model/order-details';
import * as GetOrderDetailsActions from './../action/order-details.action';

export interface State{
    result : OrderDetails[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: GetOrderDetailsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetOrderDetailsActions.GETORDERDETAILS_SUCCESS: {
            return {
                ...state,
                result: action.ordersDetails
            }
        }
        case GetOrderDetailsActions.GETORDERDETAILS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
        
        default: {
            return state;
        }
    }
}
