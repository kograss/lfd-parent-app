import { UniformOrderHistory } from './../model/order-history';
import * as GetOrderHistoryActions from './../action/order-history.action';

export interface State{
    result : UniformOrderHistory[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: GetOrderHistoryActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case GetOrderHistoryActions.GETORDERHISTORY_SUCCESS: {
            return {
                ...state,
                result: action.ordersHistory
            }
        }
        case GetOrderHistoryActions.GETORDERHISTORY_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
        
        default: {
            return state;
        }
    }
}
