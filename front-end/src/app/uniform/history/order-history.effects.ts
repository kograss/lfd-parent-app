import { OrderHistoryService } from './order-history.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as GetOrderHistoryActions from './action/order-history.action';
import * as GetOrderDetailsActions from './action/order-details.action';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class OrderHistoryEffects {

  constructor(
    private actions$: Actions,
    private orderHistoryService: OrderHistoryService
  ) { }

  @Effect()
  getOrderHistory$: Observable<Action> = this.actions$.pipe(ofType<GetOrderHistoryActions.GetOrderHistory>
    (GetOrderHistoryActions.GETORDERHISTORY),
    map(action => action),
    switchMap(
      payload => this.orderHistoryService.getOrderHistory()
        .map(results => new GetOrderHistoryActions.GetOrderHistorySuccess(results))
        .catch(err =>
          Observable.of({ type: GetOrderHistoryActions.GETORDERHISTORY_FAILURE, exception: err })
        ))
  );

  @Effect()
  getOrderDetails$: Observable<Action> = this.actions$.pipe(ofType<GetOrderDetailsActions.GetOrderDetails>
    (GetOrderDetailsActions.GETORDERDETAILS),
    map(action => action),
    switchMap(
      payload => this.orderHistoryService.getOrderDetails(payload.orderId)
        .map(results => new GetOrderDetailsActions.GetOrderDetailsSuccess(results))
        .catch(err => Observable.of({ type: GetOrderDetailsActions.GETORDERDETAILS_FAILURE, exception: err })
        ))
  );
}
