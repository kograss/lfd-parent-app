import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class OrderHistoryService {
  private _getOrderHistoryUrl = "Uniform/GetUniformOrders/{year}";
  private _getOrderDetails = "Uniform/GetUniformOrderDetails/{orderId}";

  constructor(private _http: HttpClient) {
  }

  getOrderHistory(): Observable<any> {
    return this._http
      .get(this._getOrderHistoryUrl.replace("{year}", '2020'));
  }

  getOrderDetails(orderId: number): Observable<any> {
    return this._http.get(this._getOrderDetails.replace('{orderId}', orderId.toString()));
  }
}

