import { OrderDetails } from './model/order-details';
import { Config } from './../config/config';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { UniformOrderHistory } from './model/order-history';
import * as GetOrderHistoryActions from './action/order-history.action';
import * as GetOrderDetailsActions from './action/order-details.action';
import * as fromRoot from './../../reducers';

@Component({
    selector: 'order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['./order-history.component.css']
})
export class UniformOrderHistoryComponent implements OnInit, OnDestroy {

    uniformOrders: UniformOrderHistory[];
    orderDetails: OrderDetails[];
    orderHistory: UniformOrderHistory;
    stage: string;
    total: number;

    orderId: number;
    comment: string;
    showSpinner: boolean;

    ObsOrderHistory: Observable<UniformOrderHistory[]>;
    SubObsOrderHistory: Subscription;
    obsOrderHistoryError: Observable<any>;
    subObsOrderHistoryError: Subscription;

    ObsOrderDetsils: Observable<OrderDetails[]>;
    SubObsOrderDetails: Subscription;
    obsOrderDetailsError: Observable<any>;
    subObsOrderDetailsError: Subscription;

    showDetails = false;


    constructor(private _store: Store<fromRoot.State>, private _route: Router, private config: Config) {
        this.ObsOrderHistory = this._store.select(fromRoot.selectFromOrderHistorySuccess);
        this.ObsOrderDetsils = this._store.select(fromRoot.selectFromOrderDetailsSuccess);
        this.obsOrderDetailsError = this._store.select(fromRoot.selectFromOrderDetailsFailure);
        this.obsOrderHistoryError = this._store.select(fromRoot.selectFromOrderHistoryFailure);
    }

    ngOnInit() {
        let isOnInit = true;
        this.showSpinner = true;
        this._store.dispatch(new GetOrderHistoryActions.GetOrderHistory())

        this.SubObsOrderHistory = this.ObsOrderHistory.subscribe(result => {
           
            if (!isOnInit && result) {
                this.showSpinner = false;
                this.uniformOrders = result;
                this.uniformOrders.forEach(element => {
                    switch (element.stage) {
                        case 0:
                            element.stageStatus = " RECEIVED AWAITING PROCESSING ";
                            break;
                        case 1:
                            element.stageStatus = " PACKED AWAITING COLLECTION-DELIVERY ";
                            break;
                        case 2:
                            element.stageStatus = " DELIVERED-CLOSED ";
                            break;
                        case 3:
                            element.stageStatus = " UNDER INVESTIGATION ";
                            break;
                        case 4:
                            element.stageStatus = " AWAITING STOCK ";
                            break;
                        case 5:
                            element.stageStatus = " PARTIALLY DELIVERED ";
                            break;
                        case 6:
                            element.stageStatus = " PACKED AWAITING PAYMENT ON COLLECTION ";
                            break;
                        case 7:
                            element.stageStatus = "  CANCELLED ";
                            break;
                        case 8:
                            element.stageStatus = " AWAITING PAYMENT CLEARANCE ";
                            break;
                    }

                });
            }
        })

        this.subObsOrderHistoryError = this.obsOrderHistoryError.subscribe(err => {
            //if (!isOnInit && err) {
              
                this.showSpinner = false;
            //}
        })

        this.subObsOrderDetailsError = this.obsOrderDetailsError.subscribe(err => {
           // if (!isOnInit && err) {
                this.showSpinner = false;
           // }
        })

        this.SubObsOrderDetails = this.ObsOrderDetsils.subscribe(result => {
            if (!isOnInit && result) {
                this.showSpinner = false;
                this.orderDetails = result;
                this.orderDetails.forEach(orderDetail => {
                    orderDetail.total = orderDetail.price * orderDetail.quantity;
                })
            }
        })

        isOnInit = false;
    }

    displayDetails(orderHistory) {
        this.showSpinner = true;
        this.orderId = orderHistory.orderId;
        this.comment = orderHistory.comment;
        this.showDetails = true;
        this._store.dispatch(new GetOrderDetailsActions.GetOrderDetails(orderHistory.orderId));
    }

    hideDetails() {
        this.showDetails = false;
    }

    ngOnDestroy() {
        if (this.SubObsOrderHistory) {
            this.SubObsOrderHistory.unsubscribe();
        }
        if (this.SubObsOrderDetails) {
            this.SubObsOrderDetails.unsubscribe();
        }
        if (this.subObsOrderDetailsError)
            this.subObsOrderDetailsError.unsubscribe();
        if (this.subObsOrderHistoryError)
            this.subObsOrderHistoryError.unsubscribe();
    }
}