import { UniformNewsModule } from './../uniform-news/uniform-news.module';
import { OrderHistoryService } from './order-history.service';
import { OrderHistoryEffects } from './order-history.effects';
import { UniformOrderHistoryComponent } from './order-history.component';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    UniformNewsModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    EffectsModule.forFeature([OrderHistoryEffects]),
  ],
  declarations: [UniformOrderHistoryComponent],
  exports: [UniformOrderHistoryComponent],
  providers: [OrderHistoryService]
})

export class OrderHistoryModule {

}
