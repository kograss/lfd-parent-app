export class OrderDetails {
    orderId : number;
    productId : number;
    productName : string;
    sizeOptions : string;
    colorOptions : string;
    quantity : number;
    price : number;
    subTotal : number;
    comment : string;
    total : number;
    isUniformPack: number;
    basketItemId: number;
}