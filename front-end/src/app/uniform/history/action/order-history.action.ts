import { UniformOrderHistory } from './../model/order-history';
import { Action } from '@ngrx/store';

export const GETORDERHISTORY = 'GET ORDER HISTORY';
export const GETORDERHISTORY_SUCCESS = 'GET ORDER HISTORY SUCCESS';
export const GETORDERHISTORY_FAILURE = 'GET ORDER HISTORY FAILURE';

export class GetOrderHistory {
    readonly type = GETORDERHISTORY;
    constructor() {
    }
}

export class GetOrderHistorySuccess implements Action {

    readonly type = GETORDERHISTORY_SUCCESS;
    constructor(public ordersHistory: UniformOrderHistory[]) {

    }

}

export class GetOrderHistoryFailure implements Action {

    readonly type = GETORDERHISTORY_FAILURE;
    constructor(public err: any) {

    }
}

export type All = GetOrderHistory
    | GetOrderHistorySuccess | GetOrderHistoryFailure 