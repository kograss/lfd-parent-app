import { OrderDetails } from './../model/order-details';
import { Action } from '@ngrx/store';

export const GETORDERDETAILS = 'GET ORDER DETAILS';
export const GETORDERDETAILS_SUCCESS = 'GET ORDER DETAILS SUCCESS';
export const GETORDERDETAILS_FAILURE = 'GET ORDER DETAILS FAILURE';

export class GetOrderDetails {
    readonly type = GETORDERDETAILS;
    constructor(public orderId : number) {
    }
}

export class GetOrderDetailsSuccess implements Action {

    readonly type = GETORDERDETAILS_SUCCESS;
    constructor(public ordersDetails: OrderDetails[]) {

    }

}

export class GetOrderDetailsFailure implements Action {

    readonly type = GETORDERDETAILS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = GetOrderDetails
    | GetOrderDetailsSuccess | GetOrderDetailsFailure 