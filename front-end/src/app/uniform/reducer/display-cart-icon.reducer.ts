import * as DisplayCartIconAction from './../action/display-cart-icon-action';


export interface State {
    result: boolean
}

const initialState: State = {
    result: true
};

export function reducer(state = initialState, action: DisplayCartIconAction.All): State {
    switch (action.type) {

        case DisplayCartIconAction.DISPLAY_CART_ICON: {
            return {
                ...state,
                result: true
            }
        }
        case DisplayCartIconAction.HIDE_CART_ICON: {
            return {
                ...state,
                result: false
            }
        }

        default: {
            return state;
        }

    }

}
