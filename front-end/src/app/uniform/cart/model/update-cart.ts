
export class DeleteCartModel {
    basketId : number;
    basketItemId : number;
    deleteProduct : boolean;
    status :string;
}