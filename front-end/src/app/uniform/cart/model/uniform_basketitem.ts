export class UniformBasketItem {
    id: number;
    productId: number;
    price: number;
    name: string;
    quantity: number;
    basketId: number;
    size: string;
    color: string;
    schoolId: number;
    parentId: number;
    isUniformPack: number;
}

export class uniformPackItemProduct {
    id: number;
    productId: number;
    price: number;
    name: string;
    itemId: number;
    size: string;
    color: string;
    quantity: number;
  }