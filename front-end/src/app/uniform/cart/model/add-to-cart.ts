export class AddToCartModel {
    id : number;
    productId : number;
    price : number;
    name : string;
    quantity : number = 1;
    basketId : number;
    size : string;
    color : string;
    schoolId : number;
    parentId : number;
    isUniformPack: number;
}

export class UniformCartItem {
    uniformItem: AddToCartModel;
    uniformPackItemDetails: UniformPackItemProduct[];
}


export class UniformPackItemProduct {
    id : number;
    productId : number;
    price : number;
    name : string;
    size : string;
    color : string;
    itemId: number;
    quantity: number;
}