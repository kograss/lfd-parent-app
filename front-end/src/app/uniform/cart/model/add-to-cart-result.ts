export class AddToCartResult {
    currentBasketCount : number;
    basketId : number;
}