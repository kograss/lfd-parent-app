
export class CollectionOption {
    id : number;
    type : string;
    fee : number;
}

export class UniformAddress {
    id: number;
    schoolId: number;
    parentId: number;
    address : string;
    suburb : string;
    state : string;
    postCode : string;
    isDeleted: number;
}