import { DeleteCartModel } from './model/update-cart';
import { UniformCartItem } from './model/add-to-cart';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CollectionOption, UniformAddress } from './model/collection-option';

@Injectable()
export class UniformCartService {
  private _addToCartUrl = 'Uniform/AddToBasket/';
  private _getBasketItem = 'Uniform/GetBasket/{basketId}';
  private _deleteBasketItem = 'Uniform/UpdateBasket/';
  private _getCollectionOptionsUrl = 'Uniform/GetCollectionOptions/';
  private _getUniformAdddress = 'Uniform/GetUniformAddress/';
  private _addUniformAdddress = 'Uniform/AddUniformAddress/';
  private _updateUniformAdddress = 'Uniform/UpdateUniformAddress/';

  constructor(private _http: HttpClient) { }

  addToCart(cartItem: UniformCartItem): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    let body = JSON.stringify(cartItem);
    if (cartItem) {
      return this._http
        .post(this._addToCartUrl, body, options);
    }
  }

  getBasketItem(basketId: number): Observable<any> {
    return this._http.get(this._getBasketItem.replace("{basketId}", basketId.toString()));
  }

  deleteCartItem(cartItem: DeleteCartModel): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    let body = JSON.stringify(cartItem);
    return this._http.put(this._deleteBasketItem, cartItem, options);
  }

  getCollectionOptions(): Observable<CollectionOption[]> {
    return this._http.get<CollectionOption[]>(this._getCollectionOptionsUrl);
  }

  getUniformAddress(): Observable<any> {
    return this._http.get<any>(this._getUniformAdddress);
  }

  addUniformAddress(uniformAddress: UniformAddress): Observable<any> {
    let bodyString = JSON.stringify(uniformAddress);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._http.post<any>(this._addUniformAdddress, bodyString, options);
  }

  updateUniformAddress(uniformAddress: UniformAddress): Observable<any> {
    let bodyString = JSON.stringify(uniformAddress);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._http.post<any>(this._updateUniformAdddress, bodyString, options);
  }
}
