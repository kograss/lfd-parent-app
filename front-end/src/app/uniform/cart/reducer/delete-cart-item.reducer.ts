import { DeleteCartModel } from './../model/update-cart';
import * as DeleteCartItemActions from './../action/delete-cart-item.acion';
import { UniformBasketItem } from '../model/uniform_basketitem';

export interface State {    
   
    result: UniformBasketItem[],
    err: any
}

const initialState: State = {    
   
    result: [],
    err: {}
};

export function reducer(state = initialState, action: DeleteCartItemActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case DeleteCartItemActions.UNIFORMDELETECARTITEM_SUCCESS: {
            return {
                ...state,
                result: action.deleteCartItem
            }
        }
        case DeleteCartItemActions.UNIFORMDELETECARTITEM_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
