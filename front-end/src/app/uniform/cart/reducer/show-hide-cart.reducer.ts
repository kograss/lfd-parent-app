import * as ShowHideSidebarActions from './../action/show-hide-cart.action';

export interface State {
  sidebarStatus: Boolean;
}

const initialState: State = {
  sidebarStatus: false
}

export function reducer(state = initialState, action: ShowHideSidebarActions.All): State {
  //console.log("Action Type" + action.type);
  switch (action.type) {

    case ShowHideSidebarActions.UNIFORMSHOWHIDESIDEBAR: {
      return {
        ...state,
        sidebarStatus: new Boolean(action.sidebarStatus)
      }
    }

    default: {
      return state;
    }
  }
}
