import { UniformBasketItem } from './../model/uniform_basketitem';
import * as GetBasketItemAction from './../action/get-cart-item.action';

export interface State {

    result: UniformBasketItem[],
    err: any
}

const initialState: State = {

    result: [],
    err: {}
};

export function reducer(state = initialState, action: GetBasketItemAction.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {

        case GetBasketItemAction.GETBASKETITEM_SUCCESS: {
            return {
                ...state,
                result: action.uniformBasketItem
            }
        }
        case GetBasketItemAction.GETBASKETITEM_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }

    }

}
