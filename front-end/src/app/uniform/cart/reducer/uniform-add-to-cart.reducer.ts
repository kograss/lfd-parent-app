import { AddToCartResult } from '../model/add-to-cart-result';
import * as AddToCartActions from '../action/add-to-cart.action';

export interface State {
    result: AddToCartResult ,
    err: any
}

const initialState: State = {
    result: new AddToCartResult,
    err: {}
};

export function reducer(state = initialState, action: AddToCartActions.All): State {
    switch (action.type) {

        case AddToCartActions.ADDUNIFORMTOCART_SUCCESS: {
            return {
                ...state,
                result: action.addToCartResult
            }
        }
        case AddToCartActions.ADDUNIFORMTOCART_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        case AddToCartActions.CLEAR_CART: {
            return {
                ...state,
                result: new AddToCartResult
            }
        }

        default: {
            return state;
        }

    }

}
