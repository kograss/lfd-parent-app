import { UniformCartComponent } from './cart.component';
import { UniformCartService } from './cart.service';
import { UniformCartEffects } from './cart.effects';
import { reducers } from './../../reducers';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        
        EffectsModule.forFeature([UniformCartEffects]),
    ],
    declarations: [UniformCartComponent],
    exports: [UniformCartComponent],
    providers: [UniformCartService]
})
export class UniformCartModule {

}