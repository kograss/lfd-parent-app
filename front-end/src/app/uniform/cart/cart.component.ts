import { AlertService } from './../../alert/alertService';
import { DeleteCartModel } from './model/update-cart';
import { UniformBasketItem } from './model/uniform_basketitem';
import { AddToCartResult } from './model/add-to-cart-result';
import { AddUniformToCart } from './action/add-to-cart.action';
import { AddToCartModel } from './model/add-to-cart';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import { Config } from './../config/config';
import { Observable } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, Output, EventEmitter, ElementRef, Input } from '@angular/core';
import * as fromRoot from './../../reducers';
import * as AddToCartActions from './action/add-to-cart.action';
import * as GetBasketItemAction from './action/get-cart-item.action';
import * as ShowHideSidebarActions from './../cart/action/show-hide-cart.action';
import * as DeleteCartItemActions from './action/delete-cart-item.acion';
import * as ListChildrensActions from './../../children/actions/list-children-actions';
import * as ListUserActions from './../../user-profile/actions/list-user-action';
import { element } from 'protractor';
import { Children } from '../../children/model/children';
import * as PaymentActions from './../checkout/action/uniform-checkout.action';
import { environment } from './../../../environments/environment';
import { CollectionOption, UniformAddress } from './model/collection-option';
import { UniformCartService } from './cart.service';
enum DeleteType {
  QUANTITY,
  PRODUCT,
  CART
}

@Component({
  host: {
    '(document:click)': 'onClick($event)',
  },
  selector: 'uniform-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

export class UniformCartComponent implements OnInit, OnDestroy {
  @Input('cartMode')
  cartMode: string = "CART"; //CART for cart, CHECKOUT for checkout

  @Output()
  commentsEmt: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  selectedStudentEmt: EventEmitter<Children> = new EventEmitter<Children>();
  
  @Output()
  selectedCollectionOptionEmt: EventEmitter<CollectionOption> = new EventEmitter<CollectionOption>();

  @Output()
  specifiedAddressEmt: EventEmitter<UniformAddress> = new EventEmitter<UniformAddress>();

  comments: string;
  selectedStudent: Children;
  selectedCollectionOption: CollectionOption;
  public imageBaseUrl = environment.imageBaseUrl;

  cartItem: AddToCartModel;
  productIndex: number;
  basketItems: UniformBasketItem[] = [];
  basketItem: UniformBasketItem;
  deleteItem: DeleteCartModel;

  deleteType: DeleteType;

  obsAddUniformToCart: Observable<AddToCartResult>;
  subObsAddUniformToCart: Subscription;

  obsUniformBasketItem: Observable<UniformBasketItem[]>;
  subObsUniformBasketItem: Subscription;
  obsListBasketError: Observable<any>;
  subObsListBasketError: Subscription;

  obsShowHideCart: Observable<Boolean>;
  subObsShowHideCart: Subscription;

  obsDeleteItem: Observable<UniformBasketItem[]>;
  subObsDeleteItem: Subscription;
  obsDeleteItemError: Observable<any>;
  subObsDeleteItemError: Subscription;

  obsStudents: Observable<Children[]>;
  subObsStudents: Subscription;
  obsStudentsError: Observable<any>;
  subObsStudentsError: Subscription;
  students: Children[] = [];
  collectionOptions: CollectionOption[] = [];

  displayCategories = false;
  isNewsEmpty = false;
  hideNews = true;
  displayQuantity = false;
  isCartOpen = false;
  cartTotal: number = 0;
  basketId: number;
  message: string;
  showSpinner: boolean;
  isLoading: boolean;
  quantity: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  isStudentOptional: any;
  isExternalRecipientRequired: any;
  recipient: string;
  homeDeliveryAddress: UniformAddress = new UniformAddress();
  isHomeDelivery: boolean = false ;

  constructor(private _store: Store<fromRoot.State>, private _router: Router,
    private route: ActivatedRoute, private _eref: ElementRef, private alertService: AlertService, private uniformCartService: UniformCartService) {
    this.obsAddUniformToCart = this._store.select(fromRoot.selectFromAddToCartStatusSuccess);
    this.obsUniformBasketItem = this._store.select(fromRoot.selectFromGetBasketItemSuccess);
    this.obsShowHideCart = this._store.select(fromRoot.selectFromSelectShowHideCartSuccess);
    this.obsDeleteItem = this._store.select(fromRoot.selectFromDeleteItemSuccess);
    this.obsDeleteItemError = this._store.select(fromRoot.selectFromDeleteItemFailure);
    this.obsStudents = this._store.select(fromRoot.selectListChildrensSuccess);
    this.obsStudentsError = this._store.select(fromRoot.selectListChildrensFailure);
    this.obsListBasketError = this._store.select(fromRoot.selectFromGetBasketItemFailure);
  }


  ngOnInit() {
    
    let isOnInit = true;
    this._store.dispatch(new PaymentActions.UniformClearOrderResult());
    this.basketId = +localStorage.getItem('basketId');
    this.getCollectionOptions()


    this.isExternalRecipientRequired = localStorage.getItem("isExternalRecipientRequired");
    this.isStudentOptional = localStorage.getItem("isStudentOptional");

    if (this.basketId) {
      this.showSpinner = true;
      this.isLoading = true;
      this._store.dispatch(new GetBasketItemAction.GetBasketItem(this.basketId));
    }
    
    this.subObsUniformBasketItem = this.obsUniformBasketItem.subscribe(result => {
      if (!isOnInit) {
        this.isLoading = false;
        this.showSpinner = false;
        if (result) {
          this.cartTotal = 0;
          this.basketItems = result;
          this.basketItems.forEach(basketItem => {
            this.cartTotal += basketItem.price * basketItem.quantity;
          })
        }
        else {
          this.cartTotal = 0;
          this.basketItems = [];
        }
      }
    })

    this.subObsListBasketError = this.obsListBasketError.subscribe(err => {
      if (!isOnInit && err) {
        this.isLoading = false;
        this.showSpinner = false;
      }
    })

    this.subObsDeleteItem = this.obsDeleteItemError.subscribe(err => {
      if (!isOnInit && err) {
        this.isLoading = false;
        this.showSpinner = false;
      }
    })

    if (this.cartMode == 'CHECKOUT') {
      this.subObsStudents = this.obsStudents.subscribe(students => {
        if (students) {
          this.students = students;
        }
      })

      this.subObsStudentsError = this.obsStudentsError.subscribe(err => {
        if (err) {

        }
      })

      this._store.dispatch(new ListChildrensActions.ListChildrens(true));
      this._store.dispatch(new ListUserActions.ListParent);
    }
    isOnInit = false;
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) // or some similar check
      if (this.isCartOpen) {
        this._store.dispatch(new ShowHideSidebarActions.UniformShowHideSidebar(false));
        this.isCartOpen = false;
      }
      else {
        this.isCartOpen = true;
      }
  }

  onKey(event: any) {
    this.selectedStudent = new Children;
    this.selectedStudent.firstName = event.target.value;
    this.selectedStudentEmt.emit(this.selectedStudent);
  }

  onAddressChange(event: any) {
    
    this.specifiedAddressEmt.emit(this.homeDeliveryAddress)
  }

  onStudentChanged() {
    this.recipient = "";
    this.selectedStudentEmt.emit(this.selectedStudent);
  }

  onCommentUpdate() {
    this.commentsEmt.emit(this.comments);
  }

  onCollectionOptionChanged() {
    this.selectedCollectionOptionEmt.emit(this.selectedCollectionOption);
    if(this.selectedCollectionOption.type.toLowerCase() == "Home Delivery".toLowerCase()){
      this.isHomeDelivery = true
      this.showSpinner = true;
      this.uniformCartService.getUniformAddress().subscribe(result => {
        if(result){
          this.homeDeliveryAddress = result;
          this.specifiedAddressEmt.emit(this.homeDeliveryAddress)
          localStorage.setItem('oldDeliveryAddress', JSON.stringify(result));
          console.log(result)
        }
        this.showSpinner = false;
      })
    }
    else{
      this.isHomeDelivery = false;
    }
  }

  onUniformAddressChanged(){
    this.specifiedAddressEmt.emit(this.homeDeliveryAddress);
    
  }

  checkout() {
    this._store.dispatch(new ShowHideSidebarActions.UniformShowHideSidebar(false));
    this._router.navigate(['/uniformorder/checkout']);
  }

  deleteCartItem(basketItem: UniformBasketItem, deleteProduct: boolean, status: string) {
    //this.deleteType = status;
    this.showSpinner = true;
    this.deleteType = DeleteType[status];
    this.basketItem = basketItem;

    let item = new DeleteCartModel();
    item.deleteProduct = deleteProduct;
    item.status = status;
    item.basketId = this.basketId;
    item.basketItemId = basketItem.id;
    this._store.dispatch(new DeleteCartItemActions.UniformDeleteCartItem(item));
  }

  hideCartBarComponent() {
    this._store.dispatch(new ShowHideSidebarActions.UniformShowHideSidebar(false));
  }

  getCollectionOptions(){
    this.showSpinner = true;
    this.uniformCartService.getCollectionOptions().subscribe(result => {
      this.collectionOptions = result;
      this.showSpinner = false;
    })
  }

  ngOnDestroy() {
    if (this.subObsStudents)
      this.subObsStudents.unsubscribe();
    if (this.subObsStudentsError)
      this.subObsStudentsError.unsubscribe();
    if (this.subObsAddUniformToCart) {
      this.subObsAddUniformToCart.unsubscribe();
    }
    if (this.subObsUniformBasketItem) {
      this.subObsUniformBasketItem.unsubscribe();
    }
    if (this.subObsShowHideCart) {
      this.subObsShowHideCart.unsubscribe();
    }
    if (this.subObsDeleteItem) {
      this.subObsDeleteItem.unsubscribe();
    }
    if (this.subObsDeleteItem) {
      this.subObsDeleteItem.unsubscribe();
    }
    if (this.subObsListBasketError)
      this.subObsListBasketError.unsubscribe();
  }
}
