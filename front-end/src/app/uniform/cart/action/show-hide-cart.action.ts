
export const UNIFORMSHOWHIDESIDEBAR = 'UNIFORM SHOWHIDE SIDEBAR';

export class UniformShowHideSidebar {
  readonly type = UNIFORMSHOWHIDESIDEBAR;
  constructor(public sidebarStatus: boolean) {
  }
}

export type All = UniformShowHideSidebar