import { AddToCartResult } from './../model/add-to-cart-result';
import { UniformCartItem } from './../model/add-to-cart';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';


export const ADDUNIFORMTOCART = 'ADD UNIFORM TO CART';
export const ADDUNIFORMTOCART_SUCCESS = 'ADD UNIFORM TO CART SUCCESS';
export const ADDUNIFORMTOCART_FAILURE = 'ADD UNIFORM TO CART FAILURE';
export const CLEAR_CART = 'CLEAR CART';



export class AddUniformToCart implements Action {
    readonly type = ADDUNIFORMTOCART;
    constructor(public payload: UniformCartItem) {
    }

}

export class AddUniformToCartSuccess implements Action {
    readonly type = ADDUNIFORMTOCART_SUCCESS;
    constructor(public addToCartResult: AddToCartResult) {
    }
}

export class AddUniformToCartFailure implements Action {
    readonly type = ADDUNIFORMTOCART_FAILURE;
    constructor(public err: any) {
    }

}

export class ClearCart implements Action {
    readonly type = CLEAR_CART;
    constructor() {
    }

}


export type All = AddUniformToCart
    | AddUniformToCartSuccess | AddUniformToCartFailure | ClearCart
