import { UniformBasketItem } from './../model/uniform_basketitem';
import { Action } from '@ngrx/store';

export const GETBASKETITEM = 'GET BASKETITEM';
export const GETBASKETITEM_SUCCESS = 'GET BASKETITEM SUCCESS';
export const GETBASKETITEM_FAILURE = 'GET BASKETITEM FAILURE';

export class GetBasketItem {
    readonly type = GETBASKETITEM;
    constructor(public basketId: number) {
    }
}

export class GetBasketItemSuccess implements Action {
    readonly type = GETBASKETITEM_SUCCESS ;
    constructor(public uniformBasketItem : UniformBasketItem[] ) { }
}

export class GetBasketItemFailure implements Action {
    readonly type = GETBASKETITEM_FAILURE;
    constructor(public err: any) { }
}

export type All =  GetBasketItem 
    | GetBasketItemSuccess | GetBasketItemFailure