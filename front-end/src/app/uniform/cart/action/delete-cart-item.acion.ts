import { DeleteCartModel } from './../model/update-cart';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UniformBasketItem } from '../model/uniform_basketitem';


export const UNIFORMDELETECARTITEM = 'UNIFORM DELETE CART ITEM';
export const UNIFORMDELETECARTITEM_SUCCESS = 'UNIFORM DELETE CART ITEM SUCCESS';
export const UNIFORMDELETECARTITEM_FAILURE = 'UNIFORM DELETE CART ITEM FAILURE';


export class UniformDeleteCartItem {
    readonly type = UNIFORMDELETECARTITEM;
    constructor(public payload: DeleteCartModel) {
    }
}


export class UniformDeleteCartItemSuccess implements Action {
    readonly type = UNIFORMDELETECARTITEM_SUCCESS;
    constructor(public deleteCartItem: UniformBasketItem[]) { }
}

export class UniformDeleteCartItemFailure implements Action {
    readonly type = UNIFORMDELETECARTITEM_FAILURE;
    constructor(public err: any) { }
}


export type All =  UniformDeleteCartItem
    | UniformDeleteCartItemSuccess | UniformDeleteCartItemFailure