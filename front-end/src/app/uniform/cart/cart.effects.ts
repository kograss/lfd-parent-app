import { UniformCartService } from './cart.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as AddToCartActions from './action/add-to-cart.action';
import * as GetBasketItemAction from './action/get-cart-item.action';
import * as DeleteCartItemActions from './action/delete-cart-item.acion';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class UniformCartEffects {

  constructor(
    private actions$: Actions,
    private uniformCartService: UniformCartService
  ) { }

  @Effect()
  addToCart$: Observable<Action> = this.actions$.pipe(ofType<AddToCartActions.AddUniformToCart>
    (AddToCartActions.ADDUNIFORMTOCART)
    , map(action => action.payload)
    , switchMap(
      payload => this.uniformCartService.addToCart(payload)
        .map(results => new AddToCartActions.AddUniformToCartSuccess(results))
        .catch(err =>
          Observable.of({ type: AddToCartActions.ADDUNIFORMTOCART_FAILURE, exception: err })
        ))
  );

  @Effect()
  getBasketItem$: Observable<Action> = this.actions$.pipe(ofType<GetBasketItemAction.GetBasketItem>
    (GetBasketItemAction.GETBASKETITEM)
    , map(action => action)
    , switchMap(
      payload => this.uniformCartService.getBasketItem(payload.basketId)
        .map(results => new GetBasketItemAction.GetBasketItemSuccess(results))
        .catch(err => Observable.of({ type: GetBasketItemAction.GETBASKETITEM_FAILURE, exception: err })
        ))
  );

  @Effect()
  deleteCartItem$: Observable<Action> = this.actions$.pipe(ofType<DeleteCartItemActions.UniformDeleteCartItem>
    (DeleteCartItemActions.UNIFORMDELETECARTITEM)
    , map(action => action.payload)
    , switchMap(
      payload => this.uniformCartService.deleteCartItem(payload)
        .map(results => new GetBasketItemAction.GetBasketItemSuccess(results)) //new DeleteCartItemActions.DeleteCartItemSuccess(results))
        .catch(err =>
          Observable.of({ type: DeleteCartItemActions.UNIFORMDELETECARTITEM_FAILURE, exception: err })
        ))
  );
}
