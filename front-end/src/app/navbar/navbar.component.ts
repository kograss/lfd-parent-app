import { Parent } from './../user-profile/model/parent';
import { CancelOrderResult } from './../order-history/model/cancelOrderResult';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { SchoolInfo } from '../common/school-info/model/school-info';
import { CanteenShopCart } from '../cart/model/canteenShopCart';
import { CartResult } from '../cart/model/cartResult';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListCartItemsActions from '../cart/actions/list-cart-items-action';
import * as ShowHideCartActions from '../cart/actions/show-hide-cart-modal-action';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
import * as ListParentActions from './../user-profile/actions/list-user-action';
import { Cookie } from 'ng2-cookies';
import * as ShowHideProfileAction from './action/show-hide-profile.action';
import * as ListUserActions from './../user-profile/actions/list-user-action';
import * as GetCurrentTimeActions from './action/get-current-time-action';
import { UniformNews } from '../uniform/uniform-news/model/news';
import * as GetNewsAction from '../uniform/uniform-news/action/uniform-news.action';
//import { window } from 'rxjs/operators/window';

@Component({
  selector: 'navbar',
  styleUrls: ['./navbar.component.css'],
  templateUrl: './navbar.component.html'
})
export class NavBarComponent implements OnInit, OnDestroy {
  @Output()
  showSidebar: EventEmitter<string> = new EventEmitter();

  displayCart: boolean = false;
  cartItemsCount: number = 0;
  deliveryDate;
  cartProducts = [];
  grandTotal: number;
  parent: Parent;
  currency: string;
  eventId: number;
  orderType: string;
  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentError: Observable<any>;
  subObsListParentError: Subscription;
  sidebarStatus: boolean = true;

  obsShowHideCart: Observable<boolean>;
  subObsShowHideCart: Subscription;

  obsListCartItems: Observable<CartResult[]>;
  subObsListCartItems: Subscription;
  obsListCartItemsError: Observable<any>;
  subObsListCartItemsError: Subscription;

  obsCancelOrder: Observable<CancelOrderResult>;
  subObsCancelOrder: Subscription;

  obsCartCount: Observable<number>;
  subObsCartCount: Subscription;

  obsSidebarStatus: Observable<Boolean>;
  subObsSidebarStatus: Subscription;

  obsShowHideProfile: Observable<Boolean>;
  subObsShowHideProfile: Subscription;


  //variables to denode page loading state
  isParentReady: boolean = false;
  isCartReady: boolean = false;
  showSpinner: boolean;

  showProfile: boolean;
  isUniform: boolean = false;

  currentDateTime: Date;
  obsGetCurrentTime: Observable<Date>;
  subObsGetCurrentTime: Subscription;

  obsOpenSidebar: Observable<Boolean>;
  subOpenSidebar: Subscription;

  uniformNews: UniformNews[] = [];

  obsGetNews: Observable<UniformNews[]>;
  subObsGetNews: Subscription;

  newsCount = 0;

  constructor(private _store: Store<fromRoot.State>, private _route: Router, private config: Config) {
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsShowHideCart = this._store.select(fromRoot.selectShowHideCart);
    this.obsListCartItems = this._store.select(fromRoot.selectListCartItemsSuccess);
    this.obsListCartItemsError = this._store.select(fromRoot.selectListCartItemsFailure);
    this.obsListParentError = this._store.select(fromRoot.selectListParentInfoFailure);
    this.currency = this.config.getCurrency();
    this.obsCancelOrder = this._store.select(fromRoot.selectCancelOrderSuccess);
    this.obsCartCount = this._store.select(fromRoot.selectCartCount);
    this.obsSidebarStatus = this._store.select(fromRoot.selectSidebarStatus);
    this.obsShowHideProfile = this._store.select(fromRoot.selectFromShowHideProfileSuccess);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsOpenSidebar = this._store.select(fromRoot.openSidebarStatus);
    this.obsGetCurrentTime = this._store.select(fromRoot.selectGetCurrentTimeSuccess);
    this.obsGetNews = this._store.select(fromRoot.selectFromUniformNewsStatusSuccess);
  }

  ngOnInit() {

    if (window.location.href.indexOf("uniformorder") > -1) {
      this.isUniform = true;
    }

    this.orderType = localStorage.getItem('orderType');
    this.eventId = +localStorage.getItem('eventId');
    this.deliveryDate = localStorage.getItem('deliveryDate');

    let isOnInit = true;
    this.showSpinner = true;
    this._store.dispatch(new SidebarActions.SetSidebarName('commonSidebar'));
    this._store.dispatch(new ListParentActions.ListParent());
    if (this.deliveryDate != null && this.deliveryDate.trim() != "" && this.cartItemsCount == 0) {
      let values = this.deliveryDate.split('-');
      if (values.length < 3) {
        this._route.navigate(['canteenorder/error/' + 'InvalidDate']);
        return;
      }
      this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate, this.eventId))
    }
    else {
      this._store.dispatch(new ListCartItemsActions.SetGrandTotal(0));
      //return;
    }

    this.subObsSidebarStatus = this.obsSidebarStatus.subscribe(result => {
      this.sidebarStatus = result.valueOf();
    })

    this.subObsShowHideCart = this.obsShowHideCart.subscribe(result => {
      this.displayCart = result;
    })

    this.subObsCartCount = this.obsCartCount.subscribe(count => {
      if (!isOnInit) {
        if (count) {
          this.cartItemsCount = count;
        }
      }
    })

    this.subObsListParent = this.obsListParent.subscribe(parent => {
      if (!isOnInit) {
        if (parent.parentId) {
          this.parent = parent;
          this.isParentReady = true;
          if (this.deliveryDate != null && this.deliveryDate.trim() != '') {
            this.showSpinner = !this.isParentReady || !this.isCartReady;
          }
          else {
            this.showSpinner = false;
          }
        }
      }
    })

    this.subObsListParentError = this.obsListParentError.subscribe(error => {
      if (!isOnInit)
        this.showSpinner = false;
    })

    this.subObsListCartItems = this.obsListCartItems.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.length > 0) {
            this.cartItemsCount = 0;
            this.grandTotal = 0;
            for (var item of result) {
              for (var cart of item.canteenShopcarts) {
                this.cartItemsCount += cart.productQuantity;
                this.grandTotal += cart.productPrice;
              }
            }
            this._store.dispatch(new ListCartItemsActions.SetGrandTotal(this.grandTotal));
          }
          else {
            this.cartItemsCount = 0;
            this.grandTotal = 0;
            this._store.dispatch(new ListCartItemsActions.SetGrandTotal(this.grandTotal));
          }
        }
        this.isCartReady = true;
        this.showSpinner = !this.isParentReady || !this.isCartReady;
      }
    })

    this.subObsListCartItemsError = this.obsListCartItemsError.subscribe(error => {
      if (!isOnInit)
        this.showSpinner = false;
    })

    this.subObsCancelOrder = this.obsCancelOrder.subscribe(result => {
      if (!isOnInit) {
        if (result.isOrderCancelled) {
          this.parent.credit = result.parentCredit;
        }
      }
    })

    this.subObsShowHideProfile = this.obsShowHideProfile.subscribe(result => {
      this.showProfile = result.valueOf();
    })

    this.subObsListParent = this.obsListParent.subscribe(result => {
      this.parent = result;
    })

    this._store.dispatch(new GetCurrentTimeActions.CurrentTime());

    this.subObsGetCurrentTime = this.obsGetCurrentTime.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.currentDateTime = result;

          this.updateTime();
        }
      }
    })

    this.subOpenSidebar = this.obsOpenSidebar.subscribe(res => {
      if (res && !isOnInit) {
        this.displaySidebar();
      }
    })

    this._store.dispatch(new GetNewsAction.GetNews);
    this.subObsGetNews = this.obsGetNews.subscribe(result => {
      if (!isOnInit) {
        if (result.length > 0) {
          this.uniformNews = result;
          let month = new Date().getMonth() - 2;
          console.log(month);
          this.newsCount = this.uniformNews.filter(e => new Date(e.dateEntered).getMonth() >= month).length;
        }
      }
    });

    isOnInit = false;





  }

  displayTime(obj) {
    if (obj.currentDateTime) {
      let dateInMilli = Date.parse(obj.currentDateTime); //new Date(obj.currentDateTime + "");
      let newDate = new Date(dateInMilli);
      newDate.setSeconds(newDate.getSeconds() + 1);
      obj.currentDateTime = newDate;
    }

  }

  updateTime() {

    let obj = this;
    setInterval(function () { obj.displayTime(obj); }, 1000);
    //setInterval(this.displayTime, 1000);

    console.log(this.currentDateTime);
  }

  displaySidebar() {
    this.showSidebar.emit('sidebar');
  }

  displayCartBar() {
    this.showSidebar.emit('cart');
  }

  logout() {
    Cookie.deleteAll('/');
    localStorage.clear();
    this._route.navigate(['/']);
  }

  myAccount() {
    this._store.dispatch(new ShowHideProfileAction.ShowHideProfile(!this.showProfile));
    this._store.dispatch(new ListUserActions.ListParent);
  }

  ngOnDestroy() {
    if (this.subObsListCartItems)
      this.subObsListCartItems.unsubscribe();
    if (this.subObsShowHideCart)
      this.subObsShowHideCart.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsCancelOrder)
      this.subObsCancelOrder.unsubscribe();
    if (this.subObsListCartItemsError)
      this.subObsListCartItemsError.unsubscribe();
    if (this.subObsListParentError)
      this.subObsListParentError.unsubscribe();
    if (this.subObsCartCount)
      this.subObsCartCount.unsubscribe();
    if (this.subObsShowHideProfile)
      this.subObsShowHideProfile.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsGetCurrentTime)
      this.subObsGetCurrentTime.unsubscribe();

    if (this.subOpenSidebar)
      this.subOpenSidebar.unsubscribe();
      if (this.subObsGetNews) {
        this.subObsGetNews.unsubscribe();
      }
  }

}
