import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GetCurrentTimeService {

    private _url = "School/GetCurrentTime/";
    
    constructor(private _http: HttpClient) {
    }

    getCurrentTime(): Observable<any> {
        return this._http
            .get(this._url)
            ;
    }
}