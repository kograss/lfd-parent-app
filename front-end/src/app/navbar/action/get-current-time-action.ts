import { Action } from '@ngrx/store';

export const CURRENT_TIME = '[NEWS] LIST';
export const CURRENT_TIME_SUCCESS = '[NEWS] LIST SUCCESS';
export const CURRENT_TIME_FAILURE = '[NEWS] LIST FAILURE';

export class CurrentTime {
    readonly type = CURRENT_TIME;
    constructor() {

    }
}

export class CurrentTimeSuccess implements Action {

    readonly type = CURRENT_TIME_SUCCESS;
    constructor(public currentTime: Date) {
        if (this.currentTime == null) {
            this.currentTime = new Date();
        }
    }

}

export class CurrentTimeFailure implements Action {

    readonly type = CURRENT_TIME_FAILURE;
    constructor(public err: any) {

    }
}

export type All = CurrentTime
    | CurrentTimeSuccess | CurrentTimeFailure 