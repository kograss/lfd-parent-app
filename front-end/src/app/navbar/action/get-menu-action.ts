import { Action } from '@ngrx/store';

export const GETMENU = 'GET MENU';

export class GetMenu {
    readonly type = GETMENU;
    constructor(public menu: number) {
    }
}

export type All = GetMenu