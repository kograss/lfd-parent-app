import { Action } from '@ngrx/store';

export const SHOWHIDEPROFILE = 'SHOWHIDE PROFILE';

export class ShowHideProfile {
    readonly type = SHOWHIDEPROFILE;
    constructor(public profileStatus:boolean) {
    }
}

export type All = ShowHideProfile