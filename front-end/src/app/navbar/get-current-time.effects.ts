import { GetCurrentTimeService } from './get-current-time.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as GetCurrentTimeAction from './action/get-current-time-action';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class GetCurrentTimeEffects {

    constructor(
        private actions$: Actions,
        private getCurrentTimeService: GetCurrentTimeService
    ) { }

    @Effect()
    currentTime$: Observable<Action> = this.actions$.pipe(ofType<GetCurrentTimeAction.CurrentTime>
        (GetCurrentTimeAction.CURRENT_TIME)
        ,map(action=>action)
        ,switchMap(
        payload => this.getCurrentTimeService.getCurrentTime().pipe(
            map(results => new GetCurrentTimeAction.CurrentTimeSuccess(results))
            ,catchError(err =>
                of({ type: GetCurrentTimeAction.CURRENT_TIME_FAILURE, exception: err })
            )))
        );
}