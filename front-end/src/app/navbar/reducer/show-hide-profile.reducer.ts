import * as ShowHideProfileAction from './../action/show-hide-profile.action';

export interface State {
    profileStatus:Boolean;
}

const initialState: State = {
    profileStatus : false
}

export function reducer(state = initialState, action: ShowHideProfileAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case ShowHideProfileAction.SHOWHIDEPROFILE: {
            return {
                ...state,
                profileStatus:new Boolean(action.profileStatus)
            }
        }

        default: {
            return state;
        }

    }

}
