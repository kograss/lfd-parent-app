import * as GetMenu from './../action/get-menu-action';

export interface State {
    menu: number;
}

const initialState: State = {
    menu: 0
}

export function reducer(state = initialState, action: GetMenu.All): State {
    switch (action.type) {

        case GetMenu.GETMENU: {
            return {
                ...state,
                menu: action.menu
            }
        }

        default: {
            return state;
        }

    }

}
