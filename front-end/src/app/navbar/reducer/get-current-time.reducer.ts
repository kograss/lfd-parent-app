import * as CurrentTimeAction from '../action/get-current-time-action';
export interface State{
    result : Date,
    err:any
}

const initialState: State = {
    result : null,
    err : {}
}

export function reducer(state = initialState, action: CurrentTimeAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case CurrentTimeAction.CURRENT_TIME_SUCCESS: {
            return {
                ...state,
                result: action.currentTime
            }
        }
        case CurrentTimeAction.CURRENT_TIME_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
