import { RegistrationService } from './registration.services';
import { catchError, map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch'
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as RegistrationActions from './actions/registration.actions';


@Injectable()
export class RegistrationEffects {

    constructor(
        private actions$: Actions,
        private registrationService: RegistrationService
    ) { }

    @Effect()
    registerAction$: Observable<Action> = this.actions$.pipe(ofType<RegistrationActions.Register>(RegistrationActions.REGISTRATION)
        ,map(action => action)
        ,switchMap(
        payload => this.registrationService.register(payload.userRegistration).pipe(
            map(results => new RegistrationActions.RegisterSuccess(results))
            ,catchError(err =>
                of({ type: RegistrationActions.REGISTRATION_FAILURE, err: err })
            ))
    ));

}