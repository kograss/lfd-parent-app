import * as RegistrationActions from './../actions/registration.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
   
    result: {},
    err: {}
};

export function reducer(state = initialState, action: RegistrationActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case RegistrationActions.REGISTRATION_SUCCESS: {
            return {
                ...state,
                result: action.result
            }
        }
        case RegistrationActions.REGISTRATION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }               
        default: {
            return state;
        }

    }

}
