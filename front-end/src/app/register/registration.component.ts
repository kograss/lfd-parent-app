import { Registration } from './model/registration';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as RegistrationActions from './actions/registration.actions';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { AlertService } from './../alert/alertService';

import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registration = new Registration;
  agree = 0;
  showSpinner: boolean;

  obsRegistration: Observable<any>;
  subsObsRegistration: Subscription;

  obsRegistrationError: Observable<any>;
  subsObsRegistrationError: Subscription;
  registrationForm: FormGroup;

  constructor(private _store: Store<fromRoot.State>, private _router: Router, private alertService: AlertService, private fb: FormBuilder) {
    this.obsRegistration = this._store.select(fromRoot.selectRegistrationSuccess);
    this.obsRegistrationError = this._store.select(fromRoot.selectRegistrationFaliure);
  }

  buildForm() {
    this.registrationForm = this.fb.group({
      schoolID: [this.registration.schoolID, Validators.required],
      registerAs: [this.registration.registerAs, Validators.required],
      firstName: [this.registration.firstName, Validators.required],
      lastName: [this.registration.lastName, Validators.required],
      email: [this.registration.email, Validators.required],
      mobileNumber: [this.registration.mobileNumber, Validators.required],
      password: [this.registration.password, Validators.required],
      agree: [this.registration.agree]
    });
  }

  ngOnInit() {
    //this.registration.registerAs = "1";
    this.agree = 0;
    let isOnInit = true;
    this.subsObsRegistration = this.obsRegistration.subscribe(result => {
      if (result && !isOnInit) {

        this.registration = new Registration;
        this.showSpinner = false;
        this.alertService.success(result, true);
        this._router.navigate(['/']);

      }
    });

    this.subsObsRegistrationError = this.obsRegistrationError.subscribe(result => {
      if (result && !isOnInit) {
        this.alertService.error(result.error);
        this.showSpinner = false;
      }
    })
    isOnInit = false;
    this.buildForm();
  }

  register() {
    if (this.registrationForm.invalid) {
      <any>Object.values(this.registrationForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }
    this.registration = this.registrationForm.value as Registration;
    this.showSpinner = true;
    this._store.dispatch(new RegistrationActions.Register(this.registration));
  }

  checkbox(e) {
    if (e.target.checked) {
      this.agree = 1;
    }
    else {
      this.agree = 0;
    }
  }

  ngOnDestroy() {
    if (this.subsObsRegistration) {
      this.subsObsRegistration.unsubscribe();
    }
    if (this.subsObsRegistrationError) {
      this.subsObsRegistrationError.unsubscribe();
    }
  }

}
