export class Registration{
    public schoolID : string;
    public registerAs : string;
    public firstName : string;
    public lastName : string;
    public email : string;
    public mobileNumber : string;
    public password : string;
    public agree: number;
}