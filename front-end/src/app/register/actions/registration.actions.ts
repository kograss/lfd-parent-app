import { Registration } from './../model/registration';
import { Action } from '@ngrx/store';


export const REGISTRATION = 'REGISTRATION';
export const REGISTRATION_SUCCESS = 'REGISTRATION SUCCESS';
export const REGISTRATION_FAILURE = 'REGISTRATION FAILURE';
export const CLEARRESULT = 'CLEAR RESULT';

export class Register {
    readonly type = REGISTRATION;
    constructor(public userRegistration:Registration) { }
}

export class RegisterSuccess implements Action {
    readonly type = REGISTRATION_SUCCESS;
    constructor(public result: any) { }
}

export class RegisterFailure implements Action {
    readonly type = REGISTRATION_FAILURE;
    constructor(public err: any) {
        
    }
}

export type All = Register
    | RegisterSuccess | RegisterFailure 