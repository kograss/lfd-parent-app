import { Registration } from './model/registration';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RegistrationService {
     private _registrationUrl = "Parent/RegisterParent";
   
    constructor(private _http: HttpClient) {
    }

    register(user:Registration): Observable<any> {
        var options:any = {};
        options.headers = new HttpHeaders();
        options.headers.append('Content-Type', 'application/json');
        let body = JSON.stringify(user);
        return this._http.post(this._registrationUrl, body, options);
    } 
}



