import { RegistrationEffects } from './registration.effects';
import { RegisterComponent } from './registration.component';
import { RegistrationService } from './registration.services';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertService } from './../alert/alertService';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        EffectsModule.forFeature([RegistrationEffects]),
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        })
    ],
    declarations: [RegisterComponent],
    exports: [RegisterComponent],
    providers: [RegistrationService, AlertService]
})
export class RegisterModule {

}
