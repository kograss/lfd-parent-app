import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class Config {
    private _config: Object
    private _currency = 'USD';
    constructor(private http: HttpClient) {
       /* this.getJSON().subscribe(data => 
            this._config = data, 
            error =>
             //console.log(error));*/

    }

  /*  public getJSON(): Observable<any> {
        return this.http.get("assets/config/config.json")
            .map((res: any) => res.json())
            .catch(error => 
                Observable.of(error)
            );

    }

    get(key: any) {
        return this._config[key];
    }*/

    getCurrency(){
        return this._currency;
    }
};