import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { DataTableModule } from 'angular2-datatable';
import { BrowserModule } from '@angular/platform-browser';
import { Config } from './../config/config';
import { BannedFoodService } from './banned-food.service';
import { BannedFoodComponent } from './banned-food.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        DataTableModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        EffectsModule.forFeature([])        
    ],
    declarations: [BannedFoodComponent],
    exports: [BannedFoodComponent],
    providers: [ BannedFoodService, AlertService]
})
export class BannedFoodModule {

}
