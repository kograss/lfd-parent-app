import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannedFoodComponent } from './banned-food.component';

describe('BannedFoodComponent', () => {
  let component: BannedFoodComponent;
  let fixture: ComponentFixture<BannedFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannedFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannedFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
