import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class BannedFoodService {
    private _getAllProducts = 'BannedFood/GetAllProducts';
    private _getAllItems = 'BannedFood/GetAllItems';
    private _addBannedProduct = 'BannedFood/AddBannedProduct';
    private _removeBannedProduct = 'BannedFood/RemoveBannedProduct';

    constructor(private _http: HttpClient) { }

    getAllProducts():Observable<any> {
        return this._http.get(this._getAllProducts)
    }

    getAllBannedItems():Observable<any>  {
        return this._http.get(this._getAllItems)
    }

    addBannedProduct(data):Observable<any>  {
        return this._http.post(this._addBannedProduct, data);
    }

    removeBannedProduct(data):Observable<any>  {
        return this._http.post(this._removeBannedProduct, data);
    }


}