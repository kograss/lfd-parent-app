import { Component, OnInit } from '@angular/core';
import { BannedFoodService } from './banned-food.service';
import { Student } from '../products/model/student';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as ListStudentActions from '../children/actions/list-children-actions';
import { AlertService } from '../alert/alertService';

@Component({
  selector: 'app-banned-food',
  templateUrl: './banned-food.component.html',
  styleUrls: ['./banned-food.component.css']
})
export class BannedFoodComponent implements OnInit {

  masterAllProducts: any = [];
  allProduct: any = [];
  allBannedProducts: any = [];
  students: Student[] = [];

  obsListStudents: Observable<Student[]>;
  subObsListStudents: Subscription;
  obsListStudentError: Observable<any>;
  subObsListStudentError: Subscription;

  showSpinner: boolean;
  isOnInit: boolean = false;

  constructor(
    protected _store: Store<fromRoot.State>,
    public bannedFoodService: BannedFoodService,
    private alertService: AlertService) {
    this.obsListStudents = this._store.select(fromRoot.selectListChildrenSuccess);
    this.obsListStudentError = this._store.select(fromRoot.selectListChildrenFailure);
  }

  ngOnInit() {
    this.isOnInit = true;
    this.showSpinner = true;

    if (this.students.length == 0) {
      this._store.dispatch(new ListStudentActions.ListChildrens(false));
    }

    this.subObsListStudents = this.obsListStudents.subscribe(result => {
      this.students = result;
      //this.showSpinner = false;
    })

    this.subObsListStudentError = this.obsListStudentError.subscribe(error => {
      if (!this.isOnInit) {
        // this.showSpinner = false;
      }
    })

    this.getAllData();


    // this.bannedFoodService.getAllBannedItems().subscribe(res => {
    //   this.allBannedProducts = res;

    // },

    //   err => {
    //     console.log("ERROR");
    //     this.showSpinner = false;
    //   });

  }

  getAllData() {
    this.bannedFoodService.getAllProducts().subscribe(res => {
      this.masterAllProducts = res.allProduct;
      this.allProduct = res.allProduct;
      this.allBannedProducts = res.allBannedProducts;

      this.allProduct.forEach(element => {
        element.studentIds = [];
        if (this.allBannedProducts) {
          let bannedProducts = this.allBannedProducts.filter(e => e.productId == element.id);
          if (bannedProducts) {
            bannedProducts.forEach(e => {
              element.studentIds.push(e.studentId);
            });

          }
        }

      });
      console.log(this.allProduct);
      this.showSpinner = false;
    },

      err => {
        console.log("ERROR");
        this.showSpinner = false;
      });
  }

  banSingleStudent(product, studentId, productIndex) {
    if (product.studentIds && studentId) {

      let addBannedProductInput = {
        "studentIds": [studentId],
        "productId": product.id
      };

      this.showSpinner = true;
      this.bannedFoodService.addBannedProduct(addBannedProductInput).subscribe(result => {
        if (result) {
          this.showSpinner = false;
          this.allProduct[productIndex].studentIds.push(studentId);
          this.getAllData();
          this.alertService.success("Product banned to selected students");
        }
        else {
          this.showSpinner = false;
          this.alertService.error("Something went wrong. Please try again.");
        }
      },
        err => {
          this.showSpinner = false;
          this.alertService.error("Something went wrong. Please try again.");
          console.log(err);
        }
      )
    }
    else {
      this.alertService.warn("'You must select atleast one student'");
    }
  }


  ban(product, i) {
    if (product.studentIds && product.studentIds.length > 0) {
      let addBannedProductInput = {
        "studentIds": product.studentIds,
        "productId": product.id
      };
      this.showSpinner = true;
      this.bannedFoodService.addBannedProduct(addBannedProductInput).subscribe(result => {
        if (result) {
          this.showSpinner = false;
          this.alertService.success("Product banned to selected students");
        }
        else {
          this.showSpinner = false;
          this.alertService.error("Something went wrong. Please try again.");
        }
      },
        err => {
          this.showSpinner = true;
          this.alertService.error("Something went wrong. Please try again.");
          console.log(err);
        }
      )
    }
    else {
      this.alertService.warn("'You must select atleast one student'");
    }
  }

  selectStudents(id: number, product, isChecked: boolean, productIndex: number) {
    if (isChecked) {
      this.allProduct[productIndex].studentIds.push(id);
    }
    else {
      let index = this.allProduct[productIndex].studentIds.indexOf(id);
      this.allProduct[productIndex].studentIds.splice(index, 1);
    }
  }

  isThisProductHavingStudentsToBan(product) {
    let needBanBtn = false;
    if (product && this.students && this.students.length > 0) {
      this.students.forEach(s => {
        if (product.studentIds.indexOf(s.studentId) == -1) {
          needBanBtn = true;

        }
      })

    }

    return needBanBtn;
  }

  removeBannedStudent(product, studentId, productIndex) {
    let stud = this.allBannedProducts.filter(e => e.studentId == studentId && e.productId == product.id);
    if (stud && stud.length > 0) {

      let removeBannedProductInput = {
        "Id": stud[0].Id
      };
      //    this.bannedFoodService.addBannedProduct(addBannedProductInput).subscribe(result => {
      this.showSpinner = true;
      this.bannedFoodService.removeBannedProduct(removeBannedProductInput).subscribe(result => {
        this.showSpinner = false;
        this.alertService.success("Removed Successfully.");
        let index = this.allProduct[productIndex].studentIds.indexOf(stud[0].Id);
        this.allProduct[productIndex].studentIds.splice(index, 1);

      },
        err => {
          this.showSpinner = false;
          this.alertService.error("Something went wrong. Please try again.");
          console.log(err);
        }
      )
    }
  }

  onSearchChange(value) {
    if (value) {
      this.allProduct = this.masterAllProducts.filter(e => e.productName.toLowerCase().includes(value.toLowerCase()));
    }
    else {
      this.allProduct = this.masterAllProducts;
    }
  }

  ngOnDestroy() {
    if (this.subObsListStudents)
      this.subObsListStudents.unsubscribe();
  }
}
