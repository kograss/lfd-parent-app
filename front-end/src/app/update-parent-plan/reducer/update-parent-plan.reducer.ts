import { UpdateParentPlanResult } from './../model/update-parent-plan';
import * as UpdateParentPlanActions from './../actions/update-parent-plan.actions';

export interface State{
    result : UpdateParentPlanResult,
    err:any
}

const initialState: State = {
    result : new UpdateParentPlanResult,
    err : {}
}

export function reducer(state = initialState, action: UpdateParentPlanActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case UpdateParentPlanActions.UPDATEPARENTPLAN_SUCCESS: {
            return {
                ...state,
                result: action.updateResult
            }
        }
        case UpdateParentPlanActions.UPDATEPARENTPLAN_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
