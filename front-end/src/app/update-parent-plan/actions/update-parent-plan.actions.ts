import { UpdateParentPlanResult } from './../model/update-parent-plan';
import { Action } from '@ngrx/store';

export const UPDATEPARENTPLAN = '[PARENTPLAN] UPDATE';
export const UPDATEPARENTPLANUSINGSTRIPE = '[PARENTPLAN] UPDATE USING STRIPE';
export const UPDATEPARENTPLAN_SUCCESS = '[PARENTPLAN] UPDATE SUCCESS';
export const UPDATEPARENTPLAN_FAILURE = '[PARENTPLAN] UPDATE FAILURE';

export class UpdateParentPlan {
    readonly type = UPDATEPARENTPLAN;
    constructor(public plan:number) {
    }
}

export class UpdateParentPlanUsingStripe {
    readonly type = UPDATEPARENTPLANUSINGSTRIPE;
    constructor(public amount:number, public token:any) {
    }
}

export class UpdatePlanSuccess implements Action {
    readonly type = UPDATEPARENTPLAN_SUCCESS;
    constructor(public updateResult: UpdateParentPlanResult) {

    }

}

export class UpdatePlanFailure implements Action {

    readonly type = UPDATEPARENTPLAN_FAILURE;
    constructor(public err: any) {

    }
}

export type All = UpdateParentPlan
    | UpdatePlanSuccess | UpdatePlanFailure | UpdateParentPlanUsingStripe