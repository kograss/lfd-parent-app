export class UpdateParentPlanResult {
    parentPlan: number;
    message: string;
    isPlanUpdated:boolean;
    parentCredit:number;
}