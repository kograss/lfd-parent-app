import { AlertService } from './../alert/alertService';
import { StripeComponent } from './../stripe/stripe.component';
import { Router } from '@angular/router';
import { SchoolInfo } from './../common/school-info/model/school-info';
import { Parent } from './../user-profile/model/parent';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { UpdateParentPlanResult } from './model/update-parent-plan';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as UpdateParentPlanActions from './actions/update-parent-plan.actions';
import * as fromRoot from '../reducers';

@Component({
  selector: 'update-parent-plan',
  templateUrl: './update-parent-plan.component.html',
  styleUrls: ['./update-parent-plan.component.css']
})
export class UpdateParentPlanComponent implements OnInit, OnDestroy {
  showSpinner: boolean;
  public schoolInfo: SchoolInfo;
  payButtonClicked: boolean;

  obsUpdateParentPlan: Observable<UpdateParentPlanResult>;
  subObsUpdateParentPlan: Subscription;
  obsUpdateParentPlanFailure: Observable<any>;
  subObsUpdateParentPlanFailure: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;

  obsStripePopupState: Observable<boolean>;
  subObsStripePopupState: Subscription;

  unlimitedParentPlanFee: number;
  showStripeModal: boolean = false;
  stripePaymentData: any;
  chargeAmount: number;

  constructor(private _store: Store<fromRoot.State>,
    public parent: Parent, private _router: Router, private alertService: AlertService) {
    this.obsUpdateParentPlan = this._store.select(fromRoot.selectUpdateParentPlanSuccess);
    this.obsUpdateParentPlanFailure = this._store.select(fromRoot.selectUpdateParentPlanFailure);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsStripePopupState = this._store.select(fromRoot.selectStripePopupClosed);
    this.unlimitedParentPlanFee = this.getUnlimitedParentPlanFee();
  }

  ngOnInit() {
    let isOnInit = true;
    if (this.schoolInfo && this.parent) {
      if (this.schoolInfo.fee_model == 2 || this.parent.parentPlan == 2)
        this._router.navigate(['canteenorder/dashboard/home']);
    }
    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
      }
    })

    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        if(this.schoolInfo.canteenOrderServiceFee == null){
          this.schoolInfo.canteenOrderServiceFee = 0.25;
        }
      }
    })

    this.subObsUpdateParentPlan = this.obsUpdateParentPlan.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.isPlanUpdated) {
            this.parent.parentPlan = result.parentPlan;
            this.parent.credit = result.parentCredit;
            this._router.navigate(['canteenorder/dashboard/home']);
          }
          if (result.message) {
            this.alertService.success(result.message);
            //alert(result.message);
          }
        }
        this.showSpinner = false;
        this.payButtonClicked = false;
      }
    })

    this.subObsUpdateParentPlanFailure = this.obsUpdateParentPlanFailure.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
        this.payButtonClicked = false;
      }
    })

    this.subObsStripePopupState = this.obsStripePopupState.subscribe(res => {
      if (!isOnInit) {
        if (res) {
          this.payButtonClicked = false;
        }
      }
    })
    isOnInit = false;
  }

  payUsingCredit() {
    let chargeAmount = this.unlimitedParentPlanFee * 100;
    this.chargeAmount = this.unlimitedParentPlanFee;
    this.payButtonClicked = true;
    if (this.parent) {
      // this.stripe.openCheckout('Change Plan', chargeAmount, this.parent.email, (token: any) =>
      //   this.makePayment(chargeAmount, token));
      this.showStripeModal = true;
      this.stripePaymentData = {
        name: 'Change Plan',
        amount: this.chargeAmount * 100,
        email: this.parent.email
      };
    }
  }

  stripeCallback(token) {
    console.log('token', token);
    this.showStripeModal = false;
    this.makePayment(this.chargeAmount, token);
  }

  calculateCharge(amount: number) {
    amount += amount * 0.015 + 0.30;
    amount = Math.trunc((amount + 0.00001) * 100) / 100;
    return amount;
  }

  makePayment(amount: number, token: any) {
    this.showSpinner = true;
    this._store.dispatch(new UpdateParentPlanActions.UpdateParentPlanUsingStripe(amount, token));
  }

  updatePlan(plan: number) {
    if (plan) {
      this.showSpinner = true;
      this.payButtonClicked = true;
      this._store.dispatch(new UpdateParentPlanActions.UpdateParentPlan(plan));
    }
  }

  getUnlimitedParentPlanFee() {
    let dateObj = new Date();
    let month = dateObj.getMonth() + 1;

    if (month < 4) {
      return 13.20;
    }

    if (month < 7) {
      return 9.90;
    }

    if (month < 10) {
      return 6.60;
    }

    return 3.30;
  }

  ngOnDestroy() {
    if (this.subObsUpdateParentPlan)
      this.subObsUpdateParentPlan.unsubscribe();
    if (this.subObsUpdateParentPlanFailure)
      this.subObsUpdateParentPlanFailure.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsStripePopupState)
      this.subObsStripePopupState.unsubscribe();
  }
}
