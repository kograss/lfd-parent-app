import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateParentPlanComponent } from './update-parent-plan.component';

describe('UpdateParentPlanComponent', () => {
  let component: UpdateParentPlanComponent;
  let fixture: ComponentFixture<UpdateParentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateParentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateParentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
