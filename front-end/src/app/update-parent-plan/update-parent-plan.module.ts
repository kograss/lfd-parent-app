import { AlertService } from './../alert/alertService';
import { Parent } from './../user-profile/model/parent';
import { SchoolInfo } from './../common/school-info/model/school-info';
import { UpdateParentPlanComponent } from './update-parent-plan.component';
import { UpdateParentPlanService } from './update-parent-plan.service';
import { UpdateParentPlanEffects } from './update-parent-plan-effects';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { StripeModule } from '../stripe/stripe.module';

@NgModule({
  imports: [
    CommonModule,
    StripeModule,
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    FormsModule,
    EffectsModule.forFeature([UpdateParentPlanEffects])   
],
  declarations: [UpdateParentPlanComponent],
  exports: [UpdateParentPlanComponent],
  providers: [UpdateParentPlanService, SchoolInfo, Parent, AlertService]
})
export class UpdateParentPlanModule { }
