import { UpdateParentPlanService } from './update-parent-plan.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as UpdateParentPlanActions from './actions/update-parent-plan.actions';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class UpdateParentPlanEffects {

    constructor(
        private actions$: Actions,
        private updatePlanService: UpdateParentPlanService
    ) { }

    @Effect()
    updateParentPlan$: Observable<Action> = this.actions$.pipe(ofType<UpdateParentPlanActions.UpdateParentPlan>
        (UpdateParentPlanActions.UPDATEPARENTPLAN)
        , map(action => action)
        , switchMap(
            payload => this.updatePlanService.updatePlan(payload.plan).pipe(
                map(results => new UpdateParentPlanActions.UpdatePlanSuccess(results))
                , catchError(err =>
                    of({ type: UpdateParentPlanActions.UPDATEPARENTPLAN_FAILURE, exception: err })
                )))
    );

    @Effect()
    usingStripeUpdateParentPlan$: Observable<Action> = this.actions$.pipe(ofType<UpdateParentPlanActions.UpdateParentPlanUsingStripe>
        (UpdateParentPlanActions.UPDATEPARENTPLANUSINGSTRIPE)
        , map(action => action)
        , switchMap(
            payload => this.updatePlanService.updatePlanUsingStripe(payload.amount, payload.token).pipe(
                map(results => new UpdateParentPlanActions.UpdatePlanSuccess(results))
                , catchError(err =>
                    of({ type: UpdateParentPlanActions.UPDATEPARENTPLAN_FAILURE, exception: err })
                )))
    );
}