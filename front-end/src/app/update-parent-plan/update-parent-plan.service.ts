import { UpdateParentPlanResult } from './model/update-parent-plan';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UpdateParentPlanService {

    private _url = "Parent/UpdatePlan/";
    private _stripeUrl = "Parent/PayAndUpdatePlanToUnlimited";
    
    constructor(private _http: HttpClient) {
    }

    updatePlan(plan: number): Observable<any> {
        let body = { plan: plan };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        let bodyString = JSON.stringify(body);
        return this._http.put(this._url, bodyString, options);

    }

    updatePlanUsingStripe(amount: number, token: any): Observable<any> {
        let body = { 
            stripeToken: token.id,
            amount: amount            
        };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        let bodyString = JSON.stringify(body);
        return this._http.put(this._stripeUrl, body, options);
    }
}

