import * as fromSelectCategories from '../app/categories/categories-reducer';
import * as fromSelectProducts from '../app/products/reducers/list-products-reducer';
import * as fromProductsStock from '../app/products/reducers/products-stocks-reducer';

import * as fromSelectCartItems from './cart/reducers/list-cart-items-reducer';
import * as fromSelectDeleteCartItems from './cart/reducers/delete-cart-item-reducer';
import * as fromSelectAddToCart from './cart/reducers/add-to-cart-reducer';
import * as fromShowHideCart from './cart/reducers/show-hide-cart-modal-reducer';
import * as fromCheckout from './checkout/reducers/checkout-reducer';
import * as fromListNews from './news/reducers/list-news-reducer';
import * as fromCustomError from './error-page/custom-error-reducer';
import * as fromSelectSchoolInfo from './common/school-info/reducers/list-school-info.reducer';
import * as fromSelectSidebar from './sidebar/reducers/sidebar.reducer';
import * as fromSelectParentInfo from './user-profile/reducer/list-user-reducer';
import * as fromPaymentReducer from './payment/reducers/take-payment-reducer';
import * as fromListHolidays from './home/reducer/list-holidays.reducer';
import * as fromListOrderHistory from './order-history/reducers/list-order-history.reducer';
import * as fromListOrderDetails from './order-details/reducers/order-details.reducer';
import * as fromLoginAction from './login/login.reducer';
import * as fromCancelOrder from './order-history/reducers/cancel-order.reducer';
import * as fromListChildren from './children/reducers/list-children-reducer';
import * as fromAddChildren from './children/reducers/add-children-reducer';
import * as fromSetChildReducer from './children/reducers/set-children-reducer';
import * as fromEditParentProfile from './user-profile/reducer/edit-profile-reducer';
import * as fromListStudentClasses from './children/reducers/list-studentclasses-reducer';
import * as fromStripePopupAction from './stripe/reducer/stripe-popup.reducer';
import * as fromTopUpActions from './top-up/reducer/top-up.reducer';
import * as fromUpdateParentPlan from './update-parent-plan/reducer/update-parent-plan.reducer';
import * as fromCartSpinnerActions from './home/reducer/start-cart-spinner.reducer';
import * as fromForgotPassword from './forgot-password/forgot-password.reducer';
import * as fromResetPassword from './forgot-password/reset-password.reducer';
import * as fromFavouriteOrder from './order-history/reducers/favourite-order.reducer';
import * as fromRemoveFavourite from './order-history/reducers/remove-favourite.reducer';
import * as fromRepeatOrder from './order-history/reducers/repeat-order.reducer';
import * as fromSetRepeatOrderItems from './order-history/reducers/set-repeat-order-items.reducer';
import * as fromListRosterNews from './roster-home/reducer/list-roster-news.reducer';
import * as fromListRosterShifts from './roster-home/reducer/list-roster-shifts.reducer';
import * as fromListAllShifts from './roster-shifts-by-date/reducer/shift-by-date.reducer';
import * as fromRegisterVolunteer from './roster-shifts-by-date/reducer/register-volunteer.reducer';
import * as fromListEventReducer from './event/reducers/list-event-reducer';
import * as fromShowHideSidebar from './sidebar/reducers/show-hide-sidebar.reducer';
import * as fromOpenSidebar from './sidebar/reducers/open-sidebar.reducer';
import * as fromAlertReducer from './alert/reducers/alert-reducer';
import * as fromSelectShoeHideProfile from './navbar/reducer/show-hide-profile.reducer';
import * as fromListTransactionHistory from './transaction-history/reducer/list-transaction-reducer';
import * as fromManualTopup from './top-up/reducer/manual.top-top.reducer';
import * as fromGetCurrentTime from './navbar/reducer/get-current-time.reducer';
import * as fromGetActiveCoupons from './coupons/reducer/coupons.reducer';
import * as fromRedeemCoupon from './coupons/reducer/redeem.coupon.reducer';
import * as fromSelectGetMenu from './navbar/reducer/get-menu.reducer';
import * as fromRegistration from './register/reducers/registration.reducer';

//Uniform
import * as fromSelectUniformNews from './uniform/uniform-news/reducer/uniform-news.reducer';
import * as fromSelectUniformCategories from './uniform/categories/reducer/get-categories.reducer';
import * as fromSelectUniformProducts from './uniform/products/reducer/products.reducer';
import * as fromSelectUniformPackProducts from './uniform/products/reducer/get-uniform-pack-products.reducer';
import * as fromSelectAddToCartUniform from './uniform/cart/reducer/uniform-add-to-cart.reducer';
import * as fromSelectGetBasketItem from './uniform/cart/reducer/get-cart-item.reducer';
import * as fromSelectShowHideCart from './uniform/cart/reducer/show-hide-cart.reducer';
import * as fromSelectCheckout from './uniform/checkout/reducer/uniform-checkout.reducer';
import * as fromSelectDeleteItem from './uniform/cart/reducer/delete-cart-item.reducer';
// import * as fromStripePopupAction from './uniform/stripe/reducer/stripe-popup.reducer';
import * as fromListChildrens from './children/reducers/list-children-reducer';
import * as fromListUser from './user-profile/reducer/list-user-reducer';
import * as fromOrderHistory from './uniform/history/reducer/order-history.reducer';
import * as fromOrderDetails from './uniform/history/reducer/order-details.reducer';
import * as fromGetUniformProductStock from './uniform/products/reducer/get-uniform-product-stock.reducer';
// import * as fromSelectShoeHideProfile from './navbar/reducer/show-hide-profile.reducer';
import * as fromDisplayCartIcon from './uniform/reducer/display-cart-icon.reducer';
// import * as fromAlertReducer from './alert/reducer/alert.reducer';
import * as fromSelectPayUsingBalance from './uniform/checkout/reducer/payusingbalance.reducer';

export interface State {
  fromManualTopup: fromManualTopup.State;
  searchProductCategory: fromSelectCategories.State;
  searchProduct: fromSelectProducts.State;
  getProductsStock: fromProductsStock.State
  listCartItems: fromSelectCartItems.State;
  deleteCartItem: fromSelectDeleteCartItems.State;
  addToCart: fromSelectAddToCart.State;
  showHideCart: fromShowHideCart.State;
  placeOrder: fromCheckout.State;
  listNews: fromListNews.State;
  customError: fromCustomError.State;
  schoolInfo: fromSelectSchoolInfo.State;
  selectSidebar: fromSelectSidebar.State;
  selectParentInfo: fromSelectParentInfo.State;
  makePayment: fromPaymentReducer.State;
  holidayList: fromListHolidays.State;
  orderHistory: fromListOrderHistory.State;
  orderDetails: fromListOrderDetails.State;
  loginAction: fromLoginAction.State;
  cancelOrder: fromCancelOrder.State;
  listChildrens: fromListChildren.State;
  addChildren: fromAddChildren.State;
  setChildren: fromSetChildReducer.State;
  editParentProfile: fromEditParentProfile.State;
  listClasses: fromListStudentClasses.State;
  stripePopupClosed: fromStripePopupAction.State;
  topUpAction: fromTopUpActions.State;
  updateParentPlan: fromUpdateParentPlan.State;
  cartSpinnerState: fromCartSpinnerActions.State;
  forgotPasswordAction: fromForgotPassword.State;
  resetPasswordAction: fromResetPassword.State;
  favouriteOrder: fromFavouriteOrder.State;
  removeFavourite: fromRemoveFavourite.State;
  repeatOrder: fromRepeatOrder.State;
  repeatOrderItems: fromSetRepeatOrderItems.State;
  rosterNews: fromListRosterNews.State;
  rosterShifts: fromListRosterShifts.State;
  listShiftsForCalendar: fromListAllShifts.State;
  registerVolunteer: fromRegisterVolunteer.State;
  listEvent: fromListEventReducer.State;
  sidebarStatus: fromShowHideSidebar.State;
  openSidebar: fromOpenSidebar.State;
  alert: fromAlertReducer.State;
  selectFromShowHideProfile: fromSelectShoeHideProfile.State;
  listTransactionHistory: fromListTransactionHistory.State;
  listActiveCoupons: fromGetActiveCoupons.State;
  redeemCoupon: fromRedeemCoupon.State;
  getCurrentTime: fromGetCurrentTime.State;
  fromSelectGetMenu: fromSelectGetMenu.State;
  registration: fromRegistration.State;

  //unform
  selectFromUniformNewsStatus: fromSelectUniformNews.State;
  selectFromUniformCategoriesStatus: fromSelectUniformCategories.State;
  selectFromUniformProductStatus: fromSelectUniformProducts.State;
  selectFromUniformPackProductStatus: fromSelectUniformPackProducts.State;
  selectFromAddToCartStatus: fromSelectAddToCartUniform.State;
  selectFromGetBasketItem: fromSelectGetBasketItem.State;
  selectFromSelectShowHideCart: fromSelectShowHideCart.State;
  selectFromCheckot: fromSelectCheckout.State;
  selectFromDeleteItem: fromSelectDeleteItem.State;
  selectChildren: fromListChildrens.State;
  selectUser: fromListUser.State;
  selectFromOrderHistory: fromOrderHistory.State;
  selectFromOrderDetails: fromOrderDetails.State;
  selectFromGetUniformProductStock: fromGetUniformProductStock.State;
  selectFromDisplayCartIcon: fromDisplayCartIcon.State;
  selectFromPayUsingBalance: fromSelectPayUsingBalance.State;
}

export const reducers = {
  fromManualTopup: fromManualTopup.reducer,
  searchProductCategory: fromSelectCategories.reducer,
  searchProduct: fromSelectProducts.reducer,
  getProductsStock: fromProductsStock.reducer,
  listCartItems: fromSelectCartItems.reducer,
  deleteCartItem: fromSelectDeleteCartItems.reducer,
  addToCart: fromSelectAddToCart.reducer,
  showHideCart: fromShowHideCart.reducer,
  placeOrder: fromCheckout.reducer,
  listNews: fromListNews.reducer,
  customError: fromCustomError.reducer,
  schoolInfo: fromSelectSchoolInfo.reducer,
  selectSidebar: fromSelectSidebar.reducer,
  selectParentInfo: fromSelectParentInfo.reducer,
  makePayment: fromPaymentReducer.reducer,
  holidayList: fromListHolidays.reducer,
  orderHistory: fromListOrderHistory.reducer,
  orderDetails: fromListOrderDetails.reducer,
  loginAction: fromLoginAction.reducer,
  cancelOrder: fromCancelOrder.reducer,
  listChildrens: fromListChildren.reducer,
  addChildren: fromAddChildren.reducer,
  setChildren: fromSetChildReducer.reducer,
  editParentProfile: fromEditParentProfile.reducer,
  listClasses: fromListStudentClasses.reducer,
  stripePopupClosed: fromStripePopupAction.reducer,
  topUpAction: fromTopUpActions.reducer,
  updateParentPlan: fromUpdateParentPlan.reducer,
  cartSpinnerState: fromCartSpinnerActions.reducer,
  forgotPasswordAction: fromForgotPassword.reducer,
  resetPasswordAction: fromResetPassword.reducer,
  favouriteOrder: fromFavouriteOrder.reducer,
  removeFavourite: fromRemoveFavourite.reducer,
  repeatOrder: fromRepeatOrder.reducer,
  repeatOrderItems: fromSetRepeatOrderItems.reducer,
  rosterNews: fromListRosterNews.reducer,
  rosterShifts: fromListRosterShifts.reducer,
  listShiftsForCalendar: fromListAllShifts.reducer,
  registerVolunteer: fromRegisterVolunteer.reducer,
  listEvent: fromListEventReducer.reducer,
  sidebarStatus: fromShowHideSidebar.reducer,
  openSidebar: fromOpenSidebar.reducer,
  alert: fromAlertReducer.reducer,
  selectFromShowHideProfile: fromSelectShoeHideProfile.reducer,
  listTransactionHistory: fromListTransactionHistory.reducer,
  listActiveCoupons: fromGetActiveCoupons.reducer,
  redeemCoupon: fromRedeemCoupon.reducer,
  getCurrentTime: fromGetCurrentTime.reducer,
  fromSelectGetMenu: fromSelectGetMenu.reducer,
  registration: fromRegistration.reducer,

  //uniform
  selectFromUniformNewsStatus: fromSelectUniformNews.reducer,
  selectFromUniformCategoriesStatus: fromSelectUniformCategories.reducer,
  selectFromUniformProductStatus: fromSelectUniformProducts.reducer,
  selectFromAddToCartStatus: fromSelectAddToCartUniform.reducer,
  selectFromGetBasketItem: fromSelectGetBasketItem.reducer,
  selectFromSelectShowHideCart: fromSelectShowHideCart.reducer,
  selectFromCheckot: fromSelectCheckout.reducer,
  selectFromDeleteItem: fromSelectDeleteItem.reducer,
  selectChildren: fromListChildrens.reducer,
  selectUser: fromListUser.reducer,
  selectFromOrderHistory: fromOrderHistory.reducer,
  selectFromOrderDetails: fromOrderDetails.reducer,
  selectFromGetUniformProductStock: fromGetUniformProductStock.reducer,
  selectFromDisplayCartIcon: fromDisplayCartIcon.reducer,
  selectFromPayUsingBalance: fromSelectPayUsingBalance.reducer,
};

export function selectRegistrationSuccess(state: State) {
  return state.registration.result;
}

export function selectRegistrationFaliure(state: State) {
  return state.registration.err;
}

export function selectRedeemCouponSuccess(state: State) {
  return state.redeemCoupon.result;
}

export function selectRedeemCouponSuccessFailure(state: State) {
  return state.redeemCoupon.err;
}

export function selectGetActiveCouponsSuccess(state: State) {
  return state.listActiveCoupons.result;
}

export function selectGetActiveCouponsFailure(state: State) {
  return state.listActiveCoupons.err;
}

export function selectGetCurrentTimeSuccess(state: State) {
  return state.getCurrentTime.result;
}

export function selectGetCurrentTimeFailure(state: State) {
  return state.getCurrentTime.err;
}



export function selectlistTransactionHistorySuccess(state: State) {
  return state.listTransactionHistory.result;
}

export function selectlistTransactionHistoryFailure(state: State) {
  return state.listTransactionHistory.err;
}

export function selectSearchProductCategorySuccess(state: State) {
  // //console.log(" Select Store " + JSON.stringify(state.getStore.result))
  //return "test";
  return state.searchProductCategory.result;
}

export function selectSearchProductCategoryFailure(state: State) {
  // //console.log(" Select Store " + JSON.stringify(state.getStore.result))
  //return "test";
  return state.searchProductCategory.err;
}

export function selectSearchProductsSuccess(state: State) {
  return state.searchProduct.result;
}

export function selectSearchProductsFailure(state: State) {
  return state.searchProduct.err;
}

export function selectProductsStockSuccess(state: State) {
  return state.getProductsStock.result;
}

export function selectProductsStockFailure(state: State) {
  return state.getProductsStock.err;
}

export function selectListCartItemsSuccess(state: State) {
  return state.listCartItems.result;
}

export function selectListCartItemsFailure(state: State) {
  return state.listCartItems.err;
}

export function selectGrandTotal(state: State) {
  return state.listCartItems.grandTotal;
}

export function selectCartCount(state: State) {
  return state.listCartItems.count;
}

export function selectDeleteCartItemsSuccess(state: State) {
  return state.deleteCartItem.result;
}

export function selectDeleteCartItemsFailure(state: State) {
  return state.deleteCartItem.err;
}

export function selectAddToCartSuccess(state: State) {
  return state.addToCart.result;
}

export function selectAddToCartCartFailure(state: State) {
  return state.addToCart.err;
}

export function selectShowHideCart(state: State) {
  return state.showHideCart.showCart;
}

export function selectCheckoutbuttonStatus(state: State) {
  return state.showHideCart.checkoutButtonClicked;
}

export function selectPlaceOrderSuccess(state: State) {
  return state.placeOrder.result;
}

export function selectPlaceOrderFailure(state: State) {
  return state.placeOrder.err;
}

export function selectListNewsSuccess(state: State) {
  return state.listNews.result;
}

export function selectListNewsFailure(state: State) {
  return state.listNews.err;
}

export function selectCustomErrorMessage(state: State) {
  return state.customError.errorMessage;
}

export function selectListSchoolInfoSuccess(state: State) {
  return state.schoolInfo.result;
}

export function selectListSchoolInfoFailure(state: State) {
  return state.schoolInfo.err;
}

export function selectSideBarName(state: State) {
  return state.selectSidebar.sidebarName;
}
export function selectListParentInfoSuccess(state: State) {
  return state.selectParentInfo.result;
}

export function selectListParentInfoFailure(state: State) {
  return state.selectParentInfo.err;
}

export function selectPaymentSuccess(state: State) {
  return state.makePayment.result;
}

export function selectPaymentFailure(state: State) {
  return state.makePayment.err;
}

export function selectListHolidaysSuccess(state: State) {
  return state.holidayList.result;
}

export function selectListHolidaysFailure(state: State) {
  return state.holidayList.err;
}

export function selectListOrderHistorySuccess(state: State) {
  return state.orderHistory.result;
}

export function selectListOrderHistoryFailure(state: State) {
  return state.orderHistory.err;
}

export function selectListOrderDetailsSuccess(state: State) {
  return state.orderDetails.result;
}

export function selectListOrderDetailsFailure(state: State) {
  return state.orderDetails.err;
}

export function selectLoginSuccess(state: State) {
  return state.loginAction.result;
}

export function selectLoginFailure(state: State) {
  return state.loginAction.err;
}

export function selectForgotPasswordSuccess(state: State) {
  return state.forgotPasswordAction.result;
}

export function selectForgotPasswordFailure(state: State) {
  return state.forgotPasswordAction.err;
}

export function selectResetPasswordSuccess(state: State) {
  return state.resetPasswordAction.result;
}

export function selectResetPasswordFailure(state: State) {
  return state.resetPasswordAction.err;
}

export function selectCancelOrderSuccess(state: State) {
  return state.cancelOrder.result;
}

export function selectCancelOrderFailure(state: State) {
  return state.cancelOrder.err;
}

export function selectListChildrenSuccess(state: State) {
  return state.listChildrens.result;
}

export function selectListChildrenFailure(state: State) {
  return state.listChildrens.err;
}

export function selectAddChildrenSuccess(state: State) {
  return state.addChildren.result;
}

export function selectAddChildrenFailure(state: State) {
  return state.addChildren.err;
}


export function setChildren(state: State) {
  return state.setChildren.children;
}

export function selectEditParentSuccess(state: State) {
  return state.editParentProfile.result;
}

export function selectEditParentFailure(state: State) {
  return state.editParentProfile.err;
}

export function selectListClassesSuccess(state: State) {
  return state.listClasses.result;
}

export function selectListClassesFailure(state: State) {
  return state.listClasses.err;
}

export function selectStripePopupClosed(state: State) {
  return state.stripePopupClosed.stripePopUpClosed;
}

export function selectTopUpSuccess(state: State) {
  return state.topUpAction.result;
}

export function selectTopUpFailure(state: State) {
  return state.topUpAction.err;
}

export function selectUpdateParentPlanSuccess(state: State) {
  return state.updateParentPlan.result;
}

export function selectUpdateParentPlanFailure(state: State) {
  return state.updateParentPlan.err;
}


export function selectFavouriteOrderSuccess(state: State) {
  return state.favouriteOrder.result;
}

export function selectFavouriteOrderFailure(state: State) {
  return state.favouriteOrder.err;
}


export function selectRemoveFavouriteOrderSuccess(state: State) {
  return state.removeFavourite.result;
}

export function selectRemoveFavouriteOrderFailure(state: State) {
  return state.removeFavourite.err;
}

export function selectCanRepeatOrderSuccess(state: State) {
  return state.repeatOrder.result;
}

export function selectCanRepeatOrderFailure(state: State) {
  return state.repeatOrder.err;
}

export function selectSetRepeatOrderItems(state: State) {
  return state.repeatOrderItems.result;
}

export function selectShowSpinner(state: State) {
  return state.cartSpinnerState.showSpinner;
}

export function selectListRosterNewsSuccess(state: State) {
  return state.rosterNews.result;
}

export function selectListRosterNewsFailure(state: State) {
  return state.rosterNews.err;
}

export function selectListRosterShiftsSuccess(state: State) {
  return state.rosterShifts.result;
}

export function selectListRosterShiftsFailure(state: State) {
  return state.rosterShifts.err;
}


export function selectListAllRosterShiftsCalendarSuccess(state: State) {
  return state.listShiftsForCalendar.result;
}

export function selectListAllRosterShiftsCalendarFailure(state: State) {
  return state.listShiftsForCalendar.err;
}

export function selectRegisterVolunteerSuccess(state: State) {
  return state.registerVolunteer.result;
}

export function selectRegisterVolunteerFailure(state: State) {
  return state.registerVolunteer.err;
}


export function selectListEventSuccess(state: State) {
  return state.listEvent.result;
}

export function selectListEventFailure(state: State) {
  return state.listEvent.err;
}

export function selectSidebarStatus(state: State) {
  return state.sidebarStatus.sidebarStatus;
}

export function openSidebarStatus(state: State) {
  return state.openSidebar.sidebarStatus;
}

export function getAlertCancelConfirmStatus(state: State) {
  return state.alert.result;
}

export function selectFromShowHideProfileSuccess(state: State) {
  return state.selectFromShowHideProfile.profileStatus;
}

export function fromSelectGetMenuSuccess(state: State) {
  return state.fromSelectGetMenu.menu;
}

export function AddManualTopSuccess(state: State) {
  return state.fromManualTopup.result;
}

export function addManualTopupFailure(state: State) {
  return state.fromManualTopup.err;
}

//uniform
export function selectFromDisplayCartIcon(state: State) {
  return state.selectFromDisplayCartIcon.result;
}

export function selectFromUniformNewsStatusSuccess(state: State) {
  return state.selectFromUniformNewsStatus.result;
}

export function selectFromUniformNewsStatusFailure(state: State) {
  return state.selectFromUniformNewsStatus.err;
}

export function selectFromUniformCategoriesStatusSuccess(state: State) {
  return state.selectFromUniformCategoriesStatus.result;
}

export function selectFromUniformCategoriesStatusFailure(state: State) {
  return state.selectFromUniformCategoriesStatus.err;
}

export function selectFromUniformProductStatusSuccess(state: State) {
  return state.selectFromUniformProductStatus.result;
}

export function selectFromUniformProductStatusFailure(state: State) {
  return state.selectFromUniformProductStatus.err;
}

export function selectFromUniformPackProductStatusSuccess(state: State) {
  return state.selectFromUniformPackProductStatus.result;
}

export function selectFromUniformPackProductStatusFailure(state: State) {
  return state.selectFromUniformPackProductStatus.err;
}

export function selectFromAddToCartStatusSuccess(state: State) {
  return state.selectFromAddToCartStatus.result;
}

export function selectFromAddToCartStatusFailure(state: State) {
  return state.selectFromAddToCartStatus.err;
}

export function selectFromGetBasketItemSuccess(state: State) {
  return state.selectFromGetBasketItem.result;
}

export function selectFromGetBasketItemFailure(state: State) {
  return state.selectFromGetBasketItem.err;
}

export function selectFromSelectShowHideCartSuccess(state: State) {
  return state.selectFromSelectShowHideCart.sidebarStatus;
}

export function selectFromCheckotSuccess(state: State) {
  return state.selectFromCheckot.result;
}

export function selectFromCheckotFailure(state: State) {
  return state.selectFromCheckot.err;
}

export function selectFromDeleteItemSuccess(state: State) {
  return state.selectFromDeleteItem.result;
}

export function selectFromDeleteItemFailure(state: State) {
  return state.selectFromDeleteItem.err;
}

export function selectListChildrensSuccess(state: State) {
  return state.selectChildren.result;
}

export function selectListChildrensFailure(state: State) {
  return state.selectChildren.err;
}

export function selectListUserSuccess(state: State) {
  return state.selectUser.result;
}

export function selectListUserFailure(state: State) {
  return state.selectUser.err;
}

export function selectFromOrderHistorySuccess(state: State) {
  return state.selectFromOrderHistory.result;
}

export function selectFromOrderHistoryFailure(state: State) {
  return state.selectFromOrderHistory.err;
}

export function selectFromOrderDetailsSuccess(state: State) {
  return state.selectFromOrderDetails.result;
}

export function selectFromOrderDetailsFailure(state: State) {
  return state.selectFromOrderDetails.err;
}

export function selectFromGetProductStockSuccess(state: State) {
  return state.selectFromGetUniformProductStock.result;
}

export function selectFromGetProductStockFailure(state: State) {
  return state.selectFromGetUniformProductStock.err;
}

export function selectFromPayUsingBalanceSuccess(state: State) {
  return state.selectFromPayUsingBalance.result;
}

export function selectFromPayUsingBalanceFailure(state: State) {
  return state.selectFromPayUsingBalance.err;
}