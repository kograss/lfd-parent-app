export class SchoolEventInfo {
    eventId: number
    eventName: string
    eventDate: Date
    orderingCloseDate: Date
    bagPrice: number
    isBagAvailable: boolean
    bagName: string
    IsSetPrice: number;
    eventPrice: number;
    whoPays: number;
    event_type: number;
    eventType: string; //CANTEEN or SCHOOL
    isFundRaisingEvent: boolean; //for fund raising events
}