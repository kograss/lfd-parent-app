import { EventService } from './event.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListEventActions from './actions/list-event-items-action';
import * as ListEventProductsActions from './actions/list-event-products.action';
import * as ListProductsActions from './../products/actions/list-products-actions';
import { catchError, exhaustMap, map, switchMap } from 'rxjs/operators';


@Injectable()
export class EventEffects {

  constructor(
    private actions: Actions,
    private eventService: EventService
  ) { }

  @Effect()
  listEvents = this.actions.pipe(ofType<ListEventActions.ListEvent>(ListEventActions.LISTEVENT),
    exhaustMap(payload => {
      return this.eventService.getAllEvents(payload.eventType).pipe(
        map(results => new ListEventActions.ListEventSuccess(results)),
        catchError(err =>
          of(new ListEventActions.ListEventFailure(err)),
        )
      );
    }
    ));

  @Effect()
  listEventProducts = this.actions.pipe(ofType<ListEventProductsActions.ListEventProduct>
    (ListEventProductsActions.LISTEVENTPRODUCT),
    switchMap(
      payload => this.eventService.getAllProducts(payload.eventId).pipe(
        map(results => new ListProductsActions.ListProductsSuccess(results)),
        catchError(err =>
          of(new ListProductsActions.ListProductsFailure(err))
        )
      )
    )
  );


}