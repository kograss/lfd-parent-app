import { SchoolEventInfo } from './../model/event';
import * as ListEventActions from './../actions/list-event-items-action';
export interface State{
    result : SchoolEventInfo[],
    err:any
   
}

const initialState: State = {
    result : [],
    err : {}    
}

export function reducer(state = initialState, action: ListEventActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListEventActions.LISTEVENT_SUCCESS: {
            return {
                ...state,
                result: action.events
            }
        }
        case ListEventActions.LISTEVENT_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }      
        
       

        default: {
            return state;
        }

    }

}
