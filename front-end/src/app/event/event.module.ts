import { LoadingModule } from 'ngx-loading';
import { EventService } from './event.service';
import { EventEffects } from './event-effects';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event.component';
import { EventRoutingModule } from './event-routing.module';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';
import { FundRaisingEventComponent } from './fund-raising/fund-raising.component';
import { FormsModule } from '@angular/forms';
import { DonationConfirmationComponent } from './donation-confirmation/donation-confirmation.component';
import { SafeHtmlPipe } from '../common/SafeHtml.pipe';
import { StripeModule } from '../stripe/stripe.module';

@NgModule({
  imports: [CommonModule, EventRoutingModule,
    CommonMenuModule,
    FormsModule,
    StripeModule,
    EffectsModule.forFeature([EventEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),],
  declarations: [EventComponent, FundRaisingEventComponent, DonationConfirmationComponent, ],
  exports: [EventComponent, FundRaisingEventComponent],
  providers: [EventService]
})
export class EventModule { }
