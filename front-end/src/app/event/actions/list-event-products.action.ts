import { Action } from '@ngrx/store';

export const LISTEVENTPRODUCT = '[EVENTPRODUCTS] LIST';
export const LISTEVENTPRODUCT_SUCCESS = '[EVENTPRODUCTS] LIST SUCCESS';
export const LISTEVENTPRODUCT_FAILURE = '[EVENTPRODUCTS] LIST FAILURE';


export class ListEventProduct {
  
    readonly type = LISTEVENTPRODUCT;
    constructor(public eventId:number) {
      
    }
}

export type All = ListEventProduct
