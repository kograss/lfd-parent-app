import { SchoolEventInfo } from './../model/event';
import { Action } from '@ngrx/store';

export const LISTEVENT = '[EVENT] LIST';
export const LISTEVENT_SUCCESS = '[EVENT] LIST SUCCESS';
export const LISTEVENT_FAILURE = '[EVENT] LIST FAILURE';


export class ListEvent {
  
    readonly type = LISTEVENT;
    constructor(public eventType:string) {
      
    }
}

export class ListEventSuccess implements Action {

    readonly type = LISTEVENT_SUCCESS;
    constructor(public events: any) {
        if (this.events == null) {
            this.events = [];
        }
    }

}

export class ListEventFailure implements Action {

    readonly type = LISTEVENT_FAILURE;
    constructor(public err: any) {

    }
}

  

export type All = ListEvent
    | ListEventSuccess | ListEventFailure 