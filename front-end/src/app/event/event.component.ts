import { Product } from './../products/model/product';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { SchoolEventInfo } from './model/event';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, EventEmitter, Output, ElementRef } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListEventItemsActions from './actions/list-event-items-action';
import * as ListEventProducts from './actions/list-event-products.action';
import * as ListCartItemsActions from './../cart/actions/list-cart-items-action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
import { Subscription } from 'rxjs/Subscription';
/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  selector: 'event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {
  selectedCategoryId = "CANTEEN";
  events: SchoolEventInfo[] = [];
  canteenEvents: SchoolEventInfo[] = [];
  schoolEvents: SchoolEventInfo[] = [];

  showSpinner: boolean;
  eventType = "CANTEEN"
  doneLoadingEvent: boolean;
  retrievingMsg = 'Retieving events, please wait...';


  obsListEventItems: Observable<SchoolEventInfo[]>;
  subObsListEventItems: Subscription;

  obsListEventItemsError: Observable<any>;
  subObsListEventItemsError: Subscription;

  obsProducts: Observable<Product[]>;
  subObsProducts: Subscription;
  obsProductsError: Observable<any>;
  subObsProductsError: Subscription;

  isRequiredToLoadSchoolEvents: boolean = true;

  constructor(private _store: Store<fromRoot.State>, private _route: Router) {
    this.obsListEventItems = this._store.select(fromRoot.selectListEventSuccess);
    this.obsListEventItemsError = this._store.select(fromRoot.selectListEventFailure);
    this.obsProducts = this._store.select(fromRoot.selectSearchProductsSuccess);
    this.obsProductsError = this._store.select(fromRoot.selectSearchProductsFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this.showSpinner = true;
    this._store.dispatch(new ListEventItemsActions.ListEvent('CANTEEN'));
    this.subObsListEventItems = this.obsListEventItems.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.length > 0) {
            this.events = result;
            if (this.events[0].eventType == "CANTEEN") {
              this.canteenEvents = result;
            }
            if (this.events[0].eventType == "SCHOOL") {
              this.schoolEvents = result;
              this.isRequiredToLoadSchoolEvents = false;
            }

            if (this.isRequiredToLoadSchoolEvents) {
              setTimeout(() => {
                this._store.dispatch(new ListEventItemsActions.ListEvent('SCHOOL'));
              }, 100);
              this.isRequiredToLoadSchoolEvents = false;
            }

            this.doneLoadingEvent = true;
          }
          else {
            this.events = [];
            this.retrievingMsg = 'Currently there is no event..';
            this.doneLoadingEvent = true;
            if (this.isRequiredToLoadSchoolEvents) {
              setTimeout(() => {
                this._store.dispatch(new ListEventItemsActions.ListEvent('SCHOOL'));
              }, 100);
              this.isRequiredToLoadSchoolEvents = false;
            }
          }
          this.showSpinner = false;
        }
      }
    })

    this.subObsListEventItemsError = this.obsListEventItemsError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsProducts = this.obsProducts.subscribe(products => {
      if (!isOnInit && products) {
        this.showSpinner = false;
      }
    })

    this.subObsProductsError = this.obsProductsError.subscribe(err => {
      if (!isOnInit && err) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  startOrder(event: SchoolEventInfo) {
    localStorage.setItem('eventType', event.eventType);
    this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(false));
    this.showSpinner = true;
    this._store.dispatch(new ListEventProducts.ListEventProduct(event.eventId));
    this._route.navigate(['canteenorder/dashboard/products', { mealName: event.eventName, eventType: event.eventType }]);
    let eventDate: Date = new Date(event.eventDate);
    var date = eventDate.getMonth() + 1 + "-" + eventDate.getDate() + "-" + eventDate.getFullYear();
    localStorage.setItem('orderType', 'event');
    localStorage.setItem('eventId', event.eventId + "");
    localStorage.setItem('eventName', event.eventName);
    localStorage.setItem('isSetPrice', event.IsSetPrice + "");
    localStorage.setItem('eventPrice', event.eventPrice + "");
    localStorage.setItem('deliveryDate', date);
    localStorage.setItem('showDate', eventDate.toDateString());

    if (event.event_type) {
      localStorage.setItem('event_type', event.event_type + "");
    }

    this.removeEventStuffFromLocalStorage();

    if (event.eventType == 'CANTEEN') {
      localStorage.setItem('isBagAvailable', event.isBagAvailable + "");
      localStorage.setItem('bagPrice', event.bagPrice + "");
      localStorage.setItem('bagName', event.bagName);
      localStorage.setItem('whoPays', event.whoPays + "");
      if (event.isFundRaisingEvent) {
        localStorage.setItem("isFundRaising", event.isFundRaisingEvent + "");
        localStorage.setItem("eventMoreInfo", JSON.stringify(event));
      }
    }
    this._store.dispatch(new ListCartItemsActions.ListCartItems(date, event.eventId));
  }

  moreInfo(event) {
    localStorage.setItem("eventMoreInfo", JSON.stringify(event));
    this._route.navigate(['canteenorder/dashboard/event-more-info']);
  }

  loadEvents(eventType: string) {
    this.doneLoadingEvent = false;
    if (eventType == 'CANTEEN') {
      this.eventType = 'CANTEEN';
      this._store.dispatch(new ListEventItemsActions.ListEvent(this.eventType));
    }
    else if (eventType == 'SCHOOL') {
      this.eventType = 'SCHOOL';
      this._store.dispatch(new ListEventItemsActions.ListEvent(this.eventType));
    }
  }

  getCSSClass(eventType: string) {
    if (eventType == this.eventType) {
      return 'w3-green';
    }
    else
      return 'w3-white'
  }

  removeEventStuffFromLocalStorage() {
    localStorage.removeItem('isBagAvailable');
    localStorage.removeItem('bagPrice');
    localStorage.removeItem('bagName');
    localStorage.removeItem('whoPays');
    localStorage.removeItem('isFundRaising');
  }

  ngOnDestroy() {
    if (this.subObsListEventItems)
      this.subObsListEventItems.unsubscribe();
    if (this.subObsListEventItemsError)
      this.subObsListEventItemsError.unsubscribe();
    if (this.subObsProducts)
      this.subObsProducts.unsubscribe();
    if (this.subObsProductsError)
      this.subObsProductsError.unsubscribe();
  }
}
