import { Config } from './../../config/config';
import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { SchoolEventInfo } from '../model/event';
import { Product } from '../../products/model/product';
import * as ListEventItemsActions from './../actions/list-event-items-action';
import * as ListEventProducts from './../actions/list-event-products.action';
import * as ListCartItemsActions from './../../cart/actions/list-cart-items-action';
import * as ShowHideSidebarActions from './../../sidebar/actions/show-hide-sidebar.actions';

@Component({
    selector: 'event-more-info',
    templateUrl: './event-more-info.component.html',
    styleUrls: ['./event-more-info.component.css'],

})
export class EventDescriptionComponent implements OnInit {

    eventType: "SCHOOL";
    event: any;
    currency: string;
    eventImages: any = [];
    eventDocuments: any = [];

    obsListEventItems: Observable<SchoolEventInfo[]>;
    subObsListEventItems: Subscription;

    obsListEventItemsError: Observable<any>;
    subObsListEventItemsError: Subscription;

    obsProducts: Observable<Product[]>;
    subObsProducts: Subscription;
    obsProductsError: Observable<any>;
    subObsProductsError: Subscription;



    constructor(private _store: Store<fromRoot.State>, private config: Config, private _eref: ElementRef, private _route: Router) {
        this.currency = this.config.getCurrency();
        this.obsListEventItems = this._store.select(fromRoot.selectListEventSuccess);
        this.obsListEventItemsError = this._store.select(fromRoot.selectListEventFailure);
        this.obsProducts = this._store.select(fromRoot.selectSearchProductsSuccess);
        this.obsProductsError = this._store.select(fromRoot.selectSearchProductsFailure);

    }


    ngOnInit() {
        this.event = JSON.parse(localStorage.getItem("eventMoreInfo"));
        // console.log(this.event);
        if (this.event) {
            if (this.event.images) {
                this.eventImages = this.event.images.split("| ");

            }

            if (this.event.documents) {
                this.eventDocuments = this.event.documents.split("| ");
            }
        }
        if (!this.event) {

        }
    }


    startOrder(event) {
        this.eventType = "SCHOOL";
        localStorage.setItem('eventType', this.eventType);
        this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(false));

        //this.showSpinner = true;
        this._store.dispatch(new ListEventProducts.ListEventProduct(event.eventId));
        this._route.navigate(['canteenorder/dashboard/products', { mealName: event.eventName, eventType: this.eventType }]);
        let eventDate: Date = new Date(event.eventDate);
        var date = eventDate.getMonth() + 1 + "-" + eventDate.getDate() + "-" + eventDate.getFullYear();
        localStorage.setItem('orderType', 'event');
        localStorage.setItem('eventId', event.eventId + "");
        localStorage.setItem('eventName', event.eventName);
        localStorage.setItem('isSetPrice', event.IsSetPrice + "");
        localStorage.setItem('eventPrice', event.eventPrice + "");
        localStorage.setItem('deliveryDate', date);
        localStorage.setItem('showDate', eventDate.toDateString());

        if (event.event_type) {
            localStorage.setItem('event_type', event.event_type + "");
        }

        this.removeEventStuffFromLocalStorage();



        this._store.dispatch(new ListCartItemsActions.ListCartItems(date, event.eventId));

    }

    removeEventStuffFromLocalStorage() {
        localStorage.removeItem('isBagAvailable');
        localStorage.removeItem('bagPrice');
        localStorage.removeItem('bagName');
        localStorage.removeItem('whoPays');
        localStorage.removeItem('isFundRaising');
    }

    ngOnDestroy() {
        if (this.subObsListEventItems)
            this.subObsListEventItems.unsubscribe();
        if (this.subObsListEventItemsError)
            this.subObsListEventItemsError.unsubscribe();
        if (this.subObsProducts)
            this.subObsProducts.unsubscribe();
        if (this.subObsProductsError)
            this.subObsProductsError.unsubscribe();
    }
}
