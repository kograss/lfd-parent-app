import { AlertService } from './../../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { EventDescriptionComponent } from './event-more-info.component';
import { SafeHtmlPipe } from '../../common/SafeHtml.pipe';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        EffectsModule.forFeature([])        
    ],
    declarations: [EventDescriptionComponent],
    exports: [EventDescriptionComponent],
    providers: [ AlertService]
})
export class EventMoreInfoModule {

}
