import { OnInit, OnDestroy, Component, Input } from "@angular/core";
import { Store } from "@ngrx/store";
import { Router, ActivatedRoute } from "@angular/router";
import * as fromRoot from '../../reducers';
import { SchoolEventInfo } from "../model/event";
import { Student } from "../../products/model/student";
import { StripeComponent } from "../../stripe/stripe.component";
import { Parent } from "../../user-profile/model/parent";
import { Observable, Subscription } from "rxjs";
import { EventService } from "../event.service";
import { AlertService } from "../../alert/alertService";

@Component({
  selector: 'fund-raising-event',
  templateUrl: 'fund-raising.component.html',
  styleUrls: ['./fund-raising.component.css']
})
export class FundRaisingEventComponent implements OnInit, OnDestroy {
  @Input() eventId: number;
  @Input() students: any[];

  parent: Parent;

  amount: number;
  isAmountInvalid: boolean = false;
  showSpinner: boolean;
  payButtonClicked: boolean;
  showStudentList: boolean = false;
  totalAmount: number = 0;

  eventName: string;
  eventDescription: string;
  event: any;
  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentError: Observable<any>;
  subObsListParentError: Subscription;

  showStripeModal: boolean = false;
  stripePaymentData: any;
  chargeAmount: number;


  constructor(private _store: Store<fromRoot.State>, private _route: Router, private _router: ActivatedRoute,
    private eventService: EventService, private alertService: AlertService) {
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListParentError = this._store.select(fromRoot.selectListParentInfoFailure);

  }

  ngOnInit() {
    let isOnInit = true;
    this.showSpinner = true;

    this.event = JSON.parse(localStorage.getItem("eventMoreInfo"));
    this._router.params.subscribe(params => {
      this.eventName = params['mealName'];
    });
    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
        this.showSpinner = false;
      }
      else {
        this._route.navigate(['canteenorder/error/' + 204]);
      }
    })

    this.subObsListParentError = this.obsListParentError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
  }

  showStudents() {
    this.showStudentList = true;
  }

  hideStudents() {
    this.showStudentList = false;
  }

  selectStudents(studentId, event) {
    let student = this.students.find(e => e.studentId == studentId);
    if (event.target.checked) {
      if (student) {
        student.isSelected = true;
      }
    }
    else {
      if (student) {
        student.isSelected = false;
        student.donationAmount = 0
        this.calculateTotal();
      }
    }
  }

  calculateTotal() {
    this.totalAmount = 0;
    this.students.forEach(student => {
      if (student.isSelected && student.donationAmount) {
        this.totalAmount = this.totalAmount + student.donationAmount;
      }
    })
  }

  makeDonation() {
    //1. Validate all amounts 
    //2. create object for student 
    //3. Show total amount
    //3. show stripe 
    this.payButtonClicked = true;
    //add validations
    if (this.totalAmount <= 0 || isNaN(this.totalAmount)) {
      //this.isAmountInvalid = true;
      let message = 'Amount is invalid';
      this.alertService.warn(message);
      this.payButtonClicked = false;
    }
    else {
      this.isAmountInvalid = false;
      this.totalAmount = +this.totalAmount;
      this.stripePay(this.totalAmount);
    }


  }

  stripePay(amount: number) {

    let originalAmt = amount;
    //commented for school 72 TODO: this should be configurable for event
    //amount += amount * 0.015 + 0.30;
    //amount = Math.trunc((amount + 0.00001) * 100) / 100;
    this.chargeAmount = amount;

    if (this.parent) {
      // this._stripe.openCheckout(this.eventName, amount, this.parent.email, (token: any) =>
      //     this.takePayment(token, originalAmt));
      this.showStripeModal = true;
      this.stripePaymentData = {
        name: this.eventName,
        amount: this.chargeAmount * 100,
        email: this.parent.email
      };
      this.payButtonClicked = false;
    }
    else {
      this.payButtonClicked = false;
      this._route.navigate(['canteenorder/error/' + 204]);
    }

  }

  stripeCallback(token) {
    console.log('token', token);
    this.showStripeModal = false;
    this.takePayment(token, this.chargeAmount);
  }


  takePayment(token: any, amount: number) {
    let students: any = [];
    this.students.forEach(student => {
      if (student.isSelected && student.donationAmount) {
        students.push({ studentId: student.studentId, amount: student.donationAmount });
      }
    });

    if (students.length <= 0) {
      //show error;
      this.alertService.error("Error occured while create payment. Please try again.");
    }
    else {
      this.showSpinner = true;

      let paymentRequest: any = {};
      paymentRequest.eventId = this.eventId;
      paymentRequest.eventName = this.eventName;
      paymentRequest.students = students;
      paymentRequest.stripeToken = token.id;
      paymentRequest.amount = amount;


      this.showSpinner = true;
      this.eventService.donationPayment(paymentRequest).subscribe(data => {
        this.showSpinner = false;
        this._route.navigate(['canteenorder/dashboard/donation-confirmation']);

      },
        (err) => {
          this.showSpinner = false;
          this.hideStudents();
          let errormessage = err._body ? err._body : err.error;
          this.alertService.error(errormessage);
          console.log(err)
        });
    }
  }

  ngOnDestroy() {
    if (this.subObsListParentError)
      this.subObsListParentError.unsubscribe();

    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
  }
}