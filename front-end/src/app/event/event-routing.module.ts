import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EventComponent } from './event.component';
import { DonationConfirmationComponent } from './donation-confirmation/donation-confirmation.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'event', component: EventComponent },
      
    ])
  ],
  exports: [RouterModule]
})
export class EventRoutingModule { }
