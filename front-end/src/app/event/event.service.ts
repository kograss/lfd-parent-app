import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class EventService {


    private _getCanteenEventsUrl = "Events/GetAllEvents";
    private _getSchoolEventsUrl = "SchoolEvent/GetAllEvents";
    private _getCanteenEventProductsUrl = "Events/GetAllProducts/{eventId}";
    private _getSchoolEventProductsUrl = "SchoolEvent/GetAllProducts/{eventId}";
    private _donationPaymentURL = "Stripe/MakeDonation";

    constructor(private _http: HttpClient) {
    }

    getAllEvents(eventType: string):any {
        if (eventType == 'CANTEEN') {
            return this._http.get(this._getCanteenEventsUrl);
        }
        else if (eventType == 'SCHOOL') {
            return this._http.get(this._getSchoolEventsUrl);
        }

    }


    getAllProducts(eventId: number):any {
        let eventType = localStorage.getItem('eventType');


        if (eventType == 'CANTEEN') {
            return this._http.get(this._getCanteenEventProductsUrl.replace("{eventId}", eventId.toString()));
        }
        else if (eventType == 'SCHOOL') {
            return this._http.get(this._getSchoolEventProductsUrl.replace("{eventId}", eventId.toString()));
        }

    }

    donationPayment(params: any) {

        // let body = {
        //     stripeToken: token.id,
        //     amount: amount,
        //     studentId: studentId,
        //     eventId: eventId,
        //     eventName: eventName
        // }
        let bodyString = JSON.stringify(params);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };

        return this._http.post(this._donationPaymentURL, bodyString, options);
        //  return this._http.post(this._donationPaymentURL, paymentRequest);

        //return this._http.post(this._donationPaymentURL, paymentRequest);
    }
}
