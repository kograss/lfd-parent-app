import { TopUpService } from './top-up.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as TopUpActions from './actions/top-up.actions';
import * as ManualTopupActions from './actions/manul-topup.actions';
import { catchError, map, switchMap } from 'rxjs/operators';



@Injectable()
export class TopUpEffects {

    constructor(
        private actions$: Actions,
        private topUpService: TopUpService
    ) { }

    @Effect()
    takePayment$: Observable<Action> = this.actions$.pipe(ofType<TopUpActions.TopUp>
        (TopUpActions.TOPUP)
        ,map(action=>action)
        ,switchMap(
        payload => this.topUpService.topUpRecharge(payload.amount,payload.token).pipe(
            map(results => new TopUpActions.TopUpSuccess(results))
            ,catchError(err =>
                of({ type: TopUpActions.TOPUP_FAILURE, exception: err })
            )))
        );

        @Effect()
        completeManualTopup: Observable<Action> = this.actions$.pipe(ofType<ManualTopupActions.AddManualTopup>
            (ManualTopupActions.ADDMANUALTOPUP)
            ,map(action=>action)
            ,switchMap(
            payload => this.topUpService.completeManualTopup(payload.manualTopup).pipe(
                map(results => new ManualTopupActions.AddManualTopupSuccess(results))
                ,catchError(err =>
                    of({ type: ManualTopupActions.ADDMANUALTOPUP_FAILURE, exception: err })
                )))
            );
    }