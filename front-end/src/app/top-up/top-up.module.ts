import { AlertService } from './../alert/alertService';
import { TopUpService } from './top-up.service';
import { TopUpComponent } from './top-up.component';
import { TopUpEffects } from './top-up.effects';
import { StripeComponent } from './../stripe/stripe.component';
import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { ManualTopup } from './model/ManualTopup';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';
import { StripeModule } from '../stripe/stripe.module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        StripeModule,
        CommonMenuModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        EffectsModule.forFeature([TopUpEffects]),
    ],
    declarations: [TopUpComponent],
    exports: [TopUpComponent],
    providers: [TopUpService, StripeComponent, AlertService]
})
export class TopUpModule {

}