import { AlertService } from './../alert/alertService';
import { Subscription } from 'rxjs';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Parent } from './../user-profile/model/parent';
import { Store } from '@ngrx/store';
import { StripeComponent } from './../stripe/stripe.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as TopUpActions from './actions/top-up.actions';
import * as fromRoot from './../reducers';
import * as ListParentActions from './../user-profile/actions/list-user-action';
import { TopupResult } from "./model/TopupResult";
import { ManualTopup } from './model/ManualTopup';
import * as ManualTopupActions from './actions/manul-topup.actions';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';
import { SchoolInfo } from '../common/school-info/model/school-info';

@Component({
  selector: 'top-up',
  templateUrl: './top-up.component.html',
  styleUrls: ['./top-up.component.css']
})
export class TopUpComponent implements OnInit, OnDestroy {
  schoolInfo: SchoolInfo;
  topUpOptions: number[] = [];
  selectedAmount: number = 10;
  chargeAmount: number;
  showSpinner = false;
  parent: Parent;
  topUpResult: TopupResult;
  currency: string;
  payButtonClicked: boolean;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentError: Observable<any>;
  subObsListParentError: Subscription;

  obsTopUpResult: Observable<TopupResult>;
  subObsTopUpResult: Subscription;
  obsTopUpFailure: Observable<any>;
  subObsTopUpFailure: Subscription;

  obsStripePopupState: Observable<boolean>;
  subObsStripePopupState: Subscription;

  manualTopup: ManualTopup;
  obsManualTopUpResult: Observable<string>;
  subObsManualTopUpResult: Subscription;
  obsManualTopUpFailure: Observable<any>;
  subObsanualTopUpFailure: Subscription;

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  showStripeModal: boolean = false;
  stripePaymentData: any;
  isTopupEnabled: boolean = true;

  constructor(private _store: Store<fromRoot.State>,
    private _router: Router, private config: Config, private alertService: AlertService) {

    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);

    this.manualTopup = new ManualTopup();
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListParentError = this._store.select(fromRoot.selectListParentInfoFailure);
    this.obsTopUpResult = this._store.select(fromRoot.selectTopUpSuccess);
    this.obsTopUpFailure = this._store.select(fromRoot.selectTopUpFailure);
    this.obsStripePopupState = this._store.select(fromRoot.selectStripePopupClosed);
    this.currency = this.config.getCurrency();

    this.obsManualTopUpResult = this._store.select(fromRoot.AddManualTopSuccess);
    this.obsManualTopUpFailure = this._store.select(fromRoot.addManualTopupFailure);
  }

  ngOnInit() {

    this._store.dispatch(new ListSchoolInfoActions.ListSchoolInfo);


    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        if (this.schoolInfo && this.schoolInfo.topup_method && this.schoolInfo.topup_method.includes('4')) {
          this.isTopupEnabled = true;
        }
        else {
          this.isTopupEnabled = false;
        }
      }
    })

    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {

    });


    let isOnInit = true;

    /*if(!this.parent){
      this._store.dispatch(new ListParentActions.ListParent);
    }*/
    for (var i = 20; i <= 200; i = i + 5) {
      this.topUpOptions.push(i);
    }
    //charges for recharge
    this.calculateCharge();

    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
      }
      else {
        this._router.navigate(['canteenorder/error/', 204]);
      }
    })

    this.subObsListParentError = this.obsListParentError.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsManualTopUpResult = this.obsManualTopUpResult.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result)
            this.alertService.success(result);
        }
        this.showSpinner = false;
      }
    })

    this.subObsanualTopUpFailure = this.obsTopUpFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })


    this.subObsTopUpResult = this.obsTopUpResult.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.topUpResult = result;
          if (this.topUpResult.isSucess) {
            if (this.parent) {
              this.parent.credit = result.parentCredit;
            }
          }
          if (this.topUpResult.message)
            this.alertService.success(this.topUpResult.message);
          //alert(this.topUpResult.message);
        }
        this.showSpinner = false;
        this.payButtonClicked = false;
      }
    })

    this.subObsTopUpFailure = this.obsTopUpFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsStripePopupState = this.obsStripePopupState.subscribe(res => {
      if (!isOnInit) {
        if (res) {
          this.payButtonClicked = false;
        }
      }
    })





    isOnInit = false;
  }

  payUsingCard() {
    this.payButtonClicked = true;
    if (this.parent) {
      // this.stripe.openCheckout('TopUp', this.chargeAmount * 100, this.parent.email, (token: any) =>
      //   this.takePayment(this.selectedAmount, token));
      this.showStripeModal = true;
      this.stripePaymentData = {
        name: "TopUp",
        amount: this.chargeAmount * 100,
        email: this.parent.email
      };
    }
  }

  stripeCallback(token) {
    console.log('token', token)
    this.showStripeModal = false;
    this.takePayment(this.selectedAmount, token);
  }

  takePayment(amount: number, token: any) {
    this.showSpinner = true;
    this._store.dispatch(new TopUpActions.TopUp(amount, token));
  }

  calculateCharge() {
    let amount = +this.selectedAmount;
    amount += amount * 0.015 + 0.30;
    amount = Math.trunc((amount + 0.00001) * 100) / 100;
    this.chargeAmount = amount;
  }

  requestManualTopup() {
    console.log(this.topUpOptions);
    console.log(this.manualTopup);
    this._store.dispatch(new ManualTopupActions.AddManualTopup(this.manualTopup));

  }

  ngOnDestroy() {
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsListParentError)
      this.subObsListParentError.unsubscribe();
    if (this.subObsStripePopupState)
      this.subObsStripePopupState.unsubscribe();
    if (this.subObsTopUpFailure)
      this.subObsTopUpFailure.unsubscribe();
    if (this.subObsTopUpResult)
      this.subObsTopUpResult.unsubscribe();
    if (this.subObsManualTopUpResult)
      this.subObsManualTopUpResult.unsubscribe();
    if (this.subObsanualTopUpFailure)
      this.subObsanualTopUpFailure.unsubscribe();

    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();
  }
}
