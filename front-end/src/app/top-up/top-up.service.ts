import { OrderDetails } from './../checkout/model/oreder-details';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ManualTopup } from './model/ManualTopup';
import { Observable } from 'rxjs';

@Injectable()
export class TopUpService {
    public _stripePaymentUrl='Topup';
    public _manualTopupUrl = 'Topup/Manualtopup';

    constructor(private _http:HttpClient){}
    topUpRecharge(amount: number, token: any):Observable<any> {
        let body = {
            stripeToken: token.id,
            amount: amount
        };
        let bodyString = JSON.stringify(body);
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };

       return this._http.post(this._stripePaymentUrl, bodyString, options);
    }

    completeManualTopup(manualTopup: ManualTopup):Observable<any>{
        let headers = new HttpHeaders(
            {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
            });
          let options = { headers: headers };
          let body = JSON.stringify(manualTopup);
          return this._http.post(this._manualTopupUrl, body, options);
    }
}