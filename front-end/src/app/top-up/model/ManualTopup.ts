export class ManualTopup {
    topupAmount: number;
    topupMethod: string;
    comment: string;
}