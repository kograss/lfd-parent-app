import { ManualTopup } from './../model/ManualTopup';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const ADDMANUALTOPUP = 'ADD MANUAL TOPUP';
export const ADDMANUALTOPUP_SUCCESS = 'ADD MANUAL TOPUP SUCCESS';
export const ADDMANUALTOPUP_FAILURE = 'ADD MANUL TOPUP FAILURE';


export class AddManualTopup {
    readonly type = ADDMANUALTOPUP;
    constructor(public manualTopup: ManualTopup) {
    }

}

export class AddManualTopupSuccess implements Action {
    readonly type = ADDMANUALTOPUP_SUCCESS;
    constructor(public manualtopupResult: string) {
       
    }

}

export class AddManualTopupFailure implements Action {
    readonly type = ADDMANUALTOPUP_FAILURE;
    constructor(public err: any) {
    }

}


export type All = AddManualTopup
    | AddManualTopupSuccess | AddManualTopupFailure 