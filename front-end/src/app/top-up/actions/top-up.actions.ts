import { Action } from '@ngrx/store';
import { TopupResult } from "../model/TopupResult";


export const TOPUP = 'TOPUP ACTION';
export const TOPUP_SUCCESS = 'TOPUP SUCCESS';
export const TOPUP_FAILURE = 'TOPUP FAILURE';
export const CLEARTOPUPRESULT = 'CLEAR TOPUP RESULT';



export class TopUp {
    amount: number
    token: any

    readonly type = TOPUP;
    constructor(amount, token) {
        this.amount = amount;
        this.token = token;
    }
}

export class TopUpSuccess implements Action {
    readonly type = TOPUP_SUCCESS;
    constructor(public topUpResult: TopupResult) {
    }

}

export class TopUpFailure implements Action {
    readonly type = TOPUP_FAILURE;
    constructor(public err: any) {
    }
}

export class ClearTopUpResult implements Action {
    readonly type = CLEARTOPUPRESULT;
    constructor() {
    }

}


export type All = TopUp
    | TopUpSuccess | TopUpFailure | ClearTopUpResult
