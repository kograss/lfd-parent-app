import * as TopUpAction from '../actions/top-up.actions';
import { TopupResult } from "../model/TopupResult";

export interface State {
    result: TopupResult,
    err: any
}

const initialState: State = {
    result: new TopupResult(),
    err: {}
};

export function reducer(state = initialState, action: TopUpAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case TopUpAction.TOPUP_SUCCESS: {
            return {
                ...state,
                result: action.topUpResult
            }
        }
        case TopUpAction.TOPUP_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        case TopUpAction.CLEARTOPUPRESULT: {
            return initialState;
        }

        default: {
            return state;
        }

    }

}
