import * as TopUpAction from '../actions/manul-topup.actions';

export interface State {
    result: string,
    err: any
}

const initialState: State = {
    result: "",
    err: {}
};

export function reducer(state = initialState, action: TopUpAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case TopUpAction.ADDMANUALTOPUP_SUCCESS: {
            return {
                ...state,
                result: state.result
            }
        }
        case TopUpAction.ADDMANUALTOPUP_FAILURE: {
            return {
                ...state,
                err: state.err
            }
        }

        

        default: {
            return state;
        }

    }

}
