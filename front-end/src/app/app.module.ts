import { MyAccountComponent } from './my-account/my-account.component';
import { ShiftsByDateModule } from './roster-shifts-by-date/shifts-by-date.module';
import { RosterHomeModule } from './roster-home/roster-home.module';
import { DatePickerModule } from './date-picker/date-picker.module';
import { UpdateParentPlanModule } from './update-parent-plan/update-parent-plan.module';
import { TopUpModule } from './top-up/top-up.module';
import { UserProfileModule } from './user-profile/user-profile.module';
import { ChildrenModule } from './children/children.module';
import { Config } from './config/config';
import { LoginModule } from './login/login.module';
import { FormsModule } from '@angular/forms';
import { OrderHistoryModule } from './order-history/order-history.module';
import { NavBarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SchoolInfoModule } from './common/school-info/school-info.module';
import { CheckoutModule } from './checkout/checkout.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutModule } from './about/about.module';
import { HomeModule } from './home/home.module';
import { CartModule } from './cart/cart.module';
import { CategoriesModule } from './categories/categories.module';
import { ProductsModule } from './products/products.module';
import { NewsModule } from './news/news.module';
import { Router, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { reducers } from './reducers';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { DataTableModule } from 'angular2-datatable';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoadingModule } from 'ngx-loading';
// import { StripeComponent } from './stripe/stripe.component';
import { AlertModule } from './alert/alert.module';
import { TransactionHistoryModule } from './transaction-history/transaction-history.module';
import { EffectsModule } from '@ngrx/effects';
import { GetCurrentTimeEffects } from './navbar/get-current-time.effects'
import { GetCurrentTimeService } from './navbar/get-current-time.service';
import { CouponsModule } from './coupons/coupons.module';
import { EventMoreInfoModule } from './event/more-info/event-more-info.module';
import { HomeMainComponent } from './home-main/home-main.component';
import { CommonMenuModule } from './common/components/common-menu/common-menu.module';
import { RegisterModule } from './register/registration.module';
import { BannedFoodModule } from './banned-food/banned-food.module';
import { ListHomeChecksService } from './home-main/home.service';
import { CommonService } from './common/common.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { InterceptedHttp } from './interceptor/InterceptedHttp.interceptor';
import { UniformNewsModule } from './uniform/uniform-news/uniform-news.module';
import { UniformModule } from './uniform/uniform/uniform.module';
import { UniformCartModule } from './uniform/cart/cart.module';
import { UniformCheckoutModule } from './uniform/checkout/uniform-checkout.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SupportComponent } from './support/support.component';

@NgModule({
  imports: [
    AlertModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    AboutModule,
    HomeModule,
    CartModule,
    ProductsModule,
    NewsModule,
    CategoriesModule,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
        strictStateSerializability: false,
        strictActionSerializability: false,
        strictActionWithinNgZone: false,
        strictActionTypeUniqueness: false,
      },
    }),
    CheckoutModule,
    SchoolInfoModule,
    OrderHistoryModule,
    DataTableModule,
    LoginModule,
    ChildrenModule,
    UserProfileModule,
    TopUpModule,
    ForgotPasswordModule,
    UpdateParentPlanModule,
    DatePickerModule,
    RosterHomeModule,
    ShiftsByDateModule,
    TransactionHistoryModule,
    CouponsModule,
    EventMoreInfoModule,
    CommonMenuModule,
    RegisterModule,
    BannedFoodModule,
    UniformNewsModule,
    UniformModule,
    UniformCartModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    EffectsModule.forFeature([GetCurrentTimeEffects]),
  ],
  declarations: [
    AppComponent,
    OrderConfirmationComponent,
    ErrorPageComponent,
    SidebarComponent,
    NavBarComponent,
    DashboardComponent,
    MyAccountComponent,
    HomeMainComponent,
    SupportComponent,
  ],
  exports: [],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptedHttp, multi: true },
    Config,
    GetCurrentTimeService,
    ListHomeChecksService,
    CommonService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
