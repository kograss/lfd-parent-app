import { RosterNews } from './../model/roster-news';
import { Action } from '@ngrx/store';

export const LISTROSTERNEWS = 'LIST ROSTER NEWS';
export const LISTROSTERNEWS_SUCCESS = '[ROSTERNEWS] LIST SUCCESS';
export const LISTROSTERNEWS_FAILURE = '[ROSTERNEWS] LIST FAILURE';

export class ListRosterNews {
    readonly type = LISTROSTERNEWS;
    constructor() {
    }
}

export class ListRosterNewsSuccess implements Action {

    readonly type = LISTROSTERNEWS_SUCCESS;
    constructor(public rosterNews: RosterNews[]) {
        if (this.rosterNews == null) {
            this.rosterNews = [];
        }
    }

}

export class ListRosterNewsFailure implements Action {

    readonly type = LISTROSTERNEWS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListRosterNews
    | ListRosterNewsSuccess | ListRosterNewsFailure 