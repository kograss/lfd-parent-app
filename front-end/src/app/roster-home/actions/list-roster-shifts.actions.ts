import { RosterShift } from './../model/roster-shift';
import { Action } from '@ngrx/store';

export const LISTROSTERSHIFT = 'LIST ROSTER SHIFT';
export const LISTROSTERSHIFT_SUCCESS = '[LISTROSTERSHIFT] SUCCESS';
export const LISTROSTERSHIFT_FAILURE = '[LISTROSTERSHIFT] FAILURE';

export class ListRosterShift {
    readonly type = LISTROSTERSHIFT;
    constructor() {
    }
}

export class ListRosterShiftSuccess implements Action {

    readonly type = LISTROSTERSHIFT_SUCCESS;
    constructor(public rosterShifts: RosterShift[]) {
        if (this.rosterShifts == null) {
            this.rosterShifts = [];
        }
    }

}

export class ListRosterShiftFailure implements Action {

    readonly type = LISTROSTERSHIFT_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListRosterShift
    | ListRosterShiftSuccess | ListRosterShiftFailure 