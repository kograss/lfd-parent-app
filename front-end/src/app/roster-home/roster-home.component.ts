import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { RosterShift } from './model/roster-shift';
import { RosterNews } from './model/roster-news';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from './../reducers';
import * as ListRosterNewsActions from './actions/list-roster-news.action';
import * as ListRosterShiftsActions from './actions/list-roster-shifts.actions';
import { Volunteer } from '../roster-shifts-by-date/model/volunteer-shifts';

@Component({
  selector: 'app-roster-home',
  templateUrl: './roster-home.component.html',
  styleUrls: ['./roster-home.component.css']
})
export class RosterHomeComponent implements OnInit, OnDestroy {

  showDetails: boolean = false;
  shiftVolunteers: Volunteer[] = []; 

  rosterNews: RosterNews[] = [];
  rosterShifts: RosterShift[] = [];
  showSpinner: boolean;
  selectedMonth: number = new Date().getMonth();
  obsListRosterNews: Observable<RosterNews[]>;
  subObsListRosternews: Subscription;
  obsListRosterNewsFailure: Observable<any>;
  subObsListRosterNewsFailure: Subscription;

  obsListRosterShifts: Observable<RosterShift[]>;
  subObsListRosterShifts: Subscription;
  obsListRosterShiftsFailure: Observable<any>;
  suboObsListRosterShiftsFailure: Subscription;

  constructor(private _store: Store<fromRoot.State>, private _router: Router) {
    this.obsListRosterNews = this._store.select(fromRoot.selectListRosterNewsSuccess);
    this.obsListRosterNewsFailure = this._store.select(fromRoot.selectListRosterNewsFailure);
    this.obsListRosterShifts = this._store.select(fromRoot.selectListRosterShiftsSuccess);
    this.obsListRosterShiftsFailure = this._store.select(fromRoot.selectListRosterShiftsFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this.subObsListRosternews = this.obsListRosterNews.subscribe(
      result => {
        if (!isOnInit) {
          if (result) {
            this.rosterNews = result;
          }
        }
      }
    )

    this.subObsListRosterNewsFailure = this.obsListRosterNewsFailure.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsListRosterShifts = this.obsListRosterShifts.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.rosterShifts = result;
        }
        this.showSpinner = false;
      }
    })

    this.suboObsListRosterShiftsFailure = this.obsListRosterShiftsFailure.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    this.showSpinner = true;
    this._store.dispatch(new ListRosterShiftsActions.ListRosterShift);
    this._store.dispatch(new ListRosterNewsActions.ListRosterNews);
    isOnInit = false;
  }
  showCalendar() {
    this._router.navigate(['canteenorder/dashboard/roster-calendar', { selectedMonth: this.selectedMonth }])
  }

  displayDetails(volunteers) {
    this.shiftVolunteers = volunteers;
    console.log(this.shiftVolunteers);
    this.showDetails = true;
  }

  hideDetails(){
    this.showDetails = false;
  }

  ngOnDestroy() {
    if (this.subObsListRosternews)
      this.subObsListRosternews.unsubscribe();
    if (this.subObsListRosterNewsFailure)
      this.subObsListRosterNewsFailure.unsubscribe();
    if (this.subObsListRosterShifts)
      this.subObsListRosterShifts.unsubscribe();
    if (this.suboObsListRosterShiftsFailure)
      this.suboObsListRosterShiftsFailure.unsubscribe();
  }

}
