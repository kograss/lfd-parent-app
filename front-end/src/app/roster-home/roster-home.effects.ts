import { RosterHomeService } from './roster-home.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListRosterNewsActions from './actions/list-roster-news.action';
import * as ListRosterShiftsActions from './actions/list-roster-shifts.actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class RosterHomeEffects {

    constructor(
        private actions$: Actions,
        private rosterHomeService: RosterHomeService
    ) { }

    @Effect()
    listRosterNews$: Observable<Action> = this.actions$.pipe(ofType<ListRosterNewsActions.ListRosterNews>
        (ListRosterNewsActions.LISTROSTERNEWS)
        ,map(action => action)
        ,switchMap(
        payload => this.rosterHomeService.getRosterNews().pipe(
            map(results => new ListRosterNewsActions.ListRosterNewsSuccess(results))
            ,catchError(err =>
                of({ type: ListRosterNewsActions.LISTROSTERNEWS_FAILURE, exception: err })
            )))
        );

    @Effect()
    getRosterShifts$: Observable<Action> = this.actions$.pipe(ofType<ListRosterShiftsActions.ListRosterShift>
        (ListRosterShiftsActions.LISTROSTERSHIFT)
        ,map(action => action)
        ,switchMap(
        payload => this.rosterHomeService.getRosterShifts().pipe(
            map(results => new ListRosterShiftsActions.ListRosterShiftSuccess(results))
            ,catchError(err =>
                of({ type: ListRosterShiftsActions.LISTROSTERSHIFT_FAILURE, exception: err })
            )))
        );
}
