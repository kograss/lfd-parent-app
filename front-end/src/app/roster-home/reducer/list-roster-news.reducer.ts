import { RosterNews } from './../model/roster-news';
import * as ListRosterNewActions from './../actions/list-roster-news.action';

export interface State{
    result : RosterNews[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListRosterNewActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListRosterNewActions.LISTROSTERNEWS_SUCCESS: {
            return {
                ...state,
                result: action.rosterNews
            }
        }
        case ListRosterNewActions.LISTROSTERNEWS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
