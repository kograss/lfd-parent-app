import { RosterShift } from './../model/roster-shift';
import * as ListShiftActions from '../actions/list-roster-shifts.actions';

export interface State{
    result : RosterShift[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListShiftActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListShiftActions.LISTROSTERSHIFT_SUCCESS: {
            return {
                ...state,
                result: action.rosterShifts
            }
        }
        case ListShiftActions.LISTROSTERSHIFT_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
