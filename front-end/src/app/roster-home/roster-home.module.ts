import { RosterHomeService } from './roster-home.service';
import { RosterHomeComponent } from './roster-home.component';
import { RosterHomeEffects } from './roster-home.effects';
import { LoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { DataFilterPipe } from './../common/data-filter.pipe';
import { OrderDetailsModule } from './../order-details/order-details.module';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';

@NgModule({
  imports: [
    FormsModule,
    EffectsModule.forFeature([RosterHomeEffects]),
    BrowserModule,
    OrderDetailsModule,
    DataTableModule,
    CommonModule,
    CommonMenuModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    RouterModule.forRoot([{ path: 'roster', component: RosterHomeComponent }])
  ],
  declarations: [RosterHomeComponent],
  exports: [RosterHomeComponent],
  providers: [RosterHomeService]
})
export class RosterHomeModule { }
