export class RosterNews {
    id: number;
    description: string;
    subject: string;
    dateEntered: Date;
    comment: string;
}