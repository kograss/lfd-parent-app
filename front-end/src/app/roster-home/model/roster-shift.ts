import { Volunteer } from './volunteer';
export class RosterShift{
    shiftId: number;
    shiftName: string;
    shiftDate: Date;
    volunteers: Volunteer[]=[];
}