import { RosterShift } from './model/roster-shift';
import { RosterNews } from './model/roster-news';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RosterHomeService {

    private _listRosterNewsUrl = "Rota/GetRotaNews";
    private _listRosterShiftsUrl = "Rota/GetParentsShifts";
    constructor(private _http: HttpClient) {
    }

    getRosterNews(): Observable<any> {
        return this._http
            .get(this._listRosterNewsUrl)
            ;
    }

    getRosterShifts(): Observable<any> {
        return this._http
            .get(this._listRosterShiftsUrl)
            ;
    }
    
}

