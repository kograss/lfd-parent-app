import { Router } from '@angular/router';
import { OrderResult } from './../checkout/model/order-result';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListParentActions from './../user-profile/actions/list-user-action';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})

export class OrderConfirmationComponent implements OnInit, OnDestroy {
  orderResult: OrderResult;
  obsPlaceOrder: Observable<OrderResult>;
  subObsPlaceOrder: Subscription;
  obsPlaceOrderError: Observable<any>;
  subObsPlaceOrderError: Subscription;

  constructor(private _store: Store<fromRoot.State>, private _route: Router) {
    this.obsPlaceOrder = this._store.select(fromRoot.selectPlaceOrderSuccess);
    this.obsPlaceOrderError = this._store.select(fromRoot.selectPlaceOrderFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new SidebarActions.SetSidebarName('categories'));
    this.subObsPlaceOrder = this.obsPlaceOrder.subscribe(result => {
      
      if (result.message) {
        this.orderResult = result;
        this._store.dispatch(new ListParentActions.ListParent());
        localStorage.setItem('orderResult', JSON.stringify(result));
      }
      else {
        var orderResult: OrderResult = JSON.parse(localStorage.getItem('orderResult'));
        if (orderResult) {
          this.orderResult = orderResult;
        }
        else {
          this._route.navigate(['canteenorder/dashboard/home']);
        }
      }
    })

    this.subObsPlaceOrderError = this.obsPlaceOrderError.subscribe(error => {
      if (!isOnInit) {
        console.log(error)
      }
    })
  }

  ngOnDestroy() {
    localStorage.removeItem('orderResult')
    if (this.subObsPlaceOrder)
      this.subObsPlaceOrder.unsubscribe();
    if (this.subObsPlaceOrderError)
      this.subObsPlaceOrderError.unsubscribe();
  }
}
