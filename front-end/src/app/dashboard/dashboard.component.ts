import { SidebarComponent } from './../sidebar/sidebar.component';
import { SchoolInfo } from '../common/school-info/model/school-info';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';
import * as ListCartActions from './../cart/actions/list-cart-items-action';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  schoolInfo: SchoolInfo;
  showSidebar: boolean = false;
  showCartBar: boolean = false;

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  @ViewChild(SidebarComponent) sidebar:SidebarComponent; 


  constructor(private _store: Store<fromRoot.State>) {
    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);
  }

  ngOnInit() {
    this._store.dispatch(new ListSchoolInfoActions.ListSchoolInfo);

    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
      }
    })

    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {

    });
  }

  displaySidebar(sidebar) {
   this.sidebar.isSidebarOpen = true;
   this.sidebar.navbarClicked = true;
    if (sidebar == "sidebar") {
      this.showSidebar = true;
    }
    else if (sidebar == "cart") {
      this.showCartBar = true;
    }
  }

  hideSidebar() {
      this.showSidebar = false;
  }

  hideCartBar() {
    this.showCartBar = false;
  }
  ngOnDestroy() {
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();
  }
}
