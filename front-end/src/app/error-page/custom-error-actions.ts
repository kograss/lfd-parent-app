export const ERRORMESSAGE = "ERROR MESSAGE";
export const CLEARERRORMESSAGE = "CLEAR ERROR MESSAGE";

export class SetErrorMessage {
    readonly type = ERRORMESSAGE;
    constructor(public errorMessage:string) { 
    }
}

export class ClearErrorMessage {
    readonly type = CLEARERRORMESSAGE;
    constructor() {        
     }
}

export type All = SetErrorMessage | ClearErrorMessage