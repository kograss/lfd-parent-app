import * as ErrorPageActions from './custom-error-actions';

export interface State {
    errorMessage: string
}

const initialState: State = {
    errorMessage: ''
};

export function reducer(state = initialState, action: ErrorPageActions.All): State {
    ////console.log("Action type" + action.type);
    switch (action.type) {

        case ErrorPageActions.ERRORMESSAGE: {
            return {
                ...state,
                errorMessage: action.errorMessage
            }
        }

        case ErrorPageActions.CLEARERRORMESSAGE: {
            return {
                ...state,
                errorMessage: ''
            }
        }

         default :{
             return state;
         }
    }

}
