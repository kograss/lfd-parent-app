import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit, OnDestroy {
  errorMessage: string = "Oops error occured..";
  status: string;

  constructor(private _route: ActivatedRoute, private _router: Router) {
  }



  ngOnInit() {
    this._route.params.subscribe(params => {
      if (params['status']) {
        this.status = params['status'];
        switch (this.status) {
          case '400':
            this.errorMessage = "The server encountered a temporary error, please try again.";
            break;
          case '204':
            this.errorMessage = "School details or parent details are not available, please contact admin";
            break;
          case '404':
            this.errorMessage = "We can't seem to find the page you're looking for.";
            break;
          case '500':
            this.errorMessage = "Internal Server Error Test.";
            break;
          case '503':
          case '502':
          case '504':
            this.errorMessage = "The server encountered a temporary error.";
            break;
          case 'InvalidDate':
            this.errorMessage = "Date you have selected is invalid, Please select a valid date from home page.";
            break;
          default:
            this.errorMessage = "Sorry, something went wrong.";
            break;
        }
      }
    });
  }
  logout() {
    Cookie.deleteAll('/');
    localStorage.clear();
    this._router.navigate(['/']);
  }

  ngOnDestroy() {
  }
}
