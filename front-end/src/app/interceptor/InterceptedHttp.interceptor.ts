
import { map, catchError } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { from as fromPromise, Observable } from 'rxjs';
import { Cookie } from 'ng2-cookies';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class InterceptedHttp implements HttpInterceptor {
    noHttpResource = "No HTTP resource was found";
    constructor(private _router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = localStorage.getItem("s24tkn");// Cookie.get("s24tkn");
        const headerSettings: { [name: string]: string | string[]; } = {};

        for (const key of request.headers.keys()) {
            headerSettings[key] = request.headers.getAll(key);
        }
        headerSettings['Cache-Control'] = 'no-cache';

        if (token) {
            headerSettings['Authorization'] = "bearer " + token;
        }

        if (request.url != "login" && !request.url.includes('http') && !request.url.includes("uploadFiles") && !request.url.includes("importXLLeads")) {
            if (!headerSettings['Content-Type'])
                headerSettings['Content-Type'] = 'application/json';
        }


        const newHeader = new HttpHeaders(headerSettings);

        let url = this.updateUrl(request.url)

        request = request.clone({
            url: url,
            headers: newHeader
        });

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                switch (error.status) {
                    case 401:
                    case 403:
                        Cookie.deleteAll('/');
                        localStorage.clear();
                        this._router.navigate(['/']);
                        return throwError(error);
                    case 0:
                    case 500:
                    case 501:
                    case 503:
                        this._router.navigate(['canteenorder/error/' + error.status]);
                        return throwError(error);
                    case 404:
                        let body;
                        if (error['_body'])
                            body = JSON.parse(error['_body']);
                        if (body != null) {
                            if (body.Message.indexOf(this.noHttpResource) !== -1) {
                                this._router.navigate(['canteenorder/error/' + error.status]);
                                return
                            }
                            else {
                                return throwError(error);
                            }
                        }
                        else
                            return throwError(error);

                    default:
                        return throwError(error);
                }
            }));
    }

    private updateUrl(req: string) {
        if (req.startsWith('http'))
            return req;
        if (req === "token")
            return environment.loginUrl;
        return environment.origin + req;
    }
}