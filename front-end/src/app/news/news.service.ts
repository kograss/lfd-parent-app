import { News } from './model/news';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NewsService {

    private _url = "CanteenNews";
    
    constructor(private _http: HttpClient) {
    }

    getNews(): Observable<any> {
        return this._http
            .get(this._url)
            ;
    }
}