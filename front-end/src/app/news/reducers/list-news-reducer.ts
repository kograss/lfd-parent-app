import { News } from './../model/news';
import * as ListNewsActions from '../actions/list-news-actions';
export interface State{
    result : News[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListNewsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListNewsActions.LISTNEWS_SUCCESS: {
            return {
                ...state,
                result: action.news
            }
        }
        case ListNewsActions.LISTNEWS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
