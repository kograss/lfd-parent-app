import { LoadingModule } from 'ngx-loading';
import { NewsService } from './news.service';
import { NewsEffects } from './news-effects';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news.component';
import { NewsRoutingModule } from './news-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NewsRoutingModule,
    EffectsModule.forFeature([NewsEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    RouterModule.forRoot([
      { path: 'news', component: NewsComponent }
    ])
  ],
  declarations: [NewsComponent],
  exports: [NewsComponent],
  providers: [NewsService]
})
export class NewsModule { }
