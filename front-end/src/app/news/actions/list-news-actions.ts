import { News } from './../model/news';
import { Action } from '@ngrx/store';

export const LISTNEWS = '[NEWS] LIST';
export const LISTNEWS_SUCCESS = '[NEWS] LIST SUCCESS';
export const LISTNEWS_FAILURE = '[NEWS] LIST FAILURE';

export class ListNews {
    readonly type = LISTNEWS;
    constructor() {
    
    }
}

export class ListNewsSuccess implements Action {

    readonly type = LISTNEWS_SUCCESS;
    constructor(public news: News[]) {
            if(this.news==null){
                this.news = [];
            }
    }

}

export class ListNewsFailure implements Action {

    readonly type = LISTNEWS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListNews
    | ListNewsSuccess | ListNewsFailure 