import { NewsService } from './news.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListNewsActions from './actions/list-news-actions';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class NewsEffects {

    constructor(
        private actions$: Actions,
        private newsService: NewsService
    ) { }

    @Effect()
    listNews$: Observable<Action> = this.actions$.pipe(ofType<ListNewsActions.ListNews>
        (ListNewsActions.LISTNEWS),
        map(action=>action)
        ,switchMap(
        payload => this.newsService.getNews().pipe(
            map(results => new ListNewsActions.ListNewsSuccess(results)),
            catchError(err =>
                of({ type: ListNewsActions.LISTNEWS_FAILURE, exception: err })
            )))
        );
}