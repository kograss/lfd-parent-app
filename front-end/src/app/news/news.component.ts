import { Router } from '@angular/router';
import { News } from './model/news';
import { Observable, Subscription } from 'rxjs';
import { State } from './../checkout/reducers/checkout-reducer';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListNewsActions from './actions/list-news-actions';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
import * as ListCartItemsActions from '../cart/actions/list-cart-items-action';
import * as OpenSidebarActions from '../sidebar/actions/open-sidebar.actions';
import { SchoolInfo } from '../common/school-info/model/school-info';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'news',
  templateUrl: 'news.component.html',
  styleUrls: ['news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {
  canteenClosedMessage: string = "";
  news: News[] = [];
  showSpinner: boolean;
  isNewsEmpty = false;

  obsListNews: Observable<News[]>;
  subObsListNews: Subscription;
  obsListNewsError: Observable<any>;
  subObsListNewsError: Subscription;

  schoolInfo: SchoolInfo;
  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  deliveryDate = localStorage.getItem('deliveryDate');

  constructor(private _store: Store<fromRoot.State>, private _route: Router) {
    this.obsListNews = this._store.select(fromRoot.selectListNewsSuccess);
    this.obsListNewsError = this._store.select(fromRoot.selectListNewsFailure);

    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);
  }
  ngOnInit() {
    let isOnInit = true;
    if (!this.deliveryDate || this.deliveryDate == null || this.deliveryDate.trim() == "") {
      this._route.navigate(['canteenorder/dashboard/home']);
      return;
    }
    this.subObsListNews = this.obsListNews.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.length == 0) {
            this.isNewsEmpty = true;
          }
          else {
            this.isNewsEmpty = false;
          }
          this.news = result;
        }
        this.showSpinner = false;
      }
    })

    this.subObsListNewsError = this.obsListNewsError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.schoolInfo = result;
          if (!this.schoolInfo.canteenopen) {
            this.canteenClosedMessage = "School Canteen is currently closed. Please follow up with the school for further information";
          }
        }
      }
    })

    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {

    });

    isOnInit = false;
    this.showSpinner = true;
    this._store.dispatch(new SidebarActions.SetSidebarName('categories'));
    this._store.dispatch(new ListNewsActions.ListNews());
    this._store.dispatch(new ListSchoolInfoActions.ListSchoolInfo);
  }

  showSidebar() {
    this._store.dispatch(new OpenSidebarActions.OpenSidebar(true));
  }

  ngOnDestroy() {
    if (this.subObsListNews)
      this.subObsListNews.unsubscribe();
    if (this.subObsListNewsError)
      this.subObsListNewsError.unsubscribe();
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();
  }
}
