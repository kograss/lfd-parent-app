export class News{
    NewsID : number;
    Description:string;
    DateEntered:string;
    Subject:string;
    Priority:number;
    EnteredBy:string;
    dayid:any;
    Status:string;
    Comment:string;
    schoolid:number;
}