import { AlertService } from './../alert/alertService';
// import { StripeComponent } from './../stripe/stripe.component';
import { LoadingModule } from 'ngx-loading';
import { PaymentService } from './../payment/payment-service';
import { PaymentEffects } from './../payment/take-payment-effects';
import { CheckoutService } from './checkout-service';
import { CheckoutEffects } from './checkout-effects';
import { EffectsModule } from '@ngrx/effects';
import { CartModule } from './../cart/cart.module';
import { CartComponent } from './../cart/cart.component';
import { CheckoutComponent } from './checkout-component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

import { StripeModule } from './../stripe/stripe.module';
import { BlockCopyPasteDirective } from '../common/directives/block-copy-paste.directive';

@NgModule({
    imports: [
            RouterModule,
            CommonModule,
            FormsModule,
            CartModule,
            StripeModule,
            LoadingModule.forRoot({
                fullScreenBackdrop:true
              }),     
            EffectsModule.forFeature([CheckoutEffects,PaymentEffects]),
            RouterModule.forRoot([{ path: 'checkout', component: CheckoutComponent }])
        ],
    declarations: [CheckoutComponent, BlockCopyPasteDirective],   
    exports: [CheckoutComponent,RouterModule],
    providers: [CheckoutService,PaymentService, AlertService]
})
export class CheckoutModule{

}
