import { OrderResult } from './model/order-result';
import { PlaceOrder } from './model/place-order-model';
import { OrderDetails } from './model/oreder-details';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CheckoutService {

    private _url = "PlaceOrder";
    private _eventUrl = "Events/PlaceOrder";
   
    constructor(private _http: HttpClient) {

    }

    placeOrder(orders:OrderDetails[],deliveryDate: string): Observable<any> {
        var obj = new PlaceOrder();
        obj.orderDetails = orders;
        obj.deliveryDate = deliveryDate;

        let body = orders; 
        let orderType = localStorage.getItem('orderType');     
        if(orderType == 'event'){
            return this._http
            .post(this._eventUrl,body)
            ;
        }  
        else{
            return this._http
            .post(this._url,body)
            ;
        }
        
    }

    checkOrder(orders:OrderDetails[], deliveryDate: string): Observable<OrderResult> {
      var obj = new PlaceOrder();
      obj.orderDetails = orders;
      obj.deliveryDate = deliveryDate;

      let body = orders; 
      let orderType = localStorage.getItem('orderType');     
      // if(orderType == 'event'){
      //     return this._http
      //     .post(this._eventUrl,body)
      //     .map(res => res.json());
      // }  
      // else{
          return this._http
          .post<OrderResult>('CheckIfOrderCanbePlaced',body)
          // .map(res => res.json())
          ;
      // }
      
  }

}