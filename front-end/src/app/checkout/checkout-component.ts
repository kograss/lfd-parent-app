import { AlertService } from './../alert/alertService';
// import { StripeComponent } from './../stripe/stripe.component';
import { Parent } from './../user-profile/model/parent';
import { Config } from './../config/config';
import { SchoolInfo } from './../common/school-info/model/school-info';
import { Router } from '@angular/router';
import { OrderResult } from './model/order-result';
import { DeleteCartModel } from './../cart/model/deleteCartModel';
import { CartResult } from './../cart/model/cartResult';
import { OrderDetails } from './model/oreder-details';
import { Store } from '@ngrx/store';
import { from, Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import * as fromRoot from '../reducers';
import * as CheckoutActions from './actions/checkout-actions';
import * as DeleteCartItemActions from '../cart/actions/delete-cart-item-action';
import * as ListCartItemsActions from '../cart/actions/list-cart-items-action';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
import * as ListParentActions from './../user-profile/actions/list-user-action';
import * as PaymentActions from '../payment/actions/take-payment-action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
import { CheckoutService } from './checkout-service';
import { PaymentService } from '../payment/payment-service';


@Component({
  selector: 'checkout',
  styleUrls: ['checkout-component.css'],
  templateUrl: 'checkout-component.html'
})
export class CheckoutComponent implements OnInit, OnDestroy {


  orderItems: OrderDetails[] = [];
  cartItems: CartResult[] = [];
  deliveryDate: string = localStorage.getItem('deliveryDate');
  showDate: Date;
  schoolInfo: SchoolInfo;
  parent: Parent;
  cartTotal: number;
  cartTotalLessThanCredit: boolean;
  isBalancePlusCardPayment: boolean = false;
  showSpinner: boolean;
  payButtonClicked: boolean;
  emptyFeePlan: boolean;
  currency: string;
  retrievingMsg = 'Retrieving your cart, please wait...';
  isListCartFromCheckOut: boolean;
  eventId: number = +localStorage.getItem('eventId');
  orderType: string = localStorage.getItem('orderType');
  eventName: string = localStorage.getItem('eventName');
  dateHeader: string = 'Delivery Date';
  eventType: string = localStorage.getItem('eventType');
  isSetPrice = +localStorage.getItem('isSetPrice');
  eventPrice = +localStorage.getItem('eventPrice');
  whoPaysEventFees = +localStorage.getItem('whoPays');
  paymentIntent: any

  //bag variables: only for canteen event
  bagPrice: number = +localStorage.getItem('bagPrice');
  isBagAvailable: boolean = JSON.parse(localStorage.getItem('isBagAvailable'));
  bagName: string = localStorage.getItem('bagName');

  isPayAtPickup: boolean = false;

  showStripeModal: boolean = false;
  stripePaymentData: any;

  obsListCartItems: Observable<CartResult[]>;
  subObsListCartItems: Subscription;
  obsListCartItemsError: Observable<any>;
  subObsListCartItemsError: Subscription;
  subObsListAlertConfirm: Subscription;
  obsAlertConfirm: Observable<Boolean>;

  obsPlaceOrder: Observable<OrderResult>;
  subObsPlaceOrder: Subscription;
  obsPlaceOrderError: Observable<any>;
  subObsPlaceOrderError: Subscription;

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentError: Observable<any>;
  subObsListParentError: Subscription;

  obsStripePopupState: Observable<boolean>;
  subObsStripePopupState: Subscription;
  customerStripeId: string;
  customerStripeDetails: any;
  translog: any;
  stripeToken: any;
  cardSavmentConfirmation: boolean;

  constructor(private _store: Store<fromRoot.State>, private _route: Router,
    private config: Config,
    // private _stripe: StripeComponent,
    private alertService: AlertService, private checkOutService: CheckoutService, private paymentService: PaymentService) {
    this.obsListCartItems = this._store.select(fromRoot.selectListCartItemsSuccess);
    this.obsPlaceOrder = this._store.select(fromRoot.selectPlaceOrderSuccess);
    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListCartItemsError = this._store.select(fromRoot.selectListCartItemsFailure);
    this.obsListParentError = this._store.select(fromRoot.selectListParentInfoFailure);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);
    this.obsPlaceOrderError = this._store.select(fromRoot.selectPlaceOrderFailure);
    this.obsStripePopupState = this._store.select(fromRoot.selectStripePopupClosed);
    this.obsAlertConfirm = this._store.select(fromRoot.getAlertCancelConfirmStatus)
    this.currency = this.config.getCurrency();
  }

  ngOnInit() {
    let isOnInit = true;
    if (!this.deliveryDate || this.deliveryDate == null || this.deliveryDate.trim() == '') {
      this._route.navigate(['canteenorder/dashboard/home']);
      return;
    }
    else {
      //this.showDate = this.convertStringToDate(this.deliveryDate);
      let values = this.deliveryDate.split('-');
      if (values.length < 3) {
        this._route.navigate(['canteenorder/error/' + 'InvalidDate']);
        return;
      }
      let year = +values[2];
      let month = +values[0] - 1;
      let date = +values[1];
      this.showDate = new Date(year, month, date);
      if (this.orderType == 'event') {
        this.dateHeader = 'Event Date';
        this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(false));
      }

      this._store.dispatch(new ListParentActions.ListParent());

    }

    this._store.dispatch(new SidebarActions.SetSidebarName('categories'));

    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
        if (this.parent.parentPlan != 1 && this.parent.parentPlan != 2) {
          this.emptyFeePlan = true;
        }
        else {
          this.emptyFeePlan = false;
        }
        //console.log(result);
      }
      else {
        this._route.navigate(['canteenorder/error/' + 204]);
      }
    })

    this.subObsListParentError = this.obsListParentError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    /*if (this.cartItems.length == 0) {
      this.showSpinner = true;
      this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate));
    }*/

    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        if(this.schoolInfo.canteenOrderServiceFee == null){
          this.schoolInfo.canteenOrderServiceFee = 0.25;
        }
      }
      else {
        this._route.navigate(['canteenorder/error/' + 204]);
      }
    })

    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsListCartItems = this.obsListCartItems.subscribe(result => {
      this.cartTotal = 0;
      if (result.length > 0) {
        this.cartItems = result;
        if (this.schoolInfo) {
          this.cartItems.forEach(cartItem => {
            Observable.from(cartItem.canteenShopcarts)
              .map(student => student.mealName).distinct().count()
              .subscribe(count => {
                cartItem.paperBags = count;
                cartItem.bagsTotalPrice = count * this.schoolInfo.bagprice;
                cartItem.bagType = "buy";
              })

            if (this.orderType != 'event' || (this.orderType == 'event' && this.isSetPrice != 1)) {
              cartItem.canteenShopcarts.forEach(element => {
                this.cartTotal += element.productPrice;
              })
            }
            else {
              this.cartTotal += this.eventPrice;
            }
            if (this.orderType == 'event' && this.eventType == 'CANTEEN') {
              if (this.checkBagCanteenEventCharges()) {
                this.cartTotal += this.bagPrice;
              }
            }
            else if (this.checkBagCharges()) {
              this.cartTotal += cartItem.bagsTotalPrice;
            }
            if (this.checkServiceFee()) {
              cartItem.serviceCharge = this.schoolInfo.canteenOrderServiceFee;
              let total = this.cartTotal * 100 + this.schoolInfo.canteenOrderServiceFee*100;
              this.cartTotal = total / 100;
            }
          })

          if (this.parent) {
            if (this.cartTotal > this.parent.credit) {
              this.cartTotalLessThanCredit = false;
              this.isBalancePlusCardPayment = true;
            }
            else {
              this.cartTotalLessThanCredit = true;
              this.isBalancePlusCardPayment = false;
            }
          }
          else {
            this._route.navigate(['canteenorder/error/' + 204]);
          }
        }
        else {
          this._route.navigate(['canteenorder/error/' + 204]);
        }
      }
      else {
        this.retrievingMsg = 'Your cart is empty.';
        this.cartItems = [];
      }
      if (this.isListCartFromCheckOut) {
        this.showSpinner = false;
        this.isListCartFromCheckOut = false;
      }
    })

    this.subObsListCartItemsError = this.obsListCartItemsError.subscribe(error => {
      if (!isOnInit)
        this.showSpinner = false;
    })

    this.subObsPlaceOrder = this.obsPlaceOrder.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.isOrderSuccess) {
            this._route.navigate(['canteenorder/dashboard/order-confirmation']);
            this.isListCartFromCheckOut = true;
            this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate, this.eventId));
          }
          else if (result.notAvailableProducts != null) {
            this.payButtonClicked = false;
            if (result.message) {
              if (result.notAvailableProducts.length > 0) {
                result.notAvailableProducts.forEach(element => {
                  this.cartItems.forEach(cartItem => {
                    cartItem.canteenShopcarts.forEach(product => {
                      if (product.productId == element.productId)
                        product.isProductNotAvailable = true
                    })
                  });
                });
              }
              this.alertService.error(result.message);

              //alert(result.message);
            }
          }
          else {
            this.alertService.error(result.message);
            
            // alert(result.message);
          }
        }
        this.showSpinner = false;
      }
    })

    this.subObsPlaceOrderError = this.obsPlaceOrderError.subscribe(error => {
      if (!isOnInit) {
        let errormessage = error._body ? error._body : error.error;
        this.alertService.error(errormessage);
        this.payButtonClicked = false;
        this.showSpinner = false
      }
    })

    this.subObsStripePopupState = this.obsStripePopupState.subscribe(res => {
      if (!isOnInit) {
        if (res) {
          this.payButtonClicked = false;
        }
      }
    })

    this.subObsListAlertConfirm = this.obsAlertConfirm.subscribe(result => {
      if (!isOnInit && this.cardSavmentConfirmation == true) {
        this.takePayment(this.stripePaymentData.amount, this.stripeToken, this.stripePaymentData.isBalancePlusCard, result);
        this.cardSavmentConfirmation = null;
      }
      else if(!isOnInit &&  this.cardSavmentConfirmation == false){
        this.cardSavmentConfirmation = null;
        if(result == true){
          this.getPaymentIntent(); 
        }else{
          this.showStripeModal = true;
        }
        
      }
      
    })
    isOnInit = false;
  }

  

  keyPressAlphaNumeric(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (!this.validateComment(inp)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  validateComment(input) {
    if (/[a-zA-Z0-9 ]/.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  isCartTotalLessThanCredit(): boolean {
    if (this.parent) {
      if (this.cartTotal > this.parent.credit) {
        return false;
      }
      else {
        return true;
      }
    }
  }

  isCanteenEventBalancePaymentPossible(): boolean {
    if (this.orderType == 'event' && this.eventType == 'CANTEEN') {
      return true;
    }
    return false;
  }
  
  isSchoolEventBalancePaymentPossible(): boolean {
    if (this.orderType == 'event' && this.eventType == 'SCHOOL'
      && this.schoolInfo && this.schoolInfo.transfer_weekly_sales_only
    ) {
      return true;
    }
    return false;
  }

  isCanteenBalancePaymentPossible(): boolean {
    if (this.orderType == 'canteen'
    ) {
      return true;
    }
    return false;
  }

  isBalancePlusCardPaymentPossible(): boolean {
    if (this.parent && this.parent.credit > 0 && this.cartTotal > this.parent.credit
      && this.schoolInfo && this.schoolInfo.transfer_weekly_sales_only
    ) {
      return true;
      // this.orderType == 'canteen' &&
    }
    return false;
  }

  checkBagCanteenEventCharges() {
    if (this.isBagAvailable)
      return true;
    return false;
  }

  //
  checkBagCharges() {
    if (this.schoolInfo.chargeforbag == "YES") {
      //      if ((this.orderType == 'event' && this.eventType != 'SCHOOL') || this.orderType != 'event') {
      if (this.orderType != 'event') {
        return true;
      }
    }
    return false;
  }

  checkServiceFee() {
    if (this.eventType == 'SCHOOL') {
      return false;//do not apply service fee for school events
    }
    if (this.whoPaysEventFees && this.whoPaysEventFees == 1 && this.orderType == 'event' && this.eventType == 'CANTEEN') {
      return false;
    }

    if (this.whoPaysEventFees && this.whoPaysEventFees == 0 && this.orderType == 'event' && this.eventType == 'CANTEEN') {
      return true;
    }

    if (this.parent.parentPlan == 1 && this.orderType == 'event' && this.eventType == 'SCHOOL') {
      // if ((this.schoolInfo.fee_model == 2 && this.parent.parentPlan == 1) || (this.orderType == 'event' && this.eventType == 'SCHOOL')) {
      return true;
    }
    if ((this.schoolInfo.fee_model == 2 && this.parent.parentPlan == 1 && this.orderType != 'event')) {
      return true;
    }
    if ((this.schoolInfo.fee_model == 2 && this.parent.parentPlan == 1 && this.orderType == 'event' && this.eventType == 'CANTEEN')) {
      return true;
    }
    return false;
  }

  deleteCartItem(cartId: number, deleteProduct: boolean, status: string) {
    var deleteCartItem = new DeleteCartModel();
    deleteCartItem.cartId = cartId;
    deleteCartItem.deleteProduct = deleteProduct;
    deleteCartItem.deliveryDate = this.deliveryDate;
    deleteCartItem.status = status;
    if (this.orderType == 'event') {
      deleteCartItem.eventId = this.eventId;
    }
    this.showSpinner = true;
    this.isListCartFromCheckOut = true;
    this._store.dispatch(new DeleteCartItemActions.DeleteCartItem(deleteCartItem));
  }

  pay() {
    this.payButtonClicked = true;
    this.showSpinner = true;
    this.isPayAtPickup = false;
    this.generateOrderDetails();
    this._store.dispatch(new CheckoutActions.PlaceOrder(this.orderItems, this.deliveryDate));
  }

  payAtPickup() {
    this.payButtonClicked = true;
    this.showSpinner = true;
    this.isPayAtPickup = true;
    this.generateOrderDetails();
    this._store.dispatch(new CheckoutActions.PlaceOrder(this.orderItems, this.deliveryDate));
  }

  payByBalancePlusCard() {
    let amountToPayFromTheCard = this.cartTotal - this.parent.credit;
    this.stripePay(amountToPayFromTheCard, true);
    this.showSpinner = true;
  }

  checkAndPlaceOrder(amount: number) {
    if (this.orderType != 'event') {
      this.showSpinner = true;
      this.generateOrderDetails(false);
      console.log(this.orderItems)
      console.log("Delivery Date: "+this.deliveryDate)
      this.checkOutService.checkOrder(this.orderItems, this.deliveryDate).subscribe(result => {
        if (result) {
          this.stripePay(amount);
        } else {
          this.alertService.error("Something went wrong. Please connect to the admin");
          console.log(result)
        }
      },
        error => {
          this.showSpinner = false;
          this.alertService.error(error._body);
        },
      );
    }
    else {
      this.stripePay(amount);
    }
  }

  stripePay(amount: number, isBalancePlusCard: boolean = false) {
    this.isPayAtPickup = false;
    this.payButtonClicked = true;
    amount += amount * 0.015 + 0.30;
    amount = Math.trunc((amount + 0.00001) * 100) / 100;
    amount = amount * 100;
    this.generateOrderDetails();
    if (this.parent) {
      // this._stripe.openCheckout("Canteen", amount, this.parent.email, (token: any) =>
      //   this.takePayment(amount, token));
      this.stripePaymentData = {
        name: "Canteen",
        amount,
        email: this.parent.email,
        isBalancePlusCard
      };

      this.paymentService.GetCustomerStripeDetails().subscribe(result => {
        this.customerStripeDetails = result
        this.showSpinner = false;
        if(this.customerStripeDetails != null){
          this.cardSavmentConfirmation = false;
          this.alertService.confirm("Would you like to use the saved card XXXX XXXX XXXX " + this.customerStripeDetails.cardLast4
          + " for this payment?")
          this.payButtonClicked = false;
        }else{
          this.showStripeModal = true;
        }
        
      });
    }
    else {
      this._route.navigate(['canteenorder/error/' + 204]);
    }
  }

  payByBalanceSchoolEvent(amount: number) {
    this.generateOrderDetails();
    this.showSpinner = true;
    this._store.dispatch(new PaymentActions.TakePayment(this.orderItems, amount, '', false, false));
  }

  stripeCallback(token) {
    this.showStripeModal = false;
    this.stripeToken = token
    this.cardSavmentConfirmation = true
    if(this.customerStripeDetails != null)
      this.alertService.confirm("Would to like to replace your saved card details with these card details?");
    else
      this.alertService.confirm("Would you like to save this card for future payment?");
  
  }

  takePayment(amount: number, token: any, isBalancePlusCard: boolean, saveCard: Boolean) {
    this.showSpinner = true;
    this._store.dispatch(new PaymentActions.TakePayment(this.orderItems, amount, token, isBalancePlusCard, saveCard.valueOf()));
  }

  getPaymentIntent(){
    this.showSpinner = true;
    this.paymentService.GetPaymentIntent(this.orderItems, this.stripePaymentData.amount, 
      this.stripePaymentData.isBalancePlusCard).subscribe(async result => {   
        this.translog = result[Object.keys(result)[0]];
        this.paymentIntent = JSON.parse(Object.keys(result)[0])
        this.translog.PGTransId = this.paymentIntent.id
        this.paymentService.GetCustomerStripeDetails().subscribe(result => {
          this.customerStripeDetails = result
          this.paymentService.placeOrderAfterPayment(this.orderItems, this.stripePaymentData.amount, this.stripePaymentData.isBalancePlusCard, this.translog).subscribe(result => {
            let orderResult: any = result;
            if (orderResult.isOrderSuccess) {
              localStorage.setItem('orderResult', JSON.stringify(result));
              this._route.navigate(['canteenorder/dashboard/order-confirmation']);
              this.showSpinner = false;
              this.isListCartFromCheckOut = true;
              this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate, this.eventId));
            }
            else {
              this.alertService.error(orderResult.message);
            }
          });
          
        });
        
      })
  }

  generateOrderDetails(isCredit = true) {
    this.orderItems = [];
    for (var item of this.cartItems) {
      var orderItem = new OrderDetails();
      orderItem.studentId = item.studentId;
      orderItem.comment = item.comment;
      orderItem.reason = item.reason;
      orderItem.chargeForBag = item.bagsTotalPrice;
      orderItem.totalServiceCharge = item.serviceCharge;
      orderItem.deliveryDate = this.deliveryDate;
      orderItem.bagType = item.bagType;
      orderItem.isPayAtPickup = this.isPayAtPickup;
      orderItem.isCreditPayment = isCredit;

      if (item.bagType == 'buy') {
        orderItem.bagPrice = item.bagsTotalPrice;
      }
      else {
        orderItem.bagPrice = 0;
      }
      if (this.orderType == 'event') {
        orderItem.eventId = this.eventId;
      }
      this.orderItems.push(orderItem);
    }
  }

  convertStringToDate(dateString: string) {
    let values = dateString.split('-');
    if (values.length < 2) {
      this._route.navigate(['canteenorder/error/' + 'InvalidDate']);
      //this._route.navigate(['canteenorder/error', { status: 'InvalidDate' }]);
      return;
    }
    let year = +values[2];
    let month = +values[0] - 1;
    let date = +values[1];
    return new Date(year, month, date);
  }

  changeBagType(bagType, studentCart) {
    //this.generateOrderDetails();
    this.cartItems.forEach(cartItem => {

      if (cartItem.studentId == studentCart.studentId) {

        if (bagType == 'buy') {
          //add bag price 
          this.cartTotal = this.cartTotal + cartItem.bagsTotalPrice;
          cartItem.bagType = "buy";
          //this.orderItems.find(e => e.studentId == studentCart.studentId).bagPrice = cartItem.bagsTotalPrice;
        }
        else if (bagType == 'reuseable') {
          //remove bag price
          this.cartTotal = this.cartTotal - cartItem.bagsTotalPrice;
          cartItem.bagType = "reuseable";
          //this.orderItems.find(e => e.studentId == studentCart.studentId).bagPrice = 0;
          //orderItem.bagPrice = 0;
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.subObsListCartItems)
      this.subObsListCartItems.unsubscribe();
    if (this.subObsPlaceOrder)
      this.subObsPlaceOrder.unsubscribe();
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListCartItemsError)
      this.subObsListCartItemsError.unsubscribe();
    if (this.subObsListParentError)
      this.subObsListParentError.unsubscribe();
    if (this.subObsPlaceOrderError)
      this.subObsPlaceOrderError.unsubscribe();
    if (this.subObsStripePopupState)
      this.subObsStripePopupState.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
  }
}