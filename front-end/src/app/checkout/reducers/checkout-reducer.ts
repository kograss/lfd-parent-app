import { OrderResult } from './../model/order-result';
import * as CheckoutActions from './../actions/checkout-actions';
export interface State {
    result: OrderResult,
    err: any
}

const initialState: State = {
    result:new OrderResult(),
    err: {}
};

export function reducer(state = initialState, action: CheckoutActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case CheckoutActions.PLACEORDER_SUCCESS: {
            return {
                ...state,
                result: action.paymentResult
            }
        }
        case CheckoutActions.PLACEORDER_FAILURE: {
            return {
                ...state,
                err: action.err 
            }
        }

        case CheckoutActions.CLEARORDERRESULT: {
            return {
                ...state,
                result: new OrderResult()
            }
        }

        default: {
            return state;
        }

    }

}
