import { OrderResult } from './../model/order-result';
import { OrderDetails } from './../model/oreder-details';
import { Action } from '@ngrx/store';


export const PLACEORDER = 'PLACEORDER ACTION';
export const PLACEORDER_SUCCESS = 'PLACEORDER SUCCESS';
export const PLACEORDER_FAILURE = 'PLACEORDER FAILURE';
export const CLEARORDERRESULT = 'CLEAR ORDER RESULT';



export class PlaceOrder {
    deliveryDate:string;
    orderDetails:OrderDetails[];
    readonly type = PLACEORDER;
    constructor(orderDetails: OrderDetails[],deliveryDate) {
        this.deliveryDate = deliveryDate;
        this.orderDetails = orderDetails;
    }
}

export class PlaceOrderSuccess implements Action {
    readonly type = PLACEORDER_SUCCESS;
    constructor(public paymentResult) {
    }
}

export class PlaceOrderFailure implements Action {
    readonly type = PLACEORDER_FAILURE;
    constructor(public err: any) {
    }

}

export class ClearOrderResult implements Action {
    readonly type = CLEARORDERRESULT;
    constructor() {
    }

}


export type All = PlaceOrder
    | PlaceOrderSuccess | PlaceOrderFailure | ClearOrderResult
