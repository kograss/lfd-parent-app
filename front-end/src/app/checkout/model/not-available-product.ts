export class NotAvailableProduct{
    productId:number;
    productName:string;
    mealCode:string;
    notAvailableDays:string;
}