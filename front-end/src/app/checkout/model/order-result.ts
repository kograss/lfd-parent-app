import { NotAvailableProduct } from './not-available-product';
export class OrderResult {
    orderIds: number[]=[];
    message: string;
    isOrderSuccess:boolean=false;
    notAvailableProducts:NotAvailableProduct[]=[];
}