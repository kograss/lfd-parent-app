export class OrderDetails {
    studentId: number;
    parentId: number;
    schoolId: number;
    totalServiceCharge: number;
    chargeForBag: number;
    comment: string;
    reason: string;
    deliveryDate: string;
    eventId: number;
    bagType: string;
    bagPrice: number;
    isPayAtPickup: boolean;
    isCreditPayment: boolean = true;
}