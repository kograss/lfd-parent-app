import { CheckoutService } from './checkout-service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as CheckoutActions from './actions/checkout-actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class CheckoutEffects {

    constructor(
        private actions$: Actions,
        private checkoutService:CheckoutService
    ) { }

    @Effect()
    placeOrder$: Observable<Action> = this.actions$.pipe(ofType<CheckoutActions.PlaceOrder>
        (CheckoutActions.PLACEORDER),
        switchMap(
        payload => this.checkoutService.placeOrder(payload.orderDetails,payload.deliveryDate)
            .pipe(map(results => new CheckoutActions.PlaceOrderSuccess(results))
            ,catchError(err =>
                of(new CheckoutActions.PlaceOrderFailure(err))
            ))
        ));  
}