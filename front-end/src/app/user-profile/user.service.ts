import { Observable} from 'rxjs';
import { Parent } from './model/parent';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserService {
    private _editProfileUrl = 'Parent/UpdateProfile';
    private _newProfileUrl = 'Parent/UpdateProfile';
    private _getParentUrl = 'Parent/GetParentInfo/';
    constructor(private _http: HttpClient) { }
    
    getParent():Observable<any> {
        return this._http.get(this._getParentUrl);
      }
    
    addParent(parent: Parent):Observable<any> {
        if (parent.parentId) {
            let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
            let options = { headers: headers };
            let body = JSON.stringify(parent);
            return this._http.put(this._editProfileUrl, body, options);
        }
        else {
            let headers = new HttpHeaders(
                {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                });
            let options = { headers: headers };
            let body = JSON.stringify(parent);
            return this._http.post(this._newProfileUrl, body, options);
        }
    }
}