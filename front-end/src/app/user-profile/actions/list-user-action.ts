import { Parent } from './../model/parent';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const LISTPARENT = 'LIST PARENT';
export const LISTPARENT_SUCCESS = 'LIST PARENT SUCCESS';
export const LISTPARENT_FAILURE = 'LIST PARENT FAILURE';


export class ListParent {
    readonly type = LISTPARENT;
    constructor() {
    }
}

export class ListParentSuccess implements Action {
    readonly type = LISTPARENT_SUCCESS;
    constructor(public parent: Parent) {
    }
}

export class ListParentFailure implements Action {
    readonly type = LISTPARENT_FAILURE;
    constructor(public err: any) {
    }
}

export type All = ListParent
    | ListParentSuccess | ListParentFailure
