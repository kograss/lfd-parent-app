import { Parent } from './../model/parent';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const ADDPARENT = 'ADD PARENT';
export const ADDPARENT_SUCCESS = 'ADD PARENT SUCCESS';
export const ADDPARENT_FAILURE = 'ADD PARENT FAILURE';


export class AddParent {
    readonly type = ADDPARENT;
    constructor(public payload: Parent) {
    }
}

export class AddParentSuccess implements Action {
    readonly type = ADDPARENT_SUCCESS;
    constructor(public parentAdded: Parent) {
    }
}

export class AddParentFailure implements Action {
    readonly type = ADDPARENT_FAILURE;
    constructor(public err: any) {
    }
}

export type All = AddParent
    | AddParentSuccess | AddParentFailure
