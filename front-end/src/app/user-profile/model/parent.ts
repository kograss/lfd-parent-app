export class Parent{
    parentId:number;
    firstName:string;
    lastName:string;
    mobile:string;
    email:string;
    credit:number;
    parentPlan:number;
    password:string;
    isStudentClassSetupRequired: boolean;
}