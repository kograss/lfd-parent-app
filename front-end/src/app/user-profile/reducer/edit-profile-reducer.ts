import * as AddParentActions from '../actions/edit-profile-action';
import { Parent } from '../model/parent';

export interface State {
    result: Parent,
    err: any
}

const initialState: State = {
    result: new Parent(),
    err: {}
};

export function reducer(state = initialState, action: AddParentActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case AddParentActions.ADDPARENT_SUCCESS: {
            return {
                ...state,
                result: action.parentAdded
            }
        }
        case AddParentActions.ADDPARENT_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }
    }
}
