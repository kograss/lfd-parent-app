import * as ListParentActions from '../actions/list-user-action';
import { Parent } from '../model/parent';

export interface State {
    result: Parent,
    err: any
}

const initialState: State = {
    result: new Parent(),
    err: {}
};

export function reducer(state = initialState, action: ListParentActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case ListParentActions.LISTPARENT_SUCCESS: {
            return {
                ...state,
                result: action.parent
            }
        }
        case ListParentActions.LISTPARENT_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }
    }
}
