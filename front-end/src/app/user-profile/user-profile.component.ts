import { AlertService } from './../alert/alertService';
import { AlertComponent } from './../alert/alert.component';
import {  Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Parent } from './model/parent';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as AddUserActions from './actions/edit-profile-action';
import * as ListUserActions from './actions/list-user-action';


declare var $: any;

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  
  message : string;

  obsAddUser: Observable<Parent>;
  subsObsAddUser: Subscription;
  obsAddUserFailure: Observable<any>;
  subObsAddUserFailure: Subscription;

  obsListUser: Observable<Parent>;
  subsObsListUser: Subscription;
  obsListUserFailure: Observable<any>;
  subObsListUserFailure: Subscription;

  passwordType: string = 'password';
  confirmPassword: string;
  saveParentClicked: boolean;
  showSpinner: boolean;
  isPasswordNotValid: boolean;

  constructor(private _store: Store<fromRoot.State>,
    private _router: Router, public parent: Parent, private alertService : AlertService) {
    this.obsAddUser = this._store.select(fromRoot.selectEditParentSuccess);
    this.obsListUser = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsAddUserFailure = this._store.select(fromRoot.selectEditParentFailure);
    this.obsListUserFailure = this._store.select(fromRoot.selectListParentInfoFailure);
  }

  ngOnInit() {
    let isOnInit = true;

    this.subsObsListUser = this.obsListUser.subscribe(result => {
      if (result ) {
        this.parent = result;
        if (this.saveParentClicked && !isOnInit) {
          this.showSpinner = false;
          this.message = 'Profile updated successfully..!';
          this.alertService.success(this.message);
          this.saveParentClicked = false;
          this.confirmPassword = "";
        }
      }
      else if (this.saveParentClicked) {
        this.showSpinner = false;                 
        this.message = 'Failed to update user profile.';
        this.alertService.error(this.message);
        this.saveParentClicked = false;
        this.confirmPassword = "";
      }
    })

    this.subObsListUserFailure = this.obsListUserFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsAddUserFailure = this.obsAddUserFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  saveParent() {
    
    this.saveParentClicked = true;
    if (this.parent.password == this.confirmPassword) {
      this.showSpinner = true;
      this._store.dispatch(new AddUserActions.AddParent(this.parent));
    }
    else {
      this.isPasswordNotValid = true;
    }
  }

  cancel(){
    this._router.navigate(['canteenorder/dashboard/home']);
    // this._router.navigate(['canteenorder/dashboard/home']);
  }

  showHidePassword() {
    if (this.passwordType == "password")
      this.passwordType = "text";
    else
      this.passwordType = "password";
  }

  ngOnDestroy() {
    if (this.subsObsAddUser)
      this.subsObsAddUser.unsubscribe();
    if (this.subsObsListUser)
      this.subsObsListUser.unsubscribe();
    if (this.subObsAddUserFailure)
      this.subObsAddUserFailure.unsubscribe();
    if (this.subObsListUserFailure)
      this.subObsListUserFailure.unsubscribe();
  }
}
