

import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as AddParentActions from './actions/edit-profile-action';
import * as ListParentActions from './actions/list-user-action';

import { UserService } from './user.service';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class UserProfileEffects {

    constructor(
        private actions$: Actions,
        private parentService: UserService
    ) { }

    @Effect()
    listParent$: Observable<Action> = this.actions$.pipe(ofType<ListParentActions.ListParent>(ListParentActions.LISTPARENT)
        ,map(action => action)
        ,switchMap(
        payload => this.parentService.getParent().pipe(
            map(results => new ListParentActions.ListParentSuccess(results))
            ,catchError(err =>
                of({ type: ListParentActions.LISTPARENT_FAILURE, exception: err })
            )))
        );

    @Effect()
    addParent$: Observable<Action> = this.actions$.pipe(ofType<AddParentActions.AddParent>(AddParentActions.ADDPARENT)
        ,map(action => action.payload)
        ,switchMap(
        payload => this.parentService.addParent(payload).pipe(
            map(results => new ListParentActions.ListParentSuccess(results))
            ,catchError(err =>
                of({ type: AddParentActions.ADDPARENT_FAILURE, exception: err })
            )))
        );

}