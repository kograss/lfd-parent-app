import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserProfileEffects } from './user-profile-effects';
import { EffectsModule } from '@ngrx/effects';
import { UserService } from './user.service';
import { Parent } from './model/parent';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        CommonMenuModule,
        LoadingModule.forRoot({
            fullScreenBackdrop:true
          }),
        EffectsModule.forFeature([UserProfileEffects])        
],
declarations: [ UserProfileComponent],
exports: [ UserProfileComponent ],
providers: [ UserService, AlertService, Parent ]
})  
export class UserProfileModule{

}
