import { LoadingModule } from 'ngx-loading';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories.component';
import { CategoriesRoutingModule } from './categories-routing.module';
import {EffectsModule} from '@ngrx/effects';
import {CategoryEffects} from './categories-effects';
import {CategoriesService} from './categories.service';


@NgModule({
  imports: [CommonModule, CategoriesRoutingModule,
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    EffectsModule.forRoot([CategoryEffects])
  ],
  declarations: [CategoriesComponent],
  exports: [CategoriesComponent],
  providers:[CategoriesService]
})
export class CategoriesModule { }
