import * as CategoriesActions from './categories-actions'
import { Meal } from './model/meal';

export interface State {    
   
    result: Meal[],
    err: any
}

const initialState: State = {    
   
    result:[],
    err: {}
};

export function reducer(state = initialState, action: CategoriesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case CategoriesActions.LISTMEALS_SUCCESS: {
            return {
                ...state,
                result: action.payload
            }
        }
        case CategoriesActions.LISTMEALS_FAILURE: {
            return {
                ...state,
                err: action.payload
            }
        }
          

        default: {
            return state;
        }

    }

}
