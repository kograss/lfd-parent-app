import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { SchoolInfo } from './../common/school-info/model/school-info';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as CategoryActions from './categories-actions';
import * as ListProductActions from '../products/actions/list-products-actions';
import { Observable, Subscription } from 'rxjs';
import { Meal } from './model/meal';
/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'categories',
  templateUrl: 'categories.component.html',
  styleUrls: ['categories.component.css']
})
export class CategoriesComponent implements OnInit, OnDestroy {
  @Output()
  hideNav: EventEmitter<any> = new EventEmitter();

  obsMeals: Observable<Meal[]>;
  subObsMeals: Subscription;

  obsMealsError: Observable<any>;
  subObsMealsError: Subscription;

  meals: Meal[] = [];
  deliveryDate = localStorage.getItem('deliveryDate');
  showSpinner: boolean;


  constructor(protected _store: Store<fromRoot.State>, protected _route: Router) {
    this.obsMeals = this._store.select(fromRoot.selectSearchProductCategorySuccess);
    this.obsMealsError = this._store.select(fromRoot.selectSearchProductCategoryFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    if(!this.deliveryDate || this.deliveryDate == null || this.deliveryDate.trim() == ""){
      this._route.navigate(['canteenorder/dashboard/home']);
      return;      
    }
    if (this.meals.length == 0) {
      this.showSpinner = true;
      this._store.dispatch(new CategoryActions.ListMeals(new CategoryActions.ListMealsSearchTerms(this.deliveryDate)));
    }

    this.subObsMeals = this.obsMeals.subscribe(
      meals => {
        if (!isOnInit) {
          this.meals = meals;
          this.showSpinner = false;
        }
      }
    );

    this.subObsMealsError = this.obsMealsError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  fetchProducts(categoryId: number, mealCode: string, mealName: string, categoryName: string) {
    //console.log("CategoryId " + categoryId + " name: " + categoryName + " mealName: " + mealName);
   // var deliveryDate = localStorage.getItem('deliveryDate');
    this._store.dispatch(new ListProductActions.ListProducts(new ListProductActions.ListProductsSearchTerms(mealCode, categoryId, this.deliveryDate)));
    this.hideNav.emit();
    this._route.navigate(['canteenorder/dashboard/products', { mealCode: mealCode, categoryId: categoryId, mealName: mealName, categoryName: categoryName }]);
  }

  showHideSubMenu(meal: Meal) {
    meal.showHideMeal = !meal.showHideMeal;
  }

  ngOnDestroy() {
    if (this.subObsMeals)
      this.subObsMeals.unsubscribe();
    if (this.subObsMealsError)
      this.subObsMealsError.unsubscribe()
  }
}

