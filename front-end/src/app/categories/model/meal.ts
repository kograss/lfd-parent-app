import { Category } from "./category";

export class Meal {
    name: string;
    categories: Category[];
    mealCode: string;
    showHideMeal:boolean = true; // to show/hide in side menu
}