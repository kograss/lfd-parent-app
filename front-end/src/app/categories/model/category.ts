export class Category {
    categoryID: number;
    catdescription: string;
    daily_hour:number;
    daily_min:number;  
    cut_days_before:number;  
}