

import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import { CategoriesService } from './categories.service';

import * as CategoriesActions from './categories-actions';
import { catchError, exhaustMap, map, switchMap } from 'rxjs/operators';


@Injectable()
export class CategoryEffects {

  @Effect()
  listCategories$: Observable<Action> = this.actions$.pipe(ofType<CategoriesActions.ListMeals>(CategoriesActions.LISTMEALS),  
    switchMap(
    payload => this.categoriesService.getMealCategories(payload.payload.date).pipe(
      map(results => new CategoriesActions.ListMealsSuccess(results)),
      catchError(err =>
        of(new CategoriesActions.ListMealsFailure(err))
      )
    ))
    );









  constructor(
    private actions$: Actions,
    private categoriesService: CategoriesService
  ) { }
}