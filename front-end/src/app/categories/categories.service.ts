import { Injectable } from '@angular/core';
import { Meal } from './model/meal';

import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CategoriesService {

    private _url = "Meal/{deliveryDate}";   
    

    

    constructor(private _http: HttpClient) {

    }





    getMealCategories(deliveryDate:string) {

        var mealCategories = this._http
        .get(this._url
            .replace("{deliveryDate}", deliveryDate));
        return mealCategories;
         

    }

  

}