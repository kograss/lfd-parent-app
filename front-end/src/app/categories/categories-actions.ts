import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Meal } from './model/meal';



export const LISTMEALS = '[MEAL] LIST';

export const LISTMEALS_SUCCESS = '[MEAL] LIST SUCCESS';

export const LISTMEALS_FAILURE = '[MEAL] LIST FAILURE';




export class ListMealsSearchTerms {
    date: string

    constructor(date) {
        this.date = date;
    }

}


export class ListMeals implements Action {

    readonly type = LISTMEALS;

    constructor(public payload: ListMealsSearchTerms) {

    }

}


export class ListMeals1 implements Action {

    readonly type = LISTMEALS;
    constructor(payload: any) {

    }

}


export class ListMealsSuccess implements Action {

    readonly type = LISTMEALS_SUCCESS;
    constructor(public payload) {
        if(payload == null){
            this.payload = [];
        }
        ////console.log("MEALList is "+JSON.stringify(MEALList));
    }

}

export class ListMealsFailure implements Action {

    readonly type = LISTMEALS_FAILURE;
    constructor(public payload: any) {
        //console.log("Exception is " + JSON.stringify(payload));
    }

}






export type All = ListMeals
    | ListMealsSuccess | ListMealsFailure | ListMeals1