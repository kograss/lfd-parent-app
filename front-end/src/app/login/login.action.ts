import { Action } from '@ngrx/store';

export const LOGINACTION = '[LOGIN] TOKEN';
export const LOGINACTION_SUCCESS = '[LOGIN] TOKEN SUCCESS';
export const LOGINACTION_FAILURE = '[LOGIN] TOKEN FAILURE';
export const CLEARLOGINRESULT = 'CLEAR LOGIN RESULT';

export class LoginActionCredentials {
    username: string;
    password: string;
    constructor(username, password) {
        this.username = username;
        this.password = password;
    }
}

export class LoginAction {
    readonly type = LOGINACTION
    constructor(public payload: LoginActionCredentials) { }
}

export class LoginActionSuccess implements Action {
    readonly type = LOGINACTION_SUCCESS;
    constructor(public token: any) { }
}

export class LoginActionFailure implements Action {
    readonly type = LOGINACTION_FAILURE;
    constructor(public err: any) {
        //console.log("Exception is " + JSON.stringify(err));
    }
}

export class ClearLoginResult implements Action {
    readonly type = CLEARLOGINRESULT;
    constructor() { }
}

export type All = LoginAction
    | LoginActionSuccess | LoginActionFailure | ClearLoginResult