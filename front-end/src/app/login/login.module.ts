import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { LoginService } from './login.service';
import { LoginComponent } from './login.component';
import { LoginEffects } from './login.effects';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ng2-cookies';
import { ForgotPasswordModule } from '../forgot-password/forgot-password.module';
import { LoginRoutingModule } from './login-routing.module';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    // LoginRoutingModule,
    RouterModule,
    ForgotPasswordModule,
    EffectsModule.forFeature([LoginEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    })
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  providers: [LoginService, CookieService]
})
export class LoginModule {

}
