
import * as LoginActions from './login.action'

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
   
    result: {},
    err: {}
};

export function reducer(state = initialState, action: LoginActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case LoginActions.LOGINACTION_SUCCESS: {
            return {
                ...state,
                result: action.token
            }
        }
        case LoginActions.LOGINACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }   
        
        case LoginActions.CLEARLOGINRESULT: {
            return {
                ...state,
                result: {}
            }
        }            


        default: {
            return state;
        }

    }

}
