import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class LoginService {
    private _loginUrl = "token";
    private _verifyUserUrl = "Parent/VerifyParentEmail";
    private _sendEmailVerificationLinkURL = "Parent/SendEmailVerificationLink";

    constructor(private _http: HttpClient) {

    }

    login(email: string, password: string): Observable<any> {
        //console.log(email + ',' + password + "cred");
        var userObj = "username=" + encodeURIComponent(email) +
            "&password=" + encodeURIComponent(password) +
            "&grant_type=password"

        var options:any = {};
        console.log("Login")
        options.headers = new HttpHeaders();
        options.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http.post(this._loginUrl, userObj, options);
    }

    verifyEmail(data): Observable<any> {
        return this._http.post(this._verifyUserUrl, {data: data});
      }

      sendEmailVerificationLink(data): Observable<any>{
          return this._http.post(this._sendEmailVerificationLinkURL, {data: data});
      }

}



