import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as LoginActions from './login.action';
import { Router, ActivatedRoute } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { LoginService } from './login.service';
import { AlertService } from '../alert/alertService';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ResetPassword } from '../forgot-password/reset-password.action';
declare var $: any;
declare var navigator: any;

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

//TODO:force SSL only, XSS checking

export class LoginComponent implements OnInit {
  @HostListener('window:popstate') backbuttonpressed() {
    if (navigator.app) {
      navigator.app.exitApp();
    } else if (navigator.device) {
      navigator.device.exitApp();
    } else {
      window.close();
    }
  }

  isEmailVerificationRequired: boolean = false;
  showVerficationEmailPopup: boolean = false;
  isEmailVerificationIncorrect: boolean = false;
  emailToverify: string;

  obsLoginToken: Observable<any>;
  subsObsLoginToken: Subscription;
  obsLoginFailure: Observable<any>;
  subsObsLoginFailure: Subscription;

  email: string;
  pwd: string;
  rememberMe: boolean;

  showLogin: boolean;
  loginFailed: boolean;
  loginErrorMsg: string;
  showSpinner: boolean;
  isLoginModalOpen: boolean = false;
  isForgotActive: boolean = false;

  emailVerificationData: any;
  emailVerificationDone: boolean;
  emailVerificationMessage: string;

  loginForm: FormGroup;
  isShow: boolean = true;
  type = 'password';

  constructor(private _store: Store<fromRoot.State>, private _router: Router, private alertService: AlertService,
    private route: ActivatedRoute, private loginService: LoginService, private fb: FormBuilder) {
    this.obsLoginToken = this._store.select(fromRoot.selectLoginSuccess);
    this.obsLoginFailure = this._store.select(fromRoot.selectLoginFailure);

    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      rememberMe: true,
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.emailVerificationData = params;
      if (this.emailVerificationData.data) {

        this.loginService.verifyEmail(this.emailVerificationData.data).subscribe(res => {
          if (res) {
            this.emailVerificationDone = true;
            this.emailVerificationMessage = res;
          }
        },
          err => {

          });
      }

    });

    this.subsObsLoginToken = this.obsLoginToken.subscribe(result => {
      this.showSpinner = false;
      if (result) {
        if (result.access_token) {
          let msg = result;
          if (msg.access_token != "null")
            if (msg.access_token.trim != "") {
              localStorage.setItem("s24tkn", msg.access_token);
              this._store.dispatch(new LoginActions.ClearLoginResult());
              this._router.navigate(['canteenorder/dashboard/home']);
            }
        }
      }
    })

    this.subsObsLoginFailure = this.obsLoginFailure.subscribe(res => {
      this.showSpinner = false;
      if (res) {
        if (res.status == 400) {
          this.loginErrorMsg = res.error.error; //'Invalid Username Or Password';
          this.loginFailed = true;

          if (res.error.error == "Your email is not verified. Click on send verification link button to get verification email.") {
            this.isEmailVerificationRequired = true;
          }
        }
        else if (res.status == 500 || res.status == 0) {
          this.loginErrorMsg = 'Server error please try again';
        }

      }
    })

    const tken = localStorage.getItem("s24tkn");
    if (tken) {
      if (tken != "null") {
        if (tken.trim() != "") {
          this._router.navigate(['canteenorder/dashboard/home']);
        }
      }

    }
  }

  showHideConfirmPassword() {
    if (this.loginForm.controls.password.value != null) {
      this.isShow = !this.isShow;
      if (!this.isShow) {
        if (this.type == 'password') {
          this.type = "text";
        }
      }
      else {
        if (this.type == 'text')
          this.type = "password";
      }
    }
  }

  login() {
    if (this.loginForm.invalid) {
      <any>Object.values(this.loginForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }

    this.showSpinner = true;
    this.email = this.loginForm.controls.email.value
    this.pwd = this.loginForm.controls.password.value
    this.rememberMe = this.loginForm.controls.rememberMe.value
    this._store.dispatch(new LoginActions.LoginAction(
      new LoginActions.LoginActionCredentials(this.loginForm.controls.email.value, this.loginForm.controls.password.value)));
  }

  loginPopUp() {
    this.showLogin = true;
  }

  hideLogin() {
    this.showLogin = false;
  }

  showLoginModal() {
    this.isLoginModalOpen = true;
  }

  showForgotPassword() {
    //this.isForgotActive = true;
    this._router.navigate(['canteenorder/forgot-password']);
  }

  hideForgotPassword() {
    this.isForgotActive = false;
  }


  sendVerificationEmail() {
    this.showVerficationEmailPopup = true;
  }

  closeModal() {
    this.showVerficationEmailPopup = false;
  }

  sendVerificationEmailLink() {
    if (this.validateEmail(this.emailToverify)) {
      //call service
      this.showSpinner = true;
      this.loginService.sendEmailVerificationLink(this.emailToverify).subscribe(res => {
        if (res) {
          this.emailVerificationDone = true;
          this.showSpinner = false;
          //close modal
          this.closeModal();
          this.alertService.success(res, true);
        }
      },
        err => {
          this.showSpinner = false;
          this.alertService.success("Something went wrong! Please try again.", true);
        });

      //handle result and error



    }
    else {
      this.isEmailVerificationIncorrect = true;
    }
  }

  validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return (true)
    }

    return (false)
  }

  ngOnDestroy() {
    if (this.subsObsLoginToken)
      this.subsObsLoginToken.unsubscribe();
    if (this.subsObsLoginFailure)
      this.subsObsLoginFailure.unsubscribe();
  }
}


