import { LoginService } from './login.service';
import { Action } from '@ngrx/store';
import { Observable, of, Subscription } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import * as LoginActions from './login.action';
import { catchError, map, switchMap } from 'rxjs/operators';
@Injectable()
export class LoginEffects {

  constructor(
    private actions$: Actions,
    private loginService: LoginService
  ) { }

  @Effect()
  loginAction$: Observable<Action> = this.actions$.pipe(ofType<LoginActions.LoginAction>(LoginActions.LOGINACTION)
    , map(action => action.payload)
    , switchMap(
      payload => this.loginService.login(payload.username, payload.password).pipe(
        map(results => {
          return new LoginActions.LoginActionSuccess(results)

        }),
        catchError(err => of(new LoginActions.LoginActionFailure(err)))
      )));
}
