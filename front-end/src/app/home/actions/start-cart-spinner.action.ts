
export const STARTCARTSPINNER = 'START CART SPINNER';
export const STOPCARTSPINNER = 'STOP CART SPINNER';

export class StartCartSpinner {
    readonly type = STARTCARTSPINNER;
    constructor() {
    }
}

export class StopCartSpinner {
    readonly type = STOPCARTSPINNER;
    constructor() {
    }
}


export type All = StartCartSpinner | StopCartSpinner
