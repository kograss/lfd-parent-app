import { Holidays } from './../model/holidays';
import { Action } from '@ngrx/store';

export const LISTHOLIDAYS = '[HOLIDAYS] LIST';
export const LISTHOLIDAYS_SUCCESS = '[HOLIDAYS] LIST SUCCESS';
export const LISTHOLIDAYS_FAILURE = '[HOLIDAYS] LIST FAILURE';

export class ListHolidays {
    readonly type = LISTHOLIDAYS;
    constructor() {
    }
}

export class ListHolidaysSuccess implements Action {

    readonly type = LISTHOLIDAYS_SUCCESS;
    constructor(public holidays: Holidays) {

    }

}

export class ListHolidaysFailure implements Action {

    readonly type = LISTHOLIDAYS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListHolidays
    | ListHolidaysSuccess | ListHolidaysFailure 