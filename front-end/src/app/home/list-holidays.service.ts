import { Holidays } from './model/holidays';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ListHolidaysService {

    private _url = "Holiday/GetSchoolHolidays";
    
    constructor(private _http: HttpClient) {
    }

    getHolidays(): Observable<any> {
        return this._http
            .get(this._url)
            ;
    }
}

