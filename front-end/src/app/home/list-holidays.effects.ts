import { ListHolidaysService } from './list-holidays.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListHolidaysActions from './actions/list-holidays.action';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class ListHolidaysEffects {

    constructor(
        private actions$: Actions,
        private listHolidaysService: ListHolidaysService
    ) { }

    @Effect()
    listHolidays$: Observable<Action> = this.actions$.pipe(ofType<ListHolidaysActions.ListHolidays>
        (ListHolidaysActions.LISTHOLIDAYS)
        ,map(action=>action)
        ,switchMap(
        payload => this.listHolidaysService.getHolidays().pipe(
            map(results => new ListHolidaysActions.ListHolidaysSuccess(results))
            ,catchError(err =>
                of({ type: ListHolidaysActions.LISTHOLIDAYS_FAILURE, exception: err })
            )))
        );
}