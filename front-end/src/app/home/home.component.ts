import { SchoolInfo } from './../common/school-info/model/school-info';
import { Parent } from './../user-profile/model/parent';
import { Holidays } from './model/holidays';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as SidebarActions from '../sidebar/actions/sidebar.action'
import * as ListHolidaysActions from './actions/list-holidays.action';
import * as ListCartItemsActions from './../cart/actions/list-cart-items-action';
import { IMyDpOptions, IMyOptions } from 'mydatepicker';
import * as ShowCartSpinnerActions from './actions/start-cart-spinner.action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
import { SchoolEventInfo } from '../event/model/event';
import * as CategoryActions from '../categories/categories-actions';
import * as ListProductActions from '../products/actions/list-products-actions';
import { Meal } from '../categories/model/meal';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  providers: []
})

export class HomeComponent implements OnInit, OnDestroy {
  events: SchoolEventInfo[] = [];
  isEventsAreThere: boolean;
  obsListEventItems: Observable<SchoolEventInfo[]>;
  subObsListEventItems: Subscription;

  obsListEventItemsError: Observable<any>;
  subObsListEventItemsError: Subscription;


  eventId: number = +localStorage.getItem('eventId');
  newName: string = '';
  errorMessage: string;
  names: any[] = [];
  holidays: Holidays;
  showSpinner: boolean;
  bgImageUrl = 'monday.jpg';
  orderDate: any;
  disableDateRanges: any[] = [];
  disableDays: any[] = [];
  disableWeekdays: string[] = [];
  eventOrder: boolean;
  dailyOrder: boolean;
  minDate = new Date();
  d = new Date();
  currentYear = this.d.getFullYear();
  maxDate = new Date('31-11-' + this.currentYear);
  disableUntil: any = {
    year: this.minDate.getFullYear(),
    month: this.minDate.getMonth() + 1,
    day: this.minDate.getDate() - 1
  };

  disableSince: any = {
    year: this.currentYear,
    month: 12,
    day: 31
  };

  dateOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    inline: false,
    showSelectorArrow: false,
    openSelectorOnInputClick: true,
    openSelectorTopOfInput: false,
    editableDateField: false,
    disableUntil: this.disableUntil,
    disableSince: this.disableSince,
    disableDateRanges: this.disableDateRanges,
    disableDays: this.disableDays,
    disableWeekdays: this.disableWeekdays
  };

  obsHolidays: Observable<Holidays>;
  subObsHolidays: Subscription;
  obsHolidaysError: Observable<any>;
  subObsHolidaysError: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;

  obsSchoolInfo: Observable<SchoolInfo>;
  subObsSchoolInfo: Subscription;
  dailyCutOffTimeStr = '';

  constructor(private _route: Router, private _store: Store<fromRoot.State>, public parent: Parent, public schoolInfo: SchoolInfo) {
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsHolidays = this._store.select(fromRoot.selectListHolidaysSuccess);
    this.obsHolidaysError = this._store.select(fromRoot.selectListHolidaysFailure);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);

    this.obsListEventItems = this._store.select(fromRoot.selectListEventSuccess);
    this.obsListEventItemsError = this._store.select(fromRoot.selectListEventFailure);

  }

  ngOnInit() {
    this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(true));

    this.dailyOrder = true;
    let isOnInit = true;
    if (this.minDate.getDate() == 1) {
      this.minDate = new Date(this.minDate.getFullYear(), this.minDate.getMonth(), 0);
      this.dateOptions.disableUntil = this.disableUntil = {
        year: this.minDate.getFullYear(),
        month: this.minDate.getMonth() + 1,
        day: this.minDate.getDate()
      }
    }

    this._store.dispatch(new SidebarActions.SetSidebarName('commonSidebar'));
    if (!this.holidays) {
      this.showSpinner = true;
      this._store.dispatch(new ListHolidaysActions.ListHolidays());
    }

    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
      }
    })

    this.subObsSchoolInfo = this.obsSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        // cutoff_hour = rs("daily_hour")
        // cutoff_min = rs("daily_min")
        // if cutoff_hour < 12 then
        // ampm= "AM"
        // else
        // ampm= "PM"
        // end if

        let ampm = '';
        if (this.schoolInfo && this.schoolInfo.daily_hour && this.schoolInfo.daily_hour < 12) {
          ampm = 'AM';
        } else {
          ampm = 'PM';
        }

        let cutoff_hour0 = this.schoolInfo.daily_hour + '';
        switch (this.schoolInfo.daily_hour) {
          case 12:
            cutoff_hour0 = "12"; break;
          case 13:
            cutoff_hour0 = "1"; break;
          case 14:
            cutoff_hour0 = "2"; break;
          case 15:
            cutoff_hour0 = "3"; break;
          case 16:
            cutoff_hour0 = "4"; break;
          case 17:
            cutoff_hour0 = "5"; break;
          case 18:
            cutoff_hour0 = "6"; break;
          case 19:
            cutoff_hour0 = "7"; break;
          case 20:
            cutoff_hour0 = "8"; break;
          case 21:
            cutoff_hour0 = "9"; break;
          case 22:
            cutoff_hour0 = "10"; break;
          case 23:
            cutoff_hour0 = "11"; break;
          case 24:
            cutoff_hour0 = "12"; break;
        }
        if (this.schoolInfo.daily_min < 10) {
          this.dailyCutOffTimeStr = cutoff_hour0 + ":" + '0' + this.schoolInfo.daily_min + ":00" + " " + ampm;
        } else {
          this.dailyCutOffTimeStr = cutoff_hour0 + ":" + this.schoolInfo.daily_min + ":00" + " " + ampm;
        }
      }
    })
    this.subObsHolidays = this.obsHolidays.subscribe(holidays => {
      if (!isOnInit) {
        if (holidays) {
          this.maxDate = new Date(holidays.maxDate);
          this.holidays = holidays;
          console.log(this.holidays)
          this.disableDates();
          this.showSpinner = false;
        }
      }
    })

    this.subObsHolidaysError = this.obsHolidaysError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    isOnInit = false;

    switch (this.d.getDay()) {
      case 0:
        this.bgImageUrl = 'sunday.jpg';
        break;

      case 1:
        this.bgImageUrl = 'monday.jpg';
        break;

      case 2:
        this.bgImageUrl = 'tuesday.jpg';
        break;

      case 3:
        this.bgImageUrl = 'wednesday.jpg';
        break;

      case 4:
        this.bgImageUrl = 'thursday.jpg';
        break;

      case 5:
        this.bgImageUrl = 'friday.jpg';
        break;

      case 6:
        this.bgImageUrl = 'saturday.jpg'
        break;

      default:
        this.bgImageUrl = 'monday.jpg';
        break;
    }

    /*this._store.dispatch(new ListEventItemsActions.ListEvent('CANTEEN'));
    this.subObsListEventItems = this.obsListEventItems.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          if (result.length > 0) {
            this.isEventsAreThere = true;
          }
          else {
            this.isEventsAreThere = false;
          }
          this.showSpinner = false;
        }
      }
    })*/


  }

  disableDates() {
    if (this.holidays) {
      this.dateOptions.disableSince = this.disableSince = {
        year: this.maxDate.getFullYear(),
        month: this.maxDate.getMonth() + 1,
        day: this.maxDate.getDate()
      }

      if (this.holidays.holidayDateRange) {
        for (let i = 0; i < this.holidays.holidayDateRange.length; i++) {
          let startDate = (new Date(this.holidays.holidayDateRange[i].startDate));
          let endDate = new Date(this.holidays.holidayDateRange[i].endDate);
          var date = {
            begin: {
              year: startDate.getFullYear(),
              month: startDate.getMonth() + 1,
              day: startDate.getDate()
            },
            end: {
              year: endDate.getFullYear(),
              month: endDate.getMonth() + 1,
              day: endDate.getDate()
            }
          }
          this.disableDateRanges.push(date);
        }
        this.dateOptions.disableDateRanges = this.disableDateRanges;
      }

      if (this.holidays.cutOffDays) {
        //cutoff date
        for (let j = 0; j < this.holidays.cutOffDays.length; j++) {
          let newDateObj = new Date(this.holidays.cutOffDays[j]);
          let nYear = newDateObj.getFullYear();
          let nMonth = newDateObj.getMonth();
          let nDay = newDateObj.getDate();
          var disableDate = { year: nYear, month: nMonth + 1, day: nDay }
          this.disableDays.push(disableDate);
        }
        this.dateOptions.disableDays = this.disableDays;
      }

      if (this.holidays.weeklyClosures) {
        this.holidays.weeklyClosures.forEach(day => {
          switch (day) {
            case 1:
              this.disableWeekdays.push('su');
              break;
            case 2:
              this.disableWeekdays.push('mo');
              break;
            case 3:
              this.disableWeekdays.push('tu');
              break;
            case 4:
              this.disableWeekdays.push('we');
              break;
            case 5:
              this.disableWeekdays.push('th');
              break;
            case 6:
              this.disableWeekdays.push('fr');
              break;
            case 7:
              this.disableWeekdays.push('sa');
              break;
          }
        })
      }
    }
  }

  showDailyOrder() {
    this.dailyOrder = true;
    this.eventOrder = false;
  }

  showEventOrder() {
    this.eventOrder = true;
    this.dailyOrder = false;
  }

  startOrder() {
    if (this.orderDate) {
      var month = this.orderDate.date.month;
      var date = month + "-" + this.orderDate.date.day + "-" + this.orderDate.date.year;
      //console.log(date);
      localStorage.removeItem('eventId');
      localStorage.removeItem('eventName');
      localStorage.removeItem('bagName');
      localStorage.removeItem('bagPrice');
      localStorage.removeItem('isBagAvailable');
      localStorage.removeItem('isSetPrice');
      localStorage.removeItem('eventPrice');
      localStorage.removeItem('whoPays');
      localStorage.removeItem('isFundRaising');



      localStorage.setItem('deliveryDate', date);
      localStorage.setItem('showDate', this.orderDate.jsdate.toDateString());
      localStorage.setItem('orderType', 'canteen');
      this._store.dispatch(new ShowCartSpinnerActions.StartCartSpinner);
      this._store.dispatch(new ListCartItemsActions.ListCartItems(date, this.eventId));
      this._route.navigate(['canteenorder/dashboard/news']);
    }
  }

  ngOnDestroy() {
    if (this.subObsHolidays)
      this.subObsHolidays.unsubscribe();
    if (this.subObsHolidaysError)
      this.subObsHolidaysError.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsSchoolInfo)
      this.subObsSchoolInfo.unsubscribe();
  }
}
