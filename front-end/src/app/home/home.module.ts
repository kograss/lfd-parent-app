import { UpdateParentPlanModule } from './../update-parent-plan/update-parent-plan.module';
import { LoadingModule } from 'ngx-loading';
import { ListHolidaysService } from './list-holidays.service';
import { ListHolidaysEffects } from './list-holidays.effects';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MyDatePickerModule } from 'mydatepicker';
import {EventModule} from '../event/event.module'

@NgModule({
  imports: [
    MyDatePickerModule,
    BrowserAnimationsModule,
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    FormsModule,
    UpdateParentPlanModule,
    EventModule,
    EffectsModule.forFeature([ListHolidaysEffects]),    
    RouterModule.forRoot([{ path: 'home', component: HomeComponent }])
],
  declarations: [HomeComponent],
  exports: [HomeComponent],
  providers: [ListHolidaysService]
})
export class HomeModule { }
