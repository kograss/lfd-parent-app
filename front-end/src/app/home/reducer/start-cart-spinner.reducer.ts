import * as CartSpinnerActions from './../actions/start-cart-spinner.action';

export interface State {
    showSpinner: boolean;
}

const initialState: State = {
    showSpinner: false
}

export function reducer(state = initialState, action: CartSpinnerActions.All): State {
    //console.log(action.type);    
    switch (action.type) {
        case CartSpinnerActions.STARTCARTSPINNER: {
            return {
                ...state,
                showSpinner: true
            }
        }

        case CartSpinnerActions.STOPCARTSPINNER: {
            return {
                ...state,
                showSpinner: false
            }
        }

        default: {
            return state;
        }
    }
}
