import { Holidays } from './../model/holidays';
import * as ListHolidaysActions from '../actions/list-holidays.action';

export interface State{
    result : Holidays,
    err:any
}

const initialState: State = {
    result : new Holidays,
    err : {}
}

export function reducer(state = initialState, action: ListHolidaysActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListHolidaysActions.LISTHOLIDAYS_SUCCESS: {
            return {
                ...state,
                result: action.holidays
            }
        }
        case ListHolidaysActions.LISTHOLIDAYS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
