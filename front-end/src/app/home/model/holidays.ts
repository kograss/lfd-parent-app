import { DateRange } from './dateRange';
export class Holidays
{
    maxDate:string;
    weeklyClosures:number[];
    holidayDateRange:DateRange[];
    cutOffDays:Date[];
}