import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, HostListener } from '@angular/core';
import * as fromRoot from './reducers';
import { CommonService } from './common/common.service';
import { Constants } from './common/constants';
import * as ShowHideSidebarActions from './sidebar/actions/show-hide-sidebar.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  isNetworkAvailable: boolean = navigator.onLine;
  newAppVersion: number;
  isUpdateRequired: boolean = false;
  currentVersion: number = Constants.APP_VERSION;

  constructor(private _store: Store<fromRoot.State>, private commonService: CommonService) {
    window.addEventListener("online", () => {
      this.isNetworkAvailable = true;
    });
    window.addEventListener("offline", () => {
      this.isNetworkAvailable = false;
    });

  }

  ngOnInit() {
    this.commonService.getAppVersion().subscribe(v => {
      if (v) {
        this.newAppVersion = + v;
        if (this.currentVersion < this.newAppVersion) {
          //need to update
          this.isUpdateRequired = true;
        }
      }
    });

  }

  openMarket() {
    window.open("market://details?id=com.school24.app", "_system");
  }
}
