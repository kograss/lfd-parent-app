import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const CANCEL_CONFIRM_ALERT= 'CONFIRM ALERT';




export class CancelConfirmAlert {
    readonly type = CANCEL_CONFIRM_ALERT;
    constructor(public payload: Boolean) {
    }
}


export type All = CancelConfirmAlert
    
