import { LoadingModule } from 'ngx-loading';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert.component';
import { AlertService} from './alertService';



@NgModule({
  imports: [CommonModule,],
  exports: [AlertComponent],
  declarations: [AlertComponent],   
  providers: [AlertService]
})
export class AlertModule { }
