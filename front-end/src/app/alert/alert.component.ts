import { AlertService } from './alertService';
import * as AlertActions from './actions/alert-action';
import { AlertType, Alert } from './model/alert';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, EventEmitter, Output, ElementRef } from '@angular/core';
import * as fromRoot from '../reducers';


@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html',
    styleUrls: ['alert.component.css']
})
export class AlertComponent {
    alerts: Alert[] = [];
    confirm: boolean = false;
    Type: string = ""

    constructor(private alertService: AlertService, private _store: Store<fromRoot.State>) { }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alert: Alert) => {
            if (!alert) {
                // clear alerts when an empty alert is received
                this.alerts = [];
                return;
            }
            if (alert.type == AlertType.Confirm)
                this.confirm = true;
            else
                this.confirm = false;

            switch (alert.type) {
                case AlertType.Success:
                    this.Type = "Success";
                    break;
                case AlertType.Error:
                    this.Type = "Error";
                    break;
                case AlertType.Info:
                    this.Type = "Info";
                    break;
                case AlertType.Warning:
                    this.Type = "Warning";
                    break;
                case AlertType.Confirm:
                    this.Type = "Confirm";
                    break;
            }

            // add alert to array
            this.alerts.push(alert);
        });
    }

    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'w3-green';
            case AlertType.Error:
                return 'w3-red';
            case AlertType.Info:
                return 'w3-blue';
            case AlertType.Warning:
                return 'w3-yellow';
            case AlertType.Confirm:
                return 'w3-blue';
        }
    }

    confirmAlert(alert, yes) {

        this.confirm = false;
        this.removeAlert(alert);
        this._store.dispatch(new AlertActions.CancelConfirmAlert(new Boolean(yes)));

    }
}




