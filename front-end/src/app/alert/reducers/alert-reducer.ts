import * as AlertActions from './../actions/alert-action';

export interface State {
    result: Boolean,
    err: any
}

const initialState: State = {
    result:false,
    err: {}
};

export function reducer(state = initialState, action: AlertActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case AlertActions.CANCEL_CONFIRM_ALERT: {
            return {
                ...state,
                result: action.payload
            }
        }     
        default: {
            return state;
        }

    }

}
