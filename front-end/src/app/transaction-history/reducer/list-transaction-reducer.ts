import * as ListTransactionActions from '../actions/list-transaction-action';
import { Transaction } from '../model/Transaction';

export interface State {
    result: Transaction[],
    err: any
}

const initialState: State = {
    result: [],
    err: {}
};

export function reducer(state = initialState, action: ListTransactionActions.All): State {
    switch (action.type) {

        case ListTransactionActions.LISTTRANSACTION_SUCCESS: {
            return {
                ...state,
                result: action.transactions
            }
        }
        case ListTransactionActions.LISTTRANSACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }
    }
}
