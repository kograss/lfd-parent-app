export class Transaction {
    id: number;
    amount: number;
    type: string;
    commentOrReason: string;
}