import { AlertService } from './../alert/alertService';
import { AlertComponent } from './../alert/alert.component';
import {  Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Transaction } from './model/Transaction';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListTransactionActions from './actions/list-transaction-action';


declare var $: any;

@Component({
  selector: 'transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit, OnDestroy {
  message : string;
  transactions: Transaction[];
  transactionsCopy: Transaction[];

  filterQuery = '';
  
  obsListTransactions: Observable<Transaction[]>;
  subsObsListTransactions: Subscription;
  obsListTransactionsFailure: Observable<any>;
  subObsListTransactionsFailure: Subscription;

  constructor(private _store: Store<fromRoot.State>,
    private _router: Router, private alertService : AlertService) {

    this.obsListTransactions = this._store.select(fromRoot.selectlistTransactionHistorySuccess);
    this.obsListTransactionsFailure = this._store.select(fromRoot.selectlistTransactionHistoryFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new ListTransactionActions.ListTransaction);
    this.subsObsListTransactions = this.obsListTransactions.subscribe(result => {
      if (result) {
        this.transactions = result;
        this.transactionsCopy = result;
      }
    })

    this.subObsListTransactionsFailure = this.obsListTransactionsFailure.subscribe(error => {
      if (!isOnInit) {
        //this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  showAll(){
    this.transactions = this.transactionsCopy;
  }

  creditOnly(){
    this.transactions = this.transactionsCopy.filter(transaction => transaction.type.toUpperCase().includes('CREDIT'));
  }

  debitOnly(){
    this.transactions = this.transactionsCopy.filter(transaction => transaction.type.toUpperCase().includes('DEBIT') || transaction.type.toUpperCase().includes('PAYATCHECKOUT'));
  }

  ngOnDestroy() {
    if (this.subsObsListTransactions)
      this.subsObsListTransactions.unsubscribe();
    if (this.subObsListTransactionsFailure)
      this.subObsListTransactionsFailure.unsubscribe();
  }
}
