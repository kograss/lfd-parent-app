

import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListTransactionHistoryActions from './actions/list-transaction-action';

import { TransactionHistoryService } from './transaction-history.service';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class TransactionHistoryEffects {

    constructor(
        private actions$: Actions,
        private transactionHistoryService: TransactionHistoryService
    ) { }

    @Effect()
    listTransactionHistory$: Observable<Action> = this.actions$.pipe(ofType<ListTransactionHistoryActions.ListTransaction>(ListTransactionHistoryActions.LISTTRANSACTION)
       ,map(action => action)
        ,switchMap(
        payload => this.transactionHistoryService.getTransactions().pipe(
            map(results => new ListTransactionHistoryActions.ListTransactionSuccess(results))
            ,catchError(err =>
                of({ type: ListTransactionHistoryActions.LISTTRANSACTION_FAILURE, exception: err })
            )))
        );
}