import { Transaction } from './../model/Transaction';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const LISTTRANSACTION = 'LIST TRANSACTION';
export const LISTTRANSACTION_SUCCESS = 'LIST TRANSACTION SUCCESS';
export const LISTTRANSACTION_FAILURE = 'LIST TRANSACTION FAILURE';


export class ListTransaction {
    readonly type = LISTTRANSACTION;
    constructor() {
    }
}

export class ListTransactionSuccess implements Action {
    readonly type = LISTTRANSACTION_SUCCESS;
    constructor(public transactions: Transaction[]) {
    }
}

export class ListTransactionFailure implements Action {
    readonly type = LISTTRANSACTION_FAILURE;
    constructor(public err: any) {
    }
}

export type All = ListTransaction
    | ListTransactionSuccess | ListTransactionFailure
