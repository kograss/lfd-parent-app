import { TransactionDataFilterPipe } from './transaction-data-filter.pipe';
import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { TransactionHistoryComponent } from './transaction-history.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionHistoryEffects } from './transaction-history-effects';
import { EffectsModule } from '@ngrx/effects';
import { TransactionHistoryService } from './transaction-history.service';
import { Transaction } from './model/Transaction';
import { DataTableModule } from 'angular2-datatable';
import { BrowserModule } from '@angular/platform-browser';
import { Config } from './../config/config';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';
@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        DataTableModule,
        CommonMenuModule,
        LoadingModule.forRoot({
            fullScreenBackdrop:true
          }),
        EffectsModule.forFeature([TransactionHistoryEffects])        
],
declarations: [ TransactionHistoryComponent, TransactionDataFilterPipe ],
exports: [ TransactionHistoryComponent ],
providers: [ TransactionHistoryService, AlertService, Transaction ]
})  
export class TransactionHistoryModule{

}
