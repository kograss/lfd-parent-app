import { Observable} from 'rxjs';
import { Transaction } from './model/Transaction';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TransactionHistoryService {
    private _getTransactionsUrl = 'OrderHistory/GetTransactionHistory/';
    constructor(private _http: HttpClient) { }
    
    getTransactions():Observable<any> {
        return this._http.get(this._getTransactionsUrl)
      }
}