import { Parent } from '../user-profile/model/parent';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, Output, EventEmitter, ElementRef } from '@angular/core';
import * as fromRoot from '../reducers';
import { Cookie } from 'ng2-cookies';
import * as ShowHideProfileAction from './../navbar/action/show-hide-profile.action';
import * as ListUserActions from './../user-profile/actions/list-user-action';


@Component({
  host: {
    '(document:click)': 'onClick($event)',
  },
  selector: 'my-account',
  styleUrls: ['my-account.component.css'],
  templateUrl: 'my-account.component.html'
})
export class MyAccountComponent implements OnInit, OnDestroy {

  parent: Parent;
  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;


  //variables to denode page loading state

  showSpinner: boolean;

  showProfile: boolean;

  constructor(private _store: Store<fromRoot.State>, private _route: Router, private config: Config, private _eref: ElementRef) {
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
  }

  ngOnInit() {
    this.subObsListParent = this.obsListParent.subscribe(result =>{
      this.parent = result;
    })
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) // or some similar check
      if (this.showProfile) {
        this._store.dispatch(new ShowHideProfileAction.ShowHideProfile(false));
        this.showProfile = false;
      }
      else {
        this.showProfile = true;
      }
  }

  viewProfile(){
    this._route.navigate(['canteenorder/dashboard/user-profile']);
    this._store.dispatch( new ShowHideProfileAction.ShowHideProfile(!this.showProfile));
  }

  logout() {
    Cookie.deleteAll('/');
    localStorage.clear();
    this._route.navigate(['/']);
  }

  ngOnDestroy() {
    if(this.subObsListParent)
      this.subObsListParent.unsubscribe();
  }

}
