import { AlertService } from './../alert/alertService';
import { Config } from './../config/config';
import { CartResult } from './../cart/model/cartResult';
import { OptionsSubGroup } from './options/model/options-sub-group';
import { Router, ActivatedRoute } from '@angular/router';
import { AddToCartResult } from './../cart/model/addToCartResult';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { Student } from './model/student';
import { AddToCartModel } from './../cart/model/addToCart';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { Product } from './model/product';
import { ProductStock } from './model/productStock';
import * as ListProductActions from './actions/list-products-actions';
import * as ProductStockActions from './actions/product-stock-actions';
import * as AddToCartActions from './../cart/actions/add-to-cart-action';
import * as ListStudentActions from '../children/actions/list-children-actions';
import * as ListCartItemsActions from '../cart/actions/list-cart-items-action';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
import * as ListEventProductActions from './../event/actions/list-event-products.action';
import * as ListEventItemsActions from '../event/actions/list-event-items-action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
import { BannedFoodService } from './../banned-food/banned-food.service';
import { SchoolInfo } from '../common/school-info/model/school-info';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'products',
  templateUrl: 'products.component.html',
  styleUrls: ['products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  canteenClosedMessage: string = "";
  message: string;
  searchQuery: string;
  errorImageUrl: string = 'picture_not_available.png';
  products: Product[] = [];
  productsCopy: Product[] = [];
  students: Student[] = [];
  displayOptions: boolean = false;
  deliveryDate: string = localStorage.getItem('deliveryDate');
  showDate: Date;
  mealCode: string;
  categoryId: number;
  options: OptionsSubGroup[];
  optionsSelected: string = '';
  product: Product;
  productIndex: number;
  showDescription: boolean = false;
  mealName: string;
  mealNameToDisplay: string;
  categoryNameToDisplay: string;
  showSpinner: boolean;
  isOnInit: boolean = false;
  currency: string;
  isProductEmpty: boolean;
  orderType: string;
  dateHeader: string = 'Delivery Date';
  eventId: number;
  eventType: string;
  isSetPrice: number;
  eventPrice: number;
  event_type: number;
  isFundRaising: string;

  obsProducts: Observable<Product[]>;
  subObsProducts: Subscription;
  obsProductsError: Observable<any>;
  subObsProductsError: Subscription;

  obsProductsStock: Observable<ProductStock[]>;
  subProductsStock: Subscription;
  obsProductsStockError: Observable<any>;
  subProductsStockError: Subscription;

  obsListStudents: Observable<Student[]>;
  subObsListStudents: Subscription;
  obsListStudentError: Observable<any>;
  subObsListStudentError: Subscription;

  obsAddToCart: Observable<AddToCartResult>;
  subObsAddToCart: Subscription;
  obsAddToCartError: Observable<any>;
  subObsAddToCartError: Subscription;

  obsListCartItems: Observable<CartResult[]>;
  subObsListCartItems: Subscription;
  obsListCartItemsError: Observable<any>;
  subObsListCartItemsError: Subscription;

  allBannedProducts: any = [];
  schoolInfo: SchoolInfo;
  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  constructor(
    protected _store: Store<fromRoot.State>,
    private cartItem: AddToCartModel,
    private _router: ActivatedRoute,
    private _route: Router,
    private config: Config,
    private alertService: AlertService,
    public bannedFoodService: BannedFoodService) {
    this.obsProducts = this._store.select(fromRoot.selectSearchProductsSuccess);
    this.obsListStudents = this._store.select(fromRoot.selectListChildrenSuccess);
    this.obsAddToCart = this._store.select(fromRoot.selectAddToCartSuccess);
    this.obsListCartItems = this._store.select(fromRoot.selectListCartItemsSuccess);
    this.obsAddToCartError = this._store.select(fromRoot.selectAddToCartCartFailure);
    this.obsListCartItemsError = this._store.select(fromRoot.selectListCartItemsFailure);
    this.obsListStudentError = this._store.select(fromRoot.selectListChildrenFailure);
    this.obsProductsError = this._store.select(fromRoot.selectSearchProductsFailure);
    this.currency = this.config.getCurrency();
    this.obsProductsStock = this._store.select(fromRoot.selectProductsStockSuccess);
    this.obsProductsStockError = this._store.select(fromRoot.selectProductsStockFailure);

    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);
  }

  ngOnInit() {
    this.isOnInit = true;
    this.orderType = localStorage.getItem('orderType');
    this.eventId = +localStorage.getItem('eventId');
    this.eventType = localStorage.getItem('eventType');
    this.isSetPrice = +localStorage.getItem('isSetPrice');
    this.eventPrice = +localStorage.getItem('eventPrice');
    this.event_type = +localStorage.getItem("event_type");
    this.isFundRaising = localStorage.getItem("isFundRaising");


    this._store.dispatch(new ListSchoolInfoActions.ListSchoolInfo);

    this._router.params.subscribe(params => {
      this.showSpinner = true;
      this.mealCode = params['mealCode'];
      this.categoryId = params['categoryId'];

      if (this.deliveryDate != null && this.orderType != 'event' && this.products.length == 0) {
        this._store.dispatch(new ListProductActions.ListProducts(new ListProductActions.ListProductsSearchTerms(this.mealCode, this.categoryId, this.deliveryDate)));
      }
      else if (this.orderType == 'event') {
        this.dateHeader = 'Event Date';
        if (this.products.length == 0)
          this._store.dispatch(new ListEventProductActions.ListEventProduct(this.eventId));
        this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(false));
      }
      this.mealNameToDisplay = params['mealName'];
      this.categoryNameToDisplay = params['categoryName'];

      switch (this.mealCode) {
        case 'L':
          this.mealName = "LUNCH";
          break;
        case 'R':
          this.mealName = "RECESS";
          break;
        case 'T':
          this.mealName = "THIRD";
          break;
        default:
          this.mealName = "LUNCH";
          break;
      }
    })

    if (!this.deliveryDate || this.deliveryDate == null || this.deliveryDate.trim() == '') {
      this._route.navigate(['canteenorder/dashboard/home']);
      return;
    }
    else {
      this.showDate = this.convertStringToDate(this.deliveryDate);
    }
    this._store.dispatch(new SidebarActions.SetSidebarName('categories'));

    if (this.students.length == 0) {
      this.showSpinner = true;
      this._store.dispatch(new ListStudentActions.ListChildrens(true));
    }

    this.subObsAddToCart = this.obsAddToCart.subscribe(result => {
      if (!this.isOnInit) {
        if (result) {
          if (result.isPriceZeroError) {
            this.showSpinner = false;
            this.message = 'Product price cannot be zero please select options';
            this.alertService.warn(this.message);
          }
          else if (result.isQuanityExceeded) {
            this.showSpinner = false;
            let msg = "Following student has reached maximum quantity for this product ";

            result.quantityExceedProducts.forEach(element => {
              msg = msg + " - " + element.studentName + " " + element.exceededQuantity;
            });
            //var item = result.quantityExceedProducts[0];
            //var msg = "Quantity exceeded of " + item.productName + " by " + item.exceededQuantity + " maximum quantity allowed " + item.maximumQuantityAllowed + " For student - " + item.studentName;
            this.message = 'Following student has reached maximum quantity for this product';
            this.alertService.warn(msg);
          }
          this.showSpinner = false;
          if (result.cartCount) {
            this._store.dispatch(new ListCartItemsActions.ClearCartCount);
            this._store.dispatch(new ListCartItemsActions.SetCartCount(result.cartCount));
          }
          // this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate));
        }
      }
    })

    this.subObsAddToCartError = this.obsAddToCartError.subscribe(error => {
      if (!this.isOnInit) {
        this.showSpinner = false;
      }
    })

    //this._store.dispatch(new CategoryActions.ListProducts(new CategoryActions.ListProductsSearchTerms(2, "09-15-2017")));
    this.subObsListStudents = this.obsListStudents.subscribe(result => {
      this.students = result;

    })

    this.subObsListStudentError = this.obsListStudentError.subscribe(error => {
      if (!this.isOnInit) {
        this.showSpinner = false;
      }
    })



    this.subObsProducts = this.obsProducts.subscribe(

      products => {
        console.log("products: ", products)
        if (!this.isOnInit) {
          if (products.length > 0) {
            this.products = products.sort((p1, p2) => {
              if (p1.cname < p2.cname)
                return -1;
              if (p1.cname > p2.cname)
                return 1;
              return 0;
            });
            let productIds = [];
            for (let i = 0; i < this.products.length; i++) {
              this.products[i].studentIds = this.products[i].studentIds || [];
              this.products[i].quantity = this.products[i].quantity || 1;
              this.isProductEmpty = false;
              productIds.push(this.products[i].catalogID);
              // if (this.students && this.students.length > 0 && (this.students.length == 1 || this.event_type == 2)) {
              //   this.products[i].studentIds.push(this.students[0].studentId);
              // }
            }
            if (this.products.length > 0 && this.orderType != 'event') {
              this._store.dispatch(new ProductStockActions.GetProductsStock(productIds));
              //this.cdr.detach();
            }

            this.showSpinner = false;
          }
          else {
            this.showSpinner = false;
            this.products = [];
            this.isProductEmpty = true;
          }
        }
        this.productsCopy = this.products;
      }
    );

    this.subObsProductsError = this.obsProductsError.subscribe(error => {
      if (!this.isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsListCartItems = this.obsListCartItems.subscribe(result => {
      if (!this.isOnInit) {
        if (result) {
          this.showSpinner = false;
        }
      }
    })

    this.subObsListCartItemsError = this.obsListCartItemsError.subscribe(error => {
      if (!this.isOnInit) {
        this.showSpinner = false;
      }
    })


    this.subProductsStock = this.obsProductsStock.subscribe(productsStock => {
      if (this.products.length > 0) {
        // let newProductsArray:Product[]=[];

        this.products.forEach(product => {
          product.stock = productsStock.filter(productStock => productStock.productId == product.catalogID)[0].stock;

          if (product.stock < 0)
            product.stock = 0
          if (product.stock == null)
            product.stock = -999;
          //  newProductsArray.push(product);
        })
        // this.products=newProductsArray;
        //this.cdr.detectChanges();
      }



    })

    this.subProductsStockError = this.obsProductsStockError.subscribe(productStock => {


    })

    this.bannedFoodService.getAllBannedItems().subscribe(res => {
      this.allBannedProducts = res;

    },

      err => {
        console.log("ERROR");
        this.showSpinner = false;
      });


    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        if (!this.schoolInfo.canteenopen) {
          this.canteenClosedMessage = "School Canteen is currently closed. Please follow up with the school for further information.";
        }
      }
    })


    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {

    });

    this.isOnInit = false;
  }

  filterProducts() {
    this.products = this.productsCopy;
    if (this.searchQuery && this.searchQuery.trim() != '') {
      this.products = this.products.filter(product => product.cname.toLowerCase().includes(this.searchQuery.toLowerCase()));
    }
  }

  defaultProductImage(product: Product) {
    product.cimageurl = this.errorImageUrl;
  }

  selectStudents(id: number, product: Product, isChecked: boolean, productIndex: number) {
    
    if (isChecked) {
      this.products[productIndex].studentIds.push(id);
    }
    else {
      let index = this.products[productIndex].studentIds.indexOf(id);
      if(index >= 0){
        this.products[productIndex].studentIds.splice(index, 1);
      }
    }
    
  }

  createRange(number) {
    var items: number[] = [];
    for (var i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }

  addToCart(product: Product, productIndex: number) {
    this.showSpinner = true;
    if (this.product && this.productIndex >= 0 && this.mealName && this.mealName != '') {
      this.cartItem = new AddToCartModel();
      this.cartItem.categoryId = product.ccategory;
      this.cartItem.deliveryDate = localStorage.getItem('deliveryDate');
      this.cartItem.mealName = this.mealName;
      this.cartItem.optionIds = this.optionsSelected;
      this.cartItem.productId = product.catalogID;
      this.cartItem.productName = product.cname;
      this.cartItem.quantity = product.quantity;
      this.cartItem.studentIds = product.studentIds;
      if (this.orderType == 'event') {
        this.cartItem.eventId = this.eventId;
      }
      this._store.dispatch(new AddToCartActions.AddToCart(this.cartItem));
      if (this.students.length > 1) {
        this.products[productIndex].studentIds = [];
      }
      this.product = undefined;
      this.productIndex = undefined;
      this.optionsSelected = '';
    }
  }

  showOptions(product: Product, productIndex: number) {
    this.product = product;
    this.productIndex = productIndex;
    if (product.stock > 0 && product.stock < product.quantity) {
      this.message = 'Quantity selected exceeds stock available';
      this.alertService.warn(this.message);
      return;
    }
    if (product.studentIds.length > 0) {
      if (product.OptionSubGroup ? product.OptionSubGroup.length > 0 ? true : false : false) {
        this.options = product.OptionSubGroup;
        this.displayOptions = true;
      }
      else {
        this.addToCart(product, productIndex);
      }
    }
    else {
      this.message = 'You must select at least one student';
      this.alertService.warn(this.message);
    }
  }

  hideOptions() {
    this.displayOptions = false;
  }

  selectedOptions(options) {
    this.displayOptions = false;
    if (options != 'close') {
      this.optionsSelected = options;
      this.addToCart(this.product, this.productIndex);
    }
  }

  showProductDescription(product) {
    this.showDescription = true;
    this.product = product;
  }

  hideDescription() {
    this.showDescription = false;
  }

  convertStringToDate(dateString: string) {
    let values = dateString.split('-');
    let year = +values[2];
    let month = +values[0] - 1;
    let date = +values[1];
    return new Date(year, month, date);
  }

  getTrafficLightImage(healthcategory) {
    let healthCategoryImagePath = '';
    switch (healthcategory) {
      case 1:
        healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/green.png';
        break;
      case 2:
        healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/amber.png';
        break;
      case 3:
        healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/red.png';
        break;
    }
    return healthCategoryImagePath;
  }

  isThisProductAvailableForStudent(product, student) {

    if (this.schoolInfo && this.schoolInfo.isSplitMenuActivated) {

      if ((product.productClassification == null ||
        product.productClassification.indexOf(student.classForClassification) !== -1 ||
        product.productClassification == "")) {

        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }

  }

  isThisStudentBanned(product, student) {
    if (this.allBannedProducts) {
      let x = this.allBannedProducts.filter(e => e.studentId == student && e.productId == product);
      if (x.length > 0) {
        return 1;
      }
      else {
        return 0;
      }
    }
    else {
      return 0;
    }
  }

  ifSchoolEventAllowedClassIds(product, student): boolean {
    //console.log("Allowed Classes: ", product.allowedClasses)
    if (product.allowedClasses) {
      let arr = product.allowedClasses.split(',');
      arr = arr.map(e => e.trim().toLowerCase());
      if (arr.includes(student.studentClass.toLowerCase())) {
        return true;
      }
      return false;
    }
    return true;
  }

  ngOnDestroy() {
    if (this.subObsListStudents)
      this.subObsListStudents.unsubscribe();
    if (this.subObsAddToCart)
      this.subObsAddToCart.unsubscribe();
    if (this.subObsAddToCartError)
      this.subObsAddToCartError.unsubscribe();
    if (this.subObsListCartItems)
      this.subObsListCartItems.unsubscribe();
    if (this.subObsListCartItemsError)
      this.subObsListCartItemsError.unsubscribe();
    if (this.subObsListStudentError)
      this.subObsListStudentError.unsubscribe();
    if (this.subObsProducts)
      this.subObsProducts.unsubscribe();
    if (this.subObsProductsError)
      this.subObsProductsError.unsubscribe();
    if (this.subProductsStock)
      this.subProductsStock.unsubscribe();
    if (this.subProductsStockError)
      this.subProductsStockError.unsubscribe();
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();

  }
}
