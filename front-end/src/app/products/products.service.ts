import { Injectable } from '@angular/core';
import { Product } from './model/product';

import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class ProductsService {

    private _url = "Product/{mealName}/{categoryId}/{deliveryDate}";

    private _stockurl = "GetProductStock/{productIds}";

    constructor(private _http: HttpClient) {

    }

    getProducts(mealName: string, categoryId: number, deliveryDate: Date): Observable<any> {
        var mealCategories = this._http
            .get(this._url
                .replace("{mealName}", mealName)
                .replace("{categoryId}", categoryId.toString())
                .replace("{deliveryDate}", deliveryDate.toString())
            )
            
        return mealCategories;
        //return JSON.parse(this.holidayList);        

    }

    getProductsStock(productIds: number[]):Observable<any> {
        //let  pro = {productIds: productIds}
        var productsStock = this._http
            .get(this._stockurl
                .replace("{productIds}",  JSON.stringify(productIds))                   
            )
            
        return productsStock;

    }
}