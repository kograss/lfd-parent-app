import * as ProductStockActions from './../actions/product-stock-actions'
import { ProductStock } from './../model/productStock';

export interface State {    
   
    result: ProductStock[],
    err: any
}

const initialState: State = {    
   
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ProductStockActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ProductStockActions.GETPRODUCTSSTOCK_SUCCESS: {
            return {
                ...state,
                result: action.payload
            }
        }
        case ProductStockActions.GETPRODUCTSSTOCK_FAILURE: {
            return {
                ...state,
                err: action.payload
            }
        }          

        default: {
            return state;
        }

    }

}
