import * as ListProductsActions from './../actions/list-products-actions'
import { Product } from './../model/product';

export interface State {    
   
    result: Product[],
    err: any
}

const initialState: State = {    
   
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListProductsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListProductsActions.LISTPRODUCTS_SUCCESS: {
            return {
                ...state,
                result: action.payload
            }
        }
        case ListProductsActions.LISTPRODUCTS_FAILURE: {
            return {
                ...state,
                err: action.payload
            }
        }          

        default: {
            return state;
        }

    }

}
