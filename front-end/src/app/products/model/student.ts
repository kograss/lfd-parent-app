export class Student {
    studentId: number;
    parentId: number;
    schoolId: number;
    firstName: string;
    lastName: string;
    studentClass: string;
    klass:string;
    classForClassification:string;
}