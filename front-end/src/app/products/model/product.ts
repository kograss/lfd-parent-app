import { OptionsSubGroup } from './../options/model/options-sub-group';
export class Product {
    ccategory: number;
    catalogID: number;
    ccode: string;
    cprice: number;
    cimageurl: string;
    cdateavailable: string;
    healthcategory: number;
    food_label: string;
    quantity_limit: number;
    cdescription: string;
    cname: string;
    productClassification: string;
    studentIds: number[] = [];
    quantity: number;
    OptionSubGroup: OptionsSubGroup[] = [];
    stock:number;
    allowedClasses: string;
}