import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { DescriptionComponent } from './description/description.component';
import { OptionGroupComponent } from './options/option-group/option-group.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptionsComponent } from './options/options.component';
import { RouterModule } from '@angular/router';
import { AddToCartModel } from './../cart/model/addToCart';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from './products-effects';
import { ProductsService } from './products.service';
import { BrowserModule } from '@angular/platform-browser';
import { SafeHtmlPipe } from '../common/SafeHtml.pipe';
import { FundRaisingEventComponent } from '../event/fund-raising/fund-raising.component';
import { EventModule } from '../event/event.module';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    EventModule,
    EffectsModule.forFeature([ProductEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    RouterModule.forRoot([{ path: 'products', component: ProductsComponent }])
  ],
  declarations: [
    ProductsComponent,
    OptionsComponent,
    OptionGroupComponent,
    DescriptionComponent,
    SafeHtmlPipe,
  ],
  exports: [ProductsComponent, RouterModule],
  providers: [ProductsService, AddToCartModel, AlertService]
})
export class ProductsModule { }
