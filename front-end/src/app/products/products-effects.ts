import { CartService } from './../cart/cart.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType} from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import { ProductsService } from './products.service';

import * as ListProductsActions from './actions/list-products-actions';
import * as ProductStockActions from './actions/product-stock-actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class ProductEffects {

  constructor(
    private actions$: Actions,
    private productsService: ProductsService,
    private cartService: CartService
  ) { }

  @Effect()
  listProducts$: Observable<Action> = this.actions$.pipe(ofType<ListProductsActions.ListProducts>(ListProductsActions.LISTPRODUCTS)
    ,map(action => action.payload)
    ,switchMap(
    payload => this.productsService.getProducts(payload.mealCode, payload.categoryId, payload.deliveryDate).pipe(
      map(results => new ListProductsActions.ListProductsSuccess(results))
      ,catchError(err =>
        of({ type: ListProductsActions.LISTPRODUCTS_FAILURE, err: err })
      )))
    );


    
  @Effect()
  getProductsStock$: Observable<Action> = this.actions$.pipe(ofType<ProductStockActions.GetProductsStock>(ProductStockActions.GETPRODUCTSSTOCK)
    ,map(action => action.payload)
    ,switchMap(
    payload => this.productsService.getProductsStock(payload).pipe(
      map(results => new ProductStockActions.GetProductsStockSuccess(results))
      ,catchError(err =>
        of({ type: ProductStockActions.GETPRODUCTSSTOCK_FAILURE, exception: err })
      )))
    );
}