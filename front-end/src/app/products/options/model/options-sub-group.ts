import { Options } from './options';
export class OptionsSubGroup {
    OptionSubGroupName: string;
    Max_Items: number;
    Required: boolean = false;
    options: Options[] = [];
}