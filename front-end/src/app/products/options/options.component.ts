import { OptionGroupComponent } from './option-group/option-group.component';
import { OptionsSubGroup } from './model/options-sub-group';
import { Options } from './model/options';
import { Subscription } from 'rxjs';
import { Product } from './../model/product';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChildren, QueryList,ElementRef } from '@angular/core';
import * as fromRoot from '../../reducers';

@Component({
    selector: 'options',
    templateUrl: 'options.component.html',
    host: {
        '(document:click)': 'onClick($event)',
    }
})

export class OptionsComponent implements OnInit {

    @Input()
    optionGroups: OptionsSubGroup[]

    @Output()
    exportOptions: EventEmitter<string> = new EventEmitter();

    selectedOptions: number[] = [];
    isCartOpen:boolean;
    optionGroupValid = true;

    @ViewChildren(OptionGroupComponent) viewChildren: QueryList<OptionGroupComponent>;

    constructor(private _eref: ElementRef) {
    }

    ngOnInit() {
    }

    onClick(event) {
        if (!this._eref.nativeElement.contains(event.target)) // or some similar check
          if (this.isCartOpen) {
            this.exportOptions.emit('close');
            this.isCartOpen = false;
          }
          else {
            this.isCartOpen = true;
          }
      }

    addToCart() {
        let optionIds = '';  
        this.optionGroupValid = true;      
        this.viewChildren.forEach(element => {
            if (!element.validate())
                this.optionGroupValid = false;
            //console.log(optionGroupValid);
        });

        if (this.optionGroupValid) {
            //get all ids from option group component
            this.viewChildren.forEach(element => {
               let options:number[] = element.getOptions()
               for(let optionId of options){
                   this.selectedOptions.push(optionId);
               }
            });
            this.selectedOptions.sort();
            for (var id of this.selectedOptions) {
                if (optionIds == '') {
                    optionIds += id;
                }
                else {
                    optionIds += '|' + id;
                }
            }
            //console.log(optionIds);
            this.exportOptions.emit(optionIds);
        }
    }
}