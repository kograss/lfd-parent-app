import { OptionsSubGroup } from './../model/options-sub-group';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'option-group',
    templateUrl: 'option-group.component.html'
})
export class OptionGroupComponent implements OnInit {
    @Input()
    optionSubGroup: OptionsSubGroup

    selectedRadioOption: number;
    optionIds: number[] = [];
    totalSelected: number;
    validOptions: boolean = true;
    maxSelect: number;
    isSetPrice = +localStorage.getItem('isSetPrice');
    eventPrice = +localStorage.getItem('eventPrice');
    orderType: string = localStorage.getItem('orderType');

    ngOnInit() {
        if (this.optionSubGroup.Max_Items > 0) {
            this.maxSelect = this.optionSubGroup.Max_Items;
        }
        else {
            this.maxSelect = this.optionSubGroup.options.length;
        }
    }

    public validate(): boolean {
        //console.log(this.optionSubGroup.OptionSubGroupName);
        let total = this.selectedRadioOption ? 1 + this.optionIds.length : 0 + this.optionIds.length;
        if (total == 0 && this.optionSubGroup.Required) {
            this.validOptions = false
            return false;
        }
        else
            return true;
    }

    public getOptions(): number[] {
        if (this.selectedRadioOption)
            this.optionIds.push(this.selectedRadioOption)
        return this.optionIds;
    }

    selectOption(id: number, event, groupName) {
        this.totalSelected = this.selectedRadioOption ? 2 + this.optionIds.length : 1 + this.optionIds.length;
        if (this.selectedRadioOption && event.target.type == "radio") {
            this.totalSelected = this.totalSelected - 1;
        }
        if (event.target.checked) {
            if (this.totalSelected <= this.maxSelect) {
                if (event.target.type == "radio") {
                    this.selectedRadioOption = id;
                }
                else {
                    this.optionIds.push(id);
                }
            }
            else {
                event.target.checked = false;
            }
        }
        else {
            let index = this.optionIds.indexOf(id);
            this.optionIds.splice(index, 1);
        }
    }
}