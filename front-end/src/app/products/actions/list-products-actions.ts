import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Product } from './../model/product';



export const LISTPRODUCTS = '[PRODUCT] LIST';

export const LISTPRODUCTS_SUCCESS = '[PRODUCT] LIST SUCCESS';

export const LISTPRODUCTS_FAILURE = '[PRODUCT] LIST FAILURE';




export class ListProductsSearchTerms{
    mealCode :string;
    categoryId:number;
    deliveryDate:Date;
    constructor(mealCode,categoryId, deliveryDate){
        
        this.mealCode=mealCode;
        this.categoryId=categoryId;
        this.deliveryDate = deliveryDate;
    }

}


export class ListProducts implements Action {

    readonly type = LISTPRODUCTS;  
    
    constructor(public payload:ListProductsSearchTerms) {

    }

}

export class ListProductsSuccess implements Action {

    readonly type = LISTPRODUCTS_SUCCESS;
    constructor(public payload: any) {
        if(this.payload==null){
            this.payload = [];
        }
        ////console.log("PRODUCTList is "+JSON.stringify(PRODUCTList));
    }

}

export class ListProductsFailure implements Action {

    readonly type = LISTPRODUCTS_FAILURE;
    constructor(public payload: any) {
        //console.log("Exception is " + JSON.stringify(payload));
    }

}

export type All = ListProducts
    | ListProductsSuccess | ListProductsFailure 