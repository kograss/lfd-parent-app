import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ProductStock } from './../model/productStock';



export const GETPRODUCTSSTOCK = '[PRODUCTS] GET STOCK';

export const GETPRODUCTSSTOCK_SUCCESS = '[PRODUCTS] GET STOCK SUCCESS';

export const GETPRODUCTSSTOCK_FAILURE = '[PRODUCTS] GET STOCK FAILURE';




export class GetProductsStock implements Action {

    readonly type = GETPRODUCTSSTOCK;

    constructor(public payload: number[]) {

    }

}

export class GetProductsStockSuccess implements Action {

    readonly type = GETPRODUCTSSTOCK_SUCCESS;
    constructor(public payload: ProductStock[]) {
        if (this.payload == null) {
            this.payload = [];
        }
        ////console.log("PRODUCTList is "+JSON.stringify(PRODUCTList));
    }

}

export class GetProductsStockFailure implements Action {

    readonly type = GETPRODUCTSSTOCK_FAILURE;
    constructor(public payload: any) {
        //console.log("Exception is " + JSON.stringify(payload));
    }

}

export type All = GetProductsStock
    | GetProductsStockSuccess | GetProductsStockFailure 