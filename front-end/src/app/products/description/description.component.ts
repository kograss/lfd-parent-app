import { Config } from './../../config/config';
import { Product } from './../model/product';
import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'product-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class DescriptionComponent implements OnInit {
  @Input()
  product: Product
  foodLabel: string = '';
  currency: string;
  healthCategoryImagePath: string;
  isDescOpen:boolean;
  
  @Output()
  closeDesc: EventEmitter<any> = new EventEmitter();

  constructor(private config: Config,private _eref: ElementRef) {
    this.currency = this.config.getCurrency();
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) // or some similar check
      if (this.isDescOpen) {
        this.closeDesc.emit();
        this.isDescOpen = false;
      }
      else {
        this.isDescOpen = true;
      }
  }
  ngOnInit() {
    //console.log(this.product);
    this.refineFoodLables(this.product.food_label);
    //console.log(this.product);
    switch (this.product.healthcategory) {
      case 1:
        this.healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/green.png';
        break;
      case 2:
        this.healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/amber.png';
        break;
      case 3:
        this.healthCategoryImagePath = 'https://www.school24.net.au/canteenorder/assets/red.png';
        break;
    }
  }

  refineFoodLables(food_label) {
    if (food_label) {
      if (food_label.indexOf("GL") != -1) {
        this.foodLabel = "Gluten Free" + ",";
      }
      if (food_label.indexOf("PRZ") != -1) {
        this.foodLabel = this.foodLabel + "Preservatives Free" + ",";
      }
      if (food_label.indexOf("DR") != -1) {
        this.foodLabel = this.foodLabel + "Dairy Free" + ",";
      }
      if (food_label.indexOf("SUG") != -1) {
        this.foodLabel = this.foodLabel + "No Added Sugar" + ",";
      }
      if (food_label.indexOf("LF") != -1) {
        this.foodLabel = this.foodLabel + "Low in Fat" + ",";
      }
      if (food_label.indexOf("ORG") != -1) {
        this.foodLabel = this.foodLabel + "Certified Organic" + ",";
      }
      if (food_label.indexOf("HAL") != -1) {
        this.foodLabel = this.foodLabel + "Certified Halal" + ",";
      }
      if (food_label.indexOf("VG") != -1) {
        this.foodLabel = this.foodLabel + "Vegetarian" + ",";
      }
      if (food_label.indexOf("HL") != -1) {
        this.foodLabel = this.foodLabel + "Halal" + ",";
      }
      if (food_label.indexOf("KO") != -1) {
        this.foodLabel = this.foodLabel + "Kosher";
      }
      if (this.foodLabel && this.foodLabel != null && this.foodLabel.trim() != '') {
        var lastChar = this.foodLabel.slice(-1);
        if (lastChar == ",") {
          this.foodLabel = this.foodLabel.slice(0, -1);
        }
      }
    }
  }
}
