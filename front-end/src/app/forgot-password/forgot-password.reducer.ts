
import * as ForgotPasswordActions from './forgot-password.action'

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
   
    result: {},
    err: {}
};

export function reducer(state = initialState, action: ForgotPasswordActions.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {
        
       
        case ForgotPasswordActions.FORGOTPASSWORDACTION_SUCCESS: {
            return {
                ...state,
                result: action.result
            }
        }  

        case ForgotPasswordActions.FORGOTPASSWORDACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }  

        default: {
            return state;
        }

    }

}
