import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ForgotPasswordService } from './forgot-password.service';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ResetPasswordComponent } from './reset-password.component';
import { ForgotPasswordEffects } from './forgot-password.effects';
import { EffectsModule } from '@ngrx/effects';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule } from 'ng-recaptcha';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RecaptchaModule,
    RouterModule,
    EffectsModule.forFeature([ForgotPasswordEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
    // RouterModule.forRoot([
    //     { path: 'canteenorder/forgot-password', component: ForgotPasswordComponent },
    //     { path: 'canteenorder/ResetPassword/:randomString', component: ResetPasswordComponent }
    // ]),
    RecaptchaModule.forRoot()
  ],
  declarations: [ForgotPasswordComponent, ResetPasswordComponent],
  exports: [ForgotPasswordComponent, ResetPasswordComponent],
  providers: [ForgotPasswordService, AlertService]
})
export class ForgotPasswordModule {

}
