import { Action } from '@ngrx/store';


export const FORGOTPASSWORDACTION = '[FORGOTPASSWORD] TOKEN';
export const FORGOTPASSWORDACTION_SUCCESS = '[FORGOTPASSWORD] TOKEN SUCCESS';
export const FORGOTPASSWORDACTION_FAILURE = '[FORGOTPASSWORD] TOKEN FAILURE';
export const CLEARFORGOTPASSWORDRESULT = 'CLEAR FORGOTPASSWORD RESULT';


export class ForgotPassword {
    readonly type = FORGOTPASSWORDACTION
    constructor(public payload: string) { }
}

export class ForgotPasswordSuccess implements Action {
    readonly type = FORGOTPASSWORDACTION_SUCCESS;
    constructor(public result: any) { }
}

export class ForgotPasswordFailure implements Action {
    readonly type = FORGOTPASSWORDACTION_FAILURE;
    constructor(public err: any) {
        
    }
}



export type All = ForgotPassword
    | ForgotPasswordSuccess | ForgotPasswordFailure 