import { AlertService } from './../alert/alertService';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as ForgotPasswordActions from './forgot-password.action';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})

//TODO:force SSL only, XSS checking

export class ForgotPasswordComponent implements OnInit {

  message: string;

  obsForgotPasswordSuccess: Observable<any>;
  subsForgotPasswordSuccess: Subscription;
  obsForgotPasswordFailure: Observable<any>;
  subsForgotPasswordFailure: Subscription;

  email: string;
  captcha: string;
  IsOnInit = false;

  showSpinner: boolean;

  invalidEmail = false;

  forgotPasswordForm: FormGroup;


  constructor(private _store: Store<fromRoot.State>, private _router: Router, private alertService: AlertService, private fb: FormBuilder) {
    this.obsForgotPasswordSuccess = this._store.select(fromRoot.selectForgotPasswordSuccess);
    this.obsForgotPasswordFailure = this._store.select(fromRoot.selectForgotPasswordFailure);
  }

  ngOnInit() {

    this.subsForgotPasswordSuccess = this.obsForgotPasswordSuccess.subscribe(result => {
      if (this.IsOnInit) {
        this.showSpinner = false;
        if (result) {
          this.message = result;
          this.alertService.success(this.message, true );
          this._router.navigate(['/'])

        }
      }
    })

    this.subsForgotPasswordFailure = this.obsForgotPasswordFailure.subscribe(res => {
      if (this.IsOnInit) {
        this.showSpinner = false;
        this.invalidEmail = true;

      }
    }
    )

    this.forgotPasswordForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      // capcha: ['', Validators.compose([Validators.required])],
    });

    this.IsOnInit = true;

  }

  sendResetLink() {
    if (this.forgotPasswordForm.invalid) {
      <any>Object.values(this.forgotPasswordForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }

    this.invalidEmail = false;
    this.showSpinner = true;
    this._store.dispatch(new ForgotPasswordActions.ForgotPassword(this.forgotPasswordForm.controls.username.value));
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }


  hideForgotPassword(){
    this._router.navigate(['/']);
  }


  ngOnDestroy() {
    if (this.subsForgotPasswordSuccess)
      this.subsForgotPasswordSuccess.unsubscribe();
    if (this.subsForgotPasswordFailure)
      this.subsForgotPasswordFailure.unsubscribe();
  }
}


