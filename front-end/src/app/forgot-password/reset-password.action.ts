import { Action } from '@ngrx/store';

import { ResetPasswordModel } from './model/ResetPasswordModel';

export const RESETPASSWORDACTION = '[RESETPASSWORD] TOKEN';
export const RESETPASSWORDACTION_SUCCESS = '[RESETPASSWORD] TOKEN SUCCESS';
export const RESETPASSWORDACTION_FAILURE = '[RESETPASSWORD] TOKEN FAILURE';
export const CLEARRESETPASSWORDRESULT = 'CLEAR RESETPASSWORD RESULT';


export class ResetPassword {
    readonly type = RESETPASSWORDACTION
    constructor(public payload:ResetPasswordModel) { }
}

export class ResetPasswordSuccess implements Action {
    readonly type = RESETPASSWORDACTION_SUCCESS;
    constructor(public result: any) { }
}

export class ResetPasswordFailure implements Action {
    readonly type = RESETPASSWORDACTION_FAILURE;
    constructor(public err: any) {
        
    }
}



export type All = ResetPassword
    | ResetPasswordSuccess | ResetPasswordFailure 