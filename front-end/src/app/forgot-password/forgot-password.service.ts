import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ResetPasswordModel } from './model/ResetPasswordModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ForgotPasswordService {
  private _forgotPasswordUrl = "ForgotPassword/SendLink";
  private _resetPasswordUrl = "ResetPassword";
  constructor(private _http: HttpClient) {

  }

  sendRestLink(email: string): Observable<any> {


    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    });
    let options = { headers: headers };
    var obj = { "email": email };
    var emailJson = JSON.stringify(obj);

    return this._http.post(this._forgotPasswordUrl, emailJson, options);
  }


  resetPassword(payload: ResetPasswordModel): Observable<any> {


    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    });
    let options = { headers: headers };
    var obj = { "password": payload.password, "randomString": payload.randomString };
    var passwordJson = JSON.stringify(obj);

    return this._http.put(this._resetPasswordUrl, passwordJson, options);
  }

}



