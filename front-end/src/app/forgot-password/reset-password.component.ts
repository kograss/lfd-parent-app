import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as ResetPasswordActions from './reset-password.action';
import { ActivatedRoute, Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import { ResetPasswordModel } from './model/ResetPasswordModel';
import { AlertService } from '../alert/alertService';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})

//TODO:force SSL only, XSS checking

export class ResetPasswordComponent implements OnInit {
  obsResetPasswordSuccess: Observable<any>;
  subsResetPasswordSuccess: Subscription;
  obsResetPasswordFailure: Observable<any>;
  subsResetPasswordFailure: Subscription;

  password: string;
  repeatPassword: string;
  randomString: string;
  IsOnInit = false;
  showSpinner: boolean;
  passwordsDoNotMatch = false;

  resetPasswordForm: FormGroup;

  constructor(private _store: Store<fromRoot.State>, private _router: Router, private _route: ActivatedRoute,
    private alertService: AlertService, private fb: FormBuilder) {
    this.obsResetPasswordSuccess = this._store.select(fromRoot.selectResetPasswordSuccess);
    this.obsResetPasswordFailure = this._store.select(fromRoot.selectResetPasswordFailure);
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      if (params['randomString']) {
        this.randomString = (params['randomString']);
      }
    })
    this.subsResetPasswordSuccess = this.obsResetPasswordSuccess.subscribe(result => {
      if (this.IsOnInit) {
        this.showSpinner = false;

        if (result) {
          this.alertService.success("Password has been changed successfully. Login with new password", true);
          this._router.navigate(['/']);
        }
      }
    })

    this.subsResetPasswordFailure = this.obsResetPasswordFailure.subscribe(res => {
      if (this.IsOnInit) {
        this.showSpinner = false;
        this.passwordsDoNotMatch = false;

        switch (res.status) {
          case 400: {
            this.alertService.success("The link to reset password is corrupted. Please try to reset password again by clicking on forgot password button", true);
            this._router.navigate(['/']);
            return;
          }
          case 404: {
            this.alertService.success("Parent does not exist . Please contact your administrator", true);
            this._router.navigate(['/']);
            return;
          }
          case 410: {
            this.alertService.success("The link has expired. Please try to reset password again by clicking on forgot password button", true);
            this._router.navigate(['/']);
            return;
          }

        }
      }
    }
    )

    this.IsOnInit = true;

    this.resetPasswordForm = this.fb.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      newPasswordRepeat: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    });

  }

  resetPassword() {
    if (this.resetPasswordForm.invalid) {
      <any>Object.values(this.resetPasswordForm.controls).forEach(control => {
        control.markAsTouched();
      });
      return;
    }
    let comp = this.resetPasswordForm.controls.password.value.localeCompare(this.resetPasswordForm.controls.newPasswordRepeat.value);
    this.passwordsDoNotMatch = false;
    if (comp == 0) {
      this.showSpinner = true;
      let payload: ResetPasswordModel = new ResetPasswordModel();
      payload.password = this.resetPasswordForm.controls.password.value;
      payload.randomString = this.randomString;
      this._store.dispatch(new ResetPasswordActions.ResetPassword(payload));
    }
    else {
      this.passwordsDoNotMatch = true;
    }
  }

  ngOnDestroy() {
    if (this.subsResetPasswordSuccess)
      this.subsResetPasswordSuccess.unsubscribe();
    if (this.subsResetPasswordFailure)
      this.subsResetPasswordFailure.unsubscribe();
  }
}


