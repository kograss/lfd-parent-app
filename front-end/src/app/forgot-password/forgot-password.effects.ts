import { ForgotPasswordService } from './forgot-password.service';
import { Action } from '@ngrx/store';
import { Observable, of, Subscription } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import * as ForgotPasswordActions from './forgot-password.action';
import * as ResetPasswordActions from './reset-password.action';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class ForgotPasswordEffects {

    constructor(
        private actions$: Actions,
        private forgotPasswordService: ForgotPasswordService
    ) { }

    @Effect()
    forgotPasswordAction$: Observable<Action> = this.actions$.pipe(ofType<ForgotPasswordActions.ForgotPassword>(ForgotPasswordActions.FORGOTPASSWORDACTION)
        , switchMap(
            payload => this.forgotPasswordService.sendRestLink(payload.payload).pipe(
                map(results => 
                  {
                    return new ForgotPasswordActions.ForgotPasswordSuccess(results)
                    
                  })
                  
                , catchError(err => of(new ForgotPasswordActions.ForgotPasswordFailure(err)))
            )));

    @Effect()
    resetPasswordAction$: Observable<Action> = this.actions$.pipe(ofType<ResetPasswordActions.ResetPassword>(ResetPasswordActions.RESETPASSWORDACTION)
        , switchMap(
            payload => this.forgotPasswordService.resetPassword(payload.payload).pipe(
                map(results => new ResetPasswordActions.ResetPasswordSuccess(results)),
                catchError(err => of(new ResetPasswordActions.ResetPasswordFailure(err)))
            )));



}