
import * as ResetPasswordActions from './reset-password.action'

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
   
    result: {},
    err: {}
};

export function reducer(state = initialState, action: ResetPasswordActions.All): State {
    console.log("Action Type" + action.type);
    switch (action.type) {
        
       
        case ResetPasswordActions.RESETPASSWORDACTION_SUCCESS: {
            return {
                ...state,
                result: action.result
            }
        }  

        case ResetPasswordActions.RESETPASSWORDACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }  

        default: {
            return state;
        }

    }

}
