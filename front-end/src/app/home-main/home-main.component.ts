import { Component, OnInit, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Parent } from '../user-profile/model/parent';
import * as fromRoot from '../reducers';
import * as ListParentActions from './../user-profile/actions/list-user-action';
import { SchoolInfo, SchoolShops } from '../common/school-info/model/school-info';
import * as ListSchoolInfoActions from '../common/school-info/actions/list-school-info.actions';
import * as SidebarActions from '../sidebar/actions/sidebar.action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
import { ListHomeChecksService } from './home.service';
import { StudentAvailabilityCheck } from './model/StudentAvailabilityCheck';
import { AlertService } from '../alert/alertService';
import { SchoolInfoService } from '../common/school-info/school-info.service';

declare var navigator: any;

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.css']
})
export class HomeMainComponent implements OnInit {

  @HostListener('window:popstate') backbuttonpressed() {
    if (navigator.app) {
      navigator.app.exitApp();
    } else if (navigator.device) {
      navigator.device.exitApp();
    } else {
      window.close();
    }
  }
  parent: Parent;
  schoolInfo: SchoolInfo;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentError: Observable<any>;
  subObsListParentError: Subscription;
  sidebarStatus: boolean = true;

  obsListSchoolInfo: Observable<SchoolInfo>;
  subObsListSchoolInfo: Subscription;
  obsListSchoolInfoError: Observable<any>;
  subObsListSchoolInfoError: Subscription;

  isTopupEnabled: boolean = true;
  studentAvailabilityCheck: StudentAvailabilityCheck;
  disableOrders: boolean = true;
  schoolShops = new SchoolShops();

  constructor(private _store: Store<fromRoot.State>, private _route: Router,
    private checkService: ListHomeChecksService, private alertService: AlertService,
    private schoolService: SchoolInfoService) {

    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListParentError = this._store.select(fromRoot.selectListParentInfoFailure);

    this.obsListSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
    this.obsListSchoolInfoError = this._store.select(fromRoot.selectListSchoolInfoFailure);

  }

  ngOnInit() {
    this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(true));
    this._store.dispatch(new SidebarActions.SetSidebarName('commonSidebar'));

    let isOnInit = true;
    this._store.dispatch(new ListParentActions.ListParent());
    //this._store.dispatch(new ListSchoolInfoActions.ListSchoolInfo);

    this.subObsListParent = this.obsListParent.subscribe(parent => {
      if (!isOnInit) {
        if (parent.parentId) {
          this.parent = parent;

        }
      }
    })


    this.subObsListSchoolInfo = this.obsListSchoolInfo.subscribe(result => {
      if (result) {
        this.schoolInfo = result;
        if (this.schoolInfo && this.schoolInfo.topup_method && this.schoolInfo.topup_method.includes('4')) {
          this.isTopupEnabled = true;
        }
        else {
          this.isTopupEnabled = false;
        }
      }
    })


    this.subObsListSchoolInfoError = this.obsListSchoolInfoError.subscribe(error => {

    });

    this.checkService.getStudentAvailabilityMessage().subscribe(check => {
      this.studentAvailabilityCheck = check;
      this.disableOrders = check.disableOrders;
    });

    this.schoolService.getSchoolShops().subscribe(schoolShops => {
      this.schoolShops = schoolShops;
      if (!this.schoolShops.canteenShopName) {
        this.schoolShops.canteenShopName = 'CANTEEN';
      }
      if (!this.schoolShops.uniformShopName) {
        this.schoolShops.uniformShopName = 'UNIFORM';
      }
    });

    isOnInit = false;
  }

  showOrderDisabledMessgae() {
    if (this.studentAvailabilityCheck && this.studentAvailabilityCheck.message) {
      this.alertService.error(this.studentAvailabilityCheck.message);
    }
    else {
      this.disableOrders = false;
    }
  }

  ngOnDestroy() {
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsListSchoolInfo)
      this.subObsListSchoolInfo.unsubscribe();
    if (this.subObsListSchoolInfoError)
      this.subObsListSchoolInfoError.unsubscribe();
  }

}
