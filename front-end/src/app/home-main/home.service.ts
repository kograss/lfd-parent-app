import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { StudentAvailabilityCheck } from './model/StudentAvailabilityCheck';

@Injectable()
export class ListHomeChecksService {

    private _url = "Student/GetStudentAvailabilityMessage";
    
    constructor(private _http: HttpClient) {
    }

    getStudentAvailabilityMessage(): Observable<any> {
        return this._http
            .get(this._url)
            ;
    }
}

