import { StudentClass } from './../model/class';
import { Parent } from './../../user-profile/model/parent';
import { ChildrenComponent } from './../children.component';
import { Children } from './../model/children';
import { Router } from '@angular/router';
import { ChildrenService } from '../children.service';
import { Component, NgModule, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from './../../reducers';
import { Subscription } from 'rxjs';
import * as AddChildrenActions from './../actions/add-children-actions';
import * as SetChildrenActions from './../actions/set-children-action';
import * as ListClassesActions from './../actions/list-studentclasses-actions';
import * as ListParentActions from './../../user-profile/actions/list-user-action';

declare var $: any;
@Component({
  selector: 'add-children',
  templateUrl: './add-children.component.html'
})
export class AddChildrenComponent implements OnInit, OnDestroy {
  @Input()
  mode: string;

  @Output()
  closeModel: EventEmitter<any> = new EventEmitter<any>();

  parent: Parent;
  studentClasses: StudentClass[] = [];
  showSpinner: boolean;

  obsAddChildren: Observable<Children[]>;
  subObsAddChildren: Subscription;
  obsAddChildrenFailure: Observable<any>;
  subObsAddChildrenFailure: Subscription;

  obsSetChildren: Observable<Children>;
  subObsSetChildren: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;
  obsListParentFailure: Observable<any>;
  subObsListParentFailure: Subscription;

  obsListClasses: Observable<StudentClass[]>;
  subObsListClasses: Subscription;
  obsListClassesFailure: Observable<any>;
  subObsListClassesFailure: Subscription;
  dropdownSettings = {};
  selectedItems: any[] = [];

  constructor(private _store: Store<fromRoot.State>,
    public children: Children, private _router: Router) {
    this.obsAddChildren = this._store.select(fromRoot.selectAddChildrenSuccess);
    this.obsSetChildren = this._store.select(fromRoot.setChildren);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsListClasses = this._store.select(fromRoot.selectListClassesSuccess);
    this.obsAddChildrenFailure = this._store.select(fromRoot.selectAddChildrenFailure);
    this.obsListClassesFailure = this._store.select(fromRoot.selectListClassesFailure);
    this.obsListParentFailure = this._store.select(fromRoot.selectListParentInfoFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this.showSpinner = true;
    this._store.dispatch(new ListClassesActions.ListStudentClasses());

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      placeholder: 'Select class',
    //itemsShowLimit: 3,
      allowSearchFilter: true
    };
    

    this.subObsListClasses = this.obsListClasses.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.studentClasses = result;
          if (this.studentClasses && this.studentClasses.length > 0) {
            this.studentClasses.sort((a, b) => a.name.localeCompare(b.name))
          }

          this.selectedItems.pop()
          this.selectedItems = this.studentClasses.filter(cls => cls.name == this.children.studentClass)
        }
        this.showSpinner = false;
      }
    })
    

    if (this.mode != "edit")
      this._store.dispatch(new SetChildrenActions.ClearChild());

    

    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result.parentId) {
        this.parent = result;
      }
    })

    this.subObsListParentFailure = this.obsListParentFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    this.subObsListClassesFailure = this.obsListClassesFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    

    this.subObsAddChildren = this.obsAddChildren.subscribe(result => {
      if (!isOnInit) {
        this.showSpinner = false;
        this.closeModel.emit();
      }
    })

    this.subObsAddChildrenFailure = this.obsAddChildrenFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    this.subObsSetChildren = this.obsSetChildren.subscribe(children => {
      this.children = children;
    })
    isOnInit = false;
  }

  onItemSelect(item: any) {
    
    console.log(this.selectedItems);
    
  }
  OnItemDeSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  saveChildren(children: Children) {
    this.showSpinner = true;
    //Just send the class name
    children.studentClass = this.selectedItems[0]['name']
    children.parentId = this.parent.parentId;
    if (this.mode != "edit") {
      children.status = "ACTIVE";
    }
    this._store.dispatch(new AddChildrenActions.AddChild(children));
  }

  ngOnDestroy() {
    if (this.subObsSetChildren)
      this.subObsSetChildren.unsubscribe();
    if (this.subObsAddChildren)
      this.subObsAddChildren.unsubscribe();
    if (this.subObsAddChildrenFailure)
      this.subObsAddChildrenFailure.unsubscribe();
    if (this.subObsListClasses)
      this.subObsListClasses.unsubscribe();
    if (this.subObsListClassesFailure)
      this.subObsListClassesFailure.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
    if (this.subObsListParentFailure)
      this.subObsListParentFailure.unsubscribe();
  }
}

