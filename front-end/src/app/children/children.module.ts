import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { ChildrenComponent } from './children.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChildrenEffects } from './children-effects';
import { EffectsModule } from '@ngrx/effects';
import { ChildrenService } from './children.service';
import { Children } from './model/children';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { AddChildrenComponent } from './add-children/add-children.component';
import { CommonMenuModule } from '../common/components/common-menu/common-menu.module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        NgMultiSelectDropDownModule,
        FormsModule,
        CommonMenuModule,
        LoadingModule.forRoot({
            fullScreenBackdrop:true
          }),
          
        EffectsModule.forFeature([ChildrenEffects])        
],
declarations: [ ChildrenComponent,AddChildrenComponent ],
exports: [ ChildrenComponent ],
providers: [ ChildrenService, Children ]
})  
export class ChildrenModule{

}
