import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import * as ListChildrenActions from './actions/list-children-actions';
import { Component, NgModule, OnInit, OnDestroy, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import * as AddChildrenActions from './actions/add-children-actions';
import * as SetChildrenActions from './actions/set-children-action';
import * as fromRoot from '../reducers';
import { Children } from './model/children';

declare var $: any;
@Component({
  selector: 'children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit, OnDestroy {

  obsChildrens: Observable<Children[]>;
  subsObsChildrens: Subscription;
  obsChildreFailure: Observable<any>;
  subObsChildrenFailure: Subscription;

  obsAddChildren: Observable<Children[]>;
  subsObsAddChildren: Subscription;
  obsAddChildrenFailure: Observable<any>;
  subObsAddChildrenFailure: Subscription;

  childrens: Children[] = [];
  arrayIndexChanged: number;
  show = false;
  mode: string;
  showSpinner: boolean;

  constructor(private _store: Store<fromRoot.State>, private _route: ActivatedRoute,
    private children: Children) {
    this.obsChildrens = this._store.select(fromRoot.selectListChildrenSuccess);
    this.obsAddChildren = this._store.select(fromRoot.selectAddChildrenSuccess);
    this.obsChildreFailure = this._store.select(fromRoot.selectListChildrenFailure);
    this.obsAddChildrenFailure = this._store.select(fromRoot.selectAddChildrenFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this.subsObsAddChildren = this.obsAddChildren.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.childrens = result;
        }
      }
    })

    this.subObsAddChildrenFailure = this.obsAddChildrenFailure.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })

    if (this.childrens.length == 0) {
      this.showSpinner = true;
      this._store.dispatch(new ListChildrenActions.ListChildrens(false));
    }

    this.subsObsChildrens = this.obsChildrens.subscribe(childrens => {
      if (!isOnInit) {
        if (childrens) {
          this.childrens = childrens;
        }
        this.showSpinner = false;
      }
    })

    this.subObsChildrenFailure = this.obsChildreFailure.subscribe(eror => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  addChildren() {
    this.show = true;
    this.mode = 'add';
  }

  editChildren(children: Children, index: number) {
    this.show = true;
    this.mode = 'edit';
    this.arrayIndexChanged = index;
    this._store.dispatch(new SetChildrenActions.SetChild(children));
  }

  closeModal() {
    this.show = false;
  }

  ngOnDestroy() {
    if (this.subsObsChildrens)
      this.subsObsChildrens.unsubscribe();
    if (this.subsObsAddChildren)
      this.subsObsAddChildren.unsubscribe();
    if (this.subObsAddChildrenFailure)
      this.subObsAddChildrenFailure.unsubscribe();
    if (this.subObsChildrenFailure)
      this.subObsChildrenFailure.unsubscribe();
  }
}
