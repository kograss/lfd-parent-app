import { StudentClass } from './model/class';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Children } from './model/children';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ChildrenService {
  private _getAllChildrensUrl = 'Student/GetStudents/{status}';
  private _getAllClassesUrl = 'Student/GetAllClasses/';  
  private _deleteChildrenUrl = 'Student/DeleteStudent';
  private _updateChildUrl = 'Student/EditStudent/';
  private _addChildUrl = 'Student/AddNew/';
  
  constructor(private _http: HttpClient) { }
  
  addChildren(children: Children) {
    if (children.studentId) {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers };
      let body = JSON.stringify(children);
      return this._http.put(this._updateChildUrl, body, options);
    }
    else {
      let headers = new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        });
      let options = { headers: headers };
      let body = JSON.stringify(children);
      return this._http.post(this._addChildUrl, body, options);
    }
  }

  getAllChildrens(status) {
    return this._http.get(this._getAllChildrensUrl.replace('{status}',status));
  }

  deleteChildren(children:Children) {
    return this._http.delete(this._deleteChildrenUrl +'?id='+children.studentId);
  }

  getStudentClasses() {
    return this._http.get(this._getAllClassesUrl);
  }

}   