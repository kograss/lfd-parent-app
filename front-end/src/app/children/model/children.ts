export class Children {
  studentId: number;
  parentId: number;
  schoolId: number;
  firstName: string;
  lastName: string;
  studentClass: string = 'NONE';
  klass: string;
  classForClassification: string;
  status: string;
  allergy: string;
  studentCardId: string;
  maxAllow: number;
}