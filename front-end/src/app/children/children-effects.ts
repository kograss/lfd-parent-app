

import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as AddChildrenActions from './actions/add-children-actions';
import * as ListChildrenActions from './actions/list-children-actions';
import { ChildrenService } from './children.service';
import * as ListClassesActions from './actions/list-studentclasses-actions';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class ChildrenEffects {

    constructor(
        private actions$: Actions,
        private childrenService: ChildrenService
    ) { }

    @Effect()
    listChildrens$: Observable<Action> = this.actions$.pipe(ofType<ListChildrenActions.ListChildrens>
        (ListChildrenActions.LISTCHILDRENS),
        switchMap(
        payload => this.childrenService.getAllChildrens(payload.status).pipe(
            map(results => new ListChildrenActions.ListChildrensSuccess(results)),
            catchError(err =>
                of(new ListChildrenActions.ListChildrensFailure(err))
            )
        )));

    @Effect()
    addChildren$: Observable<Action> = this.actions$.pipe(ofType<AddChildrenActions.AddChild>(AddChildrenActions.ADDCHILD),
        map(action => action.payload),
        switchMap(
        payload => this.childrenService.addChildren(payload).pipe(
            map(results => new AddChildrenActions.AddChildSuccess(results)),
            catchError(err =>
                of(new AddChildrenActions.AddChildFailure(err))
            )
        )));

        @Effect()
        listStudentClasses$: Observable<Action> = this.actions$.pipe(ofType<ListClassesActions.ListStudentClasses>
            (ListClassesActions.LISTSTUDENTCLASSES)
            //.map(action=>action.payload)
            ,switchMap(
            payload => this.childrenService.getStudentClasses().pipe(
                map(results => new ListClassesActions.ListStudentClassesSuccess(results))
                ,catchError(err =>
                    of({ type: ListClassesActions.LISTSTUDENTCLASSES_FAILURE, exception: err })
                )
            )));

}