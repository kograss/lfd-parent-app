import { Children } from './../model/children';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const ADDCHILD = 'ADD CHILD';
export const ADDCHILD_SUCCESS = 'ADD CHILD SUCCESS';
export const ADDCHILD_FAILURE = 'ADD CHILD FAILURE';


export class AddChild {
    readonly type = ADDCHILD;
    constructor(public payload: Children) {
    }

}

export class AddChildSuccess implements Action {
    readonly type = ADDCHILD_SUCCESS;
    constructor(public childAdded: any) {
        if(this.childAdded == null){
            this.childAdded = [];
        }
    }

}

export class AddChildFailure implements Action {
    readonly type = ADDCHILD_FAILURE;
    constructor(public err: any) {
    }

}


export type All = AddChild
    | AddChildSuccess | AddChildFailure 