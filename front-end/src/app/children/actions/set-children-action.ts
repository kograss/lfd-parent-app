import { Action } from '@ngrx/store';
import { Children } from './../model/children';
export const CHILD_CLEAR = 'CLEAR CHILD';
export const CHILD_SET = 'SET CHILD';


export class ClearChild implements Action {
    readonly type = CHILD_CLEAR;
    constructor() {
    }

}


export class SetChild implements Action {
    readonly type = CHILD_SET;
    constructor(public payload: Children) {
    }

}

export type All = ClearChild|SetChild
