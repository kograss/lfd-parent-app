import { StudentClass } from './../model/class';
import { Action } from '@ngrx/store';



export const LISTSTUDENTCLASSES = '[STUDENTCLASSES] LIST';
export const LISTSTUDENTCLASSES_SUCCESS = '[STUDENTCLASSES] LIST SUCCESS';
export const LISTSTUDENTCLASSES_FAILURE = '[STUDENTCLASSES] LIST FAILURE';


export class ListStudentClasses {

    readonly type = LISTSTUDENTCLASSES;
    constructor() {
    }
}

export class ListStudentClassesSuccess implements Action {

    readonly type = LISTSTUDENTCLASSES_SUCCESS;
    constructor(public studentClasses) {
        if(this.studentClasses == null){
            this.studentClasses = [];
        }
    }

}

export class ListStudentClassesFailure implements Action {

    readonly type = LISTSTUDENTCLASSES_FAILURE;
    constructor(public err: any) {

    }

}


export type All = ListStudentClasses
    | ListStudentClassesSuccess | ListStudentClassesFailure 