import { Children } from '../model/children';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';



export const LISTCHILDRENS = '[CHILDREN] LIST';
export const LISTCHILDRENS_SUCCESS = '[CHILDREN] LIST SUCCESS';
export const LISTCHILDRENS_FAILURE = '[CHILDREN] LIST FAILURE';


export class ListChildrens {

    readonly type = LISTCHILDRENS;
    constructor(public status:boolean) {
    }
}

export class ListChildrensSuccess implements Action {

    readonly type = LISTCHILDRENS_SUCCESS;
    constructor(public childrensList) {
        if(this.childrensList == null){
            this.childrensList = [];
        }
    }

}

export class ListChildrensFailure implements Action {

    readonly type = LISTCHILDRENS_FAILURE;
    constructor(public err: any) {

    }

}


export type All = ListChildrens
    | ListChildrensSuccess | ListChildrensFailure 