import { Children } from './../model/children';
import * as ChildrenActions from './../actions/list-children-actions'

export interface State {    
   
    result: Children[],
    err: any
}

const initialState: State = {    
   
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ChildrenActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ChildrenActions.LISTCHILDRENS_SUCCESS: {
            return {
                ...state,
                result: action.childrensList
            }
        }
        case ChildrenActions.LISTCHILDRENS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
