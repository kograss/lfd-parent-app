import * as SetChildrenActions from '../actions/set-children-action'
import { Children } from '../model/children';

export interface State {
    children: Children,
}

const initialState: State = {
    children: new Children(),
};

export function reducer(state = initialState, action: SetChildrenActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case SetChildrenActions.CHILD_CLEAR: {
            return {
                children: new Children(),
            }
        }
        case SetChildrenActions.CHILD_SET: {
            return {
                ...state,
                children: action.payload
            }
        }

        default: {
            return state;
        }

    }

}
