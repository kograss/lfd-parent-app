import * as AddChildrenActions from '../actions/add-children-actions'
import { Children } from '../model/children';

export interface State {
    result: Children[],
    err: any
}

const initialState: State = {
    result: [],
    err: {}
};

export function reducer(state = initialState, action: AddChildrenActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case AddChildrenActions.ADDCHILD_SUCCESS: {
            return {
                ...state,
                result: action.childAdded
            }
        }
        case AddChildrenActions.ADDCHILD_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }

    }

}
