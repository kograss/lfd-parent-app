import { StudentClass } from './../model/class';
import * as ListStudentClassesActions from './../actions/list-studentclasses-actions'

export interface State {    
   
    result: StudentClass[],
    err: any
}

const initialState: State = {    
   
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListStudentClassesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListStudentClassesActions.LISTSTUDENTCLASSES_SUCCESS: {
            return {
                ...state,
                result: action.studentClasses
            }
        }
        case ListStudentClassesActions.LISTSTUDENTCLASSES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
