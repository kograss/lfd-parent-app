import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';

import { StripeComponent } from './stripe.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    CommonModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true
    }),
  ],
  declarations: [StripeComponent],
  exports: [StripeComponent],
  providers: []
})
export class StripeModule { }
