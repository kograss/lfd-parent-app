
export const STRIPEPOPUPCLOSED = 'STRIPE POPUP CLOSE';
export const STRIPEPOPUPOPEN = 'STRIPE POPUP OPEN';

export class StripePopupClosed {
    readonly type = STRIPEPOPUPCLOSED;
    constructor() {
    }
}

export class StripePopupOpen {
    readonly type = STRIPEPOPUPOPEN;
    constructor() {
    }
}

export type All = StripePopupClosed | StripePopupOpen