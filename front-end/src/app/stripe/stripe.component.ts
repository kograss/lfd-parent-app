import { Store } from '@ngrx/store';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import * as fromRoot from './../reducers';
import * as StripePopUpCloseActions from './actions/stripe-popup.action';
import { NgForm } from '@angular/forms';

declare var Stripe;

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.css']
})
export class StripeComponent implements OnInit {
  @ViewChild('cardInfo') cardInfo: ElementRef;
  @Input('paymentDetails') paymentDetails: any;
  @Input('paymentIntent') paymentIntent: any;
  @Input('customerStripeDetails') customerStripeDetails: any;
  @Output('closeModal') closeModalEmitter: EventEmitter<any> = new EventEmitter();
  @Output('stripeCallback') stripeCallbackEmitter: EventEmitter<any> = new EventEmitter();

  card: any;
  cardCvC:any;
  cardHandler = this.onChange.bind(this);
  error: string;
  stripe: any;
  elements: any;
  isSubmitDisabled: boolean = false;
  isCvc: boolean = false;

  constructor(public _store: Store<fromRoot.State>, private cd: ChangeDetectorRef) { }
  ngOnInit() {
    this._store.dispatch(new StripePopUpCloseActions.StripePopupOpen);
  }

  useAnotherCard(){
    this.unMountCvc();
    this.mountCard();
  }

  openCheckout(productName: string, amount: number, email, tokenCallback) {
   

  }

  closeModal() {
    this._store.dispatch(new StripePopUpCloseActions.StripePopupClosed);
    this.closeModalEmitter.emit();
  }

  mountCard(){
    this.isCvc = false;
    this.card = this.elements.create('card', { hidePostalCode: true });
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);
  }

  mountCvc(){
    this.isCvc = true;
    this.cardCvC = this.elements.create('cardCvc',);
    this.cardCvC.mount(this.cardInfo.nativeElement);
    this.cardCvC.addEventListener('change', this.cardHandler);
  }

  
  ngAfterViewInit() {
    
    this.stripe = Stripe('pk_test_51NutyDKMs9a5GaqIjHnY6BmKOPerIb91MQdsXhFojrwpo8JIz9JaLx4te7CK6o2GHrhelaj4lA7Nx1jSPGLmUW2n00BTadpeJO'); 
    this.elements = this.stripe.elements();
    if(this.paymentIntent != null){
      this.mountCvc()
    }else{
      this.mountCard()
    }
  }

  unMountCard(){
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  unMountCvc(){
    this.cardCvC.removeEventListener('change', this.cardHandler);
    this.cardCvC.destroy();
  }

  ngOnDestroy() {
    if(this.isCvc){
      this.unMountCvc()
    }else{
      this.unMountCard()
    }
  }

  onChange({ error }) {
    console.log('error', error)
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  onSubmit(form: NgForm) {
    if(this.isCvc){
      this.verifyCvcAndPay()
    }else{
      this.payFirstTime()
    }
  }
  
  async payFirstTime() {
    this.isSubmitDisabled = true;
    const { token, error } = await this.stripe.createToken(this.card);
    this.isSubmitDisabled = false;
    if (error) {
      console.log('Something went wrong:', error);
      this.error = error.message;
    } else if (token) {
      console.log('Success!', token);
      this.stripeCallbackEmitter.emit(token);
    }
  }
  
  
  async verifyCvcAndPay() {
    this.isSubmitDisabled = true;
    let isSubmitDisabled = this.isSubmitDisabled;
    let stripeCallbackEmitter = this.stripeCallbackEmitter;
    await this.stripe.confirmCardPayment(
      this.paymentIntent.client_secret,
      {
        payment_method: this.paymentIntent.payment_method,
        payment_method_options: {card: {
          cvc: this.cardCvC
        }},
      }
    ).then(function(result) {
      isSubmitDisabled = false;
      if (result.error) {
        // Display error.message in your UI.
        console.log('Something went wrong:', result.error.message)
        this.error = result.error.message;
      } else {
        // The payment has succeeded
        // Display a success message cus_Oi41yanhrW0eUg
        console.log("success");
        stripeCallbackEmitter.emit(null);
      }
    });
    
  }
}



// async onSubmit(form: NgForm) {
  //   this.isSubmitDisabled = true;
  //   await this.stripe.confirmCardPayment(
  //     this.paymentIntent.client_secret,
  //     {
  //       payment_method: {card: this.card,
  //       billing_details:{
  //         name: 'Fabien IT'
  //       }},
  //       setup_future_usage: 'off_session'
        
  //     }
  //   ).then(function(result) {
  //     if (result.error) {
  //       // Display error.message in your UI.
  //       console.log(result.error.message)
  //     } else {
  //       // The payment has succeeded
  //       // Display a success message cus_Oi41yanhrW0eUg
  //       console.log("success")
  //     }
  //   });
  //   this.isSubmitDisabled = false;
  //   // if (error) {
  //   //   console.log('Something went wrong:', error);
  //   //   this.error = error.message;
  //   // } else if (token) {
  //   //   console.log('Success!', token);
  //   //   this.stripeCallbackEmitter.emit(token);
  //   // }
  // }