import * as StripeActions from './../actions/stripe-popup.action';

export interface State {
    stripePopUpClosed:boolean;
}

const initialState: State = {
    stripePopUpClosed : false
}

export function reducer(state = initialState, action: StripeActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case StripeActions.STRIPEPOPUPCLOSED: {
            return {
                ...state,
                stripePopUpClosed: true
            }
        }

        case StripeActions.STRIPEPOPUPOPEN: {
            return {
                ...state,
                stripePopUpClosed: false
            }
        }

        default: {
            return state;
        }

    }

}
