import { DatePickerComponent } from './date-picker.component';
import { LoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
    MyDatePickerModule,
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    FormsModule,
],
  declarations: [DatePickerComponent],
  exports: [DatePickerComponent],
  providers: []
})
export class DatePickerModule { }
