import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMyDpOptions, IMyOptions, IMyDateModel } from 'mydatepicker';

@Component({
  selector: 'date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {
  public date: Date;

  @Input()
  options:IMyDpOptions;

  @Output()
  selectedDate:EventEmitter<any>= new EventEmitter<any>();

  constructor() { }

  public dateOptions: IMyDpOptions = {
  };
  ngOnInit() {
  }

  onDateChanged(event: IMyDateModel) {
    console.log(event)
    this.selectedDate.emit(event);
  }
}
