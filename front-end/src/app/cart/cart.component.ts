import { Config } from './../config/config';
import { Router } from '@angular/router';
import { DeleteCartModel } from './model/deleteCartModel';
import { CartResult } from './model/cartResult';
import { CanteenShopCart } from './model/canteenShopCart';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy, EventEmitter, Output, ElementRef } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListCartItemsActions from './actions/list-cart-items-action';
import * as DeleteCartItemActions from './actions/delete-cart-item-action';
import * as ShowHideCartActions from './actions/show-hide-cart-modal-action';
import * as PaymentActions from '../checkout/actions/checkout-actions';
import * as ShowSpinnerActions from './../home/actions/start-cart-spinner.action';
import * as ShowHideSidebarActions from './../sidebar/actions/show-hide-sidebar.actions';
/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  host: {
    '(document:click)': 'onClick($event)',
  },
  moduleId: module.id,
  selector: 'cart',
  templateUrl: 'cart.component.html',
  styleUrls: ['cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
  @Output()
  hideCartBar: EventEmitter<any> = new EventEmitter();

  cart: CartResult[] = [];
  deliveryDate = localStorage.getItem('deliveryDate');
  isCartEmpty: boolean = false;
  showSpinner: boolean;
  currency: string;
  retrievingMsg = 'Updating your cart, please wait...';
  isCartOpen = false;
  grandTotal: Observable<number>;
  isFromCartSpinner: boolean;
  orderType: string = localStorage.getItem('orderType');
  eventId: number = +localStorage.getItem('eventId');
  isSetPrice = +localStorage.getItem('isSetPrice');
  
  obsListCartItems: Observable<CartResult[]>;
  subObsListCartItems: Subscription;

  obsListCartItemsError: Observable<any>;
  subObsListCartItemsError: Subscription;

  obsDeleteCartItemError: Observable<any>;
  subObsDeleteCartItemError: Subscription;

  obsShowSpinner: Observable<boolean>;
  subObsShowSpinner: Subscription;

  constructor(private _store: Store<fromRoot.State>, private _route: Router,
    private config: Config, private _eref: ElementRef) {
    this.obsListCartItems = this._store.select(fromRoot.selectListCartItemsSuccess);
    this.grandTotal = this._store.select(fromRoot.selectGrandTotal);
    this.obsListCartItemsError = this._store.select(fromRoot.selectListCartItemsFailure);
    this.obsDeleteCartItemError = this._store.select(fromRoot.selectDeleteCartItemsFailure);
    this.obsShowSpinner = this._store.select(fromRoot.selectShowSpinner);
    this.currency = this.config.getCurrency();
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new PaymentActions.ClearOrderResult());
    if (!this.deliveryDate || this.deliveryDate == null || this.deliveryDate.trim() == '') {
      this._route.navigate(['canteenorder/dashboard/home']);
      this.isCartEmpty = true;
      this.retrievingMsg = 'Your Cart is Empty..'
      return;
    }
    else {
      if (this.cart.length == 0) {
        this.showSpinner = true;
        this.isFromCartSpinner = true;
        this._store.dispatch(new ListCartItemsActions.ListCartItems(this.deliveryDate, this.eventId));
      }
    }

    /* this.subObsShowSpinner = this.obsShowSpinner.subscribe(result => {
       if (!isOnInit) {
         if (result) {
           this.showSpinner = true;
           this.isFromCartSpinner = true;
           this._store.dispatch(new ShowSpinnerActions.StopCartSpinner);
         }
       }
     })
 */
    this.subObsListCartItems = this.obsListCartItems.subscribe(result => {
      if (result.length > 0) {
        this.cart = result;
        this.isCartEmpty = false;
        //console.log(this.cart);
      }
      else {
        this.cart = [];
        this.retrievingMsg = 'Your Cart is Empty..';
        this.isCartEmpty = true;
      }
      if (this.isFromCartSpinner && !isOnInit) {
        this.showSpinner = false;
        this.isFromCartSpinner = false;
      }
    })

    this.subObsListCartItemsError = this.obsListCartItemsError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })


    this.subObsDeleteCartItemError = this.obsDeleteCartItemError.subscribe(error => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) // or some similar check
      if (this.isCartOpen) {
        this.hideCartBar.emit();
        this.isCartOpen = false;
      }
      else {
        this.isCartOpen = true;
      }
  }
  deleteCartItem(cartId: number, deleteProduct: boolean, status: string) {
    this.isFromCartSpinner = true;
    var deleteCartItem = new DeleteCartModel();
    deleteCartItem.cartId = cartId;
    deleteCartItem.deleteProduct = deleteProduct;
    deleteCartItem.deliveryDate = this.deliveryDate;
    deleteCartItem.status = status;
    if (this.orderType == 'event') {
      deleteCartItem.eventId = this.eventId;
    }
    this.showSpinner = true;
    this._store.dispatch(new DeleteCartItemActions.DeleteCartItem(deleteCartItem));
  }

  checkOut() {
    this.hideCartBarComponent();

    var orderType = localStorage.getItem('orderType');
    if (orderType == 'event') {
      this._store.dispatch(new ShowHideSidebarActions.ShowHideSidebar(false));
    }
    this._route.navigate(['canteenorder/dashboard/checkout']);
  }

  hideCartBarComponent() {
    this.hideCartBar.emit();
  }

  ngOnDestroy() {
    if (this.subObsListCartItems)
      this.subObsListCartItems.unsubscribe();
    if (this.subObsListCartItemsError)
      this.subObsListCartItemsError.unsubscribe();
    if (this.subObsDeleteCartItemError)
      this.subObsDeleteCartItemError.unsubscribe();
    if (this.subObsShowSpinner)
      this.subObsShowSpinner.unsubscribe();
  }
}
