import { Observable} from 'rxjs';
import { Store } from '@ngrx/store';
import { DeleteCartModel } from './model/deleteCartModel';
import { AddToCartModel } from './model/addToCart';
import { Product } from './../products/model/product';
import { CartResult } from './model/cartResult';
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class CartService {
    private _addToCarturl = "addToCart";
    private _addToCanteenEventCarturl = "events/addToCart";
    private _addToSchoolEventCarturl = "SchoolEvent/addToCart";
    private _getUrl = "getCart/{deliveryDate}";
    private _getCanteenEventCartUrl = "Events/GetParentCart/{eventId}/";
    private _getSchoolEventCartUrl = "SchoolEvent/GetParentCart/{eventId}/";
    private _deleteUrl = "Cart/Delete";
    private _deleteFromCanteenEventCartUrl = "Events/UpdateCart";
    private _deleteFromSchoolEventCartUrl = "SchoolEvent/UpdateCart";
    eventType: string;
    constructor(private _http: HttpClient) {

    }

    getCart(deliveryDate: string, eventId: number) {
        let orderType: string = localStorage.getItem('orderType');
        this.eventType = localStorage.getItem('eventType');
        
        if (orderType == 'event' && eventId) {
            if (this.eventType == 'CANTEEN') {
                return this._http.get(this._getCanteenEventCartUrl.replace("{eventId}", eventId.toString()));
            }
            else if (this.eventType == 'SCHOOL') {
                return this._http.get(this._getSchoolEventCartUrl
                    .replace("{eventId}", eventId.toString()))
                    ;
            }

        }
        else
            return this._http
                .get(this._getUrl
                    .replace("{deliveryDate}", deliveryDate)
                )
                ;
    }

    addToCart(cartItem: AddToCartModel) {
        this.eventType = localStorage.getItem('eventType');
        let headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            });
        let options = { headers: headers };
        let body = JSON.stringify(cartItem);
        if (cartItem.eventId) {
            if (this.eventType == 'CANTEEN') {
                return this._http
                    .post(this._addToCanteenEventCarturl, body, options)
                    ;
            }
            else if (this.eventType == 'SCHOOL') {
                return this._http
                    .post(this._addToSchoolEventCarturl, body, options)
                    ;
            }

        }
        else {
            return this._http
                .post(this._addToCarturl, body, options)
                ;
        }
    }

    deleteCartItem(cartItem: DeleteCartModel) {
        this.eventType = localStorage.getItem('eventType');        
        let headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            });
        let body = JSON.stringify(cartItem);
        let options = { headers: headers, body: body };
        if (cartItem.eventId) {
            if (this.eventType == 'CANTEEN') {
                return this._http.delete(this._deleteFromCanteenEventCartUrl, options);
            }
            else if (this.eventType == 'SCHOOL') {
                return this._http.delete(this._deleteFromSchoolEventCartUrl, options);
            }
        }
        else
            return this._http.delete(this._deleteUrl, options);
    }
}