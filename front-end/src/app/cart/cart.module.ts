import { LoadingModule } from 'ngx-loading';
import { CartService } from './cart.service';
import { CartEffects } from './cart-effects';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { CartRoutingModule } from './cart-routing.module';

@NgModule({
  imports: [CommonModule, CartRoutingModule, 
    EffectsModule.forFeature([CartEffects]),
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),],
  declarations: [CartComponent],
  exports: [CartComponent],
  providers: [CartService]
})
export class CartModule { }
