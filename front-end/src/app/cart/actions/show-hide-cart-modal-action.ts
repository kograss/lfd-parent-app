import { Action } from '@ngrx/store';

export const HIDECART = 'HIDE CART';
export const SHOWCART = 'SHOW CART';
export const CHECKOUTBUTTONCLICKEDTRUE = "CHECKOUT BUTTON CLICK TRUE";
export const CHECKOUTBUTTONCLICKEDFALSE = "CHECKOUT BUTTON CLICK FALSE";

export class HideCart {
    readonly type = HIDECART;
    constructor() {  
    
    }
}

export class ShowCart {
    readonly type = SHOWCART;
    constructor() {  
    
    }
}

export class CheckoutButtonClickTrue {
    readonly type = CHECKOUTBUTTONCLICKEDTRUE;
    constructor() {  
    
    }
}

export class CheckoutButtonClickFalse {
    readonly type = CHECKOUTBUTTONCLICKEDFALSE;
    constructor() {  
    
    }
}

export type All = HideCart | ShowCart
|CheckoutButtonClickTrue | CheckoutButtonClickFalse