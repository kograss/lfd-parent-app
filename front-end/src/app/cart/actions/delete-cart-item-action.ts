import { DeleteCartModel } from './../model/deleteCartModel';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const DELETECARTITEM = 'DELETE CART ITEM';
export const DELETECARTITEM_SUCCESS = 'DELETE CART ITEM SUCCESS';
export const DELETECARTITEM_FAILURE = 'DELETE CART ITEM FAILURE';



export class DeleteCartItem {
    readonly type = DELETECARTITEM;
    constructor(public payload: DeleteCartModel) {
    }
}


export class DeleteCartItemSuccess implements Action {
    readonly type = DELETECARTITEM_SUCCESS;
    constructor(public deleteCartItem: string) { }
}

export class DeleteCartItemFailure implements Action {
    readonly type = DELETECARTITEM_FAILURE;
    constructor(public err: any) { }
}


export type All =  DeleteCartItem
    | DeleteCartItemSuccess | DeleteCartItemFailure