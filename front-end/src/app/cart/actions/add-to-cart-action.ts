import { AddToCartResult } from './../model/addToCartResult';
import { AddToCartModel } from './../model/addToCart';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const ADDTOCART = 'ADD TO CART';
export const ADDTOCART_SUCCESS = 'ADD TO CART SUCCESS';
export const ADDTOCART_FAILURE = 'ADD TO CART FAILURE';



export class AddToCart {
    readonly type = ADDTOCART;
    constructor(public payload: AddToCartModel) {
    }

}

export class AddToCartSuccess implements Action {
    readonly type = ADDTOCART_SUCCESS;
    constructor(public addToCartResult: any) {
    }
}

export class AddToCartFailure implements Action {
    readonly type = ADDTOCART_FAILURE;
    constructor(public err: any) {
    }

}


export type All = AddToCart
    | AddToCartSuccess | AddToCartFailure
