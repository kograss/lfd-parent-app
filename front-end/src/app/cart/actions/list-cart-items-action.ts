import { CartResult } from './../model/cartResult';
import { CanteenShopCart } from './../model/canteenShopCart';
import { Action } from '@ngrx/store';

export const LISTCARTITEMS = '[CARTITEMS] LIST';
export const LISTCARTITEMS_SUCCESS = '[CARTITEMS] LIST SUCCESS';
export const LISTCARTITEMS_FAILURE = '[CARTITEMS] LIST FAILURE';
export const SETGRANDTOTAL = 'SET GRAND TOTAL';
export const CLEARGRANDTOTAL = 'CLEAR GRAND TOTAL';
export const SETCARTCOUNT = 'SET CART COUNT';
export const CLEARCARTCOUNT = 'CLEAR CART COUNT';

export class ListCartItems {
    readonly type = LISTCARTITEMS;
    constructor(public deliveryDate: string, public eventId: number) {
    }
}

export class ListCartItemsSuccess implements Action {

    readonly type = LISTCARTITEMS_SUCCESS;
    constructor(public cartItems: any) {
        if (this.cartItems == null) {
            this.cartItems = [];
        }
    }

}

export class ListCartItemsFailure implements Action {

    readonly type = LISTCARTITEMS_FAILURE;
    constructor(public err: any) {

    }
}

export class SetGrandTotal implements Action {

    readonly type = SETGRANDTOTAL;
    constructor(public total: number) {

    }
}

export class ClearGrandTotal implements Action {

    readonly type = CLEARGRANDTOTAL;
    constructor() {

    }
}

export class SetCartCount implements Action {

    readonly type = SETCARTCOUNT;
    constructor(public count: number) {

    }
}

export class ClearCartCount implements Action {

    readonly type = CLEARCARTCOUNT;
    constructor() {

    }
}

export type All = ListCartItems
    | ListCartItemsSuccess | ListCartItemsFailure | SetGrandTotal
    | SetCartCount | ClearCartCount | ClearGrandTotal