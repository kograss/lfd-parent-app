import { CartService } from './cart.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListCartItemsActions from './actions/list-cart-items-action';
import * as DeleteCartItemActions from './actions/delete-cart-item-action';
import * as AddToCartActions from './actions/add-to-cart-action';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class CartEffects {

    constructor(
        private actions$: Actions,
        private cartService: CartService
    ) { }

    @Effect()
    listCartItems$: Observable<Action> = this.actions$.pipe(ofType<ListCartItemsActions.ListCartItems>
        (ListCartItemsActions.LISTCARTITEMS),
        switchMap(
        payload => this.cartService.getCart(payload.deliveryDate, payload.eventId).pipe(
            map(results => new ListCartItemsActions.ListCartItemsSuccess(results)),
            catchError(err =>
                of(new ListCartItemsActions.ListCartItemsFailure(err))
            ))
        ));

    @Effect()
    deleteCartItem$: Observable<Action> = this.actions$.pipe(ofType<DeleteCartItemActions.DeleteCartItem>
        (DeleteCartItemActions.DELETECARTITEM),
        switchMap(
        payload => this.cartService.deleteCartItem(payload.payload).pipe(
            map(results => new ListCartItemsActions.ListCartItemsSuccess(results)),
            catchError(err =>
                of({ type: DeleteCartItemActions.DELETECARTITEM_FAILURE, exception: err })
            ))
        ));

    @Effect()
    addToCart$: Observable<Action> = this.actions$.pipe(ofType<AddToCartActions.AddToCart>
        (AddToCartActions.ADDTOCART),
        switchMap(
        payload => this.cartService.addToCart(payload.payload).pipe(
            map(results => new AddToCartActions.AddToCartSuccess(results)),
            catchError(err =>
                of({ type: AddToCartActions.ADDTOCART_FAILURE, exception: err })
            ))
        ));

}