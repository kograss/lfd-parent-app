import { CartResult } from './../model/cartResult';
import * as ShowHideCartAction from './../actions/show-hide-cart-modal-action';
export interface State {
    showCart: boolean,
    checkoutButtonClicked: boolean
}

const initialState: State = {
    showCart: false,
    checkoutButtonClicked: false
}

export function reducer(state = initialState, action: ShowHideCartAction.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case ShowHideCartAction.HIDECART: {
            return {
                ...state,
                showCart: false
            }
        }

        case ShowHideCartAction.SHOWCART: {
            return {
                ...state,
                showCart: true
            }
        }

        case ShowHideCartAction.CHECKOUTBUTTONCLICKEDFALSE: {
            return {
                ...state,
                checkoutButtonClicked: false
            }
        }

        case ShowHideCartAction.CHECKOUTBUTTONCLICKEDTRUE: {
            return {
                ...state,
                checkoutButtonClicked: true
            }
        }
        default: {
            return state;
        }

    }

}
