import * as DeleteCartItemActions from './../actions/delete-cart-item-action';
export interface State {    
   
    result: string,
    err: any
}

const initialState: State = {    
   
    result:'',
    err: {}
};

export function reducer(state = initialState, action: DeleteCartItemActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case DeleteCartItemActions.DELETECARTITEM_SUCCESS: {
            return {
                ...state,
                result: action.deleteCartItem
            }
        }
        case DeleteCartItemActions.DELETECARTITEM_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }

    }

}
