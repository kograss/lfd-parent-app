import { AddToCartResult } from './../model/addToCartResult';
import * as AddToCartActions from './../actions/add-to-cart-action';

export interface State {
    result: AddToCartResult,
    err: any
}

const initialState: State = {
    result:new AddToCartResult(),
    err: {}
};

export function reducer(state = initialState, action: AddToCartActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case AddToCartActions.ADDTOCART_SUCCESS: {
            return {
                ...state,
                result: action.addToCartResult
            }
        }
        case AddToCartActions.ADDTOCART_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }


        default: {
            return state;
        }

    }

}
