import { CartResult } from './../model/cartResult';
import * as ListCartItemsActions from './../actions/list-cart-items-action';
export interface State{
    result : CartResult[],
    err:any,
    grandTotal:number;
    count:number;
}

const initialState: State = {
    result : [],
    err : {},
    grandTotal:0,
    count:0
}

export function reducer(state = initialState, action: ListCartItemsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListCartItemsActions.LISTCARTITEMS_SUCCESS: {
            return {
                ...state,
                result: action.cartItems
            }
        }
        case ListCartItemsActions.LISTCARTITEMS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
        
        case  ListCartItemsActions.SETGRANDTOTAL: {
            return {
                ...state,
                grandTotal: action.total
            }
        }   

        case  ListCartItemsActions.CLEARGRANDTOTAL: {
            return {
                ...state,
                grandTotal: 0
            }
        }   
        
        case  ListCartItemsActions.SETCARTCOUNT: {
            return {
                ...state,
                count: action.count
            }
        }   

        case  ListCartItemsActions.CLEARCARTCOUNT: {
            return {
                ...state,
                count: 0
            }
        }   

        default: {
            return state;
        }

    }

}
