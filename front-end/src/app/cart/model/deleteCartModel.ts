export class DeleteCartModel{
    schoolId:number;
    parentId:number;
    cartId:number;
    deleteProduct:boolean;
    deliveryDate:string;
    status:string; 
    eventId:number;
    /*
        status:
            when we want to empty cart: value should be: CART
            when want to delete product from cart value should be: PRODUCT
            when want to reduce quantity of product value shoudl be:  QUANTITY
    */
    /*
        there is button for empty cart
        when current quantity is one then we have to delete the product
        when current quantity is greater than one then reduce quantity by one
    */
}