import { CanteenShopCart } from './../../cart/model/canteenShopCart';

export class CartResult {
    studentId: number;
    studentName: string;
    canteenShopcarts: CanteenShopCart[];
    comment: string;
    reason: string;
    paperBags: number;
    bagsTotalPrice: number = 0;
    serviceCharge: number = 0;
    bagType: string;
}