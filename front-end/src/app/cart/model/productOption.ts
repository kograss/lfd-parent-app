export class ProductOption{
    optionId:number;
    optionName:string;
    optionPrice:number;
}