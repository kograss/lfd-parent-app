export class AddToCartResult {
    isQuanityExceeded: boolean;
    quantityExceedProducts: QuantityExceedProduct[] = [];
    isPriceZeroError: boolean;
    cartCount:number;
    priceZeroProducts: PriceZeroProduct[] = [];
}

export class QuantityExceedProduct {
    productId: number;
    productName: string;
    maximumQuantityAllowed: number;
    exceededQuantity: number;
    studentName: string;
}

export class PriceZeroProduct {
    productId: number;
    productName: string;
    maximumQuantityAllowed: number;
    exceededQuantity: number;
}