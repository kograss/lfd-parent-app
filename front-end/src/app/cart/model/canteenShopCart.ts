import { ProductOption } from './productOption';
export class CanteenShopCart {
    id: number;
    parentId: number;
    studentId: number;
    productId: number;
    productName: string;
    productPrice:number;
    productQuantity:number;  
    schoolId:number;    
    mealName: string;
    cartDeliveryDay:number;
    cartDate:Date;
    cartDeliveryDate:Date; 
    options:ProductOption[];
    mealNameToDisplayInCart:string;
    productPriceWithOptionPrice:number;
    isProductNotAvailable:boolean;
}