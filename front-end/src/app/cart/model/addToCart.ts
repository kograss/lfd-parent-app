export class AddToCartModel {
    schoolId:number;    
    parentId:number;
    categoryId:number;
    productId:number;
    productName:string;
    studentIds:number[]=[];
    quantity:number;
    optionIds:string;  
    deliveryDate:string; 
    mealName:string;
    eventId:number;
}