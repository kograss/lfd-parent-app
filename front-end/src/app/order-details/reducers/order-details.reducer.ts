import { OrderItem } from './../model/order-item';
import * as ListOrderDetailsActions from '../actions/order-details.action';

export interface State{
    result : OrderItem[]
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListOrderDetailsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListOrderDetailsActions.LISTORDERDETAILS_SUCCESS: {
            return {
                ...state,
                result: action.order
            }
        }
        case ListOrderDetailsActions.LISTORDERDETAILS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
