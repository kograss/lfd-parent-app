import { OrderDetailsService } from './order-details.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListOrderDetailsActions from './actions/order-details.action';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class OrderDetailsEffects {

    constructor(
        private actions$: Actions,
        private listOrderDetailsService: OrderDetailsService
    ) { }

    @Effect()
    listOrderDetails$: Observable<Action> = this.actions$.pipe(ofType<ListOrderDetailsActions.ListOrderDetails>
        (ListOrderDetailsActions.LISTORDERDETAILS)
        ,map(action=>action)
        ,switchMap(
        payload => this.listOrderDetailsService.getOrderDetails(payload.order).pipe(
            map(results => new ListOrderDetailsActions.ListOrderDetailsSuccess(results)),
            catchError(err =>
                of({ type: ListOrderDetailsActions.LISTORDERDETAILS_FAILURE, exception: err })
            )))
        );
}