import { ParentOrder } from './../order-history/model/parent-order';
import { OrderItem } from './model/order-item';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class OrderDetailsService {

    private _url = "OrderHistory/GetOrderDetails/{orderId}/{isSpecialOrder}/{eventType}";

    constructor(private _http: HttpClient) {
    }

    getOrderDetails(order: ParentOrder): Observable<any> {
        return this._http
            .get(this._url.replace("{orderId}", order.orderId.toString()).replace("{isSpecialOrder}", order.isSpecialOrder + "", ).replace("{eventType}", order.eventType))
            ;
    }
}

