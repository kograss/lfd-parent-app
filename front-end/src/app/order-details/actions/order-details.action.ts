import { ParentOrder } from './../../order-history/model/parent-order';
import { OrderItem } from './../model/order-item';
import { Action } from '@ngrx/store';

export const LISTORDERDETAILS = '[ORDERDETAILS] LIST';
export const LISTORDERDETAILS_SUCCESS = '[ORDERDETAILS] LIST SUCCESS';
export const LISTORDERDETAILS_FAILURE = '[ORDERDETAILS] LIST FAILURE';

export class ListOrderDetails {
    readonly type = LISTORDERDETAILS;
    constructor(public order:ParentOrder) {
    }
}

export class ListOrderDetailsSuccess implements Action {

    readonly type = LISTORDERDETAILS_SUCCESS;
    constructor(public order:OrderItem[] ) {
        if(this.order==null){
            this.order = [];
        }
    }

}

export class ListOrderDetailsFailure implements Action {

    readonly type = LISTORDERDETAILS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListOrderDetails
    | ListOrderDetailsSuccess | ListOrderDetailsFailure 