import { Config } from './../config/config';
import { ParentOrder } from './../order-history/model/parent-order';
import { OrderItem } from './model/order-item';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';;
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListOrderDetailsActions from './actions/order-details.action';

@Component({
  selector: 'order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  @Input()
  order: ParentOrder;
  @Output()
  stopSpinner: EventEmitter<any> = new EventEmitter();
  currency: string;
  orderItems: OrderItem[];

  obsOrderDetails: Observable<OrderItem[]>;
  subObsOrderDetails: Subscription;
  obsOrderDetailsError: Observable<any>;
  subObsOrderDetailsError: Subscription;

  constructor(private _store: Store<fromRoot.State>, private config: Config) {
    this.obsOrderDetails = this._store.select(fromRoot.selectListOrderDetailsSuccess);
    this.obsOrderDetailsError = this._store.select(fromRoot.selectListOrderDetailsFailure);
    this.currency = this.config.getCurrency();
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new ListOrderDetailsActions.ListOrderDetails(this.order));
    this.subObsOrderDetails = this.obsOrderDetails.subscribe(result => {
      if (!isOnInit) {
        this.orderItems = [];
        if (result) {
          this.orderItems = result;
          //console.log(this.orderItems);
          this.stopSpinner.emit(); 

        }
        else {
          this.stopSpinner.emit();
        }
      }
    })

    this.subObsOrderDetailsError = this.obsOrderDetailsError.subscribe(error => {
      if (!isOnInit)
        this.stopSpinner.emit();
    })
    isOnInit = false;
  }

  ngOnDestroy() {
    if (this.subObsOrderDetails)
      this.subObsOrderDetails.unsubscribe();
    if (this.subObsOrderDetailsError)
      this.subObsOrderDetailsError.unsubscribe();
  }
}
