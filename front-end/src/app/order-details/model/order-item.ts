export class OrderItem{
    productId:number;
    productName:string;
    productImage:string;
    quantity:number;
    mealType:string;
    productOptions:string;
}