import { LoadingModule } from 'ngx-loading';
import { OrderDetailsService } from './order-details.service';
import { OrderDetailsEffects } from './order-detals.effects';
import { OrderDetailsComponent } from './order-details.component';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    FormsModule,
    EffectsModule.forFeature([OrderDetailsEffects]),
    BrowserModule
  ],
  declarations: [OrderDetailsComponent],
  exports: [OrderDetailsComponent],
  providers: [OrderDetailsService]
})
export class OrderDetailsModule { }
