import * as OpenSidebarActions from '../actions/open-sidebar.actions';

export interface State {
  sidebarStatus:Boolean;
}

const initialState: State = {
    sidebarStatus : false
}

export function reducer(state = initialState, action: OpenSidebarActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case OpenSidebarActions.OPENSIDEBAR: {
            return {
                ...state,
                sidebarStatus:new Boolean(action.sidebarStatus)
            }
        }

        default: {
            return state;
        }

    }

}
