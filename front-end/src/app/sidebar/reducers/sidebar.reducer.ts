import * as SidebarActions from '../actions/sidebar.action';

export interface State {
  sidebarName:string
}

const initialState: State = {
   sidebarName : ''
}

export function reducer(state = initialState, action: SidebarActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case SidebarActions.SIDEBARNAME: {
            return {
                ...state,
                sidebarName:action.sidebarName
            }
        }

        default: {
            return state;
        }

    }

}
