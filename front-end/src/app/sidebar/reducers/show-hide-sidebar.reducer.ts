import * as ShowHideSidebarActions from '../actions/show-hide-sidebar.actions';

export interface State {
  sidebarStatus:Boolean;
}

const initialState: State = {
    sidebarStatus : true
}

export function reducer(state = initialState, action: ShowHideSidebarActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case ShowHideSidebarActions.SHOWHIDESIDEBAR: {
            return {
                ...state,
                sidebarStatus:new Boolean(action.sidebarStatus)
            }
        }

        default: {
            return state;
        }

    }

}
