import { Action } from '@ngrx/store';

export const SHOWHIDESIDEBAR = 'SHOWHIDE SIDEBAR';

export class ShowHideSidebar {
    readonly type = SHOWHIDESIDEBAR;
    constructor(public sidebarStatus:boolean) {
    }
}

export type All = ShowHideSidebar