import { Action } from '@ngrx/store';

export const OPENSIDEBAR = 'OPENSIDEBAR';

export class OpenSidebar {
    readonly type = OPENSIDEBAR;
    constructor(public sidebarStatus:boolean) {
    }
}

export type All = OpenSidebar