import { Action } from '@ngrx/store';

export const SIDEBARNAME = 'SIDEBAR NAME';

export class SetSidebarName {
    sidebarName:string
    readonly type = SIDEBARNAME;
    constructor(sidebarName:string) {
        this.sidebarName = sidebarName; 
    }
}

export type All = SetSidebarName