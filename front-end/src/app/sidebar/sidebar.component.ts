import { SchoolInfo } from './../common/school-info/model/school-info';
import { Parent } from './../user-profile/model/parent';
import { Subscription } from 'rxjs';
import { CategoriesComponent } from './../categories/categories.component';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from './../cart/reducers/show-hide-cart-modal-reducer';
import { Component, Output, EventEmitter, OnInit, ViewChild, QueryList, ElementRef, OnDestroy, HostListener } from '@angular/core';
import * as fromRoot from '../reducers';

@Component({
  selector: 'sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Output()
  hideSidebar: EventEmitter<any> = new EventEmitter();

  sidebarName: string;
  obsSidebarName: Observable<string>;
  subObsSidebarName: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;

  obsSchoolInfo: Observable<SchoolInfo>;
  subObsSchoolInfo: Subscription;

  showCategories: boolean = false;
  showCommonSideBar: boolean = true;
  isSidebarOpen = false;
  navbarClicked = false;
  schoolInfo: SchoolInfo;
  parent: Parent;
  isTopupEnabled: boolean = true;

  constructor(private _store: Store<fromRoot.State>, private _eref: ElementRef) {
    this.obsSidebarName = this._store.select(fromRoot.selectSideBarName);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
    this.obsSchoolInfo = this._store.select(fromRoot.selectListSchoolInfoSuccess);
  }

  ngOnInit() {
    this.subObsSidebarName = this.obsSidebarName.subscribe(result => {
      this.sidebarName = result;
      if (this.sidebarName == 'categories') {
        this.showCategories = true;
        this.showCommonSideBar = false;
      }
      else if (this.sidebarName == 'commonSidebar') {
        this.showCommonSideBar = true;
        this.showCategories = false;
      }
    })

    this.subObsListParent = this.obsListParent.subscribe(result => {
      this.parent = result;
    })

    this.subObsSchoolInfo = this.obsSchoolInfo.subscribe(result => {
      this.schoolInfo = result;
      if (this.schoolInfo && this.schoolInfo.topup_method && this.schoolInfo.topup_method.includes('4')) {
        this.isTopupEnabled = true;
      }
      else {
        this.isTopupEnabled = false;
      }
    })
  }

  hideSidebarComponent() {
    this.hideSidebar.emit();
    this.isSidebarOpen = false;
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) {
      if (this.isSidebarOpen && !this.navbarClicked) {
        this.hideSidebar.emit();
        this.isSidebarOpen = false;
      }
      else {
        this.navbarClicked = false;
      }
    }
  }

  ngOnDestroy() {
    if (this.subObsSidebarName)
      this.subObsSidebarName.unsubscribe();
  }

  @HostListener('window:popstate') backbuttonpressed() {
    this.hideSidebarComponent();
  }
}