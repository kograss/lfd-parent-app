import { AlertService } from './../alert/alertService';
import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { CouponComponent } from './coupons.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
//import { TransactionHistoryEffects } from './transaction-history-effects';
//import { TransactionHistoryService } from './transaction-history.service';
import { Coupon } from './model/Coupon';
import { CouponRedeem } from './model/CouponRedeem';
import { DataTableModule } from 'angular2-datatable';
import { BrowserModule } from '@angular/platform-browser';
import { Config } from './../config/config';
import { CouponsEffects } from './coupons.effects';
import { CouponsService } from './coupons.service';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        DataTableModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        EffectsModule.forFeature([CouponsEffects])        
    ],
    declarations: [CouponComponent],
    exports: [CouponComponent],
    providers: [ CouponsService, AlertService, Coupon, CouponRedeem]
})
export class CouponsModule {

}
