

import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListCouponsActions from './actions/list-active-coupons';
import * as RedeemCouponActions from './actions/redeem-action.coupon';

import { CouponsService } from './coupons.service';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class CouponsEffects {

    constructor(
        private actions$: Actions,
        private couponsService: CouponsService
    ) { }

    @Effect()
    listTransactionHistory$: Observable<Action> = this.actions$.pipe(ofType<ListCouponsActions.ListCoupons>(ListCouponsActions.LISTCOUPON)
        ,map(action => action)
        ,switchMap(
        payload => this.couponsService.getCoupons().pipe(
            map(results => new ListCouponsActions.ListCouponsSuccess(results))
            ,catchError(err =>
                of({ type: ListCouponsActions.LISTCOUPON_FAILURE, exception: err })
            )))
        );

        @Effect()
        redeemCoupon$: Observable<Action> = this.actions$.pipe(ofType<RedeemCouponActions.RedeemCoupons>(RedeemCouponActions.REDEEMCOUPON)
            ,map(action => action)
            ,switchMap(
            payload => this.couponsService.redeemCoupon(payload.couponId).pipe(
                map(results => new RedeemCouponActions.RedeemCouponsSuccess(results))
                ,catchError(err =>
                    of({ type: RedeemCouponActions.REDEEMCOUPON_FAILURE, exception: err })
                )))
            );
}