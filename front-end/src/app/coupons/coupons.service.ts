import { Observable} from 'rxjs';
import { Coupon } from './model/Coupon';
import { Injectable } from '@angular/core';
import { RedeemResult } from './model/RedeemResult';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CouponsService {
    private _getCouponsUrl = 'Coupon/GetAllCoupons';
    private _redeemCouponUrl = 'Coupon/RedeemCoupon';

    constructor(private _http: HttpClient) { }
    
    getCoupons():Observable<any> {
        return this._http.get(this._getCouponsUrl);
      }

      redeemCoupon(couponId):Observable<any> {
        let headers = new HttpHeaders(
            {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
            });
          let options = { headers: headers };
          let body = JSON.stringify({couponId: couponId});
          return this._http.post(this._redeemCouponUrl, body, options);
      }
}