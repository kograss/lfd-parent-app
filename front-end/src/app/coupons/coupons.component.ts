import { AlertService } from './../alert/alertService';
import { AlertComponent } from './../alert/alert.component';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Coupon } from './model/Coupon';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from '../reducers';
import * as ListCouponsActions from './actions/list-active-coupons';
import * as RedeemCouponsActions from './actions/redeem-action.coupon';
import { CouponRedeem } from './model/CouponRedeem';
import { RedeemResult } from './model/RedeemResult';
import * as ListParentActions from './../user-profile/actions/list-user-action';


declare var $: any;

@Component({
    selector: 'coupon-list',
    templateUrl: './coupons.component.html',
    styleUrls: ['./coupons.component.css']
})
export class CouponComponent implements OnInit, OnDestroy {
    message: string;
    coupons: Coupon[];
    showSpinner = false;
    redeemResult: RedeemResult;

    obsListCoupons: Observable<Coupon[]>;
    subsObsListCoupons: Subscription;
    obsListCouponsFailure: Observable<any>;
    subObsListCouponsFailure: Subscription;



    obsRedeemCoupon: Observable<RedeemResult>;
    subsObsRedeemCoupons: Subscription;
    obsRedeemCouponsFailure: Observable<any>;
    subObsRedeemCouponsFailure: Subscription;



    constructor(private _store: Store<fromRoot.State>,
        private _router: Router, private alertService: AlertService) {

        this.obsListCoupons = this._store.select(fromRoot.selectGetActiveCouponsSuccess);
        this.obsListCouponsFailure = this._store.select(fromRoot.selectGetActiveCouponsFailure);

        this.obsRedeemCoupon = this._store.select(fromRoot.selectRedeemCouponSuccess);
        this.obsRedeemCouponsFailure = this._store.select(fromRoot.selectRedeemCouponSuccessFailure);

    }

    ngOnInit() {
        let isOnInit = true;

        this._store.dispatch(new ListCouponsActions.ListCoupons);
        this.subsObsListCoupons = this.obsListCoupons.subscribe(result => {
            if (result) {
                this.coupons = result;
            }
            this.showSpinner = false;
        })

        this.subObsListCouponsFailure = this.obsListCouponsFailure.subscribe(error => {
            if (!isOnInit) {
                //TODO: add alert
            }
        })


        this.subsObsRedeemCoupons = this.obsRedeemCoupon.subscribe(result => {
            if (!isOnInit) {
                if (result) {
                    console.log(result);
                    this.redeemResult = result;
                    this.alertService.info(this.redeemResult.message);
                     //update parent balance
                    this._store.dispatch(new ListParentActions.ListParent());
                    this._store.dispatch(new ListCouponsActions.ListCoupons);
                    this.showSpinner = false;
                }
            }
        })

        this.subObsRedeemCouponsFailure = this.obsRedeemCouponsFailure.subscribe(result => {
            this.showSpinner = false;
        })

        
        isOnInit = false;
    }

    redeemCoupon(couponId) {
        this.showSpinner = true;
        this._store.dispatch(new RedeemCouponsActions.RedeemCoupons(couponId));

    }

    ngOnDestroy() {
        if (this.subsObsListCoupons)
            this.subsObsListCoupons.unsubscribe();
        if (this.subObsListCouponsFailure)
            this.subObsListCouponsFailure.unsubscribe();
    }
}