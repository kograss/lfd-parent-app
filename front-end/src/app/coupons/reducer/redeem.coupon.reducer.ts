import * as RedeemCouponsActions from '../actions/redeem-action.coupon';
import { RedeemResult } from '../model/RedeemResult';

export interface State {
    result: RedeemResult,
    err: any
}

const initialState: State = {
    result: null,
    err: {}
};

export function reducer(state = initialState, action: RedeemCouponsActions.All): State {
    switch (action.type) {

        case RedeemCouponsActions.REDEEMCOUPON_SUCCESS: {
            return {
                ...state,
                result: action.redeemResult
            }
        }
        case RedeemCouponsActions.REDEEMCOUPON_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }
    }
}
