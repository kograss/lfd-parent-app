import * as ListCouponsActions from '../actions/list-active-coupons';
import { Coupon } from '../model/Coupon';

export interface State {
    result: Coupon[],
    err: any
}

const initialState: State = {
    result: [],
    err: {}
};

export function reducer(state = initialState, action: ListCouponsActions.All): State {
    switch (action.type) {

        case ListCouponsActions.LISTCOUPON_SUCCESS: {
            return {
                ...state,
                result: action.coupons
            }
        }
        case ListCouponsActions.LISTCOUPON_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }

        default: {
            return state;
        }
    }
}
