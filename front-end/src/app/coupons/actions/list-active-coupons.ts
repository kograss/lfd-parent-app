import { Coupon } from './../model/Coupon';
import { Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';


export const LISTCOUPON = 'LIST COUPON';
export const LISTCOUPON_SUCCESS = 'LIST COUPON SUCCESS';
export const LISTCOUPON_FAILURE = 'LIST COUPON FAILURE';


export class ListCoupons {
    readonly type = LISTCOUPON;
    constructor() {
    }
}

export class ListCouponsSuccess implements Action {
    readonly type = LISTCOUPON_SUCCESS;
    constructor(public coupons: Coupon[]) {
    }
}

export class ListCouponsFailure implements Action {
    readonly type = LISTCOUPON_FAILURE;
    constructor(public err: any) {
    }
}

export type All = ListCoupons
    | ListCouponsSuccess | ListCouponsFailure
