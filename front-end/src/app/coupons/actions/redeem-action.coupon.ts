import { Action } from '@ngrx/store';
import { RedeemResult } from '../model/RedeemResult';


export const REDEEMCOUPON = 'REDEEM COUPON';
export const REDEEMCOUPON_SUCCESS = 'REDEEM COUPON SUCCESS';
export const REDEEMCOUPON_FAILURE = 'REDEEM COUPON FAILURE';


export class RedeemCoupons {
    readonly type = REDEEMCOUPON;
    constructor(public couponId: number) {
    }
}

export class RedeemCouponsSuccess implements Action {
    readonly type = REDEEMCOUPON_SUCCESS;
    constructor(public redeemResult: RedeemResult) {
    }
}

export class RedeemCouponsFailure implements Action {
    readonly type = REDEEMCOUPON_FAILURE;
    constructor(public err: any) {
    }
}

export type All = RedeemCoupons
    | RedeemCouponsSuccess | RedeemCouponsFailure
