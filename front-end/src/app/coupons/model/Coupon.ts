export class Coupon {
    id: number;
    couponName: string;
    schoolId: number;
    couponPrice: number;
    expiryDate: Date;
    status: boolean;
}

