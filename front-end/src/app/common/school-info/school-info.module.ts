import { SchoolInfoEffects } from './school-info.effects';
import { SchoolInfoService } from './school-info.service';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([SchoolInfoEffects]),    
  ],
  declarations: [],
  exports: [],
  providers:[SchoolInfoService]
})
export class SchoolInfoModule { }
