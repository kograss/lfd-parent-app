import { SchoolInfoService } from './school-info.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';

import * as ListSchoolInfoActions from './actions/list-school-info.actions';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class SchoolInfoEffects {

    constructor(
        private actions$: Actions,
        private schoolInfoService: SchoolInfoService
    ) { }

    @Effect()
    listSchoolInfo$: Observable<Action> = this.actions$.pipe(ofType<ListSchoolInfoActions.ListSchoolInfo>
        (ListSchoolInfoActions.LISTSCHOOLINFO),
        map(action=>action)
        ,switchMap(
        payload => this.schoolInfoService.getSchoolInfo().pipe(
            map(results => new ListSchoolInfoActions.ListSchoolInfoSuccess(results)),
        catchError(err =>
                of({ type: ListSchoolInfoActions.LISTSCHOOLINFO_FAILURE, exception: err })
            )
        )));
}