import { SchoolInfo } from './../model/school-info';
import * as ListSchoolInfoActions from '../actions/list-school-info.actions';
export interface State{
    result : SchoolInfo,
    err:any
}

const initialState: State = {
    result : new SchoolInfo(),
    err : {}
}

export function reducer(state = initialState, action: ListSchoolInfoActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListSchoolInfoActions.LISTSCHOOLINFO_SUCCESS: {
            return {
                ...state,
                result: action.schoolInfo
            }
        }
        case ListSchoolInfoActions.LISTSCHOOLINFO_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
