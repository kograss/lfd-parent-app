import { SchoolInfo, SchoolShops } from './model/school-info';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SchoolInfoService {
    private _url = "School/GetSchoolInfo/";

    constructor(private _http: HttpClient) { }

    getSchoolInfo() {
        return this._http
            .get(this._url);
    }

    getSchoolShops(): Observable<SchoolShops> {
      return this._http
        .get<SchoolShops>('School/GetSchoolShops');
        // .map(res => res.json());
    }
}