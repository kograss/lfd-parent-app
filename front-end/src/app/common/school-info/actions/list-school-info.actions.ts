import { SchoolInfo } from './../model/school-info';
import { Action } from '@ngrx/store';

export const LISTSCHOOLINFO = '[SCHOOLINFO] LIST';
export const LISTSCHOOLINFO_SUCCESS = '[SCHOOLINFO] LIST SUCCESS';
export const LISTSCHOOLINFO_FAILURE = '[SCHOOLINFO] LIST FAILURE';

export class ListSchoolInfo {
    readonly type = LISTSCHOOLINFO;
    constructor() {
       
    }
}

export class ListSchoolInfoSuccess implements Action {

    readonly type = LISTSCHOOLINFO_SUCCESS;
    constructor(public schoolInfo) {

    }

}

export class ListSchoolInfoFailure implements Action {

    readonly type = LISTSCHOOLINFO_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListSchoolInfo
    | ListSchoolInfoSuccess | ListSchoolInfoFailure 