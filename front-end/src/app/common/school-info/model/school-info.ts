export class SchoolInfo {
  schoolid: number;
  shop: string;
  chargeforbag: string;
  bagprice: number;
  chargeforbaglunch: string;
  bagpricelunch: number;
  allow_recycle: boolean;
  recycleprice: number;
  canteen_status: boolean;
  s_fee_plan: number;
  fee_model: number;
  bag_policy: string;
  bag_policy_lunch: string;
  quartly_service_fee: string;
  order_service_fee: number;
  canteenopen: boolean;
  allowcomments: boolean;
  allowprocearly: boolean;
  roster_status: boolean;
  cbank: string;
  cbankaccountname: string;
  cbankaccount: string;
  cbankbsb: string;
  paymentmethod1: string;
  topup_method: string;
  cbankswiftcode: string;
  cmgremail: string;
  processed: boolean;
  uniform_status: boolean;
  uniform_open: boolean;
  isSplitMenuActivated: boolean;
  transfer_weekly_sales_only: boolean;
  canteenOrderServiceFee: number;
  daily_hour: number;
  daily_min: number;
}

export class SchoolShops {
  canteenShopName: string;
  uniformShopName: string;
}