import * as _ from "lodash";
import { Injectable } from '@angular/core';

@Injectable()
export class DataFilter {
    transform(array: any[], query: string, columnName: string): any {
        if (query) {
            return _.filter(array, row => row.columnName.toLowerCase().indexOf(query.toLowerCase()) > -1);
        }
        return array;
    }
}