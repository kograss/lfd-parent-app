import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appBlockCopyPaste]'
})
export class BlockCopyPasteDirective {
  constructor() { }

  @HostListener('paste', ['$event']) blockPaste(e: ClipboardEvent) {
    const text = e.clipboardData.getData('text/plain');
    if (/[a-zA-Z0-9 ]/.test(text)) {
      return true;
    }
    else {
      e.preventDefault();
    }
  }
}