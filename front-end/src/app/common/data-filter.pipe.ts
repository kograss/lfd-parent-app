import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {

    transform(array: any[], query: string): any {
        if (query) {
            if (!this.isNumeric(query))
                return _.filter(array, row => row.studentName.toLowerCase().indexOf(query.toLowerCase()) > -1);
            else
                return _.filter(array, row => row.orderId.toString().indexOf(query) > -1);
        }
        return array;
    }

    isNumeric(input) {
        var numbers = /^[0-9]+$/;
        if (input.match(numbers))
            return true;
        else
            return false;
    }
}