import { LoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { CommonMenuComponent } from './common-menu.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        LoadingModule.forRoot({
            fullScreenBackdrop: true
        }),
        EffectsModule.forFeature([])        
    ],
    declarations: [CommonMenuComponent],
    exports: [CommonMenuComponent],
    providers: []
})
export class CommonMenuModule {

}
