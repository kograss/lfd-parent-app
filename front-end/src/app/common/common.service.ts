import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
    private _getAppVersionURL = 'values/androidmobileappversion';

    constructor(private _http: HttpClient) { }

    getAppVersion() {
        return this._http.get(this._getAppVersionURL)
    }

}