import { Volunteer } from './../model/volunteer-shifts';
import { Parent } from './../../user-profile/model/parent';
import { RegisterVolunteerInput } from './../model/register-volunteer-input';
import { ShiftInfo } from './../model/shift-info';
import { ShiftsByDate } from './../model/shift-by-date';
import { ShiftCalendarService } from './../shift-calendar.service';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, Input, OnInit, OnChanges, OnDestroy } from '@angular/core';
import * as fromRoot from '../../reducers';
// import { CalendarMonthCellComponent } from 'angular-calendar';
import * as RegisterVolunteerActions from './../action/register-volunteer.actions';

@Component({
  selector: 'shift-details',
  templateUrl: './shift-details.component.html',
  styleUrls: ['shift-details.component.css'],
})

export class ShiftDetailsComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  shifts: ShiftsByDate[] = [];

  @Input()
  date: Date;
  perDayShift: ShiftInfo[] = [];
  required: string;
  holiday: string;
  parent: Parent;
  shiftId: number;
  showSpinner: boolean;
  registerClicked: boolean;

  obsRegisterVolunteer: Observable<Boolean>;
  subObsRegisterVolunteer: Subscription;
  obsRegisterVolunteerFailure: Observable<any>;
  subObsRegisterVolunteerFailure: Subscription;

  obsListParent: Observable<Parent>;
  subObsListParent: Subscription;

  constructor(private _store: Store<fromRoot.State>) {
    this.obsRegisterVolunteer = this._store.select(fromRoot.selectRegisterVolunteerSuccess);
    this.obsRegisterVolunteerFailure = this._store.select(fromRoot.selectRegisterVolunteerFailure);
    this.obsListParent = this._store.select(fromRoot.selectListParentInfoSuccess);
  }

  ngOnInit() {
    let isOnInit = true;
    this.subObsRegisterVolunteer = this.obsRegisterVolunteer.subscribe(result => {
      if (!isOnInit) {
        if (result && this.registerClicked) {
          this.registerClicked = false;
          let index = this.perDayShift.findIndex(shift => shift.id == this.shiftId);
          if (index >= 0) {
            let volunteer = new Volunteer();
            volunteer.name = this.parent.firstName + " " + this.parent.lastName;
            volunteer.id = this.parent.parentId;
            volunteer.email = this.parent.email;
            volunteer.mobile = this.parent.mobile;
            this.perDayShift[index].isRegistered = true;
            this.perDayShift[index].noOfVolReq -= 1;
            this.perDayShift[index].volunteers.push(volunteer)
          }
          this.showSpinner = false;
        }
        else {
        }
      }
    })

    this.subObsRegisterVolunteerFailure = this.obsRegisterVolunteerFailure.subscribe(
      err => {
        if (!isOnInit) {
          if (err && this.registerClicked) {
            this.registerClicked = false;
            let errormessage = err._body ? err._body : err.error;
            alert(errormessage);
          }
          this.showSpinner = false;
        }
      })

    this.subObsListParent = this.obsListParent.subscribe(result => {
      if (result) {
        this.parent = result;
      }
    })
    isOnInit = false;
  }

  register(shift: ShiftInfo) {
    if (confirm('Are you sure you want to register to the shift?')) {
      this.registerClicked = true;
      this.showSpinner = true;
      this.shiftId = shift.id;
      let volunteer = new RegisterVolunteerInput();
      volunteer.date = shift.date;
      volunteer.shiftId = shift.id;
      volunteer.shiftName = shift.name;
      this._store.dispatch(new RegisterVolunteerActions.RegisterVolunteer(volunteer));
    }
  }

  ngOnChanges() {
    this.shifts.forEach(perDayShifts => {
      if (perDayShifts.isHoliday) {
        this.holiday = "[Canteen Closed]"
      }
      else if (perDayShifts != null) {
        if (perDayShifts.shifts) {
          this.perDayShift = [];
          perDayShifts.shifts.forEach(shift => {
            let index = -1;
            if (this.parent) {
              index = shift.volunteers.findIndex(volunteer => volunteer.id == this.parent.parentId);
            }
            if (index != -1)
              shift.isRegistered = true;
            this.perDayShift.push(shift);
          })
        } else if (perDayShifts.isHoliday) {
          this.holiday = "[Holiday]"
        }
      }

    })
  }

  ngOnDestroy() {
    if (this.subObsRegisterVolunteer)
      this.subObsRegisterVolunteer.unsubscribe();
    if (this.subObsRegisterVolunteerFailure)
      this.subObsRegisterVolunteerFailure.unsubscribe();
    if (this.subObsListParent)
      this.subObsListParent.unsubscribe();
  }
}
