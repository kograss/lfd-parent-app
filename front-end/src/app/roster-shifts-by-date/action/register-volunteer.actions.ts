import { RegisterVolunteerInput } from './../model/register-volunteer-input';
import { Action } from '@ngrx/store';

export const REGISTERVOLUNTEER = 'REGISTERVOLUNTEER';
export const REGISTERVOLUNTEER_SUCCESS = '[REGISTERVOLUNTEER] SUCCESS';
export const REGISTERVOLUNTEER_FAILURE = '[REGISTERVOLUNTEER] FAILURE';

export class RegisterVolunteer {
    readonly type = REGISTERVOLUNTEER;
    constructor(public volunteer:RegisterVolunteerInput) {
    }
}

export class RegisterVolunteerSuccess implements Action {

    readonly type = REGISTERVOLUNTEER_SUCCESS;
    constructor(public result:Boolean) {
    }

}

export class RegisterVolunteerFailure implements Action {

    readonly type = REGISTERVOLUNTEER_FAILURE;
    constructor(public err: any) {

    }
}

export type All = RegisterVolunteer
    | RegisterVolunteerSuccess | RegisterVolunteerFailure 