import { ShiftsByDate } from './../model/shift-by-date';
import { Action } from '@ngrx/store';

export const LISTSHIFTS = 'LISTSHIFTS';
export const LISTSHIFTS_SUCCESS = '[LISTSHIFTS] SUCCESS';
export const LISTSHIFTS_FAILURE = '[LISTSHIFTS] FAILURE';

export class ListShifts {
    readonly type = LISTSHIFTS;
    constructor(public month:number,public year:number) {
    }
}

export class ListShiftsSuccess implements Action {

    readonly type = LISTSHIFTS_SUCCESS;
    constructor(public shiftsByDate: ShiftsByDate[]) {
    }

}

export class ListShiftsFailure implements Action {

    readonly type = LISTSHIFTS_FAILURE;
    constructor(public err: any) {

    }
}

export type All = ListShifts
    | ListShiftsSuccess | ListShiftsFailure 