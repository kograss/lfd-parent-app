import { ShiftsByDate } from './../model/shift-by-date';
import * as ListShiftsActions from '../action/shift-by-date.actions';

export interface State{
    result : ShiftsByDate[],
    err:any
}

const initialState: State = {
    result : [],
    err : {}
}

export function reducer(state = initialState, action: ListShiftsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListShiftsActions.LISTSHIFTS_SUCCESS: {
            return {
                ...state,
                result: action.shiftsByDate
            }
        }
        case ListShiftsActions.LISTSHIFTS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
