import * as RegisterVolunteerActions from '../action/register-volunteer.actions';

export interface State{
    result : Boolean,
    err:any
}

const initialState: State = {
    result : false,
    err : {}
}

export function reducer(state = initialState, action: RegisterVolunteerActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case RegisterVolunteerActions.REGISTERVOLUNTEER_SUCCESS: {
            return {
                ...state,
                result: new Boolean(action.result)
            }
        }
        case RegisterVolunteerActions.REGISTERVOLUNTEER_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }    
          

        default: {
            return state;
        }

    }

}
