import { ShiftsByDate } from './model/shift-by-date';
import { RosterShift } from './../roster-home/model/roster-shift';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Config } from './../config/config';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Component, OnInit, Inject, LOCALE_ID, OnChanges, OnDestroy } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import * as fromRoot from '../reducers';
import * as ListShiftsActions from './action/shift-by-date.actions';

@Component({
  selector: 'shifts-by-date',
  templateUrl: './shifts-by-date.component.html',
  styleUrls: ['./shift-by-date.component.css']
})

export class ShiftsByDateComponent implements OnInit, OnDestroy {
  view = 'month';
  viewDate: Date = new Date();
  events: CalendarEvent[] = [];
  shifts: ShiftsByDate[] = [];
  calDate: Date;
  currentDate: Date;
  showSpinner: boolean;
  selectedMonth: number;

  obsListShifts: Observable<ShiftsByDate[]>;
  subObsListShifts: Subscription;
  obsListShiftsFailure: Observable<any>;
  subObsListShiftsFailure: Subscription;

  constructor(private _store: Store<fromRoot.State>,
    @Inject(LOCALE_ID) private _locale: string,
    private _route: ActivatedRoute) {
    this.obsListShifts = this._store.select(fromRoot.selectListAllRosterShiftsCalendarSuccess);
    this.obsListShiftsFailure = this._store.select(fromRoot.selectListAllRosterShiftsCalendarFailure);
  }

  ngOnInit() {
    this._route.params.subscribe(param => {
      this.selectedMonth = param['selectedMonth'];
    })
    if (!this.selectedMonth) {
      this.selectedMonth = this.viewDate.getMonth();
    }

    let isOnInit = true;

    this.calDate = new Date();
    this.calDate.setDate(1);
    this.calDate.setMonth(this.selectedMonth);
    this.calDate = new Date(this.calDate);

    this.currentDate = new Date();
    this.currentDate.setDate(1);

    this.viewDate.setMonth(this.selectedMonth);
    this.viewDate = new Date(this.viewDate);
    if (this.shifts.length == 0) {
      this.getAllShifts();
    }
    this.subObsListShifts = this.obsListShifts.subscribe(result => {
      if (!isOnInit) {
        if (result) {
          this.shifts = result;
        }
        console.log(result);
        this.showSpinner = false;
      }
    })

    this.subObsListShiftsFailure = this.obsListShiftsFailure.subscribe(err => {
      if (!isOnInit) {
        this.showSpinner = false;
      }
    })
    isOnInit = false;
  }

  getShift(date: Date) {
    return this.shifts.filter(shift => new Date(shift.date).getMonth() == date.getMonth() && new Date(shift.date).getDate() == date.getDate());
  }

  getAllShifts() {
    this.showSpinner = true;
    this._store.dispatch(new ListShiftsActions.ListShifts(this.calDate.getMonth(), this.calDate.getFullYear()));
  }

  navigateToNextMonth() {
    this.calDate.setDate(1);
    this.calDate.setMonth(this.calDate.getMonth() + 1);
    this.calDate = new Date(this.calDate);
    this.viewDate.setDate(1);
    this.viewDate.setMonth(this.viewDate.getMonth() + 1);
    this.viewDate = new Date(this.viewDate);
    this.getAllShifts();
  }

  navigateToPrevMonth() {
    this.calDate.setDate(1);
    this.calDate.setMonth(this.calDate.getMonth() - 1);
    this.calDate = new Date(this.calDate);
    this.viewDate.setDate(1);
    this.viewDate.setMonth(this.viewDate.getMonth() - 1);
    this.viewDate = new Date(this.viewDate);
    this.getAllShifts();
  }

  ngOnDestroy() {
    if (this.subObsListShifts)
      this.subObsListShifts.unsubscribe();
  }
}