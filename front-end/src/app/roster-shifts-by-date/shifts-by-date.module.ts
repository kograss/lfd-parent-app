import { LoadingModule } from 'ngx-loading';
import { ShiftCalendarService } from './shift-calendar.service';
import { ShiftByDateEffects } from './shift-by-date.effects';
import { EffectsModule } from '@ngrx/effects';
import { ShiftDetailsComponent } from './shifts-details/shift-details.component';
import { ShiftsByDateComponent } from './shifts-by-date.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EffectsModule.forFeature([ShiftByDateEffects]),    
    BrowserAnimationsModule,
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  declarations: [ShiftsByDateComponent, ShiftDetailsComponent ],
  exports: [ShiftsByDateComponent, ShiftDetailsComponent ],
  providers: [ShiftCalendarService]
})
export class ShiftsByDateModule {}