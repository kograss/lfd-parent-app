import { RegisterVolunteerInput } from './model/register-volunteer-input';
import { ShiftsByDate } from './model/shift-by-date';
import { Product } from './../products/model/product';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ShiftCalendarService {

    private _getShiftByDateUrl = "Rota/GetMonthsShifts/{month}/{year}";
    private _registerVolunteer = "Rota/RegisterForShift";

    constructor(private _http: HttpClient) {
    }

    getShiftsByDate(month: number, year: number): Observable<any> {

        return this._http
            .get(this._getShiftByDateUrl.replace('{month}', '' + month).replace('{year}', '' + year))
            ;
    }


    registerVolunteer(voluteerInput: RegisterVolunteerInput): Observable<any> {
        let headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin':'*'
            });
        let options = { headers: headers };
        let body = JSON.stringify(voluteerInput);
        return this._http
            .post(this._registerVolunteer, body, options)
            ;
    }
}

