import { ShiftCalendarService } from './shift-calendar.service';


import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of, Subscription } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListShiftsActions from './action/shift-by-date.actions';
import * as RegisterVolunteerActions from './action/register-volunteer.actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class ShiftByDateEffects {

    constructor(
        private actions$: Actions,
        private shiftCalendarService: ShiftCalendarService
    ) { }

    @Effect()
    listShifts$: Observable<Action> = this.actions$.pipe(ofType<ListShiftsActions.ListShifts>
        (ListShiftsActions.LISTSHIFTS)
        , map(action => action)
        , switchMap(
            payload => this.shiftCalendarService.getShiftsByDate(payload.month, payload.year).pipe(
                map(results => new ListShiftsActions.ListShiftsSuccess(results))
                , catchError(err =>
                    of({ type: ListShiftsActions.LISTSHIFTS_FAILURE, exception: err })
                )))
    );

    @Effect()
    registerVolunteer$: Observable<Action> = this.actions$.pipe(ofType<RegisterVolunteerActions.RegisterVolunteer>
        (RegisterVolunteerActions.REGISTERVOLUNTEER)
        , map(action => action)
        , switchMap(
            payload => this.shiftCalendarService.registerVolunteer(payload.volunteer).pipe(
                map(results => new RegisterVolunteerActions.RegisterVolunteerSuccess(results))
                , catchError(err =>
                    of(new RegisterVolunteerActions.RegisterVolunteerFailure(err))
                )))
    );

}
