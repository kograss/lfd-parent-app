export class Volunteer{
    id:number;
    name:string;
    email:string;
    mobile:string;
}