import { ShiftInfo } from './shift-info';
export class ShiftsByDate{
    date:string;
    isHoliday:boolean;
    isNonWorkingday:boolean;
    shifts:ShiftInfo[]=[];
}