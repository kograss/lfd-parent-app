import { Volunteer } from './volunteer-shifts';
export class ShiftInfo{
    id:number;
    name:string;
    shiftMax:number;
    leader:number;
    noOfVolReq:number;
    date:string;
    volunteers:Volunteer[]=[];
    isRegistered:boolean;
    isPastDay: boolean;
}