import { AlertService } from './../alert/alertService';
import { Config } from './../config/config';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as fromRoot from './../reducers';


@Component({
  selector: 'support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit, OnDestroy {
  

  constructor(private _store: Store<fromRoot.State>,
    private _router: Router, private config: Config, private alertService: AlertService) {

    
  }

  ngOnInit() {

    
  }

  

  ngOnDestroy() {
  }
}
