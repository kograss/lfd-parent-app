import { ShiftsByDateComponent } from './roster-shifts-by-date/shifts-by-date.component';
import { RosterHomeComponent } from './roster-home/roster-home.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { UpdateParentPlanComponent } from './update-parent-plan/update-parent-plan.component';
import { TopUpComponent } from './top-up/top-up.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ChildrenComponent } from './children/children.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { NewsComponent } from './news/news.component';
import { HomeComponent } from './home/home.component';
import { CheckoutComponent } from './checkout/checkout-component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { CouponComponent } from './coupons/coupons.component';
import { EventDescriptionComponent } from './event/more-info/event-more-info.component';
import { HomeMainComponent } from './home-main/home-main.component';
import { EventComponent } from './event/event.component';
import { RegisterComponent } from './register/registration.component';
import { BannedFoodComponent } from './banned-food/banned-food.component';
import { ForgotPassword } from './forgot-password/forgot-password.action';
import { DonationConfirmationComponent } from './event/donation-confirmation/donation-confirmation.component';
import { UniformComponent } from './uniform/uniform/uniform.component';
import { UniformNewsComponent } from './uniform/uniform-news/uniform-news.component';
import { UniformProductsComponent } from './uniform/products/products.component';
import { UniformCheckoutComponent } from './uniform/checkout/uniform-checkout.component';
import { UniformOrderConfirmationComponent } from './uniform/order-confirmation/order-confirmation.component';
import { UniformOrderHistoryComponent } from './uniform/history/order-history.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './forgot-password/reset-password.component';
import { SupportComponent } from './support/support.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'canteenorder/login', pathMatch: 'full' },
      { path: 'canteenorder', redirectTo: 'canteenorder/login', pathMatch: 'full' },
      { path: 'canteenorder/login', component: LoginComponent },
      { path: 'canteenorder/register', component: RegisterComponent },
      { path: 'canteenorder/error/:status', component: ErrorPageComponent },
      { path: 'canteenorder/forgot-password', component: ForgotPasswordComponent },
      { path: 'canteenorder/ResetPassword/:randomString', component: ResetPasswordComponent },
      {
        path: 'canteenorder/dashboard', component: DashboardComponent,
        children: [
          { path: 'home', component: HomeMainComponent },
          { path: 'canteen', component: HomeComponent },
          { path: 'events', component: EventComponent },
          { path: 'news', component: NewsComponent },
          { path: 'checkout', component: CheckoutComponent },
          { path: 'history', component: OrderHistoryComponent },
          { path: 'students', component: ChildrenComponent },
          { path: 'products', component: ProductsComponent },
          { path: 'user-profile', component: UserProfileComponent },
          { path: 'order-confirmation', component: OrderConfirmationComponent },
          { path: 'top-up', component: TopUpComponent },
          { path: 'update-parent-plan', component: UpdateParentPlanComponent },
          { path: 'roster', component: RosterHomeComponent },
          { path: 'roster-calendar', component: ShiftsByDateComponent },
          { path: 'transaction-history', component: TransactionHistoryComponent },
          { path: 'coupons', component: CouponComponent },
          { path: 'event-more-info', component: EventDescriptionComponent },
          { path: 'ban-food', component: BannedFoodComponent },
          { path: 'donation-confirmation', component: DonationConfirmationComponent },
          { path: 'support', component: SupportComponent }
        ]
      },
      { path: 'uniformorder', redirectTo: 'uniformorder/home', pathMatch: 'full' },
      {
        path: 'uniformorder', component: DashboardComponent,
        children: [
          { path: '', redirectTo: 'uniformorder/home', pathMatch: 'full' },
          { path: 'home', component: UniformComponent },
          { path: 'news', component: UniformNewsComponent },
          { path: 'products', component: UniformProductsComponent },
          { path: 'checkout', component: UniformCheckoutComponent },
          { path: 'order-confirmation', component: UniformOrderConfirmationComponent },
          { path: 'history', component: UniformOrderHistoryComponent }
        ]
      }

      /* define app module routes here, e.g., to lazily load a module
         (do not place feature module routes here, use an own -routing.module.ts in the feature instead)
       */
    ], { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

