export const environment = {
  production: true,
  imageBaseUrl: '/uniformorder/assets/',
  // origin: 'https://www.school24.net.au/mpAPITest/api/',
  // loginUrl: 'https://www.school24.net.au/mpAPITest/token',
  // origin: 'https://school24.clikacademy.ma/api/',
  // loginUrl:'https://school24.clikacademy.ma/token'
  // origin: 'http://www.school24.net.au/mpAPI/api/',
  // loginUrl:'http://www.school24.net.au/mpAPI/token'

  origin: 'https://www.school24.net.au/mpAPI/api/',
  loginUrl: 'https://www.school24.net.au/mpAPI/token',

  // origin: 'http://ec2-52-32-84-25.us-west-2.compute.amazonaws.com:54422/api/',
  // loginUrl: 'http://ec2-52-32-84-25.us-west-2.compute.amazonaws.com:54422/token'

};