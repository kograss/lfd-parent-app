// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  imageBaseUrl: '/assets/',
  origin: 'http://localhost:54421/api/',
  loginUrl: 'http://localhost:54421/token',
  
  // origin: 'http://www.school24.net.au/mpAPITest/api/',
  // loginUrl:'http://www.school24.net.au/mpAPITest/token'
  // origin: 'http://localhost:54422/api/',
  // loginUrl: 'http://localhost:54422/token'
  // origin: 'https://www.school24.net.au/canteenorderWebAPI/api/',
  // loginUrl:'https://www.school24.net.au/canteenorderWebAPI/token',
  // origin: 'http://ec2-52-32-84-25.us-west-2.compute.amazonaws.com:54422/api/',
  // loginUrl: 'http://ec2-52-32-84-25.us-west-2.compute.amazonaws.com:54422/token'


  //origin: 'https://www.school24.net.au/mpAPI/api/',
  //loginUrl:'https://www.school24.net.au/mpAPI/token'

  // origin: 'http://52.32.84.25:6628/canteenorderWebApiDev/api/',
  // loginUrl: 'http://52.32.84.25:6628/canteenorderWebApiDev/token'
};
//https://52.32.84.25:4862/WebAPI/ --- ppe db url
//http://localhost:54422/
//https://www.school24.net.au/canteenorderWebAPI/' .. prod
